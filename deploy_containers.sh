#!/bin/bash

#
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#

function push_if_build_success {
  if [ $? -eq 0 ]; then
    docker push "$IMAGE_NAME":latest
    docker push "$IMAGE_NAME":"$VERSION"
  else
    echo "Failed to build $IMAGE_NAME"
    exit 1
  fi
}

echo "$DOCKER_PASSWORD" | docker login -u "$DOCKER_USERNAME" --password-stdin docker.io
root=$(pwd)
find . -type d -name "container" | while read -r path; do
  # extract the root module name, e.g. from 'users/spring-application/container' extract 'users'
  module=$(echo "$path" | cut -d "/" -f2)
  if [ "$module" = "automations" ]; then
    module=$module-$(echo "$path" | cut -d "/" -f3 | cut -d "-" -f1)
  fi
  IMAGE_NAME=$DOCKER_USERNAME/luckyauction-$module
  cd "$path" || (echo "Failed to enter to $path directory" && exit 1)
  docker build --build-arg JAR_FILE="$(echo ./*.jar)" -t "$IMAGE_NAME":latest -t "$IMAGE_NAME":"$VERSION" .
  push_if_build_success

  cd "$root" || (echo "Failed to return to $root directory" && exit 1)
done

# build container for web-app
cd web-app || (echo "Failed to enter to web-app directory" && exit 1)
IMAGE_NAME=$DOCKER_USERNAME/luckyauction-webapp
docker build -t "$IMAGE_NAME":latest -t "$IMAGE_NAME":"$VERSION" .
push_if_build_success