/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.util;

import java.time.Duration;
import java.util.Calendar;
import java.util.Date;

/**
 * Utilities to simplify operations with {@link Calendar} objects.
 */
public final class CalendarUtils {

    /**
     * Create a new Calendar instance starting from the specified Date.
     * @param date  the desired date
     * @return      a new Calendar instance, corresponding to the specified date
     */
    public static Calendar fromDate(final Date date) {
        final Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(date.getTime());
        return calendar;
    }

    /**
     * Add the specified amount of time to the starting datetime.<br>
     * Example: to add 3 hours to a starting date:
     * <pre>
     * finishDate = add(startingDate, 3, Calendar.HOUR);
     * </pre>
     * @param startingTime      the starting date
     * @param amount            the amount of time to add
     * @param field             the unity of time to add (e.g. {@link Calendar#HOUR})
     * @return                  the result of operation, in a new Calendar instance
     */
    public static Calendar add(final Calendar startingTime, final int amount, final int field) {
        final Calendar endingTime = (Calendar) startingTime.clone();
        endingTime.add(field, amount);
        return endingTime;
    }

    /**
     * Add the specified duration to the starting datetime.<br>
     * @param startingTime      the starting date
     * @param duration          the duration to add
     * @return                  the result of operation, in a new Calendar instance
     */
    public static Calendar add(final Calendar startingTime, final Duration duration) {
        return add(startingTime, (int) duration.toMillis(), Calendar.MILLISECOND);
    }

    /**
     * Get the difference between two dates.
     * @param to        the finish datetime
     * @param from      the starting datetime
     * @return          the result of operation, into a {@link Duration} object
     */
    public static Duration diff(final Calendar to, final Calendar from) {
        return Duration.ofMillis(to.getTimeInMillis() - from.getTimeInMillis());
    }

    /**
     * Get the age, given the birthdate.
     * @param birthDate     the birthdate
     * @return              the years elapsed since the birthdate (i.e. the age)
     */
    public static Integer getAge(final Calendar birthDate) {
        final Calendar today = Calendar.getInstance();
        int age = today.get(Calendar.YEAR) - birthDate.get(Calendar.YEAR);

        if (today.get(Calendar.DAY_OF_YEAR) < birthDate.get(Calendar.DAY_OF_YEAR)) {
            age--;
        }

        return age;
    }

    /**
     * This method makes a secure clone of the Calendar object. In fact,
     * if it is null it returns null without throwing any exception.
     * Otherwise, it returns the copy of the object.
     * @param datetime  the Calendar object to clone
     * @return          the cloned object if it is not null, otherwise it returns null
     */
    public static Calendar copy(final Calendar datetime) {
        return datetime == null ? null : (Calendar) datetime.clone();
    }

    /**
     * This method makes a secure clone of the Date object. In fact,
     * if it is null it returns null without throwing any exception.
     * Otherwise, it returns the copy of the object.
     * @param datetime  the Date object to clone
     * @return          the cloned object if it is not null, otherwise it returns null
     */
    public static Date copy(final Date datetime) {
        return datetime == null ? null : (Date) datetime.clone();
    }

    private CalendarUtils() { }
}
