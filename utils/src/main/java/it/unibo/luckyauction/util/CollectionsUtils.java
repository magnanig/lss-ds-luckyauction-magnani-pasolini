/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class CollectionsUtils<T> {

    private final Comparator<T> orderingCriteria;

    public CollectionsUtils(final Comparator<T> orderingCriteria) {
        this.orderingCriteria = orderingCriteria;
    }

    /**
     * Sort items in ascending order, according to the previously specified criteria.
     * @param items     the items to be sort
     * @return          a new list containing items properly sorted
     */
    public List<T> sort(final Collection<T> items) {
        return sortBy(items, orderingCriteria);
    }

    /**
     * Sort items in reverse order (i.e. descending), according to the previously
     * specified criteria.
     * @param items     the items to be sort
     * @return          a new list containing items properly sorted
     */
    public List<T> reversedSort(final Collection<T> items) {
        return sortBy(items, orderingCriteria.reversed());
    }

    private List<T> sortBy(final Collection<T> items, final Comparator<T> criteria) {
        return items.stream()
                .sorted(criteria)
                .collect(Collectors.toList());
    }

    /**
     * Get the list of elements that are in first collection, but not in
     * the second one, i.e. the set operation of difference
     * (<code>first - second</code>).
     * @param first     the first collection
     * @param second    the second collection, to subtract to the first
     * @param <T>       the type of elements contained in both collections
     * @return          the elements contained in the first collection but
     *                  not in the second one
     */
    public static <T> List<T> difference(final Collection<T> first, final Collection<T> second) {
        final List<T> difference = new ArrayList<>(first);
        difference.removeAll(second);
        return difference;
    }

    public static <T extends Comparable<T>> T min(final T first, final T second) {
        if (first.compareTo(second) <= 0) {
            return first;
        }
        return second;
    }

    public static <T extends Comparable<T>> T max(final T first, final T second) {
        if (min(first, second) == first) {
            return second;
        }
        return first;
    }
}
