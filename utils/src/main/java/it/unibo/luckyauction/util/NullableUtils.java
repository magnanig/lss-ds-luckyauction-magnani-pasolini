/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.util;

import java.util.Optional;
import java.util.function.Function;

public final class NullableUtils {

    private NullableUtils() { }

    /**
     * Apply the specified mapper on the object, only if it is not null.
     * @param nullableObject    the desired object, that may be null
     * @param mapper            the function to apply on the object, if it is not null
     * @param <T>               the type of the initial object (i.e. the function input type)
     * @param <R>               the type of the transformed object (i.e. the function return type)
     * @return                  the result of applying mapper on the object
     */
    public static <T, R> R safeCall(final T nullableObject, final Function<T, R> mapper) {
        return Optional.ofNullable(nullableObject)
                .map(mapper)
                .orElse(null);
    }

    /**
     * Apply the specified mapper on the object, only if it is not null (i.e. optional not empty).
     * @param nullableObject    the desired object, wrapped into nullable optional
     * @param mapper            the function to apply on the object, if it is not null
     * @param <T>               the type of the initial object (i.e. the function input type)
     * @param <R>               the type of the transformed object (i.e. the function return type)
     * @return                  the result of applying mapper on the object
     */
    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    public static <T, R> R safeCall(final Optional<T> nullableObject, final Function<T, R> mapper) {
        return safeCall(nullableObject.orElse(null), mapper);
    }

}
