/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.util;

import java.util.Objects;

/**
 * Utility class for performing sleeps.
 */
public class Sleeper {

    private static final Integer DEFAULT_EXTRA_SLEEP_MILLIS = 10;
    private final Integer extraSleepMillis;
    private Integer extraSleptMillis;

    /**
     * Create a new instance to use in order to perform sleeps.
     * @param extraSleepMillis  extra milliseconds to add to normal sleep
     */
    public Sleeper(final Integer extraSleepMillis) {
        this.extraSleepMillis = Objects.requireNonNull(extraSleepMillis);
        this.extraSleptMillis = 0;
    }

    /**
     * Create a new instance to use in order to perform sleeps. A default value of
     * ten milliseconds will be assigned as extra time.
     */
    public Sleeper() {
        this(DEFAULT_EXTRA_SLEEP_MILLIS);
    }

    /**
     * Each subsequent sleep will always take extra 10 ms, without accumulating delay.
     * @apiNote this method can be useful if you have to wait for an auction to finish,
     * but waiting takes a little extra time to allow the auction closing operations
     * to proceed properly.
     */
    public void sleepEquallyMoreThan(final Long millis) throws InterruptedException {
        extraSleptMillis = extraSleepMillis - extraSleptMillis;
        Thread.sleep(millis + extraSleptMillis);
    }

    /**
     * View method {@link #sleepEquallyMoreThan(Long) sleepEquallyMoreThan} for details.
     */
    public void sleepEquallyMoreThan(final Integer millis) throws InterruptedException {
        sleepEquallyMoreThan(millis.longValue());
    }

    /**
     * Classic sleep without introducing any delay. View method
     * {@link Thread#sleep(long) Thread.sleep} for more details.
     */
    public void sleep(final Integer millis) throws InterruptedException {
        Thread.sleep(millis);
    }
}
