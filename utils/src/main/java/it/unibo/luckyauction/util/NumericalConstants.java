/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.util;

/**
 * The integer numerical constants.
 */
@SuppressWarnings("checkstyle:JavadocVariable") // the name of each constant is autoesplicative
public final class NumericalConstants {

    public static final int ZERO = 0;
    public static final int ONE = 1;
    public static final int TWO = 2;
    public static final int THREE = 3;
    public static final int FOUR = 4;
    public static final int FIVE = 5;
    public static final int SIX = 6;
    public static final int SEVEN = 7;
    public static final int EIGHT = 8;
    public static final int NINE = 9;
    public static final int TEN = 10;
    public static final int TWENTY = 20;
    public static final int TWENTY_FIVE = 25;
    public static final int FIFTY = 50;
    public static final int ONE_HUNDRED = 100;
    public static final int ONE_HUNDRED_AND_FIFTY = 150;
    public static final int ONE_HUNDRED_AND_SIXTY = 160;
    public static final int ONE_HUNDRED_AND_SEVENTY = 170;
    public static final int TWO_HUNDRED = 200;
    public static final int THREE_HUNDRED = 300;
    public static final int FIVE_HUNDRED = 500;
    public static final int ONE_THOUSAND = 1000;

    public static final int BYTES_IN_KB = 1024;

    private NumericalConstants() { }
}
