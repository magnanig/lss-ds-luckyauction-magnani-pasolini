/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

class SleepTests {

    private static final int EXTRA_TIME = 10;

    @Test
    void testSleep() throws InterruptedException {
        final Sleeper sleeper = new Sleeper();
        final int sleepTime = 10;
        final long start = System.currentTimeMillis();
        sleeper.sleep(sleepTime);
        final long end = System.currentTimeMillis();
        final long elapsed = end - start;
        assertIsNearOver(elapsed, sleepTime);
    }

    @Test
    void testSleepMoreThan() throws InterruptedException {
        final Sleeper sleeper = new Sleeper(EXTRA_TIME);
        final long sleepTime = 20;
        final long start = System.currentTimeMillis();
        sleeper.sleepEquallyMoreThan(sleepTime);
        final long end = System.currentTimeMillis();
        final long elapsed = end - start;
        assertIsNearOver(elapsed, sleepTime + EXTRA_TIME);
    }

    @Test
    void testSleepEquallyMoreThan() throws InterruptedException {
        final Sleeper sleeper = new Sleeper(EXTRA_TIME);
        final long sleepTime = 20;
        long start = System.currentTimeMillis();
        sleeper.sleepEquallyMoreThan(sleepTime);
        long end = System.currentTimeMillis();
        long elapsed = end - start;
        assertIsNearOver(elapsed, sleepTime + EXTRA_TIME);
        start = System.currentTimeMillis();
        sleeper.sleepEquallyMoreThan(sleepTime);
        end = System.currentTimeMillis();
        elapsed = end - start;
        assertIsNearOver(elapsed, sleepTime - EXTRA_TIME);
    }

    private void assertIsNearOver(final long number, final long threshold) {
        final int delta = 5;
        assertTrue(number >= threshold - delta);
    }
}
