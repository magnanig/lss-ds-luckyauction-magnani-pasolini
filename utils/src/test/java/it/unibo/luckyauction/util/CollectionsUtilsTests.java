/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.util;

import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class CollectionsUtilsTests {

    @Test
    void ascAndDescSortingTest() {
        final CollectionsUtils<Integer> collectionsUtilsUtils = new CollectionsUtils<>(Integer::compare);
        final List<Integer> disorderedIntegerList = List.of(2, 10, 5);
        final List<Integer> integerListDesc = List.of(10, 5, 2);
        final List<Integer> integerListAsc = List.of(2, 5, 10);

        assertEquals(integerListDesc, collectionsUtilsUtils.reversedSort(disorderedIntegerList));
        assertEquals(integerListAsc, collectionsUtilsUtils.sort(disorderedIntegerList));

        assertNotEquals(integerListDesc, collectionsUtilsUtils.sort(disorderedIntegerList));
        assertNotEquals(integerListAsc, collectionsUtilsUtils.reversedSort(disorderedIntegerList));
    }

    @Test
    void minAndMaxTest() {
        assertEquals(10, CollectionsUtils.max(1, 10));
        assertEquals(10, CollectionsUtils.max(10, 1));
        assertEquals(1, CollectionsUtils.min(1, 10));
        assertEquals(1, CollectionsUtils.min(10, 1));
    }

    @Test
    void collectionsDifferenceTest() {
        assertEquals(List.of(1), CollectionsUtils.difference(List.of(1), Collections.emptyList()));
        assertEquals(List.of(1), CollectionsUtils.difference(List.of(1, 2, 10), List.of(10, 2)));
    }
}
