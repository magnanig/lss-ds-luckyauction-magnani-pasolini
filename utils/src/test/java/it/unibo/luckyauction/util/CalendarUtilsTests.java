/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.util;

import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.util.Calendar;
import java.util.Date;

import static it.unibo.luckyauction.util.NumericalConstants.FIVE;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class CalendarUtilsTests {

    @Test
    void convertDateToCalendarTest() {
        final Calendar oneYearAgoExpected = Calendar.getInstance();
        oneYearAgoExpected.add(Calendar.YEAR, -1);
        final Date oneYearAgoDate = oneYearAgoExpected.getTime();

        final Calendar oneYearAgoActual = CalendarUtils.fromDate(oneYearAgoDate);
        assertEquals(oneYearAgoExpected, oneYearAgoActual);
    }

    @Test
    void addUtilityTest() {
        final Calendar currentTime = Calendar.getInstance();
        final Calendar expectedTime = (Calendar) currentTime.clone();
        expectedTime.add(Calendar.HOUR, 3);
        assertEquals(expectedTime, CalendarUtils.add(currentTime, 3, Calendar.HOUR));
        assertEquals(expectedTime, CalendarUtils.add(currentTime, Duration.ofHours(3)));
    }

    @Test
    void differenceBetweenTwoCalendarTypeTest() {
        final Calendar currentTime = Calendar.getInstance();
        final Calendar fiveDaysBefore = (Calendar) currentTime.clone();
        fiveDaysBefore.add(Calendar.DATE, -FIVE);

        assertEquals(Duration.ofDays(FIVE), CalendarUtils.diff(currentTime, fiveDaysBefore));
    }

    @Test
    void convertBirthDateToAge() {
        final Calendar fiveYearBefore = Calendar.getInstance();
        fiveYearBefore.add(Calendar.YEAR, -FIVE);

        assertEquals(FIVE, CalendarUtils.getAge(fiveYearBefore));

        fiveYearBefore.add(Calendar.MONTH, 2);
        assertEquals(4, CalendarUtils.getAge(fiveYearBefore));

        assertEquals(0, CalendarUtils.getAge(Calendar.getInstance()));
    }

    @Test
    void copyUtilityTest() {
        final Calendar currentTime = Calendar.getInstance();
        assertEquals(currentTime, CalendarUtils.copy(currentTime));
        assertNull(CalendarUtils.copy((Calendar) null));
    }
}
