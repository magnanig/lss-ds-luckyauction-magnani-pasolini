dependencies {
    implementation("org.springframework.boot:spring-boot-starter-web:2.6.7")
    implementation("org.springframework:spring-webflux:5.3.19")
    implementation("io.projectreactor.netty:reactor-netty-http:1.0.18")
    implementation("io.netty:netty-resolver-dns-native-macos:4.1.74.Final:osx-aarch_64")

    implementation(project(":configuration"))
    implementation(project(":utils"))

    implementation(project(":users:adapter"))
    implementation(project(":movements:adapter"))
    implementation(project(":auctions:adapter"))
    implementation(project(":automations:adapter"))
}