/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.spring.util.api;

import it.unibo.luckyauction.auction.adapter.AuctionsHistoryWeb;
import it.unibo.luckyauction.auction.adapter.api.AuctionsHistoryApi;
import it.unibo.luckyauction.configuration.HostsConstants;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.web.reactive.function.client.WebClient;

import java.time.Duration;
import java.util.List;
import java.util.Optional;

import static it.unibo.luckyauction.spring.util.http.WebClientUtils.checkErrors;

public class SpringAuctionsHistoryApi implements AuctionsHistoryApi {

    private static final Duration THREE_SECONDS = Duration.ofSeconds(3);
    private static final String AUCTION_ID_FIELD = "auctionId";
    private static final String USERNAME_FIELD = "username";
    private final WebClient client;

    public SpringAuctionsHistoryApi() {
        client = WebClient.create(HostsConstants.AUCTIONS_HISTORY_API);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void createAuctionsHistory(final String auctionsHistoryId, final String username) {
        client.post()
                .uri(uri -> uri
                        .queryParam("auctionsHistoryId", auctionsHistoryId)
                        .queryParam("username", username)
                        .build())
                .retrieve()
                .toBodilessEntity()
                .blockOptional(THREE_SECONDS);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<AuctionsHistoryWeb> findByUsername(final String username) {
        return client.get()
                .uri(uri -> uri.queryParam(USERNAME_FIELD, username).build())
                .retrieve()
                .bodyToMono(AuctionsHistoryWeb.class)
                .blockOptional(THREE_SECONDS);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<String> deleteByUsername(final String username) {
        return client.delete()
                .uri(uri -> uri.queryParam(USERNAME_FIELD, username).build())
                .retrieve()
                .bodyToMono(String.class)
                .blockOptional(THREE_SECONDS);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addAwardedAuction(final String auctionId, final String username) {
        checkErrors(client.post()
                .uri(uri -> uri.path("/awarded")
                        .queryParam(AUCTION_ID_FIELD, auctionId)
                        .queryParam(USERNAME_FIELD, username)
                        .build())
                .retrieve())
                .toBodilessEntity()
                .blockOptional(THREE_SECONDS);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addCreatedEndedAuction(final String auctionId, final String username) {
        checkErrors(client.post()
                .uri(uri -> uri.path("/created/ended")
                        .queryParam(AUCTION_ID_FIELD, auctionId)
                        .queryParam(USERNAME_FIELD, username)
                        .build())
                .retrieve())
                .toBodilessEntity()
                .blockOptional(THREE_SECONDS);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addCreatedOngoingAuction(final String auctionId, final String username) {
        checkErrors(client.post()
                .uri(uri -> uri.path("/created/ongoing")
                        .queryParam(AUCTION_ID_FIELD, auctionId)
                        .queryParam(USERNAME_FIELD, username)
                        .build())
                .retrieve())
                .toBodilessEntity()
                .blockOptional(THREE_SECONDS);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addLostAuction(final String auctionId, final String username) {
        checkErrors(client.post()
                .uri(uri -> uri.path("/lost")
                        .queryParam(AUCTION_ID_FIELD, auctionId)
                        .queryParam(USERNAME_FIELD, username)
                        .build())
                .retrieve())
                .toBodilessEntity()
                .blockOptional(THREE_SECONDS);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addParticipatingAuction(final String auctionId, final String username) {
        checkErrors(client.post()
                .uri(uri -> uri.path("/participating")
                        .queryParam(AUCTION_ID_FIELD, auctionId)
                        .queryParam(USERNAME_FIELD, username)
                        .build())
                .retrieve())
                .toBodilessEntity()
                .blockOptional(THREE_SECONDS);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<String> getUsersParticipatingInAuction(final String auctionId) {
        return client.get()
                .uri(uri -> uri.path("/participating/users")
                        .queryParam(AUCTION_ID_FIELD, auctionId)
                        .build())
                .exchangeToMono(response -> response.bodyToMono(new ParameterizedTypeReference<List<String>>() { }))
                .block(THREE_SECONDS);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeParticipatingAuction(final String auctionId, final String username) {
        checkErrors(client.delete()
                .uri(uri -> uri.path("/participating")
                        .queryParam(AUCTION_ID_FIELD, auctionId)
                        .queryParam(USERNAME_FIELD, username)
                        .build())
                .retrieve())
                .toBodilessEntity()
                .blockOptional(THREE_SECONDS);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeCreatedOngoingAuction(final String auctionId, final String username) {
        checkErrors(client.delete()
                .uri(uri -> uri.path("/created/ongoing")
                        .queryParam(AUCTION_ID_FIELD, auctionId)
                        .queryParam(USERNAME_FIELD, username)
                        .build())
                .retrieve())
                .toBodilessEntity()
                .blockOptional(THREE_SECONDS);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeAuctionFromAllHistory(final String auctionId) {
        client.delete()
                .uri("/" + auctionId)
                .retrieve()
                .toBodilessEntity()
                .blockOptional(THREE_SECONDS);
    }
}
