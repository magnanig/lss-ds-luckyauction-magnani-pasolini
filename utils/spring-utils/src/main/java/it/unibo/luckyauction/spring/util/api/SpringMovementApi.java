/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.spring.util.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import it.unibo.luckyauction.configuration.HostsConstants;
import it.unibo.luckyauction.movement.adapter.AwardedAuctionTransactionsPairWeb;
import it.unibo.luckyauction.movement.adapter.FreezingWeb;
import it.unibo.luckyauction.movement.adapter.MovementsHistoryWeb;
import it.unibo.luckyauction.movement.adapter.TransactionWeb;
import it.unibo.luckyauction.movement.adapter.api.MovementApi;
import org.springframework.http.MediaType;
import org.springframework.http.codec.json.Jackson2JsonDecoder;
import org.springframework.web.reactive.function.client.ExchangeStrategies;
import org.springframework.web.reactive.function.client.WebClient;

import java.math.BigDecimal;
import java.time.Duration;
import java.util.Optional;

import static it.unibo.luckyauction.spring.util.http.WebClientUtils.checkErrors;

public class SpringMovementApi implements MovementApi {

    private static final Duration THREE_SECONDS = Duration.ofSeconds(3);
    private static final String USERNAME_FIELD = "username";
    private final WebClient client;

    public SpringMovementApi() {
        final ObjectMapper objectMapper = new ObjectMapper();
        final SimpleModule module = new SimpleModule();
        module.addDeserializer(TransactionWeb.class, new TransactionWebDeserializer());
        objectMapper.registerModule(module);

        client = WebClient.builder()
                .baseUrl(HostsConstants.MOVEMENTS_API)
                .exchangeStrategies(ExchangeStrategies.builder()
                        .codecs(configurator -> configurator.defaultCodecs().jackson2JsonDecoder(
                                new Jackson2JsonDecoder(
                                        objectMapper,
                                        new MediaType[]{ MediaType.APPLICATION_JSON })
                                ))
                        .build())
                .build();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void createMovementsHistory(final String movementsHistoryId, final String username) {
        client.post()
                .uri(uri -> uri.path("/histories")
                        .queryParam("movementsHistoryId", movementsHistoryId)
                        .queryParam(USERNAME_FIELD, username)
                        .build())
                .retrieve()
                .toBodilessEntity()
                .blockOptional(THREE_SECONDS);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<MovementsHistoryWeb> deleteMovementsHistory(final String username) {
        return client.delete()
                .uri(uri -> uri.path("/histories")
                        .queryParam(USERNAME_FIELD, username)
                        .build())
                .retrieve()
                .bodyToMono(MovementsHistoryWeb.class)
                .blockOptional(THREE_SECONDS);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Boolean userHasSufficientBudget(final String username, final BigDecimal sufficientBudget) {
        return checkErrors(client.get()
                .uri(uri -> uri.path("/budget")
                        .queryParam(USERNAME_FIELD, username)
                        .queryParam("sufficientBudget", sufficientBudget)
                        .build())
                .retrieve())
                .bodyToMono(Boolean.class)
                .block(THREE_SECONDS);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FreezingWeb freezeMoney(final String bidderUsername, final BigDecimal raisedAmount, final String auctionId,
                                   final String productName) {
        return checkErrors(client.post()
                .uri(uri -> uri.path("/freezes")
                        .queryParam("bidderUsername", bidderUsername)
                        .queryParam("raisedAmount", raisedAmount)
                        .queryParam("auctionId", auctionId)
                        .queryParam("productName", productName)
                        .build())
                .retrieve())
                .bodyToMono(FreezingWeb.class)
                .block(THREE_SECONDS);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FreezingWeb unfreezeMoney(final String username, final String auctionId) {
        return checkErrors(client.delete()
                .uri(uri -> uri.path("/freezes")
                        .queryParam(USERNAME_FIELD, username)
                        .queryParam("auctionId", auctionId)
                        .build())
                .retrieve())
                .bodyToMono(FreezingWeb.class)
                .block(THREE_SECONDS);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AwardedAuctionTransactionsPairWeb exchangeMoneyAfterAwardedAuction(final String buyerUsername,
                                                                              final String sellerUsername,
                                                                              final BigDecimal amount,
                                                                              final String auctionId,
                                                                              final String productName) {
        return checkErrors(client.post()
                .uri(uri -> uri.path("/exchange")
                        .queryParam("buyerUsername", buyerUsername)
                        .queryParam("sellerUsername", sellerUsername)
                        .queryParam("amount", amount)
                        .queryParam("auctionId", auctionId)
                        .queryParam("productName", productName)
                        .build())
                .retrieve())
                .bodyToMono(AwardedAuctionTransactionsPairWeb.class)
                .block(THREE_SECONDS);
    }
}
