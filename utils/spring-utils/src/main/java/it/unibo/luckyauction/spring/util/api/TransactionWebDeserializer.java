/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.spring.util.api;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import it.unibo.luckyauction.movement.adapter.AwardedAuctionTransactionWeb;
import it.unibo.luckyauction.movement.adapter.ChargingFromCardTransactionWeb;
import it.unibo.luckyauction.movement.adapter.MovementTypeWeb;
import it.unibo.luckyauction.movement.adapter.TransactionWeb;
import it.unibo.luckyauction.movement.adapter.WithdrawalTransactionWeb;

import java.io.IOException;
import java.io.Serial;

public class TransactionWebDeserializer extends StdDeserializer<TransactionWeb> {

    @Serial
    private static final long serialVersionUID = -8851566879042874202L;

    public TransactionWebDeserializer() {
        super(TransactionWeb.class);
    }

    /**
     * Deserialize a Transaction into the properly class.
     */
    @Override
    public TransactionWeb deserialize(final JsonParser jsonParser,
                                      final DeserializationContext deserializationContext) throws IOException {
        final ObjectCodec codec = jsonParser.getCodec();
        final JsonNode tree = codec.readTree(jsonParser);
        final JsonNode typeNode = tree.get("type");

        if (typeNode == null) {
            return null;
        }

        final MovementTypeWeb type = MovementTypeWeb.valueOf(typeNode.asText());
        return switch (type) {
            case CHARGING_FROM_CARD -> codec.treeToValue(tree, ChargingFromCardTransactionWeb.class);
            case WITHDRAWAL -> codec.treeToValue(tree, WithdrawalTransactionWeb.class);
            case AWARDED_AUCTION -> codec.treeToValue(tree, AwardedAuctionTransactionWeb.class);
            default -> throw new IllegalStateException("Unexpected value: " + type);
        };
    }
}
