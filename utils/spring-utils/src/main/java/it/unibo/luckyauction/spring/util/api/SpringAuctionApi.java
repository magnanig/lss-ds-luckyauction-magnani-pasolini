/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.spring.util.api;

import it.unibo.luckyauction.auction.adapter.AuctionWeb;
import it.unibo.luckyauction.auction.adapter.BidWeb;
import it.unibo.luckyauction.auction.adapter.api.AuctionApi;
import it.unibo.luckyauction.configuration.HostsConstants;
import it.unibo.luckyauction.util.NumericalConstants;
import org.springframework.web.reactive.function.client.ExchangeStrategies;
import org.springframework.web.reactive.function.client.WebClient;

import java.math.BigDecimal;
import java.time.Duration;
import java.util.Optional;

import static it.unibo.luckyauction.spring.util.http.WebClientUtils.checkErrors;

public class SpringAuctionApi implements AuctionApi {

    private static final Duration THREE_SECONDS = Duration.ofSeconds(3);
    private final WebClient client;

    public SpringAuctionApi() {
        final int maxBytes = 2 * NumericalConstants.BYTES_IN_KB * NumericalConstants.BYTES_IN_KB;
        client = WebClient.builder()
                        .exchangeStrategies(ExchangeStrategies.builder()
                                .codecs(codecs -> codecs.defaultCodecs().maxInMemorySize(maxBytes))
                                .build())
                .baseUrl(HostsConstants.AUCTIONS_API)
                .build();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<AuctionWeb> findById(final String auctionId) {
        return client.get()
                .uri("/" + auctionId)
                .retrieve()
                .bodyToMono(AuctionWeb.class)
                .blockOptional(THREE_SECONDS);
    }

    /**
     * {@inheritDoc}
     * @return
     */
    @Override
    public Optional<BidWeb> getActualBid(final String auctionId) {
        return checkErrors(client.get()
                .uri("/" + auctionId + "/bid")
                .retrieve())
                .bodyToMono(BidWeb.class)
                .blockOptional(THREE_SECONDS);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BigDecimal getStartingPrice(final String auctionId) {
        return checkErrors(client.get()
                .uri("/" + auctionId + "/startingPrice")
                .retrieve())
                .bodyToMono(BigDecimal.class)
                .block(THREE_SECONDS);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BidWeb addNewBid(final String auctionId, final String username, final BigDecimal bidAmount) {
        return checkErrors(client.post()
                .uri(uri -> uri.path("/" + auctionId + "/bids")
                        .queryParam("amount", bidAmount)
                        .queryParam("username", username)
                        .build())
                .retrieve())
                .bodyToMono(BidWeb.class)
                .block(THREE_SECONDS);
    }
}
