/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.spring.util.api;

import it.unibo.luckyauction.configuration.HostsConstants;
import it.unibo.luckyauction.user.adapter.UserAuthenticationWeb;
import it.unibo.luckyauction.user.adapter.UserWeb;
import it.unibo.luckyauction.user.adapter.api.UserApi;
import org.springframework.web.reactive.function.client.WebClient;

import java.time.Duration;
import java.util.Optional;

public class SpringUserApi implements UserApi {

    private static final Duration THREE_SECONDS = Duration.ofSeconds(3);
    private final WebClient client;

    public SpringUserApi() {
        client = WebClient.create(HostsConstants.USERS_API);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<UserWeb> findById(final String userId) {
        return client.get()
                .uri("/" + userId)
                .retrieve()
                .bodyToMono(UserWeb.class)
                .blockOptional(THREE_SECONDS);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<UserWeb> findByUsername(final String username) {
        return client.get()
                .uri(uri -> uri.path("/username")
                        .queryParam("username", username)
                        .build())
                .retrieve()
                .bodyToMono(UserWeb.class)
                .blockOptional(THREE_SECONDS);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<UserAuthenticationWeb> findUserAuthenticationByUsername(final String username) {
        return client.get()
                .uri(uri -> uri.path("/authentication")
                        .queryParam("username", username)
                        .build())
                .retrieve()
                .bodyToMono(UserAuthenticationWeb.class)
                .blockOptional(THREE_SECONDS);
    }
}
