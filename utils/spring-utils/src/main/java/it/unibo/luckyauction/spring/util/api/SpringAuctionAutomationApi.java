/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.spring.util.api;

import it.unibo.luckyauction.automation.adapter.UserAuctionsAutomationsWeb;
import it.unibo.luckyauction.automation.adapter.api.AuctionAutomationApi;
import it.unibo.luckyauction.configuration.HostsConstants;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.web.reactive.function.client.WebClient;

import java.time.Duration;
import java.util.List;
import java.util.Optional;

import static it.unibo.luckyauction.spring.util.http.WebClientUtils.checkErrors;

@SuppressWarnings("PMD.AvoidDuplicateLiterals")
public class SpringAuctionAutomationApi implements AuctionAutomationApi {

    private static final Duration THREE_SECONDS = Duration.ofSeconds(3);
    private final WebClient client;

    public SpringAuctionAutomationApi() {
        client = WebClient.create(HostsConstants.AUTOMATIONS_API);
    }

    /**
     * {@inheritDoc}
     * @param username
     */
    @Override
    public void createUserAuctionAutomations(final String username) {
        client.post()
                .uri(uri -> uri.queryParam("username", username).build())
                .retrieve()
                .toBodilessEntity()
                .blockOptional(THREE_SECONDS);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UserAuctionsAutomationsWeb> getAllActiveAutomations() {
        return client.get()
                .uri("/active")
                .exchangeToMono(response -> response
                        .bodyToMono(new ParameterizedTypeReference<List<UserAuctionsAutomationsWeb>>() { }))
                .block(THREE_SECONDS);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<UserAuctionsAutomationsWeb> deleteByUsername(final String username) {
        return client.delete()
                .uri(uri -> uri.queryParam("username", username).build())
                .retrieve()
                .bodyToMono(UserAuctionsAutomationsWeb.class)
                .blockOptional(THREE_SECONDS);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void disableAutomation(final String auctionId, final String username) {
        checkErrors(client.put()
                .uri(uri -> uri.path("/" + auctionId)
                        .queryParam("username", username)
                        .queryParam("active", false)
                        .build())
                .retrieve())
                .toBodilessEntity()
                .blockOptional(THREE_SECONDS);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeAuctionAutomation(final String auctionId, final String username) {
        client.delete()
                .uri(uri -> uri.path("/" + auctionId)
                        .queryParam("username", username)
                        .build())
                .retrieve()
                .toBodilessEntity()
                .blockOptional(THREE_SECONDS);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeAllAutomationsForAuction(final String auctionId) {
        client.delete()
                .uri("/" + auctionId)
                .retrieve()
                .toBodilessEntity()
                .blockOptional(THREE_SECONDS);
    }
}
