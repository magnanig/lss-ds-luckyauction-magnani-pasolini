module lss.ds.luckyauction.magnani.pasolini.utils.spring.main {
    exports it.unibo.luckyauction.spring.util.api;
    exports it.unibo.luckyauction.spring.util.http;
    requires spring.webflux;
    requires spring.web;
    requires org.apache.tomcat.embed.core;
    requires com.fasterxml.jackson.databind;
    requires lss.ds.luckyauction.magnani.pasolini.configuration.main;
    requires lss.ds.luckyauction.magnani.pasolini.users.adapter.main;
    requires lss.ds.luckyauction.magnani.pasolini.movements.adapter.main;
    requires lss.ds.luckyauction.magnani.pasolini.auctions.adapter.main;
    requires lss.ds.luckyauction.magnani.pasolini.automations.adapter.main;
    requires lss.ds.luckyauction.magnani.pasolini.utils.main;
    requires reactor.core;
    requires spring.core;
}