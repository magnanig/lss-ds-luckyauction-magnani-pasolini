FROM openjdk:16
ARG JAR_FILE
COPY $JAR_FILE app.jar
COPY wait-for-it.sh wait-for-it.sh
CMD ["java", "-jar", "app.jar"]