dependencies {
    implementation(project(":utils"))
    implementation(project(":automations:domain"))

    testImplementation(project(":uuid-generator"))
    testImplementation(project(":automations:mongo-repository"))

    // this will add following dependencies: test --> users.domain.testFixtures
    testImplementation(testFixtures(project(":auctions:domain")))
}