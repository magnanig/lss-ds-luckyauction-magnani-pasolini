/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.automation.usecase;

import it.unibo.luckyauction.auction.domain.AuctionTestUtils;
import it.unibo.luckyauction.automation.domain.entity.AuctionAutomation;
import it.unibo.luckyauction.automation.domain.entity.UserAuctionsAutomations;
import it.unibo.luckyauction.automation.repository.AuctionAutomationMongoRepository;
import it.unibo.luckyauction.automation.usecase.exception.AuctionAutomationAlreadyExistsException;
import it.unibo.luckyauction.automation.usecase.exception.AuctionAutomationNotFoundForUserException;
import it.unibo.luckyauction.automation.usecase.exception.UserAuctionsAutomationsAlreadyExistsException;
import it.unibo.luckyauction.automation.usecase.exception.UserAuctionsAutomationsNotFoundException;
import it.unibo.luckyauction.automation.usecase.port.AuctionAutomationRepository;
import it.unibo.luckyauction.util.NumericalConstants;
import it.unibo.luckyauction.uuid.UUIdGenerator;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestMethodOrder;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Collection;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static it.unibo.luckyauction.auction.domain.AuctionTestUtils.FIRST_USERNAME;
import static it.unibo.luckyauction.auction.domain.AuctionTestUtils.SECOND_USERNAME;
import static it.unibo.luckyauction.auction.domain.AuctionTestUtils.THIRD_USERNAME;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class AuctionAutomationTests {

    private static final AuctionAutomationRepository AUCTION_AUTOMATION_REPO =
            AuctionAutomationMongoRepository.testRepository();
    public static final BigDecimal FIRST_AMOUNT_VALUE = new BigDecimal("52.38");
    private static final String FIRST_AUCTION_ID = "111";
    private static final String SECOND_AUCTION_ID = "222";

    private final CreateUserAuctionsAutomations createUserAuctionsAutomations =
            new CreateUserAuctionsAutomations(AUCTION_AUTOMATION_REPO);
    private final DeleteUserAuctionsAutomations deleteUserAuctionsAutomations =
            new DeleteUserAuctionsAutomations(AUCTION_AUTOMATION_REPO);
    private final FindUserAuctionsAutomations findUserAuctionsAutomations =
            new FindUserAuctionsAutomations(AUCTION_AUTOMATION_REPO);
    private final AddAuctionAutomation addAutomation =
            new AddAuctionAutomation(AUCTION_AUTOMATION_REPO, new UUIdGenerator());
    private final SetEnableStatus setEnableStatus = new SetEnableStatus(AUCTION_AUTOMATION_REPO);
    private final FindAuctionAutomation findAutomations = new FindAuctionAutomation(AUCTION_AUTOMATION_REPO);
    private final RemoveAuctionAutomation removeAutomation = new RemoveAuctionAutomation(AUCTION_AUTOMATION_REPO);

    private AuctionAutomation firstIdAuctionAutomationForFirstUser;
    private AuctionAutomation firstIdAuctionAutomationForSecondUser;
    private AuctionAutomation secondIdAuctionAutomationForSecondUser;
    private Calendar afterTwoDays;
    private final Supplier<AuctionAutomation.Builder> preBuiltAutomation = () -> AuctionAutomation.builder()
            .limitPrice(FIRST_AMOUNT_VALUE).priceIncrease(BigDecimal.ONE)
            .raiseDelay(AuctionTestUtils.ONE_DAY_DURATION).automationEnd(afterTwoDays);

    @BeforeAll
    void createUsersAuctionsAutomations() {
        afterTwoDays = Calendar.getInstance();
        afterTwoDays.add(Calendar.DATE, NumericalConstants.TWO);
        createUserAuctionAutomation(FIRST_USERNAME);
        createUserAuctionAutomation(SECOND_USERNAME);
    }

    @AfterAll
    void deleteUsersAuctionsAutomations() {
        deleteUserAuctionsAutomations.deleteByUsername(FIRST_USERNAME);
        deleteUserAuctionsAutomations.deleteByUsername(SECOND_USERNAME);
    }

    @Test @Order(1)
    void userAutomationsAlreadyExistExceptionTest() {
        final Exception exception = assertThrows(UserAuctionsAutomationsAlreadyExistsException.class,
                () -> createUserAuctionsAutomations.create(new UserAuctionsAutomations(FIRST_USERNAME)));
        assertTrue(exception.getMessage().contains("automations for user " + FIRST_USERNAME + " already exists"));
    }

    @Test @Order(2)
    void addAuctionsAutomationsTest() {
        firstIdAuctionAutomationForFirstUser = addAutomation.addAutomation(
                preBuiltAutomation.get().auctionId(FIRST_AUCTION_ID).build(), FIRST_USERNAME);
        firstIdAuctionAutomationForSecondUser = addAutomation.addAutomation(
                preBuiltAutomation.get().auctionId(FIRST_AUCTION_ID).build(), SECOND_USERNAME);
        secondIdAuctionAutomationForSecondUser = addAutomation.addAutomation(
                preBuiltAutomation.get().auctionId(SECOND_AUCTION_ID).build(), SECOND_USERNAME);

        final Exception alreadyExistsException = assertThrows(AuctionAutomationAlreadyExistsException.class,
                () -> addAutomation.addAutomation(firstIdAuctionAutomationForFirstUser, FIRST_USERNAME));
        assertTrue(alreadyExistsException.getMessage().contains("Automation for auction " + FIRST_AUCTION_ID
                + " already exists"));

        final Exception notFoundException = assertThrows(UserAuctionsAutomationsNotFoundException.class,
                () -> addAutomation.addAutomation(firstIdAuctionAutomationForFirstUser, THIRD_USERNAME));
        assertTrue(notFoundException.getMessage().contains("automations for user " + THIRD_USERNAME
                + " not found"));
    }

    @Test @Order(3)
    void findAuctionsAutomationsTest() {
        final Set<AuctionAutomation> activeAutomationsFound = findAutomations.getAllActiveAutomations()
                .stream()
                .map(UserAuctionsAutomations::getAuctionsAutomations)
                .flatMap(Collection::stream)
                .collect(Collectors.toSet());

        final AuctionAutomation firstAutomationForFirstUser = findAutomations
                .findByAuctionId(FIRST_AUCTION_ID, FIRST_USERNAME).orElseThrow();
        final AuctionAutomation firstAutomationForSecondUser = findAutomations
                .findByAuctionId(FIRST_AUCTION_ID, SECOND_USERNAME).orElseThrow();
        final AuctionAutomation secondAutomationForSecondUser = findAutomations
                .findByAuctionId(SECOND_AUCTION_ID, SECOND_USERNAME).orElseThrow();

        assertEquals(Set.of(firstAutomationForFirstUser, firstAutomationForSecondUser, secondAutomationForSecondUser),
                activeAutomationsFound);

        assertEquals(Set.of(firstAutomationForFirstUser, firstAutomationForSecondUser),
                findAutomations.findByAuctionId(FIRST_AUCTION_ID));

        assertTrue(firstAutomationForFirstUser.isEnabled(), "Automation must be active by default");
        assertTrue(firstAutomationForSecondUser.isEnabled(), "Automation must be active by default");
        assertTrue(secondAutomationForSecondUser.isEnabled(), "Automation must be active by default");
        assertEquals(firstIdAuctionAutomationForFirstUser, firstAutomationForFirstUser);
        assertEquals(firstIdAuctionAutomationForSecondUser, firstAutomationForSecondUser);
        assertEquals(secondIdAuctionAutomationForSecondUser, secondAutomationForSecondUser);
    }

    @Test @Order(3)
    void setEnableStatusTest() {
        AuctionAutomation automation;
        setEnableStatus.disable(FIRST_AUCTION_ID, FIRST_USERNAME);
        automation = findAutomations.findByAuctionId(FIRST_AUCTION_ID, FIRST_USERNAME).orElseThrow();
        assertFalse(automation.isEnabled(), "Automation must be disabled");

        setEnableStatus.active(FIRST_AUCTION_ID, FIRST_USERNAME);
        automation = findAutomations.findByAuctionId(FIRST_AUCTION_ID, FIRST_USERNAME).orElseThrow();
        assertTrue(automation.isEnabled(), "Automation must be active");

        final Exception wrongAuctionIdException = assertThrows(AuctionAutomationNotFoundForUserException.class,
                () -> setEnableStatus.disable("wrongId", FIRST_USERNAME));
        assertTrue(wrongAuctionIdException.getMessage().contains("No automation found"));
    }

    @Test @Order(NumericalConstants.FIVE)
    void removeAutomationForAuctionTest() {
        removeAutomation.removeAutomationForAuction(SECOND_AUCTION_ID, SECOND_USERNAME);

        final UserAuctionsAutomations automations = findUserAuctionsAutomations
                .findByUsername(SECOND_USERNAME).orElseThrow();

        assertEquals(firstIdAuctionAutomationForSecondUser,
                automations.getAuctionsAutomations().stream().findFirst().orElseThrow());
    }

    @Test @Order(NumericalConstants.SIX)
    void removeAllAutomationsForAuctionTest() {
        removeAutomation.removeAllAutomationsForAuction(FIRST_AUCTION_ID);

        final UserAuctionsAutomations firstUserAutomations = findUserAuctionsAutomations
                .findByUsername(FIRST_USERNAME).orElseThrow();
        assertTrue(firstUserAutomations.getAuctionsAutomations().isEmpty(), "There must be no automation for"
                + " user FIRST_USERNAME");

        final UserAuctionsAutomations secondUserAutomations = findUserAuctionsAutomations
                .findByUsername(SECOND_USERNAME).orElseThrow();
        assertTrue(secondUserAutomations.getAuctionsAutomations().isEmpty(), "there must be no automation for"
                + " user SECOND_USERNAME");
    }

    private void createUserAuctionAutomation(final String username) {
        if (findUserAuctionsAutomations.findByUsername(username).isPresent()) {
            deleteUserAuctionsAutomations.deleteByUsername(username);
        }
        createUserAuctionsAutomations.create(new UserAuctionsAutomations(username));
    }
}
