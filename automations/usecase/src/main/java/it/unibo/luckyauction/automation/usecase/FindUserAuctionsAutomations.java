/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.automation.usecase;

import it.unibo.luckyauction.automation.domain.entity.UserAuctionsAutomations;
import it.unibo.luckyauction.automation.usecase.port.AuctionAutomationRepository;

import java.util.Optional;

public class FindUserAuctionsAutomations {

    private final AuctionAutomationRepository auctionAutomationRepository;

    /**
     * Create a new instance to use in order to search user auctions automations.
     */
    public FindUserAuctionsAutomations(final AuctionAutomationRepository auctionAutomationRepository) {
        this.auctionAutomationRepository = auctionAutomationRepository;
    }

    /**
     * Find a user auctions automations by its username.
     * @param username  the desired username
     * @return          the corresponding user auctions automations, if exists
     */
    public Optional<UserAuctionsAutomations> findByUsername(final String username) {
        return auctionAutomationRepository.findByUsername(username);
    }
}
