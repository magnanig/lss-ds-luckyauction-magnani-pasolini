/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.automation.usecase;

import it.unibo.luckyauction.automation.domain.entity.AuctionAutomation;
import it.unibo.luckyauction.automation.usecase.exception.AuctionAutomationAlreadyExistsException;
import it.unibo.luckyauction.automation.usecase.exception.UserAuctionsAutomationsNotFoundException;
import it.unibo.luckyauction.automation.usecase.port.AuctionAutomationRepository;
import it.unibo.luckyauction.util.port.IdGenerator;

public class AddAuctionAutomation {

    private final AuctionAutomationRepository auctionAutomationRepository;
    private final IdGenerator auctionAutomationIdGenerator;

    /**
     * Create a new instance to use in order to add auction automation.
     * @param auctionAutomationRepository   the auction automation repository
     * @param auctionAutomationIdGenerator  the generator of auction automation ids
     */
    public AddAuctionAutomation(final AuctionAutomationRepository auctionAutomationRepository,
                                final IdGenerator auctionAutomationIdGenerator) {
        this.auctionAutomationRepository = auctionAutomationRepository;
        this.auctionAutomationIdGenerator = auctionAutomationIdGenerator;
    }

    /**
     * Add an auction automation for the specified user.
     * @param auctionAutomation the auction automation to add
     * @param username          the username of the desired user
     * @throws UserAuctionsAutomationsNotFoundException if no user auctions automations with the
     *                                                  specified username exists
     * @throws AuctionAutomationAlreadyExistsException if current auction automation already exists
     */
    public AuctionAutomation addAutomation(final AuctionAutomation auctionAutomation, final String username) {
        if (auctionAutomationRepository.findByUsername(username).isEmpty()) {
            throw new UserAuctionsAutomationsNotFoundException(username);
        }
        if (auctionAutomationRepository.findAutomationForAuction(auctionAutomation.getAuctionId(), username)
                .isPresent()) {
            throw new AuctionAutomationAlreadyExistsException(auctionAutomation.getAuctionId());
        }
        final AuctionAutomation newAuctionAutomation = AuctionAutomation.builder()
                .auctionAutomationId(auctionAutomationIdGenerator.generate())
                .auctionId(auctionAutomation.getAuctionId())
                .limitPrice(auctionAutomation.getLimitPrice())
                .priceIncrease(auctionAutomation.getPriceIncrease())
                .raiseDelay(auctionAutomation.getRaiseDelay())
                .automationEnd(auctionAutomation.getAutomationEnd().orElse(null))
                .build();
        auctionAutomationRepository.addAuctionAutomation(newAuctionAutomation, username);
        return newAuctionAutomation;
    }
}
