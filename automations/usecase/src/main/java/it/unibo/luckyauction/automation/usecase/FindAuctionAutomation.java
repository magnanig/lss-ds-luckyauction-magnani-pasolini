/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.automation.usecase;

import it.unibo.luckyauction.automation.domain.entity.AuctionAutomation;
import it.unibo.luckyauction.automation.domain.entity.UserAuctionsAutomations;
import it.unibo.luckyauction.automation.usecase.port.AuctionAutomationRepository;

import java.util.Optional;
import java.util.Set;

public class FindAuctionAutomation {

    private final AuctionAutomationRepository auctionAutomationRepository;

    /**
     * Create a new instance to use in order to search an auction automation.
     * @param auctionAutomationRepository   the auction automation repository
     */
    public FindAuctionAutomation(final AuctionAutomationRepository auctionAutomationRepository) {
        this.auctionAutomationRepository = auctionAutomationRepository;
    }

    /**
     * Get all active user auction automations.
     * @return  the list of all active auction automations
     */
    public Set<UserAuctionsAutomations> getAllActiveAutomations() {
        return auctionAutomationRepository.getAllActiveAutomations();
    }

    /**
     * Find the auction automation by its auction id and the associated user.
     * @param auctionId the desired auction id
     * @param username  the username of the desired user
     * @return          the auction automation with specified auction id and user, if any
     */
    public Optional<AuctionAutomation> findByAuctionId(final String auctionId, final String username) {
        return auctionAutomationRepository.findAutomationForAuction(auctionId, username);
    }

    /**
     * Find the auctions automations for the given auction id.
     * @param auctionId the id of the desired auction
     * @return          the list of corresponding auctions automations
     */
    public Set<AuctionAutomation> findByAuctionId(final String auctionId) {
        return auctionAutomationRepository.findAutomationsForAuction(auctionId);
    }
}
