/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.automation.usecase.port;

import it.unibo.luckyauction.automation.domain.entity.AuctionAutomation;
import it.unibo.luckyauction.automation.domain.entity.UserAuctionsAutomations;

import java.util.Optional;
import java.util.Set;

public interface AuctionAutomationRepository {

    /**
     * Add a new user auctions automations object to the repository.
     * @param auctionAutomation the user auctions automations to add
     */
    void create(UserAuctionsAutomations auctionAutomation);

    /**
     * Delete a user auctions automations object from the repository by its username.
     * @param username  the desired username
     * @return          if the object has been deleted an optional containing the deleted
     *                  object is returned, otherwise an empty optional is returned
     */
    Optional<UserAuctionsAutomations> deleteByUsername(String username);

    /**
     * Find a user auctions automations by its username.
     * @param username  the desired username
     * @return          the corresponding user auctions automations, if any
     */
    Optional<UserAuctionsAutomations> findByUsername(String username);

    /**
     * Get all active user auctions automations.
     *
     * @return a list containing all active user auctions automations
     */
    Set<UserAuctionsAutomations> getAllActiveAutomations();

    /**
     * Add a new auction automation for the specified username.
     * @param auctionAutomation the auction automation to add
     * @param username          the username of the involved user
     */
    void addAuctionAutomation(AuctionAutomation auctionAutomation, String username);

    /**
     * Remove an auction automation for the specified username.
     * @param auctionId the id of the auction whose automation to remove
     * @param username  the username of the involved user
     * @return          the just deleted auction automation, if any
     */
    Optional<AuctionAutomation> removeAuctionAutomation(String auctionId, String username);

    /**
     * Remove all auction automation for the specified auction id.
     * @param auctionId the id of the auction from which to remove all its automations
     */
    void removeAllAutomationsForAuction(String auctionId);

    /**
     * Set the "enabled" field of a specific auction automation.
     * @param enabled   the status to be set
     * @param auctionId the id of the automated auction
     * @param username  the username to which the auction automation is associated
     */
    void setEnableStatus(boolean enabled, String auctionId, String username);

    /**
     * Find an auction automation by auction id and username.
     * @param auctionId the id of the desired auction
     * @param username  the desired username
     * @return          the corresponding auction automation, if any
     */
    Optional<AuctionAutomation> findAutomationForAuction(String auctionId, String username);

    /**
     * Find the auctions automations for the given auction id.
     * @param auctionId the id of the desired auction
     * @return          the list of corresponding auctions automations
     */
    Set<AuctionAutomation> findAutomationsForAuction(String auctionId);

}
