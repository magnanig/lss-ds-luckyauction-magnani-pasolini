/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.automation.usecase;

import it.unibo.luckyauction.automation.usecase.exception.AuctionAutomationNotFoundForUserException;
import it.unibo.luckyauction.automation.usecase.port.AuctionAutomationRepository;

public class SetEnableStatus {

    private final AuctionAutomationRepository auctionAutomationRepository;

    /**
     * Create a new instance to use in order to set new auction automation state.
     */
    public SetEnableStatus(final AuctionAutomationRepository auctionAutomationRepository) {
        this.auctionAutomationRepository = auctionAutomationRepository;
    }

    /**
     * Sets the auction automation "enabled" field to "true".
     * @param auctionId the id of the automated auction
     * @param username  the username to which the auction automation is associated
     */
    public void active(final String auctionId, final String username) {
        setEnabledStatus(true, auctionId, username);
    }

    /**
     * Sets the auction automation "enabled" field to "false".
     * @param auctionId the id of the automated auction
     * @param username  the username to which the auction automation is associated
     */
    public void disable(final String auctionId, final String username) {
        setEnabledStatus(false, auctionId, username);
    }

    private void setEnabledStatus(final boolean enabled, final String auctionId, final String username) {
        if (auctionAutomationRepository.findAutomationForAuction(auctionId, username).isEmpty()) {
            throw new AuctionAutomationNotFoundForUserException(auctionId, username);
        }
        auctionAutomationRepository.setEnableStatus(enabled, auctionId, username);
    }
}
