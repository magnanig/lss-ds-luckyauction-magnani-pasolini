/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.automation.usecase;

import it.unibo.luckyauction.automation.domain.entity.AuctionAutomation;
import it.unibo.luckyauction.automation.usecase.port.AuctionAutomationRepository;

import java.util.Optional;

public class RemoveAuctionAutomation {

    private final AuctionAutomationRepository auctionAutomationRepository;

    /**
     * Create a new instance to use in order to remove an auction automation.
     */
    public RemoveAuctionAutomation(final AuctionAutomationRepository auctionAutomationRepository) {
        this.auctionAutomationRepository = auctionAutomationRepository;
    }

    /**
     * Remove an auction automation for the specified username.
     * @param auctionId the id of the auction whose automation to remove
     * @param username  the username of the involved user
     * @return          the just deleted auction automation, if any
     */
    public Optional<AuctionAutomation> removeAutomationForAuction(final String auctionId, final String username) {
        return auctionAutomationRepository.removeAuctionAutomation(auctionId, username);
    }

    /**
     * Remove all auction automation for the specified auction id.
     * @param auctionId the id of the auction from which to remove all its automations
     */
    public void removeAllAutomationsForAuction(final String auctionId) {
        auctionAutomationRepository.removeAllAutomationsForAuction(auctionId);
    }
}
