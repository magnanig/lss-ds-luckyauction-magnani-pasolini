module lss.ds.luckyauction.magnani.pasolini.automations.usecase.main {
    exports it.unibo.luckyauction.automation.usecase;
    exports it.unibo.luckyauction.automation.usecase.port;
    exports it.unibo.luckyauction.automation.usecase.exception;
    requires lss.ds.luckyauction.magnani.pasolini.utils.main;
    requires lss.ds.luckyauction.magnani.pasolini.automations.domain.main;
}