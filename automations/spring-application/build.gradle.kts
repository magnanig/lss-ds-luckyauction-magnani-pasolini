plugins {
    id("org.springframework.boot") version "2.6.6"
}

apply(plugin = "org.springframework.boot")

rootProject.extra["mainClassProp"] = "it.unibo.luckyauction.automation.application.AuctionAutomationMain"

dependencies {
    implementation(project(":configuration"))
    implementation(project(":utils"))

    implementation(project(":automations:domain"))
    implementation(project(":automations:usecase"))
    implementation(project(":automations:adapter"))
    implementation(project(":automations:controller"))

    implementation(project(":socketio-server"))

    implementation("org.springframework.boot:spring-boot-starter-web:2.6.6")

    compileOnly("org.projectlombok:lombok:1.18.22")
    annotationProcessor("org.projectlombok:lombok:1.18.22")
}

// the version of jar task, for spring
tasks.bootJar {
    mainClass.set("it.unibo.luckyauction.automation.application.AuctionAutomationMain")
    archiveBaseName.set("luckyauction-automations-spring-application")
}
