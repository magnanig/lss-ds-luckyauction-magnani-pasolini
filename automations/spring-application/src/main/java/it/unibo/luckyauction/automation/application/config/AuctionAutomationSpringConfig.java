/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.automation.application.config;

import it.unibo.luckyauction.automation.adapter.socket.AuctionAutomationSocket;
import it.unibo.luckyauction.automation.controller.AuctionAutomationController;
import it.unibo.luckyauction.automation.controller.config.AuctionAutomationConfig;
import it.unibo.luckyauction.socket.server.AuctionAutomationSocketIO;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static it.unibo.luckyauction.configuration.SocketsConstants.AUCTION_AUTOMATIONS_PORT;
import static it.unibo.luckyauction.configuration.SocketsConstants.AUCTION_AUTOMATION_HOST;

@Configuration
public class AuctionAutomationSpringConfig {

    /**
     * Build the auction automation controller of the application, capable of
     * orchestrating auction automations.
     * See {@link AuctionAutomationController} for more details.
     * @return      the just built auction automation controller
     */
    @Bean
    public AuctionAutomationController auctionAutomationController(final AuctionAutomationSocket auctionAutomationSocket) {
        return new AuctionAutomationConfig().auctionAutomationController(auctionAutomationSocket);
    }

    @Bean
    public AuctionAutomationSocket auctionAutomationSocket() {
        return new AuctionAutomationSocketIO(AUCTION_AUTOMATION_HOST, AUCTION_AUTOMATIONS_PORT).start();
    }
}
