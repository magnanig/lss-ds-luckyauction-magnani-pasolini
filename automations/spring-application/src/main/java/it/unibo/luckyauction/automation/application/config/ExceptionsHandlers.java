/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.automation.application.config;

import it.unibo.luckyauction.automation.domain.exception.AuctionAutomationValidationException;
import it.unibo.luckyauction.automation.usecase.exception.AuctionAutomationAlreadyExistsException;
import it.unibo.luckyauction.automation.usecase.exception.AuctionAutomationNotFoundForUserException;
import it.unibo.luckyauction.automation.usecase.exception.UserAuctionsAutomationsAlreadyExistsException;
import it.unibo.luckyauction.automation.usecase.exception.UserAuctionsAutomationsNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * This class contains auction automations exception handlers, which specify
 * how the Spring server should respond to the client when an exception is thrown.
 */
@ControllerAdvice
public class ExceptionsHandlers {

    /**
     * Handler for {@link AuctionAutomationValidationException}.
     * @param ex    the thrown exception reference
     * @return      the response with status code 400 (bad request), containing the
     *              error message in the body
     */
    @ExceptionHandler(AuctionAutomationValidationException.class)
    public ResponseEntity<String> handleAuctionAutomationValidationException(
            final AuctionAutomationValidationException ex) {
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(ex.getMessage());
    }

    /**
     * Handler for {@link AuctionAutomationAlreadyExistsException}.
     * @param ex    the thrown exception reference
     * @return      the response with status code 400 (bad request), containing the
     *              error message in the body
     */
    @ExceptionHandler(AuctionAutomationAlreadyExistsException.class)
    public ResponseEntity<String> handleAuctionAutomationAlreadyExistsException(
            final AuctionAutomationAlreadyExistsException ex) {
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(ex.getMessage());
    }

    /**
     * Handler for {@link AuctionAutomationNotFoundForUserException}.
     * @param ex    the thrown exception reference
     * @return      the response with status code 400 (bad request), containing the
     *              error message in the body
     */
    @ExceptionHandler(AuctionAutomationNotFoundForUserException.class)
    public ResponseEntity<String> handleAuctionAutomationNotFoundForUserException(
            final AuctionAutomationNotFoundForUserException ex) {
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(ex.getMessage());
    }

    /**
     * Handler for {@link UserAuctionsAutomationsAlreadyExistsException}.
     * @param ex    the thrown exception reference
     * @return      the response with status code 400 (bad request), containing the
     *              error message in the body
     */
    @ExceptionHandler(UserAuctionsAutomationsAlreadyExistsException.class)
    public ResponseEntity<String> handleUserAuctionsAutomationsAlreadyExistsException(
            final UserAuctionsAutomationsAlreadyExistsException ex) {
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(ex.getMessage());
    }

    /**
     * Handler for {@link UserAuctionsAutomationsNotFoundException}.
     * @param ex    the thrown exception reference
     * @return      the response with status code 400 (bad request), containing the
     *              error message in the body
     */
    @ExceptionHandler(UserAuctionsAutomationsNotFoundException.class)
    public ResponseEntity<String> handleUserAuctionsAutomationsNotFoundException(
            final UserAuctionsAutomationsNotFoundException ex) {
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(ex.getMessage());
    }

    /**
     * Handler for a generic {@link Exception}. This handler will be triggered
     * only if none of the others matches with the thrown exception.
     * @param ex    the thrown exception reference
     * @return      the response with status code 500, containing the error
     *              message in the body
     */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<String> handleException(final Exception ex) {
        ex.printStackTrace();
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(ex.getMessage());
    }
}
