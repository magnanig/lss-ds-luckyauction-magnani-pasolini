/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.automation.application;

import it.unibo.luckyauction.automation.controller.AuctionAutomationController;
import it.unibo.luckyauction.automation.adapter.AuctionAutomationWeb;
import it.unibo.luckyauction.automation.adapter.UserAuctionsAutomationsWeb;
import it.unibo.luckyauction.automation.domain.entity.UserAuctionsAutomations;
import it.unibo.luckyauction.automation.usecase.exception.AuctionAutomationNotFoundForUserException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/auctions/automations")
@CrossOrigin(allowCredentials = "true", originPatterns = "http://localhost:[*]")
@SuppressWarnings("PMD.AvoidDuplicateLiterals") // to favor a better reading of the code
public class AuctionAutomationRestController {

    private final AuctionAutomationController auctionAutomationController;

    @Autowired
    public AuctionAutomationRestController(final AuctionAutomationController auctionAutomationController) {
        this.auctionAutomationController = auctionAutomationController;
    }

    /**
     * Create a new user auctions automations in the repository.
     */
    @PostMapping
    public void create(@RequestParam final String username) {
        auctionAutomationController.create(new UserAuctionsAutomations(username));
    }

    /**
     * Find a user auctions automations by its username.
     * @param username  the desired username
     * @return          the corresponding user auctions automations, if exists
     */
    @GetMapping
    public Optional<UserAuctionsAutomationsWeb> findByUsername(@RequestParam final String username) {
        return auctionAutomationController.findByUsername(username).map(UserAuctionsAutomationsWeb::fromDomain);
    }

    /**
     * Get all active user auction automations.
     * @return  the list of all active auction automations
     */
    @GetMapping("/active")
    public List<UserAuctionsAutomationsWeb> getAllActiveAutomations() {
        return auctionAutomationController.getAllActiveAutomations().stream()
                .map(UserAuctionsAutomationsWeb::fromDomain)
                .collect(java.util.stream.Collectors.toList());
    }

    /**
     * Add an auction automation for the specified user.
     * @param auctionAutomation the auction automation to add
     * @param username          the username of the desired user
     */
    @PutMapping
    public AuctionAutomationWeb addAuctionAutomation(@RequestBody final AuctionAutomationWeb auctionAutomation,
                                     @RequestParam final String username) {
        return AuctionAutomationWeb.fromDomain(auctionAutomationController
                .addAuctionAutomation(auctionAutomation.toDomain(), username));
    }

    /**
     * Delete a user auctions automations by the associated username.
     * @param username  the desired username
     * @return          the just deleted user auctions automations, if existed
     */
    @DeleteMapping
    public Optional<UserAuctionsAutomationsWeb> deleteByUsername(@RequestParam final String username) {
        return auctionAutomationController.deleteByUsername(username).map(UserAuctionsAutomationsWeb::fromDomain);
    }

    /**
     * Find the auction automation by its auction id and the associated user.
     * @param auctionId the desired auction id
     * @param username  the username of the desired user
     * @return          the auction automation with specified auction id and user, if any
     */
    @GetMapping("/{auctionId}")
    public Optional<AuctionAutomationWeb> findAutomationForAuction(@PathVariable final String auctionId,
                                                                   @RequestParam final String username) {
        return auctionAutomationController.findAutomationForAuction(auctionId, username)
                .map(AuctionAutomationWeb::fromDomain);
    }

    /**
     * Sets the auction automation "enabled" field.
     * @param auctionId the id of the automated auction
     * @param username  the username to which the auction automation is associated
     * @param active    the status to be set
     */
    @PutMapping("/{auctionId}")
    public void setEnableStatus(@PathVariable final String auctionId, @RequestParam final String username,
                                @RequestParam final boolean active) throws AuctionAutomationNotFoundForUserException {
        auctionAutomationController.setEnableStatus(active, auctionId, username);
    }

    /**
     * If the "username" parameter is specified, removes auction automation of the
     * specified id for the specified username. Otherwise, it removes all auction
     * automations with the specified id.
     * @param auctionId the id of the auction whose automation to remove
     * @param username  the username of the involved user
     * @apiNote the "username" parameter is optional
     */
    @DeleteMapping("/{auctionId}")
    public void removeAuctionAutomations(@PathVariable final String auctionId,
                                         @RequestParam(required = false) final String username) {
        if (username == null || username.isEmpty()) {
            auctionAutomationController.removeAllAutomationsForAuction(auctionId);
        } else {
            auctionAutomationController.removeAuctionAutomation(auctionId, username);
        }
    }
}
