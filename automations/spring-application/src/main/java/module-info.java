module lss.ds.luckyauction.magnani.pasolini.automations.spring.application.main {
    requires lss.ds.luckyauction.magnani.pasolini.configuration.main;
    requires lss.ds.luckyauction.magnani.pasolini.utils.main;
    requires lss.ds.luckyauction.magnani.pasolini.socketio.server.main;
    requires lss.ds.luckyauction.magnani.pasolini.automations.domain.main;
    requires lss.ds.luckyauction.magnani.pasolini.automations.usecase.main;
    requires lss.ds.luckyauction.magnani.pasolini.automations.adapter.main;
    requires lss.ds.luckyauction.magnani.pasolini.automations.controller.main;
    requires lombok;
    requires spring.web;
    requires spring.boot;
    requires spring.core;
    requires spring.beans;
    requires spring.context;
    requires spring.boot.autoconfigure;
    requires org.apache.tomcat.embed.core;
}