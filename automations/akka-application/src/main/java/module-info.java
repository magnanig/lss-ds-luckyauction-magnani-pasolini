module lss.ds.luckyauction.magnani.pasolini.automations.application.akka.main {
    requires lss.ds.luckyauction.magnani.pasolini.configuration.main;
    requires lss.ds.luckyauction.magnani.pasolini.automations.adapter.main;
    requires lss.ds.luckyauction.magnani.pasolini.automations.domain.main;
    requires lss.ds.luckyauction.magnani.pasolini.auctions.adapter.main;
    requires lss.ds.luckyauction.magnani.pasolini.auctions.domain.main;
    requires lss.ds.luckyauction.magnani.pasolini.utils.main;
    requires lss.ds.luckyauction.magnani.pasolini.utils.spring.main;
    requires lss.ds.luckyauction.magnani.pasolini.socketio.client.main;
    requires akka.actor.typed;
    requires java.logging;
    requires akka.actor;
}