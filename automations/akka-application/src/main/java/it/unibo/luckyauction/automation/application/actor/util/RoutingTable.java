/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.automation.application.actor.util;

import akka.actor.ActorRef;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

public class RoutingTable {

    @SuppressWarnings("PMD.AvoidFieldNameMatchingTypeName")
    private final Map<String, Map<String, ActorRef>> routingTable; // auctionId -> Map[automationId, actorRef]

    public RoutingTable() {
        this.routingTable = new Hashtable<>();
    }

    /**
     * Add a new route to the routing table: auctionId -> automationId -> userActorRef.
     * @param auctionId             the auction id associated to the automation
     * @param auctionAutomationId   the auction automation id to add
     * @param userActor             the user actor that manages the auction automation
     */
    public void addRoute(final String auctionId, final String auctionAutomationId, final ActorRef userActor) {
        final Map<String, ActorRef> currentRouteActors = routingTable.putIfAbsent(auctionId,
                new HashMap<>(Map.of(auctionAutomationId, userActor)));
        if (currentRouteActors != null) {
            currentRouteActors.put(auctionAutomationId, userActor);
        }
    }

    /**
     * Remove all routes for the given auction id.
     * @param auctionId the auction id whose routes must be removed
     */
    public void removeAllRoutesForAuction(final String auctionId) {
        routingTable.remove(auctionId);
    }

    /**
     * Remove the route for the given auction id and user actor.
     * @param auctionId the auction id whose route must be removed
     * @param userActor the user actor whose route must be removed
     */
    public void removeRoute(final String auctionId, final ActorRef userActor) {
        Optional.ofNullable(routingTable.get(auctionId)).ifPresent(automationsActors -> {
            if (automationsActors.size() == 1) {
                routingTable.remove(auctionId);
            } else {
                automationsActors.entrySet().removeIf(automationActor -> automationActor.getValue().equals(userActor));
            }
        });
    }

    /**
     * Remove a single route, for the given auction automation id.
     * @param auctionAutomationId   the auction automation id whose route must be removed
     */
    public void removeRouteForAuctionAutomation(final String auctionAutomationId) {
        routingTable.entrySet().stream()
                .filter(auctionsAutomations -> auctionsAutomations.getValue().containsKey(auctionAutomationId))
                .findFirst()
                .ifPresent(auctionsAutomations -> {
                    auctionsAutomations.getValue().remove(auctionAutomationId);
                    if (auctionsAutomations.getValue().isEmpty()) {
                        routingTable.remove(auctionsAutomations.getKey());
                    }
                });
    }

    /**
     * Get all users actors managing an automation for the given auction.
     * @param auctionId the id of the desired auction
     * @return          the set of users actors managing an automation for the given auction
     */
    public Set<ActorRef> getActorsForAuctionRoute(final String auctionId) {
        return new HashSet<>(Optional.ofNullable(routingTable.get(auctionId))
                .map(Map::values)
                .orElse(Collections.emptySet()));
    }

    /**
     * Get the user actor managing the given auction automation.
     * @param auctionAutomationId   the id of the desired auction automation
     * @return                      the user actor managing the given auction automation
     */
    public Optional<ActorRef> getActorForAuctionAutomation(final String auctionAutomationId) {
        return routingTable.values().stream()
                .map(Map::entrySet)
                .flatMap(Collection::stream)
                .filter(automationActor -> automationActor.getKey().equals(auctionAutomationId))
                .findFirst()
                .map(Map.Entry::getValue);
    }

    /**
     * Check whether the routing table is empty, i.e. no routes exist.
     * @return  true if the routing table is emptu; false otherwise
     */
    public boolean isEmpty() {
        return routingTable.isEmpty();
    }
}
