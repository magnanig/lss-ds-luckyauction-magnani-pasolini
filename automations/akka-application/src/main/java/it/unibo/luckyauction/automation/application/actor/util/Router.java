/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.automation.application.actor.util;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class Router {

    private final Set<Integer> socketsBucketsIds;
    private Integer messagesPerMinute;

    public Router() {
        this(new HashSet<>());
    }

    public Router(final Set<Integer> socketsBucketsIds) {
        this.socketsBucketsIds = new HashSet<>(socketsBucketsIds);
        this.messagesPerMinute = 0;
    }

    /**
     * Add the specified socket to list of sockets on which router is connected.
     * @param socketId  the socket id to add
     */
    public void addBidSocketId(final Integer socketId) {
        this.socketsBucketsIds.add(socketId);
    }

    /**
     * Remove the specified socket from list of sockets on which router is connected.
     * @param socketId  the socket id to remove
     */
    public void removeBidSocketId(final Integer socketId) {
        this.socketsBucketsIds.remove(socketId);
    }

    /**
     * Get the list of sockets buckets id on which router is connected.
     */
    public Set<Integer> getSocketsBucketsIds() {
        return Collections.unmodifiableSet(socketsBucketsIds);
    }

    /**
     * Get the number of messages per minute sent by router to other actors.
     */
    public Integer getMessagesPerMinute() {
        return messagesPerMinute;
    }

    /**
     * Set the number of messages per minute sent by router to other actors.
     */
    public void setMessagesPerMinute(final Integer messagesPerMinute) {
        this.messagesPerMinute = messagesPerMinute;
    }

    /**
     * Increment by 1 the number of messages per minute sent by router to other actors.
     */
    public void incrementMessagesPerMinute() {
        messagesPerMinute++;
    }

    /**
     * Reset (i.e. set to 0) the number of messages per minute sent by router to other actors.
     */
    public void resetMessagesPerMinute() {
        this.messagesPerMinute = 0;
    }

}
