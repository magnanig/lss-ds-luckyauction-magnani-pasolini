/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.automation.application.actor;

import akka.actor.AbstractActorWithTimers;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.japi.pf.ReceiveBuilder;
import it.unibo.luckyauction.automation.adapter.AuctionAutomationWeb;
import it.unibo.luckyauction.automation.adapter.api.AuctionAutomationApi;
import it.unibo.luckyauction.automation.adapter.socket.Token;
import it.unibo.luckyauction.automation.application.actor.message.CreateAutomation;
import it.unibo.luckyauction.automation.application.actor.message.RouterActorTerminated;
import it.unibo.luckyauction.automation.application.actor.message.TimeToCheckRouterDeletion;
import it.unibo.luckyauction.automation.application.actor.message.UpdateMessagesPerMinute;
import it.unibo.luckyauction.automation.application.actor.util.Router;
import it.unibo.luckyauction.automation.application.actor.util.RoutersManager;
import it.unibo.luckyauction.socket.client.AuctionAutomationSocketClient;
import it.unibo.luckyauction.spring.util.api.SpringAuctionAutomationApi;

import java.time.Duration;
import java.util.UUID;
import java.util.logging.Logger;

import static it.unibo.luckyauction.configuration.SocketsConstants.AUCTION_AUTOMATIONS_PORT;
import static it.unibo.luckyauction.configuration.SocketsConstants.AUCTION_AUTOMATION_HOST;

public class MasterActor extends AbstractActorWithTimers {

    private static final Duration ROUTER_DELETION_CHECK_INTERVAL = Duration.ofMinutes(30);

    private final RoutersManager<ActorRef> routersManager;
    private final AuctionAutomationSocketClient auctionAutomationSocket;
    private final AuctionAutomationApi auctionAutomationApi;

    private final Logger logger = Logger.getLogger(getClass().getName());

    public MasterActor() {
        super();
        routersManager = new RoutersManager<>();
        auctionAutomationApi = new SpringAuctionAutomationApi();
        auctionAutomationSocket = AuctionAutomationSocketClient.builder()
                .host(AUCTION_AUTOMATION_HOST)
                .port(AUCTION_AUTOMATIONS_PORT)
                .onSocketConnected(this::onSocketConnected)
                .onAutomationCreation(this::onAuctionAutomationCreation)
                .build();
        auctionAutomationSocket.connect();
        logger.info("Started master actor");
    }

    @Override
    public void preStart() {
        timers().startTimerWithFixedDelay(UUID.randomUUID(), new TimeToCheckRouterDeletion(),
                ROUTER_DELETION_CHECK_INTERVAL);
    }

    @Override
    public void postStop() {
        auctionAutomationSocket.disconnect();
    }

    @Override
    public Receive createReceive() {
        return new ReceiveBuilder()
                .match(UpdateMessagesPerMinute.class, update -> routersManager.findRouterByActor(sender())
                        .ifPresent(router -> router.setMessagesPerMinute(update.newValue())))
                .match(TimeToCheckRouterDeletion.class, () -> sender().equals(self()), timeout ->
                        routersManager.checkForRouterDeletion())
                .match(RouterActorTerminated.class, msg -> routersManager.routerTerminated(sender()))
                .build();
    }

    private void onAuctionAutomationCreation(final AuctionAutomationWeb auctionAutomation, final Token token) {
        onAuctionAutomationCreation(auctionAutomation, token, true);
    }

    private void onAuctionAutomationCreation(final AuctionAutomationWeb auctionAutomation, final Token token,
                                             final boolean raiseUpImmediately) {
        logger.info("Create automation for user " + token.getTokenValue());
        routersManager.selectFreestRouterOrReEnableDeletingRouter().orElseGet(() -> {
            logger.info("Going to create a new Router actor");
            final Router newRouter = new Router();
            final ActorRef newRouterActor = getContext().actorOf(Props.create(RouterActor.class, newRouter));
            routersManager.addRouter(newRouterActor, newRouter);
            return newRouterActor;
        }).tell(new CreateAutomation(auctionAutomation.toDomain(), token, raiseUpImmediately), self());
    }

    private void onSocketConnected() {
        auctionAutomationApi.getAllActiveAutomations().forEach(userAuctionsAutomations ->
                userAuctionsAutomations.getAuctionsAutomations().forEach(auctionAutomation ->
                        onAuctionAutomationCreation(auctionAutomation,
                                new Token(userAuctionsAutomations.getUsername()), false)));
    }

}
