/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.automation.application.actor;

import akka.actor.AbstractActorWithTimers;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.japi.pf.ReceiveBuilder;
import it.unibo.luckyauction.auction.adapter.BidWeb;
import it.unibo.luckyauction.automation.application.actor.message.CreateAutomation;
import it.unibo.luckyauction.automation.application.actor.message.RaisedNewBid;
import it.unibo.luckyauction.automation.application.actor.message.RouterActorTerminated;
import it.unibo.luckyauction.automation.application.actor.message.TimeToResetMessagesPerMinute;
import it.unibo.luckyauction.automation.application.actor.message.UpdateMessagesPerMinute;
import it.unibo.luckyauction.automation.application.actor.message.UserActorTerminated;
import it.unibo.luckyauction.automation.application.actor.util.Router;
import it.unibo.luckyauction.automation.application.actor.util.RoutingTable;
import it.unibo.luckyauction.socket.client.SocketsManager;

import java.time.Duration;
import java.util.UUID;
import java.util.logging.Logger;

import static it.unibo.luckyauction.configuration.SocketsConstants.AUCTION_AUTOMATIONS_PORT;
import static it.unibo.luckyauction.configuration.SocketsConstants.AUCTION_AUTOMATION_HOST;
import static it.unibo.luckyauction.configuration.SocketsConstants.BIDS_HOST;
import static it.unibo.luckyauction.configuration.SocketsConstants.BIDS_PORT;

public class RouterActor extends AbstractActorWithTimers {
    private ActorRef master;
    private final Router router;
    private final RoutingTable routingTable;
    private final SocketsManager socketsManager;
    private final Logger logger = Logger.getLogger(getClass().getName());

    public RouterActor(final Router router) {
        super();
        this.router = router;
        this.routingTable = new RoutingTable();
        this.socketsManager = new SocketsManager.Builder()
                .socketsBucketsIds(router.getSocketsBucketsIds())
                .bidsHost(BIDS_HOST)
                .bidsPort(BIDS_PORT)
                .auctionAutomationHost(AUCTION_AUTOMATION_HOST)
                .auctionAutomationPort(AUCTION_AUTOMATIONS_PORT)
                .onAuctionAutomationStop(this::onAuctionAutomationStop)
                .onAllAutomationsForAuctionStop(this::onAllAutomationsForAuctionStop)
                .onBidRaised(this::onBidRaised)
                .build();
        logger.info("Started router actor");
    }

    @Override
    public void preStart() {
        master = getContext().getParent();
        socketsManager.connect();
        timers().startTimerWithFixedDelay(UUID.randomUUID(), new TimeToResetMessagesPerMinute(), Duration.ofMinutes(1));
    }

    @Override
    public void postStop() {
        socketsManager.disconnect();
    }

    @Override
    public Receive createReceive() {
        return new ReceiveBuilder()
                .match(TimeToResetMessagesPerMinute.class, timeout -> {
                    master.tell(new UpdateMessagesPerMinute(router.getMessagesPerMinute()), self());
                    router.resetMessagesPerMinute();
                })
                .match(CreateAutomation.class, createAutomation -> {
                    final ActorRef newActor = getContext().actorOf(Props.create(UserActor.class,
                            createAutomation.userToken(), createAutomation.auctionAutomation(),
                            createAutomation.raiseUpImmediately()));
                    routingTable.addRoute(createAutomation.auctionAutomation().getAuctionId(),
                            createAutomation.auctionAutomation().getAuctionAutomationId(), newActor);
                })
                .match(UserActorTerminated.class, userActorTerminated -> {
                        routingTable.removeRoute(userActorTerminated.auctionId(), sender());
                        checkForRouterTermination();
                })
                .build();
    }

    private void onBidRaised(final BidWeb raisedBid, final String auctionId) {
        routingTable.getActorsForAuctionRoute(auctionId).forEach(actor -> {
            actor.tell(new RaisedNewBid(raisedBid.toDomainBid()), self());
            router.incrementMessagesPerMinute();
        });
    }

    private void onAuctionAutomationStop(final String auctionAutomationId) {
        logger.info("Going to stop auction automation #" + auctionAutomationId);
        routingTable.getActorForAuctionAutomation(auctionAutomationId).ifPresent(userActor -> {
            logger.info("Stopping user actor " + userActor);
            getContext().stop(userActor);
            routingTable.removeRouteForAuctionAutomation(auctionAutomationId);
            checkForRouterTermination();
        });
    }

    private void onAllAutomationsForAuctionStop(final String auctionId) {
        routingTable.getActorsForAuctionRoute(auctionId).forEach(actor -> {
            getContext().stop(actor);
            routingTable.removeAllRoutesForAuction(auctionId);
            checkForRouterTermination();
        });
    }

    private void checkForRouterTermination() {
        if (routingTable.isEmpty()) {
            getContext().stop(self());
            master.tell(new RouterActorTerminated(), self());
        }
    }
}
