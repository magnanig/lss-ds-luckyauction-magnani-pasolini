/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.automation.application.actor.util;

import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.logging.Logger;

public class RoutersManager<T> {

    private static final Integer MAX_MPM_FREQ = 100;
    private static final Integer AVG_MPM_DELETION_THRESHOLD = MAX_MPM_FREQ / 2;

    private final Map<T, Router> routers;
    private final Map<T, Router> deletingRouters;

    private final Logger logger = Logger.getLogger(getClass().getName());

    public RoutersManager() {
        this.routers = new HashMap<>();
        this.deletingRouters = new HashMap<>();
    }

    /**
     * Add a new router actor to the manager.
     * @param routerActor   the router actor to add
     * @param router        the router associated to the router actor
     */
    public void addRouter(final T routerActor, final Router router) {
        routers.put(routerActor, router);
    }

    /**
     * Remove the specified router from either active routers or deleting routers list.
     * @param routerActor   the router to be removed
     */
    public void routerTerminated(final T routerActor) {
        routers.remove(routerActor);
        deletingRouters.remove(routerActor);
    }

    /**
     * Check if a router can be deleted (i.e. it is receiving a few messages),
     * and in such case, move it into the deleting set.
     */
    public void checkForRouterDeletion() {
        if (routerCanBeDeleted()) {
            logger.info("Going to delete a router");
            final T deletingRouter = selectFreestRouter().orElseThrow();
            deletingRouters.put(deletingRouter, routers.remove(deletingRouter));
        }
    }

    /**
     * Check if a router can be deleted (i.e. it is receiving a few messages).
     * @return  true if the router can be deleted, false otherwise
     */
    public boolean routerCanBeDeleted() {
        return routers.size() > 1 && routers.values().stream()
                .mapToInt(Router::getMessagesPerMinute)
                .average()
                .orElse(Double.MAX_VALUE) <= AVG_MPM_DELETION_THRESHOLD;
    }

    /**
     * Find the router associated to the given router actor.
     * @param routerActor   the router actor
     * @return              the router associated to such router actor
     */
    public Optional<Router> findRouterByActor(final T routerActor) {
        return Optional.ofNullable(routers.get(routerActor))
                .or(() -> Optional.ofNullable(deletingRouters.get(routerActor)));
    }

    /**
     * Select the freest router (i.e. the one with the lowest messages per minute)
     * from the list of active routers that have a messages per minute under the
     * threshold.
     * @return  the freest router, if any match the criteria
     */
    public Optional<T> selectFreestRouter() {
        return selectFreestRouter(routers.entrySet())
                .map(Map.Entry::getKey);
    }

    /**
     * Select the freest router (i.e. the one with the lowest messages per minute)
     * with messages per minute under the threshold, if any, otherwise re-enable
     * a router that was previously added to the deleting set (if its messages per
     * minute is under the threshold).
     * @return  the freest router, if any match the criteria
     */
    public Optional<T> selectFreestRouterOrReEnableDeletingRouter() {
        return selectFreestRouter(routers.entrySet())
                .or(this::reEnableDeletingRouter)
                .map(Map.Entry::getKey);
    }

    private Optional<Map.Entry<T, Router>> selectFreestRouter(final Collection<Map.Entry<T, Router>> routers) {
        return routers.stream()
                .filter(entry -> entry.getValue().getMessagesPerMinute() < MAX_MPM_FREQ)
                .min(Comparator.comparing(e -> e.getValue().getMessagesPerMinute()));
    }

    private Optional<Map.Entry<T, Router>> reEnableDeletingRouter() {
        return selectFreestRouter(deletingRouters.entrySet())
                .stream()
                .peek(entry -> {
                    deletingRouters.remove(entry.getKey());
                    routers.put(entry.getKey(), entry.getValue());
                })
                .findFirst();
    }
}
