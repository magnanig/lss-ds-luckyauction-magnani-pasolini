/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.automation.application.actor;


import akka.actor.AbstractActorWithTimers;
import akka.actor.ReceiveTimeout;
import akka.japi.pf.ReceiveBuilder;
import it.unibo.luckyauction.auction.adapter.BidWeb;
import it.unibo.luckyauction.auction.adapter.api.AuctionApi;
import it.unibo.luckyauction.auction.domain.valueobject.Bid;
import it.unibo.luckyauction.automation.adapter.api.AuctionAutomationApi;
import it.unibo.luckyauction.automation.adapter.socket.Token;
import it.unibo.luckyauction.automation.application.actor.message.AutomationEnded;
import it.unibo.luckyauction.automation.application.actor.message.RaisedNewBid;
import it.unibo.luckyauction.automation.application.actor.message.TimeToRaiseUp;
import it.unibo.luckyauction.automation.application.actor.message.UserActorTerminated;
import it.unibo.luckyauction.automation.domain.entity.AuctionAutomation;
import it.unibo.luckyauction.spring.util.api.SpringAuctionApi;
import it.unibo.luckyauction.spring.util.api.SpringAuctionAutomationApi;
import it.unibo.luckyauction.util.CalendarUtils;
import scala.concurrent.duration.Duration;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.UUID;
import java.util.logging.Logger;

public class UserActor extends AbstractActorWithTimers {

    private final Token token;
    private final AuctionAutomation auctionAutomation;
    private final BigDecimal auctionStartingPrice;

    private final AuctionApi auctionApi;
    private final AuctionAutomationApi auctionAutomationApi;

    private final Logger logger = Logger.getLogger(UserActor.class.getName());

    public UserActor(final Token userToken, final AuctionAutomation auctionAutomation,
                     final boolean tryRaiseUpImmediately) {
        super();
        this.token = userToken;
        this.auctionAutomation = auctionAutomation;
        this.auctionApi = new SpringAuctionApi();
        this.auctionAutomationApi = new SpringAuctionAutomationApi();
        this.auctionStartingPrice = auctionApi.getStartingPrice(auctionAutomation.getAuctionId());
        raiseUpImmediatelyOrSetReceiveTimeout(tryRaiseUpImmediately);
    }

    @Override
    public void preStart() {
        logger.info("Started user actor for " + token.getTokenValue() + " with automation " + auctionAutomation);
        auctionAutomation.getAutomationEnd().ifPresent(endTime -> timers().startSingleTimer(UUID.randomUUID(),
                new AutomationEnded(), CalendarUtils.diff(endTime, Calendar.getInstance())));
        tryRaiseUp();
    }

    @Override
    public Receive createReceive() {
        return new ReceiveBuilder()
                .match(RaisedNewBid.class, this::onBidRaised)
                .match(TimeToRaiseUp.class, timeout -> tryRaiseUp())
                .match(ReceiveTimeout.class, timeout -> {
                    getContext().setReceiveTimeout(Duration.Undefined()); // disable timeout
                    tryRaiseUp();
                })
                .match(AutomationEnded.class, automationEnded -> terminate())
                .build();
    }

    private void raiseUpImmediatelyOrSetReceiveTimeout(final boolean raiseUpImmediately) {
        if (raiseUpImmediately) {
            tryRaiseUp();
        } else {
            auctionApi.getActualBid(auctionAutomation.getAuctionId()).ifPresentOrElse(
                    actualBid -> getContext().setReceiveTimeout(auctionAutomation.getRaiseDelay()
                            .minus(CalendarUtils.diff(Calendar.getInstance(), actualBid.toDomainBid().getTimestamp()))),
                    this::tryRaiseUp);
        }
    }

    private void onBidRaised(final RaisedNewBid raisedNewBid) {
        if (!isCurrentUserBid(raisedNewBid.newBid())) {
            logger.info("--------- Received bid from another user ---------");
            if (canRaiseUp(raisedNewBid.newBid().getAmount())) {
                timers().startSingleTimer(UUID.randomUUID(), new TimeToRaiseUp(), auctionAutomation.getRaiseDelay());
            } else {
                terminate();
            }
        }
    }

    private BigDecimal nextBidAmount(final BigDecimal actualBidAmount) {
        return actualBidAmount.add(auctionAutomation.getPriceIncrease());
    }

    private boolean canRaiseUp(final BigDecimal actualBidAmount) {
        return nextBidAmount(actualBidAmount).compareTo(auctionAutomation.getLimitPrice()) <= 0;
    }

    private boolean isCurrentUserBid(final Bid bid) {
        return bid.getBidderUsername().equals(token.getTokenValue());
    }

    private void tryRaiseUp() {
        auctionApi.getActualBid(auctionAutomation.getAuctionId()).map(BidWeb::toDomainBid).ifPresentOrElse(
                actualBid -> {
                    if (!isCurrentUserBid(actualBid)) {
                        raiseUpOrTerminate(actualBid.getAmount());
                    }
                },
                () -> raiseUpOrTerminate(auctionStartingPrice));
    }

    private void raiseUpOrTerminate(final BigDecimal actualBidAmount) {
        if (canRaiseUp(actualBidAmount)) {
            final BigDecimal nextBidAmount = nextBidAmount(actualBidAmount);
            logger.info("Raising up for " + token.getTokenValue() + ". Bid amount: " + nextBidAmount);
            try {
                auctionApi.addNewBid(auctionAutomation.getAuctionId(), token.getTokenValue(), nextBidAmount);
            } catch (final Exception exception) {
                logger.warning("Failed to raise up for " + token.getTokenValue() + " of value "
                        + nextBidAmount(actualBidAmount) + ": " + exception.getMessage());
                terminate();
            }
            if (nextBidAmount.compareTo(auctionAutomation.getLimitPrice()) >= 0) {
                logger.info("Reached limit price: automation can terminate");
                terminate();
            }
        } else {
            terminate();
        }
    }

    private void terminate() {
        timers().cancelAll();
        getContext().getParent().tell(new UserActorTerminated(auctionAutomation.getAuctionId()), self());
        auctionAutomationApi.removeAuctionAutomation(auctionAutomation.getAuctionId(), token.getTokenValue());
        logger.info("Terminating user actor for " + token.getTokenValue() + " with automation " + auctionAutomation);
        getContext().stop(self());
    }
}
