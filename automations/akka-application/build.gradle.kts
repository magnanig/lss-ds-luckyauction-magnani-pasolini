val akkaScalaVersion = "2.13"
rootProject.extra["mainClassProp"] = "it.unibo.luckyauction.automation.application.actor.AkkaMain"

plugins {
    id("com.github.johnrengelman.shadow") version "7.0.0"
}

dependencies {
    implementation(project(":configuration"))
    implementation(project(":utils"))
    implementation(project(":utils:spring-utils"))
    implementation(project(":socketio-client"))

    implementation(project(":automations:adapter"))
    implementation(project(":automations:domain"))

    implementation(project(":auctions:adapter"))
    implementation(project(":auctions:domain"))

    implementation("com.typesafe.akka:akka-actor-typed_$akkaScalaVersion:2.6.18")
}

application {
    mainClass.set("it.unibo.luckyauction.automation.application.actor.AkkaMain")
}