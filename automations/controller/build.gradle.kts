dependencies {
    implementation(project(":configuration"))
    implementation(project(":utils"))
    implementation(project(":uuid-generator"))

    implementation(project(":automations:domain"))
    implementation(project(":automations:usecase"))
    implementation(project(":automations:adapter"))
    implementation(project(":automations:mongo-repository"))
}