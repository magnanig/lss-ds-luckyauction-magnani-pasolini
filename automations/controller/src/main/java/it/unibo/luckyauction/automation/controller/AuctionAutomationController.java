/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.automation.controller;

import it.unibo.luckyauction.automation.adapter.socket.AuctionAutomationSocket;
import it.unibo.luckyauction.automation.adapter.socket.CreateAuctionAutomation;
import it.unibo.luckyauction.automation.adapter.socket.StopAllAuctionAutomations;
import it.unibo.luckyauction.automation.adapter.socket.StopAuctionAutomation;
import it.unibo.luckyauction.automation.adapter.socket.Token;
import it.unibo.luckyauction.automation.domain.entity.AuctionAutomation;
import it.unibo.luckyauction.automation.domain.entity.UserAuctionsAutomations;
import it.unibo.luckyauction.automation.usecase.AddAuctionAutomation;
import it.unibo.luckyauction.automation.usecase.CreateUserAuctionsAutomations;
import it.unibo.luckyauction.automation.usecase.DeleteUserAuctionsAutomations;
import it.unibo.luckyauction.automation.usecase.FindAuctionAutomation;
import it.unibo.luckyauction.automation.usecase.FindUserAuctionsAutomations;
import it.unibo.luckyauction.automation.usecase.RemoveAuctionAutomation;
import it.unibo.luckyauction.automation.usecase.SetEnableStatus;
import it.unibo.luckyauction.automation.usecase.exception.AuctionAutomationNotFoundForUserException;

import java.util.Optional;
import java.util.Set;

@SuppressWarnings("PMD.AvoidFieldNameMatchingMethodName")
public class AuctionAutomationController {

    private final CreateUserAuctionsAutomations createUserAuctionsAutomations;
    private final DeleteUserAuctionsAutomations deleteUserAuctionsAutomations;
    private final FindUserAuctionsAutomations findUserAuctionsAutomations;
    private final AddAuctionAutomation addAuctionAutomation;
    private final RemoveAuctionAutomation removeAuctionAutomation;
    private final FindAuctionAutomation findAuctionAutomation;
    private final SetEnableStatus setEnableStatus;

    private final AuctionAutomationSocket auctionAutomationSocket;

    public AuctionAutomationController(final CreateUserAuctionsAutomations createUserAuctionsAutomations,
                                       final DeleteUserAuctionsAutomations deleteUserAuctionsAutomations,
                                       final FindUserAuctionsAutomations findUserAuctionsAutomations,
                                       final AddAuctionAutomation addAuctionAutomation,
                                       final RemoveAuctionAutomation removeAuctionAutomation,
                                       final FindAuctionAutomation findAuctionAutomation,
                                       final SetEnableStatus setEnableStatus,
                                       final AuctionAutomationSocket auctionAutomationSocket) {
        this.createUserAuctionsAutomations = createUserAuctionsAutomations;
        this.deleteUserAuctionsAutomations = deleteUserAuctionsAutomations;
        this.findUserAuctionsAutomations = findUserAuctionsAutomations;
        this.addAuctionAutomation = addAuctionAutomation;
        this.removeAuctionAutomation = removeAuctionAutomation;
        this.findAuctionAutomation = findAuctionAutomation;
        this.setEnableStatus = setEnableStatus;
        this.auctionAutomationSocket = auctionAutomationSocket;
        createAuctionAutomationsNamespaces();
    }

    /**
     * Create a new user auctions automations.
     * @param userAuctionsAutomations   the new user auctions automations object to create
     */
    public void create(final UserAuctionsAutomations userAuctionsAutomations) {
        createUserAuctionsAutomations.create(userAuctionsAutomations);
    }

    /**
     * Delete a user auctions automations by the associated username.
     * @param username  the desired username
     * @return          the just deleted user auctions automations, if existed
     */
    public Optional<UserAuctionsAutomations> deleteByUsername(final String username) {
        final Optional<UserAuctionsAutomations> deleted = deleteUserAuctionsAutomations.deleteByUsername(username);
        deleted.ifPresent(userAuctionsAutomations -> userAuctionsAutomations.getAuctionsAutomations()
                .forEach(this::stopAuctionAutomationSocket)
        );
        return deleted;
    }

    /**
     * Find a user auctions automations by its username.
     * @param username  the desired username
     * @return          the corresponding user auctions automations, if exists
     */
    public Optional<UserAuctionsAutomations> findByUsername(final String username) {
        return findUserAuctionsAutomations.findByUsername(username);
    }

    /**
     * Get all active automations for each user.
     * @return  the list of all active auction automations
     */
    public Set<UserAuctionsAutomations> getAllActiveAutomations() {
        return findAuctionAutomation.getAllActiveAutomations();
    }

    /**
     * Add an auction automation for the specified user.
     * @param auctionAutomation the auction automation to add
     * @param username          the username of the desired user
     */
    public AuctionAutomation addAuctionAutomation(final AuctionAutomation auctionAutomation, final String username) {
        final AuctionAutomation newAuctionAutomation = addAuctionAutomation.addAutomation(auctionAutomation, username);
        auctionAutomationSocket.addAuctionAutomationNamespace(newAuctionAutomation.getAuctionAutomationId());
        auctionAutomationSocket.send(new CreateAuctionAutomation(newAuctionAutomation, new Token(username)));
        return newAuctionAutomation;
    }

    /**
     * Remove an auction automation for the specified username.
     * @param auctionId the id of the auction whose automation to remove
     * @param username  the username of the involved user
     */
    public void removeAuctionAutomation(final String auctionId, final String username) {
        removeAuctionAutomation.removeAutomationForAuction(auctionId, username)
                .ifPresent(this::stopAuctionAutomationSocket);
    }

    /**
     * Remove all auction automation for the specified auction id.
     * @param auctionId the id of the auction from which to remove all its automations
     */
    public void removeAllAutomationsForAuction(final String auctionId) {
        findAuctionAutomation.findByAuctionId(auctionId).forEach(this::stopAuctionAutomationSocket);
        auctionAutomationSocket.send(new StopAllAuctionAutomations(auctionId));
        removeAuctionAutomation.removeAllAutomationsForAuction(auctionId);
    }

    /**
     * Sets the auction automation "enabled" field.
     * @param auctionId the id of the automated auction
     * @param username  the username to which the auction automation is associated
     * @param enabled   the status that will be set
     */
    public void setEnableStatus(final boolean enabled, final String auctionId, final String username)
            throws AuctionAutomationNotFoundForUserException {
        final AuctionAutomation auctionAutomation = findAuctionAutomation.findByAuctionId(auctionId, username)
                .orElseThrow(() -> new AuctionAutomationNotFoundForUserException(auctionId, username));
        if (enabled) {
            setEnableStatus.active(auctionId, username);
            auctionAutomationSocket.send(new CreateAuctionAutomation(auctionAutomation, new Token(username)));
        } else {
            setEnableStatus.disable(auctionId, username);
            auctionAutomationSocket.send(new StopAuctionAutomation(auctionAutomation.getAuctionAutomationId()),
                    auctionAutomation.getAuctionAutomationId());
        }
    }

    /**
     * Find the auction automation by its auction id and the associated user.
     * @param auctionId the desired auction id
     * @param username  the username of the desired user
     * @return          the auction automation with specified auction id and user, if any
     */
    public Optional<AuctionAutomation> findAutomationForAuction(final String auctionId, final String username) {
        return findAuctionAutomation.findByAuctionId(auctionId, username);
    }

    private void createAuctionAutomationsNamespaces() {
        getAllActiveAutomations().stream()
                .flatMap(userAuctionsAutomations -> userAuctionsAutomations.getAuctionsAutomations().stream())
                .forEach(automation ->
                        auctionAutomationSocket.addAuctionAutomationNamespace(automation.getAuctionAutomationId()));
    }

    private void stopAuctionAutomationSocket(final AuctionAutomation auctionAutomation) {
        auctionAutomationSocket.send(new StopAuctionAutomation(auctionAutomation.getAuctionAutomationId()),
                auctionAutomation.getAuctionAutomationId());
        auctionAutomationSocket.removeAuctionAutomationNamespace(auctionAutomation.getAuctionAutomationId());
    }
}
