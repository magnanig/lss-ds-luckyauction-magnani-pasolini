/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.automation.controller.config;

import it.unibo.luckyauction.automation.adapter.socket.AuctionAutomationSocket;
import it.unibo.luckyauction.automation.controller.AuctionAutomationController;
import it.unibo.luckyauction.automation.repository.AuctionAutomationMongoRepository;
import it.unibo.luckyauction.automation.usecase.AddAuctionAutomation;
import it.unibo.luckyauction.automation.usecase.CreateUserAuctionsAutomations;
import it.unibo.luckyauction.automation.usecase.DeleteUserAuctionsAutomations;
import it.unibo.luckyauction.automation.usecase.FindAuctionAutomation;
import it.unibo.luckyauction.automation.usecase.FindUserAuctionsAutomations;
import it.unibo.luckyauction.automation.usecase.RemoveAuctionAutomation;
import it.unibo.luckyauction.automation.usecase.SetEnableStatus;
import it.unibo.luckyauction.automation.usecase.port.AuctionAutomationRepository;
import it.unibo.luckyauction.util.port.IdGenerator;
import it.unibo.luckyauction.uuid.UUIdGenerator;

import static it.unibo.luckyauction.configuration.ConfigurationConstants.TEST_MODE;

public class AuctionAutomationConfig {

    private final AuctionAutomationRepository auctionAutomationRepository;
    private final IdGenerator auctionAutomationIdGenerator = new UUIdGenerator();

    public AuctionAutomationConfig() {
        this.auctionAutomationRepository = TEST_MODE
                ? AuctionAutomationMongoRepository.testRepository()
                : AuctionAutomationMongoRepository.defaultRepository();
    }

    /**
     * Build the auction automation controller of the application, capable of orchestrating auctions automations.
     * See {@link AuctionAutomationController} for more details.
     * @return      the just built auction automation controller
     */
    public AuctionAutomationController auctionAutomationController(final AuctionAutomationSocket auctionAutomationSocket) {
        return new AuctionAutomationController(createUserAuctionsAutomations(), deleteUserAuctionsAutomations(),
                findUserAuctionsAutomations(), addAuctionAutomation(), removeAuctionAutomation(),
                findAuctionAutomation(), setEnableStatus(), auctionAutomationSocket);
    }

    private CreateUserAuctionsAutomations createUserAuctionsAutomations() {
        return new CreateUserAuctionsAutomations(auctionAutomationRepository);
    }

    private DeleteUserAuctionsAutomations deleteUserAuctionsAutomations() {
        return new DeleteUserAuctionsAutomations(auctionAutomationRepository);
    }

    private FindUserAuctionsAutomations findUserAuctionsAutomations() {
        return new FindUserAuctionsAutomations(auctionAutomationRepository);
    }

    private AddAuctionAutomation addAuctionAutomation() {
        return new AddAuctionAutomation(auctionAutomationRepository, auctionAutomationIdGenerator);
    }

    private RemoveAuctionAutomation removeAuctionAutomation() {
        return new RemoveAuctionAutomation(auctionAutomationRepository);
    }

    private FindAuctionAutomation findAuctionAutomation() {
        return new FindAuctionAutomation(auctionAutomationRepository);
    }

    @SuppressWarnings("PMD.LinguisticNaming") // it's the name of the use case
    private SetEnableStatus setEnableStatus() {
        return new SetEnableStatus(auctionAutomationRepository);
    }
}
