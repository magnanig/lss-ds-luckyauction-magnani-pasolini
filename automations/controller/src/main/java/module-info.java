module lss.ds.luckyauction.magnani.pasolini.automations.controller.main {
    requires lss.ds.luckyauction.magnani.pasolini.configuration.main;
    exports it.unibo.luckyauction.automation.controller;
    exports it.unibo.luckyauction.automation.controller.config;
    requires lss.ds.luckyauction.magnani.pasolini.utils.main;
    requires lss.ds.luckyauction.magnani.pasolini.uuid.generator.main;
    requires lss.ds.luckyauction.magnani.pasolini.automations.domain.main;
    requires lss.ds.luckyauction.magnani.pasolini.automations.usecase.main;
    requires lss.ds.luckyauction.magnani.pasolini.automations.adapter.main;
    requires lss.ds.luckyauction.magnani.pasolini.automations.repository.mongo.main;
}