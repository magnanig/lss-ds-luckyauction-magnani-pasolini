/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.automation.controller;

import it.unibo.luckyauction.automation.controller.config.AuctionAutomationConfig;
import it.unibo.luckyauction.automation.domain.entity.AuctionAutomation;
import it.unibo.luckyauction.automation.domain.entity.UserAuctionsAutomations;
import it.unibo.luckyauction.automation.usecase.exception.AuctionAutomationNotFoundForUserException;
import it.unibo.luckyauction.util.NumericalConstants;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestMethodOrder;

import java.math.BigDecimal;
import java.time.Duration;
import java.util.Collection;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class AutomationControllerTests {
    private static final String FIRST_USERNAME = "firstUsername";
    private static final String AUCTION_TEST_ID_ONE = "auction-one";
    private static final String AUCTION_TEST_ID_TWO = "auction-two";
    private static final Duration AUTOMATION_RAISE_DELAY = Duration.ofHours(1);
    private static final BigDecimal AUTOMATION_PRICE_INCREASE = BigDecimal.ONE;
    private static final BigDecimal AUTOMATION_LIMIT_PRICE = BigDecimal.valueOf(50.0);

    private final AuctionAutomationController automationController = new AuctionAutomationConfig()
            .auctionAutomationController(new EmptyAuctionAutomationSocket());
    private AuctionAutomation firstAutomation;
    private AuctionAutomation secondAutomation;

    @BeforeAll
    void createUserAuctionsAutomations() {
        if (automationController.findByUsername(FIRST_USERNAME).isPresent()) {
            automationController.deleteByUsername(FIRST_USERNAME);
        }
        automationController.create(new UserAuctionsAutomations(FIRST_USERNAME));
    }

    @AfterAll
    void deleteUserAuctionsAutomations() {
        automationController.deleteByUsername(FIRST_USERNAME).ifPresentOrElse(userAuctionsAutomations ->
                assertEquals(FIRST_USERNAME, userAuctionsAutomations.getUsername()), Assertions::fail);
    }

    @Test @Order(1)
    void findUserUserAuctionsAutomationsByUsernameTest() {
        final UserAuctionsAutomations automations = automationController.findByUsername(FIRST_USERNAME).orElseThrow();

        assertEquals(FIRST_USERNAME, automations.getUsername());
        assertEquals(0, automations.getAuctionsAutomations().size());
    }

    @Test @Order(2)
    void addAuctionAutomationTest() {
        firstAutomation = automationController
                .addAuctionAutomation(createNewAuctionAutomation(AUCTION_TEST_ID_ONE), FIRST_USERNAME);

        assertTrue(firstAutomation.isEnabled());
        assertEquals(AUCTION_TEST_ID_ONE, firstAutomation.getAuctionId());
        assertEquals(AUTOMATION_LIMIT_PRICE, firstAutomation.getLimitPrice());
        assertEquals(AUTOMATION_PRICE_INCREASE, firstAutomation.getPriceIncrease());
        assertEquals(AUTOMATION_RAISE_DELAY, firstAutomation.getRaiseDelay());

        secondAutomation = automationController
                .addAuctionAutomation(createNewAuctionAutomation(AUCTION_TEST_ID_TWO), FIRST_USERNAME);

        assertTrue(secondAutomation.isEnabled());
        assertEquals(AUCTION_TEST_ID_TWO, secondAutomation.getAuctionId());
        assertEquals(AUTOMATION_LIMIT_PRICE, secondAutomation.getLimitPrice());
        assertEquals(AUTOMATION_PRICE_INCREASE, secondAutomation.getPriceIncrease());
        assertEquals(AUTOMATION_RAISE_DELAY, secondAutomation.getRaiseDelay());
    }

    @Test @Order(3)
    void findAllActiveAutomationsTest() {
        final List<AuctionAutomation> activeAutomations = automationController.getAllActiveAutomations()
                .stream()
                .map(UserAuctionsAutomations::getAuctionsAutomations)
                .flatMap(Collection::stream)
                .toList();

        assertEquals(List.of(firstAutomation, secondAutomation), activeAutomations);
    }

    @Test @Order(4)
    void findAutomationForAuctionTest() {
        assertEquals(firstAutomation,
                automationController.findAutomationForAuction(AUCTION_TEST_ID_ONE, FIRST_USERNAME).orElseThrow());
        assertEquals(secondAutomation,
                automationController.findAutomationForAuction(AUCTION_TEST_ID_TWO, FIRST_USERNAME).orElseThrow());
    }

    @Test @Order(NumericalConstants.FIVE)
    void setEnableStatusExceptionTest() {
        final Exception exception = assertThrows(AuctionAutomationNotFoundForUserException.class,
                () -> automationController.setEnableStatus(false, "wrongAuctionId", FIRST_USERNAME));
        assertTrue(exception.getMessage().contains("No automation found"));
    }

    @Test @Order(NumericalConstants.SIX)
    void setEnableStatusTest() {
        automationController.setEnableStatus(false, AUCTION_TEST_ID_ONE, FIRST_USERNAME);
        assertFalse(automationController
                .findAutomationForAuction(AUCTION_TEST_ID_ONE, FIRST_USERNAME).orElseThrow().isEnabled());
    }

    @Test @Order(NumericalConstants.SEVEN)
    void removeAuctionAutomationTest() {
        automationController.removeAuctionAutomation(AUCTION_TEST_ID_ONE, FIRST_USERNAME);

        assertEquals(1,
                automationController.findByUsername(FIRST_USERNAME).orElseThrow().getAuctionsAutomations().size());
        assertFalse(automationController.findAutomationForAuction(AUCTION_TEST_ID_ONE, FIRST_USERNAME).isPresent());
    }

    @Test @Order(NumericalConstants.EIGHT)
    void removeAllAutomationsForAuctionTest() {
        automationController.removeAllAutomationsForAuction(AUCTION_TEST_ID_TWO);

        assertEquals(0,
                automationController.findByUsername(FIRST_USERNAME).orElseThrow().getAuctionsAutomations().size());
        assertFalse(automationController.findAutomationForAuction(AUCTION_TEST_ID_TWO, FIRST_USERNAME).isPresent());
    }

    private AuctionAutomation createNewAuctionAutomation(final String auctionId) {
        return AuctionAutomation.builder()
                .auctionId(auctionId)
                .limitPrice(AUTOMATION_LIMIT_PRICE)
                .priceIncrease(AUTOMATION_PRICE_INCREASE)
                .raiseDelay(AUTOMATION_RAISE_DELAY)
                .enabled(true)
                .build();
    }
}
