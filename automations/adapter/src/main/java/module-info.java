module lss.ds.luckyauction.magnani.pasolini.automations.adapter.main {
    exports it.unibo.luckyauction.automation.adapter;
    exports it.unibo.luckyauction.automation.adapter.api;
    exports it.unibo.luckyauction.automation.adapter.socket;
    requires lss.ds.luckyauction.magnani.pasolini.utils.main;
    requires lss.ds.luckyauction.magnani.pasolini.automations.domain.main;
    requires lss.ds.luckyauction.magnani.pasolini.automations.usecase.main;
}