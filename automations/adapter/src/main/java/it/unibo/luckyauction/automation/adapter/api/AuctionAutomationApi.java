/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.automation.adapter.api;

import it.unibo.luckyauction.automation.adapter.UserAuctionsAutomationsWeb;

import java.util.List;
import java.util.Optional;

public interface AuctionAutomationApi {

    /**
     * Create a new user auctions automations.
     * @param username  the username associated to the user auction automations object
     */
    void createUserAuctionAutomations(String username);

    /**
     * Get all active user auctions automations.
     * @return  a list containing all active user auctions automations
     */
    List<UserAuctionsAutomationsWeb> getAllActiveAutomations();

    /**
     * Delete a user auctions automations by the associated username.
     * @param username  the desired username
     * @return          the just deleted user auctions automations, if existed
     */
    Optional<UserAuctionsAutomationsWeb> deleteByUsername(String username);

    /**
     * Disable the automation for the specified auction and user.
     * @param auctionId     the id of the auction whose automation to disable
     * @param username      the username of the involved user
     */
    void disableAutomation(String auctionId, String username);

    /**
     * Remove an auction automation for the specified username.
     * @param auctionId the id of the auction whose automation to remove
     * @param username  the username of the involved user
     */
    void removeAuctionAutomation(String auctionId, String username);

    /**
     * Remove all auction automation for the specified auction id.
     * @param auctionId the id of the auction from which to remove all its automations
     */
    void removeAllAutomationsForAuction(String auctionId);
}
