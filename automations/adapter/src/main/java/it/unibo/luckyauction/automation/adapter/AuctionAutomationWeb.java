/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.automation.adapter;

import it.unibo.luckyauction.automation.domain.entity.AuctionAutomation;
import it.unibo.luckyauction.util.CalendarUtils;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

import static it.unibo.luckyauction.util.NullableUtils.safeCall;

/**
 * Represents the schema of an {@link AuctionAutomation}.
 * This is a (de)serializable version with all getters and setters,
 * to be used to send object through the network.
 */
public final class AuctionAutomationWeb {

    private String auctionAutomationId;
    private String auctionId;
    private BigDecimal limitPrice;
    private BigDecimal priceIncrease;
    private DurationWeb raiseDelay;
    private Date automationEnd;
    private Boolean enabled;

    public static AuctionAutomationWeb fromDomain(final AuctionAutomation auctionAutomation) {
        final AuctionAutomationWeb auctionAutomationWeb = new AuctionAutomationWeb();
        auctionAutomationWeb.auctionAutomationId = auctionAutomation.getAuctionAutomationId();
        auctionAutomationWeb.auctionId = auctionAutomation.getAuctionId();
        auctionAutomationWeb.limitPrice = auctionAutomation.getLimitPrice();
        auctionAutomationWeb.priceIncrease = auctionAutomation.getPriceIncrease();
        auctionAutomationWeb.raiseDelay = DurationWeb.fromDuration(auctionAutomation.getRaiseDelay());
        auctionAutomationWeb.automationEnd = safeCall(auctionAutomation.getAutomationEnd(), Calendar::getTime);
        auctionAutomationWeb.enabled = auctionAutomation.isEnabled();
        return auctionAutomationWeb;
    }

    public AuctionAutomation toDomain() {
        return AuctionAutomation.builder()
                .auctionAutomationId(auctionAutomationId)
                .auctionId(auctionId)
                .limitPrice(limitPrice)
                .priceIncrease(priceIncrease)
                .raiseDelay(raiseDelay.toDuration())
                .automationEnd(safeCall(automationEnd, CalendarUtils::fromDate))
                .enabled(enabled)
                .build();
    }

    public String getAuctionAutomationId() {
        return auctionAutomationId;
    }

    public void setAuctionAutomationId(final String auctionAutomationId) {
        this.auctionAutomationId = auctionAutomationId;
    }

    public String getAuctionId() {
        return auctionId;
    }

    public void setAuctionId(final String auctionId) {
        this.auctionId = auctionId;
    }

    public BigDecimal getLimitPrice() {
        return limitPrice;
    }

    public void setLimitPrice(final BigDecimal limitPrice) {
        this.limitPrice = limitPrice;
    }

    public BigDecimal getPriceIncrease() {
        return priceIncrease;
    }

    public void setPriceIncrease(final BigDecimal priceIncrease) {
        this.priceIncrease = priceIncrease;
    }

    public DurationWeb getRaiseDelay() {
        return raiseDelay.copy();
    }

    public void setRaiseDelay(final DurationWeb raiseDelay) {
        this.raiseDelay = raiseDelay.copy();
    }

    public Date getAutomationEnd() {
        return safeCall(automationEnd, CalendarUtils::copy);
    }

    public void setAutomationEnd(final Date automationEnd) {
        this.automationEnd = safeCall(automationEnd, CalendarUtils::copy);
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(final Boolean enabled) {
        this.enabled = enabled;
    }
}
