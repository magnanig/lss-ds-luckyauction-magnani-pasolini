/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.automation.adapter.socket;

import it.unibo.luckyauction.automation.adapter.AuctionAutomationWeb;
import it.unibo.luckyauction.automation.domain.entity.AuctionAutomation;

public class CreateAuctionAutomation implements AuctionAutomationEvent {

    private final AuctionAutomationWeb auctionAutomation;
    private final Token token;

    public CreateAuctionAutomation(final AuctionAutomation auctionAutomation, final Token token) {
        this.auctionAutomation = AuctionAutomationWeb.fromDomain(auctionAutomation);
        this.token = token;
    }

    @Override
    public EventType eventType() {
        return EventType.CREATE_AUTOMATION;
    }

    @Override
    public Object[] extractArgs() {
        return new Object[] { auctionAutomation, token };
    }
}
