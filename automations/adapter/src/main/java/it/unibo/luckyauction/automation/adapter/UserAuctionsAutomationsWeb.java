/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.automation.adapter;

import it.unibo.luckyauction.automation.domain.entity.UserAuctionsAutomations;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Represents the schema of a {@link UserAuctionsAutomations}.
 * This is a (de)serializable version with all getters and setters,
 * to be used to send object through the network.
 */
public final class UserAuctionsAutomationsWeb {

    private String username;
    private Set<AuctionAutomationWeb> auctionsAutomations;

    public static UserAuctionsAutomationsWeb fromDomain(final UserAuctionsAutomations userAuctionsAutomations) {
        final UserAuctionsAutomationsWeb userAuctionsAutomationsWeb = new UserAuctionsAutomationsWeb();
        userAuctionsAutomationsWeb.username = userAuctionsAutomations.getUsername();
        userAuctionsAutomationsWeb.auctionsAutomations = userAuctionsAutomations.getAuctionsAutomations().stream()
                .map(AuctionAutomationWeb::fromDomain)
                .collect(Collectors.toSet());
        return userAuctionsAutomationsWeb;
    }

    public UserAuctionsAutomations toDomain() {
        final UserAuctionsAutomations userAuctionsAutomations = new UserAuctionsAutomations(username);
        if (auctionsAutomations != null && !auctionsAutomations.isEmpty()) {
            userAuctionsAutomations.addAllAuctionsAutomations(auctionsAutomations.stream()
                    .map(AuctionAutomationWeb::toDomain)
                    .collect(Collectors.toSet()));
        }
        return userAuctionsAutomations;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(final String username) {
        this.username = username;
    }

    public Set<AuctionAutomationWeb> getAuctionsAutomations() {
        return new HashSet<>(auctionsAutomations);
    }

    public void setAuctionsAutomations(final Set<AuctionAutomationWeb> auctionsAutomations) {
        this.auctionsAutomations = new HashSet<>(auctionsAutomations);
    }
}
