dependencies {
    implementation(project(":utils"))
    implementation(project(":automations:domain"))
    implementation(project(":automations:usecase"))
}