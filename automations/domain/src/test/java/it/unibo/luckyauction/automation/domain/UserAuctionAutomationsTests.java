/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.automation.domain;

import it.unibo.luckyauction.auction.domain.AuctionTestUtils;
import it.unibo.luckyauction.automation.domain.entity.AuctionAutomation;
import it.unibo.luckyauction.automation.domain.entity.UserAuctionsAutomations;
import it.unibo.luckyauction.automation.domain.exception.AuctionAutomationValidationException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.math.BigDecimal;
import java.time.Duration;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class UserAuctionAutomationsTests {

    private AuctionAutomation firstAutomation;
    private AuctionAutomation secondAutomation;
    private AuctionAutomation thirdAutomation;

    @BeforeAll
    void config() {
        final Calendar afterTwoDays = Calendar.getInstance(); afterTwoDays.add(Calendar.DATE, 2);
        final Supplier<AuctionAutomation.Builder> preBuiltAutomation = () -> AuctionAutomation.builder()
                .limitPrice(AuctionTestUtils.NEW_BID_RAISE_AMOUNT).priceIncrease(BigDecimal.ONE)
                .raiseDelay(Duration.ofMinutes(1)).automationEnd(afterTwoDays);

        firstAutomation = preBuiltAutomation.get().auctionId("111").build();
        secondAutomation = preBuiltAutomation.get().auctionId("222").build();
        thirdAutomation = preBuiltAutomation.get().auctionId("333").build();
    }

    @Test
    void userAuctionsAutomationsTest() {
        final UserAuctionsAutomations userAutomations = new UserAuctionsAutomations(AuctionTestUtils.FIRST_USERNAME);

        assertEquals(AuctionTestUtils.FIRST_USERNAME, userAutomations.getUsername());
        assertEquals(Collections.emptySet(), userAutomations.getAuctionsAutomations());
        assertEquals(Optional.empty(), userAutomations.getAuctionAutomationForAuction("444"));

        userAutomations.addAuctionAutomation(firstAutomation);
        assertEquals(new HashSet<>(List.of(firstAutomation)), userAutomations.getAuctionsAutomations());
        userAutomations.addAllAuctionsAutomations(List.of(secondAutomation, thirdAutomation));
        assertEquals(new HashSet<>(List.of(firstAutomation, secondAutomation, thirdAutomation)),
                userAutomations.getAuctionsAutomations());

        userAutomations.removeAuctionAutomation(secondAutomation.getAuctionId());
        assertEquals(new HashSet<>(List.of(firstAutomation, thirdAutomation)),
                userAutomations.getAuctionsAutomations());
    }

    @Test
    void userAuctionsAutomationsExceptionTest() {
        final Exception usernameException = assertThrows(AuctionAutomationValidationException.class,
                () -> new UserAuctionsAutomations(""));
        assertTrue(usernameException.getMessage().contains("Username is not set"));
    }
}
