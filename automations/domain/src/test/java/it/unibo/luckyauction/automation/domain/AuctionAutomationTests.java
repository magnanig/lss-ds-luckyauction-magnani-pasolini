/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.automation.domain;

import it.unibo.luckyauction.auction.domain.AuctionTestUtils;
import it.unibo.luckyauction.automation.domain.entity.AuctionAutomation;
import it.unibo.luckyauction.automation.domain.exception.AuctionAutomationValidationException;
import it.unibo.luckyauction.util.NumericalConstants;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.math.BigDecimal;
import java.time.Duration;
import java.util.Calendar;
import java.util.function.Supplier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class AuctionAutomationTests {

    private final Calendar afterTwoDays = Calendar.getInstance();

    @BeforeAll
    void config() {
        afterTwoDays.add(Calendar.DATE, 2);
    }

    @Test
    void auctionAutomationTest() {
        final Supplier<AuctionAutomation.Builder> preBuiltAutomation = () -> AuctionAutomation.builder()
                .auctionAutomationId("automationId").auctionId(AuctionTestUtils.AUCTION_TEST_ID)
                .limitPrice(AuctionTestUtils.NEW_BID_RAISE_AMOUNT).priceIncrease(BigDecimal.ONE)
                .raiseDelay(Duration.ofMinutes(1)).automationEnd(afterTwoDays).enabled(true);

        final AuctionAutomation defaultAutomation = preBuiltAutomation.get().build();
        final AuctionAutomation automation = preBuiltAutomation.get().enabled(true).build();

        assertEquals(defaultAutomation.isEnabled(), automation.isEnabled());
        assertEquals("automationId", automation.getAuctionAutomationId());
        assertEquals(AuctionTestUtils.AUCTION_TEST_ID, automation.getAuctionId());
        assertEquals(AuctionTestUtils.NEW_BID_RAISE_AMOUNT, automation.getLimitPrice());
        assertEquals(BigDecimal.ONE, automation.getPriceIncrease());
        assertEquals(Duration.ofMinutes(1), automation.getRaiseDelay());
        assertTrue(automation.getAutomationEnd().isPresent());
        assertEquals(afterTwoDays, automation.getAutomationEnd().get());
        assertTrue(automation.isEnabled());
        automation.setEnabled(false);
        assertFalse(automation.isEnabled());
    }

    @Test
    void auctionIdExceptionTest() {
        final Exception auctionIdException = assertThrows(AuctionAutomationValidationException.class,
                () -> AuctionAutomation.builder().limitPrice(AuctionTestUtils.NEW_BID_RAISE_AMOUNT)
                        .priceIncrease(BigDecimal.ONE).raiseDelay(Duration.ofMinutes(1))
                        .automationEnd(afterTwoDays).build());
        assertTrue(auctionIdException.getMessage().contains("Auction id is not set"));
    }

    @Test
    void limitPriceExceptionsTest() {
        final Supplier<AuctionAutomation.Builder> preBuiltAutomation =
                () -> AuctionAutomation.builder().auctionId(AuctionTestUtils.AUCTION_TEST_ID)
                        .priceIncrease(BigDecimal.ONE).raiseDelay(Duration.ofMinutes(1))
                        .automationEnd(afterTwoDays);

        final Exception firstException = assertThrows(AuctionAutomationValidationException.class,
                () -> preBuiltAutomation.get().build());
        assertTrue(firstException.getMessage().contains("Limit price is not set"));

        final Exception secondException = assertThrows(AuctionAutomationValidationException.class,
                () -> preBuiltAutomation.get().limitPrice(BigDecimal.ZERO).build());
        assertTrue(secondException.getMessage().contains("must be greater than zero"));
    }

    @Test
    void priceIncreaseExceptionsTest() {
        final Supplier<AuctionAutomation.Builder> preBuiltAutomation =
                () -> AuctionAutomation.builder().auctionId(AuctionTestUtils.AUCTION_TEST_ID)
                        .limitPrice(AuctionTestUtils.NEW_BID_RAISE_AMOUNT)
                        .raiseDelay(Duration.ofMinutes(1)).automationEnd(afterTwoDays);

        final Exception firstException = assertThrows(AuctionAutomationValidationException.class,
                () -> preBuiltAutomation.get().build());
        assertTrue(firstException.getMessage().contains("Price increase is not set"));

        final Exception secondException = assertThrows(AuctionAutomationValidationException.class,
                () -> preBuiltAutomation.get().priceIncrease(BigDecimal.ZERO).build());
        assertTrue(secondException.getMessage().contains("must be greater than or equal to one"));

        final Exception thirdException = assertThrows(AuctionAutomationValidationException.class,
                () -> preBuiltAutomation.get()
                        .priceIncrease(AuctionTestUtils.NEW_BID_RAISE_AMOUNT.add(BigDecimal.ONE)).build());
        assertTrue(thirdException.getMessage().contains("Price increase cannot be greater than the limit price"));
    }

    @Test
    void raiseDelayExceptionsTest() {
        final Supplier<AuctionAutomation.Builder> preBuiltAutomation =
                () -> AuctionAutomation.builder().auctionId(AuctionTestUtils.AUCTION_TEST_ID)
                        .limitPrice(AuctionTestUtils.NEW_BID_RAISE_AMOUNT).priceIncrease(BigDecimal.ONE)
                        .automationEnd(afterTwoDays);

        final Exception firstException = assertThrows(AuctionAutomationValidationException.class,
                () -> preBuiltAutomation.get().build());
        assertTrue(firstException.getMessage().contains("Raise delay is not set"));

        final Exception secondException = assertThrows(AuctionAutomationValidationException.class,
                () -> preBuiltAutomation.get().raiseDelay(Duration.ofSeconds(0)).build());
        assertTrue(secondException.getMessage().contains("Raise delay cannot be less than thirty seconds"));

        final Exception thirdException = assertThrows(AuctionAutomationValidationException.class,
                () -> preBuiltAutomation.get().raiseDelay(Duration.ofHours(NumericalConstants.TWENTY_FIVE)).build());
        assertTrue(thirdException.getMessage().contains("Raise delay cannot be more than one day"));
    }
}
