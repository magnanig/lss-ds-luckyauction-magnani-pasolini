module lss.ds.luckyauction.magnani.pasolini.automations.domain.main {
    exports it.unibo.luckyauction.automation.domain.entity;
    exports it.unibo.luckyauction.automation.domain.exception;
    requires lombok;
    requires lss.ds.luckyauction.magnani.pasolini.utils.main;
}