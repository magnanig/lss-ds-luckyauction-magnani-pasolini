/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.automation.domain.entity;

import it.unibo.luckyauction.automation.domain.exception.AuctionAutomationValidationException;
import it.unibo.luckyauction.util.CalendarUtils;
import it.unibo.luckyauction.util.NullableUtils;
import lombok.Generated;

import java.math.BigDecimal;
import java.time.Duration;
import java.util.Calendar;
import java.util.Objects;
import java.util.Optional;

public final class AuctionAutomation {

    private final String auctionAutomationId;
    private final String auctionId;
    private final BigDecimal limitPrice;
    private final BigDecimal priceIncrease;
    private final Duration raiseDelay;
    private final Calendar automationEnd;
    private Boolean enabled;

    private AuctionAutomation(final String auctionAutomationId, final String auctionId, final BigDecimal limitPrice,
                              final BigDecimal priceIncrease, final Duration raiseDelay, final Calendar automationEnd,
                              final Boolean enabled) {
        this.auctionAutomationId = auctionAutomationId;
        this.auctionId = auctionId;
        this.limitPrice = limitPrice;
        this.priceIncrease = priceIncrease;
        this.raiseDelay = raiseDelay;
        this.automationEnd = automationEnd;
        this.enabled = enabled;
    }

    public String getAuctionAutomationId() {
        return auctionAutomationId;
    }

    public String getAuctionId() {
        return auctionId;
    }

    public BigDecimal getLimitPrice() {
        return limitPrice;
    }

    public BigDecimal getPriceIncrease() {
        return priceIncrease;
    }

    public Duration getRaiseDelay() {
        return raiseDelay;
    }

    public Optional<Calendar> getAutomationEnd() {
        return Optional.ofNullable(CalendarUtils.copy(automationEnd));
    }

    public Boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(final Boolean enabled) {
        this.enabled = enabled;
    }

    public static Builder builder() {
        return new Builder();
    }

    @Override @Generated
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final AuctionAutomation that = (AuctionAutomation) o;
        return Objects.equals(auctionAutomationId, that.auctionAutomationId) && enabled.equals(that.enabled)
                && auctionId.equals(that.auctionId) && limitPrice.equals(that.limitPrice)
                && priceIncrease.equals(that.priceIncrease) && raiseDelay.equals(that.raiseDelay)
                && Objects.equals(automationEnd, that.automationEnd);
    }

    @Override @Generated
    public int hashCode() {
        return Objects.hash(auctionId, limitPrice, priceIncrease, raiseDelay, automationEnd, enabled);
    }

    @Override @Generated
    public String toString() {
        return "AuctionAutomation{"
                + "auctionAutomationId='" + auctionAutomationId + "'"
                + ", auctionId='" + auctionId + "'"
                + ", limitPrice=" + limitPrice
                + ", priceIncrease=" + priceIncrease
                + ", raiseDelay=" + raiseDelay
                + ", automationEnd=" + NullableUtils.safeCall(automationEnd, Calendar::getTime)
                + ", isEnabled=" + enabled
                + "}";
    }

    @SuppressWarnings({"checkstyle:JavadocVariable", "PMD.AvoidFieldNameMatchingMethodName"})
    public static final class Builder {

        private String auctionAutomationId;
        private String auctionId;
        private BigDecimal limitPrice;
        private BigDecimal priceIncrease;
        private Duration raiseDelay;
        private Calendar automationEnd;
        private Boolean enabled;

        private Builder() { }

        public Builder auctionAutomationId(final String auctionAutomationId) {
            this.auctionAutomationId = auctionAutomationId;
            return this;
        }

        public Builder auctionId(final String auctionId) {
            this.auctionId = auctionId;
            return this;
        }

        public Builder limitPrice(final BigDecimal limitPrice) {
            this.limitPrice = limitPrice;
            return this;
        }

        public Builder priceIncrease(final BigDecimal priceIncrease) {
            this.priceIncrease = priceIncrease;
            return this;
        }

        public Builder raiseDelay(final Duration raiseDelay) {
            this.raiseDelay = raiseDelay;
            return this;
        }

        public Builder automationEnd(final Calendar automationEnd) {
            this.automationEnd = CalendarUtils.copy(automationEnd);
            return this;
        }

        public Builder enabled(final Boolean enabled) {
            this.enabled = enabled;
            return this;
        }

        public AuctionAutomation build() {
            validate();
            return new AuctionAutomation(auctionAutomationId, auctionId, limitPrice, priceIncrease, raiseDelay,
                    automationEnd, enabled);
        }

        private void validate() {
            if (auctionId == null || auctionId.isEmpty()) {
                throw new AuctionAutomationValidationException("Auction id is not set in auction automation");
            }
            checkLimitPrice();
            checkPriceIncrease();
            checkRaiseDelay();
            enabled = enabled == null || enabled;
        }

        private void checkLimitPrice() {
            if (limitPrice == null) {
                throw new AuctionAutomationValidationException("Limit price is not set in auction automation");
            }
            if (limitPrice.compareTo(BigDecimal.ZERO) <= 0 || limitPrice.scale() > 2) {
                throw new AuctionAutomationValidationException("Limit price of the auction automation must be "
                        + "greater than zero and can have at most two decimal digits");
            }
        }

        private void checkPriceIncrease() {
            if (priceIncrease == null) {
                throw new AuctionAutomationValidationException("Price increase is not set in auction automation");
            }
            if (priceIncrease.compareTo(BigDecimal.ONE) < 0 || priceIncrease.scale() > 2) {
                throw new AuctionAutomationValidationException("Price increase of the auction automation must be "
                        + "greater than or equal to one and can have at most two decimal digits");
            }
            if (priceIncrease.compareTo(limitPrice) > 0) {
                throw new AuctionAutomationValidationException("Price increase cannot be greater than the limit price");
            }
        }

        private void checkRaiseDelay() {
            if (raiseDelay == null) {
                throw new AuctionAutomationValidationException("Raise delay is not set in auction automation");
            }
            if (raiseDelay.compareTo(Duration.ofSeconds(1)) < 0) {
                throw new AuctionAutomationValidationException("Raise delay cannot be less than thirty seconds");
            }
            if (raiseDelay.compareTo(Duration.ofDays(1)) > 0) {
                throw new AuctionAutomationValidationException("Raise delay cannot be more than one day");
            }
        }
    }
}
