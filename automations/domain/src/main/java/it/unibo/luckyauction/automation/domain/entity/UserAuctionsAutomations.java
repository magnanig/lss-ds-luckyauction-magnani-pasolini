/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.automation.domain.entity;

import it.unibo.luckyauction.automation.domain.exception.AuctionAutomationValidationException;
import lombok.Generated;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

/**
 * Class of auctions automations associated with a single user.
 */
public class UserAuctionsAutomations {

    private final String username;
    private final Set<AuctionAutomation> auctionsAutomations;

    /**
     * Create the auctions automations object for a specific user.
     */
    public UserAuctionsAutomations(final String username) {
        if (username == null || username.isEmpty()) {
            throw new AuctionAutomationValidationException("Username is not set in user auction automation");
        }
        this.username = username;
        this.auctionsAutomations = new HashSet<>();
    }

    /**
     * Get the user's username.
     */
    public String getUsername() {
        return username;
    }

    /**
     * Get the automation of a specific auction.
     * @param auctionId the auction id
     */
    public Optional<AuctionAutomation> getAuctionAutomationForAuction(final String auctionId) {
        return auctionsAutomations.stream()
                .filter(auctionAutomation -> auctionAutomation.getAuctionId().equals(auctionId))
                .findFirst();
    }

    /**
     * Get all the auctions automations associated with the specific user.
     */
    public Set<AuctionAutomation> getAuctionsAutomations() {
        return Collections.unmodifiableSet(auctionsAutomations);
    }

    /**
     * Adds an auction automation.
     * @param auctionAutomation the auction automation to add
     */
    public void addAuctionAutomation(final AuctionAutomation auctionAutomation) {
        auctionsAutomations.add(auctionAutomation);
    }

    /**
     * Adds a collection of auctions automations.
     * @param auctionsAutomations   the auctions automations collections to add
     */
    public void addAllAuctionsAutomations(final Collection<AuctionAutomation> auctionsAutomations) {
        this.auctionsAutomations.addAll(auctionsAutomations);
    }

    /**
     * Removes a specific auction automation.
     * @param auctionId the id of the auction whose automation to remove
     */
    public void removeAuctionAutomation(final String auctionId) {
        auctionsAutomations.removeIf(elem -> elem.getAuctionId().equals(auctionId));
    }

    /**
     * {@inheritDoc}
     */
    @Override @Generated
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final UserAuctionsAutomations that = (UserAuctionsAutomations) o;
        return username.equals(that.username) && auctionsAutomations.equals(that.auctionsAutomations);
    }

    /**
     * {@inheritDoc}
     */
    @Override @Generated
    public int hashCode() {
        return Objects.hash(username, auctionsAutomations);
    }

    /**
     * {@inheritDoc}
     */
    @Override @Generated
    public String toString() {
        return "UserAuctionsAutomations{\n"
                + "\tusername='" + username + "'" + "\n"
                + "\tauctionsAutomations=" + auctionsAutomations + "\n"
                + "}";
    }
}
