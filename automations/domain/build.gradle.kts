dependencies {
    implementation(project(":utils"))

    // this will add following dependencies: test --> users.domain.testFixtures
    testImplementation(testFixtures(project(":auctions:domain")))
}