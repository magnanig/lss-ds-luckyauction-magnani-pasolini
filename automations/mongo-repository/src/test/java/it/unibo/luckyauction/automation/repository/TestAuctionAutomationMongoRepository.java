/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.automation.repository;

import it.unibo.luckyauction.automation.domain.entity.AuctionAutomation;
import it.unibo.luckyauction.automation.domain.entity.UserAuctionsAutomations;
import it.unibo.luckyauction.automation.usecase.port.AuctionAutomationRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.math.BigDecimal;
import java.time.Duration;
import java.util.Collections;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestAuctionAutomationMongoRepository {

    private static final String USERNAME = "test";

    private final AuctionAutomationRepository auctionAutomationRepository =
            AuctionAutomationMongoRepository.testRepository();

    private final AuctionAutomation auctionAutomation = AuctionAutomation.builder()
            .auctionId("1")
            .limitPrice(new BigDecimal(100))
            .priceIncrease(new BigDecimal(1))
            .raiseDelay(Duration.ofMinutes(3))
            .build();

    @BeforeEach
    void createUserAuctionsAutomations() {
        if (auctionAutomationRepository.findByUsername(USERNAME).isPresent()) {
            auctionAutomationRepository.deleteByUsername(USERNAME);
        }
        assertTrue(auctionAutomationRepository.findByUsername(USERNAME).isEmpty());
        auctionAutomationRepository.create(new UserAuctionsAutomations(USERNAME));
        assertTrue(auctionAutomationRepository.findByUsername(USERNAME).isPresent());
    }

    @AfterEach
    void deleteUserAuctionsAutomations() {
        auctionAutomationRepository.deleteByUsername(USERNAME);
        assertTrue(auctionAutomationRepository.findByUsername(USERNAME).isEmpty());
    }

    @Test
    void testAddAuctionAutomation() {
        auctionAutomationRepository.addAuctionAutomation(auctionAutomation, USERNAME);
        assertEquals(Optional.of(auctionAutomation), auctionAutomationRepository.findAutomationForAuction(
                auctionAutomation.getAuctionId(), USERNAME));
    }

    @Test
    void testGetAllActiveAutomations() {
        testAddAuctionAutomation();
        final UserAuctionsAutomations userAuctionsAutomations = new UserAuctionsAutomations(USERNAME);
        userAuctionsAutomations.addAuctionAutomation(auctionAutomation);
        assertEquals(Set.of(userAuctionsAutomations), auctionAutomationRepository.getAllActiveAutomations());
    }

    @Test
    void testAuctionAutomationChangeEnabledStatus() {
        testAddAuctionAutomation();
        auctionAutomation.setEnabled(!auctionAutomation.isEnabled());
        auctionAutomationRepository.setEnableStatus(auctionAutomation.isEnabled(),
                auctionAutomation.getAuctionId(), USERNAME);
        assertEquals(Optional.of(auctionAutomation), auctionAutomationRepository.findAutomationForAuction(
                auctionAutomation.getAuctionId(), USERNAME));
        auctionAutomation.setEnabled(!auctionAutomation.isEnabled());
    }

    @Test
    void testRemoveAuctionAutomation() {
        testAddAuctionAutomation();
        auctionAutomationRepository.removeAuctionAutomation(auctionAutomation.getAuctionId(), USERNAME);
        assertEquals(Optional.empty(), auctionAutomationRepository.findAutomationForAuction(
                auctionAutomation.getAuctionId(), USERNAME));
    }

    @Test
    void testRemoveAllAuctionAutomationsForAuction() {
        testAddAuctionAutomation();
        auctionAutomationRepository.removeAllAutomationsForAuction(auctionAutomation.getAuctionId());
        assertEquals(Collections.emptySet(), auctionAutomationRepository.findAutomationsForAuction(
                auctionAutomation.getAuctionId()));
        assertEquals(Collections.emptySet(), auctionAutomationRepository.getAllActiveAutomations());
    }

    @Test
    void testAllAutomationsForAuction() {
        testAddAuctionAutomation();
        assertEquals(Set.of(auctionAutomation),
                auctionAutomationRepository.findAutomationsForAuction(auctionAutomation.getAuctionId()));
    }
}
