module lss.ds.luckyauction.magnani.pasolini.automations.repository.mongo.main {
    exports it.unibo.luckyauction.automation.repository;
    requires lss.ds.luckyauction.magnani.pasolini.utils.main;
    requires lss.ds.luckyauction.magnani.pasolini.mongo.utils.main;
    requires lss.ds.luckyauction.magnani.pasolini.automations.domain.main;
    requires lss.ds.luckyauction.magnani.pasolini.automations.usecase.main;
    requires lss.ds.luckyauction.magnani.pasolini.automations.adapter.main;
    requires mongo.java.driver;
}