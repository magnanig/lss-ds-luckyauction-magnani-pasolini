/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.automation.repository;

import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Updates;
import it.unibo.luckyauction.automation.adapter.AuctionAutomationWeb;
import it.unibo.luckyauction.automation.adapter.UserAuctionsAutomationsWeb;
import it.unibo.luckyauction.automation.domain.entity.AuctionAutomation;
import it.unibo.luckyauction.automation.domain.entity.UserAuctionsAutomations;
import it.unibo.luckyauction.repository.util.CommonsMongoOperations;
import it.unibo.luckyauction.repository.util.MongoDatabaseManager;
import it.unibo.luckyauction.automation.usecase.port.AuctionAutomationRepository;
import org.bson.BsonBoolean;
import org.bson.BsonDocument;
import org.bson.BsonString;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Updates.push;
import static com.mongodb.client.model.Updates.set;
import static it.unibo.luckyauction.repository.util.MongoConnectionsConstants.AUCTIONS_AUTOMATIONS_COLLECTION_NAME;
import static it.unibo.luckyauction.repository.util.MongoConnectionsConstants.AUCTIONS_AUTOMATIONS_CONNECTION_STRING;
import static it.unibo.luckyauction.repository.util.MongoConnectionsConstants.AUCTIONS_AUTOMATIONS_DB_NAME;
import static it.unibo.luckyauction.repository.util.MongoConnectionsConstants.TEST_CONNECTION_STRING;
import static it.unibo.luckyauction.repository.util.MongoConnectionsConstants.TEST_DB_NAME;

public final class AuctionAutomationMongoRepository implements AuctionAutomationRepository {

    private static final String USERNAME_FIELD = "username";
    private static final String AUCTIONS_AUTOMATIONS_FIELD = "auctionsAutomations";
    private final MongoCollection<UserAuctionsAutomationsWeb> usersAuctionsAutomationsCollection;
    private final CommonsMongoOperations<UserAuctionsAutomationsWeb, UserAuctionsAutomations> commons;

    private AuctionAutomationMongoRepository(final String connectionString, final String databaseName,
                                             final String collectionName) {
        usersAuctionsAutomationsCollection = new MongoDatabaseManager(connectionString, databaseName)
                .getCollection(collectionName, UserAuctionsAutomationsWeb.class);
        commons = new CommonsMongoOperations<>(usersAuctionsAutomationsCollection,
                UserAuctionsAutomationsWeb::toDomain);
    }

    public static AuctionAutomationMongoRepository defaultRepository() {
        return new AuctionAutomationMongoRepository(AUCTIONS_AUTOMATIONS_CONNECTION_STRING,
                AUCTIONS_AUTOMATIONS_DB_NAME, AUCTIONS_AUTOMATIONS_COLLECTION_NAME);
    }

    public static AuctionAutomationMongoRepository testRepository() {
        return new AuctionAutomationMongoRepository(TEST_CONNECTION_STRING, TEST_DB_NAME,
                AUCTIONS_AUTOMATIONS_COLLECTION_NAME);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void create(final UserAuctionsAutomations auctionAutomation) {
        usersAuctionsAutomationsCollection.insertOne(UserAuctionsAutomationsWeb.fromDomain(auctionAutomation));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<UserAuctionsAutomations> deleteByUsername(final String username) {
        return commons.deleteOneByFilter(eq(USERNAME_FIELD, username));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<UserAuctionsAutomations> findByUsername(final String username) {
        return commons.findOneByFilter(eq(USERNAME_FIELD, username));
    }

    @Override
    public Set<UserAuctionsAutomations> getAllActiveAutomations() {
        return usersAuctionsAutomationsCollection.aggregate(List.of(
                new BsonDocument("$unwind", new BsonString("$auctionsAutomations")),
                new BsonDocument("$match", new BsonDocument("auctionsAutomations.enabled", new BsonBoolean(true))),
                new BsonDocument("$group", new BsonDocument()
                        .append("_id", new BsonString("$_id"))
                        .append("username", new BsonDocument("$first", new BsonString("$username")))
                        .append(AUCTIONS_AUTOMATIONS_FIELD, new BsonDocument("$push", new BsonString("$auctionsAutomations")))
                )), UserAuctionsAutomationsWeb.class)
                .into(new ArrayList<>()).stream()
                .map(UserAuctionsAutomationsWeb::toDomain)
                .collect(Collectors.toSet());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addAuctionAutomation(final AuctionAutomation auctionAutomation, final String username) {
        usersAuctionsAutomationsCollection.updateOne(eq(USERNAME_FIELD, username),
                push(AUCTIONS_AUTOMATIONS_FIELD, AuctionAutomationWeb.fromDomain(auctionAutomation)));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<AuctionAutomation> removeAuctionAutomation(final String auctionId, final String username) {
        return Optional.ofNullable(usersAuctionsAutomationsCollection.findOneAndUpdate(eq(USERNAME_FIELD, username),
                Updates.pullByFilter(new BsonDocument(AUCTIONS_AUTOMATIONS_FIELD,
                        new BsonDocument("auctionId", new BsonString(auctionId))))))
                .flatMap(userAuctionsAutomations -> userAuctionsAutomations.getAuctionsAutomations().stream()
                        .filter(auctionAutomation -> auctionAutomation.getAuctionId().equals(auctionId))
                        .findFirst()
                        .map(AuctionAutomationWeb::toDomain));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeAllAutomationsForAuction(final String auctionId) {
        usersAuctionsAutomationsCollection.updateMany(new BasicDBObject(),
                Updates.pullByFilter(new BsonDocument(AUCTIONS_AUTOMATIONS_FIELD,
                        new BsonDocument("auctionId", new BsonString(auctionId)))));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setEnableStatus(final boolean enabled, final String auctionId, final String username) {
        usersAuctionsAutomationsCollection.updateOne(and(
                eq(USERNAME_FIELD, username),
                eq("auctionsAutomations.auctionId", auctionId)
        ), set("auctionsAutomations.$.enabled", enabled));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<AuctionAutomation> findAutomationForAuction(final String auctionId, final String username) {
        return Optional.ofNullable(usersAuctionsAutomationsCollection.find(eq(USERNAME_FIELD, username))
                        .map(userAuctionsAutomationsWeb -> userAuctionsAutomationsWeb.getAuctionsAutomations().stream()
                                .filter(auctionAutomationWeb -> auctionAutomationWeb.getAuctionId().equals(auctionId))
                                .findFirst())
                        .first())
                .orElseGet(Optional::empty) // like a flatten
                .map(AuctionAutomationWeb::toDomain);
    }

    @Override
    public Set<AuctionAutomation> findAutomationsForAuction(final String auctionId) {
        return usersAuctionsAutomationsCollection.find(eq("auctionsAutomations.auctionId", auctionId))
                .projection(new BsonDocument(AUCTIONS_AUTOMATIONS_FIELD, new BsonDocument("$elemMatch",
                                new BsonDocument("auctionId", new BsonString(auctionId)))))
                .into(new ArrayList<>()).stream()
                .flatMap(userAuctionsAutomationsWeb -> userAuctionsAutomationsWeb.getAuctionsAutomations().stream())
                .map(AuctionAutomationWeb::toDomain)
                .collect(Collectors.toSet());
    }
}
