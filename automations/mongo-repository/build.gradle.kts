dependencies {
    implementation(project(":utils"))
    implementation(project(":mongo-utils"))
    implementation(project(":automations:domain"))
    implementation(project(":automations:usecase"))
    implementation(project(":automations:adapter"))

    implementation("org.mongodb:mongo-java-driver:3.12.10")
}