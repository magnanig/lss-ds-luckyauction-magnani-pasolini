dependencies {
    implementation(project(":configuration"))
    implementation(project(":utils"))
    implementation(project(":auctions:adapter"))
    implementation(project(":automations:adapter"))

    // socket.io server side
    implementation("com.corundumstudio.socketio:netty-socketio:1.7.19")
}