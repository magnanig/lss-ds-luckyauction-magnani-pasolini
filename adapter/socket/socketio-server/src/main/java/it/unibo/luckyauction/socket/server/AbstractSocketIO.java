/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.socket.server;

import com.corundumstudio.socketio.Configuration;
import com.corundumstudio.socketio.SocketIOServer;
import com.corundumstudio.socketio.Transport;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;

@SuppressWarnings("PMD.AbstractClassWithoutAbstractMethod")
public abstract class AbstractSocketIO<T extends AbstractSocketIO<T>> {

    private final SocketIOServer socket;

    private final Logger logger = System.getLogger(getClass().getName());

    protected AbstractSocketIO(final String host, final Integer port, final String namespace) {
        this(host, port);
        socket.addNamespace(namespace);
    }

    protected AbstractSocketIO(final String host, final Integer port) {
        final Configuration config = new Configuration();
        config.setHostname(host);
        config.setPort(port);
        config.setTransports(Transport.WEBSOCKET);
        socket = new SocketIOServer(config);
        attachListeners();
    }

    /**
     * Start the socket server.
     * @return this, for chaining
     */
    @SuppressWarnings("unchecked")
    public T start() {
        socket.start();
        return (T) this;
    }

    /**
     * Disconnect (i.e. stop) the socket server.
     */
    public void disconnect() {
        socket.stop();
    }

    /**
     * Add the specified namespace to the socket server.
     * @param namespace the namespace to add
     */
    protected void addNamespace(final String namespace) {
        socket.addNamespace(namespace);
        logger.log(Level.DEBUG, "Added namespace " + namespace);
    }

    /**
     * Remove the specified namespace from the socket server.
     * @param namespace the namespace to remove
     */
    protected void removeNamespace(final String namespace) {
        socket.removeNamespace(namespace);
        logger.log(Level.DEBUG, "Removed namespace " + namespace);
    }

    /**
     * Send the desired message on the specified namespace.
     * @param namespace the namespace to send the message on
     * @param topic     the topic of the message
     * @param args      the message to send
     */
    protected void sendToNamespace(final String namespace, final String topic, final Object... args) {
        logger.log(Level.DEBUG, "Sending message to namespace  " + namespace);
        socket.getNamespace(namespace).getBroadcastOperations().sendEvent(topic, args);
    }

    private void attachListeners() {
        socket.addConnectListener(client -> logger.log(Level.INFO, "Connected " + client.getRemoteAddress()
                + " on " + getClass().getSimpleName().replace("IO", "")));
        socket.addDisconnectListener(client -> logger.log(Level.INFO, "Disconnected "
                + client.getRemoteAddress() + " from " + getClass().getSimpleName().replace("IO", "")));
    }
}
