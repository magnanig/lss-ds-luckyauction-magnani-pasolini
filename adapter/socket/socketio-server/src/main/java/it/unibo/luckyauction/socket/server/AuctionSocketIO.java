/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.socket.server;

import it.unibo.luckyauction.auction.adapter.socket.AuctionEvent;
import it.unibo.luckyauction.auction.adapter.socket.AuctionSocket;

public class AuctionSocketIO extends AbstractSocketIO<AuctionSocketIO> implements AuctionSocket {

    private static final String NAMESPACE = "/auctions";

    public AuctionSocketIO(final String host, final Integer port) {
        super(host, port, NAMESPACE);
    }

    @Override
    public void addAuctionNamespace(final String auctionId) {
        addNamespace(NAMESPACE + "/" + auctionId);
    }

    @Override
    public void removeAuctionNamespace(final String auctionId) {
        removeNamespace(NAMESPACE + "/" + auctionId);
    }

    @Override
    public void send(final AuctionEvent auctionEvent) {
        sendToNamespace(NAMESPACE, auctionEvent.eventType().toString(), auctionEvent.extractArgs());
        sendToNamespace(NAMESPACE + "/" + auctionEvent.getAuctionId(), auctionEvent.eventType().toString(),
                auctionEvent.extractArgs());
    }

}
