/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.socket.server;

import it.unibo.luckyauction.auction.adapter.BidWeb;
import it.unibo.luckyauction.auction.adapter.socket.BidsSocket;

import java.util.Set;

import static it.unibo.luckyauction.configuration.SocketsConstants.DEFAULT_BID_SOCKET_BUCKET;
import static it.unibo.luckyauction.configuration.SocketsConstants.getBidSocketBucketId;

public class BidsSocketsIO extends AbstractSocketIO<BidsSocketsIO> implements BidsSocket {

    private static final String BASE_NAMESPACE = "/bids";
    private static final String BID_RAISED = "BID_RAISED";

    public BidsSocketsIO(final String host, final Integer port, final Set<Integer> socketBuckets) {
        super(host, port);
        socketBuckets.stream()
                .map(socketBucket -> BASE_NAMESPACE + "/" + socketBucket)
                .forEach(this::addNamespace);
    }

    public BidsSocketsIO(final String host, final Integer port) {
        this(host, port, Set.of(DEFAULT_BID_SOCKET_BUCKET));
    }

    @Override
    public void addAuctionNamespace(final String auctionId) {
        addNamespace(BASE_NAMESPACE + "/" + auctionId);
    }

    @Override
    public void removeAuctionNamespace(final String auctionId) {
        removeNamespace(BASE_NAMESPACE + "/" + auctionId);
    }

    @Override
    public void send(final BidWeb bid, final String auctionId) {
        sendToNamespace(BASE_NAMESPACE + "/" + getBidSocketBucketId(auctionId), BID_RAISED, bid, auctionId);
        sendToNamespace(BASE_NAMESPACE + "/" + auctionId, BID_RAISED, bid, auctionId);
    }

}
