/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.socket.server;

import it.unibo.luckyauction.automation.adapter.socket.AuctionAutomationEvent;
import it.unibo.luckyauction.automation.adapter.socket.AuctionAutomationSocket;

public class AuctionAutomationSocketIO extends AbstractSocketIO<AuctionAutomationSocketIO>
        implements AuctionAutomationSocket {

    private static final String AUCTION_AUTOMATION_NAMESPACE = "/automations";

    public AuctionAutomationSocketIO(final String host, final Integer port) {
        super(host, port, AUCTION_AUTOMATION_NAMESPACE);
    }

    @Override
    public void addAuctionAutomationNamespace(final String auctionAutomationId) {
        addNamespace(AUCTION_AUTOMATION_NAMESPACE + "/" + auctionAutomationId);
    }

    @Override
    public void removeAuctionAutomationNamespace(final String auctionAutomationId) {
        removeNamespace(AUCTION_AUTOMATION_NAMESPACE + "/" + auctionAutomationId);
    }

    @Override
    public void send(final AuctionAutomationEvent auctionAutomationEvent) {
        sendToNamespace(AUCTION_AUTOMATION_NAMESPACE, auctionAutomationEvent.eventType().toString(),
                auctionAutomationEvent.extractArgs());
    }

    @Override
    public void send(final AuctionAutomationEvent auctionAutomationEvent, final String auctionAutomationId) {
        sendToNamespace(AUCTION_AUTOMATION_NAMESPACE, auctionAutomationEvent.eventType().toString(),
                auctionAutomationEvent.extractArgs());
        sendToNamespace(AUCTION_AUTOMATION_NAMESPACE + "/" + auctionAutomationId,
                auctionAutomationEvent.eventType().toString(), auctionAutomationEvent.extractArgs());
    }
}
