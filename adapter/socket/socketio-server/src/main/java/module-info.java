module lss.ds.luckyauction.magnani.pasolini.socketio.server.main {
    exports it.unibo.luckyauction.socket.server;
    requires lss.ds.luckyauction.magnani.pasolini.configuration.main;
    requires lss.ds.luckyauction.magnani.pasolini.utils.main;
    requires lss.ds.luckyauction.magnani.pasolini.auctions.adapter.main;
    requires lss.ds.luckyauction.magnani.pasolini.automations.adapter.main;
    requires netty.socketio;
}