dependencies {
    implementation(project(":configuration"))
    implementation(project(":utils"))
    implementation(project(":auctions:adapter"))
    implementation(project(":automations:adapter"))

    implementation("io.socket:socket.io-client:1.0.0")
    implementation("com.google.code.gson:gson:2.9.0")
}