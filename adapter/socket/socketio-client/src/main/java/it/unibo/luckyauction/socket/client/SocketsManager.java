/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.socket.client;

import it.unibo.luckyauction.auction.adapter.BidWeb;
import it.unibo.luckyauction.util.CollectionsUtils;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

import static it.unibo.luckyauction.configuration.SocketsConstants.DEFAULT_BID_SOCKET_BUCKET;

public final class SocketsManager {

    private final String bidsHost;
    private final Integer bidsPort;
    private final BiConsumer<BidWeb, String> onBidRaised;
    private final AuctionAutomationSocketClient auctionAutomationSocket;
    private final Map<Integer, BidsSocketClient> bidsSockets;

    private SocketsManager(final String bidsHost, final String auctionAutomationHost,
                           final Integer auctionAutomationPort, final Integer bidsPort, final Set<Integer> bidsSocketIds,
                           final Consumer<String> onAuctionAutomationStop,
                           final Consumer<String> onAllAutomationsForAuctionStop,
                           final BiConsumer<BidWeb, String> onBidRaised) {
        this.bidsHost = bidsHost;
        this.bidsPort = bidsPort;
        this.onBidRaised = onBidRaised;
        this.auctionAutomationSocket = AuctionAutomationSocketClient.builder()
                .host(auctionAutomationHost)
                .port(auctionAutomationPort)
                .onAutomationStop(onAuctionAutomationStop)
                .onAllAutomationsForAuctionStop(onAllAutomationsForAuctionStop)
                .build();
        if (bidsSocketIds == null || bidsSocketIds.isEmpty()) {
            this.bidsSockets = new HashMap<>(Map.of(DEFAULT_BID_SOCKET_BUCKET,
                    new BidsSocketClient(bidsHost, bidsPort, onBidRaised)));
        } else {
            this.bidsSockets = bidsSocketIds.stream().collect(Collectors.toMap(Function.identity(),
                    socketId -> new BidsSocketClient(bidsHost, bidsPort, socketId, onBidRaised)));
        }
    }

    public void connect() {
        bidsSockets.values().forEach(BidsSocketClient::connect);
        auctionAutomationSocket.connect();
    }

    public void disconnect() {
        bidsSockets.values().forEach(BidsSocketClient::disconnect);
    }

    public void addBidSocketAndConnect(final Integer socketId) {
        final BidsSocketClient socket = new BidsSocketClient(bidsHost, bidsPort, socketId, onBidRaised);
        bidsSockets.put(socketId, socket);
        socket.connect();
    }

    public void setBidsSocketsAndConnect(final Set<Integer> bidsSocketsIds) {
        bidsSockets.entrySet().removeIf(entry -> {
            if (!bidsSocketsIds.contains(entry.getKey())) {
                entry.getValue().disconnect();
                return true;
            }
            return false;
        });
        CollectionsUtils.difference(bidsSocketsIds, bidsSockets.keySet())
                .forEach(this::addBidSocketAndConnect);
    }

    public void removeBidSocketAncDisconnect(final Integer socketId) {
        Optional.ofNullable(bidsSockets.remove(socketId)).ifPresent(BidsSocketClient::disconnect);
    }


    @SuppressWarnings({"PMD.AvoidFieldNameMatchingMethodName", "checkstyle:DesignForExtension"})
    public static class Builder {

        private String bidsHost;
        private String auctionAutomationHost;
        private Integer auctionAutomationPort;
        private Integer bidsPort;
        private Set<Integer> socketsIds;
        private Consumer<String> onAuctionAutomationStop;
        private BiConsumer<BidWeb, String> onBidRaised;
        private Consumer<String> onAllAutomationsForAuctionStop;

        public Builder bidsHost(final String bidsHost) {
            this.bidsHost = bidsHost;
            return this;
        }

        public Builder auctionAutomationHost(final String auctionAutomationHost) {
            this.auctionAutomationHost = auctionAutomationHost;
            return this;
        }

        public Builder auctionAutomationPort(final Integer auctionAutomationPort) {
            this.auctionAutomationPort = auctionAutomationPort;
            return this;
        }

        public Builder bidsPort(final Integer bidsPort) {
            this.bidsPort = bidsPort;
            return this;
        }

        public Builder socketsBucketsIds(final Set<Integer> socketsBucketsIds) {
            this.socketsIds = Collections.unmodifiableSet(socketsBucketsIds);
            return this;
        }

        public Builder onAuctionAutomationStop(final Consumer<String> onAuctionAutomationStop) {
            this.onAuctionAutomationStop = onAuctionAutomationStop;
            return this;
        }

        public Builder onAllAutomationsForAuctionStop(final Consumer<String> onAllAutomationsForAuctionStop) {
            this.onAllAutomationsForAuctionStop = onAllAutomationsForAuctionStop;
            return this;
        }

        public Builder onBidRaised(final BiConsumer<BidWeb, String> onBidRaised) {
            this.onBidRaised = onBidRaised;
            return this;
        }

        public SocketsManager build() {
            return new SocketsManager(bidsHost, auctionAutomationHost, auctionAutomationPort, bidsPort,
                    socketsIds, onAuctionAutomationStop, onAllAutomationsForAuctionStop, onBidRaised);
        }
    }

}
