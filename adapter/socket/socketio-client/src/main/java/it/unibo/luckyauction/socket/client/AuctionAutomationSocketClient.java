/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.socket.client;

import com.google.gson.Gson;
import it.unibo.luckyauction.automation.adapter.AuctionAutomationWeb;
import it.unibo.luckyauction.socket.util.JsonWithDateParser;
import it.unibo.luckyauction.automation.adapter.socket.Token;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

public final class AuctionAutomationSocketClient extends SocketClient {

    private static final String AUCTION_AUTOMATION_NAMESPACE = "/automations";
    private static final String CREATE_AUTOMATION = "CREATE_AUTOMATION";
    private static final String STOP_AUTOMATION = "STOP_AUTOMATION";
    private static final String STOP_ALL_AUTOMATIONS = "STOP_ALL_AUTOMATIONS";

    private AuctionAutomationSocketClient(final String host, final Integer port, final Runnable onSocketConnected,
                                          final BiConsumer<AuctionAutomationWeb, Token> onAutomationCreation,
                                          final Consumer<String> onAutomationStop,
                                          final Consumer<String> onAllAutomationsForAuctionStop) {
        super(host, port, AUCTION_AUTOMATION_NAMESPACE, onSocketConnected);
        final Gson jsonParser = JsonWithDateParser.buildParser();
        if (onAutomationCreation != null) {
            getSocket().on(CREATE_AUTOMATION, args -> onAutomationCreation.accept(
                    jsonParser.fromJson(args[0].toString(), AuctionAutomationWeb.class),
                    jsonParser.fromJson(args[1].toString(), Token.class))
            );
        }

        if (onAutomationStop != null) {
            getSocket().on(STOP_AUTOMATION, args -> onAutomationStop.accept(args[0].toString()));
        }

        if (onAllAutomationsForAuctionStop != null) {
            getSocket().on(STOP_ALL_AUTOMATIONS, args -> onAllAutomationsForAuctionStop.accept(args[0].toString()));
        }
    }

    public static Builder builder() {
        return new Builder();
    }

    @SuppressWarnings("PMD.AvoidFieldNameMatchingMethodName")
    public static final class Builder {

        private String host;
        private Integer port;
        private BiConsumer<AuctionAutomationWeb, Token> onAutomationCreation;
        private Consumer<String> onAutomationStop;
        private Consumer<String> onAllAutomationsForAuctionStop;
        private Runnable onSocketConnected;

        private Builder() { }

        public Builder host(final String host) {
            this.host = host;
            return this;
        }

        public Builder port(final Integer port) {
            this.port = port;
            return this;
        }

        public Builder onAutomationCreation(final BiConsumer<AuctionAutomationWeb, Token> onAutomationCreation) {
            this.onAutomationCreation = onAutomationCreation;
            return this;
        }

        public Builder onSocketConnected(final Runnable onSocketConnected) {
            this.onSocketConnected = onSocketConnected;
            return this;
        }

        public Builder onAutomationStop(final Consumer<String> onAutomationStop) {
            this.onAutomationStop = onAutomationStop;
            return this;
        }

        public Builder onAllAutomationsForAuctionStop(final Consumer<String> onAllAutomationsForAuctionStop) {
            this.onAllAutomationsForAuctionStop = onAllAutomationsForAuctionStop;
            return this;
        }

        public AuctionAutomationSocketClient build() {
            return new AuctionAutomationSocketClient(host, port, onSocketConnected, onAutomationCreation,
                    onAutomationStop, onAllAutomationsForAuctionStop);
        }

    }
}
