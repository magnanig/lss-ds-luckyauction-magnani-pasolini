/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.socket.client;

import com.google.gson.Gson;
import it.unibo.luckyauction.auction.adapter.BidWeb;
import it.unibo.luckyauction.socket.util.JsonWithDateParser;

import java.util.function.BiConsumer;

import static it.unibo.luckyauction.configuration.SocketsConstants.DEFAULT_BID_SOCKET_BUCKET;

public class BidsSocketClient extends SocketClient {

    private static final String BIDS_BASE_NAMESPACE = "/bids";
    private static final String BID_RAISED = "BID_RAISED";

    public BidsSocketClient(final String host, final Integer port, final Integer bidSocketBucket,
                            final BiConsumer<BidWeb, String> onBidRaised) {
        super(host, port, BIDS_BASE_NAMESPACE + "/" + bidSocketBucket);
        final Gson jsonParser = JsonWithDateParser.buildParser();
        getSocket().on(BID_RAISED, args -> onBidRaised.accept(
                jsonParser.fromJson(args[0].toString(), BidWeb.class), args[1].toString()));
    }

    public BidsSocketClient(final String host, final Integer port, final BiConsumer<BidWeb, String> onBidRaised) {
        this(host, port, DEFAULT_BID_SOCKET_BUCKET, onBidRaised);
    }
}
