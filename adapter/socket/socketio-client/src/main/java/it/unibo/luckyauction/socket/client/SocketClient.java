/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.socket.client;

import io.socket.client.IO;
import io.socket.client.Socket;
import java.lang.System.Logger;

import java.net.URI;

public class SocketClient {

    private final String host;
    private final Integer port;
    private final String namespace;
    private final Socket socket;

    protected SocketClient(final String host, final Integer port, final String namespace) {
        this(host, port, namespace, () -> { });
    }

    protected SocketClient(final String host, final Integer port, final String namespace, final Runnable onConnect) {
        this.host = host;
        this.port = port;
        this.namespace = namespace;
        this.socket = createSocket(host, port, namespace, onConnect);
    }

    /**
     * Conncet the socket to the server.
     */
    public void connect() {
        socket.connect();
    }

    /**
     * Disconnect the socket from the server.
     */
    public void disconnect() {
        socket.disconnect();
    }

    /**
     * Get the host on which the socket is connected.
     */
    public String getHost() {
        return host;
    }

    /**
     * Get the port on which the socket is connected.
     */
    public Integer getPort() {
        return port;
    }

    /**
     * Get the namespace of the socket.
     */
    public String getNamespace() {
        return namespace;
    }

    /**
     * Get the socket instance.
     */
    protected Socket getSocket() {
        return socket;
    }

    @SuppressWarnings("checkstyle:MagicNumber")
    private Socket createSocket(final String host, final Integer port, final String namespace, final Runnable onConnect) {
        final Logger logger = System.getLogger(SocketClient.class.getName());
        final IO.Options options = new IO.Options();
        options.transports = new String[]{ "websocket" };
        options.reconnectionAttempts = 50;
        options.reconnectionDelay = 1000;
        options.timeout = 500;

        final String uri = String.format("http://%s:%d%s", host, port, namespace);
        final Socket socket = IO.socket(URI.create(uri), options);
        socket.on(Socket.EVENT_ERROR, args -> logger.log(Logger.Level.ERROR, "Error while connecting on socket "
                + uri + ": " + args[0]));
        socket.on(Socket.EVENT_CONNECT, args -> {
            logger.log(Logger.Level.INFO, "Connected to server " + namespace);
            if (onConnect != null) {
                onConnect.run();
            }
        });
        socket.on(Socket.EVENT_DISCONNECT, args -> logger.log(Logger.Level.INFO, "Disconnected from server "
                + namespace));
        return socket;
    }

}
