module lss.ds.luckyauction.magnani.pasolini.socketio.client.main {
    exports it.unibo.luckyauction.socket.client;
    requires lss.ds.luckyauction.magnani.pasolini.configuration.main;
    requires lss.ds.luckyauction.magnani.pasolini.utils.main;
    requires lss.ds.luckyauction.magnani.pasolini.auctions.adapter.main;
    requires lss.ds.luckyauction.magnani.pasolini.automations.adapter.main;
    requires socket.io.client;
    requires engine.io.client;
    requires com.google.gson;
}