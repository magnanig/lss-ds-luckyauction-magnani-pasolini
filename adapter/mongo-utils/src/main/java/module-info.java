module lss.ds.luckyauction.magnani.pasolini.mongo.utils.main {
    exports it.unibo.luckyauction.repository.util;
    requires mongo.java.driver;
    requires java.logging;
}