/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.repository.util;

public final class MongoConnectionsConstants {

    // assure both MONGO_USERNAME and MONGO_PASSWORD environment variables are properly set
    static {
        if (System.getenv("MONGO_USERNAME") == null) {
            throw new RuntimeException("MONGO_USERNAME environment variable is not set");
        }
        if (System.getenv("MONGO_PASSWORD") == null) {
            throw new RuntimeException("MONGO_PASSWORD environment variable is not set");
        }
    }

    private static final String HOST = "mongodb+srv://" + System.getenv("MONGO_USERNAME") + ":"
            + System.getenv("MONGO_PASSWORD") + "@cluster0.fgmc9.mongodb.net/";
    private static final String SETTINGS = "?retryWrites=true&w=majority";

    /**
     * The name of the user database.
     */
    public static final String USERS_DB_NAME = "la-users";

    /**
     * The name of the user authentication database.
     */
    public static final String USERS_AUTH_DB_NAME = "la-users";

    /**
     * The name of the movement database.
     */
    public static final String MOVEMENTS_DB_NAME = "la-movements";

    /**
     * The name of the auction database.
     */
    public static final String AUCTIONS_DB_NAME = "la-auctions";

    /**
     * The name of the auction history database.
     */
    public static final String AUCTIONS_HISTORIES_DB_NAME = "la-auctions"; // ok, in same database for auctions

    /**
     * The name of the auction automations database.
     */
    public static final String AUCTIONS_AUTOMATIONS_DB_NAME = "la-automations";

    /**
     * The name of the test database.
     */
    public static final String TEST_DB_NAME = "test";



    /**
     * The name of the user collection within the user database.
     */
    public static final String USERS_COLLECTION_NAME = "users";

    /**
     * The name of the user authentication collection within the user database.
     */
    public static final String USERS_AUTH_COLLECTION_NAME = "users-auth";

    /**
     * The name of the movement collection within the movement database.
     */
    public static final String MOVEMENTS_COLLECTION_NAME = "movements";

    /**
     * The name of the auction collection within the auction database.
     */
    public static final String AUCTIONS_COLLECTION_NAME = "auctions";

    /**
     * The name of the auction history collection within the auction database.
     */
    public static final String AUCTIONS_HISTORIES_COLLECTION_NAME = "histories";

    /**
     * The name of the auction automations collection within the auction automations database.
     */
    public static final String AUCTIONS_AUTOMATIONS_COLLECTION_NAME = "automations";



    /**
     * The connection string for the user database.
     */
    public static final String USERS_CONNECTION_STRING = HOST + USERS_DB_NAME + SETTINGS;

    /**
     * The connection string for the user authentication database.
     */
    public static final String USERS_AUTH_CONNECTION_STRING = HOST + USERS_AUTH_DB_NAME + SETTINGS;

    /**
     * The connection string for the movement database.
     */
    public static final String MOVEMENTS_CONNECTION_STRING = HOST + MOVEMENTS_DB_NAME + SETTINGS;

    /**
     * The connection string for the auction automations database.
     */
    public static final String AUCTIONS_HISTORIES_CONNECTION_STRING = HOST + AUCTIONS_HISTORIES_DB_NAME + SETTINGS;

    /**
     * The connection string for the auction database.
     */
    public static final String AUCTIONS_CONNECTION_STRING = HOST + AUCTIONS_DB_NAME + SETTINGS;

    /**
     * The connection string for the test database.
     */
    public static final String TEST_CONNECTION_STRING = HOST + SETTINGS;

    /**
     * The connection string for the auction automations database.
     */
    public static final String AUCTIONS_AUTOMATIONS_CONNECTION_STRING = HOST + AUCTIONS_AUTOMATIONS_DB_NAME + SETTINGS;

    private MongoConnectionsConstants() { }
}
