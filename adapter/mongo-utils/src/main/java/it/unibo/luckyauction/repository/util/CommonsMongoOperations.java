/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.repository.util;

import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoCollection;
import org.bson.conversions.Bson;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * A MongoDB wrapper for common operations on a collection.
 * @param <W>   the type of the wrapped MongoDB collection, i.e. the Web version
 *              (or adapter)
 * @param <D>   the domain type corresponding to such wrapped collection,
 *              i.e. the domain version)
 * <br>
 * Example: <br>
 * <code>W</code> can be <code>UserWeb</code> and <code>D</code> can be <code>User</code>.
 */
public class CommonsMongoOperations<W, D> {

    private final MongoCollection<W> mongoCollection;
    private final Function<W, D> mapper;

    /**
     * Create a new instance to manage common operations on the given collection.
     * @param mongoCollection    the MongoDB collection to wrap
     * @param mapper             the mapper to apply to the wrapped collection in order
     *                           to map it to the domain type
     */
    public CommonsMongoOperations(final MongoCollection<W> mongoCollection, final Function<W, D> mapper) {
        this.mongoCollection = mongoCollection;
        this.mapper = mapper;
    }

    /**
     * Find the first document matching the filter.
     * @param filter    the filter to apply to look for the document
     * @return          the first document matching the filter, if any
     */
    public Optional<D> findOneByFilter(final Bson filter) {
        return Optional.ofNullable(mongoCollection.find(filter).first())
                .map(mapper);
    }

    /**
     * Find all documents matching the filter.
     * @param filter    the filter to apply to look for the documents
     * @param mapper    the mapper to apply to matching documents
     * @param <T>       the desired type of the matching documents
     * @return          the list of matching documents, properly mapped to the
     *                  desired type
     */
    public <T> List<T> findManyByFilter(final Bson filter, final Function<W, T> mapper) {
        return mongoCollection.find(filter)
                .into(new ArrayList<>()).stream()
                .map(mapper)
                .collect(Collectors.toList());
    }

    /**
     * Find all documents matching the filter.
     * @param filter    the filter to apply to look for the documents
     * @return          the list of matching documents
     */
    public List<D> findManyByFilter(final Bson filter) {
        return findManyByFilter(filter, mapper);
    }

    /**
     * Find all documents in the collection.
     * @return          the list of all documents in the collection
     */
    public List<D> findMany() {
        return findManyByFilter(new BasicDBObject(), mapper);
    }

    /**
     * Delete the first document matching the filter.
     * @param filter    the filter to apply to look for the document
     * @return          the deleted document, if any
     */
    public Optional<D> deleteOneByFilter(final Bson filter) {
        return Optional.ofNullable(mongoCollection.findOneAndDelete(filter))
                .map(mapper);
    }
}
