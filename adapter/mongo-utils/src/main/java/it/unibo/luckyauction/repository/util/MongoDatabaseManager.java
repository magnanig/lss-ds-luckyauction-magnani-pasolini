/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.repository.util;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientSettings;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.codecs.configuration.CodecRegistries;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;

import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class is used to manage the connection to the MongoDB database,
 * with codecs to encode and decode POJO objects.
 */
public class MongoDatabaseManager {

    private final MongoDatabase database;
    private final Logger logger;

    /**
     * Create a new MongoDB manager for the desired database.
     * @param connectionString    the connection string to the MongoDB database
     * @param databaseName        the name of the database
     */
    public MongoDatabaseManager(final String connectionString, final String databaseName) {
        this(connectionString, databaseName, PojoCodecProvider.builder().automatic(true).build());
    }

    /**
     * Create a new MongoDB manager for the desired database.
     * @param connectionString      the connection string to the MongoDB database
     * @param databaseName          the name of the database
     * @param pojoCodecProvider     the codec provider to encode and decode POJO objects
     */
    @SuppressWarnings("PMD.CloseResource") // connections pool managed by mongo client
    public MongoDatabaseManager(final String connectionString, final String databaseName,
                                final PojoCodecProvider pojoCodecProvider) {
        final MongoClient mongoClient = new MongoClient(new MongoClientURI(connectionString));
        final CodecRegistry codecRegistries = CodecRegistries.fromRegistries(
                MongoClientSettings.getDefaultCodecRegistry(),
                CodecRegistries.fromProviders(pojoCodecProvider)
        );
        this.database = mongoClient.getDatabase(databaseName).withCodecRegistry(codecRegistries);
        this.logger = Logger.getLogger("org.mongodb.driver");
        configLogger();
    }

    /**
     * Get the collection with the given name.
     * @param collectionName    the name of the collection
     * @param documentClass     the class of the documents in the collection
     * @param <T>               the type of the documents in the collection
     * @return                  the MongoDB collection
     */
    public <T> MongoCollection<T> getCollection(final String collectionName, final Class<T> documentClass) {
        return database.getCollection(collectionName, documentClass);
    }

    private void configLogger() {
        logger.setLevel(Level.WARNING); // outputs only from warnings
        Arrays.stream(logger.getHandlers()).forEach(handler -> handler.setLevel(Level.WARNING));
    }
}
