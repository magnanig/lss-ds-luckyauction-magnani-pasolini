module lss.ds.luckyauction.magnani.pasolini.sha256encoder.main {
    exports it.unibo.luckyauction.encoder;
    requires org.apache.commons.codec;
    requires lss.ds.luckyauction.magnani.pasolini.utils.main;
}