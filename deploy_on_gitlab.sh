#!/bin/bash

#
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#


# Since GitLab Releases has a limit of only 10 MB for file upload,
# the for loop over jars has been disabled (note the .xar extension
# at the end of pattern). If you want to enable it, replace the .xar
# extension with .jar.

if [ "$(git tag -l "$VERSION")" ]; then
    echo "Version $VERSION has already been deployed. Skipping this stage"
    exit 0
fi

git tag -a "$VERSION" -m "Version $VERSION"
git push origin "$VERSION"

# upload all needed jar(s) to GitLab, and save the links to each file
links="[";
shopt -s nullglob # in order to skip for loop if no file is found
for file in docker-compose.yml \
  {{users,movements,auctions,automations}/*-application,gateway}/build/libs/luckyauction*[[:digit:]].xar ; do # replace xar with jar

  echo "Uploading $file to GitLab"
  response=$(curl -s --request POST --header "PRIVATE-TOKEN: $GITLAB_TOKEN" \
    --form "file=@$file" "https://gitlab.com/api/v4/projects/35229850/uploads")
  if [ "$response" = "null" ]; then
    echo "Failed to upload $file to GitLab"
    exit 1
  else
    name=$(echo "$response" | jq -r '.alt')
    url="https://gitlab.com$(echo "$response" | jq -r '.url')"
    echo "File $file ($name) uploaded to $url"
    links="$links{\"name\": \"$name\", \"url\": \"$url\"},"
  fi
done

# remove last comma from links
links=${links%?}

if [ -z "$links" ]; then  # check if links are empty
  echo "No files to upload. Going to create an empty release"
  curl --header 'Content-Type: application/json' --header "PRIVATE-TOKEN: $GITLAB_TOKEN" \
         --data "{ \"tag_name\": \"$VERSION\" }" \
         --request POST "https://gitlab.com/api/v4/projects/35229850/releases"
else
  links="$links]"
  echo "Going to create release with links to the jar(s)"
  curl --header 'Content-Type: application/json' --header "PRIVATE-TOKEN: $GITLAB_TOKEN" \
       --data "{ \"tag_name\": \"$VERSION\", \"assets\": { \"links\": $links } }" \
       --request POST "https://gitlab.com/api/v4/projects/35229850/releases"
fi
