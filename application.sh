#!/bin/bash

#
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#

# Start or stop the whole application
# Usage:
# ./application.sh [start [interruptionTime] | stop]"
# (interruptionTime in format such as 2022-01-01T09:30:00)

mkdir -p logs

# check if first argument is "run" or "start" (or empty)
if [ $# -eq 0 ] || [ "$1" = "run" ] || [ "$1" = "start" ]; then
  echo "Starting all processes. Please wait for the confirm message..."
  exec -a la-users java -jar users/spring-application/build/libs/luckyauction*.jar </dev/null &>logs/users.log &
  LA_PIDS=$!
  exec -a la-movements java -jar movements/spring-application/build/libs/luckyauction*.jar </dev/null &>logs/movements.log &
  LA_PIDS="$LA_PIDS $!"
  exec -a la-automations java -jar automations/spring-application/build/libs/luckyauction*.jar </dev/null &>logs/automations.log &
  LA_PIDS="$LA_PIDS $!"
  # barrier synchronization
  ./wait-for-it.sh localhost:8081 -t 60 -q -- sh ./wait-for-it.sh localhost:8082 -t 60 -q -- sh ./wait-for-it.sh localhost:8084 -s -q -- echo "[40%] Users, Movements and Automations are up..."
  if [ $# -lt 2 ]; then
    exec -a la-auctions java -jar auctions/spring-application/build/libs/luckyauction*.jar </dev/null &>logs/auctions.log &
    LA_PIDS="$LA_PIDS $!"
  else
    exec -a la-auctions java -jar auctions/spring-application/build/libs/luckyauction*.jar "$2" </dev/null &>logs/auctions.log &
    LA_PIDS="$LA_PIDS $!"
  fi
  ./wait-for-it.sh localhost:8083 -t 120 -q -s -- echo "[60%] Auctions is up..."
  exec -a la-akka java -jar automations/akka-application/build/libs/luckyauction*-all.jar </dev/null &>logs/automations-akka.log &
  LA_PIDS="$LA_PIDS $!"
  exec -a la-gateway java -jar gateway/build/libs/luckyauction*.jar </dev/null &>logs/gateway.log &
  LA_PIDS="$LA_PIDS $!"
  ./wait-for-it.sh localhost:8080 -t 120 -q -s -- echo "[85%] Akka and Gateway are up..."
  cd web-app || (echo "web-app not found" && exit 1)
  exec -a la-web node app.js &>../logs/web-app.log &
  LA_PIDS="$LA_PIDS $!"
  cd ..
  ./wait-for-it.sh localhost:3000 -t 120 -q -s -- echo "[100%] Web app is up"
  export LA_PIDS
  echo "$LA_PIDS" > LA_PIDS
  echo "Complete. Started processes [$LA_PIDS]"
elif [ "$1" = "stop" ] || [ "$1" = "end" ] || [ "$1" = "close" ]; then
  # check if LA_PIDS variable is not set
  if [ -z "$LA_PIDS" ]; then
    # check if LA_PIDS file exists
    if [ -f "LA_PIDS" ]; then
      LA_PIDS=$(cat LA_PIDS)
    else
      echo "LA_PIDS not found. Stop manually background processes"
      exit 1
    fi
  fi
  # shellcheck disable=SC2086,SC2116,SC2046 # --> it must be split to use the kill command
  kill -9 $(echo $LA_PIDS)
  echo "All processes stopped: [$LA_PIDS]"
  unset LA_PIDS
  rm LA_PIDS
fi
