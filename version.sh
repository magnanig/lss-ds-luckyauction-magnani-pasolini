#!/bin/bash

#
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#


# Git Semantic Versioning (for Travis CI)

# Computes the next version (i.e. tag) for the last commit,
# starting from the last stable version. If a version is
# already set, simply returns that; otherwise, takes the last
# stable version (default 0.1.0), adds the current branch name
# and the date of the last  commit. So, the result will be in
# the following form: <stable version>-<branch name>-<timestamp>
# (e.g. 1.2.3-domain-20211015T123056).

# By default, the resulting version will be exported into the
# VERSION variable, so remember to call the script in your current
# shell (by running: source version.sh or . version.sh).
# If you want the version printed in standard output, call this
# script with the first argument set to 'echo' (i.e. ./version.sh echo).

VERSION=$(git describe --tags --abbrev=0 --candidates=0 2>/dev/null) # get the last commit's tag, if any
if [[ $VERSION == "" ]]; then # last commit is no tagged; let's compute its version
  # take the first tag part (until "-"). If no tag is found, use 0.1.0
  stable_version=$( (git describe --tags --abbrev=0 2>/dev/null || echo "0.1.0") | sed 's/-.*//' )
  timestamp=$(TZ=UTC0 git log -n1 --date=format-local:'%Y%m%dT%H%M%S' --format=%cd) # last commit date time
  VERSION="$stable_version-$TRAVIS_BRANCH-$timestamp"
fi

if [[ $# -gt 0 && $1 == "echo" ]]; then
  echo "$VERSION"
else
  export VERSION
fi
