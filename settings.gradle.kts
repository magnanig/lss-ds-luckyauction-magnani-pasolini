rootProject.name = "lss-ds-luckyauction-magnani-pasolini"

include("utils")
include("gateway")
include("configuration")

configureSubProject("utils:spring-utils")

configureSubProject("users:domain")
configureSubProject("users:usecase")
configureSubProject("users:controller")
configureSubProject("users:adapter")
configureSubProject("users:mongo-repository")
configureSubProject("users:spring-application")

configureSubProject("movements:domain")
configureSubProject("movements:usecase")
configureSubProject("movements:controller")
configureSubProject("movements:adapter")
configureSubProject("movements:mongo-repository")
configureSubProject("movements:spring-application")

configureSubProject("auctions:domain")
configureSubProject("auctions:usecase")
configureSubProject("auctions:controller")
configureSubProject("auctions:adapter")
configureSubProject("auctions:mongo-repository")
configureSubProject("auctions:memory-repository")
configureSubProject("auctions:spring-application")

configureSubProject("automations:domain")
configureSubProject("automations:usecase")
configureSubProject("automations:controller")
configureSubProject("automations:adapter")
configureSubProject("automations:mongo-repository")
configureSubProject("automations:spring-application")
configureSubProject("automations:akka-application")

configureProjectInPath("mongo-utils", "adapter/mongo-utils")
configureProjectInPath("uuid-generator", "adapter/uuid-generator")
configureProjectInPath("sha256encoder", "adapter/encoder/sha256")
configureProjectInPath("socketio-server", "adapter/socket/socketio-server")
configureProjectInPath("socketio-client", "adapter/socket/socketio-client")

fun configureSubProject(fullName: String) {
    include(fullName)
    findProject(fullName)?.name = fullName.substringAfterLast(":")
}

fun configureProjectInPath(name: String, path: String) {
    include(name)
    val project = findProject(":$name")
    project?.projectDir = file(path)
    project?.name = name
}
