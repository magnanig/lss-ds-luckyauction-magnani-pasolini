# LSS-DS LuckyAuction Magnani Pasolini

Project of "Laboratorio di Sistemi Software" and "Sistemi Distribuiti" about auctions distributed system, based on micro-services.

## Modularizzazione
L'applicazione è organizzata a moduli, che rispecchiano i _bounded context_ definiti in fase di analisi, ovvero **Users**, **Movements**, **Auctions** e **Automations**. Troviamo poi alcuni moduli extra che permettono all'applicazione di funzionare, in particolare si citano il **Gateway** e uno relativo alla **WebApp**, che consente una comoda fruizione del sistema realizzato. Le dipendenze tra questi moduli sono mostrate nella figura 1, dove più che dipendenze vere e proprie si può parlare di relazioni di tipo _expose_. In particolare, prendendo come esempio il Gateway, osserviamo come questo si connetta alle API esposte dai microservizi (bounded context) citati in precedenza. Dunque, all'interno del Gateway (ma anche negli altri moduli citati) non sono mai presenti riferimenti a classi appartenenti ad altri moduli, ma le comunicazioni avvengono sempre tramite API ReST (motivo per cui troviamo la relazione stereotipata con _expose_ nel diagramma). L'unica eccezione riguarda i moduli inerenti alle **Socket**, le cui classi vengono effettivamente utilizzate all'interno dei bounded context interessati (dipendenza vera e propria).
<div align="center">
    <figure>
        <img src="./report/resources/modules.jpg" alt="modules" width="700"/>
        <figcaption>Figura 1 - Diagramma dei package rappresentante l'organizzazione dei moduli principali che compongono l'applicazione.</figcaption>
    </figure>
</div>   

## Modularizzazione dei bounded context
Ciascuno dei bounded context mostrati precedentemente è poi organizzato internamente secondo la visione della [clean architecture](./report/report.md#clean-architecture), pertanto tale struttura deve essere mantenuta anche a livello di codice. A tal fine, si è scelto di creare un modulo per ciascun livello dell'architettura, al cui interno saranno poi presenti le classi necessarie al suo corretto funzionamento. Per capire meglio quali siano i diversi moduli e quali le loro dipendenze, ci si avvale del **diagramma dei package** mostrato in figura 2. Questa organizzazione è stata applicata allo stesso modo per ogni bounded context.

<div align="center">
    <figure>
        <img src="./report/resources/packages.svg" alt="packages" width="400"/>
        <figcaption>Figura 2 - Diagramma dei package rappresentante l'organizzazione adottata per ciascun bounded context.</figcaption>
    </figure>
</div>

Il modulo **Domain** contiene le classi che rappresentano il dominio, ovvero quegli elementi che compongono il cuore del bounded context; **UseCase** i casi d'uso individuati in fase di knowledge crunching, i quali dipendono dal dominio; il **Controller** orchestra i diversi casi d'uso, al fine di raggiungere l'obiettivo desiderato; infine, più esternamente troviamo il livello applicativo e di persistenza, ovvero **SpringApplication** e **MongoRepository**, che si appoggiano sul modulo **Adapter** per la conversione da/verso il dominio. SpringApplication riceve le richieste HTTP e le inoltra al controller, motivo per cui dipende da quest'ultimo, mentre MongoRepository implementa i metodi dei repository definiti nei casi d'uso.

Per maggiori dettagli si rimanda al report completo raggiungibile cliccando [qui](report/report.md).
