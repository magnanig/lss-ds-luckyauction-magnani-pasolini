# Autovalutazione e convalida
Vista la natura del progetto, sin dall'inizio è sembrato opportuno adottare una serie di strumenti che durante l'intero ciclo di sviluppo aiutassero a mantenere un buon livello di **qualità del codice** realizzato. A tal fine, come già specificato nella sezione relativa alla [build automation](#controllo-di-qualità), sono stati adottati i seguenti plugin: **CheckStyle**, **PMD** e **SpotBugs**. A ogni push, la pipeline della Continuous Integration verifica che il codice soddisfi sempre la qualità specificata nelle configurazioni di tali plugin, facendo fallire la build qualora ciò non avvenga (con il conseguente invio di una email).

Inoltre, per garantire l'**efficacia** dei risultati del progetto, sono stati sviluppati numerosi test. Nello specifico, sono stati realizzati due tipologie di test: **unit test** e **integration test**. I primi, sono stati impiegati per testare singolarmente i livelli di dominio, casi d'uso e persistenza (di ogni contesto). Mentre i secondi sono stati realizzati a livello di controller per testare la coordinazione dei vari componenti del singolo bounded context, nonché eventuali interazioni con altri micro-servizi.

Come già descritto nella sezione di [build automation](#test-con-coverage), a supporto della qualità dei test realizzati, è stato adottato il plugin JaCoCo il quale consente di analizzare la copertura dei test (**coverage**). Nel caso di questo progetto, si è deciso di impostare una copertura minima dell'80%.

I test totali realizzati nei diversi microservizi sono pari a 261, con una coverage media del 96.1%. La tabella seguente mostra un riepilogo dettagliato del numero di test effettuati in ciascun modulo (i.e. incrocio riga-colonna), riportando tra parentesi la relativa percentuale di coverage raggiunta.

|                 | **domain** | **usecase** | **controller** | **mongo-repository** |
|:----------------|:----------:|:-----------:|:--------------:|:--------------------:|
| **users**       | 35 (100%)  |  5 (100%)   |    4 (99%)     |       3 (89%)        |
| **movements**   | 37 (100%)  |   6 (98%)   |    6 (99%)     |       7 (96%)        |
| **auctions**    |  65 (96%)  |  22 (95%)   |    10 (86%)    |       34 (95%)       |
| **automations** |  7 (98%)   |  6 (100%)   |    8 (90%)     |       6 (97%)        |

Oltre a questi, sono presenti ulteriori 13 test relativi alle funzioni di utility presenti nel rispettivo modulo. Dunque, in totale sono stati realizzati 274 test.

Infine, grazie alle **Condition of Satisfaction** (CoS) definite durante la sessione di knowledge crunching, associate a tutti i diversi scenari di utilizzo, è possibile stabilire un criterio per la valutazione del software prodotto e la sua conformità ai requisiti. Nello specifico, mediante il prototipo di front-end realizzato è possibile verificare l'assolvimento di tutte le diverse CoS e quindi il raggiungimento degli obiettivi richiesti.