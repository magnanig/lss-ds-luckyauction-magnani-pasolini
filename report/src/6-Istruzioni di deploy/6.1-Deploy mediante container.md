## Deploy mediante container
La soluzione più semplice per effettuare il deploy dell'intera applicazione (i.e. tutti i diversi microservizi) è quella che sfrutta i container **Docker**. Qui, ogni microservizio è in esecuzione su un container diverso, ed è pertanto necessario scaricare tutte le rispettive immagini da DockerHub e mandarle in esecuzione sulle giuste porte. Tutte queste operazioni sono state automatizzate nel file _docker-compose.yml_, che consente di impostare tutte le regole per il deploy dei diversi container sulle giuste porte. Pertanto, dopo aver avviato il demone di Docker ed essersi posizionati nella directory contenente il file `docker-compose.yml` (scaricabile anche da GitLab releases), per effettuare il deploy dell'intera applicazione sfruttando i container è sufficiente eseguire il seguente comando:
```bash
docker compose up
```
(in alcune versioni/sistemi operativi può essere necessario scriverlo nella forma `docker-compose up`).

Qualora si vogliano avviare i container in seguito ad una interruzione del servizio, per recuperare il tempo di down, si può eseguire il seguente comando:
```bash
INTERRUPTION_TIME="2022-01-01T12:30:00" docker compose up
```
impostando naturalmente INTERRUPTION_TIME con la giusta data e ora in cui ha avuto inizio l'interruzione (rispettando il formato).

Quando si desidera stoppare l'applicazione è sufficiente premere `CTRL+C`: questo termina tutti i container, che saranno eventualmente pronti per una nuova esecuzione con uno dei due comandi di cui sopra. Da precisare che la nuova esecuzione non comporterà un nuovo download delle immagini (ovviamente a patto che queste non siano state eliminate manualmente).

Questa soluzione con i container ha il grosso vantaggio che non è necessario avere alcun ambiente di sviluppo per poter eseguire i vari microservizi: tutto il necessario è dentro ogni container. Un piccolo contro, se vogliamo, è la dimensione di ogni immagine, circa 250 MB, che moltiplicato per 7 immagini corrisponde a un totale di 1,7 GB, che comunque non dovrebbe essere un grosso problema al giorno d'oggi. In ogni caso, se si vuole evitare un download di queste dimensioni, si può optare per la build manuale (vedi sezione seguente).