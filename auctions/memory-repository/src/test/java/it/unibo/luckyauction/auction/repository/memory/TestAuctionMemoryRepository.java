/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.repository.memory;

import it.unibo.luckyauction.auction.domain.entity.Auction;
import it.unibo.luckyauction.auction.domain.valueobject.Bid;
import it.unibo.luckyauction.auction.domain.valueobject.Product;
import it.unibo.luckyauction.auction.domain.valueobject.ProductCategory;
import it.unibo.luckyauction.auction.domain.valueobject.ProductState;
import it.unibo.luckyauction.auction.usecase.port.AuctionMemoryRepository;
import it.unibo.luckyauction.util.CalendarUtils;
import it.unibo.luckyauction.util.Sleeper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.math.BigDecimal;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestAuctionMemoryRepository {

    private static final String USERNAME = "testUser";
    private static final String AUCTION_ID = "1";
    private final AuctionMemoryRepository auctionRepository = new AuctionMemoryCollection();
    private final Duration duration = Duration.ofMillis(300);
    private final Duration extensionDuration = Duration.ofMillis(200);
    private final Auction auction = Auction.builder()
            .auctionId(AUCTION_ID)
            .product(Product.builder()
                    .name("iPhone X")
                    .category(ProductCategory.INFORMATICS)
                    .state(ProductState.USED_OPTIMUM_CONDITION)
                    .image("base64img")
                    .build())
            .startingPrice(BigDecimal.ONE)
            .duration(duration)
            .extensionDuration(extensionDuration)
            .sellerUsername(USERNAME)
            .bidsHistoryId("1")
            .build();

    @BeforeEach
    void testAuctionCreate() {
        if (auctionRepository.findById(auction.getAuctionId()).isPresent()) {
            auctionRepository.deleteById(auction.getAuctionId());
        }
        assertEquals(Optional.empty(), auctionRepository.findById(auction.getAuctionId()));
        auctionRepository.create(auction);
        assertEquals(Optional.of(auction), auctionRepository.findById(auction.getAuctionId()));
        assertEquals(List.of(auction), auctionRepository.getAllAuctions());
    }

    @AfterEach
    void testAuctionDeletion() {
        if (auctionRepository.findById(AUCTION_ID).isPresent()) {
            assertEquals(Optional.of(AUCTION_ID), auctionRepository.deleteById(AUCTION_ID).map(Auction::getAuctionId));
            assertEquals(Optional.empty(), auctionRepository.findById(AUCTION_ID));
            assertEquals(Collections.emptyList(), auctionRepository.getAllAuctions());
        }
    }

    @Test
    void testFindByUsername() {
        assertEquals(List.of(auction), auctionRepository.findByUsername(USERNAME));
    }

    @Test
    void testDeleteByUsername() {
        assertEquals(List.of(auction), auctionRepository.deleteByUsername(USERNAME));
        assertEquals(Collections.emptyList(), auctionRepository.findByUsername(USERNAME));
    }

    @SuppressWarnings("PMD.JUnitTestsShouldIncludeAssert") // in fact, they are inside lambda...
    @Test
    void testBidAdd() {
        final Auction auction = auctionRepository.findById(AUCTION_ID).orElseThrow();
        auction.start();
        final List<Bid> bids = new ArrayList<>();
        Stream.of("10.99", "15.20", "17.00")
                .map(amount -> new Bid(new BigDecimal(amount), USERNAME))
                .forEach(bid -> {
                    assertEquals(bids, auctionRepository.getBidsHistory(auction.getAuctionId()));
                    bids.add(bid);
                    auction.addNewBid(bid);
                    assertEquals(Optional.of(bid),
                            auctionRepository.findById(auction.getAuctionId()).flatMap(Auction::getActualBid));
                    assertEquals(bids, auctionRepository.getBidsHistory(auction.getAuctionId()));
                });
    }

    @Test
    void testExpectedClosingTimeUpdate() throws InterruptedException {
        final Auction auction = auctionRepository.findById(AUCTION_ID).orElseThrow();
        auction.start();
        final Calendar finalExpectedClosingTime = CalendarUtils.add(auction.getExpectedClosingTime(),
                auction.getExtensionAmount().orElseThrow());
        new Sleeper().sleepEquallyMoreThan(auction.getDuration().toMillis());
        // now auction is expected to be closed, but since no bids are received and
        // extension has been scheduled, auction will be extended
        assertTrue(auction.isActive());
        assertExpectedClosingTime(finalExpectedClosingTime);
    }

    @Test
    void testAuctionClosing() throws InterruptedException {
        final Auction auction = auctionRepository.findById(AUCTION_ID).orElseThrow();
        final Calendar expectedClosingTime = auction.getExpectedClosingTime();
        auction.start();
        final Bid winningBid = new Bid(new BigDecimal("25.00"), "winner");
        auction.addNewBid(winningBid);
        new Sleeper().sleepEquallyMoreThan(auction.getDuration().toMillis());
        assertFalse(auction.isActive());
        assertEquals(Optional.of(winningBid),
                auctionRepository.findById(auction.getAuctionId()).flatMap(Auction::getWinningBid));
        assertTrue(CalendarUtils.diff(auction.getClosingTime().orElseThrow(),
                expectedClosingTime).getSeconds() < 1);
    }

    private void assertExpectedClosingTime(final Calendar expectedClosingTime) {
        assertEquals(Optional.of(expectedClosingTime),
                auctionRepository.findById(AUCTION_ID).map(Auction::getExpectedClosingTime));
    }

}
