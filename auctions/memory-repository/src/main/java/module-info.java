module lss.ds.luckyauction.magnani.pasolini.auctions.repository.memory.main {
    exports it.unibo.luckyauction.auction.repository.memory;
    requires lss.ds.luckyauction.magnani.pasolini.utils.main;
    requires lss.ds.luckyauction.magnani.pasolini.auctions.domain.main;
    requires lss.ds.luckyauction.magnani.pasolini.auctions.usecase.main;
}