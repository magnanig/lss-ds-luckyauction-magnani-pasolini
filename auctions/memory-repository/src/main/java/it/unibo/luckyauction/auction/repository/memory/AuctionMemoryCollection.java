/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.repository.memory;

import it.unibo.luckyauction.auction.domain.entity.Auction;
import it.unibo.luckyauction.auction.domain.valueobject.Bid;
import it.unibo.luckyauction.auction.usecase.exception.AuctionNotFoundException;
import it.unibo.luckyauction.auction.usecase.port.AuctionMemoryRepository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AuctionMemoryCollection implements AuctionMemoryRepository {

    private final List<Auction> auctions;

    public AuctionMemoryCollection() {
        this.auctions = new ArrayList<>();
    }

    /**
     * {@inheritDoc}.
     */
    @Override
    public void create(final Auction auction) {
        auctions.add(auction);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<Auction> deleteById(final String auctionId) {
       return deleteOneByFilter(auction -> auction.getAuctionId().equals(auctionId)).stream()
               .peek(Auction::cancel)
               .findFirst();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Auction> deleteByUsername(final String sellerUsername) {
        return deleteAllByFilter(auction -> auction.getSellerUsername().equals(sellerUsername));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<Auction> findById(final String auctionId) {
        return findOneByFilter(auction -> auction.getAuctionId().equals(auctionId));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Auction> findByUsername(final String sellerUsername) {
        return auctions.stream()
                .filter(auction -> auction.getSellerUsername().equals(sellerUsername))
                .collect(Collectors.toList());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Auction> getAllAuctions() {
        return Collections.unmodifiableList(auctions);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Bid> getBidsHistory(final String auctionId) {
        return findOneByFilter(auction -> auction.getAuctionId().equals(auctionId))
                .orElseThrow(() -> new AuctionNotFoundException(auctionId))
                .getBids();
    }

    private List<Auction> deleteAllByFilter(final Predicate<Auction> filter) {
        final List<Auction> removed = findAllByFilter(filter)
                .collect(Collectors.toList());
        auctions.removeAll(removed);
        return removed;

    }

    private Optional<Auction> deleteOneByFilter(final Predicate<Auction> filter) {
        final Optional<Auction> removedAuction = findOneByFilter(filter);
        removedAuction.ifPresent(auctions::remove);
        return removedAuction;
    }

    private Optional<Auction> findOneByFilter(final Predicate<Auction> filter) {
        return findAllByFilter(filter).findFirst();
    }

    private Stream<Auction> findAllByFilter(final Predicate<Auction> filter) {
        return auctions.stream().filter(filter);
    }
}
