dependencies {
    implementation(project(":utils"))
    implementation(project(":auctions:domain"))
    implementation(project(":auctions:usecase"))
}