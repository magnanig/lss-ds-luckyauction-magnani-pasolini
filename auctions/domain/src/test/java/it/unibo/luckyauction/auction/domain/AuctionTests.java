/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.domain;

import it.unibo.luckyauction.auction.domain.entity.Auction;
import it.unibo.luckyauction.auction.domain.valueobject.Bid;
import it.unibo.luckyauction.util.CalendarUtils;
import it.unibo.luckyauction.util.NumericalConstants;
import it.unibo.luckyauction.util.Sleeper;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import static it.unibo.luckyauction.util.NumericalConstants.TWO;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class AuctionTests {
    private static final Duration ONE_HUNDRED_MILLIS = Duration.ofMillis(100);

    @Test
    void endedAuctionCreationTest() {
        final Bid winningBid = new Bid(AuctionTestUtils.BID_RAISE_AMOUNT, AuctionTestUtils.SECOND_USERNAME);
        final Calendar twoDaysBefore = Calendar.getInstance();
        twoDaysBefore.add(Calendar.DATE, -TWO);

        // if the actualBid is not set but the auction is closed,
        // check that the actualBid is set equal to the winningBid
        final Auction endedAuction = Auction.builder()
                .auctionId(AuctionTestUtils.AUCTION_TEST_ID)
                .creationTime(twoDaysBefore)
                .product(AuctionTestUtils.SHOES_PRODUCT.get()).sellerUsername(AuctionTestUtils.FIRST_USERNAME)
                .startingPrice(AuctionTestUtils.AUCTION_PRICE).duration(AuctionTestUtils.TWO_DAYS_DURATION)
                .winningBid(winningBid)
                .closingTime(Calendar.getInstance())
                .bidsHistoryId(AuctionTestUtils.BIDS_HISTORY_ID).bids(List.of(winningBid))
                .build();

        assertFalse(endedAuction.isActive());
        assertTrue(endedAuction.getActualBid().isPresent());
        assertEquals(winningBid, endedAuction.getActualBid().get());
    }

    @Test
    void auctionGettersTest() throws InterruptedException {
        final Sleeper s = new Sleeper();
        final Bid firstBid = new Bid(AuctionTestUtils.BID_RAISE_AMOUNT, AuctionTestUtils.SECOND_USERNAME);
        final Calendar yesterday = Calendar.getInstance();
        yesterday.add(Calendar.DATE, -NumericalConstants.ONE);
        final Calendar afterTwoDays = Calendar.getInstance();
        afterTwoDays.add(Calendar.DATE, TWO);

        final Auction completeAuction = Auction.builder()
                .auctionId(AuctionTestUtils.AUCTION_TEST_ID)
                .product(AuctionTestUtils.SHOES_PRODUCT.get())
                .sellerUsername(AuctionTestUtils.FIRST_USERNAME)
                .startingPrice(AuctionTestUtils.AUCTION_PRICE)
                .duration(ONE_HUNDRED_MILLIS)
                .bidsHistoryId(AuctionTestUtils.BIDS_HISTORY_ID)
                .build();
        completeAuction.start();

        final Calendar expectedClosingTime = CalendarUtils.add(completeAuction.getCreationTime(), ONE_HUNDRED_MILLIS);

        assertTrue(completeAuction.getCreationTime().isSet(Calendar.DATE));
        assertEquals(AuctionTestUtils.AUCTION_TEST_ID, completeAuction.getAuctionId());
        assertEquals(AuctionTestUtils.SHOES_PRODUCT.get(), completeAuction.getProduct());
        assertEquals(AuctionTestUtils.FIRST_USERNAME, completeAuction.getSellerUsername());
        assertEquals(AuctionTestUtils.AUCTION_PRICE, completeAuction.getStartingPrice());
        assertEquals(ONE_HUNDRED_MILLIS, completeAuction.getDuration());
        assertEquals(Collections.emptyList(), completeAuction.getBids());
        assertTrue(completeAuction.getWinningBid().isEmpty());
        assertTrue(completeAuction.getActualBid().isEmpty());
        assertEquals(AuctionTestUtils.BIDS_HISTORY_ID, completeAuction.getBidsHistoryId());

        assertTrue(completeAuction.getExtensionAmount().isEmpty());
        assertEquals(expectedClosingTime, completeAuction.getExpectedClosingTime());
        assertTrue(completeAuction.isActive());
        // add new bid
        assertFalse(completeAuction.addNewBid(firstBid), "The auction duration has not been extended "
                + "because we are not in the end zone");
        assertTrue(completeAuction.getActualBid().isPresent());
        assertEquals(firstBid, completeAuction.getActualBid().get());
        assertEquals(List.of(firstBid), completeAuction.getBids());
        // wait not to receive bids until the end
        s.sleepEquallyMoreThan(ONE_HUNDRED_MILLIS.toMillis());

        // closing time check --> don't use equals because it is normal to have some decime of difference
        assertTrue(CalendarUtils.diff(completeAuction.getClosingTime().orElseThrow(),
                expectedClosingTime).getSeconds() < 1);
        assertTrue(completeAuction.getWinningBid().isPresent());
        assertEquals(firstBid, completeAuction.getWinningBid().get());
    }
}
