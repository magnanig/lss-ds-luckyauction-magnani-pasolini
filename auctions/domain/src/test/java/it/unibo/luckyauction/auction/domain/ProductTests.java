/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.domain;

import it.unibo.luckyauction.auction.domain.exception.ProductValidationException;
import it.unibo.luckyauction.auction.domain.valueobject.Product;
import it.unibo.luckyauction.auction.domain.valueobject.ProductCategory;
import it.unibo.luckyauction.auction.domain.valueobject.ProductState;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ProductTests {

    private static final String IMAGE_BASE_64 = "imageBase64";

    @Test
    void productExceptionsTest() {
        final Exception nameException = assertThrows(ProductValidationException.class,
                () -> Product.builder().category(ProductCategory.SHOES_AND_BAGS).state(ProductState.USED_BAD_CONDITION)
                        .image(IMAGE_BASE_64).build());

        final Exception categoryException = assertThrows(ProductValidationException.class,
                () -> Product.builder().name(AuctionTestUtils.SHOES_PRODUCT_NAME).state(ProductState.USED_BAD_CONDITION)
                        .image(IMAGE_BASE_64).build());

        final Exception stateException = assertThrows(ProductValidationException.class,
                () -> Product.builder().name(AuctionTestUtils.SHOES_PRODUCT_NAME).category(ProductCategory.SHOES_AND_BAGS)
                        .image(IMAGE_BASE_64).build());

        final Exception imageException = assertThrows(ProductValidationException.class,
                () -> Product.builder().name(AuctionTestUtils.SHOES_PRODUCT_NAME).category(ProductCategory.SHOES_AND_BAGS)
                        .state(ProductState.USED_BAD_CONDITION).build());

        assertTrue(nameException.getMessage().contains("Product name is not set"));
        assertTrue(categoryException.getMessage().contains("Product category is not set"));
        assertTrue(stateException.getMessage().contains("Product state is not set"));
        assertTrue(imageException.getMessage().contains("Product image is not set"));
    }

    @Test
    void productGettersTest() {
        final Product firstProduct = Product.builder().name(AuctionTestUtils.SHOES_PRODUCT_NAME)
                .description(AuctionTestUtils.SHOES_DESCRIPTION).category(ProductCategory.SHOES_AND_BAGS)
                .state(ProductState.USED_BAD_CONDITION).image(IMAGE_BASE_64).build();

        final Product secondProduct = Product.builder().name(AuctionTestUtils.SHOES_PRODUCT_NAME)
                .description(AuctionTestUtils.SHOES_DESCRIPTION).category(ProductCategory.SHOES_AND_BAGS)
                .state(ProductState.USED_BAD_CONDITION).image(IMAGE_BASE_64).build();

        assertEquals(firstProduct, secondProduct);
        assertEquals(AuctionTestUtils.SHOES_PRODUCT_NAME, firstProduct.name());
        assertEquals(AuctionTestUtils.SHOES_DESCRIPTION, firstProduct.description());
        assertEquals(ProductCategory.SHOES_AND_BAGS, firstProduct.category());
        assertEquals(ProductState.USED_BAD_CONDITION, firstProduct.state());
        assertEquals(IMAGE_BASE_64, firstProduct.image());
    }
}
