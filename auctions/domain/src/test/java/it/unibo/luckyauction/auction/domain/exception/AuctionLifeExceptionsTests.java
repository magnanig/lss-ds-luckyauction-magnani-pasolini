/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.domain.exception;

import it.unibo.luckyauction.auction.domain.AuctionTestUtils;
import it.unibo.luckyauction.auction.domain.entity.AuctionLife;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.time.Duration;
import java.util.Calendar;

import static it.unibo.luckyauction.auction.domain.config.AuctionConstants.getMaxAuctionDuration;
import static it.unibo.luckyauction.util.NumericalConstants.TWO;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class AuctionLifeExceptionsTests {

    private static final Duration ONE_DAY_DURATION = Duration.ofDays(1);
    private static final Duration TEN_DAY_DURATION = Duration.ofDays(10);
    private static final Duration CORRECT_DURATION = getMaxAuctionDuration();

    private final Calendar currentTime = Calendar.getInstance();
    private final Calendar twoDaysBefore = Calendar.getInstance();
    private final Calendar yesterday = Calendar.getInstance();
    private final Calendar tomorrow = Calendar.getInstance();

    @BeforeAll
    /* package */ void config() {
        twoDaysBefore.add(Calendar.DATE, -TWO);
        yesterday.add(Calendar.DATE, -1);
        tomorrow.add(Calendar.DATE, 1);
    }

    @Test
    /* package */ void creationTimeExceptionsTest() {
        final Exception creationTimeException = assertThrows(AuctionLifeValidationException.class,
                () -> AuctionLife.builder().duration(AuctionTestUtils.TWO_DAYS_DURATION)
                        .closingTime(Calendar.getInstance()).build());

        assertTrue(creationTimeException.getMessage().contains("Since you specified a closing time, a creation time "
                + "must be provided too"));

        assertDoesNotThrow(() -> AuctionLife.builder().creationTime(twoDaysBefore)
                .duration(AuctionTestUtils.TWO_DAYS_DURATION)
                .closingTime(Calendar.getInstance())
                .build());

        final AuctionLife auctionLife = AuctionLife.builder().creationTime(twoDaysBefore)
                .duration(AuctionTestUtils.TWO_DAYS_DURATION).closingTime(Calendar.getInstance()).build();
        assertTrue(auctionLife.getClosingTime().isPresent(), "Closing time is set");
        assertEquals(auctionLife.getClosingTime().get(),
                auctionLife.getExpectedClosingTime());
    }

    @Test
    /* package */ void durationExceptionsTest() {
        assertDoesNotThrow(() -> AuctionLife.builder().creationTime(yesterday).duration(ONE_DAY_DURATION)
                .closingTime(currentTime).build());

        final Exception firstDurationException = assertThrows(AuctionLifeValidationException.class,
                () -> AuctionLife.builder().extensionDuration(CORRECT_DURATION).build());

        final Exception secondDurationException = assertThrows(AuctionLifeValidationException.class,
                () -> AuctionLife.builder().creationTime(twoDaysBefore).duration(ONE_DAY_DURATION)
                        .closingTime(currentTime).build());

        assertTrue(firstDurationException.getMessage().contains("Auction duration is not set. Please specify "
                        + "a duration or an expected closing time"));
        assertTrue(secondDurationException.getMessage().contains("The specified duration does not correspond to the "
                + "effective auction duration"));
    }

    @Test
    /* package */ void extensionDurationExceptionsTest() {
        final Duration wrongExtensionDuration = getMaxAuctionDuration().plus(ONE_DAY_DURATION);
        final Exception extensionDurationException = assertThrows(AuctionLifeValidationException.class, () ->
                AuctionLife.builder().duration(CORRECT_DURATION).extensionDuration(wrongExtensionDuration).build());
        assertTrue(extensionDurationException.getMessage().contains("Wrong extensionDuration!"));

        final Duration durationGreaterThanTenDays = getMaxAuctionDuration().plus(TEN_DAY_DURATION);
        assertDoesNotThrow(() -> AuctionLife.builder()
                .duration(durationGreaterThanTenDays).extensionDuration(CORRECT_DURATION).build());
    }

    @Test
    /* package */ void closingTimeExceptionTest() {
        final Calendar yesterday = Calendar.getInstance();
        yesterday.add(Calendar.DATE, -1);

        final Exception firstException = assertThrows(AuctionLifeValidationException.class,
                () -> AuctionLife.builder().creationTime(currentTime)
                        .duration(AuctionTestUtils.TWO_DAYS_DURATION).closingTime(yesterday).build());
        final Exception secondException = assertThrows(AuctionLifeValidationException.class,
                () -> AuctionLife.builder().creationTime(yesterday).closingTime(tomorrow).build());

        assertTrue(firstException.getMessage().contains("Closing time cannot be equal to or less than "
                + "creation time"));
        assertTrue(secondException.getMessage().contains("Closing time cannot be a future date"));
    }
}
