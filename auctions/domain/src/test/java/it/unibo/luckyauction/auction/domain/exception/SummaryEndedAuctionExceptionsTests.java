/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.domain.exception;

import it.unibo.luckyauction.auction.domain.AuctionTestUtils;
import it.unibo.luckyauction.auction.domain.valueobject.AwardedAuction;
import it.unibo.luckyauction.auction.domain.valueobject.Bid;
import it.unibo.luckyauction.auction.domain.valueobject.CreatedEndedAuction;
import it.unibo.luckyauction.auction.domain.valueobject.LostAuction;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.Calendar;
import java.util.function.Supplier;

import static it.unibo.luckyauction.auction.domain.AuctionTestUtils.CONTACTS;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class SummaryEndedAuctionExceptionsTests {

    private static final String CLOSING_TIME_EXCEPTION = "Closing time cannot be equal to or less than creation time";
    private static final String WINNING_BID_EXCEPTION = "The auction winner cannot be the seller himself";
    private static final String IS_NOT_SET_EXCEPTION = "is not set";

    private Calendar creationTime;
    private Calendar closingTime;

    private static final Bid WINNING_BID = new Bid(AuctionTestUtils.BID_RAISE_AMOUNT, AuctionTestUtils.SECOND_USERNAME);

    @BeforeAll
    void config() {
        creationTime = Calendar.getInstance();

        closingTime = Calendar.getInstance();
        closingTime.add(Calendar.DATE, 1);
    }

    @Test
    void createdEndedAuctionExceptionsTest() {
        final Supplier<CreatedEndedAuction.CreatedEndedAuctionBuilder> preBuiltEndedAuction = () ->
                CreatedEndedAuction.builder().product(AuctionTestUtils.SHOES_PRODUCT.get()).creationTime(creationTime)
                        .sellerUsername(AuctionTestUtils.FIRST_USERNAME).startingPrice(AuctionTestUtils.AUCTION_PRICE);

        final Exception contactsException = assertThrows(AuctionValidationException.class,
                () -> preBuiltEndedAuction.get().winningBid(WINNING_BID).closingTime(closingTime).build());
        final Exception closingTimeNullException = assertThrows(AuctionValidationException.class,
                () -> preBuiltEndedAuction.get().winningBid(WINNING_BID).buyerContacts(CONTACTS.get())
                        .sellerContacts(CONTACTS.get()).build());
        final Exception closingTimeException = assertThrows(AuctionValidationException.class,
                () -> preBuiltEndedAuction.get().closingTime(creationTime).winningBid(WINNING_BID)
                        .buyerContacts(CONTACTS.get())
                        .sellerContacts(CONTACTS.get()).build());

        assertTrue(contactsException.getMessage().contains("If a winning bid is set, both seller and buyer contacts"
                + " must be provided"));
        assertTrue(closingTimeNullException.getMessage().contains(IS_NOT_SET_EXCEPTION));
        assertTrue(closingTimeException.getMessage().contains(CLOSING_TIME_EXCEPTION));
        // winning bid is not set, but the exception is not thrown
        assertDoesNotThrow(() -> preBuiltEndedAuction.get().closingTime(closingTime).build());
    }

    @Test
    void awardedAuctionExceptionsTest() {
        final Supplier<AwardedAuction.AwardedAuctionBuilder> preBuiltEndedAuction = () -> AwardedAuction.builder()
                .product(AuctionTestUtils.SHOES_PRODUCT.get()).creationTime(creationTime)
                .sellerUsername(AuctionTestUtils.FIRST_USERNAME).startingPrice(AuctionTestUtils.AUCTION_PRICE);

        final Exception contactsException = assertThrows(AuctionValidationException.class,
                () -> preBuiltEndedAuction.get().winningBid(WINNING_BID).closingTime(closingTime).build());
        final Exception closingTimeNullException = assertThrows(AuctionValidationException.class,
                () -> preBuiltEndedAuction.get().winningBid(WINNING_BID).buyerContacts(CONTACTS.get())
                        .sellerContacts(CONTACTS.get()).build());
        final Exception closingTimeException = assertThrows(AuctionValidationException.class,
                () -> preBuiltEndedAuction.get().closingTime(creationTime).winningBid(WINNING_BID)
                        .buyerContacts(CONTACTS.get()).sellerContacts(CONTACTS.get()).build());

        final Exception winningBidNullException = assertThrows(AuctionValidationException.class,
                () -> preBuiltEndedAuction.get().sellerContacts(CONTACTS.get())
                        .closingTime(closingTime).build());
        final Exception winningBidException = assertThrows(AuctionValidationException.class,
                () -> preBuiltEndedAuction.get().closingTime(closingTime)
                        .winningBid(new Bid(AuctionTestUtils.BID_RAISE_AMOUNT, AuctionTestUtils.FIRST_USERNAME))
                        .buyerContacts(CONTACTS.get()).sellerContacts(CONTACTS.get()).build());

        assertTrue(contactsException.getMessage().contains("Both seller and buyer contacts must be set"));
        assertTrue(closingTimeNullException.getMessage().contains(IS_NOT_SET_EXCEPTION));
        assertTrue(closingTimeException.getMessage().contains(CLOSING_TIME_EXCEPTION));
        assertTrue(winningBidNullException.getMessage().contains(IS_NOT_SET_EXCEPTION));
        assertTrue(winningBidException.getMessage().contains(WINNING_BID_EXCEPTION));
    }

    @Test
    void lostAuctionExceptionTest() {
        final Supplier<LostAuction.LostAuctionBuilder> preBuiltEndedAuction = () -> LostAuction.builder()
                .product(AuctionTestUtils.SHOES_PRODUCT.get()).creationTime(creationTime)
                .sellerUsername(AuctionTestUtils.FIRST_USERNAME).startingPrice(AuctionTestUtils.AUCTION_PRICE);

        final Exception closingTimeNullException = assertThrows(AuctionValidationException.class,
                () -> preBuiltEndedAuction.get().winningBid(WINNING_BID).build());
        final Exception closingTimeException = assertThrows(AuctionValidationException.class,
                () -> preBuiltEndedAuction.get().closingTime(creationTime).winningBid(WINNING_BID).build());

        final Exception winningBidNullException = assertThrows(AuctionValidationException.class,
                () -> preBuiltEndedAuction.get().closingTime(closingTime).build());
        final Exception winningBidException = assertThrows(AuctionValidationException.class,
                () -> preBuiltEndedAuction.get().closingTime(closingTime)
                        .winningBid(new Bid(AuctionTestUtils.BID_RAISE_AMOUNT, AuctionTestUtils.FIRST_USERNAME))
                        .build());

        assertTrue(closingTimeNullException.getMessage().contains(IS_NOT_SET_EXCEPTION));
        assertTrue(closingTimeException.getMessage().contains(CLOSING_TIME_EXCEPTION));
        assertTrue(winningBidNullException.getMessage().contains(IS_NOT_SET_EXCEPTION));
        assertTrue(winningBidException.getMessage().contains(WINNING_BID_EXCEPTION));
    }
}
