/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.domain;

import it.unibo.luckyauction.auction.domain.exception.UserContactInformationValidationException;
import it.unibo.luckyauction.auction.domain.valueobject.UserContactInformation;
import org.junit.jupiter.api.Test;

import static it.unibo.luckyauction.auction.domain.AuctionTestUtils.CELLULAR_NUMBER;
import static it.unibo.luckyauction.auction.domain.AuctionTestUtils.EMAIL;
import static it.unibo.luckyauction.auction.domain.AuctionTestUtils.TELEPHONE_NUMBER;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class UserContactInformationTests {

    @Test
    void userContactInformationTest() {
        final UserContactInformation userContacts = new UserContactInformation(EMAIL, CELLULAR_NUMBER,
                TELEPHONE_NUMBER);

        assertEquals(EMAIL, userContacts.email());
        assertEquals(CELLULAR_NUMBER, userContacts.cellularNumber());
        assertEquals(TELEPHONE_NUMBER, userContacts.telephoneNumber());
    }

    @Test
    void telephoneNumberOptionalTest() {
        final UserContactInformation userContacts = new UserContactInformation(EMAIL, CELLULAR_NUMBER);

        assertEquals(EMAIL, userContacts.email());
        assertEquals(CELLULAR_NUMBER, userContacts.cellularNumber());
        assertNull(userContacts.telephoneNumber());
    }

    @Test
    void userContactInformationExceptionsTest() {
        final Exception nullEmailException = assertThrows(UserContactInformationValidationException.class,
                () -> new UserContactInformation(null, CELLULAR_NUMBER));
        assertTrue(nullEmailException.getMessage().contains("User's email is not set"));

        final Exception emptyEmailException = assertThrows(UserContactInformationValidationException.class,
                () -> new UserContactInformation("", CELLULAR_NUMBER));
        assertTrue(emptyEmailException.getMessage().contains("User's email is not set"));

        final Exception nullCellularNumberException = assertThrows(UserContactInformationValidationException.class,
                () -> new UserContactInformation(EMAIL, null));
        assertTrue(nullCellularNumberException.getMessage().contains("User's cellular number is not set"));

        final Exception emptyCellularNumberException = assertThrows(UserContactInformationValidationException.class,
                () -> new UserContactInformation(EMAIL, ""));
        assertTrue(emptyCellularNumberException.getMessage().contains("User's cellular number is not set"));
    }
}
