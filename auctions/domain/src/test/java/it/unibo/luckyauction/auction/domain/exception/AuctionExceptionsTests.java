/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.domain.exception;

import it.unibo.luckyauction.auction.domain.AuctionTestUtils;
import it.unibo.luckyauction.auction.domain.entity.Auction;
import it.unibo.luckyauction.auction.domain.valueobject.Bid;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;
import java.util.function.Supplier;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class AuctionExceptionsTests {

    private static final int ONE_DAY = 1;
    private static final int TWO_DAYS = 2;
    private final BigDecimal negativeAmount = BigDecimal.valueOf(-9, 99);
    private final BigDecimal amountWithThreeDecimal = BigDecimal.valueOf(25, 876);

    @Test
    void abstractAuctionExceptionsTest() {
        final Exception productException = assertThrows(AuctionValidationException.class,
                () -> Auction.builder().sellerUsername(AuctionTestUtils.FIRST_USERNAME)
                        .startingPrice(AuctionTestUtils.AUCTION_PRICE).duration(AuctionTestUtils.TWO_DAYS_DURATION)
                        .bidsHistoryId(AuctionTestUtils.BIDS_HISTORY_ID).build());
        assertTrue(productException.getMessage().contains("Product is not set"));

        final Exception nullSellerUsernameException = assertThrows(AuctionValidationException.class,
                () -> Auction.builder().product(AuctionTestUtils.SHOES_PRODUCT.get())
                        .startingPrice(AuctionTestUtils.AUCTION_PRICE)
                        .duration(AuctionTestUtils.TWO_DAYS_DURATION)
                        .bidsHistoryId(AuctionTestUtils.BIDS_HISTORY_ID).build());
        assertTrue(nullSellerUsernameException.getMessage().contains("Seller username is not set"));

        final Exception emptySellerUsernameException = assertThrows(AuctionValidationException.class,
                () -> Auction.builder().sellerUsername("").product(AuctionTestUtils.SHOES_PRODUCT.get())
                        .startingPrice(AuctionTestUtils.AUCTION_PRICE)
                        .duration(AuctionTestUtils.TWO_DAYS_DURATION)
                        .bidsHistoryId(AuctionTestUtils.BIDS_HISTORY_ID).build());
        assertTrue(emptySellerUsernameException.getMessage().contains("Seller username is not set"));

        final Exception firstPriceException = assertThrows(AuctionValidationException.class,
                () -> Auction.builder().product(AuctionTestUtils.SHOES_PRODUCT.get())
                        .sellerUsername(AuctionTestUtils.FIRST_USERNAME)
                        .duration(AuctionTestUtils.TWO_DAYS_DURATION)
                        .bidsHistoryId(AuctionTestUtils.BIDS_HISTORY_ID).build());
        assertTrue(firstPriceException.getMessage().contains("Starting price is not set"));

        final Exception secondPriceException = assertThrows(AuctionValidationException.class,
                () -> Auction.builder().product(AuctionTestUtils.SHOES_PRODUCT.get())
                        .sellerUsername(AuctionTestUtils.FIRST_USERNAME)
                        .startingPrice(negativeAmount).duration(AuctionTestUtils.TWO_DAYS_DURATION)
                        .bidsHistoryId(AuctionTestUtils.BIDS_HISTORY_ID).build());
        assertTrue(secondPriceException.getMessage().contains("must be greater than zero"));

        final Exception thirdPriceException = assertThrows(AuctionValidationException.class,
                () -> Auction.builder().product(AuctionTestUtils.SHOES_PRODUCT.get())
                        .sellerUsername(AuctionTestUtils.FIRST_USERNAME)
                        .startingPrice(amountWithThreeDecimal).duration(AuctionTestUtils.TWO_DAYS_DURATION)
                        .bidsHistoryId(AuctionTestUtils.BIDS_HISTORY_ID).build());
        assertTrue(thirdPriceException.getMessage().contains("can have at most two decimal digits"));
    }

    @Test
    void abstractOngoingAuctionExceptionsTest() {
        final Supplier<Auction.AuctionBuilder> preBuildAuction = () -> Auction.builder()
                .product(AuctionTestUtils.SHOES_PRODUCT.get()).sellerUsername(AuctionTestUtils.SELLER_USERNAME)
                .startingPrice(AuctionTestUtils.AUCTION_PRICE).bidsHistoryId(AuctionTestUtils.BIDS_HISTORY_ID);

        final Exception durationException = assertThrows(AuctionLifeValidationException.class,
                () -> preBuildAuction.get().build());
        assertTrue(durationException.getMessage().contains("Auction duration is not set"));

        final String exceptionMessage = "The new bid must have a greater amount than the previous one";
        final Auction ongoingAuction = preBuildAuction.get().duration(AuctionTestUtils.TWO_DAYS_DURATION).build();
        final Bid newBid = new Bid(AuctionTestUtils.BID_RAISE_AMOUNT, AuctionTestUtils.FIRST_USERNAME);
        ongoingAuction.addNewBid(newBid); // add new bid

        final Exception nullBidException = assertThrows(AuctionValidationException.class,
                () -> ongoingAuction.addNewBid(null)); // add null bid
        assertTrue(nullBidException.getMessage().contains(exceptionMessage));

        final Exception sameBidException = assertThrows(AuctionValidationException.class,
                () -> ongoingAuction.addNewBid(newBid)); // add bid as before
        assertTrue(sameBidException.getMessage().contains(exceptionMessage));
    }

    @Test
    void abstractAuctionWithLifeExceptionsTest() {
        final Calendar twoDaysBefore = Calendar.getInstance();
        twoDaysBefore.add(Calendar.DATE, -TWO_DAYS);
        final Calendar yesterday = Calendar.getInstance();
        yesterday.add(Calendar.DATE, -ONE_DAY);
        final Calendar afterTwoDays = Calendar.getInstance();
        afterTwoDays.add(Calendar.DATE, TWO_DAYS);

        bidsHistoryExceptionTest();

        final Exception winnerEqualsSellerException = assertThrows(AuctionValidationException.class,
                () -> Auction.builder().creationTime(twoDaysBefore).product(AuctionTestUtils.SHOES_PRODUCT.get())
                        .sellerUsername(AuctionTestUtils.FIRST_USERNAME).startingPrice(AuctionTestUtils.AUCTION_PRICE)
                        .duration(AuctionTestUtils.TWO_DAYS_DURATION).bidsHistoryId(AuctionTestUtils.BIDS_HISTORY_ID)
                        .winningBid(new Bid(AuctionTestUtils.BID_RAISE_AMOUNT, AuctionTestUtils.FIRST_USERNAME))
                        .closingTime(Calendar.getInstance()).build());
        assertTrue(winnerEqualsSellerException.getMessage().contains("auction winner cannot be the seller"));

        final Exception winningBidException = assertThrows(AuctionValidationException.class,
                () -> Auction.builder().creationTime(twoDaysBefore).product(AuctionTestUtils.SHOES_PRODUCT.get())
                        .sellerUsername(AuctionTestUtils.FIRST_USERNAME).startingPrice(AuctionTestUtils.AUCTION_PRICE)
                        .duration(AuctionTestUtils.TWO_DAYS_DURATION).bidsHistoryId(AuctionTestUtils.BIDS_HISTORY_ID)
                        .winningBid(new Bid(AuctionTestUtils.BID_RAISE_AMOUNT, AuctionTestUtils.SECOND_USERNAME)).build());
        assertTrue(winningBidException.getMessage().contains("If the winner is present, the closing time must also be "
                + "present"));

        final Exception actualAndWinningBidException = assertThrows(AuctionValidationException.class,
                () -> Auction.builder().creationTime(twoDaysBefore).product(AuctionTestUtils.SHOES_PRODUCT.get())
                        .sellerUsername(AuctionTestUtils.FIRST_USERNAME).startingPrice(AuctionTestUtils.AUCTION_PRICE)
                        .duration(AuctionTestUtils.TWO_DAYS_DURATION).bidsHistoryId(AuctionTestUtils.BIDS_HISTORY_ID)
                        .actualBid(new Bid(AuctionTestUtils.BID_RAISE_AMOUNT, AuctionTestUtils.SECOND_USERNAME))
                        .winningBid(new Bid(AuctionTestUtils.NEW_BID_RAISE_AMOUNT, AuctionTestUtils.SECOND_USERNAME))
                        .closingTime(Calendar.getInstance()).build());
        assertTrue(actualAndWinningBidException.getMessage().contains("The actual bid and the winning bid "
                + "must be the same"));
    }

    private void bidsHistoryExceptionTest() {
        final Exception bidsHistoryIdException = assertThrows(AuctionValidationException.class,
                () -> Auction.builder().product(AuctionTestUtils.SHOES_PRODUCT.get())
                        .sellerUsername(AuctionTestUtils.FIRST_USERNAME)
                        .startingPrice(AuctionTestUtils.AUCTION_PRICE).duration(AuctionTestUtils.TWO_DAYS_DURATION)
                        .actualBid(new Bid(AuctionTestUtils.BID_RAISE_AMOUNT, AuctionTestUtils.SECOND_USERNAME))
                        .bids(List.of(new Bid(AuctionTestUtils.BID_RAISE_AMOUNT, AuctionTestUtils.SECOND_USERNAME)))
                        .build());
        assertTrue(bidsHistoryIdException.getMessage().contains("Bids history id must be set since a list of "
                + "bids is specified"));

        final Exception actualBidException = assertThrows(AuctionValidationException.class,
                () -> Auction.builder().product(AuctionTestUtils.SHOES_PRODUCT.get())
                        .sellerUsername(AuctionTestUtils.FIRST_USERNAME)
                        .startingPrice(AuctionTestUtils.AUCTION_PRICE).duration(AuctionTestUtils.TWO_DAYS_DURATION)
                        .bidsHistoryId(AuctionTestUtils.BIDS_HISTORY_ID)
                        .bids(List.of(new Bid(AuctionTestUtils.BID_RAISE_AMOUNT, AuctionTestUtils.SECOND_USERNAME)))
                        .build());
        assertTrue(actualBidException.getMessage().contains("There is at least one bid, but the actual bid "
                        + "is not set"));

        final Exception bidsException = assertThrows(AuctionValidationException.class,
                () -> Auction.builder().product(AuctionTestUtils.SHOES_PRODUCT.get())
                        .sellerUsername(AuctionTestUtils.FIRST_USERNAME).startingPrice(AuctionTestUtils.AUCTION_PRICE)
                        .duration(AuctionTestUtils.TWO_DAYS_DURATION).bidsHistoryId(AuctionTestUtils.BIDS_HISTORY_ID)
                        .actualBid(new Bid(AuctionTestUtils.BID_RAISE_AMOUNT, AuctionTestUtils.SECOND_USERNAME))
                        .build());
        assertTrue(bidsException.getMessage().contains("The actual bid (or winning bid) is not present "
                + "in bids list"));
    }

    @Test
    void creationTimeExceptionTest() {
        final Calendar afterTwoDays = Calendar.getInstance();
        afterTwoDays.add(Calendar.DATE, 2);

        // if the closingTime is present, the creationTime must also be present
        final Exception creationTimeException = assertThrows(AuctionLifeValidationException.class, () ->
                Auction.builder()
                .auctionId(AuctionTestUtils.AUCTION_TEST_ID).product(AuctionTestUtils.SHOES_PRODUCT.get())
                .sellerUsername(AuctionTestUtils.FIRST_USERNAME).startingPrice(AuctionTestUtils.AUCTION_PRICE)
                .duration(AuctionTestUtils.TWO_DAYS_DURATION).bidsHistoryId(AuctionTestUtils.BIDS_HISTORY_ID)
                .winningBid(new Bid(AuctionTestUtils.BID_RAISE_AMOUNT, AuctionTestUtils.SECOND_USERNAME))
                .closingTime(afterTwoDays).build());
        assertTrue(creationTimeException.getMessage().contains("Since you specified a closing time, a creation time "
                + "must be provided too"));
    }
}
