/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.domain;

import it.unibo.luckyauction.auction.domain.valueobject.AwardedAuction;
import it.unibo.luckyauction.auction.domain.valueobject.Bid;
import it.unibo.luckyauction.auction.domain.valueobject.CreatedEndedAuction;
import it.unibo.luckyauction.auction.domain.valueobject.LostAuction;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.Calendar;
import java.util.function.Supplier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class SummaryEndedAuctionTests {
    private static final Bid WINNING_BID = new Bid(AuctionTestUtils.BID_RAISE_AMOUNT, AuctionTestUtils.SECOND_USERNAME);

    private Calendar creationTime;
    private Calendar closingTime;

    @BeforeAll
    void config() {
        creationTime = Calendar.getInstance();

        closingTime = Calendar.getInstance();
        closingTime.add(Calendar.DATE, 1);
    }

    @Test
    void createdEndedAuctionTest() {
        final Supplier<CreatedEndedAuction.CreatedEndedAuctionBuilder> preBuiltEndedAuction =
                () -> CreatedEndedAuction.builder().auctionId(AuctionTestUtils.AUCTION_TEST_ID)
                        .product(AuctionTestUtils.SHOES_PRODUCT.get()).creationTime(creationTime)
                        .sellerUsername(AuctionTestUtils.FIRST_USERNAME).sellerContacts(AuctionTestUtils.CONTACTS.get())
                        .startingPrice(AuctionTestUtils.AUCTION_PRICE).closingTime(closingTime);

        final CreatedEndedAuction auctionWithoutWinner = preBuiltEndedAuction.get().build();
        assertEquals(AuctionTestUtils.AUCTION_TEST_ID, auctionWithoutWinner.getAuctionId());
        assertEquals(AuctionTestUtils.SHOES_PRODUCT.get(), auctionWithoutWinner.getProduct());
        assertEquals(creationTime, auctionWithoutWinner.getCreationTime());
        assertEquals(AuctionTestUtils.FIRST_USERNAME, auctionWithoutWinner.getSellerUsername());
        assertEquals(closingTime, auctionWithoutWinner.getClosingTime());
        assertNull(auctionWithoutWinner.getWinningBid());
        assertEquals(AuctionTestUtils.AUCTION_PRICE, auctionWithoutWinner.getStartingPrice());
        assertFalse(auctionWithoutWinner.getBuyerContacts().isPresent());
        assertTrue(auctionWithoutWinner.getSellerContacts().isPresent());
        assertTrue(auctionWithoutWinner.getPdfReport().isEmpty(),
                "The pdf report must not be present as no winner has been announced");

        final CreatedEndedAuction auctionWithWinner = preBuiltEndedAuction.get().winningBid(WINNING_BID)
                .buyerContacts(AuctionTestUtils.CONTACTS.get()).build();
        assertTrue(auctionWithWinner.getBuyerContacts().isPresent());
        assertTrue(auctionWithWinner.getSellerContacts().isPresent());
        assertEquals(AuctionTestUtils.CONTACTS.get(), auctionWithWinner.getBuyerContacts().get());
        assertEquals(AuctionTestUtils.CONTACTS.get(), auctionWithWinner.getSellerContacts().get());
        assertFalse(auctionWithWinner.getPdfReport().isEmpty(), "The pdf report must be present");
    }

    @Test
    void awardedAuctionExceptionTest() {
        final AwardedAuction awardedAuction = AwardedAuction.builder().auctionId(AuctionTestUtils.AUCTION_TEST_ID)
                .product(AuctionTestUtils.SHOES_PRODUCT.get()).creationTime(creationTime)
                .sellerUsername(AuctionTestUtils.FIRST_USERNAME).sellerContacts(AuctionTestUtils.CONTACTS.get())
                .startingPrice(AuctionTestUtils.AUCTION_PRICE).closingTime(closingTime).winningBid(WINNING_BID)
                .buyerContacts(AuctionTestUtils.CONTACTS.get()).build();

        assertEquals(AuctionTestUtils.AUCTION_TEST_ID, awardedAuction.getAuctionId());
        assertEquals(AuctionTestUtils.SHOES_PRODUCT.get(), awardedAuction.getProduct());
        assertEquals(creationTime, awardedAuction.getCreationTime());
        assertEquals(AuctionTestUtils.FIRST_USERNAME, awardedAuction.getSellerUsername());
        assertEquals(closingTime, awardedAuction.getClosingTime());
        assertEquals(WINNING_BID, awardedAuction.getWinningBid());
        assertEquals(AuctionTestUtils.AUCTION_PRICE, awardedAuction.getStartingPrice());
        assertEquals(AuctionTestUtils.SECOND_USERNAME, awardedAuction.getWinningBid().getBidderUsername());
        assertEquals(AuctionTestUtils.CONTACTS.get(), awardedAuction.getSellerContacts());
        assertEquals(AuctionTestUtils.CONTACTS.get(), awardedAuction.getSellerContacts());
        assertEquals(AuctionTestUtils.CONTACTS.get(), awardedAuction.getBuyerContacts());
        assertNotNull(awardedAuction.getPdfReport().getPdfReportBytes(),
                "The pdf report must be present");
    }

    @Test
    void lostAuctionExceptionTest() {
        final LostAuction createdEndedAuction = LostAuction.builder().auctionId(AuctionTestUtils.AUCTION_TEST_ID)
                .product(AuctionTestUtils.SHOES_PRODUCT.get()).creationTime(creationTime)
                .sellerUsername(AuctionTestUtils.FIRST_USERNAME).startingPrice(AuctionTestUtils.AUCTION_PRICE)
                .closingTime(closingTime).winningBid(WINNING_BID).build();

        assertEquals(AuctionTestUtils.AUCTION_TEST_ID, createdEndedAuction.getAuctionId());
        assertEquals(AuctionTestUtils.SHOES_PRODUCT.get(), createdEndedAuction.getProduct());
        assertEquals(creationTime, createdEndedAuction.getCreationTime());
        assertEquals(AuctionTestUtils.FIRST_USERNAME, createdEndedAuction.getSellerUsername());
        assertEquals(closingTime, createdEndedAuction.getClosingTime());
        assertEquals(WINNING_BID, createdEndedAuction.getWinningBid());
        assertEquals(AuctionTestUtils.SECOND_USERNAME, createdEndedAuction.getWinningBid().getBidderUsername());
        assertEquals(AuctionTestUtils.AUCTION_PRICE, createdEndedAuction.getStartingPrice());
    }
}
