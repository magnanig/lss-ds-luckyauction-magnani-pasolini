/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.domain;

import it.unibo.luckyauction.auction.domain.util.PdfGenerator;
import it.unibo.luckyauction.auction.domain.valueobject.AwardedAuction;
import it.unibo.luckyauction.auction.domain.valueobject.Bid;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import static it.unibo.luckyauction.util.NumericalConstants.TWO;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class PdfFileGenerationTests {

    private static final String OUTPUT_PATH = "./src/test/resources/";
    private static final String FILE_NAME = "auctionSummary.pdf";

    @SuppressWarnings("PMD.UnusedPrivateMethod")
    // it is useful if you want to save a file locally
    private void pdfGeneratorTest() {
        final Bid winningBid = new Bid(AuctionTestUtils.BID_RAISE_AMOUNT, AuctionTestUtils.SECOND_USERNAME);
        final Calendar twoDaysBefore = Calendar.getInstance();
        twoDaysBefore.add(Calendar.DATE, -TWO);

        final AwardedAuction awardedAuction = AwardedAuction.builder()
                .auctionId(AuctionTestUtils.AUCTION_TEST_ID)
                .creationTime(twoDaysBefore)
                .product(AuctionTestUtils.SHOES_PRODUCT.get()).sellerUsername(AuctionTestUtils.FIRST_USERNAME)
                .startingPrice(AuctionTestUtils.AUCTION_PRICE)
                .winningBid(winningBid)
                .closingTime(Calendar.getInstance())
                .build();

        PdfGenerator.auctionPdfGenerator(awardedAuction, AuctionTestUtils.CONTACTS.get(),
                AuctionTestUtils.CONTACTS.get(), OUTPUT_PATH + FILE_NAME);
        assertTrue(new File(OUTPUT_PATH + FILE_NAME).exists(), "The file is present");
    }

    @Test
    void calendarTimestampTest() {
        // "02 feb 2022 alle 15:05:19"
        final int year = 2022;
        final int day = 2;
        final int hourOfDay = 16;
        final int minute = 7;
        final int second = 14;
        final Calendar specificTime = Calendar.getInstance();
        specificTime.set(year, Calendar.FEBRUARY, day, hourOfDay, minute, second);
        final String date = new SimpleDateFormat("dd MMM yyyy", Locale.ITALIAN).format(specificTime.getTime());
        final String time = new SimpleDateFormat("HH:mm:ss", Locale.ITALIAN).format(specificTime.getTime());

        assertEquals("02 feb 2022", date);
        assertEquals("16:07:14", time);
    }
}
