/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.domain;

import it.unibo.luckyauction.auction.domain.entity.Auction;
import it.unibo.luckyauction.auction.domain.valueobject.Bid;
import it.unibo.luckyauction.util.CalendarUtils;
import it.unibo.luckyauction.util.Sleeper;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.util.Calendar;
import java.util.Optional;
import java.util.function.Supplier;

import static it.unibo.luckyauction.auction.domain.config.AuctionConstants.getAuctionEndingZone;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ActiveAuctionTests {

    private final Duration fastAuctionDuration = Duration.ofMillis(100);

    private final Sleeper s = new Sleeper();
    private final Supplier<Auction.AuctionBuilder> preBuiltAuction = () -> Auction.builder()
            .auctionId(AuctionTestUtils.AUCTION_TEST_ID)
            .product(AuctionTestUtils.SHOES_PRODUCT.get())
            .sellerUsername(AuctionTestUtils.FIRST_USERNAME)
            .startingPrice(AuctionTestUtils.AUCTION_PRICE)
            .bidsHistoryId(AuctionTestUtils.BIDS_HISTORY_ID);

    @Test
    void lifeNotExtensionWithoutBidTest() throws InterruptedException {
        final Auction nonExtendableAuction = preBuiltAuction.get().duration(fastAuctionDuration).build();
        nonExtendableAuction.start();
        final Calendar expectedClosingTime = CalendarUtils.add(
                nonExtendableAuction.getCreationTime(), fastAuctionDuration);

        // wait not to receive bids until the end
        s.sleepEquallyMoreThan(fastAuctionDuration.toMillis());
        assertFalse(nonExtendableAuction.isActive());
        assertTrue(nonExtendableAuction.getBids().isEmpty());
        assertTrue(nonExtendableAuction.getWinningBid().isEmpty());
        // the expected closing time shouldn't have extended
        // don't use equals because it is normal to have some decime of difference
        assertTrue(CalendarUtils.diff(nonExtendableAuction.getClosingTime().orElseThrow(),
                expectedClosingTime).getSeconds() < 1);
    }

    @Test
    void lifeNotExtensionWithBidTest() throws InterruptedException {
        final Bid bid = new Bid(AuctionTestUtils.BID_RAISE_AMOUNT, AuctionTestUtils.SECOND_USERNAME);
        final Auction nonExtendableAuction = preBuiltAuction.get().duration(fastAuctionDuration).build();
        nonExtendableAuction.start();
        final Calendar expectedClosingTime = CalendarUtils.add(
                nonExtendableAuction.getCreationTime(), fastAuctionDuration);

        assertTrue(nonExtendableAuction.getActualBid().isEmpty());
        // add a bid before entering the ending zone
        assertFalse(nonExtendableAuction.addNewBid(bid),
                "The auction duration has not been extended because we are not in the end zone");
        assertEquals(Optional.of(bid), nonExtendableAuction.getActualBid());
        assertTrue(nonExtendableAuction.isActive());
        // wait for auction closure
        s.sleepEquallyMoreThan(fastAuctionDuration.toMillis());
        assertFalse(nonExtendableAuction.isActive());
        assertTrue(nonExtendableAuction.getWinningBid().isPresent());
        assertEquals(bid, nonExtendableAuction.getWinningBid().get());
        // the expected closing time shouldn't have extended
        // don't use equals because it is normal to have some decime of difference
        assertTrue(CalendarUtils.diff(nonExtendableAuction.getClosingTime().orElseThrow(),
                expectedClosingTime).getSeconds() < 1);
    }

    @Test
    void lifeExtensionWithoutBidTest() throws InterruptedException {
        final Auction extendableAuction = preBuiltAuction.get()
                .duration(fastAuctionDuration)
                .extensionDuration(fastAuctionDuration).build();
        extendableAuction.start();
        final Calendar initialExpectedClosingTime = extendableAuction.getExpectedClosingTime();
        final Calendar newExpectedClosingTime = CalendarUtils.add(initialExpectedClosingTime, fastAuctionDuration);

        assertTrue(extendableAuction.getExtensionAmount().isPresent());
        assertEquals(fastAuctionDuration, extendableAuction.getExtensionAmount().get());
        // wait not to receive bids until the auction's initial duration
        s.sleepEquallyMoreThan(fastAuctionDuration.toMillis()); // 100ms
        assertTrue(extendableAuction.isActive());
        assertTrue(extendableAuction.getBids().isEmpty());
        assertTrue(extendableAuction.getActualBid().isEmpty());
        // the expected closing time should have been extended
        assertEquals(newExpectedClosingTime, extendableAuction.getExpectedClosingTime());
        // wait for auction closure
        s.sleepEquallyMoreThan(fastAuctionDuration.toMillis()); // 200ms
        assertTrue(extendableAuction.getWinningBid().isEmpty());
        // don't use equals because it is normal to have some decime of difference
        assertTrue(CalendarUtils.diff(extendableAuction.getClosingTime().orElseThrow(),
                newExpectedClosingTime).getSeconds() < 1);
    }

    @Test
    void lifeExtensionWithBidTest() throws InterruptedException {
        final Duration endingZoneDuration = getAuctionEndingZone(fastAuctionDuration);
        final Long theoreticalRemainingTime = endingZoneDuration.toMillis();
        final Long timeToEnterInEndingZone = fastAuctionDuration.minus(endingZoneDuration).toMillis();
        final Bid bid = new Bid(AuctionTestUtils.BID_RAISE_AMOUNT, AuctionTestUtils.SECOND_USERNAME);
        final Auction extendableAuction = preBuiltAuction.get().duration(fastAuctionDuration).build();
        extendableAuction.start();
        final Calendar initialExpectedClosingTime = extendableAuction.getExpectedClosingTime();
        final Calendar expectedClosingTimeAfterBid = CalendarUtils.add(initialExpectedClosingTime, endingZoneDuration);

        // wait for the necessary time to enter to ending zone
        s.sleepEquallyMoreThan(timeToEnterInEndingZone); // 50ms
        // add new bid in ending zone
        assertTrue(extendableAuction.addNewBid(bid),
                "The auction duration has been extended because we are in the end zone");
        assertEquals(Optional.of(bid), extendableAuction.getActualBid());
        s.sleepEquallyMoreThan(theoreticalRemainingTime); // 100ms
        // the auction must still be active
        assertTrue(extendableAuction.isActive());
        // the expected closing time should extend
        assertEquals(expectedClosingTimeAfterBid, extendableAuction.getExpectedClosingTime());
        // wait for effective auction closure (i.e. for the extension duration time)
        s.sleepEquallyMoreThan(endingZoneDuration.toMillis()); // 150ms
        assertFalse(extendableAuction.isActive());
        assertTrue(extendableAuction.getWinningBid().isPresent());
        assertEquals(bid, extendableAuction.getWinningBid().get());
        // don't use equals because it is normal to have some decime of difference
        assertTrue(CalendarUtils.diff(extendableAuction.getClosingTime().orElseThrow(),
                expectedClosingTimeAfterBid).getSeconds() < 1);
    }
}
