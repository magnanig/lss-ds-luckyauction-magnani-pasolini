/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.domain;

import it.unibo.luckyauction.auction.domain.exception.BidValidationException;
import it.unibo.luckyauction.auction.domain.valueobject.Bid;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Calendar;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class BidTests {

    private final BigDecimal negativeAmount = BigDecimal.valueOf(-9, 99); // 9.99
    private final BigDecimal amountWithThreeDecimal = BigDecimal.valueOf(25, 876); // 25.876

    @Test
    void bidExceptionsTest() {
        final Exception usernameException = assertThrows(BidValidationException.class,
                () -> new Bid(AuctionTestUtils.BID_RAISE_AMOUNT, ""));

        final Exception firstAmountException = assertThrows(BidValidationException.class,
                () -> new Bid(null, AuctionTestUtils.FIRST_USERNAME));

        final Exception secondAmountException = assertThrows(BidValidationException.class,
                () -> new Bid(negativeAmount, AuctionTestUtils.FIRST_USERNAME));

        final Exception thirdAmountException = assertThrows(BidValidationException.class,
                () -> new Bid(amountWithThreeDecimal, AuctionTestUtils.FIRST_USERNAME));

        assertTrue(usernameException.getMessage().contains("username is not set"));
        assertTrue(firstAmountException.getMessage().contains("amount is not set"));
        assertTrue(secondAmountException.getMessage().contains("must be greater than zero"));
        assertTrue(thirdAmountException.getMessage().contains("can have at most two decimal digits"));
    }

    @Test
    void bidGettersTest() {
        final Bid bidWithoutTimestamp = new Bid(AuctionTestUtils.BID_RAISE_AMOUNT,
                AuctionTestUtils.FIRST_USERNAME, null);
        assertNotNull(bidWithoutTimestamp.getTimestamp(), "Timestamp must be set");
        assertTrue(bidWithoutTimestamp.getTimestamp().isSet(Calendar.DATE), "The date must be set");

        final Calendar timestamp = Calendar.getInstance();
        final Bid firstBid = new Bid(AuctionTestUtils.BID_RAISE_AMOUNT, AuctionTestUtils.FIRST_USERNAME, timestamp);
        final Bid secondBid = new Bid(AuctionTestUtils.BID_RAISE_AMOUNT, AuctionTestUtils.FIRST_USERNAME, timestamp);

        assertEquals(firstBid, secondBid);
        assertEquals(timestamp, firstBid.getTimestamp());
        assertEquals(AuctionTestUtils.BID_RAISE_AMOUNT, firstBid.getAmount());
        assertEquals(AuctionTestUtils.FIRST_USERNAME, firstBid.getBidderUsername());
    }
}
