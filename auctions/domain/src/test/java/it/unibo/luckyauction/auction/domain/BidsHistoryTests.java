/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.domain;

import it.unibo.luckyauction.auction.domain.entity.BidsHistory;
import it.unibo.luckyauction.auction.domain.valueobject.Bid;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class BidsHistoryTests {

    private static final Bid FIRST_BID = new Bid(AuctionTestUtils.BID_RAISE_AMOUNT, AuctionTestUtils.FIRST_USERNAME);
    private static final Bid SECOND_BID = new Bid(AuctionTestUtils.NEW_BID_RAISE_AMOUNT, AuctionTestUtils.SECOND_USERNAME);

    @Test
    void emptyBidsHistoryTest() {
        final BidsHistory bidsHistory = new BidsHistory(AuctionTestUtils.BIDS_HISTORY_ID);
        final BidsHistory otherBidsHistory = new BidsHistory(AuctionTestUtils.BIDS_HISTORY_ID);

        assertTrue(bidsHistory.getBids().isEmpty(), "Bids list must be empty");
        assertEquals(Collections.emptyList(), bidsHistory.getBids());
        assertEquals(bidsHistory, otherBidsHistory);
    }

    @Test
    void fullBidsHistoryTest() {
        final BidsHistory bidsHistory = new BidsHistory(
                AuctionTestUtils.BIDS_HISTORY_ID, List.of(FIRST_BID, SECOND_BID));

        assertTrue(bidsHistory.getBids().contains(FIRST_BID), "Bids list must contain firstBid");
        assertTrue(bidsHistory.getBids().contains(SECOND_BID), "Bids list must contain secondBid");
        assertEquals(List.of(FIRST_BID, SECOND_BID), bidsHistory.getBids());
        assertEquals(AuctionTestUtils.BIDS_HISTORY_ID, bidsHistory.getBidsHistoryId());
    }

    @Test
    void addNewBidsTest() {
        final BidsHistory bidsHistory = new BidsHistory(AuctionTestUtils.BIDS_HISTORY_ID);

        bidsHistory.addBid(FIRST_BID);
        assertEquals(List.of(FIRST_BID), bidsHistory.getBids());
        bidsHistory.addBid(SECOND_BID);
        assertEquals(List.of(FIRST_BID, SECOND_BID), bidsHistory.getBids());
    }

    @Test
    void addAllBidsTest() {
        final BidsHistory bidsHistory = new BidsHistory(AuctionTestUtils.BIDS_HISTORY_ID);

        bidsHistory.addAllBids(List.of(FIRST_BID, SECOND_BID));
        assertEquals(List.of(FIRST_BID, SECOND_BID), bidsHistory.getBids());
    }
}
