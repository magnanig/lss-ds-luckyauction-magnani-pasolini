/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.domain;

import it.unibo.luckyauction.auction.domain.valueobject.ProductCategory;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ProductCategoryEnumTest {

    @Test
    void productCategoryTest() {
        assertThrows(IllegalArgumentException.class, () -> ProductCategory.valueOf("Clothing"));
        assertThrows(IllegalArgumentException.class, () -> ProductCategory.valueOf("clothing"));
        assertNotNull(ProductCategory.valueOf("CLOTHING"));
        assertNotNull(ProductCategory.valueOf("GAMES"));
        assertNotNull(ProductCategory.valueOf("VIDEO_GAMES"));
        assertNotNull(ProductCategory.valueOf("VEHICLES"));
        assertNotNull(ProductCategory.valueOf("CD_AND_VINYLS"));
        assertNotNull(ProductCategory.valueOf("ELECTRONICS"));
        assertNotNull(ProductCategory.valueOf("JEWELS"));
        assertNotNull(ProductCategory.valueOf("HOME_APPLIANCES"));
        assertNotNull(ProductCategory.valueOf("INFORMATICS"));
        assertNotNull(ProductCategory.valueOf("BOOKS"));
        assertNotNull(ProductCategory.valueOf("WATCHES"));
        assertNotNull(ProductCategory.valueOf("HANDMADE_PRODUCTS"));
        assertNotNull(ProductCategory.valueOf("SHOES_AND_BAGS"));
        assertNotNull(ProductCategory.valueOf("MUSICAL_INSTRUMENTS"));
        assertNotNull(ProductCategory.valueOf("OTHER"));
    }

    @Test
    void productCategoryListTest() {
        assertEquals(
                List.of(ProductCategory.CLOTHING, ProductCategory.GAMES, ProductCategory.VIDEO_GAMES,
                        ProductCategory.VEHICLES, ProductCategory.CD_AND_VINYLS, ProductCategory.ELECTRONICS,
                        ProductCategory.JEWELS, ProductCategory.HOME_APPLIANCES, ProductCategory.INFORMATICS,
                        ProductCategory.BOOKS, ProductCategory.WATCHES, ProductCategory.HANDMADE_PRODUCTS,
                        ProductCategory.SHOES_AND_BAGS, ProductCategory.MUSICAL_INSTRUMENTS, ProductCategory.OTHER),
                Arrays.stream(ProductCategory.values()).toList()
        );
    }
}
