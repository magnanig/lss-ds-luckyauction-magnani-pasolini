/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.domain;

import it.unibo.luckyauction.auction.domain.exception.AuctionValidationException;
import it.unibo.luckyauction.auction.domain.valueobject.Bid;
import it.unibo.luckyauction.auction.domain.valueobject.CreatedOngoingAuction;
import it.unibo.luckyauction.auction.domain.valueobject.ParticipatingAuction;
import it.unibo.luckyauction.util.CalendarUtils;
import it.unibo.luckyauction.util.Sleeper;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.util.Calendar;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class SummaryOngoingAuctionTests {
    private static final Duration TWENTY_MILLIS = Duration.ofMillis(20);
    private static final Calendar CREATION_TIME = Calendar.getInstance();
    private static final Bid BID = new Bid(AuctionTestUtils.BID_RAISE_AMOUNT, AuctionTestUtils.SECOND_USERNAME);

    @Test
    void createdOngoingAuctionWithoutDurationTest() {
        final Calendar afterOneHundredSeconds = Calendar.getInstance();
        afterOneHundredSeconds.add(Calendar.MILLISECOND, 100);
        // test builder only with expectedClosingTime, without duration and creationTime
        final CreatedOngoingAuction createdOngoingAuction = CreatedOngoingAuction.builder()
                .auctionId(AuctionTestUtils.AUCTION_TEST_ID).product(AuctionTestUtils.SHOES_PRODUCT.get())
                .sellerUsername(AuctionTestUtils.FIRST_USERNAME).startingPrice(AuctionTestUtils.AUCTION_PRICE)
                .expectedClosingTime(afterOneHundredSeconds).build();
        assertEquals(afterOneHundredSeconds, createdOngoingAuction.getExpectedClosingTime());
        assertNotSame(afterOneHundredSeconds, createdOngoingAuction.getExpectedClosingTime());
        // test builder exception without both duration and expectedClosingTime
        final Exception durationException = assertThrows(AuctionValidationException.class,
                () -> CreatedOngoingAuction.builder().auctionId(AuctionTestUtils.AUCTION_TEST_ID)
                        .product(AuctionTestUtils.SHOES_PRODUCT.get()).sellerUsername(AuctionTestUtils.FIRST_USERNAME)
                        .startingPrice(AuctionTestUtils.AUCTION_PRICE).build());
        assertTrue(durationException.getMessage().contains("Both auction duration and expected closing time"
                + " are not set. Please specify at least one of them"));
    }
    @Test
    void createdOngoingAuctionTest() throws InterruptedException {
        final CreatedOngoingAuction createdOngoingAuction = CreatedOngoingAuction.builder()
                .auctionId(AuctionTestUtils.AUCTION_TEST_ID)
                .product(AuctionTestUtils.SHOES_PRODUCT.get())
                .creationTime(CREATION_TIME)
                .sellerUsername(AuctionTestUtils.FIRST_USERNAME)
                .startingPrice(AuctionTestUtils.AUCTION_PRICE)
                .duration(AuctionTestUtils.TWO_DAYS_DURATION)
                .build();

        assertEquals(AuctionTestUtils.AUCTION_TEST_ID, createdOngoingAuction.getAuctionId());
        assertEquals(AuctionTestUtils.SHOES_PRODUCT.get(), createdOngoingAuction.getProduct());
        assertEquals(CREATION_TIME, createdOngoingAuction.getCreationTime());
        assertEquals(AuctionTestUtils.FIRST_USERNAME, createdOngoingAuction.getSellerUsername());
        assertEquals(AuctionTestUtils.AUCTION_PRICE, createdOngoingAuction.getStartingPrice());

        final Sleeper s = new Sleeper();
        s.sleepEquallyMoreThan(TWENTY_MILLIS.toMillis());

        assertTrue(createdOngoingAuction.getActualBid().isEmpty());
        assertTrue(createdOngoingAuction.getExtensionAmount().isEmpty());
        assertTrue(createdOngoingAuction.remainingTime().compareTo(AuctionTestUtils.TWO_DAYS_DURATION) < 0,
                "The remaining time must be less than the initial duration");
        assertEquals(AuctionTestUtils.TWO_DAYS_DURATION, createdOngoingAuction.getDuration());
        assertEquals(CalendarUtils.add(CREATION_TIME, AuctionTestUtils.TWO_DAYS_DURATION),
                createdOngoingAuction.getExpectedClosingTime());
    }

    @Test
    void participatingAuctionTest() {
        final ParticipatingAuction participatingAuction = ParticipatingAuction.builder()
                .auctionId(AuctionTestUtils.AUCTION_TEST_ID)
                .product(AuctionTestUtils.SHOES_PRODUCT.get())
                .creationTime(CREATION_TIME)
                .sellerUsername(AuctionTestUtils.FIRST_USERNAME)
                .startingPrice(AuctionTestUtils.AUCTION_PRICE)
                .actualBid(BID)
                .duration(AuctionTestUtils.TWO_DAYS_DURATION)
                .build();

        assertEquals(AuctionTestUtils.AUCTION_TEST_ID, participatingAuction.getAuctionId());
        assertEquals(AuctionTestUtils.SHOES_PRODUCT.get(), participatingAuction.getProduct());
        assertEquals(CREATION_TIME, participatingAuction.getCreationTime());
        assertEquals(AuctionTestUtils.FIRST_USERNAME, participatingAuction.getSellerUsername());
        assertEquals(AuctionTestUtils.AUCTION_PRICE, participatingAuction.getStartingPrice());

        assertTrue(participatingAuction.getActualBid().isPresent());
        assertEquals(BID, participatingAuction.getActualBid().get());
        assertTrue(participatingAuction.getExtensionAmount().isEmpty());
        assertTrue(participatingAuction.remainingTime().compareTo(AuctionTestUtils.TWO_DAYS_DURATION) < 0);
        assertEquals(AuctionTestUtils.TWO_DAYS_DURATION, participatingAuction.getDuration());
        assertEquals(CalendarUtils.add(CREATION_TIME, AuctionTestUtils.TWO_DAYS_DURATION),
                participatingAuction.getExpectedClosingTime());
    }
}
