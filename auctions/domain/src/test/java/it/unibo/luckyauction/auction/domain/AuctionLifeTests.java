/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.domain;

import it.unibo.luckyauction.auction.domain.entity.AuctionLife;
import it.unibo.luckyauction.util.CalendarUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.time.Duration;
import java.util.Calendar;

import static it.unibo.luckyauction.auction.domain.config.AuctionConstants.getMaxAuctionDuration;
import static it.unibo.luckyauction.util.NumericalConstants.TWO;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class AuctionLifeTests {

    private static final Duration CORRECT_DURATION = getMaxAuctionDuration();

    private final Calendar currentTime = Calendar.getInstance();
    private final Calendar afterTwoDays = Calendar.getInstance();
    private final Calendar twoDaysBefore = Calendar.getInstance();

    @BeforeAll
    void config() {
        twoDaysBefore.add(Calendar.DATE, -TWO);
        afterTwoDays.add(Calendar.DATE, TWO);
    }

    @Test
    void auctionLifeWithoutDurationTest() {
        final AuctionLife lifeWithoutDuration = AuctionLife.builder().expectedClosingTime(afterTwoDays).build();
        assertEquals(afterTwoDays, lifeWithoutDuration.getExpectedClosingTime());
        assertEquals(CalendarUtils.diff(lifeWithoutDuration.getExpectedClosingTime(),
                lifeWithoutDuration.getCreationTime()), lifeWithoutDuration.getDuration());
    }

    @Test
    void auctionLifeWithClosingTimeTest() {
        final AuctionLife auctionLife = AuctionLife.builder().creationTime(twoDaysBefore)
                .closingTime(currentTime).build();

        assertEquals(twoDaysBefore, auctionLife.getCreationTime());
        assertEquals(CalendarUtils.diff(currentTime, twoDaysBefore), auctionLife.getDuration());
        assertEquals(currentTime, auctionLife.getExpectedClosingTime());
        assertTrue(auctionLife.getClosingTime().isPresent(), "Closing time is set");
        assertEquals(currentTime, auctionLife.getClosingTime().get());
        assertTrue(auctionLife.getExtensionAmount().isEmpty(), "Extension amount is not set");
    }

    @Test
    void auctionLifeTest() throws InterruptedException {
        final AuctionLife auctionLife = AuctionLife.builder()
                .duration(CORRECT_DURATION).extensionDuration(CORRECT_DURATION).build();
        final Calendar initialExpectedClosingTime = CalendarUtils.add(auctionLife.getCreationTime(), CORRECT_DURATION);
        final Calendar finalExpectedClosingTime = CalendarUtils.add(initialExpectedClosingTime, CORRECT_DURATION);

        assertEquals(Calendar.getInstance().get(Calendar.DAY_OF_MONTH),
                auctionLife.getCreationTime().get(Calendar.DAY_OF_MONTH));
        assertTrue(auctionLife.getExtensionAmount().isPresent(), "Automatic extension has been set");
        assertEquals(CORRECT_DURATION, auctionLife.getExtensionAmount().get());
        assertEquals(CORRECT_DURATION, auctionLife.getDuration());
        assertEquals(initialExpectedClosingTime, auctionLife.getExpectedClosingTime());
        assertTrue(auctionLife.getClosingTime().isEmpty(), "The object must be empty because the auction life "
                + "has not yet ended");

        assertFalse(auctionLife.isEnded());
        assertTrue(auctionLife.isOngoing());
        assertFalse(auctionLife.isInEndingZone(), "It's not in ending zone because there are more than 30 "
                + "minutes left for conclusion");
        Thread.sleep(10);
        assertTrue(auctionLife.remainingTime().compareTo(CORRECT_DURATION) < 0, "The remaining time "
                + "must be less than the initial duration");

        // notify that the auction has been reached, saying that no bids have been received so far
        auctionLife.notifyReachedExpectedClosingTime(true);
        assertEquals(finalExpectedClosingTime, auctionLife.getExpectedClosingTime());
        assertTrue(auctionLife.getExtensionAmount().isEmpty(), "The auction duration has already been "
                + "extended and cannot be extended in the future");
        assertTrue(auctionLife.isOngoing(), "The auction is still active");
        assertTrue(auctionLife.getClosingTime().isEmpty(), "The auction is still active");

        // notify a new raise
        auctionLife.notifyBidRaised();
        // check that the expected closing time has not been extended, as we are not in the ending zone
        assertEquals(finalExpectedClosingTime, auctionLife.getExpectedClosingTime());

        // notify that the auction has been reached, saying that bids have received at least one bid
        auctionLife.notifyReachedExpectedClosingTime(false);
        // check that the auction has actually been closed
        assertTrue(auctionLife.isEnded(), "The auction is closed");
        assertFalse(auctionLife.isOngoing(), "The auction must not be active");
        assertTrue(auctionLife.getClosingTime().isPresent(), "The auction is closed");
    }
}
