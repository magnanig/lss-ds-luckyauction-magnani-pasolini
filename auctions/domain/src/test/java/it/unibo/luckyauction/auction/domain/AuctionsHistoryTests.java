/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.domain;

import it.unibo.luckyauction.auction.domain.entity.AuctionsHistory;
import it.unibo.luckyauction.auction.domain.valueobject.AwardedAuction;
import it.unibo.luckyauction.auction.domain.valueobject.Bid;
import it.unibo.luckyauction.auction.domain.valueobject.CreatedEndedAuction;
import it.unibo.luckyauction.auction.domain.valueobject.CreatedOngoingAuction;
import it.unibo.luckyauction.auction.domain.valueobject.LostAuction;
import it.unibo.luckyauction.auction.domain.valueobject.ParticipatingAuction;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.function.Supplier;

import static it.unibo.luckyauction.auction.domain.AuctionTestUtils.FIRST_AUCTIONS_HISTORY_ID;
import static it.unibo.luckyauction.auction.domain.AuctionTestUtils.AUCTION_PRICE;
import static it.unibo.luckyauction.auction.domain.AuctionTestUtils.BID_RAISE_AMOUNT;
import static it.unibo.luckyauction.auction.domain.AuctionTestUtils.SHOES_PRODUCT;
import static it.unibo.luckyauction.auction.domain.AuctionTestUtils.TWO_DAYS_DURATION;
import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class AuctionsHistoryTests {

    private static final Supplier<AuctionsHistory> AUCTIONS_HISTORY_SUPPLIER =
            () -> new AuctionsHistory(FIRST_AUCTIONS_HISTORY_ID, AuctionTestUtils.FIRST_USERNAME);

    private Calendar oneDayBefore;

    @BeforeAll
    void config() {
        oneDayBefore = Calendar.getInstance();
        oneDayBefore.add(Calendar.DATE, -1);
    }

    @Test
    void auctionsHistoryInitializationTest() {
        final AuctionsHistory auctionsHistory = AUCTIONS_HISTORY_SUPPLIER.get();
        assertEquals(FIRST_AUCTIONS_HISTORY_ID, auctionsHistory.getAuctionsHistoryId());
        assertEquals(AuctionTestUtils.FIRST_USERNAME, auctionsHistory.getUsername());
        assertEquals(Collections.emptyList(), auctionsHistory.getCreatedOngoingAuctions());
        assertEquals(Collections.emptyList(), auctionsHistory.getParticipatingAuctions());
        assertEquals(Collections.emptyList(), auctionsHistory.getCreatedEndedAuctions());
        assertEquals(Collections.emptyList(), auctionsHistory.getAwardedAuctions());
        assertEquals(Collections.emptyList(), auctionsHistory.getLostAuctions());
    }

    @Test
    void addAwardedAuctionTest() {
        final AuctionsHistory auctionsHistory = AUCTIONS_HISTORY_SUPPLIER.get();
        final AwardedAuction awardedAuction = AwardedAuction.builder().creationTime(oneDayBefore)
                .product(SHOES_PRODUCT.get()).sellerUsername(AuctionTestUtils.SELLER_USERNAME)
                .sellerContacts(AuctionTestUtils.CONTACTS.get()).startingPrice(AUCTION_PRICE)
                .winningBid(new Bid(BID_RAISE_AMOUNT, AuctionTestUtils.FIRST_USERNAME))
                .buyerContacts(AuctionTestUtils.CONTACTS.get()).closingTime(Calendar.getInstance()).build();

        auctionsHistory.addAwardedAuction(awardedAuction);
        assertEquals(List.of(awardedAuction), auctionsHistory.getAwardedAuctions());
    }

    @Test
    void addLostAuctionTest() {
        final AuctionsHistory auctionsHistory = AUCTIONS_HISTORY_SUPPLIER.get();
        final LostAuction lostAuction = LostAuction.builder().creationTime(oneDayBefore).product(SHOES_PRODUCT.get())
                .sellerUsername(AuctionTestUtils.SELLER_USERNAME).startingPrice(AUCTION_PRICE)
                .winningBid(new Bid(BID_RAISE_AMOUNT, AuctionTestUtils.BUYER_USERNAME))
                .closingTime(Calendar.getInstance()).build();

        auctionsHistory.addLostAuction(lostAuction);
        assertEquals(List.of(lostAuction), auctionsHistory.getLostAuctions());
    }

    @Test
    void addAndRemoveParticipatingAuctionTest() {
        final AuctionsHistory auctionsHistory = AUCTIONS_HISTORY_SUPPLIER.get();
        final ParticipatingAuction participatingAuction = ParticipatingAuction.builder().creationTime(oneDayBefore)
                .product(SHOES_PRODUCT.get()).sellerUsername(AuctionTestUtils.SELLER_USERNAME)
                .startingPrice(AUCTION_PRICE).duration(TWO_DAYS_DURATION).build();

        auctionsHistory.addParticipatingAuction(participatingAuction); // add
        assertEquals(List.of(participatingAuction), auctionsHistory.getParticipatingAuctions());
        auctionsHistory.removeParticipatingAuction(participatingAuction); // remove
        assertEquals(Collections.emptyList(), auctionsHistory.getParticipatingAuctions());
    }

    @Test
    void addCreatedEndedAuctionTest() {
        final AuctionsHistory auctionsHistory = AUCTIONS_HISTORY_SUPPLIER.get();
        final CreatedEndedAuction createdEndedAuction = CreatedEndedAuction.builder().creationTime(oneDayBefore)
                .product(SHOES_PRODUCT.get()).sellerUsername(AuctionTestUtils.FIRST_USERNAME)
                .startingPrice(AUCTION_PRICE).closingTime(Calendar.getInstance()).build();

        auctionsHistory.addCreatedEndedAuction(createdEndedAuction);
        assertEquals(List.of(createdEndedAuction), auctionsHistory.getCreatedEndedAuctions());
    }

    @Test
    void addAndRemoveCreatedOngoingAuctionTest() {
        final AuctionsHistory auctionsHistory = AUCTIONS_HISTORY_SUPPLIER.get();
        final CreatedOngoingAuction createdOngoingAuction = CreatedOngoingAuction.builder().creationTime(oneDayBefore)
                .product(SHOES_PRODUCT.get()).sellerUsername(AuctionTestUtils.FIRST_USERNAME)
                .startingPrice(AUCTION_PRICE).duration(TWO_DAYS_DURATION).build();

        auctionsHistory.addCreatedOngoingAuction(createdOngoingAuction); // add
        assertEquals(List.of(createdOngoingAuction), auctionsHistory.getCreatedOngoingAuctions());
        auctionsHistory.removeCreatedOngoingAuction(createdOngoingAuction); // remove
        assertEquals(Collections.emptyList(), auctionsHistory.getCreatedOngoingAuctions());
    }
}
