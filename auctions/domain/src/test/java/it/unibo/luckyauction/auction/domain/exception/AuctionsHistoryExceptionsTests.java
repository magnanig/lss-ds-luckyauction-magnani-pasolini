/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.domain.exception;

import it.unibo.luckyauction.auction.domain.AuctionTestUtils;
import it.unibo.luckyauction.auction.domain.entity.AuctionsHistory;
import it.unibo.luckyauction.auction.domain.valueobject.AwardedAuction;
import it.unibo.luckyauction.auction.domain.valueobject.Bid;
import it.unibo.luckyauction.auction.domain.valueobject.CreatedEndedAuction;
import it.unibo.luckyauction.auction.domain.valueobject.CreatedOngoingAuction;
import it.unibo.luckyauction.auction.domain.valueobject.LostAuction;
import it.unibo.luckyauction.auction.domain.valueobject.ParticipatingAuction;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.Calendar;
import java.util.Collections;
import java.util.function.Supplier;

import static it.unibo.luckyauction.auction.domain.AuctionTestUtils.FIRST_AUCTIONS_HISTORY_ID;
import static it.unibo.luckyauction.auction.domain.AuctionTestUtils.AUCTION_PRICE;
import static it.unibo.luckyauction.auction.domain.AuctionTestUtils.BID_RAISE_AMOUNT;
import static it.unibo.luckyauction.auction.domain.AuctionTestUtils.SHOES_PRODUCT;
import static it.unibo.luckyauction.auction.domain.AuctionTestUtils.TWO_DAYS_DURATION;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class AuctionsHistoryExceptionsTests {

    private static final String LIST_IS_NULL_EXCEPTION = "At least one list in the auctions history is null";
    private static final String CONSINSTENCY_EXCEPTION = "Consistency error when adding";
    private static final Supplier<AuctionsHistory> AUCTIONS_HISTORY_SUPPLIER =
            () -> new AuctionsHistory(FIRST_AUCTIONS_HISTORY_ID, AuctionTestUtils.FIRST_USERNAME);

    private Calendar oneDayBefore;

    @BeforeAll
    void config() {
        oneDayBefore = Calendar.getInstance();
        oneDayBefore.add(Calendar.DATE, -1);
    }

    @Test
    void addAwardedAuctionExceptionTest() {
        final AwardedAuction awardedAuction = AwardedAuction.builder().creationTime(oneDayBefore)
                .product(SHOES_PRODUCT.get()).sellerUsername(AuctionTestUtils.SELLER_USERNAME)
                .sellerContacts(AuctionTestUtils.CONTACTS.get()).startingPrice(AUCTION_PRICE)
                .winningBid(new Bid(BID_RAISE_AMOUNT, AuctionTestUtils.BUYER_USERNAME))
                .buyerContacts(AuctionTestUtils.CONTACTS.get()).closingTime(Calendar.getInstance()).build();
        // error: the user of the auction history is not the same as the winner of the auction you want to add
        final Exception exception = assertThrows(AuctionsHistoryConsistencyErrorException.class,
                () -> AUCTIONS_HISTORY_SUPPLIER.get().addAwardedAuction(awardedAuction));
        assertTrue(exception.getMessage().contains(CONSINSTENCY_EXCEPTION));
    }

    @Test
    void addLostAuctionExceptionTest() {
        final LostAuction lostAuction = LostAuction.builder().creationTime(oneDayBefore).product(SHOES_PRODUCT.get())
                .sellerUsername(AuctionTestUtils.SELLER_USERNAME).startingPrice(AUCTION_PRICE)
                .winningBid(new Bid(BID_RAISE_AMOUNT, AuctionTestUtils.FIRST_USERNAME))
                .closingTime(Calendar.getInstance()).build();
        // error: the user of the auction history is the same as the winner of the auction you want to add
        final Exception exception = assertThrows(AuctionsHistoryConsistencyErrorException.class,
                () -> AUCTIONS_HISTORY_SUPPLIER.get().addLostAuction(lostAuction));
        assertTrue(exception.getMessage().contains(CONSINSTENCY_EXCEPTION));
    }

    @Test
    void addParticipatingAuctionExceptionTest() {
        final ParticipatingAuction participatingAuction = ParticipatingAuction.builder().creationTime(oneDayBefore)
                .product(SHOES_PRODUCT.get()).sellerUsername(AuctionTestUtils.FIRST_USERNAME)
                .startingPrice(AUCTION_PRICE).duration(TWO_DAYS_DURATION).build();
        // error: the user of the auction history is the same as the seller of the auction you want to add
        final Exception exception = assertThrows(AuctionsHistoryConsistencyErrorException.class,
                () -> AUCTIONS_HISTORY_SUPPLIER.get().addParticipatingAuction(participatingAuction));
        assertTrue(exception.getMessage().contains(CONSINSTENCY_EXCEPTION));
    }

    @Test
    void addCreatedEndedAuctionExceptionTest() {
        final CreatedEndedAuction createdEndedAuction = CreatedEndedAuction.builder().creationTime(oneDayBefore)
                .product(SHOES_PRODUCT.get()).sellerUsername(AuctionTestUtils.SELLER_USERNAME)
                .startingPrice(AUCTION_PRICE).closingTime(Calendar.getInstance()).build();
        // error: the user of the auction history does not correspond with the seller of the auction you want to add
        final Exception exception = assertThrows(AuctionsHistoryConsistencyErrorException.class,
                () -> AUCTIONS_HISTORY_SUPPLIER.get().addCreatedEndedAuction(createdEndedAuction));
        assertTrue(exception.getMessage().contains(CONSINSTENCY_EXCEPTION));
    }

    @Test
    void addCreatedOngoingAuctionExceptionTest() {
        final CreatedOngoingAuction createdOngoingAuction = CreatedOngoingAuction.builder().creationTime(oneDayBefore)
                .product(SHOES_PRODUCT.get()).sellerUsername(AuctionTestUtils.SELLER_USERNAME)
                .startingPrice(AUCTION_PRICE).duration(TWO_DAYS_DURATION).build();
        // error: the user of the auction history does not correspond with the seller of the auction you want to add
        final Exception exception = assertThrows(AuctionsHistoryConsistencyErrorException.class,
                () -> AUCTIONS_HISTORY_SUPPLIER.get().addCreatedOngoingAuction(createdOngoingAuction));
        assertTrue(exception.getMessage().contains(CONSINSTENCY_EXCEPTION));
    }

    @Test
    void initializationExceptionsTest() {
        final Exception usernameException = assertThrows(AuctionsHistoryValidationException.class,
                () -> new AuctionsHistory(FIRST_AUCTIONS_HISTORY_ID, ""));
        assertTrue(usernameException.getMessage().contains("Auctions history username is not set"));

        final Exception awardedAuctionsException = assertThrows(AuctionsHistoryValidationException.class,
                () -> new AuctionsHistory(FIRST_AUCTIONS_HISTORY_ID, "", null,
                        Collections.emptyList(), Collections.emptyList(), Collections.emptyList(),
                        Collections.emptyList()));
        assertTrue(awardedAuctionsException.getMessage().contains(LIST_IS_NULL_EXCEPTION));

        final Exception lostAuctionsException = assertThrows(AuctionsHistoryValidationException.class,
                () -> new AuctionsHistory(FIRST_AUCTIONS_HISTORY_ID, "", Collections.emptyList(), null,
                        Collections.emptyList(), Collections.emptyList(), Collections.emptyList()));
        assertTrue(lostAuctionsException.getMessage().contains(LIST_IS_NULL_EXCEPTION));

        final Exception participatingAuctionsException = assertThrows(AuctionsHistoryValidationException.class,
                () -> new AuctionsHistory(FIRST_AUCTIONS_HISTORY_ID, "", Collections.emptyList(),
                        Collections.emptyList(), null, Collections.emptyList(),
                        Collections.emptyList()));
        assertTrue(participatingAuctionsException.getMessage().contains(LIST_IS_NULL_EXCEPTION));

        final Exception createdOngoingAuctionsException = assertThrows(AuctionsHistoryValidationException.class,
                () -> new AuctionsHistory(FIRST_AUCTIONS_HISTORY_ID, "", Collections.emptyList(),
                        Collections.emptyList(), Collections.emptyList(), null,
                        Collections.emptyList()));
        assertTrue(createdOngoingAuctionsException.getMessage().contains(LIST_IS_NULL_EXCEPTION));

        final Exception createdEndedAuctionsException = assertThrows(AuctionsHistoryValidationException.class,
                () -> new AuctionsHistory(FIRST_AUCTIONS_HISTORY_ID, "", Collections.emptyList(),
                        Collections.emptyList(), Collections.emptyList(), Collections.emptyList(),
                        null));
        assertTrue(createdEndedAuctionsException.getMessage().contains(LIST_IS_NULL_EXCEPTION));
    }
}
