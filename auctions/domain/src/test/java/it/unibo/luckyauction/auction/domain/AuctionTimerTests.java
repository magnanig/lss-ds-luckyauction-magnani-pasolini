/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.domain;

import it.unibo.luckyauction.auction.domain.entity.Auction;
import it.unibo.luckyauction.util.NumericalConstants;
import it.unibo.luckyauction.util.Sleeper;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestMethodOrder;

import java.time.Duration;
import java.util.Calendar;
import java.util.function.Supplier;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class AuctionTimerTests {

    private final Supplier<Auction.AuctionBuilder> preBuiltAuction = () -> Auction.builder()
            .auctionId(AuctionTestUtils.AUCTION_TEST_ID)
            .product(AuctionTestUtils.SHOES_PRODUCT.get())
            .sellerUsername(AuctionTestUtils.FIRST_USERNAME)
            .startingPrice(AuctionTestUtils.AUCTION_PRICE)
            .bidsHistoryId(AuctionTestUtils.BIDS_HISTORY_ID);

    private final Calendar currentTime = Calendar.getInstance();
    private final Calendar twoDaysBefore = Calendar.getInstance();
    private Sleeper s = new Sleeper();

    @BeforeAll
    void config() {
        twoDaysBefore.add(Calendar.DATE, -NumericalConstants.TWO);
    }

    @Test
    @Order(1)
    void timerExceptionTest() {
        final Auction endedAuction = preBuiltAuction.get()
                .creationTime(twoDaysBefore).closingTime(currentTime).build();

        final Exception timerException = assertThrows(IllegalStateException.class, endedAuction::start);
        assertTrue(timerException.getMessage().contains("Auction has already been closed"));
    }

    @Test
    @Order(2)
    void interruptionExceptionTest() {
        final Calendar afterOneMinute = Calendar.getInstance();
        afterOneMinute.add(Calendar.MINUTE, 1);

        final Auction ongoingAuction = preBuiltAuction.get().duration(AuctionTestUtils.TWO_DAYS_DURATION).build();
        ongoingAuction.start();
        ongoingAuction.cancel();

        final Exception interruptionException = assertThrows(IllegalArgumentException.class,
                () -> ongoingAuction.resumeAfterInterruption(afterOneMinute));
        assertTrue(interruptionException.getMessage().contains("The interruption can't be in the future"));
    }

    @Test
    @Order(3)
    void cancelTimerTest() {
        final Auction auction = preBuiltAuction.get()
                .duration(AuctionTestUtils.TWO_DAYS_DURATION).build();
        auction.start();
        assertTrue(auction.isActive());
        auction.cancel();
        assertTrue(auction.isActive(), "Auction must still be active");
    }

    @Test
    @Order(4)
    void resumesAfterInterruptionTest() throws InterruptedException {
        final Calendar oneHundredMillisBefore = Calendar.getInstance();
        final Calendar fiftyMillisBefore = Calendar.getInstance();
        oneHundredMillisBefore.add(Calendar.MILLISECOND, -NumericalConstants.ONE_HUNDRED);
        fiftyMillisBefore.add(Calendar.MILLISECOND, -NumericalConstants.FIFTY);

        final Auction auction = preBuiltAuction.get()
                .creationTime(oneHundredMillisBefore) // 100 millis ago
                .duration(Duration.ofMillis(NumericalConstants.TWO_HUNDRED)).build(); // duration of 200 millis

        assertTrue(auction.isActive());
        assertTrue(auction.getClosingTime().isEmpty());
        // timer re-start after server interruption
        auction.resumeAfterInterruption(fiftyMillisBefore); // interruption occurred 50 millis ago
        assertDurationAboutEquals(Duration.ofMillis(NumericalConstants.ONE_HUNDRED_AND_FIFTY),
                auction.remainingTime(), Duration.ofMillis(NumericalConstants.TEN));
        // wait for the end of the auction
        s.sleepEquallyMoreThan(NumericalConstants.ONE_HUNDRED_AND_FIFTY);
        assertFalse(auction.isActive());
    }

    @Test
    @Order(NumericalConstants.FIVE)
    void resumeBeforeAuctionEndsTest() throws InterruptedException {
        final Calendar interruptionBeginTime;
        final Auction auction = preBuiltAuction.get() // created in this instant
                .duration(Duration.ofMillis(NumericalConstants.TWO_HUNDRED)) // duration of 200 millis
                .build();
        // timer start
        auction.start();
        // wait 50 millis
        s = new Sleeper();
        s.sleep(NumericalConstants.FIFTY);
        interruptionBeginTime = Calendar.getInstance();
        // now it should remain 150 ms
        assertDurationAboutEquals(Duration.ofMillis(NumericalConstants.ONE_HUNDRED_AND_FIFTY),
                auction.remainingTime(), Duration.ofMillis(NumericalConstants.TEN));
        // simulate a server interruption of 50 millis
        auction.cancel();
        s.sleep(NumericalConstants.FIFTY);
        // timer re-start after server interruption
        auction.resumeAfterInterruption(interruptionBeginTime);
        assertTrue(auction.isActive());
        // now it should STILL remain 150 ms
        assertDurationAboutEquals(Duration.ofMillis(NumericalConstants.ONE_HUNDRED_AND_FIFTY),
                auction.remainingTime(), Duration.ofMillis(NumericalConstants.TEN));
        // wait for the end of the auction
        s.sleep(NumericalConstants.ONE_HUNDRED_AND_SEVENTY);
        assertFalse(auction.isActive());
    }

    @Test
    @Order(NumericalConstants.SIX)
    void resumeAfterAuctionEndsTest() throws InterruptedException {
        final Calendar interruptionBeginTime;
        final Auction auction = preBuiltAuction.get() // created in this instant
                .duration(Duration.ofMillis(NumericalConstants.ONE_HUNDRED)) // duration of 100 millis
                .build();
        // timer start
        auction.start();
        // wait 50 millis
        s.sleep(NumericalConstants.FIFTY);
        interruptionBeginTime = Calendar.getInstance();
        // now it should remain half of the original duration (i.e. 50 ms)
        assertDurationAboutEquals(Duration.ofMillis(NumericalConstants.FIFTY),
                auction.remainingTime(), Duration.ofMillis(NumericalConstants.TEN));
        // simulate a server interruption of 300 millis
        auction.cancel();
        s.sleep(NumericalConstants.THREE_HUNDRED);
        // timer re-start after server interruption
        auction.resumeAfterInterruption(interruptionBeginTime);
        assertTrue(auction.isActive());
        // now it should STILL remain half of the original duration (i.e. 50 ms)
        assertDurationAboutEquals(Duration.ofMillis(NumericalConstants.FIFTY),
                auction.remainingTime(), Duration.ofMillis(NumericalConstants.TEN));
        // wait for the end of the auction
        s.sleepEquallyMoreThan(NumericalConstants.FIFTY);
        assertFalse(auction.isActive());
    }

    private void assertDurationAboutEquals(final Duration expected, final Duration actual, final Duration delta) {
        assertTrue(expected.minus(actual).abs().compareTo(delta) <= 0, "Expected " + expected
            + " but was " + actual);
    }
}
