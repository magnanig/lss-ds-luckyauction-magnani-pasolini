/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.domain.event;

import it.unibo.luckyauction.auction.domain.entity.Auction;
import it.unibo.luckyauction.auction.domain.valueobject.Bid;
import it.unibo.luckyauction.auction.domain.valueobject.Product;
import it.unibo.luckyauction.auction.domain.valueobject.ProductCategory;
import it.unibo.luckyauction.auction.domain.valueobject.ProductState;
import it.unibo.luckyauction.util.Sleeper;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.Duration;
import java.util.concurrent.atomic.AtomicBoolean;

import static it.unibo.luckyauction.auction.domain.config.AuctionConstants.getAuctionEndingZone;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SuppressWarnings("PMD.JUnitAssertionsShouldIncludeMessage")
class AuctionEventsTests {

    private final Duration fastAuctionDuration = Duration.ofMillis(100);

    private final BigDecimal iPhonePrice = new BigDecimal(300);
    private final Auction.AuctionBuilder prebuiltAuction = Auction.builder()
            .auctionId("1")
            .product(Product.builder()
                    .name("iPhone X")
                    .category(ProductCategory.INFORMATICS)
                    .state(ProductState.USED_OPTIMUM_CONDITION)
                    .image("base64img")
                    .build())
            .duration(fastAuctionDuration)
            .sellerUsername("seller")
            .startingPrice(iPhonePrice)
            .bidsHistoryId("1");

    @Test
    /* package */ void auctionClosedEventRaiseTest() throws InterruptedException {
        final Sleeper s = new Sleeper();
        final AtomicBoolean closed = new AtomicBoolean(); // false
        AuctionDomainEvent.registerAuctionClosedHandler(auctionClosed -> closed.set(true));
        prebuiltAuction.build().start();
        assertFalse(closed.get());
        s.sleepEquallyMoreThan(fastAuctionDuration.toMillis());
        assertTrue(closed.get());
    }

    @Test
    /* package */ void auctionExtendedEventRaiseTest() throws InterruptedException {
        final Sleeper s = new Sleeper();
        final AtomicBoolean extended = new AtomicBoolean(); // false
        AuctionDomainEvent.registerAuctionExtendedHandler(auctionExtended -> extended.set(true));
        prebuiltAuction.extensionDuration(fastAuctionDuration).build().start();
        assertFalse(extended.get());
        s.sleepEquallyMoreThan(fastAuctionDuration.toMillis());
        assertTrue(extended.get());
    }

    @Test
    /* package */ void auctionExpectedClosingTimeUpdatedEventRaiseTest() throws InterruptedException {
        final Sleeper s = new Sleeper();
        final AtomicBoolean expectedClosingTimeUpdated = new AtomicBoolean(); // false
        AuctionDomainEvent.registerAuctionExpectingClosingTimeUpdatedHandler(auctionExtended ->
                expectedClosingTimeUpdated.set(true));
        final Auction auction = prebuiltAuction.build();
        auction.start();
        assertFalse(expectedClosingTimeUpdated.get());
        final Duration endingZoneDuration = getAuctionEndingZone(fastAuctionDuration);
        final Long timeToEnterToEndingZone = fastAuctionDuration.minus(endingZoneDuration).toMillis();
        s.sleepEquallyMoreThan(timeToEnterToEndingZone);
        assertFalse(expectedClosingTimeUpdated.get()); // still false...
        auction.addNewBid(new Bid(new BigDecimal("2"), "test"));
        assertTrue(expectedClosingTimeUpdated.get());
    }
}
