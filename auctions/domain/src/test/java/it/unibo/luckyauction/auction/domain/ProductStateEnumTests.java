/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.domain;

import it.unibo.luckyauction.auction.domain.valueobject.ProductCategory;
import it.unibo.luckyauction.auction.domain.valueobject.ProductState;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ProductStateEnumTests {

    @Test
    void productStateTest() {
        assertThrows(IllegalArgumentException.class, () -> ProductCategory.valueOf("New"));
        assertThrows(IllegalArgumentException.class, () -> ProductCategory.valueOf("new"));
        assertNotNull(ProductState.valueOf("NEW"));
        assertNotNull(ProductState.valueOf("USED_OPTIMUM_CONDITION"));
        assertNotNull(ProductState.valueOf("USED_GOOD_CONDITION"));
        assertNotNull(ProductState.valueOf("USED_BAD_CONDITION"));
        assertNotNull(ProductState.valueOf("USED_POOR_CONDITION"));
    }

    @Test
    void productStateListTest() {
        assertEquals(
                List.of(ProductState.NEW, ProductState.USED_OPTIMUM_CONDITION, ProductState.USED_GOOD_CONDITION,
                        ProductState.USED_BAD_CONDITION, ProductState.USED_POOR_CONDITION),
                Arrays.stream(ProductState.values()).toList()
        );
    }
}
