/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.domain;

import it.unibo.luckyauction.auction.domain.entity.Auction;
import it.unibo.luckyauction.auction.domain.service.AuctionService;
import it.unibo.luckyauction.auction.domain.valueobject.Bid;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class AuctionServiceTests {
    private static final Duration ONE_HUNDRED_MILLIS = Duration.ofMillis(100);
    private Auction ongoingAuction;

    @BeforeEach
    void createOngoingAuction() {
        ongoingAuction = Auction.builder().auctionId(AuctionTestUtils.AUCTION_TEST_ID)
                .product(AuctionTestUtils.SHOES_PRODUCT.get()).sellerUsername(AuctionTestUtils.SELLER_USERNAME)
                .startingPrice(AuctionTestUtils.AUCTION_PRICE).duration(ONE_HUNDRED_MILLIS)
                .bidsHistoryId(AuctionTestUtils.BIDS_HISTORY_ID).build();
        ongoingAuction.start();
    }

    @Test
    void userCanRaiseTest() throws InterruptedException {
        assertTrue(AuctionService.userCanRaise(AuctionTestUtils.FIRST_USERNAME, ongoingAuction));

        final Bid firstBid = new Bid(AuctionTestUtils.BID_RAISE_AMOUNT, AuctionTestUtils.FIRST_USERNAME);
        ongoingAuction.addNewBid(firstBid);

        assertFalse(AuctionService.userCanRaise(AuctionTestUtils.FIRST_USERNAME, ongoingAuction));
        assertTrue(AuctionService.userCanRaise(AuctionTestUtils.SECOND_USERNAME, ongoingAuction));
        Thread.sleep(100); // wait for the end of the auction
        // the auction has ended
        assertFalse(AuctionService.userCanRaise(AuctionTestUtils.BUYER_USERNAME, ongoingAuction));
    }

    @Test
    void isValidBidAmountTest() throws InterruptedException {
        final Bid invalidBid = new Bid(AuctionTestUtils.AUCTION_PRICE, AuctionTestUtils.BUYER_USERNAME);
        final Bid firstValidBid = new Bid(AuctionTestUtils.BID_RAISE_AMOUNT, AuctionTestUtils.FIRST_USERNAME);
        final Bid secondValidBid = new Bid(AuctionTestUtils.NEW_BID_RAISE_AMOUNT, AuctionTestUtils.SECOND_USERNAME);

        assertFalse(AuctionService.isValidBidAmount(ongoingAuction, invalidBid));
        assertTrue(AuctionService.isValidBidAmount(ongoingAuction, firstValidBid));
        ongoingAuction.addNewBid(firstValidBid);
        assertFalse(AuctionService.isValidBidAmount(ongoingAuction, invalidBid));
        assertTrue(AuctionService.isValidBidAmount(ongoingAuction, secondValidBid));
        Thread.sleep(100); // wait for the end of the auction
    }
}
