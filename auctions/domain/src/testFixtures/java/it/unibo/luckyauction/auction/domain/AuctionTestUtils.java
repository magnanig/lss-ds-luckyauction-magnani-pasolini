/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.domain;

import it.unibo.luckyauction.auction.domain.valueobject.Product;
import it.unibo.luckyauction.auction.domain.valueobject.ProductCategory;
import it.unibo.luckyauction.auction.domain.valueobject.ProductState;
import it.unibo.luckyauction.auction.domain.valueobject.UserContactInformation;

import java.math.BigDecimal;
import java.time.Duration;
import java.util.function.Supplier;

public final class AuctionTestUtils {

    /**
     * One day duration of a generic auction.
     */
    public static final Duration ONE_DAY_DURATION = Duration.ofDays(1);
    /**
     * Duration of two days.
     */
    public static final Duration TWO_DAYS_DURATION = Duration.ofDays(2);
    /**
     * Name of a generic pair of shoes.
     */
    public static final String SHOES_PRODUCT_NAME = "Golden Goose shoes white and blue limited edition 2015";
    /**
     * Description of a generic pair of shoes.
     */
    public static final String SHOES_DESCRIPTION = "The shoes have also been used very often to go to work, "
            + "not just to go out on festive occasions";
    /**
     * Product of a generic auction.
     */
    public static final Supplier<Product> SHOES_PRODUCT =  () -> Product.builder().name(SHOES_PRODUCT_NAME)
            .category(ProductCategory.SHOES_AND_BAGS).state(ProductState.USED_OPTIMUM_CONDITION)
            .description(SHOES_DESCRIPTION).image("base64img").build();
    /**
     * Name of the iPhone.
     */
    public static final String IPHONE_PRODUCT_NAME = "iPhone XR 64 GB black";
    /**
     * Product of a generic auction.
     */
    public static final Supplier<Product> IPHONE_PRODUCT = () -> Product.builder().name(IPHONE_PRODUCT_NAME)
            .category(ProductCategory.INFORMATICS).state(ProductState.USED_OPTIMUM_CONDITION)
            .image("base64img").build();
    /**
     * ID of a generic auctions history.
     */
    public static final String FIRST_AUCTIONS_HISTORY_ID = "FIRST_AH_ID";
    /**
     * ID of a generic auctions history.
     */
    public static final String SECOND_AUCTIONS_HISTORY_ID = "SECOND_AH_ID";
    /**
     * ID of a generic auctions history.
     */
    public static final String THIRD_AUCTIONS_HISTORY_ID = "THIRD_AH_ID";
    /**
     * ID of a generic bids history.
     */
    public static final String BIDS_HISTORY_ID = "123";
    /**
     * ID of a generic auction.
     */
    public static final String AUCTION_TEST_ID = "4567";
    /**
     * Starting price of a generic auction.
     */
    public static final BigDecimal AUCTION_PRICE = BigDecimal.valueOf(50.00);
    /**
     * Bid raise amount of a generic auction.
     */
    public static final BigDecimal BID_RAISE_AMOUNT = BigDecimal.valueOf(52.00);
    /**
     * New bid raise amount of a generic auction.
     */
    public static final BigDecimal NEW_BID_RAISE_AMOUNT = BigDecimal.valueOf(55.00);


    /**
     * User's email.
     */
    public static final String EMAIL = "ts@email.com";
    /**
     * User's cellular number.
     */
    public static final String CELLULAR_NUMBER = "0123456789";
    /**
     * User's telephone number.
     */
    public static final String TELEPHONE_NUMBER = "0123456788";
    /**
     * User's contact information.
     */
    public static final Supplier<UserContactInformation> CONTACTS =
            () -> new UserContactInformation(EMAIL, CELLULAR_NUMBER, TELEPHONE_NUMBER);


    /**
     * Username of a generic seller user.
     */
    public static final String SELLER_USERNAME = "sellerUsername";
    /**
     * Username of a winner user.
     */
    public static final String WINNER_USERNAME = "winnerUsername";
    /**
     * Username of a generic buyer user.
     */
    public static final String BUYER_USERNAME = "buyerUsername";
    /**
     * Username of the test user.
     */
    public static final String FIRST_USERNAME = "firstUsername";
    /**
     * Username of another test user.
     */
    public static final String SECOND_USERNAME = "secondUsername";
    /**
     * Username of another test user.
     */
    public static final String THIRD_USERNAME = "thirdUsername";


    private AuctionTestUtils() { }
}
