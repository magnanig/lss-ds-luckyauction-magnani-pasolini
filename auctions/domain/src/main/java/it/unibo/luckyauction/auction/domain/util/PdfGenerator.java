/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.domain.util;

import it.unibo.luckyauction.auction.domain.valueobject.SummaryEndedAuction;
import it.unibo.luckyauction.auction.domain.valueobject.UserContactInformation;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Auction pdf generator.
 */
public final class PdfGenerator {

    private static final PDFont PDF_BOLD_FONT = PDType1Font.COURIER_BOLD;
    private static final PDFont PDF_NORMAL_FONT = PDType1Font.COURIER;
    private static final float FONT_SIZE = 13;
    private static final float LEADING = 1.5f * FONT_SIZE;

    private PdfGenerator() { }

    /**
     * Generates a pdf file containing a summary of the main information of an adjudicated auction.
     * @param endedAuction      the ended auction containing the winning bid
     * @param sellerContacts    the seller contact information to add in the final pdf report
     * @param buyerContacts     the buyer contact information to add in the final pdf report
     * @return                  the byte array of the pdf document
     */
    @SuppressWarnings("PMD.CloseResource")
    public static byte[] auctionPdfGenerator(final SummaryEndedAuction endedAuction,
                                             final UserContactInformation sellerContacts,
                                             final UserContactInformation buyerContacts) {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        final PDDocument doc = getPDDocument(endedAuction, sellerContacts, buyerContacts);
        try {
            doc.save(byteArrayOutputStream);
            doc.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return byteArrayOutputStream.toByteArray();
    }

    /**
     * Generates a pdf file containing a summary of the main information of an adjudicated auction.
     * @param endedAuction      the ended auction containing the winning bid
     * @param sellerContacts    the seller contact information to add in the final pdf report
     * @param buyerContacts     the buyer contact information to add in the final pdf report
     * @param outputFilePath    the path to save the pdf file; this must also include the
     *                          file name complete with .pdf extension.
     *                          Example: "./src/summary.pdf"
     */
    @SuppressWarnings("PMD.CloseResource")
    public static void auctionPdfGenerator(final SummaryEndedAuction endedAuction,
                                           final UserContactInformation sellerContacts,
                                           final UserContactInformation buyerContacts, final String outputFilePath) {
        final PDDocument doc = getPDDocument(endedAuction, sellerContacts, buyerContacts);
        try {
            doc.save(outputFilePath);
            doc.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("PMD.CloseResource")
    private static PDDocument getPDDocument(final SummaryEndedAuction endedAuction,
                                            final UserContactInformation sellerContacts,
                                            final UserContactInformation buyerContacts) {
        final PDDocument doc = new PDDocument();
        try {
            final PDPage page = new PDPage();
            doc.addPage(page);

            try (PDPageContentStream contentStream = new PDPageContentStream(doc, page)) {

                final PDRectangle mediaBox = page.getMediaBox();
                final float margin = 72;
                final float width = mediaBox.getWidth() - 2 * margin;
                final float startX = mediaBox.getLowerLeftX() + margin;
                final float startY = mediaBox.getUpperRightY() - margin;

                contentStream.beginText();
                contentStream.setFont(PDF_NORMAL_FONT, FONT_SIZE);
                contentStream.newLineAtOffset(startX, startY);

                // introduction
                addMoreLines(contentStream, width, "#" + endedAuction.getAuctionId() + " auction: "
                        + endedAuction.getProduct().name(), PDF_BOLD_FONT);

                // data summary
                addOneLine(contentStream, "Creation time: "
                        + getAuctionDateTime(endedAuction.getCreationTime().getTime()));
                addOneLine(contentStream, "Closing time: "
                        + getAuctionDateTime(endedAuction.getClosingTime().getTime()));
                addOneLine(contentStream, "Starting price: " + endedAuction.getStartingPrice() + " €",
                        PDF_BOLD_FONT);
                addOneLine(contentStream, "Final price: " + endedAuction.getWinningBid().getAmount() + " €",
                        PDF_BOLD_FONT);

                addOneLine(contentStream, "Seller user (contacts)", PDF_BOLD_FONT);
                addUserContacts(contentStream, endedAuction.getSellerUsername(), sellerContacts);

                addOneLine(contentStream, "Buyer user (contacts)", PDF_BOLD_FONT);
                addUserContacts(contentStream, endedAuction.getWinningBid().getBidderUsername(), buyerContacts);

                // end note line
                addEndNoteLine(contentStream);

                // end file
                contentStream.endText();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return doc;
    }

    private static String getAuctionDateTime(final Date timestamp) {
        final SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy HH:mm:ss", Locale.ITALIAN);
        sdf.setTimeZone(TimeZone.getTimeZone("Europe/Rome"));
        return sdf.format(timestamp);
    }

    private static void addEndNoteLine(final PDPageContentStream contentStream) {
        try {
            contentStream.setFont(PDF_NORMAL_FONT, 8);
            contentStream.newLineAtOffset(0, -LEADING);
            contentStream.newLineAtOffset(0, -LEADING);
            contentStream.newLineAtOffset(0, -LEADING);
            contentStream.newLineAtOffset(0, -LEADING);
            contentStream.showText("NOTE: This document represents a summary of the auction. "
                    + "It has no legal validity.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void addUserContacts(final PDPageContentStream contentStream,
                                        final String username, final UserContactInformation userContacts) {
        try {
            contentStream.setFont(PDF_NORMAL_FONT, FONT_SIZE);
            contentStream.newLineAtOffset(0, -LEADING);
            contentStream.showText("username: " + username);
            contentStream.newLineAtOffset(0, -LEADING);
            contentStream.showText("email: " + userContacts.email());
            contentStream.newLineAtOffset(0, -LEADING);
            contentStream.showText("cell: " + userContacts.cellularNumber());
            if (userContacts.telephoneNumber() != null) {
                contentStream.newLineAtOffset(0, -LEADING);
                contentStream.showText("tel: " + userContacts.telephoneNumber());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Use this method when you want to add short text in the pdf that is definitely
     * within a line of the pdf file. In case the text is long and cannot fit in a
     * single line, use the
     * {@link #addMoreLines(PDPageContentStream, float, String, PDFont...)} method.
     */
    private static void addOneLine(final PDPageContentStream contentStream, final String line, final PDFont... font) {
        final PDFont fontToUse = font.length > 0 ? font[0] : PDF_NORMAL_FONT;
        try {
            contentStream.setFont(fontToUse, FONT_SIZE);
            contentStream.newLineAtOffset(0, -LEADING);
            contentStream.newLineAtOffset(0, -LEADING);
            contentStream.showText(line);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Use this method when you want to add long text in the pdf that cannot fit in
     * a single line of the pdf file. In case the text is short and can fit in
     * only one line, use the
     * {@link #addOneLine(PDPageContentStream, String, PDFont...)} method.
     */
    private static void addMoreLines(final PDPageContentStream contentStream, final float width,
                                     final String textToAdd, final PDFont... font) {
        final PDFont fontToUse = font.length > 0 ? font[0] : PDF_NORMAL_FONT;
        final List<String> lines = new ArrayList<>();
        try {
            contentStream.setFont(fontToUse, FONT_SIZE);
            int lastSpace = -1;
            String text = textToAdd;
            while (text.length() > 0) {
                int spaceIndex = text.indexOf(' ', lastSpace + 1);
                if (spaceIndex < 0) {
                    spaceIndex = text.length();
                }
                String subString = text.substring(0, spaceIndex);
                final float size = FONT_SIZE * PDF_NORMAL_FONT.getStringWidth(subString) / 1000;
                // System.out.printf("'%s' - %f of %f\n", subString, size, width);
                if (size > width) {
                    if (lastSpace < 0) {
                        lastSpace = spaceIndex;
                    }
                    subString = text.substring(0, lastSpace);
                    lines.add(subString);
                    text = text.substring(lastSpace).trim();
                    // System.out.printf("'%s' is line\n", subString);
                    lastSpace = -1;
                } else if (spaceIndex == text.length()) {
                    lines.add(text);
                    // System.out.printf("'%s' is line\n", text);
                    text = "";
                } else {
                    lastSpace = spaceIndex;
                }
            }

            for (final String line : lines) {
                contentStream.showText(line);
                contentStream.newLineAtOffset(0, -LEADING);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
