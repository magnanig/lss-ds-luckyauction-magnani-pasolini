/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.domain.valueobject;

import it.unibo.luckyauction.auction.domain.exception.BidValidationException;
import it.unibo.luckyauction.util.CalendarUtils;
import lombok.Generated;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Objects;

/**
 * A bid for an auction.
 */
public class Bid implements Comparable<Bid> {

    private final BigDecimal amount;
    private final String bidderUsername;
    private final Calendar timestamp;

    /**
     * Create a new bid with the specified value.
     * @param amount            the value of the bid (i.e. amount of money)
     * @param bidderUsername    the username of the user raising the bid
     * @param timestamp         the timestamp of the bid creation
     */
    public Bid(final BigDecimal amount, final String bidderUsername, final Calendar timestamp) {
        this.amount = amount;
        this.bidderUsername = bidderUsername;
        this.timestamp = timestamp == null ? Calendar.getInstance() : timestamp;
        validate();
    }

    /**
     * Create a new bid with the specified value, setting the timestamp to
     * the current time.
     * @param amount            the value of the bid (i.e. amount of money)
     * @param bidderUsername    the username of the user raising the bid
     */
    public Bid(final BigDecimal amount, final String bidderUsername) {
        this(amount, bidderUsername, Calendar.getInstance());
    }

    /**
     * Get the value of the bid.
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * Get the username of the user raising the bid.
     */
    public String getBidderUsername() {
        return bidderUsername;
    }

    /**
     * Get the timestamp of the bid raise.
     */
    public Calendar getTimestamp() {
        return CalendarUtils.copy(timestamp);
    }

    /**
     * {@inheritDoc}
     */
    @Override @Generated
    public int compareTo(final Bid bid) {
        return amount.compareTo(bid.getAmount());
    }

    /**
     * {@inheritDoc}
     */
    @Override @Generated
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final Bid bid = (Bid) o;
        return amount.compareTo(bid.amount) == 0 && bidderUsername.equals(bid.bidderUsername)
                && timestamp.equals(bid.timestamp);
    }

    /**
     * {@inheritDoc}
     */
    @Override @Generated
    public int hashCode() {
        return Objects.hash(amount, bidderUsername, timestamp);
    }

    /**
     * {@inheritDoc}
     */
    @Override @Generated
    public String toString() {
        return "Bid{\n"
                + "\tamount='" + amount.toString() + "'\n"
                + "\tusername='" + bidderUsername + "'\n"
                + "\ttimestamp=" + timestamp.getTime() + "\n"
                + "}";
    }

    private void validate() {
        if (bidderUsername == null || bidderUsername.isEmpty()) {
            throw new BidValidationException("Bidder username is not set");
        }
        if (amount == null) {
            throw new BidValidationException("Bid amount is not set");
        }
        if (amount.compareTo(BigDecimal.ZERO) <= 0 || amount.scale() > 2) {
            throw new BidValidationException("Bid amount must be greater than zero and can have at most "
                    + "two decimal digits");
        }
    }
}
