/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.domain.event;

import java.util.ArrayList;
import java.util.List;

public final class AuctionDomainEvent {

    private static final List<AuctionEventHandler<AuctionClosed>> AUCTION_CLOSED_HANDLERS = new ArrayList<>();
    private static final List<AuctionEventHandler<AuctionExtended>> AUCTION_EXTENDED_HANDLERS = new ArrayList<>();
    private static final List<AuctionEventHandler<AuctionPostponedOnBidRaise>>
            AUCTION_EXPECTED_CLOSING_TIME_UPDATED_HANDLERS = new ArrayList<>();

    public static void registerAuctionClosedHandler(final AuctionEventHandler<AuctionClosed> auctionEventHandler) {
        addIfNotPresent(auctionEventHandler, AUCTION_CLOSED_HANDLERS);
    }

    public static void registerAuctionExtendedHandler(final AuctionEventHandler<AuctionExtended> auctionEventHandler) {
        addIfNotPresent(auctionEventHandler, AUCTION_EXTENDED_HANDLERS);
    }

    public static void registerAuctionExpectingClosingTimeUpdatedHandler(
            final AuctionEventHandler<AuctionPostponedOnBidRaise> auctionEventHandler) {
        addIfNotPresent(auctionEventHandler, AUCTION_EXPECTED_CLOSING_TIME_UPDATED_HANDLERS);
    }

    public static void raise(final AuctionEvent auctionEvent) {
        if (auctionEvent instanceof AuctionClosed auctionClosed) {
            notifyAll(auctionClosed, AUCTION_CLOSED_HANDLERS);
        } else if (auctionEvent instanceof AuctionExtended auctionExtended) {
            notifyAll(auctionExtended, AUCTION_EXTENDED_HANDLERS);
        } else if (auctionEvent instanceof AuctionPostponedOnBidRaise auctionPostponedOnBidRaise) {
            notifyAll(auctionPostponedOnBidRaise, AUCTION_EXPECTED_CLOSING_TIME_UPDATED_HANDLERS);
        }
    }

    private static <T extends AuctionEvent> void notifyAll(final T auctionEvent,
                                                           final List<AuctionEventHandler<T>> handlers) {
        handlers.forEach(handler -> handler.handle(auctionEvent));
    }

    private static <T> void addIfNotPresent(final T element, final List<T> list) {
        if (!list.contains(element)) {
            list.add(element);
        }
    }

    private AuctionDomainEvent() { }
}
