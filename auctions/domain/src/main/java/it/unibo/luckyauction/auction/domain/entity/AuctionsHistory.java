/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.domain.entity;

import it.unibo.luckyauction.auction.domain.exception.AuctionsHistoryConsistencyErrorException;
import it.unibo.luckyauction.auction.domain.exception.AuctionsHistoryValidationException;
import it.unibo.luckyauction.auction.domain.valueobject.AwardedAuction;
import it.unibo.luckyauction.auction.domain.valueobject.CreatedEndedAuction;
import it.unibo.luckyauction.auction.domain.valueobject.CreatedOngoingAuction;
import it.unibo.luckyauction.auction.domain.valueobject.LostAuction;
import it.unibo.luckyauction.auction.domain.valueobject.ParticipatingAuction;
import lombok.Generated;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * The history of auctions of a user.
 */
public class AuctionsHistory {

    private static final String AUCTION_MESSAGE = "\nThe auction is:\n";

    private final String auctionsHistoryId;
    private final String username;

    private final List<AwardedAuction> awardedAuctions;
    private final List<LostAuction> lostAuctions;
    private final List<ParticipatingAuction> participatingAuctions;
    private final List<CreatedOngoingAuction> createdOngoingAuctions;
    private final List<CreatedEndedAuction> createdEndedAuctions;

    /**
     * Create a new AuctionsHistory object for the specified user.
     * @param auctionsHistoryId         the id of the auctions history
     * @param username                  the username of the user whose history refers to
     * @param awardedAuctions           a collection of awarded auctions
     * @param lostAuctions              a collection of lost auctions
     * @param participatingAuctions     a collection of participating auctions
     * @param createdOngoingAuctions    a collection of created ongoing auctions
     * @param createdEndedAuctions      a collection of created ended auctions
     */
    public AuctionsHistory(final String auctionsHistoryId, final String username,
                           final Collection<AwardedAuction> awardedAuctions,
                           final Collection<LostAuction> lostAuctions,
                           final Collection<ParticipatingAuction> participatingAuctions,
                           final Collection<CreatedOngoingAuction> createdOngoingAuctions,
                           final Collection<CreatedEndedAuction> createdEndedAuctions) {
        if (awardedAuctions == null || lostAuctions == null || participatingAuctions == null
                || createdOngoingAuctions == null || createdEndedAuctions == null) {
            throw new AuctionsHistoryValidationException("At least one list in the auctions history is null");
        }
        this.auctionsHistoryId = auctionsHistoryId;
        this.username = username;
        this.awardedAuctions = new ArrayList<>(awardedAuctions);
        this.lostAuctions = new ArrayList<>(lostAuctions);
        this.participatingAuctions = new ArrayList<>(participatingAuctions);
        this.createdOngoingAuctions = new ArrayList<>(createdOngoingAuctions);
        this.createdEndedAuctions = new ArrayList<>(createdEndedAuctions);
        validateAuctionsHistory();
    }

    /**
     * Create a nre auctions history for the specified user.
     * @param auctionsHistoryId         the id of the auctions history
     * @param username                  the username of the user whose history refers to
     */
    public AuctionsHistory(final String auctionsHistoryId, final String username) {
        this(auctionsHistoryId, username, Collections.emptyList(), Collections.emptyList(), Collections.emptyList(),
                Collections.emptyList(), Collections.emptyList());
    }

    /**
     * Add an awarded auction to the current history.
     * @param awardedAuction                            the awarded auction to add
     * @throws AuctionsHistoryConsistencyErrorException if the auction history user is not the auction winner or
     *                                                  if the auction history user is the same as the auction seller
     */
    public void addAwardedAuction(final AwardedAuction awardedAuction) {
        if (!username.equals(awardedAuction.getWinningBid().getBidderUsername())
                || username.equals(awardedAuction.getSellerUsername())) {
            throw new AuctionsHistoryConsistencyErrorException("Consistency error when adding awarded auction in the "
                    + "auctions history of user " + username + AUCTION_MESSAGE + awardedAuction);
        }
        awardedAuctions.add(awardedAuction);
    }

    /**
     * Add a participating auction to the current history.
     * @param participatingAuction                      the participating auction to add
     * @throws AuctionsHistoryConsistencyErrorException if the auction history user is the same as the auction seller
     */
    public void addParticipatingAuction(final ParticipatingAuction participatingAuction) {
        if (username.equals(participatingAuction.getSellerUsername())) {
            throw new AuctionsHistoryConsistencyErrorException("Consistency error when adding participating auction "
                    + "in the auctions history of user " + username + AUCTION_MESSAGE + participatingAuction);
        }
        participatingAuctions.add(participatingAuction);
    }

    /**
     * Add a lost auction to the current history.
     * @param lostAuction                               the lost auction to add
     * @throws AuctionsHistoryConsistencyErrorException if the auction history user is the same as the auction winner or
     *                                                  if the auction history user is the same as the auction seller
     */
    public void addLostAuction(final LostAuction lostAuction) {
        if (username.equals(lostAuction.getWinningBid().getBidderUsername())
                || username.equals(lostAuction.getSellerUsername())) {
            throw new AuctionsHistoryConsistencyErrorException("Consistency error when adding lost auction in the "
                    + "auctions history of user " + username + AUCTION_MESSAGE + lostAuction);
        }
        lostAuctions.add(lostAuction);
    }

    /**
     * Add a created ongoing auction to the current history.
     * @param createdOngoingAuction                     the created ongoing auction to add
     * @throws AuctionsHistoryConsistencyErrorException if the auction history user is not the auction seller
     */
    public void addCreatedOngoingAuction(final CreatedOngoingAuction createdOngoingAuction) {
        if (!username.equals(createdOngoingAuction.getSellerUsername())) {
            throw new AuctionsHistoryConsistencyErrorException("Consistency error when adding created ongoing auction "
                    + "in the auctions history of user " + username + AUCTION_MESSAGE + createdOngoingAuction);
        }
        createdOngoingAuctions.add(createdOngoingAuction);
    }

    /**
     * Add a created ended auction to the current history.
     * @param createdEndedAuction                       the created ended auction to add
     * @throws AuctionsHistoryConsistencyErrorException if the auction history user is the same as the auction winner
     *                                                  (if the auction winner is present) or if the auction history
     *                                                  user is not the auction seller
     */
    public void addCreatedEndedAuction(final CreatedEndedAuction createdEndedAuction) {
        if (createdEndedAuction.getWinningBid() != null && username.equals(createdEndedAuction.getWinningBid()
                .getBidderUsername()) || !username.equals(createdEndedAuction.getSellerUsername())) {
            throw new AuctionsHistoryConsistencyErrorException("Consistency error when adding created ended auction "
                    + "in the auctions history of user " + username + AUCTION_MESSAGE + createdEndedAuction);
        }
        createdEndedAuctions.add(createdEndedAuction);
    }

    /**
     * Remove a created ongoing auction from the user's auctions history.
     * @param createdOngoingAuction the created ongoing auction to remove
     */
    public void removeCreatedOngoingAuction(final CreatedOngoingAuction createdOngoingAuction) {
        createdOngoingAuctions.remove(createdOngoingAuction);
    }

    /**
     * Remove a participating auction from the user's auctions history.
     * @param participatingAuction the participating auction to remove
     */
    public void removeParticipatingAuction(final ParticipatingAuction participatingAuction) {
        participatingAuctions.remove(participatingAuction);
    }

    /**
     * Get the id of the auctions' history.
     */
    public String getAuctionsHistoryId() {
        return auctionsHistoryId;
    }

    /**
     * Get the list of the awarded auctions.
     */
    public List<AwardedAuction> getAwardedAuctions() {
        return Collections.unmodifiableList(awardedAuctions);
    }

    /**
     * Get the list of the lost auctions.
     */
    public List<LostAuction> getLostAuctions() {
        return Collections.unmodifiableList(lostAuctions);
    }

    /**
     * Get the list of the participating auctions.
     */
    public List<ParticipatingAuction> getParticipatingAuctions() {
        return Collections.unmodifiableList(participatingAuctions);
    }

    /**
     * Get the list of the created ongoing auctions.
     */
    public List<CreatedOngoingAuction> getCreatedOngoingAuctions() {
        return Collections.unmodifiableList(createdOngoingAuctions);
    }

    /**
     * Get the list of the created ended auctions.
     */
    public List<CreatedEndedAuction> getCreatedEndedAuctions() {
        return Collections.unmodifiableList(createdEndedAuctions);
    }

    /**
     * Get the username of the user owning this auctions history.
     */
    public String getUsername() {
        return username;
    }

    /**
     * {@inheritDoc}
     */
    @Override @Generated
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final AuctionsHistory that = (AuctionsHistory) o;
        return auctionsHistoryId.equals(that.auctionsHistoryId) && username.equals(that.username)
                && awardedAuctions.equals(that.awardedAuctions) && lostAuctions.equals(that.lostAuctions)
                && participatingAuctions.equals(that.participatingAuctions)
                && createdOngoingAuctions.equals(that.createdOngoingAuctions)
                && createdEndedAuctions.equals(that.createdEndedAuctions);
    }

    /**
     * {@inheritDoc}
     */
    @Override @Generated
    public int hashCode() {
        return Objects.hash(auctionsHistoryId, username, awardedAuctions, lostAuctions, participatingAuctions,
                createdOngoingAuctions, createdEndedAuctions);
    }

    /**
     * {@inheritDoc}
     */
    @Override @Generated
    public String toString() {
        return "AuctionsHistory{\n"
                + "\tauctionsHistoryId='" + auctionsHistoryId + "'\n"
                + "\tusername='" + username + "'\n"
                + "\tawardedAuctions=" + awardedAuctions.toString() + "\n"
                + "\tlostAuctions=" + lostAuctions.toString() + "\n"
                + "\tparticipatingAuctions=" + participatingAuctions.toString() + "\n"
                + "\tcreatedOngoingAuctions=" + createdOngoingAuctions.toString() + "\n"
                + "\tcreatedEndedAuctions=" + createdEndedAuctions.toString() + "\n"
                + "}";
    }

    private void validateAuctionsHistory() {
        if (username == null || username.isEmpty()) {
            throw new AuctionsHistoryValidationException("Auctions history username is not set");
        }
    }
}
