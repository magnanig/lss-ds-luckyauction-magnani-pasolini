/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.domain.valueobject;

import it.unibo.luckyauction.auction.domain.entity.Auction;
import it.unibo.luckyauction.auction.domain.exception.AuctionValidationException;
import it.unibo.luckyauction.auction.domain.util.PdfGenerator;
import lombok.Generated;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Optional;

/**
 * A summary representing a created auction that is already ended.
 * A user will have a CreatedEndedAuction in their auctions' history
 * when they have created the auction, and it is ended.
 */
public final class CreatedEndedAuction extends SummaryEndedAuction {

    private final UserContactInformation sellerContacts;
    private final UserContactInformation buyerContacts;

    private CreatedEndedAuction(final String auctionId, final Product product, final String sellerUsername,
                                final BigDecimal startingPrice, final Calendar creationTime, final Calendar closingTime,
                                final Bid winningBig, final UserContactInformation sellerContacts,
                                final UserContactInformation buyerContacts) {
        super(auctionId, product, sellerUsername, startingPrice, creationTime, closingTime, winningBig);
        this.sellerContacts = sellerContacts;
        this.buyerContacts = buyerContacts;
    }

    /**
     * Build the summary for a created ended auction, starting from the given
     * complete auction with a winner.
     * @param auction   the full auction, to use to build the summary
     * @param sellerContacts    the seller contact information to add in the final pdf report
     * @param buyerContacts     the buyer contact information to add in the final pdf report
     * @return          the created ended auction summary
     */
    @Generated
    public static CreatedEndedAuction fromAuctionWithWinner(final Auction auction,
                                                            final UserContactInformation sellerContacts,
                                                            final UserContactInformation buyerContacts) {
        if (auction.getWinningBid().isEmpty()) {
            throw new AuctionValidationException("The specified auction has not a winner, i.e. a winning bid is not set. "
                    + "Please use the method fromEmptyAuction instead");
        }
        return fromAuction(auction, sellerContacts, buyerContacts);
    }

    /**
     * Build the summary for a created ended auction, starting from the given
     * complete empty auction, i.e. an auction with no winner (no bids received).
     * @param auction   the full auction, to use to build the summary
     * @return          the created ended auction summary
     */
    @Generated
    public static CreatedEndedAuction fromAuctionWithoutWinner(final Auction auction) {
        if (auction.getWinningBid().isPresent()) {
            throw new AuctionValidationException("The specified auction is not empty, i.e. a winning bid is set. "
                    + "Please use the method fromAuctionWithWinner instead");
        }
        return fromAuction(auction, null, null);
    }

    public Optional<UserContactInformation> getSellerContacts() {
        return Optional.ofNullable(sellerContacts);
    }

    public Optional<UserContactInformation> getBuyerContacts() {
        return Optional.ofNullable(buyerContacts);
    }

    /**
     * Get the PDF report, containing information about the auction sale,
     * if the auction has a winner.
     * @return  the pdf report, if someone has been awarded the auction
     */
    public Optional<PdfReport> getPdfReport() {
        if (getWinningBid() == null) {
            return Optional.empty();
        }
        return Optional.of(new PdfReport(PdfGenerator.auctionPdfGenerator(this, sellerContacts, buyerContacts)));
    }

    @Generated
    private static CreatedEndedAuction fromAuction(final Auction auction, final UserContactInformation sellerContacts,
                                                   final UserContactInformation buyerContacts) {
        return builder()
                .auctionId(auction.getAuctionId())
                .product(auction.getProduct())
                .startingPrice(auction.getStartingPrice())
                .sellerUsername(auction.getSellerUsername())
                .creationTime(auction.getCreationTime())
                .closingTime(auction.getClosingTime().orElse(null))
                .winningBid(auction.getWinningBid().orElse(null))
                .sellerContacts(sellerContacts)
                .buyerContacts(buyerContacts)
                .build();
    }

    public static CreatedEndedAuctionBuilder builder() {
        return new CreatedEndedAuctionBuilder();
    }

    @SuppressWarnings("PMD.AvoidFieldNameMatchingMethodName")
    public static final class CreatedEndedAuctionBuilder
            extends Builder<CreatedEndedAuctionBuilder, CreatedEndedAuction> {

        private UserContactInformation sellerContacts;
        private UserContactInformation buyerContacts;

        private CreatedEndedAuctionBuilder() {
            super();
        }

        public CreatedEndedAuctionBuilder sellerContacts(final UserContactInformation sellerContacts) {
            this.sellerContacts = sellerContacts;
            return this;
        }

        public CreatedEndedAuctionBuilder buyerContacts(final UserContactInformation buyerContacts) {
            this.buyerContacts = buyerContacts;
            return this;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        protected CreatedEndedAuction createObject() {
            return new CreatedEndedAuction(auctionId, product, sellerUsername, startingPrice, creationTime, closingTime,
                    winningBid, sellerContacts, buyerContacts);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        protected void validateAndPrepare() throws AuctionValidationException {
            if (winningBid != null && (sellerContacts == null || buyerContacts == null)) {
                throw new AuctionValidationException("If a winning bid is set, both seller and buyer contacts must be "
                        + "provided");
            }
            super.validateAndPrepare();
        }
    }

}
