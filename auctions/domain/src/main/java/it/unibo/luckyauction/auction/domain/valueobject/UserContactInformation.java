/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.domain.valueobject;

import it.unibo.luckyauction.auction.domain.exception.UserContactInformationValidationException;

/**
 * This class represents a user's contact information. This information
 * is used to compose the final pdf report when an auction is awarded.
 * @param email             email of the user
 * @param cellularNumber    cellular number of the user
 * @param telephoneNumber   telephone number of the user. This field is optional
 */
public record UserContactInformation(String email, String cellularNumber, String telephoneNumber) {

    public UserContactInformation(final String email, final String cellularNumber, final String telephoneNumber) {
        this.email = email;
        this.cellularNumber = cellularNumber;
        this.telephoneNumber = telephoneNumber;
        validateUserContactInformation();
    }

    public UserContactInformation(final String email, final String cellularNumber) {
        this(email, cellularNumber, null);
    }

    private void validateUserContactInformation() {
        if (email == null || email.isEmpty()) {
            throw new UserContactInformationValidationException("User's email is not set");
        }
        if (cellularNumber == null || cellularNumber.isEmpty()) {
            throw new UserContactInformationValidationException("User's cellular number is not set");
        }
    }
}
