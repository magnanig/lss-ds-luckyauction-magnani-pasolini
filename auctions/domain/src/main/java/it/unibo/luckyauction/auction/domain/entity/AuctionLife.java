/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.domain.entity;

import it.unibo.luckyauction.auction.domain.config.AuctionConstants;
import it.unibo.luckyauction.auction.domain.exception.AuctionLifeValidationException;
import it.unibo.luckyauction.util.CalendarUtils;
import lombok.Generated;

import java.time.Duration;
import java.util.Calendar;
import java.util.Objects;
import java.util.Optional;

import static it.unibo.luckyauction.auction.domain.config.AuctionConstants.getAuctionEndingZone;
import static it.unibo.luckyauction.auction.domain.config.AuctionConstants.getAuctionExtensionAmount;
import static it.unibo.luckyauction.auction.domain.config.AuctionConstants.getMaxAuctionDuration;
import static it.unibo.luckyauction.auction.domain.config.AuctionConstants.getMinAuctionDuration;
import static it.unibo.luckyauction.util.NullableUtils.safeCall;

/**
 * A class representing and managing the life of an auction, from
 * the creation to the termination.
 */
public final class AuctionLife {

    private final Calendar creationTime;
    private Calendar expectedClosingTime;
    private Calendar closingTime;
    private Duration extensionAmount;

    private AuctionLife(final Calendar creationTime, final Calendar expectedClosingTime,
                       final Calendar closingTime, final Duration extensionAmount) {
        this.creationTime = creationTime;
        this.expectedClosingTime = expectedClosingTime;
        this.closingTime = closingTime;
        this.extensionAmount = extensionAmount;
    }

    /**
     * Get the creation time of the auction.
     */
    public Calendar getCreationTime() {
        return CalendarUtils.copy(creationTime);
    }

    /**
     * Get the optional extension amount, i.e. the amount of time for which
     * auction is extended if no bids are received when auction should close.
     */
    public Optional<Duration> getExtensionAmount() {
        return Optional.ofNullable(extensionAmount);
    }

    /**
     * Get the duration of the auction.
     */
    public Duration getDuration() {
        return CalendarUtils.diff(expectedClosingTime, creationTime);
    }

    /**
     * Get the expected closing time.
     */
    public Calendar getExpectedClosingTime() {
        return CalendarUtils.copy(expectedClosingTime);
    }

    /**
     * Get the optional closing time.
     */
    public Optional<Calendar> getClosingTime() {
        return Optional.ofNullable(closingTime);
    }

    /**
     * Check whether the auction is ended or not.
     * @return  true if the auction is ended, false otherwise
     */
    public boolean isEnded() {
        return !isOngoing();
    }

    /**
     * Check whether the auction is still ongoing or not.
     * @return  true if the auction is ongoing, false otherwise
     */
    public boolean isOngoing() {
        return getClosingTime().isEmpty();
    }

    /**
     * Check whether the auction is in ending zone, i.e. the conclusive minutes
     * (see {@link AuctionConstants} for details about times).
     * @return  true if auction is in ending zone, false otherwise
     */
    public boolean isInEndingZone() {
        return remainingTime().compareTo(getAuctionEndingZone(getDuration())) < 0;
    }

    /**
     * Get the auction remaining time.
     */
    public Duration remainingTime() {
        return CalendarUtils.diff(expectedClosingTime, Calendar.getInstance());
    }

    /**
     * Notify that the expected closing time has been reached. If no bids have
     * been received and automatic extension is enabled, extend the auction for the
     * previously set amount, otherwise terminate the auction and save the closing
     * time.
     * @param noBidsReceived    whether no bids have been received (true) or yes (false)
     */
    public void notifyReachedExpectedClosingTime(final boolean noBidsReceived) {
        if (noBidsReceived && extensionAmount != null) {
            extendDuration(extensionAmount);
            extensionAmount = null;
        } else {
            closingTime = expectedClosingTime;
        }
    }

    /**
     * Notify that a new bid has been raised. If auction is in ending zone,
     * the expected ended time will be postponed by the amount specified in
     * {@link AuctionConstants}.
     * @return  true if the auction has been extended, false otherwise
     */
    public Boolean notifyBidRaised() {
        if (isInEndingZone()) {
            extendDuration(getAuctionExtensionAmount(getDuration()));
            return true;
        }
        return false;
    }

    /**
     * Extend auction by the specified duration.
     * @param extensionDuration     the amount of time to add
     */
    /* package */ void extendDuration(final Duration extensionDuration) {
        this.expectedClosingTime = CalendarUtils.add(expectedClosingTime, extensionDuration);
    }

    @Override @Generated
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final AuctionLife that = (AuctionLife) o;
        return creationTime.equals(that.creationTime) && expectedClosingTime.equals(that.expectedClosingTime)
                && Objects.equals(closingTime, that.closingTime)
                && Objects.equals(extensionAmount, that.extensionAmount);
    }

    @Override @Generated
    public int hashCode() {
        return Objects.hash(creationTime, expectedClosingTime, closingTime, extensionAmount);
    }

    @Override @Generated
    public String toString() {
        return "AuctionLife{\n"
                + "\t\tcreationTime=" + creationTime.getTime() + "\n"
                + "\t\texpectedClosingTime=" + expectedClosingTime.getTime() + "\n"
                + "\t\tclosingTime=" + safeCall(closingTime, Calendar::getTime) + "\n"
                + "\t\textensionAmount=" + extensionAmount + "\n"
                + "\t}";
    }

    public static AuctionLifeBuilder builder() {
        return new AuctionLifeBuilder();
    }

    @SuppressWarnings("PMD.AvoidFieldNameMatchingMethodName")
    public static final class AuctionLifeBuilder {
        private Calendar creationTime;
        private Calendar expectedClosingTime;
        private Calendar closingTime;
        private Duration duration;
        private Duration extensionDuration;

        private AuctionLifeBuilder() { }

        public AuctionLifeBuilder creationTime(final Calendar creationTime) {
            this.creationTime = CalendarUtils.copy(creationTime);
            return this;
        }

        public AuctionLifeBuilder expectedClosingTime(final Calendar expectedClosingTime) {
            this.expectedClosingTime = CalendarUtils.copy(expectedClosingTime);
            return this;
        }

        public AuctionLifeBuilder closingTime(final Calendar closingTime) {
            this.closingTime = CalendarUtils.copy(closingTime);
            return this;
        }

        public AuctionLifeBuilder duration(final Duration duration) {
            this.duration = duration;
            return this;
        }

        public AuctionLifeBuilder extensionDuration(final Duration extensionDuration) {
            this.extensionDuration = extensionDuration;
            return this;
        }

        public AuctionLife build() {
            validateAuctionLifeObject();
            return new AuctionLife(creationTime, expectedClosingTime, closingTime, extensionDuration);
        }

        private void validateAuctionLifeObject() {
            if (creationTime == null) {
                if (closingTime != null) {
                    // if closing time is set, then the creation time is mandatory
                    throw new AuctionLifeValidationException("Since you specified a closing time, a creation time "
                            + "must be provided too");
                }
                // if none between creation and closing time are set, then creation time is set with the current time
                creationTime = Calendar.getInstance();
            }
            // closing time cannot be a future date
            if (closingTime != null && closingTime.after(Calendar.getInstance())) {
                throw new AuctionLifeValidationException("Closing time cannot be a future date");
            }
            // closing time cannot be equal to or less than creation time
            if (closingTime != null && closingTime.compareTo(creationTime) <= 0) {
                throw new AuctionLifeValidationException("Closing time cannot be equal to or less than creation time");
            }
            // duration can be absent only if the creation time and the closing time are present
            if (closingTime != null) {
                final Duration effectiveDuration = CalendarUtils.diff(closingTime, creationTime);
                if (duration != null && Math.abs(effectiveDuration.minus(duration).toMinutes()) > 1) {
                    throw new AuctionLifeValidationException("The specified duration does not correspond to the "
                            + "effective auction duration (based on closing time and creation time)");
                }
                duration = duration == null ? effectiveDuration : duration;
            }
            // if creationTime and closingTime are not set, either duration or expected closing time are mandatory
            if (duration == null) {
                if (expectedClosingTime == null) {
                    throw new AuctionLifeValidationException("Auction duration is not set. Please specify a duration "
                            + "or an expected closing time");
                }
                duration = CalendarUtils.diff(expectedClosingTime, creationTime);
            }
            // check if the extensionDuration is within the minimum and maximum allowed
            checkValidExtensionDuration(extensionDuration);
            // if the closingTime is set, then the expectedClosingTime is set equal to the closingTime.
            // Otherwise, if the closingTime is not set then the expectedClosingTime is set automatically
            // by adding the actual duration to the creationTime
            if (expectedClosingTime == null) {
                expectedClosingTime = closingTime == null ? CalendarUtils.add(creationTime, duration) : closingTime;
            }
        }

        private void checkValidExtensionDuration(final Duration extensionDuration) {
            if (extensionDuration != null && (extensionDuration.compareTo(getMaxAuctionDuration()) > 0
                    || extensionDuration.compareTo(getMinAuctionDuration()) < 0)) {
                throw new AuctionLifeValidationException("Wrong extensionDuration! The auction extensionDuration "
                        + "times must be between " + getMinAuctionDuration().toHours() + " hours and "
                        + getMaxAuctionDuration().toDays() + " days");
            }
        }
    }
}
