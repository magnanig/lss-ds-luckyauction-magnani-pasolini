/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.domain.entity;

import it.unibo.luckyauction.auction.domain.valueobject.Bid;
import it.unibo.luckyauction.auction.domain.valueobject.Product;
import it.unibo.luckyauction.util.CalendarUtils;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

import static it.unibo.luckyauction.util.CollectionsUtils.max;

public final class Auction extends AbstractAuctionWithLife {

    /**
     * How much the new bid must be greater, at least, than the actual one
     * (or the starting price, if no bid is present).
     */
    public static final BigDecimal MIN_PRICE_RAISED = BigDecimal.ONE;

    private Timer timer;

    // Here the inherited auctionId field is the primary key

    private Auction(final String auctionId, final Product product, final String sellerUsername,
                    final BigDecimal startingPrice, final AuctionLife auctionLife, final Bid actualBid,
                    final Bid winningBid, final BidsHistory bidsHistory) {
        super(auctionId, product, sellerUsername, startingPrice, auctionLife, actualBid, winningBid, bidsHistory);
    }

    /**
     * Start the auction, i.e. start a timer for the expected closing time.
     * @throws IllegalStateException    if the auction has already been closed
     *                                  (i.e. there is already a closing time set),
     *                                  or if this method is called after the expected
     *                                  closing time
     */
    public synchronized void start() {
        if (!isActive() || Calendar.getInstance().after(getExpectedClosingTime())) {
            throw new IllegalStateException("Auction has already been closed");
        }
        scheduleTimerForReachedExpectedClosingTime();
    }

    /**
     * Resume the auction after a server interruption. In order to guarantee fairness,
     * the auction will be extended by the duration of the interruption.
     * For example, if auction should have been closed at 5 pm, but an interruption occurred
     * starting from 4 pm, the auction will be extended by 1 hour (starting from when this
     * method gets called).
     * @param interruptionBeginTime     when the interruption began
     */
    public synchronized void resumeAfterInterruption(final Calendar interruptionBeginTime) {
        final Calendar currentTime = Calendar.getInstance();
        if (interruptionBeginTime.after(currentTime)) {
            throw new IllegalArgumentException("The interruption can't be in the future");
        }
        if (isActive()) {
            // if the auction closing operations didn't finish before the interruption
            if (interruptionBeginTime.after(getExpectedClosingTime())) {
                notifyReachedExpectedClosingTime(); // completion of auction closing operations
            } else { // extend the expected closing time by the time the server has been down
                extendDuration(CalendarUtils.diff(currentTime, max(interruptionBeginTime, getCreationTime())));
                scheduleTimerForReachedExpectedClosingTime();
            }
        }
    }

    public synchronized void resume() {
        if (Calendar.getInstance().before(getExpectedClosingTime())) {
            scheduleTimerForReachedExpectedClosingTime();
        } else {
            notifyReachedExpectedClosingTime();
        }
    }

    /**
     * Cancel the auction, i.e. stop its timer. Note that this method
     * does not make the auction end (i.e. if auction was active before
     * calling this method, it will still remain so).
     */
    public void cancel() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized Boolean addNewBid(final Bid bid) {
        if (super.addNewBid(bid)) {
            scheduleTimerForReachedExpectedClosingTime();
            return true;
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected synchronized void notifyReachedExpectedClosingTime() {
        super.notifyReachedExpectedClosingTime();
        if (isActive()) {
            scheduleTimerForReachedExpectedClosingTime();
        }
    }

    private void scheduleTimerForReachedExpectedClosingTime() {
        if (timer != null) {
            timer.cancel();
        }
        timer = new Timer();
        timer.schedule(new ReachedExpectedClosingTime(), getExpectedClosingTime().getTime());
    }

    @Override
    public Auction copy() {
        return Auction.builder()
                .auctionId(this.getAuctionId())
                .product(this.getProduct())
                .creationTime(this.getCreationTime())
                .sellerUsername(this.getSellerUsername())
                .startingPrice(this.getStartingPrice())
                .duration(this.getDuration())
                .extensionDuration(this.getExtensionAmount().isPresent() ? this.getExtensionAmount().get() : null)
                .bids(this.getBids())
                .bidsHistoryId(this.getBidsHistoryId())
                .actualBid(this.getActualBid().isPresent() ? this.getActualBid().get() : null)
                .winningBid(this.getWinningBid().isPresent() ? this.getWinningBid().get() : null)
                .expectedClosingTime(this.getExpectedClosingTime())
                .closingTime(this.getClosingTime().isPresent() ? this.getClosingTime().get() : null)
                .build();
    }

    public static AuctionBuilder builder() {
        return new AuctionBuilder();
    }

    public static class AuctionBuilder extends Builder<AuctionBuilder, Auction> {

        protected AuctionBuilder() {
            super();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        protected Auction createObject() {
            return new Auction(auctionId, product, sellerUsername, startingPrice, auctionLife, actualBid,
                    winningBid, bidsHistory);
        }

    }

    private class ReachedExpectedClosingTime extends TimerTask {

        @Override
        public void run() {
            notifyReachedExpectedClosingTime();
        }
    }
}
