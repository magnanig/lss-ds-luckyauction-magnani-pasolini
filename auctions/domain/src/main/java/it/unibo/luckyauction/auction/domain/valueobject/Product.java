/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.domain.valueobject;

import it.unibo.luckyauction.auction.domain.exception.ProductValidationException;
import lombok.Generated;

/**
 * Create a new product with the specified parameters.
 * @param name          the name of the product
 * @param description   an optional description for the product
 * @param category      the product category
 * @param state         the state (i.e. condition) of the product
 * @param image         an image representing the product, in base 64 encoding
 */
public record Product(String name, String description, ProductCategory category, ProductState state, String image) {

    public static ProductBuilder builder() {
        return new ProductBuilder();
    }

    @Override @Generated
    public String toString() {
        return "Product{"
                + "name='" + name + '\''
                + ", description='" + description + '\''
                + ", category=" + category
                + ", state=" + state
                + '}';
    }

    @SuppressWarnings({"checkstyle:JavadocVariable", "PMD.AvoidFieldNameMatchingMethodName"})
    public static final class ProductBuilder {
        private String name;
        private String description;
        private ProductCategory productCategory;
        private ProductState state;
        private String image;

        private ProductBuilder() { }

        public ProductBuilder name(final String name) {
            this.name = name;
            return this;
        }

        public ProductBuilder description(final String description) {
            this.description = description;
            return this;
        }

        public ProductBuilder category(final ProductCategory productCategory) {
            this.productCategory = productCategory;
            return this;
        }

        public ProductBuilder state(final ProductState state) {
            this.state = state;
            return this;
        }

        public ProductBuilder image(final String image) {
            this.image = image;
            return this;
        }

        public Product build() {
            validateProductObject();
            return new Product(name, description, productCategory, state, image);
        }

        private void validateProductObject() {
            if (name == null || name.isEmpty()) {
                throw new ProductValidationException("Product name is not set");
            }
            if (productCategory == null) {
                throw new ProductValidationException("Product category is not set");
            }
            if (state == null) {
                throw new ProductValidationException("Product state is not set");
            }
            if (image == null) {
                throw new ProductValidationException("Product image is not set");
            }
        }
    }
}
