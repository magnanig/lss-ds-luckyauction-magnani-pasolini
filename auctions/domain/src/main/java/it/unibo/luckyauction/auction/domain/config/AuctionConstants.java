/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.domain.config;

import java.time.Duration;

import static it.unibo.luckyauction.configuration.ConfigurationConstants.TEST_MODE;

public final class AuctionConstants {
    // Useful to be printed in javadoc
    private static final int MIN_AUCTION_HOURS = 1;
    private static final int MAX_AUCTION_DAYS = 7;
    private static final int FLASH_AUCTION_HOURS = 3;
    private static final int AUCTION_ENDING_ZONE_MINUTES = 30;
    private static final int AUCTION_EXTENSION_AMOUNT_HOURS = 1;
    private static final int FLASH_AUCTION_ENDING_ZONE_MINUTES = 15;
    private static final int FLASH_AUCTION_EXTENSION_AMOUNT_MINUTES = 30;

    private static final Duration MIN_AUCTION_DURATION = Duration.ofHours(MIN_AUCTION_HOURS);
    private static final Duration MAX_AUCTION_DURATION = Duration.ofDays(MAX_AUCTION_DAYS);
    private static final Duration FLASH_AUCTION_DURATION = Duration.ofHours(FLASH_AUCTION_HOURS);
    private static final Duration AUCTION_ENDING_ZONE = Duration.ofMinutes(AUCTION_ENDING_ZONE_MINUTES);
    private static final Duration AUCTION_EXTENSION_AMOUNT = Duration.ofHours(AUCTION_EXTENSION_AMOUNT_HOURS);
    private static final Duration FLASH_AUCTION_ENDING_ZONE = Duration.ofMinutes(FLASH_AUCTION_ENDING_ZONE_MINUTES);
    private static final Duration FLASH_AUCTION_EXTENSION_AMOUNT =
            Duration.ofMinutes(FLASH_AUCTION_EXTENSION_AMOUNT_MINUTES);
    private static final Duration TEST_FAST_AUCTION_EXTENSION_AMOUNT = Duration.ofMillis(50);

    public static Duration getMinAuctionDuration() {
        return TEST_MODE ? Duration.ofMillis(1) : MIN_AUCTION_DURATION;
    }

    public static Duration getMaxAuctionDuration() {
        return MAX_AUCTION_DURATION;
    }

    /**
     * Get the ending zone duration (starting from the end) for the given auction
     * duration. For flash auction (i.e. auction with duration no longer than
     * {@value FLASH_AUCTION_HOURS} hours), it corresponds to last
     * {@value FLASH_AUCTION_ENDING_ZONE_MINUTES} minutes, otherwise last
     * {@value AUCTION_ENDING_ZONE_MINUTES} minutes.
     * @param auctionDuration   the auction duration
     * @return                  the duration of the ending zone, starting from the expected
     *                          closing time
     */
    public static Duration getAuctionEndingZone(final Duration auctionDuration) {
        if (TEST_MODE && auctionDuration.compareTo(MIN_AUCTION_DURATION) < 0) {
            return TEST_FAST_AUCTION_EXTENSION_AMOUNT;
        }
        return auctionDuration.compareTo(FLASH_AUCTION_DURATION) > 0
                ? AUCTION_ENDING_ZONE
                : FLASH_AUCTION_ENDING_ZONE;
    }

    /**
     * Get the extension duration, to be applied when raising bid during the ending
     * zone. For flash auction (i.e. auction with duration no longer than
     * {@value FLASH_AUCTION_HOURS} hours), it corresponds to
     * {@value FLASH_AUCTION_EXTENSION_AMOUNT_MINUTES} minutes, otherwise
     * {@value AUCTION_EXTENSION_AMOUNT_HOURS} hours.
     * @param auctionDuration   the auction duration
     * @return                  the duration of the auction extension
     */
    public static Duration getAuctionExtensionAmount(final Duration auctionDuration) {
        if (TEST_MODE && auctionDuration.compareTo(MIN_AUCTION_DURATION) < 0) {
            return TEST_FAST_AUCTION_EXTENSION_AMOUNT;
        }
        return auctionDuration.compareTo(FLASH_AUCTION_DURATION) > 0
                ? AUCTION_EXTENSION_AMOUNT
                : FLASH_AUCTION_EXTENSION_AMOUNT;
    }

    private AuctionConstants() { }
}
