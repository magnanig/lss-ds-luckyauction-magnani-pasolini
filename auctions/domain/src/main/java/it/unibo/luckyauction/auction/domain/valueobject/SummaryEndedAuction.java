/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.domain.valueobject;

import it.unibo.luckyauction.auction.domain.exception.AuctionValidationException;
import it.unibo.luckyauction.util.CalendarUtils;
import lombok.Generated;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Objects;

/**
 * A summary for a generic ended auction.
 */
public abstract class SummaryEndedAuction extends AbstractAuction {

    private final Calendar creationTime;
    private final Calendar closingTime;
    private final Bid winningBid;

    /**
     * Configure auction with the specified parameters.
     * @param auctionId         the id of the auction
     * @param product           the product associated to the auction
     * @param sellerUsername    the username of the seller (i.e. who created the auction)
     * @param startingPrice     the starting price
     * @param creationTime      the creation time
     * @param closingTime       the closing time
     * @param winningBid        the winning bid
     */
    protected SummaryEndedAuction(final String auctionId, final Product product, final String sellerUsername,
                                  final BigDecimal startingPrice, final Calendar creationTime,
                                  final Calendar closingTime, final Bid winningBid) {
        super(auctionId, product, sellerUsername, startingPrice);
        this.creationTime = creationTime;
        this.closingTime = closingTime;
        this.winningBid = winningBid;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Calendar getCreationTime() {
        return CalendarUtils.copy(creationTime);
    }

    /**
     * Get the closing time.
     */
    public Calendar getClosingTime() {
        return CalendarUtils.copy(closingTime);
    }

    /**
     * Get the winning bid.
     * @return  the winning bid, or <code>null</code> if the auction has
     *          ended without any bid
     */
    public Bid getWinningBid() {
        return winningBid;
    }

    /**
     * {@inheritDoc}
     */
    @Override @Generated
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        final SummaryEndedAuction that = (SummaryEndedAuction) o;
        return creationTime.equals(that.creationTime) && closingTime.equals(that.closingTime)
                && Objects.equals(winningBid, that.winningBid);
    }

    /**
     * {@inheritDoc}
     */
    @Override @Generated
    public int hashCode() {
        return Objects.hash(super.hashCode(), creationTime, closingTime, winningBid);
    }

    /**
     * {@inheritDoc}
     */
    @Override @Generated
    public String toString() {
        return super.toString()
                + "\tcreationTime=" + creationTime + "\n"
                + "\tclosingTime=" + closingTime + "\n"
                + "\twinningBid=" + winningBid;
    }

    @SuppressWarnings({"checkstyle:all", "PMD.AvoidFieldNameMatchingMethodName", "unchecked"})
    protected abstract static class Builder<
            T extends Builder<T, R>,
            R extends SummaryEndedAuction> extends AbstractAuction.Builder<T, R> {

        protected Calendar closingTime;
        protected Bid winningBid;

        public T closingTime(final Calendar closingTime) {
            this.closingTime = CalendarUtils.copy(closingTime);
            return (T) this;
        }

        public T winningBid(final Bid winningBid) {
            this.winningBid = winningBid;
            return (T) this;
        }

        /**
         * {@inheritDoc}
         * @throws AuctionValidationException
         */
        @Override
        protected void validateAndPrepare() throws AuctionValidationException {
            super.validateAndPrepare();
            if (closingTime == null) {
                throw new AuctionValidationException("Closing time is not set");
            }
            if (closingTime.compareTo(creationTime) <= 0) {
                throw new AuctionValidationException("Closing time cannot be equal to or less than creation time");
            }
            if (winningBid != null && winningBid.getBidderUsername().equals(sellerUsername)) {
                throw new AuctionValidationException("The auction winner cannot be the seller himself");
            }
        }
    }
}
