/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.domain.valueobject;

import it.unibo.luckyauction.auction.domain.entity.Auction;
import lombok.Generated;

import java.math.BigDecimal;
import java.time.Duration;
import java.util.Calendar;

/**
 * A summary representing a participation to an auction that is still ongoing.
 * A user will have a ParticipatingAuction in their auctions' history when
 * they have raised at least a bid for the auction.
 */
public final class ParticipatingAuction extends SummaryOngoingAuction {

    private ParticipatingAuction(final String auctionId, final Product product, final String sellerUsername,
                                 final Calendar creationTime, final Calendar expectedClosingTime, final Bid actualBid,
                                 final Duration duration, final BigDecimal startingPrice,
                                 final Duration extensionDuration) {
        super(auctionId, product, sellerUsername, actualBid, startingPrice, creationTime, duration, extensionDuration,
                expectedClosingTime);
    }

    /**
     * Build the summary for a participating auction, starting from the given
     * complete auction.
     * @param auction   the full auction, to use to build the summary
     * @return          the participating auction summary
     */
    @Generated
    public static ParticipatingAuction fromAuction(final Auction auction) {
        return builder()
                .auctionId(auction.getAuctionId())
                .product(auction.getProduct())
                .startingPrice(auction.getStartingPrice())
                .actualBid(auction.getActualBid().orElse(null))
                .sellerUsername(auction.getSellerUsername())
                .creationTime(auction.getCreationTime())
                .expectedClosingTime(auction.getExpectedClosingTime())
                .extensionDuration(auction.getExtensionAmount().orElse(null))
                .build();
    }

    public static ParticipatingAuctionBuilder builder() {
        return new ParticipatingAuctionBuilder();
    }

    public static class ParticipatingAuctionBuilder
            extends SummaryOngoingAuction.Builder<ParticipatingAuctionBuilder, ParticipatingAuction> {

        protected ParticipatingAuctionBuilder() {
            super();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        protected ParticipatingAuction createObject() {
            return new ParticipatingAuction(auctionId, product, sellerUsername, creationTime, expectedClosingTime,
                    actualBid, duration, startingPrice, extensionDuration);
        }

    }
}
