/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.domain.valueobject;

/**
 * The state (i.e. condition) of a product.
 */
public enum ProductState {
    /**
     * A new product.
     */
    NEW,
    /**
     * A used product in optimum condition (i.e. with no damage).
     */
    USED_OPTIMUM_CONDITION,
    /**
     * A used product in good condition (e.g. working product
     * with some little damage).
     */
    USED_GOOD_CONDITION,
    /**
     * A used product in bad condition (e.g. working product
     * with many damages).
     */
    USED_BAD_CONDITION,
    /**
     * A used product in poor condition (e.g. non-working product
     * with a lot of damage).
     */
    USED_POOR_CONDITION
}
