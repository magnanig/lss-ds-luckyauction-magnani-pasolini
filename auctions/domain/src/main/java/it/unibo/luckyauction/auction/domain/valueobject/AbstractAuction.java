/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.domain.valueobject;

import it.unibo.luckyauction.auction.domain.exception.AuctionValidationException;
import it.unibo.luckyauction.util.CalendarUtils;
import lombok.Generated;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Objects;

/**
 * A generic auction containing basilar information, such as
 * auction id, product, seller username and creation time.
 */
public abstract class AbstractAuction {

    private final String auctionId;
    private final Product product;
    private final String sellerUsername;
    private final BigDecimal startingPrice;

    /**
     * Configure auction with the specified parameters.
     * @param auctionId         the id of the auction
     * @param product           the product associated to the auction
     * @param sellerUsername    the username of the seller (i.e. who created the auction)
     * @param startingPrice     the starting price
     */
    protected AbstractAuction(final String auctionId, final Product product, final String sellerUsername,
                              final BigDecimal startingPrice) {
        this.auctionId = auctionId;
        this.product = product;
        this.sellerUsername = sellerUsername;
        this.startingPrice = startingPrice;
    }

    /**
     * Get the auction id.
     * @return  the auction id
     */
    public String getAuctionId() {
        return auctionId;
    }

    /**
     * Get the product associated to the current auction.
     * @return  the product in sale
     */
    public Product getProduct() {
        return product;
    }

    /**
     * Get the seller username (i.e. who created the auction).
     * @return  the username of the seller
     */
    public String getSellerUsername() {
        return sellerUsername;
    }

    /**
     * Get the starting price.
     */
    public BigDecimal getStartingPrice() {
        return startingPrice;
    }

    /**
     * Get the creation time of the auction.
     * @return  the creation time
     */
    public abstract Calendar getCreationTime();

    /**
     * {@inheritDoc}
     */
    @Override @Generated
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final AbstractAuction that = (AbstractAuction) o;
        return auctionId.equals(that.auctionId) && product.equals(that.product)
                && sellerUsername.equals(that.sellerUsername) && startingPrice.compareTo(that.startingPrice) == 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override @Generated
    public int hashCode() {
        return Objects.hash(auctionId, product, sellerUsername, startingPrice);
    }

    /**
     * {@inheritDoc}
     */
    @Override @Generated
    public String toString() {
        return "\n"
                + "\tauctionId='" + auctionId + "'\n"
                + "\tproduct=" + product.toString() + "\n"
                + "\tsellerUsername='" + sellerUsername + "'\n"
                + "\tstartingPrice=" + startingPrice;
    }

    @SuppressWarnings({"checkstyle:all", "PMD.AvoidFieldNameMatchingMethodName", "unchecked"})
    protected abstract static class Builder<
            T extends Builder<T, R>,
            R extends AbstractAuction> {

        protected String auctionId;
        protected Product product;
        protected Calendar creationTime;
        protected String sellerUsername;
        protected BigDecimal startingPrice;

        public T auctionId(final String auctionId) {
            this.auctionId = auctionId;
            return (T) this;
        }

        public T product(final Product product) {
            this.product = product;
            return (T) this;
        }

        public T creationTime(final Calendar creationTime) {
            this.creationTime = CalendarUtils.copy(creationTime);
            return (T) this;
        }

        public T sellerUsername(final String sellerUsername) {
            this.sellerUsername = sellerUsername;
            return (T) this;
        }

        public T startingPrice(final BigDecimal startingPrice) {
            this.startingPrice = startingPrice;
            return (T) this;
        }

        public final R build() {
            validateAndPrepare();
            return createObject();
        }

        /**
         * Check whether object is ready to be built (i.e. if all non-null fields
         * are set with valid values) and then prepare object for the build.
         * If you need to override this method, remember to call super (either at
         * the beginning or at the end).
         *
         * @throws AuctionValidationException  if one or more fields are not valid
         */
        protected void validateAndPrepare() throws AuctionValidationException {
            if (creationTime == null) {
                creationTime = Calendar.getInstance();
            }
            if (product == null) {
                throw new AuctionValidationException("Product is not set");
            }
            if (sellerUsername == null || sellerUsername.isEmpty()) {
                throw new AuctionValidationException("Seller username is not set");
            }
            if (startingPrice == null) {
                throw new AuctionValidationException("Starting price is not set");
            }
            if (startingPrice.compareTo(BigDecimal.ZERO) <= 0 || startingPrice.scale() > 2) {
                throw new AuctionValidationException("Starting price must be greater than zero and can have at most "
                        + "two decimal digits");
            }
        }

        /**
         * Create the object that current builder is able to.
         * @return  the built object, with the desired field properly set
         */
        protected abstract R createObject();
    }

}
