/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.domain.event;

import it.unibo.luckyauction.auction.domain.entity.AbstractAuctionWithLife;

/**
 * Represents an event that notifies the closure of an auction.
 */
public class AuctionClosed implements AuctionEvent {

    private final AbstractAuctionWithLife closedAuction;

    /**
     * Create a new instance to represent the closing of an auction.
     * @param closedAuction the auction that has been closed
     */
    public AuctionClosed(final AbstractAuctionWithLife closedAuction) {
        this.closedAuction = closedAuction.copy();
    }

    /**
     * Get the closed auction.
     */
    public AbstractAuctionWithLife getClosedAuction() {
        return closedAuction.copy();
    }
}
