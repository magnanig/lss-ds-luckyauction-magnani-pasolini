/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.domain.entity;

import it.unibo.luckyauction.auction.domain.valueobject.Bid;
import it.unibo.luckyauction.util.CollectionsUtils;
import lombok.Generated;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

/**
 * Represents the list of the bids associated to an auction.
 */
public class BidsHistory {

    private final String bidsHistoryId;
    private final List<Bid> bids;

    private final CollectionsUtils<Bid> collectionsUtils;

    /**
     * Create a new bids' history with the specified id and bids. If bids
     * are not sorted, they will be ordered by timestamp.
     * @param bidsHistoryId     the id of the bids history
     * @param bids              a collection of bids
     */
    public BidsHistory(final String bidsHistoryId, final Collection<Bid> bids) {
        this.collectionsUtils = new CollectionsUtils<>(Comparator.comparing(Bid::getTimestamp));
        this.bidsHistoryId = bidsHistoryId;
        this.bids = bids == null ? Collections.emptyList() : collectionsUtils.sort(bids);
    }

    /**
     * Create a new empty bids history with the specified id.
     * @param bidsHistoryId     the id of the bids history
     */
    public BidsHistory(final String bidsHistoryId) {
        this(bidsHistoryId, Collections.emptyList());
    }

    /**
     * Get the id of the bids' history.
     */
    public String getBidsHistoryId() {
        return bidsHistoryId;
    }

    /**
     * Get all bids, sorted by their timestamp.
     * @return  the list of bids, sorted by timestamp
     */
    public List<Bid> getBids() {
        return Collections.unmodifiableList(bids);
    }

    /**
     * Add a new bid to the history.
     * @param bid   the bid to add
     */
    public void addBid(final Bid bid) {
        bids.add(bid);
    }

    /**
     * Add all the specified bids to the history. If the bids to add
     * are not sorted, they will be added after being ordered by timestamp.
     * @param bids      the bids to add
     */
    public void addAllBids(final Collection<Bid> bids) {
        this.bids.addAll(collectionsUtils.sort(bids));
    }

    /**
     * {@inheritDoc}
     */
    @Override @Generated
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final BidsHistory that = (BidsHistory) o;
        return bidsHistoryId.equals(that.bidsHistoryId) && bids.equals(that.bids);
    }

    /**
     * {@inheritDoc}
     */
    @Override @Generated
    public int hashCode() {
        return Objects.hash(bidsHistoryId, bids);
    }

    /**
     * {@inheritDoc}
     */
    @Override @Generated
    public String toString() {
        return "BidsHistory{\n"
                + "\t\tbidsHistoryId='" + bidsHistoryId + "'\n"
                + "\t\tbids=" + bids + "\n"
                + "\t}";
    }
}
