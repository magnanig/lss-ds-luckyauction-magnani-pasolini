/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.domain.entity;

import it.unibo.luckyauction.auction.domain.config.AuctionConstants;
import it.unibo.luckyauction.auction.domain.event.AuctionClosed;
import it.unibo.luckyauction.auction.domain.event.AuctionDomainEvent;
import it.unibo.luckyauction.auction.domain.event.AuctionEventSource;
import it.unibo.luckyauction.auction.domain.event.AuctionExtended;
import it.unibo.luckyauction.auction.domain.event.AuctionPostponedOnBidRaise;
import it.unibo.luckyauction.auction.domain.exception.AuctionValidationException;
import it.unibo.luckyauction.auction.domain.valueobject.AbstractOngoingAuction;
import it.unibo.luckyauction.auction.domain.valueobject.Bid;
import it.unibo.luckyauction.auction.domain.valueobject.Product;
import it.unibo.luckyauction.util.CalendarUtils;
import lombok.Generated;

import java.math.BigDecimal;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * A generic auction with all information about its life cycle,
 * from the creation, to the eventual ending, tracking all bids history.
 */
public abstract class AbstractAuctionWithLife extends AbstractOngoingAuction implements AuctionEventSource {

    private final AuctionLife auctionLife;
    private Bid winningBid;
    private final BidsHistory bidsHistory;

    /**
     * Configure auction with the specified parameters.
     * @param auctionId             the id of the auction
     * @param product               the product associated to the auction
     * @param sellerUsername        the username of the seller (i.e. who created the auction)
     * @param startingPrice         the starting price
     * @param auctionLife           the information about auction life (times and extension)
     * @param actualBid             the optional actual bid (if not specified use <code>null</code>)
     * @param winningBid            the winning bid, if any (otherwise use null)
     * @param bidsHistory           the bid history object to track all bids
     */
    protected AbstractAuctionWithLife(final String auctionId, final Product product, final String sellerUsername,
                                      final BigDecimal startingPrice, final AuctionLife auctionLife,
                                      final Bid actualBid, final Bid winningBid, final BidsHistory bidsHistory) {
        super(auctionId, product, sellerUsername, actualBid, startingPrice);
        this.auctionLife = auctionLife;
        this.winningBid = winningBid;
        this.bidsHistory = bidsHistory;
    }

    public abstract Auction copy();

    /**
     * Add new bid to the current auction. Auction will be extended automatically
     * if this method is called while in ending zone (see {@link AuctionConstants}
     * for the amount of time). <br>
     * This method will raise a {@link AuctionPostponedOnBidRaise} event if the
     * expected closing time has changed, due to a bid raise in ending zone. <br>
     * Note: do not confuse the <b>mandatory</b> extension for bid raise in ending zone
     * with the <b>optional</b> auction extension that happens when reaching the expected
     * closing time if no bids are received.
     * @param bid   the bid to add
     * @return      true if auction has been extended, false otherwise
     */
    public synchronized Boolean addNewBid(final Bid bid) {
        setActualBid(bid);
        bidsHistory.addBid(bid);
        if (auctionLife.notifyBidRaised()) {
            AuctionDomainEvent.raise(new AuctionPostponedOnBidRaise(getAuctionId(), getExpectedClosingTime()));
            return true;
        }
        return false;
    }

    /**
     * Check whether auction is still active (i.e. ongoing) or not.
     * @return  true if auction is still active, false otherwise
     */
    public synchronized boolean isActive() {
        return auctionLife.isOngoing();
    }

    /**
     * Get the winning bid, if present.
     */
    public synchronized Optional<Bid> getWinningBid() {
        return Optional.ofNullable(winningBid);
    }

    /**
     * Get the bid history id.
     */
    public String getBidsHistoryId() {
        return bidsHistory.getBidsHistoryId();
    }

    /**
     * Get the list of bids, sorted by timestamp.
     */
    public synchronized List<Bid> getBids() {
        return bidsHistory.getBids();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Calendar getCreationTime() {
        return auctionLife.getCreationTime();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized Optional<Duration> getExtensionAmount() {
        return auctionLife.getExtensionAmount();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Duration getDuration() {
        return auctionLife.getDuration();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized Calendar getExpectedClosingTime() {
        return auctionLife.getExpectedClosingTime();
    }

    /**
     * Get the optional closing time.
     */
    public synchronized Optional<Calendar> getClosingTime() {
        return auctionLife.getClosingTime();
    }

    /**
     * Extend auction by the specified duration and produce the
     * AuctionExpectingClosingTimeUpdated event.
     * @param extensionDuration     the amount of time to add
     */
    protected synchronized void extendDuration(final Duration extensionDuration) {
        auctionLife.extendDuration(extensionDuration);
        AuctionDomainEvent.raise(new AuctionPostponedOnBidRaise(getAuctionId(), getExpectedClosingTime()));
    }

    /**
     * Notify that the expected closing time has been reached. Then, the actual bid,
     * if set, becomes the winning bid, otherwise the auction is extended, if automatic
     * extension has been specified. <br>
     * Anyway, a new event is raised, to notify what is happened (i.e. extension or auction
     * closed, see {@link AuctionExtended} and {@link AuctionClosed}).
     */
    protected synchronized void notifyReachedExpectedClosingTime() {
        auctionLife.notifyReachedExpectedClosingTime(getBids().isEmpty());
        if (isActive()) {
            AuctionDomainEvent.raise(new AuctionExtended(getAuctionId(), getExpectedClosingTime()));
        } else {
            if (getActualBid().isPresent()) {
                winningBid = getActualBid().get();
            }
            AuctionDomainEvent.raise(new AuctionClosed(this));
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override @Generated
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        final AbstractAuctionWithLife that = (AbstractAuctionWithLife) o;
        return auctionLife.equals(that.auctionLife);
    }

    /**
     * {@inheritDoc}
     */
    @Override @Generated
    public int hashCode() {
        return Objects.hash(super.hashCode(), auctionLife);
    }

    /**
     * {@inheritDoc}
     */
    @Override @Generated
    public String toString() {
        return super.toString() + "\n"
                + "\twinningBid=" + getWinningBid() + "\n"
                + "\tbidsHistory=" + bidsHistory.toString() + "\n"
                + "\tauctionLife=" + auctionLife;
    }

    @SuppressWarnings({"checkstyle:all", "PMD.AvoidFieldNameMatchingMethodName", "unchecked"})
    protected abstract static class Builder<
            T extends Builder<T, R>,
            R extends AbstractAuctionWithLife> extends AbstractOngoingAuction.Builder<T, R> {

        protected Calendar closingTime;
        protected Bid winningBid;
        protected AuctionLife auctionLife;
        protected String bidsHistoryId;
        protected List<Bid> bids;
        protected BidsHistory bidsHistory;

        public Builder() {
            super();
            bids = new ArrayList<>();
        }

        public T winningBid(final Bid winningBid) {
            this.winningBid = winningBid;
            return (T) this;
        }

        public T closingTime(final Calendar closingTime) {
            this.closingTime = CalendarUtils.copy(closingTime);
            return (T) this;
        }

        public T bidsHistoryId(final String bidsHistoryId) {
            this.bidsHistoryId = bidsHistoryId;
            return (T) this;
        }

        public T bids(final Collection<Bid> bids) {
            this.bids = new ArrayList<>(bids);
            return (T) this;
        }

        /**
         * {@inheritDoc}
         * @throws AuctionValidationException
         */
        @Override
        protected void validateAndPrepare() throws AuctionValidationException {
            if (!bids.isEmpty() && (bidsHistoryId == null || bidsHistoryId.isEmpty())) {
                throw new AuctionValidationException("Bids history id must be set since a list of bids is specified");
            }
            this.bidsHistory = new BidsHistory(bidsHistoryId, bids);

            this.auctionLife = AuctionLife.builder().creationTime(creationTime).expectedClosingTime(expectedClosingTime)
                    .closingTime(closingTime).duration(duration).extensionDuration(extensionDuration).build();
            this.creationTime = auctionLife.getCreationTime();
            this.duration = auctionLife.getDuration();
            this.expectedClosingTime = auctionLife.getExpectedClosingTime();
            this.closingTime = auctionLife.getClosingTime().orElse(null);

            if (winningBid != null && winningBid.getBidderUsername().equals(sellerUsername)) {
                throw new AuctionValidationException("The auction winner cannot be the seller himself");
            }
            if (winningBid != null && closingTime == null) {
                throw new AuctionValidationException("If the winner is present, the closing time must also be present");
            }
            if (winningBid != null && actualBid != null && !actualBid.equals(winningBid)) {
                throw new AuctionValidationException("The actual bid and the winning bid must be the same");
            }
            if (winningBid != null) {
                actualBid = winningBid;
            }
            if (actualBid != null && (bids.isEmpty() || !bids.contains(actualBid))) {
                throw new AuctionValidationException("The actual bid (or winning bid) is not present in bids list");
            }
            if (actualBid == null && !bids.isEmpty()) {
                throw new AuctionValidationException("There is at least one bid, but the actual bid is not set");
            }
            super.validateAndPrepare();
        }
    }
}
