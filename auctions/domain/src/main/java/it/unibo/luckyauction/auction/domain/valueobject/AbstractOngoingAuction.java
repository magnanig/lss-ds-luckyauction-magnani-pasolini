/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.domain.valueobject;

import it.unibo.luckyauction.auction.domain.exception.AuctionValidationException;
import it.unibo.luckyauction.util.CalendarUtils;
import lombok.Generated;

import java.math.BigDecimal;
import java.time.Duration;
import java.util.Calendar;
import java.util.Objects;
import java.util.Optional;

/**
 * A generic ongoing auction, i.e. an active auction that will eventually end.
 */
public abstract class AbstractOngoingAuction extends AbstractAuction {

    private Bid actualBid;

    /**
     * Configure auction with the specified parameters.
     * @param auctionId         the id of the auction
     * @param product           the product associated to the auction
     * @param sellerUsername    the username of the seller (i.e. who created the auction)
     * @param actualBid         the actual bid, if any (otherwise use <code>null</code>)
     * @param startingPrice     the starting price
     */
    protected AbstractOngoingAuction(final String auctionId, final Product product, final String sellerUsername,
                                     final Bid actualBid, final BigDecimal startingPrice) {
        super(auctionId, product, sellerUsername, startingPrice);
        this.actualBid = actualBid;
    }

    /**
     * Set actual bid with the specified one.
     * @param bid   the new bid
     */
    protected void setActualBid(final Bid bid) {
        if (bid == null || actualBid != null && bid.compareTo(actualBid) <= 0) {
            throw new AuctionValidationException("The new bid must have a greater amount than the previous one");
        }
        actualBid = bid;
    }

    /**
     * Get the actual bid, if present.
     */
    public Optional<Bid> getActualBid() {
        return Optional.ofNullable(actualBid);
    }

    /**
     * Get the remaining time.
     */
    public Duration remainingTime() {
        return CalendarUtils.diff(getExpectedClosingTime(), Calendar.getInstance());
    }

    /**
     * Get the optional extension amount, i.e. the amount of time for which
     * auction is extended if no bids are received when auction should close.
     */
    public abstract Optional<Duration> getExtensionAmount();

    /**
     * Get the duration of the auction.
     */
    public abstract Duration getDuration();

    /**
     * Get the expected closing time.
     */
    public abstract Calendar getExpectedClosingTime();

    /**
     * {@inheritDoc}
     */
    @Override @Generated
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        final AbstractOngoingAuction that = (AbstractOngoingAuction) o;
        return Objects.equals(actualBid, that.actualBid);
    }

    /**
     * {@inheritDoc}
     */
    @Override @Generated
    public int hashCode() {
        return Objects.hash(super.hashCode(), actualBid);
    }

    /**
     * {@inheritDoc}
     */
    @Override @Generated
    public String toString() {
        return super.toString() + "\n"
                + "\tactualBid=" + actualBid;
    }

    @SuppressWarnings({"checkstyle:all", "PMD.AvoidFieldNameMatchingMethodName", "unchecked"})
    protected abstract static class Builder<
            T extends Builder<T, R>,
            R extends AbstractOngoingAuction> extends AbstractAuction.Builder<T, R> {

        protected Bid actualBid;
        protected Duration duration;
        protected Duration extensionDuration;
        protected Calendar expectedClosingTime;

        public T actualBid(final Bid actualBid) {
            this.actualBid = actualBid;
            return (T) this;
        }

        public T extensionDuration(final Duration extensionDuration) {
            this.extensionDuration = extensionDuration;
            return (T) this;
        }

        public T duration(final Duration duration) {
            this.duration = duration;
            return (T) this;
        }

        public T expectedClosingTime(final Calendar expectedClosingTime) {
            this.expectedClosingTime = CalendarUtils.copy(expectedClosingTime);
            return (T) this;
        }

        /**
         * {@inheritDoc}
         * @throws AuctionValidationException
         */
        @Override
        protected void validateAndPrepare() throws AuctionValidationException {
            super.validateAndPrepare();
            if (duration == null && expectedClosingTime == null) {
                throw new AuctionValidationException("Both auction duration and expected closing time are not set. "
                        + "Please specify at least one of them");
            }
        }
    }
}
