/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.domain.valueobject;

import it.unibo.luckyauction.auction.domain.entity.Auction;
import it.unibo.luckyauction.auction.domain.exception.AuctionValidationException;
import lombok.Generated;

import java.math.BigDecimal;
import java.util.Calendar;

/**
 * A summary representing a lost auction. A user will have a
 * LostAuction in their auctions' history if they have lost
 * the corresponding auction, i.e. they raised bid(s) for it
 * but didn't manage to win (someone else raised a higher bid).
 */
public final class LostAuction extends SummaryEndedAuction {

    private LostAuction(final String auctionId, final Product product, final String sellerUsername,
                        final BigDecimal startingPrice, final Calendar creationTime, final Calendar closingTime,
                        final Bid winningBig) {
        super(auctionId, product, sellerUsername, startingPrice, creationTime, closingTime, winningBig);
    }

    /**
     * Get the winning bid.
     */
    @SuppressWarnings("PMD.UselessOverridingMethod") // useful just to override javadoc
    @Override
    public Bid getWinningBid() {
        return super.getWinningBid();
    }

    /**
     * Build the summary for a lost auction, starting from the given
     * complete auction.
     * @param auction   the full auction, to use to build the summary
     * @return          the lost auction summary
     */
    @Generated
    public static LostAuction fromAuction(final Auction auction) {
        return builder()
                .auctionId(auction.getAuctionId())
                .product(auction.getProduct())
                .startingPrice(auction.getStartingPrice())
                .sellerUsername(auction.getSellerUsername())
                .creationTime(auction.getCreationTime())
                .closingTime(auction.getClosingTime().orElse(null))
                .winningBid(auction.getWinningBid().orElse(null))
                .build();
    }

    public static LostAuctionBuilder builder() {
        return new LostAuctionBuilder();
    }

    public static class LostAuctionBuilder extends Builder<LostAuctionBuilder, LostAuction> {

        protected LostAuctionBuilder() {
            super();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        protected LostAuction createObject() {
            return new LostAuction(auctionId, product, sellerUsername, startingPrice, creationTime, closingTime,
                    winningBid);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        protected void validateAndPrepare() throws AuctionValidationException {
            if (winningBid == null) {
                throw new AuctionValidationException("Winning bid is not set");
            }
            super.validateAndPrepare();
        }
    }
}
