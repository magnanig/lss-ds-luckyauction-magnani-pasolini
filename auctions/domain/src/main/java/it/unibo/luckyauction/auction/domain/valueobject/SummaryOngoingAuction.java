/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.domain.valueobject;

import it.unibo.luckyauction.util.CalendarUtils;

import java.math.BigDecimal;
import java.time.Duration;
import java.util.Calendar;
import java.util.Optional;

/**
 * A summary for an ongoing auction, storing only essential information.
 */
public abstract class SummaryOngoingAuction extends AbstractOngoingAuction {

    private final Calendar creationTime;
    private final Duration extensionDuration;
    private final Calendar expectedClosingTime;

    /**
     * Configure auction with the specified parameters.
     * @param auctionId             the id of the auction
     * @param product               the product associated to the auction
     * @param sellerUsername        the username of the seller (i.e. who created the auction)
     * @param actualBid             the actual bid, if any (otherwise use <code>null</code>)
     * @param startingPrice         the starting price
     * @param creationTime          the creation time
     * @param duration              the duration
     * @param extensionDuration     the optional extension duration, i.e. the amount of time for which
                                    auction is extended if no bids are received when auction should close
     * @param expectedClosingTime   the expected closing time, either if auction has been extended or not
     */
    protected SummaryOngoingAuction(final String auctionId, final Product product, final String sellerUsername,
                                    final Bid actualBid, final BigDecimal startingPrice, final Calendar creationTime,
                                    final Duration duration, final Duration extensionDuration,
                                    final Calendar expectedClosingTime) {
        super(auctionId, product, sellerUsername, actualBid, startingPrice);
        this.creationTime = creationTime;
        this.extensionDuration = extensionDuration;
        this.expectedClosingTime = expectedClosingTime == null ? CalendarUtils.add(creationTime, duration)
                : expectedClosingTime;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Calendar getCreationTime() {
        return CalendarUtils.copy(creationTime);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<Duration> getExtensionAmount() {
        return Optional.ofNullable(extensionDuration);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Duration getDuration() {
        return CalendarUtils.diff(expectedClosingTime, creationTime);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Calendar getExpectedClosingTime() {
        return CalendarUtils.copy(expectedClosingTime);
    }
}
