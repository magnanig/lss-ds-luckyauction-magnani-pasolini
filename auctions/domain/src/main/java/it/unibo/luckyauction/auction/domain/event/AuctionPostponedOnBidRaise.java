/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.domain.event;

import it.unibo.luckyauction.util.CalendarUtils;

import java.util.Calendar;

/**
 * Represents an event to notify that an auction's expected closing time has been
 * postponed, due to a bid raise in ending zone.
 */
public class AuctionPostponedOnBidRaise implements AuctionEvent {

    private final String auctionId;
    private final Calendar newExpectedClosingTime;

    /**
     * Create a new instance to represent the auction's expected closing time postponement.
     * @param auctionId                 the auction id
     * @param newExpectedClosingTime    the new auction closing time
     */
    public AuctionPostponedOnBidRaise(final String auctionId, final Calendar newExpectedClosingTime) {
        this.auctionId = auctionId;
        this.newExpectedClosingTime = CalendarUtils.copy(newExpectedClosingTime);
    }

    /**
     * Get the id of the extended auction.
     */
    public String getAuctionId() {
        return auctionId;
    }

    /**
     * Get the new auction closing time.
     */
    public Calendar getNewExpectedClosingTime() {
        return CalendarUtils.copy(newExpectedClosingTime);
    }
}
