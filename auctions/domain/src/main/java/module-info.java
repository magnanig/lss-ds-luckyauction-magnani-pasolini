module lss.ds.luckyauction.magnani.pasolini.auctions.domain.main {
    exports it.unibo.luckyauction.auction.domain.valueobject;
    exports it.unibo.luckyauction.auction.domain.entity;
    exports it.unibo.luckyauction.auction.domain.service;
    exports it.unibo.luckyauction.auction.domain.exception;
    exports it.unibo.luckyauction.auction.domain.event;
    exports it.unibo.luckyauction.auction.domain.config;
    requires lss.ds.luckyauction.magnani.pasolini.utils.main;
    requires lss.ds.luckyauction.magnani.pasolini.configuration.main;
    requires lombok;
    requires org.apache.pdfbox;
}