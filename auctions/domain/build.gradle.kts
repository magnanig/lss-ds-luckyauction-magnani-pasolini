plugins {
    // this will add following dependencies: testFixtures --> main and test --> testFixtures
    id("java-test-fixtures")
}

dependencies {
    implementation(project(":configuration"))
    implementation(project(":utils"))
    implementation("org.apache.pdfbox:pdfbox:2.0.24")
}
