/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.adapter;

import it.unibo.luckyauction.util.CalendarUtils;
import it.unibo.luckyauction.auction.domain.valueobject.Bid;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Represents the schema of an {@link Bid}. This is a (de)serializable version
 * with all getters and setters, to be used to send object through the network.
 */
public final class BidWeb {
    private BigDecimal amount;
    private String bidderUsername;
    private Date timestamp;

    public BidWeb() {
        timestamp = new Date();
    }

    public static BidWeb fromDomain(final Bid bid) {
        final BidWeb bidWeb = new BidWeb();
        bidWeb.amount = bid.getAmount();
        bidWeb.bidderUsername = bid.getBidderUsername();
        bidWeb.timestamp = bid.getTimestamp().getTime();
        return bidWeb;
    }

    public Bid toDomainBid() {
        return new Bid(amount, bidderUsername, CalendarUtils.fromDate(timestamp));
    }

    public BidWeb copy() {
        final BidWeb bidWeb = new BidWeb();
        bidWeb.amount = amount;
        bidWeb.bidderUsername = bidderUsername;
        bidWeb.timestamp = CalendarUtils.copy(timestamp);
        return bidWeb;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(final BigDecimal amount) {
        this.amount = amount;
    }

    public String getBidderUsername() {
        return bidderUsername;
    }

    public void setBidderUsername(final String bidderUsername) {
        this.bidderUsername = bidderUsername;
    }

    public Date getTimestamp() {
        return CalendarUtils.copy(timestamp);
    }

    public void setTimestamp(final Date timestamp) {
        this.timestamp = CalendarUtils.copy(timestamp);
    }
}
