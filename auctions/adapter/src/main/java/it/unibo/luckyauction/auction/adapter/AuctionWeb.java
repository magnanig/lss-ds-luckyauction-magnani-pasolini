/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.adapter;

import it.unibo.luckyauction.auction.domain.entity.Auction;
import it.unibo.luckyauction.auction.domain.entity.BidsHistory;
import it.unibo.luckyauction.util.CalendarUtils;

import java.util.Calendar;
import java.util.Date;

import static it.unibo.luckyauction.util.NullableUtils.safeCall;

/**
 * Represents the schema of an {@link Auction}. This is a (de)serializable
 * version with all getters and setters, to be used to send object
 * through the network.
 */
public final class AuctionWeb extends AbstractOngoingAuctionWeb {

    private BidWeb winningBid;
    private Date closingTime;
    private BidsHistoryWeb bidsHistory;

    public AuctionWeb() {
        super();
    }

    AuctionWeb(final Auction auction) {
        super(auction);
        winningBid = safeCall(auction.getWinningBid(), BidWeb::fromDomain);
        closingTime = safeCall(auction.getClosingTime(), Calendar::getTime);
        bidsHistory = BidsHistoryWeb.fromDomain(new BidsHistory(auction.getBidsHistoryId(), auction.getBids()));
    }

    public static AuctionWeb fromDomain(final Auction auction) {
        return new AuctionWeb(auction);
    }

    @Override
    public Auction toDomainAuction() {
        return Auction.builder()
                .auctionId(getAuctionId())
                .product(getProduct().toDomainProduct())
                .startingPrice(getStartingPrice())
                .sellerUsername(getSellerUsername())
                .creationTime(CalendarUtils.fromDate(getCreationTime()))
                .closingTime(safeCall(closingTime, CalendarUtils::fromDate))
                .expectedClosingTime(safeCall(getExpectedClosingTime(), CalendarUtils::fromDate))
                .extensionDuration(safeCall(getExtensionAmount(), DurationWeb::toDuration))
                .actualBid(safeCall(getActualBid(), BidWeb::toDomainBid))
                .winningBid(safeCall(getWinningBid(), BidWeb::toDomainBid))
                .bidsHistoryId(bidsHistory.getBidsHistoryId())
                .bids(bidsHistory.toDomainBidsHistory().getBids())
                .build();
    }

    public BidWeb getWinningBid() {
        return safeCall(winningBid, BidWeb::copy);
    }

    public void setWinningBid(final BidWeb winningBid) {
        this.winningBid = safeCall(winningBid, BidWeb::copy);
    }

    public Date getClosingTime() {
        return safeCall(closingTime, CalendarUtils::copy);
    }

    public void setClosingTime(final Date closingTime) {
        this.closingTime = safeCall(closingTime, CalendarUtils::copy);
    }

    public BidsHistoryWeb getBidsHistory() {
        return bidsHistory.copy();
    }

    public void setBidsHistory(final BidsHistoryWeb bidsHistory) {
        this.bidsHistory = bidsHistory.copy();
    }

    @Override
    public String toString() {
        return "AuctionWeb{"
                + "bidsHistory=" + bidsHistory
                + "\n\t" + super.toString()
                + '}';
    }
}
