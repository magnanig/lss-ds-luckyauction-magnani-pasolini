/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.adapter;

import it.unibo.luckyauction.util.CalendarUtils;
import it.unibo.luckyauction.auction.domain.valueobject.AwardedAuction;

/**
 * Represents the schema of an {@link AwardedAuction}. This is a (de)serializable version
 * with all getters and setters, to be used to send object through the network.
 */
public final class AwardedAuctionWeb extends AbstractEndedAuctionWeb {

    private UserContactInformationWeb sellerContacts;
    private UserContactInformationWeb buyerContacts;

    public AwardedAuctionWeb() {
        super();
    }

    public AwardedAuctionWeb(final AwardedAuction awardedAuction) {
        super(awardedAuction);
        this.sellerContacts = UserContactInformationWeb.fromDomain(awardedAuction.getSellerContacts());
        this.buyerContacts = UserContactInformationWeb.fromDomain(awardedAuction.getBuyerContacts());
    }

    public static AwardedAuctionWeb fromDomain(final AwardedAuction awardedAuction) {
        return new AwardedAuctionWeb(awardedAuction);
    }

    @Override
    public AwardedAuction toDomainAuction() {
        return AwardedAuction.builder()
                .auctionId(getAuctionId())
                .creationTime(CalendarUtils.fromDate(getCreationTime()))
                .closingTime(CalendarUtils.fromDate(getClosingTime()))
                .product(getProduct().toDomainProduct())
                .startingPrice(getStartingPrice())
                .sellerUsername(getSellerUsername())
                .winningBid(getWinningBid().toDomainBid())
                .sellerContacts(sellerContacts.toDomain())
                .buyerContacts(buyerContacts.toDomain())
                .build();
    }

    public UserContactInformationWeb getSellerContacts() {
        return sellerContacts;
    }

    public void setSellerContacts(final UserContactInformationWeb sellerContacts) {
        this.sellerContacts = sellerContacts;
    }

    public UserContactInformationWeb getBuyerContacts() {
        return buyerContacts;
    }

    public void setBuyerContacts(final UserContactInformationWeb buyerContacts) {
        this.buyerContacts = buyerContacts;
    }
}
