/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.adapter;

import it.unibo.luckyauction.auction.domain.entity.AuctionsHistory;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Represents the schema of an {@link AuctionsHistory}. This is a (de)serializable
 * version with all getters and setters, to be used to send object
 * through the network.
 */
public final class AuctionsHistoryWeb {

    private String auctionsHistoryId;
    private String username;

    private List<AwardedAuctionWeb> awardedAuctions;
    private List<LostAuctionWeb> lostAuctions;
    private List<ParticipatingAuctionWeb> participatingAuctions;
    private List<CreatedOngoingAuctionWeb> createdOngoingAuctions;
    private List<CreatedEndedAuctionWeb> createdEndedAuctions;

    public static AuctionsHistoryWeb fromDomain(final AuctionsHistory auctionsHistory) {
        final AuctionsHistoryWeb auctionsHistoryWeb = new AuctionsHistoryWeb();
        auctionsHistoryWeb.auctionsHistoryId = auctionsHistory.getAuctionsHistoryId();
        auctionsHistoryWeb.awardedAuctions = auctionsHistory.getAwardedAuctions().stream()
                .map(AwardedAuctionWeb::fromDomain).collect(Collectors.toList());
        auctionsHistoryWeb.lostAuctions = auctionsHistory.getLostAuctions().stream()
                .map(LostAuctionWeb::fromDomain).collect(Collectors.toList());
        auctionsHistoryWeb.participatingAuctions = auctionsHistory.getParticipatingAuctions().stream()
                .map(ParticipatingAuctionWeb::fromDomain).collect(Collectors.toList());
        auctionsHistoryWeb.createdOngoingAuctions = auctionsHistory.getCreatedOngoingAuctions().stream()
                .map(CreatedOngoingAuctionWeb::fromDomain).collect(Collectors.toList());
        auctionsHistoryWeb.createdEndedAuctions = auctionsHistory.getCreatedEndedAuctions().stream()
                .map(CreatedEndedAuctionWeb::fromDomain).collect(Collectors.toList());
        auctionsHistoryWeb.username = auctionsHistory.getUsername();
        return auctionsHistoryWeb;
    }

    public AuctionsHistory toDomain() {
        return new AuctionsHistory(auctionsHistoryId, username,
                awardedAuctions.stream().map(AwardedAuctionWeb::toDomainAuction).toList(),
                lostAuctions.stream().map(LostAuctionWeb::toDomainAuction).toList(),
                participatingAuctions.stream().map(ParticipatingAuctionWeb::toDomainAuction).toList(),
                createdOngoingAuctions.stream().map(CreatedOngoingAuctionWeb::toDomainAuction).toList(),
                createdEndedAuctions.stream().map(CreatedEndedAuctionWeb::toDomainAuction).toList()
        );
    }

    public String getAuctionsHistoryId() {
        return auctionsHistoryId;
    }

    public void setAuctionsHistoryId(final String auctionsHistoryId) {
        this.auctionsHistoryId = auctionsHistoryId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(final String username) {
        this.username = username;
    }

    public List<AwardedAuctionWeb> getAwardedAuctions() {
        return new ArrayList<>(awardedAuctions);
    }

    public void setAwardedAuctions(final List<AwardedAuctionWeb> awardedAuctions) {
        this.awardedAuctions = new ArrayList<>(awardedAuctions);
    }

    public List<LostAuctionWeb> getLostAuctions() {
        return new ArrayList<>(lostAuctions);
    }

    public void setLostAuctions(final List<LostAuctionWeb> lostAuctions) {
        this.lostAuctions = new ArrayList<>(lostAuctions);
    }

    public List<ParticipatingAuctionWeb> getParticipatingAuctions() {
        return new ArrayList<>(participatingAuctions);
    }

    public void setParticipatingAuctions(final List<ParticipatingAuctionWeb> participatingAuctions) {
        this.participatingAuctions = new ArrayList<>(participatingAuctions);
    }

    public List<CreatedOngoingAuctionWeb> getCreatedOngoingAuctions() {
        return new ArrayList<>(createdOngoingAuctions);
    }

    public void setCreatedOngoingAuctions(final List<CreatedOngoingAuctionWeb> createdOngoingAuctions) {
        this.createdOngoingAuctions = new ArrayList<>(createdOngoingAuctions);
    }

    public List<CreatedEndedAuctionWeb> getCreatedEndedAuctions() {
        return new ArrayList<>(createdEndedAuctions);
    }

    public void setCreatedEndedAuctions(final List<CreatedEndedAuctionWeb> createdEndedAuctions) {
        this.createdEndedAuctions = new ArrayList<>(createdEndedAuctions);
    }
}
