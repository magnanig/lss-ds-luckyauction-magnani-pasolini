/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.adapter;

import it.unibo.luckyauction.auction.domain.entity.BidsHistory;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Represents the schema of an {@link BidsHistory}. This is a (de)serializable version
 * with all getters and setters, to be used to send object through the network.
 */
public final class BidsHistoryWeb {

    private String bidsHistoryId;
    private List<BidWeb> bids;

    public static BidsHistoryWeb fromDomain(final BidsHistory bidsHistory) {
        final BidsHistoryWeb bidsHistoryWeb = new BidsHistoryWeb();
        bidsHistoryWeb.bidsHistoryId = bidsHistory.getBidsHistoryId();
        bidsHistoryWeb.bids = bidsHistory.getBids().stream().map(BidWeb::fromDomain).collect(Collectors.toList());
        return bidsHistoryWeb;
    }

    public BidsHistory toDomainBidsHistory() {
        final BidsHistory bidsHistory = new BidsHistory(bidsHistoryId);
        bidsHistory.addAllBids(bids.stream().map(BidWeb::toDomainBid).collect(Collectors.toList()));
        return bidsHistory;
    }

    public BidsHistoryWeb copy() {
        final BidsHistoryWeb bidsHistoryWeb = new BidsHistoryWeb();
        bidsHistoryWeb.bidsHistoryId = bidsHistoryId;
        bidsHistoryWeb.bids = new ArrayList<>(bids);
        return bidsHistoryWeb;
    }

    public String getBidsHistoryId() {
        return bidsHistoryId;
    }

    public void setBidsHistoryId(final String bidsHistoryId) {
        this.bidsHistoryId = bidsHistoryId;
    }

    public List<BidWeb> getBids() {
        return new ArrayList<>(bids);
    }

    public void setBids(final List<BidWeb> bids) {
        this.bids = new ArrayList<>(bids);
    }
}
