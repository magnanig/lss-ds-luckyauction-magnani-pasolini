/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.adapter.socket;

import it.unibo.luckyauction.util.CalendarUtils;

import java.util.Calendar;
import java.util.Date;

public class AuctionExpectedClosingTimeChanged implements AuctionEvent {

    private final String auctionId;
    private final Date newExpectedClosingTime;

    public AuctionExpectedClosingTimeChanged(final String auctionId, final Calendar newExpectedClosingTime) {
        this.auctionId = auctionId;
        this.newExpectedClosingTime = newExpectedClosingTime.getTime();
    }

    @Override
    public EventType eventType() {
        return EventType.EXPECTED_CLOSING_TIME_CHANGED;
    }

    @Override
    public Object[] extractArgs() {
        return new Object[] { auctionId, getNewExpectedClosingTime() };
    }

    @Override
    public String getAuctionId() {
        return auctionId;
    }

    /**
     * Get the new expected closing time for the auction.
     */
    public Date getNewExpectedClosingTime() {
        return CalendarUtils.copy(newExpectedClosingTime);
    }
}
