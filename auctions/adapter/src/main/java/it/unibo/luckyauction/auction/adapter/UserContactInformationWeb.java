/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.adapter;

import it.unibo.luckyauction.auction.domain.valueobject.UserContactInformation;

public final class UserContactInformationWeb {
    private String email;
    private String cellularNumber;
    private String telephoneNumber;

    public static UserContactInformationWeb fromDomain(final UserContactInformation userContactInformation) {
        final UserContactInformationWeb userContactInformationWeb = new UserContactInformationWeb();
        userContactInformationWeb.setEmail(userContactInformation.email());
        userContactInformationWeb.setCellularNumber(userContactInformation.cellularNumber());
        userContactInformationWeb.setTelephoneNumber(userContactInformation.telephoneNumber());
        return userContactInformationWeb;
    }

    public UserContactInformation toDomain() {
        return new UserContactInformation(email, cellularNumber, telephoneNumber);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public String getCellularNumber() {
        return cellularNumber;
    }

    public void setCellularNumber(final String cellularNumber) {
        this.cellularNumber = cellularNumber;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(final String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }
}
