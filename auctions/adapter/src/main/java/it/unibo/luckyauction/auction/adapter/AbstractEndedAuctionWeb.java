/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.adapter;

import it.unibo.luckyauction.auction.domain.valueobject.SummaryEndedAuction;
import it.unibo.luckyauction.util.CalendarUtils;

import java.util.Date;

import static it.unibo.luckyauction.util.NullableUtils.safeCall;

/**
 * Represents the schema of an {@link AbstractEndedAuctionWeb}. This is a (de)serializable
 * version with all getters and setters, to be used to send object
 * through the network.
 */
@SuppressWarnings("checkstyle:DesignForExtension")
public abstract class AbstractEndedAuctionWeb extends AbstractAuctionWeb {

    private BidWeb winningBid;
    private Date closingTime;

    protected AbstractEndedAuctionWeb() {
        super();
    }

    protected AbstractEndedAuctionWeb(final SummaryEndedAuction summaryEndedAuction) {
        super(summaryEndedAuction);
        winningBid = safeCall(summaryEndedAuction.getWinningBid(), BidWeb::fromDomain);
        closingTime = summaryEndedAuction.getClosingTime().getTime();
    }

    @Override
    public abstract SummaryEndedAuction toDomainAuction();

    public BidWeb getWinningBid() {
        return safeCall(winningBid, BidWeb::copy);
    }

    public void setWinningBid(final BidWeb winningBid) {
        this.winningBid = safeCall(winningBid, BidWeb::copy);
    }

    public Date getClosingTime() {
        return CalendarUtils.copy(closingTime);
    }

    public void setClosingTime(final Date closingTime) {
        this.closingTime = CalendarUtils.copy(closingTime);
    }
}
