/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.adapter;

import it.unibo.luckyauction.util.CalendarUtils;
import it.unibo.luckyauction.auction.domain.valueobject.CreatedOngoingAuction;

import static it.unibo.luckyauction.util.NullableUtils.safeCall;

/**
 * Represents the schema of an {@link CreatedOngoingAuction}. This is a (de)serializable version
 * with all getters and setters, to be used to send object through the network.
 */
public final class CreatedOngoingAuctionWeb extends AbstractOngoingAuctionWeb {

    public CreatedOngoingAuctionWeb() {
        super();
    }

    CreatedOngoingAuctionWeb(final CreatedOngoingAuction createdOngoingAuction) {
        super(createdOngoingAuction);
    }

    public static CreatedOngoingAuctionWeb fromDomain(final CreatedOngoingAuction createdOngoingAuction) {
        return new CreatedOngoingAuctionWeb(createdOngoingAuction);
    }

    @Override
    public CreatedOngoingAuction toDomainAuction() {
        return CreatedOngoingAuction.builder()
                .auctionId(getAuctionId())
                .product(getProduct().toDomainProduct())
                .startingPrice(getStartingPrice())
                .actualBid(safeCall(getActualBid(), BidWeb::toDomainBid))
                .sellerUsername(getSellerUsername())
                .creationTime(CalendarUtils.fromDate(getCreationTime()))
                .expectedClosingTime(safeCall(getExpectedClosingTime(), CalendarUtils::fromDate))
                .startingPrice(getStartingPrice())
                .extensionDuration(safeCall(getExtensionAmount(), DurationWeb::toDuration))
                .build();
    }
}
