/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.adapter;

import java.time.Duration;

/**
 * Represents the schema of a {@link Duration}. This is a (de)serializable
 * version with all getters and setters, to be used to send object
 * through the network.
 */
public final class DurationWeb {

    private static final Integer SECONDS_IN_MINUTE = 60;
    private static final Integer MINUTES_IN_HOUR = 60;
    private static final Integer HOURS_IN_DAY = 24;

    private Integer millis;
    private Integer seconds;
    private Integer minutes;
    private Integer hours;
    private Integer days;

    public DurationWeb() {
        millis = 0;
        seconds = 0;
        minutes = 0;
        hours = 0;
        days = 0;
    }

    public static DurationWeb fromDuration(final Duration duration) {
        final DurationWeb durationWeb = new DurationWeb();
        durationWeb.millis = duration.toMillisPart();
        durationWeb.seconds = duration.toSecondsPart();
        durationWeb.minutes = duration.toMinutesPart();
        durationWeb.hours = duration.toHoursPart();
        durationWeb.days = Math.toIntExact(duration.toDaysPart());
        return durationWeb;
    }

    public Duration toDuration() {
        return Duration.ofMillis(millis
                + seconds * 1000L
                + minutes * SECONDS_IN_MINUTE * 1000L
                + hours * MINUTES_IN_HOUR * SECONDS_IN_MINUTE * 1000L
                + days * HOURS_IN_DAY * MINUTES_IN_HOUR * SECONDS_IN_MINUTE * 1000L
        );
    }

    public DurationWeb copy() {
        final DurationWeb durationWeb = new DurationWeb();
        durationWeb.millis = millis;
        durationWeb.seconds = seconds;
        durationWeb.minutes = minutes;
        durationWeb.hours = hours;
        durationWeb.days = days;
        return durationWeb;
    }

    public Integer getMillis() {
        return millis;
    }

    public void setMillis(final Integer millis) {
        this.millis = millis;
    }

    public Integer getSeconds() {
        return seconds;
    }

    public void setSeconds(final Integer seconds) {
        this.seconds = seconds;
    }

    public Integer getMinutes() {
        return minutes;
    }

    public void setMinutes(final Integer minutes) {
        this.minutes = minutes;
    }

    public Integer getHours() {
        return hours;
    }

    public void setHours(final Integer hours) {
        this.hours = hours;
    }

    public Integer getDays() {
        return days;
    }

    public void setDays(final Integer days) {
        this.days = days;
    }
}
