/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.adapter.api;

import it.unibo.luckyauction.auction.adapter.AuctionsHistoryWeb;

import java.util.List;
import java.util.Optional;

public interface AuctionsHistoryApi {

    /**
     * Create a new auctions' history.
     * @param auctionsHistoryId     the id of the new auctions' history
     * @param username              the username associated to the auctions' history
     */
    void createAuctionsHistory(String auctionsHistoryId, String username);

    /**
     * Find the auctions' history by the associated user.
     * @param username  the desired username
     * @return          the auctions' history of the specified user
     */
    Optional<AuctionsHistoryWeb> findByUsername(String username);

    /**
     * Remove the auctions' history of the specified user.
     * @param username  the username of the user whose auctions' history
     *                  has to be removed
     * @return          the id of the just deleted auctions' history, if any
     */
    Optional<String> deleteByUsername(String username);

    /**
     * Add an awarded auction for the specified user.
     * @param auctionId     the reference to the auction to add
     * @param username      the username of the desired user
     */
    void addAwardedAuction(String auctionId, String username);

    /**
     * Add a created ended auction for the specified user.
     * @param auctionId     the reference to the auction to add
     * @param username      the username of the desired user
     */
    void addCreatedEndedAuction(String auctionId, String username);

    /**
     * Add a created ongoing auction for the specified user.
     * @param auctionId     the reference to the auction to add
     * @param username      the username of the desired user
     */
    void addCreatedOngoingAuction(String auctionId, String username);

    /**
     * Add a lost auction for the specified user.
     * @param auctionId     the reference to the auction to add
     * @param username      the username of the desired user
     */
    void addLostAuction(String auctionId, String username);

    /**
     * Add a participating auction for the specified user.
     * @param auctionId     the reference to the auction to add
     * @param username      the username of the desired user
     */
    void addParticipatingAuction(String auctionId, String username);

    /**
     * Get the list of the user participating in the specified auction.
     * @param auctionId     the desired auction id
     * @return              the list of the username of the user participating
     *                      in the specified auction
     */
    List<String> getUsersParticipatingInAuction(String auctionId);

    /**
     * Remove auction from the list of the participating auction for
     * the specified user.
     * @param auctionId     the reference to the auction to remove
     * @param username      the desired username
     */
    void removeParticipatingAuction(String auctionId, String username);

    /**
     * Remove auction from the list of the created ongoing auction for
     * the specified user.
     * @param auctionId     the reference to the auction to remove
     * @param username      the desired username
     */
    void removeCreatedOngoingAuction(String auctionId, String username);

    /**
     * Remove the auction from all auction histories of all users.
     * @param auctionId the id of the desired auction
     */
    void removeAuctionFromAllHistory(String auctionId);

}
