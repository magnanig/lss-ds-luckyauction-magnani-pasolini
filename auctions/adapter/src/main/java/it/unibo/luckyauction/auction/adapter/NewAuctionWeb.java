/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.adapter;

import it.unibo.luckyauction.auction.domain.entity.Auction;
import it.unibo.luckyauction.util.CalendarUtils;

import java.math.BigDecimal;
import java.time.Duration;
import java.util.Date;

import static it.unibo.luckyauction.util.NullableUtils.safeCall;

/**
 * Represents the scheme for creating a new auction. It is suitable for allow the user to enter fields
 * more easily when creating a new auction from the browser. This is a (de)serializable version
 * with all getters and setters, to be used to send object through the network.
 */
public final class NewAuctionWeb {

    private ProductWeb product;
    private DurationWeb duration;
    private Date expectedClosingTime;
    private Date expectedExtensionClosingTime;
    private DurationWeb extensionAmount;
    private BigDecimal startingPrice;

    public Auction toDomainAuction(final String username) {
        if (expectedExtensionClosingTime != null && expectedClosingTime != null) {
            extensionAmount = DurationWeb.fromDuration(
                    Duration.ofMillis(expectedExtensionClosingTime.getTime() - expectedClosingTime.getTime()));
        }
        return Auction.builder()
                .product(product.toDomainProduct())
                .sellerUsername(username)
                .duration(safeCall(duration, DurationWeb::toDuration))
                .expectedClosingTime(safeCall(expectedClosingTime, CalendarUtils::fromDate))
                .extensionDuration(safeCall(extensionAmount, DurationWeb::toDuration))
                .startingPrice(startingPrice)
                .build();
    }

    public ProductWeb getProduct() {
        return product.copy();
    }

    public void setProduct(final ProductWeb product) {
        this.product = product.copy();
    }

    public DurationWeb getDuration() {
        return safeCall(duration, DurationWeb::copy);
    }

    public void setDuration(final DurationWeb duration) {
        this.duration = safeCall(duration, DurationWeb::copy);
    }

    public Date getExpectedClosingTime() {
        return safeCall(expectedClosingTime, CalendarUtils::copy);
    }

    public void setExpectedClosingTime(final Date expectedClosingTime) {
        this.expectedClosingTime = safeCall(expectedClosingTime, CalendarUtils::copy);
    }

    public DurationWeb getExtensionAmount() {
        return safeCall(extensionAmount, DurationWeb::copy);
    }

    public void setExtensionAmount(final DurationWeb extensionAmount) {
        this.extensionAmount = safeCall(extensionAmount, DurationWeb::copy);
    }

    public Date getExpectedExtensionClosingTime() {
        return safeCall(expectedExtensionClosingTime, CalendarUtils::copy);
    }

    public void setExpectedExtensionClosingTime(final Date expectedExtensionClosingTime) {
        this.expectedExtensionClosingTime = safeCall(expectedExtensionClosingTime, CalendarUtils::copy);
    }

    public BigDecimal getStartingPrice() {
        return startingPrice;
    }

    public void setStartingPrice(final BigDecimal startingPrice) {
        this.startingPrice = startingPrice;
    }
}
