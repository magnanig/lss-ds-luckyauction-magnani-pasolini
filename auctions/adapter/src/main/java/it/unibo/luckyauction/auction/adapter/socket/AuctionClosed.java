/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.adapter.socket;

import it.unibo.luckyauction.util.CalendarUtils;

import java.util.Calendar;
import java.util.Date;

public class AuctionClosed implements AuctionEvent {

    private final String auctionId;
    private final Date closingTime;

    public AuctionClosed(final String auctionId, final Calendar closingTime) {
        this.auctionId = auctionId;
        this.closingTime = closingTime.getTime();
    }

    @Override
    public EventType eventType() {
        return EventType.AUCTION_CLOSED;
    }

    @Override
    public Object[] extractArgs() {
        return new Object[] { auctionId, getClosingTime() };
    }

    @Override
    public String getAuctionId() {
        return auctionId;
    }

    /**
     * Get the closing time of the just closed auction.
     */
    public Date getClosingTime() {
        return CalendarUtils.copy(closingTime);
    }
}
