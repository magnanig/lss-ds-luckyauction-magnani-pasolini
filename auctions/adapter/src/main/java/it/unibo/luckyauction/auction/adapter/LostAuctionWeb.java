/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.adapter;

import it.unibo.luckyauction.util.CalendarUtils;
import it.unibo.luckyauction.auction.domain.valueobject.LostAuction;

/**
 * Represents the schema of an {@link LostAuction}. This is a (de)serializable version
 * with all getters and setters, to be used to send object through the network.
 */
public final class LostAuctionWeb extends AbstractEndedAuctionWeb {

    public LostAuctionWeb() {
        super();
    }

    LostAuctionWeb(final LostAuction lostAuction) {
        super(lostAuction);
    }

    public static LostAuctionWeb fromDomain(final LostAuction lostAuction) {
        return new LostAuctionWeb(lostAuction);
    }

    @Override
    public LostAuction toDomainAuction() {
        return LostAuction.builder()
                .auctionId(getAuctionId())
                .product(getProduct().toDomainProduct())
                .startingPrice(getStartingPrice())
                .sellerUsername(getSellerUsername())
                .creationTime(CalendarUtils.fromDate(getCreationTime()))
                .closingTime(CalendarUtils.fromDate(getClosingTime()))
                .winningBid(getWinningBid().toDomainBid())
                .build();
    }
}
