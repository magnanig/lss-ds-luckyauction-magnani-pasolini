/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.adapter;

import it.unibo.luckyauction.auction.domain.valueobject.Product;
import it.unibo.luckyauction.auction.domain.valueobject.ProductCategory;
import it.unibo.luckyauction.auction.domain.valueobject.ProductState;

/**
 * Represents the schema of a {@link Product}.
 * This is a (de)serializable version with all getters and setters,
 * to be used to send object through the network.
 */
public final class ProductWeb {

    private String name;
    private String description;
    private ProductCategory category;
    private ProductState state;
    private String image;

    public static ProductWeb fromDomainProduct(final Product product) {
        final ProductWeb productWeb = new ProductWeb();
        productWeb.name = product.name();
        productWeb.description = product.description();
        productWeb.category = product.category();
        productWeb.state = product.state();
        productWeb.image = product.image();
        return productWeb;
    }

    public Product toDomainProduct() {
        return Product.builder()
                .name(name)
                .description(description)
                .category(category)
                .state(state)
                .image(image)
                .build();
    }

    public ProductWeb copy() {
        final ProductWeb productWeb = new ProductWeb();
        productWeb.name = name;
        productWeb.description = description;
        productWeb.category = category;
        productWeb.state = state;
        productWeb.image = image;
        return productWeb;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public ProductCategory getCategory() {
        return category;
    }

    public void setCategory(final ProductCategory productCategory) {
        this.category = productCategory;
    }

    public ProductState getState() {
        return state;
    }

    public void setState(final ProductState state) {
        this.state = state;
    }

    public String getImage() {
        return image;
    }

    public void setImage(final String image) {
        this.image = image;
    }
}
