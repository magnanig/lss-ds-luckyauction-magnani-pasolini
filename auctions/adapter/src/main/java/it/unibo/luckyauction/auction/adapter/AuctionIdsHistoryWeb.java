/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.adapter;

import it.unibo.luckyauction.auction.domain.entity.AuctionsHistory;
import it.unibo.luckyauction.auction.domain.valueobject.AbstractAuction;
import it.unibo.luckyauction.auction.domain.valueobject.AwardedAuction;
import it.unibo.luckyauction.auction.domain.valueobject.CreatedEndedAuction;
import it.unibo.luckyauction.auction.domain.valueobject.UserContactInformation;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Represents the schema of an {@link AuctionsHistory}, but instead of containing lists of auction objects,
 * it contains lists of auction ids. This is a (de)serializable version with all getters and setters,
 * to be used to send object through the network.
 */
public final class AuctionIdsHistoryWeb {

    private String auctionsHistoryId;
    private String username;

    private List<AuctionReference> awardedAuctions;
    private List<AuctionReference> lostAuctions;
    private List<AuctionReference> participatingAuctions;
    private List<AuctionReference> createdOngoingAuctions;
    private List<AuctionReference> createdEndedAuctions;

    public static AuctionIdsHistoryWeb fromDomain(final AuctionsHistory auctionsHistory) {
        final AuctionIdsHistoryWeb auctionIdsHistoryWeb = new AuctionIdsHistoryWeb();
        auctionIdsHistoryWeb.auctionsHistoryId = auctionsHistory.getAuctionsHistoryId();
        auctionIdsHistoryWeb.username = auctionsHistory.getUsername();
        auctionIdsHistoryWeb.awardedAuctions = auctionsToAuctionReferences(auctionsHistory.getAwardedAuctions());
        auctionIdsHistoryWeb.lostAuctions = auctionsToAuctionReferences(auctionsHistory.getLostAuctions());
        auctionIdsHistoryWeb.participatingAuctions = auctionsToAuctionReferences(auctionsHistory.getParticipatingAuctions());
        auctionIdsHistoryWeb.createdOngoingAuctions = auctionsToAuctionReferences(auctionsHistory.getCreatedOngoingAuctions());
        auctionIdsHistoryWeb.createdEndedAuctions = auctionsToAuctionReferences(auctionsHistory.getCreatedEndedAuctions());
        return auctionIdsHistoryWeb;
    }

    public String getAuctionsHistoryId() {
        return auctionsHistoryId;
    }

    public void setAuctionsHistoryId(final String auctionsHistoryId) {
        this.auctionsHistoryId = auctionsHistoryId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(final String username) {
        this.username = username;
    }

    public List<AuctionReference> getAwardedAuctions() {
        return Collections.unmodifiableList(awardedAuctions);
    }

    public void setAwardedAuctions(final List<AuctionReference> awardedAuctions) {
        this.awardedAuctions = awardedAuctions;
    }

    public List<AuctionReference> getLostAuctions() {
        return Collections.unmodifiableList(lostAuctions);
    }

    public void setLostAuctions(final List<AuctionReference> lostAuctions) {
        this.lostAuctions = lostAuctions;
    }

    public List<AuctionReference> getParticipatingAuctions() {
        return Collections.unmodifiableList(participatingAuctions);
    }

    public void setParticipatingAuctions(final List<AuctionReference> participatingAuctions) {
        this.participatingAuctions = participatingAuctions;
    }

    public List<AuctionReference> getCreatedOngoingAuctions() {
        return Collections.unmodifiableList(createdOngoingAuctions);
    }

    public void setCreatedOngoingAuctions(final List<AuctionReference> createdOngoingAuctions) {
        this.createdOngoingAuctions = createdOngoingAuctions;
    }

    public List<AuctionReference> getCreatedEndedAuctions() {
        return Collections.unmodifiableList(createdEndedAuctions);
    }

    public void setCreatedEndedAuctions(final List<AuctionReference> createdEndedAuctions) {
        this.createdEndedAuctions = createdEndedAuctions;
    }

    private static <T extends AbstractAuction> List<AuctionReference> auctionsToAuctionReferences(final List<T> auctions) {
        return auctions.stream()
                .map(auction -> {
                    if (auction instanceof AwardedAuction awardedAuction) {
                        return new AuctionReference(awardedAuction.getAuctionId(), awardedAuction.getSellerContacts(),
                                awardedAuction.getBuyerContacts());
                    }
                    if (auction instanceof CreatedEndedAuction createdEndedAuction) {
                        return new AuctionReference(createdEndedAuction.getAuctionId(),
                                createdEndedAuction.getSellerContacts().orElse(null),
                                createdEndedAuction.getBuyerContacts().orElse(null));
                    }
                    return new AuctionReference(auction.getAuctionId());
                })
                .collect(Collectors.toList());
    }

    public static final class AuctionReference {
        private String auctionId;
        private UserContactInformationWeb sellerContacts;
        private UserContactInformationWeb buyerContacts;

        public AuctionReference() { }

        public AuctionReference(final String auctionId) {
            this.auctionId = auctionId;
        }

        public AuctionReference(final String auctionId, final UserContactInformation sellerContacts,
                                final UserContactInformation buyerContacts) {
            this.auctionId = auctionId;
            this.sellerContacts = UserContactInformationWeb.fromDomain(sellerContacts);
            this.buyerContacts = UserContactInformationWeb.fromDomain(buyerContacts);
        }

        public String getAuctionId() {
            return auctionId;
        }

        public void setAuctionId(final String auctionId) {
            this.auctionId = auctionId;
        }

        public UserContactInformationWeb getSellerContacts() {
            return sellerContacts;
        }

        public void setSellerContacts(final UserContactInformationWeb sellerContacts) {
            this.sellerContacts = sellerContacts;
        }

        public UserContactInformationWeb getBuyerContacts() {
            return buyerContacts;
        }

        public void setBuyerContacts(final UserContactInformationWeb buyerContacts) {
            this.buyerContacts = buyerContacts;
        }
    }
}
