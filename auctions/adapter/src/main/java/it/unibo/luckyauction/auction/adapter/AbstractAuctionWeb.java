/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.adapter;

import it.unibo.luckyauction.auction.domain.valueobject.AbstractAuction;
import it.unibo.luckyauction.util.CalendarUtils;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

/**
 * Represents the schema of an {@link AbstractAuction}. This is a (de)serializable
 * version with all getters and setters, to be used to send object
 * through the network.
 */
@SuppressWarnings("checkstyle:DesignForExtension")
public abstract class AbstractAuctionWeb {

    private String auctionId; // it is a foreign key, not a primary key
    private ProductWeb product;
    private BigDecimal startingPrice;
    private Date creationTime;
    private String sellerUsername;

    protected AbstractAuctionWeb() {
        creationTime = Calendar.getInstance().getTime();
    }

    protected AbstractAuctionWeb(final AbstractAuction abstractAuction) {
        this.auctionId = abstractAuction.getAuctionId();
        this.product = ProductWeb.fromDomainProduct(abstractAuction.getProduct());
        this.startingPrice = abstractAuction.getStartingPrice();
        this.creationTime = abstractAuction.getCreationTime().getTime();
        this.sellerUsername = abstractAuction.getSellerUsername();
    }

    public abstract AbstractAuction toDomainAuction();

    public String getAuctionId() {
        return auctionId;
    }

    public void setAuctionId(final String auctionId) {
        this.auctionId = auctionId;
    }

    public ProductWeb getProduct() {
        return product.copy();
    }

    public void setProduct(final ProductWeb product) {
        this.product = product.copy();
    }

    public BigDecimal getStartingPrice() {
        return startingPrice;
    }

    public void setStartingPrice(final BigDecimal startingPrice) {
        this.startingPrice = startingPrice;
    }

    public Date getCreationTime() {
        return CalendarUtils.copy(creationTime);
    }

    public void setCreationTime(final Date creationTime) {
        this.creationTime = CalendarUtils.copy(creationTime);
    }

    public String getSellerUsername() {
        return sellerUsername;
    }

    public void setSellerUsername(final String sellerUsername) {
        this.sellerUsername = sellerUsername;
    }

}
