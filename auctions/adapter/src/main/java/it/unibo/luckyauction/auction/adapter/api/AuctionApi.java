/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.adapter.api;

import it.unibo.luckyauction.auction.adapter.AuctionWeb;
import it.unibo.luckyauction.auction.adapter.BidWeb;
import it.unibo.luckyauction.auction.usecase.exception.AuctionNotFoundException;

import java.math.BigDecimal;
import java.util.Optional;

public interface AuctionApi {

    /**
     * Find an auction by its id.
     * @param auctionId the desired auction id
     * @return          the corresponding auction, if present
     */
    Optional<AuctionWeb> findById(String auctionId);

    /**
     * Get the actual bid for the specified auction.
     * @param auctionId the auction id
     * @return          the actual bid
     * @throws AuctionNotFoundException if no auction with the specified id exists
     */
    Optional<BidWeb> getActualBid(String auctionId);

    /**
     * Get the starting price of the specified auction.
     * @param auctionId the auction id
     * @return          the starting price
     * @throws AuctionNotFoundException if no auction with the specified id exists
     */
    BigDecimal getStartingPrice(String auctionId);

    /**
     * Add a new bid for the specified auction.
     * @param auctionId     the id of the auction to which add the bid
     * @param username      the username of the user raising the bid
     * @param bidAmount     the amount of the bid
     * @return              the just added bid
     */
    BidWeb addNewBid(String auctionId, String username, BigDecimal bidAmount);
}
