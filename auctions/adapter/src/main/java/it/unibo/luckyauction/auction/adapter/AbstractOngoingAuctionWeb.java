/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.adapter;

import it.unibo.luckyauction.util.CalendarUtils;
import it.unibo.luckyauction.auction.domain.valueobject.AbstractOngoingAuction;

import java.util.Calendar;
import java.util.Date;

import static it.unibo.luckyauction.util.NullableUtils.safeCall;

/**
 * Represents the schema of an {@link AbstractOngoingAuction}. This is a (de)serializable
 * version with all getters and setters, to be used to send object
 * through the network.
 */
@SuppressWarnings("checkstyle:DesignForExtension")
public abstract class AbstractOngoingAuctionWeb extends AbstractAuctionWeb {

    private DurationWeb extensionAmount;
    private Date expectedClosingTime;
    private BidWeb actualBid;

    protected AbstractOngoingAuctionWeb() {
        super();
    }

    protected AbstractOngoingAuctionWeb(final AbstractOngoingAuction abstractOngoingAuction) {
        super(abstractOngoingAuction);
        extensionAmount = safeCall(abstractOngoingAuction.getExtensionAmount(), DurationWeb::fromDuration);
        expectedClosingTime = safeCall(abstractOngoingAuction.getExpectedClosingTime(), Calendar::getTime);
        actualBid = safeCall(abstractOngoingAuction.getActualBid(), BidWeb::fromDomain);
    }

    @Override
    public abstract AbstractOngoingAuction toDomainAuction();

    public DurationWeb getExtensionAmount() {
        return safeCall(extensionAmount, DurationWeb::copy);
    }

    public void setExtensionAmount(final DurationWeb extensionAmount) {
        this.extensionAmount = safeCall(extensionAmount, DurationWeb::copy);
    }

    public Date getExpectedClosingTime() {
        return CalendarUtils.copy(expectedClosingTime);
    }

    public void setExpectedClosingTime(final Date expectedClosingTime) {
        this.expectedClosingTime = CalendarUtils.copy(expectedClosingTime);
    }

    public BidWeb getActualBid() {
        return safeCall(actualBid, BidWeb::copy);
    }

    public void setActualBid(final BidWeb actualBid) {
        this.actualBid = safeCall(actualBid, BidWeb::copy);
    }

}
