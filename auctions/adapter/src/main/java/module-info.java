module lss.ds.luckyauction.magnani.pasolini.auctions.adapter.main {
    exports it.unibo.luckyauction.auction.adapter;
    exports it.unibo.luckyauction.auction.adapter.api;
    exports it.unibo.luckyauction.auction.adapter.socket;
    requires lss.ds.luckyauction.magnani.pasolini.utils.main;
    requires lss.ds.luckyauction.magnani.pasolini.auctions.domain.main;
    requires lss.ds.luckyauction.magnani.pasolini.auctions.usecase.main;
}