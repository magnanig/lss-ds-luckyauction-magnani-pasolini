rootProject.extra["moduleName"] = "lss.ds.luckyauction.magnani.pasolini.auction.adapter.main"

dependencies {
    implementation(project(":auctions:domain"))
    implementation(project(":auctions:usecase"))
    implementation(project(":utils"))
}