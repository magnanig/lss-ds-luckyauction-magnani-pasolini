dependencies {
    implementation(project(":utils"))
    implementation(project(":mongo-utils"))
    implementation(project(":auctions:domain"))
    implementation(project(":auctions:usecase"))
    implementation(project(":auctions:adapter"))

    implementation("org.mongodb:mongo-java-driver:3.12.10")
}