module lss.ds.luckyauction.magnani.pasolini.auctions.repository.mongo.main {
    exports it.unibo.luckyauction.auction.repository;
    requires lss.ds.luckyauction.magnani.pasolini.utils.main;
    requires lss.ds.luckyauction.magnani.pasolini.mongo.utils.main;
    requires lss.ds.luckyauction.magnani.pasolini.auctions.domain.main;
    requires lss.ds.luckyauction.magnani.pasolini.auctions.usecase.main;
    requires lss.ds.luckyauction.magnani.pasolini.auctions.adapter.main;
    requires mongo.java.driver;
}