/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.repository;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Updates;
import it.unibo.luckyauction.auction.adapter.AbstractAuctionWeb;
import it.unibo.luckyauction.auction.adapter.AuctionIdsHistoryWeb;
import it.unibo.luckyauction.auction.adapter.AuctionWeb;
import it.unibo.luckyauction.auction.adapter.AuctionsHistoryWeb;
import it.unibo.luckyauction.auction.adapter.AwardedAuctionWeb;
import it.unibo.luckyauction.auction.adapter.CreatedEndedAuctionWeb;
import it.unibo.luckyauction.auction.adapter.CreatedOngoingAuctionWeb;
import it.unibo.luckyauction.auction.adapter.LostAuctionWeb;
import it.unibo.luckyauction.auction.adapter.ParticipatingAuctionWeb;
import it.unibo.luckyauction.auction.domain.entity.AuctionsHistory;
import it.unibo.luckyauction.auction.domain.valueobject.AbstractAuction;
import it.unibo.luckyauction.auction.domain.valueobject.AwardedAuction;
import it.unibo.luckyauction.auction.domain.valueobject.CreatedEndedAuction;
import it.unibo.luckyauction.auction.domain.valueobject.CreatedOngoingAuction;
import it.unibo.luckyauction.auction.domain.valueobject.LostAuction;
import it.unibo.luckyauction.auction.domain.valueobject.ParticipatingAuction;
import it.unibo.luckyauction.auction.domain.valueobject.UserContactInformation;
import it.unibo.luckyauction.auction.usecase.exception.AuctionNotFoundException;
import it.unibo.luckyauction.auction.usecase.exception.AuctionsHistoryNotFoundException;
import it.unibo.luckyauction.auction.usecase.port.AuctionsHistoryRepository;
import it.unibo.luckyauction.repository.util.MongoDatabaseManager;
import org.bson.BsonArray;
import org.bson.BsonDocument;
import org.bson.BsonInt32;
import org.bson.BsonString;
import org.bson.conversions.Bson;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Updates.pullByFilter;
import static com.mongodb.client.model.Updates.push;
import static it.unibo.luckyauction.repository.util.MongoConnectionsConstants.AUCTIONS_COLLECTION_NAME;
import static it.unibo.luckyauction.repository.util.MongoConnectionsConstants.AUCTIONS_CONNECTION_STRING;
import static it.unibo.luckyauction.repository.util.MongoConnectionsConstants.AUCTIONS_HISTORIES_COLLECTION_NAME;
import static it.unibo.luckyauction.repository.util.MongoConnectionsConstants.AUCTIONS_HISTORIES_DB_NAME;
import static it.unibo.luckyauction.repository.util.MongoConnectionsConstants.TEST_CONNECTION_STRING;
import static it.unibo.luckyauction.repository.util.MongoConnectionsConstants.TEST_DB_NAME;

@SuppressWarnings("PMD.AvoidDuplicateLiterals")
public final class AuctionsHistoryMongoRepository implements AuctionsHistoryRepository {

    private static final String JOIN_AUCTIONS_SUFFIX = "_auctions";

    private final MongoCollection<AuctionIdsHistoryWeb> auctionsHistoriesCollection;
    private final MongoCollection<AuctionWeb> auctionsCollection;

    private AuctionsHistoryMongoRepository(final String connectionString, final String databaseName,
                                           final String auctionsHistoriesCollectionName,
                                           final String auctionsCollectionName) {
        final MongoDatabaseManager manager = new MongoDatabaseManager(connectionString, databaseName);
        auctionsHistoriesCollection = manager.getCollection(auctionsHistoriesCollectionName, AuctionIdsHistoryWeb.class);
        auctionsCollection = manager.getCollection(auctionsCollectionName, AuctionWeb.class);
    }

    public static AuctionsHistoryMongoRepository defaultRepository() {
        return new AuctionsHistoryMongoRepository(AUCTIONS_CONNECTION_STRING, AUCTIONS_HISTORIES_DB_NAME,
                AUCTIONS_HISTORIES_COLLECTION_NAME, AUCTIONS_COLLECTION_NAME);
    }

    public static AuctionsHistoryMongoRepository testRepository() {
        return new AuctionsHistoryMongoRepository(TEST_CONNECTION_STRING, TEST_DB_NAME,
                AUCTIONS_HISTORIES_COLLECTION_NAME, AUCTIONS_COLLECTION_NAME);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void create(final AuctionsHistory auctionsHistory) {
        auctionsHistoriesCollection.insertOne(AuctionIdsHistoryWeb.fromDomain(auctionsHistory));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<String> deleteByUsername(final String username) {
        return Optional.ofNullable(auctionsHistoriesCollection.findOneAndDelete(eq("username", username)))
                .map(AuctionIdsHistoryWeb::getAuctionsHistoryId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<AuctionsHistory> findByUsername(final String username) {
        return new JoinAuctionsPipelineBuilder<>(AuctionsHistoryWeb.class, AuctionsHistoryWeb::toDomain)
                .addUsernameMatch(username)
                .aggregateWithSingleOutput();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addAwardedAuction(final String auctionId, final UserContactInformation sellerContacts,
                                  final UserContactInformation buyerContacts, final String username)
            throws AuctionNotFoundException, AuctionsHistoryNotFoundException {
        pushAuctionId("awardedAuctions", auctionId, username, sellerContacts, buyerContacts);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addCreatedEndedAuction(final String auctionId, final String username)
            throws AuctionNotFoundException, AuctionsHistoryNotFoundException {
        pushAuctionId("createdEndedAuctions", auctionId, username);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addCreatedEndedAuction(final String auctionId, final UserContactInformation sellerContacts,
                                       final UserContactInformation buyerContacts, final String username)
            throws AuctionNotFoundException, AuctionsHistoryNotFoundException {
        pushAuctionId("createdEndedAuctions", auctionId, username, sellerContacts, buyerContacts);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addCreatedOngoingAuction(final String auctionId, final String username)
            throws AuctionNotFoundException, AuctionsHistoryNotFoundException {
        pushAuctionId("createdOngoingAuctions", auctionId, username);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addLostAuction(final String auctionId, final String username)
            throws AuctionNotFoundException, AuctionsHistoryNotFoundException {
        pushAuctionId("lostAuctions", auctionId, username);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addParticipatingAuction(final String auctionId, final String username)
            throws AuctionNotFoundException, AuctionsHistoryNotFoundException {
        pushAuctionId("participatingAuctions", auctionId, username);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<AwardedAuction> getAwardedAuctions(final String username) {
        return joinAuctionsForUser(username, AwardedAuctionWeb.class, AwardedAuctionWeb::toDomainAuction);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<CreatedOngoingAuction> getCreatedOngoingAuctions(final String username) {
        return joinAuctionsForUser(username, CreatedOngoingAuctionWeb.class, CreatedOngoingAuctionWeb::toDomainAuction);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<CreatedEndedAuction> getCreatedEndedAuctions(final String username) {
        return joinAuctionsForUser(username, CreatedEndedAuctionWeb.class, CreatedEndedAuctionWeb::toDomainAuction);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<LostAuction> getLostAuctions(final String username) {
        return joinAuctionsForUser(username, LostAuctionWeb.class, LostAuctionWeb::toDomainAuction);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ParticipatingAuction> getParticipatingAuctions(final String username) {
        return joinAuctionsForUser(username, ParticipatingAuctionWeb.class, ParticipatingAuctionWeb::toDomainAuction);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<AwardedAuction> getAwardedAuction(final String auctionId, final String username) {
        return joinAuctionForUser(auctionId, username, AwardedAuctionWeb.class, AwardedAuctionWeb::toDomainAuction);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<CreatedOngoingAuction> getCreatedOngoingAuction(final String auctionId, final String username) {
        return joinAuctionForUser(auctionId, username, CreatedOngoingAuctionWeb.class,
                CreatedOngoingAuctionWeb::toDomainAuction);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<CreatedEndedAuction> getCreatedEndedAuction(final String auctionId, final String username) {
        return joinAuctionForUser(auctionId, username, CreatedEndedAuctionWeb.class,
                CreatedEndedAuctionWeb::toDomainAuction);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<LostAuction> getLostAuction(final String auctionId, final String username) {
        return joinAuctionForUser(auctionId, username, LostAuctionWeb.class, LostAuctionWeb::toDomainAuction);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<ParticipatingAuction> getParticipatingAuction(final String auctionId, final String username) {
        return joinAuctionForUser(auctionId, username, ParticipatingAuctionWeb.class,
                ParticipatingAuctionWeb::toDomainAuction);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<String> getUsersParticipatingInAuction(final String auctionId) {
        return auctionsHistoriesCollection.find(eq("participatingAuctions.auctionId", auctionId))
                .map(AuctionIdsHistoryWeb::getUsername)
                .into(new ArrayList<>());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeParticipatingAuction(final String auctionId, final String username)
            throws AuctionNotFoundException, AuctionsHistoryNotFoundException {
        pullAuctionId("participatingAuctions", auctionId, username);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeCreatedOngoingAuction(final String auctionId, final String username)
            throws AuctionNotFoundException, AuctionsHistoryNotFoundException {
        pullAuctionId("createdOngoingAuctions", auctionId, username);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeAuctionFromAllHistory(final String auctionId) {
        auctionsHistoriesCollection.updateMany(new BasicDBObject(), Updates.combine(
                Stream.of("awardedAuctions", "lostAuctions", "participatingAuctions",
                                "createdOngoingAuctions", "createdEndedAuctions")
                        .map(field -> pullByFilter(new BsonDocument(field,
                                new BsonDocument("auctionId", new BsonString(auctionId)))))
                        .collect(Collectors.toList())
                ));
    }

    private <T extends AbstractAuctionWeb, D extends AbstractAuction> List<D> joinAuctionsForUser(
            final String username,
            final Class<T> auctionClass,
            final Function<T, D> auctionMapper) {
        return new JoinAuctionsPipelineBuilder<>(auctionClass, auctionMapper)
                .addUsernameMatch(username)
                .aggregateWithMultipleOutputs();
    }

    private <T extends AbstractAuctionWeb, D extends AbstractAuction> Optional<D> joinAuctionForUser(
            final String auctionId,
            final String username,
            final Class<T> auctionClass,
            final Function<T, D> auctionMapper) {
        return new JoinAuctionsPipelineBuilder<>(auctionClass, auctionMapper)
                .addUsernameMatch(username)
                .addAuctionIdMatch(auctionId)
                .aggregateWithSingleOutput();
    }

    private void assertAuctionExists(final String auctionId) {
        if (auctionsCollection.find(eq("auctionId", auctionId)).first() == null) {
            throw new AuctionNotFoundException(auctionId);
        }
    }

    private void updateHistory(final String auctionId, final String username, final Bson update) {
        assertAuctionExists(auctionId);
        if (auctionsHistoriesCollection.findOneAndUpdate(eq("username", username), update) == null) {
            throw new AuctionsHistoryNotFoundException(username);
        }
    }

    private void pushAuctionId(final String fieldName, final String auctionId, final String username) {
        updateHistory(auctionId, username, push(fieldName, new AuctionIdsHistoryWeb.AuctionReference(auctionId)));
    }

    private void pushAuctionId(final String fieldName, final String auctionId, final String username,
                               final UserContactInformation sellerContacts, final UserContactInformation buyerContacts) {
        updateHistory(auctionId, username, push(fieldName, new AuctionIdsHistoryWeb.AuctionReference(auctionId,
                sellerContacts, buyerContacts))
        );
    }

    private void pullAuctionId(final String fieldName, final String auctionId, final String username) {
        updateHistory(auctionId, username, pullByFilter(new BsonDocument(fieldName,
                new BsonDocument("auctionId", new BsonString(auctionId)))));
    }

    private String auctionClassToFieldName(final Class<? extends AbstractAuctionWeb> auctionClass) {
        final String output = auctionClass.getSimpleName().replace("Web", "");
        return String.valueOf(output.charAt(0)).toLowerCase(Locale.ROOT) + (output.substring(1)) + "s";
    }

    private final class JoinAuctionsPipelineBuilder<T, D> {

        private final List<Bson> pipeline;
        private final List<String> auctionSummaryFields;
        private final Class<T> joinResultClass;
        private final Function<T, D> mapperToDomainAuction;

        @SuppressWarnings("unchecked")
        private JoinAuctionsPipelineBuilder(final Class<T> joinResultClass, final Function<T, D> mapperToDomainAuction,
                                            final String... auctionSummaryFields) {
            this.pipeline = new ArrayList<>();
            this.auctionSummaryFields = AbstractAuctionWeb.class.isAssignableFrom(joinResultClass)
                    ? List.of(auctionClassToFieldName((Class<? extends AbstractAuctionWeb>) joinResultClass))
                    : List.of(auctionSummaryFields);
            this.joinResultClass = joinResultClass;
            this.mapperToDomainAuction = mapperToDomainAuction;
        }

        public JoinAuctionsPipelineBuilder<T, D> addUsernameMatch(final String username) {
            pipeline.add(new BsonDocument("$match", new BsonDocument("username", new BsonString(username))));
            return this;
        }

        public JoinAuctionsPipelineBuilder<T, D> addAuctionIdMatch(final String auctionId) {
            pipeline.add(new BsonDocument("$unwind", new BsonString("$" + auctionSummaryFields.get(0))));
            pipeline.add(new BsonDocument("$match",
                    new BsonDocument(auctionSummaryFields.get(0) + ".auctionId", new BsonString(auctionId))));
            pipeline.add(new BsonDocument("$addFields",
                    new BsonDocument(auctionSummaryFields.get(0), new BsonArray(
                            List.of(new BsonString("$" + auctionSummaryFields.get(0))))))
            );
            return this;
        }

        public Optional<D> aggregateWithSingleOutput() {
            completePipeline();
            return auctionsHistoriesCollection.aggregate(pipeline, joinResultClass)
                    .into(new ArrayList<>()).stream()
                    .map(mapperToDomainAuction)
                    .findFirst();
        }

        public List<D> aggregateWithMultipleOutputs() {
            completePipeline();
            return auctionsHistoriesCollection.aggregate(pipeline, joinResultClass)
                    .into(new ArrayList<>()).stream()
                    .map(mapperToDomainAuction)
                    .collect(Collectors.toList());
        }

        private void completePipeline() {
            pipeline.addAll(buildPipelineWithLookupAndMerge(auctionSummaryFields.toArray(new String[0])));

            if (auctionSummaryFields.size() == 1) {
                final String localField = auctionSummaryFields.get(0);

                pipeline.add(new BasicDBObject("$unwind", new BsonString("$" + localField)));
                pipeline.add(new BasicDBObject("$replaceRoot",
                        new BsonDocument("newRoot", new BsonString("$" + localField))));
            }
        }

        @SuppressWarnings("checkstyle:FinalParameters")
        private List<Bson> buildPipelineWithLookupAndMerge(String... auctionSummaryFields) {
            // lookup fields; if not specified, keep all
            if (auctionSummaryFields == null || auctionSummaryFields.length == 0) {
                auctionSummaryFields = new String[]{"awardedAuctions", "lostAuctions", "participatingAuctions",
                        "createdOngoingAuctions", "createdEndedAuctions"};
            }
            final List<Bson> pipeline = Stream.of(auctionSummaryFields)
                    .map(this::buildLookupBson)
                    .collect(Collectors.toList());

            // merge the output of (each) lookup with original json(s), using $addFields construct
            final BsonDocument addFields = new BsonDocument();
            Stream.of(auctionSummaryFields).forEach(field ->
                    addFields.append(field, new BsonDocument(
                            "$map", new BsonDocument()
                            .append("input", new BsonString("$" + field))
                            .append("in", new BsonDocument("$mergeObjects", new BsonArray(List.of(
                                    new BsonString("$$this"),
                                    new BsonDocument("$arrayElemAt", new BsonArray(List.of(
                                            new BsonString("$" + field + JOIN_AUCTIONS_SUFFIX),
                                            new BsonDocument("$indexOfArray", new BsonArray(List.of(
                                                    new BsonString("$" + field + "_auctions.auctionId"),
                                                    new BsonString("$$this.auctionId")
                                            )))
                                    )))
                            ))))
                    ))
            );
            pipeline.add(new BasicDBObject("$addFields", addFields));

            // remove temporary field(s) used for lookup
            final BsonDocument excludedFields = new BsonDocument();
            Stream.of(auctionSummaryFields).forEach(field ->
                    excludedFields.append(field + JOIN_AUCTIONS_SUFFIX, new BsonInt32(0)));
            pipeline.add(new BasicDBObject("$project", excludedFields));

            return pipeline;
        }

        private Bson buildLookupBson(final String localField) {
            final DBObject lookupFields = new BasicDBObject("from", "auctions");
            lookupFields.put("localField", localField + ".auctionId");
            lookupFields.put("foreignField", "auctionId");
            lookupFields.put("as", localField + JOIN_AUCTIONS_SUFFIX);
            return new BasicDBObject("$lookup", lookupFields);
        }
    }
}
