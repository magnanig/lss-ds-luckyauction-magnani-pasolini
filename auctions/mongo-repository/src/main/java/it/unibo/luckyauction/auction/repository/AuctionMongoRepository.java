/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.repository;

import com.mongodb.client.MongoCollection;
import it.unibo.luckyauction.auction.adapter.AuctionWeb;
import it.unibo.luckyauction.auction.adapter.BidWeb;
import it.unibo.luckyauction.auction.domain.entity.Auction;
import it.unibo.luckyauction.auction.domain.valueobject.Bid;
import it.unibo.luckyauction.auction.usecase.port.AuctionFilter;
import it.unibo.luckyauction.auction.usecase.port.AuctionRepository;
import it.unibo.luckyauction.repository.util.CommonsMongoOperations;
import it.unibo.luckyauction.repository.util.MongoDatabaseManager;
import it.unibo.luckyauction.util.CalendarUtils;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.Decimal128;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.exists;
import static com.mongodb.client.model.Filters.ne;
import static com.mongodb.client.model.Filters.not;
import static com.mongodb.client.model.Filters.or;
import static com.mongodb.client.model.Projections.excludeId;
import static com.mongodb.client.model.Projections.fields;
import static com.mongodb.client.model.Projections.include;
import static com.mongodb.client.model.Updates.combine;
import static com.mongodb.client.model.Updates.push;
import static com.mongodb.client.model.Updates.set;
import static com.mongodb.client.model.Updates.unset;
import static it.unibo.luckyauction.repository.util.MongoConnectionsConstants.AUCTIONS_COLLECTION_NAME;
import static it.unibo.luckyauction.repository.util.MongoConnectionsConstants.AUCTIONS_CONNECTION_STRING;
import static it.unibo.luckyauction.repository.util.MongoConnectionsConstants.AUCTIONS_HISTORIES_DB_NAME;
import static it.unibo.luckyauction.repository.util.MongoConnectionsConstants.TEST_CONNECTION_STRING;
import static it.unibo.luckyauction.repository.util.MongoConnectionsConstants.TEST_DB_NAME;

@SuppressWarnings("PMD.AvoidDuplicateLiterals")
public final class AuctionMongoRepository implements AuctionRepository {

    private final MongoCollection<AuctionWeb> auctionsCollection;
    private final CommonsMongoOperations<AuctionWeb, Auction> commons;
    private final Bson activeAuctionsBsonFilter = or(
            not(exists("closingTime")),
            eq("closingTime", ""),
            eq("closingTime", null)
    );

    private AuctionMongoRepository(final String connectionString, final String databaseName,
                                   final String collectionName) {
        auctionsCollection = new MongoDatabaseManager(connectionString, databaseName)
                .getCollection(collectionName, AuctionWeb.class);
        commons = new CommonsMongoOperations<>(auctionsCollection, AuctionWeb::toDomainAuction);
    }

    public static AuctionMongoRepository defaultRepository() {
        return new AuctionMongoRepository(AUCTIONS_CONNECTION_STRING, AUCTIONS_HISTORIES_DB_NAME,
                AUCTIONS_COLLECTION_NAME);
    }

    public static AuctionMongoRepository testRepository() {
        return new AuctionMongoRepository(TEST_CONNECTION_STRING, TEST_DB_NAME, AUCTIONS_COLLECTION_NAME);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void create(final Auction auction) {
        auctionsCollection.insertOne(AuctionWeb.fromDomain(auction));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<Auction> deleteById(final String auctionId) {
        return commons.deleteOneByFilter(eq("auctionId", auctionId));
    }

    /**
     * Delete all auctions of a specific seller user.
     * @param sellerUsername    the desired seller username
     */
    @Override
    public void deleteByUsername(final String sellerUsername) {
        auctionsCollection.deleteMany(eq("sellerUsername", sellerUsername));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addNewBid(final String auctionId, final Bid bid) {
        auctionsCollection.findOneAndUpdate(eq("auctionId", auctionId), combine(
                        set("actualBid", BidWeb.fromDomain(bid)),
                        push("bidsHistory.bids", BidWeb.fromDomain(bid))
                )
        );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<Auction> findById(final String auctionId) {
        return commons.findOneByFilter(eq("auctionId", auctionId));
    }

    /**
     * Get auctions list of a specific username.
     */
    @Override
    public List<Auction> findByUsername(final String sellerUsername) {
        return commons.findManyByFilter(eq("sellerUsername", sellerUsername));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Auction> getAllAuctions() {
        return commons.findMany();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Auction> getAllActiveAuctions() {
        return commons.findManyByFilter(activeAuctionsBsonFilter);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Auction> findActiveAuctions(final AuctionFilter auctionFilter) {
        if (auctionFilter.isEmpty()) {
            return getAllActiveAuctions();
        }
        return commons.findManyByFilter(and(activeAuctionsBsonFilter, auctionFilterToBson(auctionFilter)));
    }

    private <T> T getField(final String auctionId, final String fieldName, final Class<T> fieldClass) {
        return auctionsCollection.find(eq("auctionId", auctionId), Document.class)
                .projection(fields(include(fieldName), excludeId()))
                .map(document -> document.get(fieldName, fieldClass))
                .first();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<Bid> getActualBidByAuctionId(final String auctionId) {
        return Optional.ofNullable(getField(auctionId, "actualBid", Document.class))
                .map(document -> new Bid(
                        new BigDecimal(document.get("amount", Decimal128.class).toString()),
                        document.getString("bidderUsername"),
                        CalendarUtils.fromDate(document.get("timestamp", Date.class)))
                );
    }

    @Override
    public BigDecimal getStartingPriceByAuctionId(final String auctionId) {
        return Optional.ofNullable(getField(auctionId, "startingPrice", Decimal128.class))
                .map(value -> new BigDecimal(value.toString()))
                .orElse(null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Bid> getBidsHistory(final String auctionId) {
        return commons.findOneByFilter(eq("auctionId", auctionId))
                .map(Auction::getBids)
                .orElse(Collections.emptyList());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateExpectedClosingTime(final String auctionId, final Calendar newExpectedClosingTime) {
        auctionsCollection.findOneAndUpdate(eq("auctionId", auctionId),
                set("expectedClosingTime", newExpectedClosingTime.getTime()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void extendAuction(final String auctionId, final Calendar newExpectedClosingTime) {
        auctionsCollection.updateOne(eq("auctionId", auctionId), combine(
                unset("extensionAmount"),
                set("expectedClosingTime", newExpectedClosingTime.getTime())
        ));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void closeAuction(final String auctionId, final Calendar closingTime, final Bid winningBid) {
        final Bson update = winningBid == null
                ? combine(
                    unset("actualBid"),
                    set("closingTime", closingTime.getTime())
                )
                : combine(
                    unset("actualBid"),
                    set("winningBid", BidWeb.fromDomain(winningBid)),
                    set("closingTime", closingTime.getTime())
                );
        auctionsCollection.findOneAndUpdate(eq("auctionId", auctionId), update);
    }

    private Bson auctionFilterToBson(final AuctionFilter auctionFilter) {
        final List<Bson> bsonFilters = new ArrayList<>();
        auctionFilter.getProductState()
                .ifPresent(productState -> bsonFilters.add(eq("product.state", productState.name())));
        auctionFilter.getProductCategory()
                .ifPresent(productCategory -> bsonFilters.add(
                        eq("product.category", productCategory.name())));
        auctionFilter.getExceptUsername()
                .ifPresent(excludedUsername -> bsonFilters.add(ne("sellerUsername", excludedUsername)));
        return and(bsonFilters);
    }
}
