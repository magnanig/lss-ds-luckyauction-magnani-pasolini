/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.repository;

import it.unibo.luckyauction.auction.domain.entity.Auction;
import it.unibo.luckyauction.auction.usecase.port.AuctionRepository;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SuppressWarnings("PMD.JUnit4TestShouldUseTestAnnotation") // they are test utilities, not tests
public final class AuctionTestUtils {

    public static void testCreateAuction(final AuctionRepository auctionRepository, final Auction auction) {
        if (auctionRepository.findById(auction.getAuctionId()).isPresent()) {
            auctionRepository.deleteById(auction.getAuctionId());
        }
        assertEquals(Optional.empty(), auctionRepository.findById(auction.getAuctionId()));
        auctionRepository.create(auction);
        assertEquals(Optional.of(auction), auctionRepository.findById(auction.getAuctionId()));
        assertTrue(auctionRepository.getAllAuctions().contains(auction));
    }

    public static void testAuctionDeletion(final AuctionRepository auctionRepository, final Auction auction) {
        if (auctionRepository.findById(auction.getAuctionId()).isPresent()) {
            assertEquals(Optional.of(auction.getAuctionId()),
                    auctionRepository.deleteById(auction.getAuctionId()).map(Auction::getAuctionId));
            assertEquals(Optional.empty(), auctionRepository.findById(auction.getAuctionId()));
        }
    }

    private AuctionTestUtils() { }
}
