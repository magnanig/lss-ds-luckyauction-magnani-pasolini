/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.repository;

import it.unibo.luckyauction.auction.domain.entity.Auction;
import it.unibo.luckyauction.auction.domain.entity.AuctionsHistory;
import it.unibo.luckyauction.auction.domain.valueobject.AwardedAuction;
import it.unibo.luckyauction.auction.domain.valueobject.Bid;
import it.unibo.luckyauction.auction.domain.valueobject.CreatedEndedAuction;
import it.unibo.luckyauction.auction.domain.valueobject.CreatedOngoingAuction;
import it.unibo.luckyauction.auction.domain.valueobject.LostAuction;
import it.unibo.luckyauction.auction.domain.valueobject.ParticipatingAuction;
import it.unibo.luckyauction.auction.domain.valueobject.Product;
import it.unibo.luckyauction.auction.domain.valueobject.ProductCategory;
import it.unibo.luckyauction.auction.domain.valueobject.ProductState;
import it.unibo.luckyauction.auction.domain.valueobject.UserContactInformation;
import it.unibo.luckyauction.auction.usecase.port.AuctionRepository;
import it.unibo.luckyauction.auction.usecase.port.AuctionsHistoryRepository;
import it.unibo.luckyauction.util.CalendarUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.math.BigDecimal;
import java.time.Duration;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class TestAuctionsHistoryRepository {

    private final AuctionRepository auctionRepository = AuctionMongoRepository.testRepository();
    private final AuctionsHistoryRepository auctionsHistoryRepository = AuctionsHistoryMongoRepository.testRepository();

    private final UserContactInformation sellerContacts = new UserContactInformation("seller@test.com",
            "0541123456");
    private final UserContactInformation buyerContacts = new UserContactInformation("buyer@test.com",
            "0541654321");

    private static final String USERNAME = "test";
    private static final String ANOTHER_USERNAME = "anotherTest";
    private final Bid winningBid = new Bid(new BigDecimal("290"), USERNAME);

    private final Map<String, AuctionsHistory> auctionsHistories = Map.of(
            USERNAME, new AuctionsHistory("1", USERNAME),
            ANOTHER_USERNAME, new AuctionsHistory("2", ANOTHER_USERNAME)
    );
    private final Auction.AuctionBuilder prebuiltAuction = Auction.builder()
            .product(Product.builder()
                    .name("iPhone X")
                    .category(ProductCategory.INFORMATICS)
                    .state(ProductState.USED_OPTIMUM_CONDITION)
                    .image("base64img")
                    .build())
            .startingPrice(BigDecimal.ONE)
            .duration(Duration.ofHours(1))
            .extensionDuration(Duration.ofHours(1))
            .sellerUsername("testUser")
            .bidsHistoryId("1");

    private final Auction ongoingAuction = prebuiltAuction.auctionId("1").build();
    private final Auction endedAuction = prebuiltAuction.auctionId("2")
            .startingPrice(BigDecimal.ONE)
            .creationTime(CalendarUtils.add(Calendar.getInstance(), ongoingAuction.getDuration().negated()))
            .closingTime(Calendar.getInstance())
            .bids(List.of(winningBid))
            .winningBid(winningBid)
            .build();

    @BeforeAll
    void createTestAuctions() {
        if (auctionRepository.findById(ongoingAuction.getAuctionId()).isPresent()) {
            auctionRepository.deleteById(ongoingAuction.getAuctionId());
        }
        auctionRepository.create(ongoingAuction);
        if (auctionRepository.findById(endedAuction.getAuctionId()).isPresent()) {
            auctionRepository.deleteById(endedAuction.getAuctionId());
        }
        auctionRepository.create(endedAuction);
    }

    @ParameterizedTest
    @ValueSource(strings = { "test", "anotherTest" })
    @Order(0)
    void testAuctionsHistoryCreation(final String username) {
        if (auctionsHistoryRepository.findByUsername(username).isPresent()) {
            auctionsHistoryRepository.deleteByUsername(username);
            assertEquals(Optional.empty(), auctionsHistoryRepository.findByUsername(username));
        }
        auctionsHistoryRepository.create(auctionsHistories.get(username));
        assertEquals(Optional.of(auctionsHistories.get(username)), auctionsHistoryRepository.findByUsername(username));
    }

    @Test
    void testGetAddAwardedAuctions() {
        auctionsHistoryRepository.addAwardedAuction(endedAuction.getAuctionId(), sellerContacts, buyerContacts, USERNAME);
        assertEquals(List.of(AwardedAuction.fromAuction(endedAuction, sellerContacts, buyerContacts)),
                auctionsHistoryRepository.getAwardedAuctions(USERNAME));
        assertEquals(Optional.of(AwardedAuction.fromAuction(endedAuction, sellerContacts, buyerContacts)),
                auctionsHistoryRepository.getAwardedAuction(endedAuction.getAuctionId(), USERNAME));
    }

    @Test
    void testGetAddLostAuctions() {
        auctionsHistoryRepository.addLostAuction(endedAuction.getAuctionId(), USERNAME);
        assertEquals(Optional.of(LostAuction.fromAuction(endedAuction)),
                auctionsHistoryRepository.getLostAuction(endedAuction.getAuctionId(), USERNAME));
        assertEquals(List.of(LostAuction.fromAuction(endedAuction)),
                auctionsHistoryRepository.getLostAuctions(USERNAME));
    }

    @Test
    void testGetAddCreatedEndedAuctions() {
        auctionsHistoryRepository.addCreatedEndedAuction(endedAuction.getAuctionId(), buyerContacts, sellerContacts, USERNAME);
        assertEquals(Optional.of(CreatedEndedAuction.fromAuctionWithWinner(endedAuction, sellerContacts, buyerContacts)),
                auctionsHistoryRepository.getCreatedEndedAuction(endedAuction.getAuctionId(), USERNAME));
        assertEquals(List.of(CreatedEndedAuction.fromAuctionWithWinner(endedAuction, sellerContacts, buyerContacts)),
                auctionsHistoryRepository.getCreatedEndedAuctions(USERNAME));
    }

    @Test
    void testGetAddRemoveCreatedOngoingAuctions() {
        auctionsHistoryRepository.addCreatedOngoingAuction(ongoingAuction.getAuctionId(), USERNAME);
        assertEquals(Optional.of(CreatedOngoingAuction.fromAuction(ongoingAuction)),
                auctionsHistoryRepository.getCreatedOngoingAuction(ongoingAuction.getAuctionId(), USERNAME));
        assertEquals(List.of(CreatedOngoingAuction.fromAuction(ongoingAuction)),
                auctionsHistoryRepository.getCreatedOngoingAuctions(USERNAME));
        auctionsHistoryRepository.removeCreatedOngoingAuction(ongoingAuction.getAuctionId(), USERNAME);
        assertEquals(Collections.emptyList(), auctionsHistoryRepository.getCreatedOngoingAuctions(USERNAME));
    }

    @Test
    void testGetAddRemoveParticipatingAuctions() {
        auctionsHistoryRepository.addParticipatingAuction(ongoingAuction.getAuctionId(), USERNAME);
        assertEquals(Optional.of(ParticipatingAuction.fromAuction(ongoingAuction)),
                auctionsHistoryRepository.getParticipatingAuction(ongoingAuction.getAuctionId(), USERNAME));
        assertEquals(List.of(ParticipatingAuction.fromAuction(ongoingAuction)),
                auctionsHistoryRepository.getParticipatingAuctions(USERNAME));
        auctionsHistoryRepository.removeParticipatingAuction(ongoingAuction.getAuctionId(), USERNAME);
        assertEquals(Collections.emptyList(), auctionsHistoryRepository.getParticipatingAuctions(USERNAME));
    }

    @Test
    @Order(Integer.MAX_VALUE - 2)
    void testGetUsersParticipatingInAuction() {
        assertEquals(Collections.emptyList(),
                auctionsHistoryRepository.getUsersParticipatingInAuction(ongoingAuction.getAuctionId()));
        auctionsHistoryRepository.addParticipatingAuction(ongoingAuction.getAuctionId(), USERNAME);
        auctionsHistoryRepository.addParticipatingAuction(ongoingAuction.getAuctionId(), ANOTHER_USERNAME);
        assertEquals(List.of(USERNAME, ANOTHER_USERNAME),
                auctionsHistoryRepository.getUsersParticipatingInAuction(ongoingAuction.getAuctionId()));
    }

    @SuppressWarnings("PMD.JUnitTestsShouldIncludeAssert")
    @Test
    @Order(Integer.MAX_VALUE - 1)
    void testRemoveAuctionFromAllHistories() {
        auctionsHistoryRepository.removeAuctionFromAllHistory(endedAuction.getAuctionId());
        final AuctionsHistory userAuctionsHistory = auctionsHistoryRepository.findByUsername(USERNAME).orElseThrow();
        final AuctionsHistory anotherUserAuctionsHistory = auctionsHistoryRepository.findByUsername(ANOTHER_USERNAME)
                .orElseThrow();
        List.of(userAuctionsHistory, anotherUserAuctionsHistory).forEach(auctionsHistory -> {
            assertEquals(Collections.emptyList(), auctionsHistory.getAwardedAuctions());
            assertEquals(Collections.emptyList(), auctionsHistory.getLostAuctions());
            assertEquals(Collections.emptyList(), auctionsHistory.getCreatedEndedAuctions());
        });
    }

    @Test
    @Order(Integer.MAX_VALUE)
    void cleanAuctionsAndHistory() {
        auctionRepository.deleteById(ongoingAuction.getAuctionId());
        auctionRepository.deleteById(endedAuction.getAuctionId());
        auctionsHistoryRepository.deleteByUsername(USERNAME);
        auctionsHistoryRepository.deleteByUsername(ANOTHER_USERNAME);
        assertEquals(Optional.empty(), auctionsHistoryRepository.findByUsername(USERNAME));
        assertEquals(Optional.empty(), auctionsHistoryRepository.findByUsername(ANOTHER_USERNAME));
    }

}
