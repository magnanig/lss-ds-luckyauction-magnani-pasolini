/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.repository;

import it.unibo.luckyauction.auction.domain.entity.Auction;
import it.unibo.luckyauction.auction.domain.valueobject.Bid;
import it.unibo.luckyauction.auction.domain.valueobject.Product;
import it.unibo.luckyauction.auction.domain.valueobject.ProductCategory;
import it.unibo.luckyauction.auction.domain.valueobject.ProductState;
import it.unibo.luckyauction.auction.usecase.port.AuctionRepository;
import it.unibo.luckyauction.util.CalendarUtils;
import it.unibo.luckyauction.util.NumericalConstants;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.math.BigDecimal;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class TestAuctionLifeCycleMongoRepository {

    private final AuctionRepository auctionRepository = AuctionMongoRepository.testRepository();
    private final Auction auction = Auction.builder()
            .auctionId("1")
            .product(Product.builder()
                    .name("iPhone X")
                    .category(ProductCategory.INFORMATICS)
                    .state(ProductState.USED_OPTIMUM_CONDITION)
                    .image("base64img")
                    .build())
            .startingPrice(BigDecimal.ONE)
            .duration(Duration.ofHours(1))
            .extensionDuration(Duration.ofHours(1))
            .sellerUsername("testUser")
            .bidsHistoryId("1")
            .build();

    private final List<Bid> bids = new ArrayList<>();

    @SuppressWarnings("PMD.JUnitTestsShouldIncludeAssert")
    @Test
    @BeforeAll
    @Order(NumericalConstants.ONE)
    void testAuctionCreate() {
        AuctionTestUtils.testCreateAuction(auctionRepository, auction);
    }

    @ParameterizedTest
    @ValueSource(strings = { "10.99", "15.20", "17.00" })
    @Order(NumericalConstants.TWO)
    void testBidAdd(final String bidAmount) {
        assertEquals(bids, auctionRepository.getBidsHistory(auction.getAuctionId()));
        final String username = "testUser";
        final Bid bid = new Bid(new BigDecimal(bidAmount), username);
        bids.add(bid);
        auctionRepository.addNewBid(auction.getAuctionId(), bid);
        assertEquals(Optional.of(bid),
                auctionRepository.findById(auction.getAuctionId()).flatMap(Auction::getActualBid));
        assertEquals(bids, auctionRepository.getBidsHistory(auction.getAuctionId()));
    }

    @Test
    @Order(NumericalConstants.THREE)
    void testExpectedClosingTimeUpdate() {
        assertExpectedClosingTime(auction.getExpectedClosingTime());
        final Calendar newExpectedClosingTime = CalendarUtils.add(auction.getExpectedClosingTime(),
                Duration.ofMinutes(10));
        auctionRepository.updateExpectedClosingTime(auction.getAuctionId(), newExpectedClosingTime);
        assertExpectedClosingTime(newExpectedClosingTime);

        // restore expected closing time
        auctionRepository.updateExpectedClosingTime(auction.getAuctionId(), auction.getExpectedClosingTime());
        assertExpectedClosingTime(auction.getExpectedClosingTime());
    }

    @Test
    @Order(NumericalConstants.FOUR)
    void testAuctionExtension() {
        final Calendar newExpectedClosingTime = CalendarUtils.add(auction.getExpectedClosingTime(),
                auction.getExtensionAmount().orElseThrow());
        auctionRepository.extendAuction(auction.getAuctionId(), newExpectedClosingTime);
        final Auction returnedAuction = auctionRepository.findById(auction.getAuctionId()).orElseThrow();
        assertEquals(newExpectedClosingTime, returnedAuction.getExpectedClosingTime());
        assertEquals(Optional.empty(), returnedAuction.getExtensionAmount());
    }

    @Test
    @Order(NumericalConstants.FIVE)
    void testAuctionClosing() {
        final Calendar closingTime = Calendar.getInstance();
        final Bid winningBid = new Bid(new BigDecimal("25.00"), "winner");
        auctionRepository.addNewBid(auction.getAuctionId(), winningBid);
        assertEquals(Optional.of(winningBid),
                auctionRepository.findById(auction.getAuctionId()).flatMap(Auction::getActualBid));
        auctionRepository.closeAuction(auction.getAuctionId(), closingTime, winningBid);

        final Auction returnedAuction = auctionRepository.findById(auction.getAuctionId()).orElseThrow();
        assertEquals(Optional.of(closingTime), returnedAuction.getClosingTime());
        assertEquals(Optional.of(winningBid), returnedAuction.getWinningBid());
    }

    @SuppressWarnings("PMD.JUnitTestsShouldIncludeAssert")
    @Test
    @AfterAll
    @Order(NumericalConstants.SIX)
    void testAuctionDeletion() {
        AuctionTestUtils.testAuctionDeletion(auctionRepository, auction);
    }

    private void assertExpectedClosingTime(final Calendar expectedClosingTime) {
        assertEquals(Optional.of(expectedClosingTime),
                auctionRepository.findById(auction.getAuctionId()).map(Auction::getExpectedClosingTime));
    }

}
