/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.repository;

import it.unibo.luckyauction.auction.domain.entity.Auction;
import it.unibo.luckyauction.auction.domain.valueobject.Bid;
import it.unibo.luckyauction.auction.domain.valueobject.Product;
import it.unibo.luckyauction.auction.domain.valueobject.ProductCategory;
import it.unibo.luckyauction.auction.domain.valueobject.ProductState;
import it.unibo.luckyauction.auction.usecase.port.AuctionRepository;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.Duration;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TestAuctionMongoRepository {

    private static final AuctionRepository AUCTION_REPOSITORY = AuctionMongoRepository.testRepository();
    private static final Bid ACTUAL_BID = new Bid(new BigDecimal("110"), "buyer");
    private static final Auction AUCTION = Auction.builder()
            .auctionId("0")
            .product(Product.builder()
                    .name("name")
                    .category(ProductCategory.CLOTHING)
                    .state(ProductState.NEW)
                    .image("base64img")
                    .build())
            .duration(Duration.ofMinutes(1))
            .startingPrice(BigDecimal.ONE)
            .actualBid(ACTUAL_BID)
            .bidsHistoryId("0")
            .bids(List.of(ACTUAL_BID))
            .sellerUsername("test")
            .build();

    @BeforeAll
    static void createAuction() {
        AuctionTestUtils.testCreateAuction(AUCTION_REPOSITORY, AUCTION);
    }

    @AfterAll
    static void deleteAuctionBySellerUsername() {
        AUCTION_REPOSITORY.deleteByUsername(AUCTION.getSellerUsername());
    }

    @Test
    void testFindByUsername() {
        assertEquals(List.of(AUCTION), AUCTION_REPOSITORY.findByUsername(AUCTION.getSellerUsername()));
    }

    @Test
    void testGetStartingPrice() {
        assertEquals(AUCTION.getStartingPrice(), AUCTION_REPOSITORY.getStartingPriceByAuctionId(AUCTION.getAuctionId()));
    }

    @Test
    void testGetActualBid() {
        assertEquals(AUCTION.getActualBid(), AUCTION_REPOSITORY.getActualBidByAuctionId(AUCTION.getAuctionId()));
    }
}
