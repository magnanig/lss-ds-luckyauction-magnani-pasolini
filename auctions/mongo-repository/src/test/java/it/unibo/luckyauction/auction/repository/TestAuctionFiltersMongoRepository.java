/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.repository;

import it.unibo.luckyauction.auction.domain.entity.Auction;
import it.unibo.luckyauction.auction.domain.valueobject.Product;
import it.unibo.luckyauction.auction.domain.valueobject.ProductCategory;
import it.unibo.luckyauction.auction.domain.valueobject.ProductState;
import it.unibo.luckyauction.auction.usecase.port.AuctionFilter;
import it.unibo.luckyauction.auction.usecase.port.AuctionRepository;
import it.unibo.luckyauction.util.CalendarUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.math.BigDecimal;
import java.time.Duration;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestAuctionFiltersMongoRepository {

    private static final String TOM = "tom";
    private static final String BOB = "bob";
    private static final String JACK = "jack";

    private final AuctionRepository auctionRepository = AuctionMongoRepository.testRepository();
    private final Auction newClothingTomAuction = buildActiveAuction(ProductState.NEW, ProductCategory.CLOTHING,
            TOM, "1");
    private final Auction usedGoodBookBobAuction = buildActiveAuction(ProductState.USED_GOOD_CONDITION,
            ProductCategory.BOOKS, BOB, "2");
    private final Auction usedBadVehicleJackAuction = buildActiveAuction(ProductState.USED_BAD_CONDITION,
            ProductCategory.VEHICLES, JACK, "3");
    private final Auction endedNewClothingTomAuction = Auction.builder()
            .auctionId("0")
            .product(Product.builder()
                    .name("name")
                    .category(ProductCategory.CLOTHING)
                    .state(ProductState.NEW)
                    .image("base64img")
                    .build())
            .startingPrice(BigDecimal.ONE)
            .creationTime(CalendarUtils.add(Calendar.getInstance(), Duration.ofMinutes(10).negated()))
            .closingTime(Calendar.getInstance())
            .sellerUsername("tom")
            .bidsHistoryId("0")
            .build();

    @BeforeAll
    void createAuctions() {
        Set.of(newClothingTomAuction, usedGoodBookBobAuction, usedBadVehicleJackAuction, endedNewClothingTomAuction)
                .forEach(auction -> AuctionTestUtils.testCreateAuction(auctionRepository, auction));
    }

    @AfterAll
    void deleteAuctions() {
        auctionRepository.deleteById(newClothingTomAuction.getAuctionId());
        auctionRepository.deleteById(usedGoodBookBobAuction.getAuctionId());
        auctionRepository.deleteById(usedBadVehicleJackAuction.getAuctionId());
        auctionRepository.deleteById(endedNewClothingTomAuction.getAuctionId());
    }

    @Test
    void testGetAllActiveAuctions() {
        assertEquals(Set.of(newClothingTomAuction, usedGoodBookBobAuction, usedBadVehicleJackAuction),
                new HashSet<>(auctionRepository.getAllActiveAuctions()));
        assertEquals(Set.of(newClothingTomAuction, usedGoodBookBobAuction, usedBadVehicleJackAuction),
                new HashSet<>(auctionRepository.findActiveAuctions(new AuctionFilter.Builder().build())));
    }

    @Test
    void testProductStateFilter() {
        assertEquals(Set.of(newClothingTomAuction), new HashSet<>(auctionRepository.findActiveAuctions(
                new AuctionFilter.Builder().productState(ProductState.NEW).build())));
        assertEquals(Set.of(usedGoodBookBobAuction), new HashSet<>(auctionRepository.findActiveAuctions(
                new AuctionFilter.Builder().productState(ProductState.USED_GOOD_CONDITION).build())));
        assertEquals(Set.of(usedBadVehicleJackAuction), new HashSet<>(auctionRepository.findActiveAuctions(
                new AuctionFilter.Builder().productState(ProductState.USED_BAD_CONDITION).build())));
        assertEquals(Collections.emptySet(), new HashSet<>(auctionRepository.findActiveAuctions(
                new AuctionFilter.Builder().productState(ProductState.USED_OPTIMUM_CONDITION).build())));
        assertEquals(Collections.emptySet(), new HashSet<>(auctionRepository.findActiveAuctions(
                new AuctionFilter.Builder().productState(ProductState.USED_POOR_CONDITION).build())));
    }

    @Test
    void testProductCategoryFilter() {
        assertEquals(Set.of(newClothingTomAuction), new HashSet<>(auctionRepository.findActiveAuctions(
                new AuctionFilter.Builder().productCategory(ProductCategory.CLOTHING).build())));
        assertEquals(Set.of(usedGoodBookBobAuction), new HashSet<>(auctionRepository.findActiveAuctions(
                new AuctionFilter.Builder().productCategory(ProductCategory.BOOKS).build())));
        assertEquals(Set.of(usedBadVehicleJackAuction), new HashSet<>(auctionRepository.findActiveAuctions(
                new AuctionFilter.Builder().productCategory(ProductCategory.VEHICLES).build())));
        Arrays.stream(ProductCategory.values())
                .filter(productCategory -> productCategory != ProductCategory.CLOTHING
                                && productCategory != ProductCategory.BOOKS && productCategory != ProductCategory.VEHICLES)
                .forEach(otherProductCategory -> assertEquals(Collections.emptySet(),
                        new HashSet<>(auctionRepository.findActiveAuctions(new AuctionFilter.Builder()
                                .productCategory(ProductCategory.INFORMATICS).build())))
                );
    }

    @Test
    void testExceptFilter() {
        assertEquals(Set.of(usedGoodBookBobAuction, usedBadVehicleJackAuction),
                new HashSet<>(auctionRepository.findActiveAuctions(new AuctionFilter.Builder()
                        .exceptUsername(TOM).build())));
        assertEquals(Set.of(newClothingTomAuction, usedBadVehicleJackAuction),
                new HashSet<>(auctionRepository.findActiveAuctions(new AuctionFilter.Builder()
                        .exceptUsername(BOB).build())));
        assertEquals(Set.of(usedGoodBookBobAuction, newClothingTomAuction),
                new HashSet<>(auctionRepository.findActiveAuctions(new AuctionFilter.Builder()
                        .exceptUsername(JACK).build())));
    }

    @Test
    void testProductStateAndCategoryFilters() {
        assertEquals(Set.of(newClothingTomAuction), new HashSet<>(auctionRepository.findActiveAuctions(
                new AuctionFilter.Builder()
                        .productState(ProductState.NEW)
                        .productCategory(ProductCategory.CLOTHING)
                        .build())
        ));
        assertEquals(Set.of(usedGoodBookBobAuction), new HashSet<>(auctionRepository.findActiveAuctions(
                new AuctionFilter.Builder()
                        .productState(ProductState.USED_GOOD_CONDITION)
                        .productCategory(ProductCategory.BOOKS)
                        .build())
        ));
        assertEquals(Set.of(usedBadVehicleJackAuction), new HashSet<>(auctionRepository.findActiveAuctions(
                new AuctionFilter.Builder()
                        .productState(ProductState.USED_BAD_CONDITION)
                        .productCategory(ProductCategory.VEHICLES)
                        .build())
        ));
        assertEquals(Collections.emptySet(), new HashSet<>(auctionRepository.findActiveAuctions(
                new AuctionFilter.Builder()
                        .productState(ProductState.NEW)
                        .productCategory(ProductCategory.INFORMATICS)
                        .build())
        ));
        assertEquals(Collections.emptySet(), new HashSet<>(auctionRepository.findActiveAuctions(
                new AuctionFilter.Builder()
                        .productState(ProductState.USED_POOR_CONDITION)
                        .productCategory(ProductCategory.BOOKS)
                        .build())
        ));
    }

    @Test
    void testProductStateAndExceptFilters() {
        assertEquals(Set.of(newClothingTomAuction), new HashSet<>(auctionRepository.findActiveAuctions(
                new AuctionFilter.Builder()
                        .productState(ProductState.NEW)
                        .exceptUsername(BOB)
                        .build())
        ));
        assertEquals(Set.of(usedGoodBookBobAuction), new HashSet<>(auctionRepository.findActiveAuctions(
                new AuctionFilter.Builder()
                        .productState(ProductState.USED_GOOD_CONDITION)
                        .exceptUsername(TOM)
                        .build())
        ));
        assertEquals(Set.of(usedBadVehicleJackAuction), new HashSet<>(auctionRepository.findActiveAuctions(
                new AuctionFilter.Builder()
                        .productState(ProductState.USED_BAD_CONDITION)
                        .exceptUsername(BOB)
                        .build())
        ));
        assertEquals(Collections.emptySet(), new HashSet<>(auctionRepository.findActiveAuctions(
                new AuctionFilter.Builder()
                        .productState(ProductState.NEW)
                        .exceptUsername(TOM)
                        .build())
        ));
        assertEquals(Collections.emptySet(), new HashSet<>(auctionRepository.findActiveAuctions(
                new AuctionFilter.Builder()
                        .productState(ProductState.USED_GOOD_CONDITION)
                        .exceptUsername(BOB)
                        .build())
        ));
        assertEquals(Collections.emptySet(), new HashSet<>(auctionRepository.findActiveAuctions(
                new AuctionFilter.Builder()
                        .productState(ProductState.USED_BAD_CONDITION)
                        .exceptUsername(JACK)
                        .build())
        ));
    }

    @Test
    void testProductCategoryAndExceptFilters() {
        assertEquals(Set.of(newClothingTomAuction), new HashSet<>(auctionRepository.findActiveAuctions(
                new AuctionFilter.Builder()
                        .productCategory(ProductCategory.CLOTHING)
                        .exceptUsername(BOB)
                        .build())
        ));
        assertEquals(Set.of(usedGoodBookBobAuction), new HashSet<>(auctionRepository.findActiveAuctions(
                new AuctionFilter.Builder()
                        .productCategory(ProductCategory.BOOKS)
                        .exceptUsername(TOM)
                        .build())
        ));
        assertEquals(Set.of(usedBadVehicleJackAuction), new HashSet<>(auctionRepository.findActiveAuctions(
                new AuctionFilter.Builder()
                        .productCategory(ProductCategory.VEHICLES)
                        .exceptUsername(BOB)
                        .build())
        ));
        assertEquals(Collections.emptySet(), new HashSet<>(auctionRepository.findActiveAuctions(
                new AuctionFilter.Builder()
                        .productCategory(ProductCategory.CLOTHING)
                        .exceptUsername(TOM)
                        .build())
        ));
        assertEquals(Collections.emptySet(), new HashSet<>(auctionRepository.findActiveAuctions(
                new AuctionFilter.Builder()
                        .productCategory(ProductCategory.BOOKS)
                        .exceptUsername(BOB)
                        .build())
        ));
        assertEquals(Collections.emptySet(), new HashSet<>(auctionRepository.findActiveAuctions(
                new AuctionFilter.Builder()
                        .productCategory(ProductCategory.VEHICLES)
                        .exceptUsername(JACK)
                        .build())
        ));
    }

    @Test
    void testAllFilters() {
        assertEquals(Set.of(newClothingTomAuction), new HashSet<>(auctionRepository.findActiveAuctions(
                new AuctionFilter.Builder()
                                .productCategory(ProductCategory.CLOTHING)
                                .productState(ProductState.NEW)
                                .exceptUsername(BOB)
                                .build())
        ));
        assertEquals(Set.of(usedGoodBookBobAuction), new HashSet<>(auctionRepository.findActiveAuctions(
                new AuctionFilter.Builder()
                        .productCategory(ProductCategory.BOOKS)
                        .productState(ProductState.USED_GOOD_CONDITION)
                        .exceptUsername(TOM)
                        .build())
        ));
        assertEquals(Set.of(usedBadVehicleJackAuction), new HashSet<>(auctionRepository.findActiveAuctions(
                new AuctionFilter.Builder()
                        .productCategory(ProductCategory.VEHICLES)
                        .productState(ProductState.USED_BAD_CONDITION)
                        .exceptUsername(BOB)
                        .build())
        ));
    }


    private Auction buildActiveAuction(final ProductState productState, final ProductCategory productCategory,
                                       final String sellerUsername, final String auctionId) {
        return Auction.builder()
                .auctionId(auctionId)
                .product(Product.builder()
                        .name("name")
                        .category(productCategory)
                        .state(productState)
                        .image("base64img")
                        .build())
                .startingPrice(BigDecimal.ONE)
                .duration(Duration.ofHours(1))
                .extensionDuration(Duration.ofHours(1))
                .sellerUsername(sellerUsername)
                .bidsHistoryId("0")
                .build();
    }
}
