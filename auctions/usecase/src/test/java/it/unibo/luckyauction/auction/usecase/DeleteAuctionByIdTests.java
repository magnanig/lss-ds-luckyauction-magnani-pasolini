/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.usecase;

import it.unibo.luckyauction.auction.domain.AuctionTestUtils;
import it.unibo.luckyauction.auction.domain.entity.Auction;
import it.unibo.luckyauction.auction.repository.memory.AuctionMemoryCollection;
import it.unibo.luckyauction.auction.repository.AuctionMongoRepository;
import it.unibo.luckyauction.auction.usecase.exception.AuctionNotAllowedException;
import it.unibo.luckyauction.auction.usecase.port.AuctionMemoryRepository;
import it.unibo.luckyauction.auction.usecase.port.AuctionRepository;
import it.unibo.luckyauction.uuid.UUIdGenerator;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import static it.unibo.luckyauction.auction.domain.AuctionTestUtils.FIRST_USERNAME;
import static it.unibo.luckyauction.auction.domain.AuctionTestUtils.SECOND_USERNAME;
import static it.unibo.luckyauction.util.NumericalConstants.TWO;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class DeleteAuctionByIdTests {
    private static final AuctionMemoryRepository AUCTION_MEMORY_REPOSITORY = new AuctionMemoryCollection();
    private static final AuctionRepository AUCTION_REPOSITORY = AuctionMongoRepository.testRepository();

    private final CreateAuction createAuction = new CreateAuction(AUCTION_REPOSITORY, AUCTION_MEMORY_REPOSITORY,
            new UUIdGenerator(), new UUIdGenerator());
    private final FindAuction findAuction = new FindAuction(AUCTION_REPOSITORY, AUCTION_MEMORY_REPOSITORY);
    private final DeleteAuction deleteAuction = new DeleteAuction(AUCTION_REPOSITORY, AUCTION_MEMORY_REPOSITORY);

    @BeforeAll
    void createUserAuctions() {
        if (!findAuction.findByUsername(FIRST_USERNAME).isEmpty()) {
            deleteUserAuction();
        }
    }

    @AfterAll
    void deleteUserAuction() {
        deleteAuction.deleteByUsername(FIRST_USERNAME);
    }

    @Test
    void deleteAuctionById() {
        final String sampleId = "sampleId";
        final Calendar twoDaysBefore = Calendar.getInstance();
        twoDaysBefore.add(Calendar.DATE, -TWO);
        final Auction endedAuctionToCreate = Auction.builder().auctionId(sampleId)
                .creationTime(twoDaysBefore).product(AuctionTestUtils.IPHONE_PRODUCT.get())
                .sellerUsername(FIRST_USERNAME).startingPrice(AuctionTestUtils.AUCTION_PRICE)
                .closingTime(Calendar.getInstance()).bidsHistoryId(AuctionTestUtils.BIDS_HISTORY_ID).build();

        createAuction.createEndedAuctionOnlyForTests(endedAuctionToCreate); // cerate auction
        assertTrue(deleteAuction.deleteById(sampleId).isPresent()); // successfully deleted
    }

    @Test
    void deleteAuctionByIdAndUsernameTest() {
        final Calendar twoDaysBefore = Calendar.getInstance();
        twoDaysBefore.add(Calendar.DATE, -TWO);
        final Auction endedAuctionToCreate = Auction.builder().auctionId(AuctionTestUtils.AUCTION_TEST_ID)
                .creationTime(twoDaysBefore).product(AuctionTestUtils.IPHONE_PRODUCT.get())
                .sellerUsername(FIRST_USERNAME).startingPrice(AuctionTestUtils.AUCTION_PRICE)
                .closingTime(Calendar.getInstance()).bidsHistoryId(AuctionTestUtils.BIDS_HISTORY_ID).build();

        final Auction endedAuction = createAuction.createEndedAuctionOnlyForTests(endedAuctionToCreate);
        assertEquals(List.of(endedAuction), findAuction.findByUsername(FIRST_USERNAME));

        final Exception wrongAuctionIdException = assertThrows(AuctionNotAllowedException.class,
                () -> deleteAuction.deleteById(AuctionTestUtils.AUCTION_TEST_ID, SECOND_USERNAME));
        assertTrue(wrongAuctionIdException.getMessage().contains("they are not the creator of such auction"));
        // delete auction by id
        assertTrue(deleteAuction.deleteById(AuctionTestUtils.AUCTION_TEST_ID, FIRST_USERNAME).isPresent());
        assertEquals(Collections.emptyList(), findAuction.findByUsername(FIRST_USERNAME));
    }
}
