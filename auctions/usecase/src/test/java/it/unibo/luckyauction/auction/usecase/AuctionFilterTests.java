/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.usecase;

import it.unibo.luckyauction.auction.domain.AuctionTestUtils;
import it.unibo.luckyauction.auction.domain.valueobject.ProductCategory;
import it.unibo.luckyauction.auction.domain.valueobject.ProductState;
import it.unibo.luckyauction.auction.usecase.port.AuctionFilter;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

class AuctionFilterTests {
    @Test
    void auctionFilterWithAllFiledTest() {
        final AuctionFilter auctionFilter = new AuctionFilter.Builder().productCategory(ProductCategory.CLOTHING)
                .productState(ProductState.NEW).exceptUsername(AuctionTestUtils.SELLER_USERNAME).build();

        assertTrue(auctionFilter.getProductCategory().isPresent());
        assertTrue(auctionFilter.getExceptUsername().isPresent());
        assertTrue(auctionFilter.getProductState().isPresent());

        assertEquals(ProductState.NEW, auctionFilter.getProductState().get());
        assertEquals(ProductCategory.CLOTHING, auctionFilter.getProductCategory().get());
        assertEquals(AuctionTestUtils.SELLER_USERNAME, auctionFilter.getExceptUsername().get());
    }

    @Test
    void auctionFilterWithoutField() {
        final AuctionFilter emptyFilter = new AuctionFilter(null, null, null);
        assertNull(emptyFilter.productCategory());
        assertNull(emptyFilter.exceptUsername());
        assertNull(emptyFilter.productState());
        assertTrue(emptyFilter.isEmpty());

        final AuctionFilter emptyFilterByBuilder = new AuctionFilter.Builder().build();
        assertFalse(emptyFilterByBuilder.getProductCategory().isPresent());
        assertFalse(emptyFilterByBuilder.getExceptUsername().isPresent());
        assertFalse(emptyFilterByBuilder.getProductState().isPresent());
        assertTrue(emptyFilterByBuilder.isEmpty());
    }
}
