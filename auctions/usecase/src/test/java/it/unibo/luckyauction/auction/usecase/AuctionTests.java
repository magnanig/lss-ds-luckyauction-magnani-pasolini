/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.usecase;

import it.unibo.luckyauction.auction.domain.AuctionTestUtils;
import it.unibo.luckyauction.auction.domain.entity.Auction;
import it.unibo.luckyauction.auction.domain.exception.AuctionLifeValidationException;
import it.unibo.luckyauction.auction.domain.valueobject.Bid;
import it.unibo.luckyauction.auction.domain.valueobject.ProductCategory;
import it.unibo.luckyauction.auction.repository.memory.AuctionMemoryCollection;
import it.unibo.luckyauction.auction.repository.AuctionMongoRepository;
import it.unibo.luckyauction.auction.usecase.exception.AuctionNotFoundException;
import it.unibo.luckyauction.auction.usecase.exception.ClosingAuctionException;
import it.unibo.luckyauction.auction.usecase.exception.InvalidBidRaiseException;
import it.unibo.luckyauction.auction.usecase.port.AuctionFilter;
import it.unibo.luckyauction.auction.usecase.port.AuctionMemoryRepository;
import it.unibo.luckyauction.auction.usecase.port.AuctionRepository;
import it.unibo.luckyauction.util.NumericalConstants;
import it.unibo.luckyauction.uuid.UUIdGenerator;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestMethodOrder;

import java.math.BigDecimal;
import java.time.Duration;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;

import static it.unibo.luckyauction.auction.domain.AuctionTestUtils.FIRST_USERNAME;
import static it.unibo.luckyauction.auction.domain.AuctionTestUtils.SECOND_USERNAME;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class AuctionTests {
    private static final String NOT_FOUND_MESSAGE = "not found";
    private static final AuctionMemoryRepository AUCTION_MEMORY_REPOSITORY = new AuctionMemoryCollection();
    private static final AuctionRepository AUCTION_REPOSITORY = AuctionMongoRepository.testRepository();

    private final CreateAuction createAuction = new CreateAuction(AUCTION_REPOSITORY, AUCTION_MEMORY_REPOSITORY,
            new UUIdGenerator(), new UUIdGenerator());
    private final FindAuction findAuction = new FindAuction(AUCTION_REPOSITORY, AUCTION_MEMORY_REPOSITORY);
    private final FindBidsHistory findBidsHistory = new FindBidsHistory(AUCTION_REPOSITORY);
    private final RaiseBid raiseBid = new RaiseBid(AUCTION_REPOSITORY, AUCTION_MEMORY_REPOSITORY);
    private final NotifyAuctionExtension notifyAuctionExtension = new NotifyAuctionExtension(AUCTION_REPOSITORY);
    private final NotifyAuctionPostponedOnBidRaise notifyAuctionPostponedOnBidRaise =
            new NotifyAuctionPostponedOnBidRaise(AUCTION_REPOSITORY);
    private final PerformOperationsPostAuctionClosing performOperationsPostAuctionClosing =
            new PerformOperationsPostAuctionClosing(AUCTION_REPOSITORY, AUCTION_MEMORY_REPOSITORY);
    private final DeleteAuction deleteAuction = new DeleteAuction(AUCTION_REPOSITORY, AUCTION_MEMORY_REPOSITORY);

    private Auction endedAuction;
    private Auction ongoingAuction;
    private final Supplier<Auction> updatedOngoingAuction =
            () -> findAuction.findById(ongoingAuction.getAuctionId()).orElseThrow();
    private final Bid bid = new Bid(AuctionTestUtils.BID_RAISE_AMOUNT, SECOND_USERNAME);

    @BeforeAll
    void createUserAuctions() {
        final Calendar twoDaysBefore = Calendar.getInstance();
        twoDaysBefore.add(Calendar.DATE, -NumericalConstants.TWO);

        final Auction endedAuctionToCreate = Auction.builder().auctionId(AuctionTestUtils.AUCTION_TEST_ID)
                .creationTime(twoDaysBefore).product(AuctionTestUtils.IPHONE_PRODUCT.get())
                .sellerUsername(FIRST_USERNAME).startingPrice(AuctionTestUtils.AUCTION_PRICE)
                .closingTime(Calendar.getInstance()).bidsHistoryId(AuctionTestUtils.BIDS_HISTORY_ID).build();

        final Auction ongoingAuctionToCreate = Auction.builder().product(AuctionTestUtils.SHOES_PRODUCT.get())
                .sellerUsername(FIRST_USERNAME).startingPrice(AuctionTestUtils.AUCTION_PRICE)
                .duration(AuctionTestUtils.ONE_DAY_DURATION).build();

        if (!findAuction.findByUsername(FIRST_USERNAME).isEmpty()) {
            deleteUserAuctions();
        }
        this.ongoingAuction = createAuction.createAndStart(ongoingAuctionToCreate);
        this.endedAuction = createAuction.createEndedAuctionOnlyForTests(endedAuctionToCreate);
    }

    @AfterAll
    void deleteUserAuctions() {
        deleteAuction.deleteByUsername(FIRST_USERNAME);
    }

    @Test @Order(1)
    void findAuctionsTest() {
        assertEquals(List.of(ongoingAuction, endedAuction), findAuction.getAllAuctions());
        assertEquals(List.of(ongoingAuction), findAuction.getAllActiveAuctions());
        assertEquals(Collections.emptyList(),
                findAuction.findActiveAuctions(new AuctionFilter(
                        ProductCategory.CD_AND_VINYLS, null, null)));
        assertEquals(AuctionTestUtils.AUCTION_PRICE, findAuction.getStartingPrice(endedAuction.getAuctionId()));
        assertEquals(Optional.empty(), findAuction.getActualBid(endedAuction.getAuctionId()));

        assertThrows(AuctionNotFoundException.class, () -> findAuction.getActualBid("wrongId"));
        assertThrows(AuctionNotFoundException.class, () -> findAuction.getStartingPrice("wrongId"));
    }

    @Test @Order(2)
    void raiseBidExceptionTest() {
        final Exception auctionNotFoundException = assertThrows(AuctionNotFoundException.class,
                () -> raiseBid.raise(ongoingAuction.getAuctionId() + "e3rt", bid));
        assertTrue(auctionNotFoundException.getMessage().contains(NOT_FOUND_MESSAGE));

        final Exception endedAuctionException = assertThrows(InvalidBidRaiseException.class,
                () -> raiseBid.raise(endedAuction.getAuctionId(), bid));
        assertTrue(endedAuctionException.getMessage().contains("You can't raise for an ended auction"));

        final Exception raiseAmountException = assertThrows(InvalidBidRaiseException.class,
                () -> raiseBid.raise(ongoingAuction.getAuctionId(), new Bid(BigDecimal.ONE, SECOND_USERNAME)));
        assertTrue(raiseAmountException.getMessage().contains("it has to be at least 1 € greater"
                + " than the current one"));

        final Exception sellerUserException = assertThrows(InvalidBidRaiseException.class,
                () -> raiseBid.raise(ongoingAuction.getAuctionId(), new Bid(AuctionTestUtils.BID_RAISE_AMOUNT,
                        FIRST_USERNAME)));
        assertTrue(sellerUserException.getMessage().contains("can't raise since he is the auction creator"));
    }

    @Test @Order(3)
    void raiseBidTest() {
        assertTrue(raiseBid.raise(ongoingAuction.getAuctionId(), bid).isEmpty(),
                "There was no bid prior to this current one");
        assertEquals(List.of(bid), findBidsHistory.getBidsHistory(ongoingAuction.getAuctionId()));

        final Exception exception = assertThrows(InvalidBidRaiseException.class,
                () -> raiseBid.raise(ongoingAuction.getAuctionId(),
                        new Bid(AuctionTestUtils.NEW_BID_RAISE_AMOUNT, SECOND_USERNAME)));
        assertTrue(exception.getMessage().contains("the last bid is already his"));
    }

    @Test @Order(4)
    void extendAuctionTest() {
        final Exception extendAuctionException = assertThrows(AuctionNotFoundException.class,
                () -> notifyAuctionExtension.notify(ongoingAuction.getAuctionId() + "e3rt",
                        Calendar.getInstance()));
        assertTrue(extendAuctionException.getMessage().contains(NOT_FOUND_MESSAGE));

        final Calendar oldExpectedClosingTime = updatedOngoingAuction.get().getExpectedClosingTime();
        final Calendar newExpectedClosingTime = (Calendar) oldExpectedClosingTime.clone();
        newExpectedClosingTime.add(Calendar.DATE, 1);

        // this operation does not change the object in memory, it only updates the object on the database
        notifyAuctionExtension.notify(ongoingAuction.getAuctionId(), newExpectedClosingTime);

        // check that the update has not affected the auction object in memory
        final Calendar updatedExpectedClosingTime = updatedOngoingAuction.get().getExpectedClosingTime();
        assertEquals(oldExpectedClosingTime, updatedExpectedClosingTime);
        assertNotEquals(newExpectedClosingTime, updatedExpectedClosingTime);
    }

    @Test @Order(NumericalConstants.FIVE)
    void updateExpectedClosingTimeTest() {
        final Calendar oldExpectedClosingTime = updatedOngoingAuction.get().getExpectedClosingTime();
        final Calendar newExpectedClosingTime = (Calendar) oldExpectedClosingTime.clone();
        newExpectedClosingTime.add(Calendar.DATE, 1);

        final Exception auctionAbsent = assertThrows(AuctionNotFoundException.class,
                () -> notifyAuctionPostponedOnBidRaise.notify("wrongId", newExpectedClosingTime));
        assertTrue(auctionAbsent.getMessage().contains(NOT_FOUND_MESSAGE));

        // this operation does not change the object in memory, it only updates the object on the database
        notifyAuctionPostponedOnBidRaise.notify(ongoingAuction.getAuctionId(), newExpectedClosingTime);

        // check that the update has not affected the auction object in memory
        final Calendar updatedExpectedClosingTime = updatedOngoingAuction.get().getExpectedClosingTime();
        assertEquals(oldExpectedClosingTime, updatedExpectedClosingTime);
        assertNotEquals(newExpectedClosingTime, updatedExpectedClosingTime);
    }

    @Test @Order(NumericalConstants.SIX)
    void operationsPostAuctionClosingExceptionTest() {
        final Exception closingTimeException = assertThrows(ClosingAuctionException.class,
                () -> performOperationsPostAuctionClosing.performAfterClosingOperations(updatedOngoingAuction.get()));
        assertTrue(closingTimeException.getMessage().contains("closing time is not set"));
    }

    @Test @Order(NumericalConstants.SEVEN)
    void createAuctionExceptionsTest() {
        final Exception maxDurationException = assertThrows(AuctionLifeValidationException.class, () ->
                createAuction.createAndStart(Auction.builder()
                        .product(AuctionTestUtils.SHOES_PRODUCT.get()).sellerUsername(FIRST_USERNAME)
                        .startingPrice(AuctionTestUtils.AUCTION_PRICE).duration(Duration.ofDays(8)).build()
                )
        );
        assertTrue(maxDurationException.getMessage().contains("Wrong duration!"));

        final Exception minDurationException = assertThrows(AuctionLifeValidationException.class, () ->
                createAuction.createAndStart(Auction.builder()
                        .product(AuctionTestUtils.SHOES_PRODUCT.get()).sellerUsername(FIRST_USERNAME)
                        .startingPrice(AuctionTestUtils.AUCTION_PRICE).duration(Duration.ofNanos(1)).build()
                )
        );
        assertTrue(minDurationException.getMessage().contains("Wrong duration!"));
    }
}
