/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.usecase;

import it.unibo.luckyauction.auction.domain.AuctionTestUtils;
import it.unibo.luckyauction.auction.domain.entity.Auction;
import it.unibo.luckyauction.auction.domain.event.AuctionDomainEvent;
import it.unibo.luckyauction.auction.repository.memory.AuctionMemoryCollection;
import it.unibo.luckyauction.auction.repository.AuctionMongoRepository;
import it.unibo.luckyauction.auction.usecase.port.AuctionMemoryRepository;
import it.unibo.luckyauction.auction.usecase.port.AuctionRepository;
import it.unibo.luckyauction.util.Sleeper;
import it.unibo.luckyauction.uuid.UUIdGenerator;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.time.Duration;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class AuctionsSyncAfterServerDownTests {
    private static final AuctionMemoryRepository AUCTION_MEMORY_REPO = new AuctionMemoryCollection();
    private static final AuctionRepository AUCTION_REPO = AuctionMongoRepository.testRepository();

    private final CreateAuction createAuction = new CreateAuction(AUCTION_REPO, AUCTION_MEMORY_REPO, 
            new UUIdGenerator(), new UUIdGenerator());
    private final FindAuction findAuction = new FindAuction(AUCTION_REPO, AUCTION_MEMORY_REPO);
    private final DeleteAuction deleteAuction = new DeleteAuction(AUCTION_REPO, AUCTION_MEMORY_REPO);
    private final SynchronizeRepositories synchronizeRepositories = new SynchronizeRepositories(AUCTION_REPO, 
            AUCTION_MEMORY_REPO);

    private Auction ongoingAuction;

    @BeforeEach
    void createAuctions() {
        final Auction ongoingAuction = Auction.builder()
                .product(AuctionTestUtils.SHOES_PRODUCT.get())
                .sellerUsername(AuctionTestUtils.FIRST_USERNAME)
                .startingPrice(AuctionTestUtils.AUCTION_PRICE)
                .duration(Duration.ofSeconds(10))
                .build();

        if (!findAuction.findByUsername(AuctionTestUtils.FIRST_USERNAME).isEmpty()) {
            deleteAuctions();
        }
        this.ongoingAuction = createAuction.createAndStart(ongoingAuction);
    }

    @AfterEach
    void deleteAuctions() {
        deleteAuction.deleteByUsername(AuctionTestUtils.FIRST_USERNAME);
    }

    @Test
    void populationSyncTest() {
        AUCTION_MEMORY_REPO.deleteById(ongoingAuction.getAuctionId()); // simulate a server down
        assertEquals(Optional.empty(), AUCTION_MEMORY_REPO.findById(ongoingAuction.getAuctionId()));
        synchronizeRepositories.synchronize();
        // now the lost auction must be properly recreated
        assertEquals(Optional.of(ongoingAuction), AUCTION_MEMORY_REPO.findById(ongoingAuction.getAuctionId()));
    }

    @Test
    void auctionSyncAfterClosureDuringServerDownTest() throws InterruptedException {
        // prepare the handler to call when auction ends
        AuctionDomainEvent.registerAuctionClosedHandler(event ->
                AUCTION_REPO.closeAuction(event.getClosedAuction().getAuctionId(),
                        event.getClosedAuction().getClosingTime().orElseThrow(),
                        event.getClosedAuction().getWinningBid().orElse(null)));

        final Duration desiredRemainingTime = Duration.ofSeconds(3);
        Thread.sleep(ongoingAuction.remainingTime().minus(desiredRemainingTime).toMillis());
        final Auction ongoingAuctionCopy = ongoingAuction;
        // simulate a server down
        ongoingAuction.cancel();
        AUCTION_MEMORY_REPO.deleteById(ongoingAuction.getAuctionId());
        ongoingAuction = null;
        // from this point, ongoingAuction will not exist anymore, since server is down
        new Sleeper().sleepEquallyMoreThan(desiredRemainingTime.toMillis());
        // now auction must be ended while server was down...
        synchronizeRepositories.synchronize();
        assertEquals(Optional.empty(), AUCTION_MEMORY_REPO.findById(ongoingAuctionCopy.getAuctionId()));
        final Auction nowEndedAuction = AUCTION_REPO.findById(ongoingAuctionCopy.getAuctionId()).orElseThrow();
        assertFalse(nowEndedAuction.isActive());
        assertTrue(nowEndedAuction.getClosingTime().isPresent());
    }
}
