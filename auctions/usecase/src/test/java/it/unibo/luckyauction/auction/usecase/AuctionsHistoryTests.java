/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.usecase;

import it.unibo.luckyauction.auction.domain.AuctionTestUtils;
import it.unibo.luckyauction.auction.domain.entity.Auction;
import it.unibo.luckyauction.auction.domain.entity.AuctionsHistory;
import it.unibo.luckyauction.auction.domain.valueobject.AwardedAuction;
import it.unibo.luckyauction.auction.domain.valueobject.Bid;
import it.unibo.luckyauction.auction.domain.valueobject.CreatedEndedAuction;
import it.unibo.luckyauction.auction.domain.valueobject.CreatedOngoingAuction;
import it.unibo.luckyauction.auction.domain.valueobject.LostAuction;
import it.unibo.luckyauction.auction.domain.valueobject.ParticipatingAuction;
import it.unibo.luckyauction.auction.repository.AuctionMongoRepository;
import it.unibo.luckyauction.auction.repository.AuctionsHistoryMongoRepository;
import it.unibo.luckyauction.auction.repository.memory.AuctionMemoryCollection;
import it.unibo.luckyauction.auction.usecase.exception.AuctionNotFoundException;
import it.unibo.luckyauction.auction.usecase.exception.AuctionsHistoryAlreadyExistsException;
import it.unibo.luckyauction.auction.usecase.exception.AuctionsHistoryNotFoundException;
import it.unibo.luckyauction.auction.usecase.port.AuctionMemoryRepository;
import it.unibo.luckyauction.auction.usecase.port.AuctionRepository;
import it.unibo.luckyauction.auction.usecase.port.AuctionsHistoryRepository;
import it.unibo.luckyauction.util.NumericalConstants;
import it.unibo.luckyauction.uuid.UUIdGenerator;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestMethodOrder;

import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import static it.unibo.luckyauction.auction.domain.AuctionTestUtils.CONTACTS;
import static it.unibo.luckyauction.auction.domain.AuctionTestUtils.FIRST_USERNAME;
import static it.unibo.luckyauction.auction.domain.AuctionTestUtils.SECOND_USERNAME;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class AuctionsHistoryTests {
    private static final AuctionMemoryRepository AUCTION_MEMORY_REPO = new AuctionMemoryCollection();
    private static final AuctionRepository AUCTION_REPO = AuctionMongoRepository.testRepository();
    private static final AuctionsHistoryRepository AUCTIONS_HISTORY_REPO =
            AuctionsHistoryMongoRepository.testRepository();

    private final CreateAuctionsHistory createAuctionsHistory = new CreateAuctionsHistory(AUCTIONS_HISTORY_REPO);
    private final FindAuctionsHistory findAuctionsHistory = new FindAuctionsHistory(AUCTIONS_HISTORY_REPO);
    private final FindUsersParticipatingInAuction
            findUserParticipatingInAuction = new FindUsersParticipatingInAuction(AUCTIONS_HISTORY_REPO);
    private final AddSummaryAuction addSummaryAuction = new AddSummaryAuction(AUCTIONS_HISTORY_REPO);
    private final RemoveSummaryOngoingAuction
            removeSummaryOngoingAuction = new RemoveSummaryOngoingAuction(AUCTIONS_HISTORY_REPO);
    private final RemoveAllSummariesForAuction
            removeAllSummariesForAuction = new RemoveAllSummariesForAuction(AUCTIONS_HISTORY_REPO);
    private final DeleteAuctionsHistory deleteAuctionsHistory = new DeleteAuctionsHistory(AUCTIONS_HISTORY_REPO);

    private final CreateAuction createAuction = new CreateAuction(AUCTION_REPO, AUCTION_MEMORY_REPO,
            new UUIdGenerator(), new UUIdGenerator());
    private final FindAuction findAuction = new FindAuction(AUCTION_REPO, AUCTION_MEMORY_REPO);
    private final RaiseBid raiseBid = new RaiseBid(AUCTION_REPO, AUCTION_MEMORY_REPO);
    private final DeleteAuction deleteAuction = new DeleteAuction(AUCTION_REPO, AUCTION_MEMORY_REPO);

    private Auction endedAuction;
    private Auction ongoingAuction;

    @BeforeAll
    void createObjects() {
        addAuctionsHistoryToDB();
        // before creating the auctions and inserting them in the history,
        // the auction history must already be present
        if (!findAuction.findByUsername(FIRST_USERNAME).isEmpty()) {
            deleteAuction.deleteByUsername(FIRST_USERNAME);
        }
        addEndedAuctionToDB(); // only for test
        addOngoingAuctionToDB();
    }

    @AfterAll
    void deleteObjects() {
        deleteAuctionsHistoryFromDB();
        deleteAuction.deleteByUsername(FIRST_USERNAME);
    }

    @Test
    @Order(1)
    void createAuctionsHistoryExceptionTest() {
        final Exception createAuctionsHistoryException = assertThrows(AuctionsHistoryAlreadyExistsException.class,
                () -> createAuctionsHistory.create(new AuctionsHistory(AuctionTestUtils.FIRST_AUCTIONS_HISTORY_ID,
                        FIRST_USERNAME)));
        assertTrue(createAuctionsHistoryException.getMessage().contains("already exists"));
    }

    @Test
    @Order(2)
    void addSummaryAuctionExceptionTest() {
        final String wrongAuctionId = AuctionTestUtils.AUCTION_TEST_ID + "bs3e";
        final String wrongUsername = FIRST_USERNAME + "xY";

        final Exception auctionNotFoundException = assertThrows(AuctionNotFoundException.class,
                () -> addSummaryAuction.addCreatedOngoingAuction(wrongAuctionId, FIRST_USERNAME));
        assertTrue(auctionNotFoundException.getMessage().contains("Auction " + wrongAuctionId + " not found"));

        final Exception auctionsHistoryNotFoundException = assertThrows(AuctionsHistoryNotFoundException.class,
                () -> addSummaryAuction.addCreatedOngoingAuction(endedAuction.getAuctionId(), wrongUsername));
        assertTrue(auctionsHistoryNotFoundException.getMessage().contains("Auctions history not found"));
    }

    @Test
    @Order(3)
    void findAuctionTest() {
        // check for endedAuction
        assertTrue(findAuctionsHistory.getCreatedEndedAuction(endedAuction.getAuctionId(), FIRST_USERNAME)
                .isPresent(), "the endedAuction must be in the created ended auctions of the FIRST user");
        assertTrue(findAuctionsHistory.getLostAuction(endedAuction.getAuctionId(), SECOND_USERNAME)
                .isPresent(), "the endedAuction must be in the lost auctions of the SECOND user");
        assertTrue(findAuctionsHistory.getAwardedAuction(endedAuction.getAuctionId(), AuctionTestUtils.THIRD_USERNAME)
                .isPresent(), "the endedAuction must be in the awarded auctions of the THIRD user");
        // check for ongoingAuction
        assertTrue(findAuctionsHistory.getCreatedOngoingAuction(ongoingAuction.getAuctionId(), FIRST_USERNAME)
                .isPresent(), "the ongoingAuction must be in the created ongoing auctions of the FIRST user");
        assertTrue(findAuctionsHistory.getParticipatingAuction(ongoingAuction.getAuctionId(), SECOND_USERNAME)
                .isPresent(), "the ongoingAuction must be in the participating auctions of the SECOND user");
    }

    @Test
    @Order(4)
    void findAuctionsTest() { // checks similar to the previous test
        // check for endedAuction
        assertEquals(List.of(CreatedEndedAuction.fromAuctionWithWinner(endedAuction, CONTACTS.get(), CONTACTS.get())),
                findAuctionsHistory.getCreatedEndedAuctions(FIRST_USERNAME));
        assertEquals(List.of(LostAuction.fromAuction(endedAuction)),
                findAuctionsHistory.getLostAuctions(SECOND_USERNAME));
        assertEquals(List.of(AwardedAuction.fromAuction(endedAuction, CONTACTS.get(), CONTACTS.get())),
                findAuctionsHistory.getAwardedAuctions(AuctionTestUtils.THIRD_USERNAME));
        // check for ongoingAuction
        assertEquals(List.of(CreatedOngoingAuction.fromAuction(ongoingAuction)),
                findAuctionsHistory.getCreatedOngoingAuctions(FIRST_USERNAME));
        assertEquals(List.of(ParticipatingAuction.fromAuction(ongoingAuction)),
                findAuctionsHistory.getParticipatingAuctions(SECOND_USERNAME));
    }

    @Test
    @Order(NumericalConstants.FIVE)
    void findAuctionsHistoryFirstTest() {
        // check the auction history of the FIRST_USERNAME user
        final AuctionsHistory auctionsHistory = findAuctionsHistory.findByUsername(FIRST_USERNAME).orElseThrow();
        assertNotNull(auctionsHistory);

        assertEquals(List.of(CreatedEndedAuction.fromAuctionWithWinner(endedAuction, CONTACTS.get(),
                CONTACTS.get())), auctionsHistory.getCreatedEndedAuctions());
        assertEquals(List.of(CreatedOngoingAuction.fromAuction(ongoingAuction)),
                auctionsHistory.getCreatedOngoingAuctions());

        assertTrue(auctionsHistory.getParticipatingAuctions().isEmpty());
        assertTrue(auctionsHistory.getAwardedAuctions().isEmpty());
        assertTrue(auctionsHistory.getLostAuctions().isEmpty());
    }

    /*
    Current situation

    EndedAuction: FIRST_USERNAME is the seller user         OngoingAuction: FIRST_USERNAME is the seller user
        - firstBid --> SECOND_USERNAME                          - firstBid --> SECOND_USERNAME
        - winningBid --> THIRD_USERNAME
     */

    @Test
    @Order(NumericalConstants.SIX)
    void findAuctionsHistorySecondTest() {
        // check the auction history of the SECOND_USERNAME user
        final AuctionsHistory auctionsHistory = findAuctionsHistory.findByUsername(SECOND_USERNAME).orElseThrow();
        assertNotNull(auctionsHistory);

        assertEquals(List.of(LostAuction.fromAuction(endedAuction)), auctionsHistory.getLostAuctions());
        assertEquals(List.of(ParticipatingAuction.fromAuction(ongoingAuction)),
                auctionsHistory.getParticipatingAuctions());

        assertTrue(auctionsHistory.getCreatedOngoingAuctions().isEmpty());
        assertTrue(auctionsHistory.getCreatedEndedAuctions().isEmpty());
        assertTrue(auctionsHistory.getAwardedAuctions().isEmpty());
    }

    @Test
    @Order(NumericalConstants.SEVEN)
    void findUserParticipatingInAuctionTest() {
        assertEquals(List.of(SECOND_USERNAME),
                findUserParticipatingInAuction.findByAuctionId(ongoingAuction.getAuctionId()));

        assertEquals(Collections.emptyList(),
                findUserParticipatingInAuction.findByAuctionId(endedAuction.getAuctionId()));
    }

    @Test
    @Order(NumericalConstants.EIGHT)
    void removeSummaryOngoingAuctionTest() {
        removeSummaryOngoingAuction.removeParticipatingAuction(ongoingAuction.getAuctionId(), SECOND_USERNAME);
        final AuctionsHistory secondAuctionsHistory = findAuctionsHistory.findByUsername(SECOND_USERNAME).orElseThrow();
        assertNotNull(secondAuctionsHistory);
        assertTrue(secondAuctionsHistory.getParticipatingAuctions().isEmpty());

        removeSummaryOngoingAuction.removeCreatedOngoingAuction(ongoingAuction.getAuctionId(), FIRST_USERNAME);
        final AuctionsHistory firstAuctionsHistory = findAuctionsHistory.findByUsername(FIRST_USERNAME).orElseThrow();
        assertNotNull(firstAuctionsHistory);
        assertTrue(firstAuctionsHistory.getCreatedOngoingAuctions().isEmpty());
    }

    @Test
    @Order(NumericalConstants.NINE)
    void removeAllSummariesForAuctionTest() {
        // from the previous tests, the "endedAuction" auction has been remained in the following auction histories
        //      (1) FIRST_USERNAME  --> CreatedEndedAuction
        //      (2) SECOND_USERNAME --> LostAuction

        removeAllSummariesForAuction.removeAuction(endedAuction.getAuctionId());

        final AuctionsHistory firstAuctionsHistory = findAuctionsHistory.findByUsername(FIRST_USERNAME).orElseThrow();
        assertTrue(firstAuctionsHistory.getCreatedOngoingAuctions().isEmpty());

        final AuctionsHistory secondAuctionsHistory = findAuctionsHistory.findByUsername(SECOND_USERNAME).orElseThrow();
        assertTrue(secondAuctionsHistory.getParticipatingAuctions().isEmpty());
    }

    private void addEndedAuctionToDB() {
        final Calendar twoDaysBefore = Calendar.getInstance();
        twoDaysBefore.add(Calendar.DATE, -NumericalConstants.TWO);
        final Bid bid = new Bid(AuctionTestUtils.BID_RAISE_AMOUNT, SECOND_USERNAME);
        final Bid winningBid = new Bid(AuctionTestUtils.NEW_BID_RAISE_AMOUNT, AuctionTestUtils.THIRD_USERNAME);
        // add the ended auction of the seller FIRST_USERNAME user
        final Auction endedAuctionToCreate = Auction.builder().auctionId(AuctionTestUtils.AUCTION_TEST_ID)
                .creationTime(twoDaysBefore).product(AuctionTestUtils.IPHONE_PRODUCT.get())
                .sellerUsername(FIRST_USERNAME).startingPrice(AuctionTestUtils.AUCTION_PRICE).winningBid(winningBid)
                .closingTime(Calendar.getInstance()).bidsHistoryId(AuctionTestUtils.BIDS_HISTORY_ID)
                .bids(List.of(bid, winningBid)).build();
        endedAuction = createAuction.createEndedAuctionOnlyForTests(endedAuctionToCreate);
        assertEquals(endedAuction.getAuctionId(), endedAuctionToCreate.getAuctionId());
        // add the ID of the auctions in the respective lists of the AuctionsHistory object
        addSummaryAuction.addCreatedEndedAuction(endedAuction.getAuctionId(), CONTACTS.get(),
                CONTACTS.get(), FIRST_USERNAME);
        addSummaryAuction.addAwardedAuction(endedAuction.getAuctionId(), CONTACTS.get(),
                CONTACTS.get(), AuctionTestUtils.THIRD_USERNAME);
        addSummaryAuction.addLostAuction(endedAuction.getAuctionId(), SECOND_USERNAME);
    }

    private void addOngoingAuctionToDB() {
        final Bid bid = new Bid(AuctionTestUtils.BID_RAISE_AMOUNT, SECOND_USERNAME);

        final Calendar twoDaysBefore = Calendar.getInstance();
        twoDaysBefore.add(Calendar.DATE, -NumericalConstants.TWO);
        // add the ongoing auction of the seller FIRST_USERNAME user
        final Auction ongoingAuctionToCreate = Auction.builder().product(AuctionTestUtils.SHOES_PRODUCT.get())
                .sellerUsername(FIRST_USERNAME).startingPrice(AuctionTestUtils.AUCTION_PRICE)
                .duration(AuctionTestUtils.TWO_DAYS_DURATION).build();
        ongoingAuction = createAuction.createAndStart(ongoingAuctionToCreate);
        assertNotNull(ongoingAuction, "newAuctionToCreate has been created");
        // add a raise to the ongoing auction by user SECOND_USERNAME
        raiseBid.raise(ongoingAuction.getAuctionId(), bid);
        // add the ID of the auctions in the respective lists of the AuctionsHistory object
        addSummaryAuction.addCreatedOngoingAuction(ongoingAuction.getAuctionId(), FIRST_USERNAME);
        addSummaryAuction.addParticipatingAuction(ongoingAuction.getAuctionId(), SECOND_USERNAME);
    }

    private void addAuctionsHistoryToDB() {
        addAuctionHistory(AuctionTestUtils.FIRST_AUCTIONS_HISTORY_ID, FIRST_USERNAME);
        addAuctionHistory(AuctionTestUtils.SECOND_AUCTIONS_HISTORY_ID, SECOND_USERNAME);
        addAuctionHistory(AuctionTestUtils.THIRD_AUCTIONS_HISTORY_ID, AuctionTestUtils.THIRD_USERNAME);
    }

    private void addAuctionHistory(final String auctionsHistoryId, final String username) {
        if (findAuctionsHistory.findByUsername(username).isPresent()) {
            deleteAuctionsHistory.deleteByUsername(username);
        }
        createAuctionsHistory.create(new AuctionsHistory(auctionsHistoryId, username));
    }

    private void deleteAuctionsHistoryFromDB() {
        deleteAuctionsHistory.deleteByUsername(FIRST_USERNAME);
        deleteAuctionsHistory.deleteByUsername(SECOND_USERNAME);
        deleteAuctionsHistory.deleteByUsername(AuctionTestUtils.THIRD_USERNAME);
    }
}
