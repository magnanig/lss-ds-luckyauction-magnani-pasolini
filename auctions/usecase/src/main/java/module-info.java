module lss.ds.luckyauction.magnani.pasolini.auctions.usecase.main {
    exports it.unibo.luckyauction.auction.usecase.port;
    exports it.unibo.luckyauction.auction.usecase.exception;
    exports it.unibo.luckyauction.auction.usecase;
    requires lss.ds.luckyauction.magnani.pasolini.utils.main;
    requires lss.ds.luckyauction.magnani.pasolini.auctions.domain.main;
}