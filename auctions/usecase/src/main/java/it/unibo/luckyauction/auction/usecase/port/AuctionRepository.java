/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.usecase.port;


import it.unibo.luckyauction.auction.domain.entity.Auction;
import it.unibo.luckyauction.auction.domain.valueobject.Bid;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

public interface AuctionRepository {

    /**
     * Create a new auction.
     * @param auction   the auction to create
     */
    void create(Auction auction);

    /**
     * Delete an auction by its id.
     * @param auctionId     the id of the auction to remove
     * @return              the corresponding auction, if present
     */
    Optional<Auction> deleteById(String auctionId);

    /**
     * Delete all auctions of a specific seller user.
     * @param sellerUsername    the desired seller username
     */
    void deleteByUsername(String sellerUsername);

    /**
     * Find an auction by its id.
     * @param auctionId     the desired auction id
     * @return              the corresponding auction, if present
     */
    Optional<Auction> findById(String auctionId);

    /**
     * Get auctions list of a specific username.
     * @param sellerUsername    the desired seller username
     * @return                  the list of all his auctions
     */
    List<Auction> findByUsername(String sellerUsername);

    /**
     * Get the list of all auctions.
     * @return  the list of all auctions
     */
    List<Auction> getAllAuctions();

    /**
     * Get the list of all active auctions.
     * @return  the list of all active auctions
     */
    List<Auction> getAllActiveAuctions();

    /**
     * Get the auctions matching the specified filters.
     * @param auctionFilter     the desired filters
     * @return                  the list of auctions matching such filter
     */
    List<Auction> findActiveAuctions(AuctionFilter auctionFilter);

    /**
     * Get the actual bid for the specified auction.
     * @param auctionId the auction id
     * @return          the actual bid, if any
     */
    Optional<Bid> getActualBidByAuctionId(String auctionId);

    /**
     * Get the starting price of the specified auction.
     * @param auctionId the auction id
     * @return          the starting price
     */
    BigDecimal getStartingPriceByAuctionId(String auctionId);

    /**
     * Add a new bid for the specified auction.
     * @param auctionId     the id of the auction to which add the bid
     * @param bid           the bid to add
     */
    void addNewBid(String auctionId, Bid bid);

    /**
     * Get all the bids raised for the specified auction.
     * @param auctionId     the desired auction id
     * @return              the list of bids raised for such auction
     */
    List<Bid> getBidsHistory(String auctionId);

    /**
     * Update the expected closing time for the specified auction.
     * @param auctionId                 the id of the auction to update
     * @param newExpectedClosingTime    the new expected closing time for such auction
     */
    void updateExpectedClosingTime(String auctionId, Calendar newExpectedClosingTime);

    /**
     * Extend the specified auction, updating its expected closing time.
     * @param auctionId                 the id of the auction to update
     * @param newExpectedClosingTime    the new expected closing time for such auction
     */
    void extendAuction(String auctionId, Calendar newExpectedClosingTime);

    /**
     * Close the specified auction, by setting its closing time and winning bid.
     * @param auctionId     the id of the auction to close
     * @param closingTime   the timestamp at which auction has closed
     * @param winningBid    the winning bid
     */
    void closeAuction(String auctionId, Calendar closingTime, Bid winningBid);
}
