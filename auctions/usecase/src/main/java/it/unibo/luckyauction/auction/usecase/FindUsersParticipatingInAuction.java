/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.usecase;

import it.unibo.luckyauction.auction.usecase.port.AuctionsHistoryRepository;

import java.util.List;

/**
 * The class to manage the search of users participating in auctions.
 */
public class FindUsersParticipatingInAuction {

    private final AuctionsHistoryRepository auctionsHistoryRepository;

    /**
     * Create a new instance to use in order to search users participating in auctions.
     * @param auctionsHistoryRepository     the auctions' history repository
     *                                      (i.e. a database of auctions histories)
     */
    public FindUsersParticipatingInAuction(final AuctionsHistoryRepository auctionsHistoryRepository) {
        this.auctionsHistoryRepository = auctionsHistoryRepository;
    }

    /**
     * Get the list of the user participating in the specified auction.
     * @param auctionId     the desired auction id
     * @return              the list of the username of the user participating
     *                      in the specified auction
     */
    public List<String> findByAuctionId(final String auctionId) {
        return auctionsHistoryRepository.getUsersParticipatingInAuction(auctionId);
    }
}
