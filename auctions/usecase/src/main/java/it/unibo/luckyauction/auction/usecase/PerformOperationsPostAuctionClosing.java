/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.usecase;

import it.unibo.luckyauction.auction.domain.entity.AbstractAuctionWithLife;
import it.unibo.luckyauction.auction.usecase.exception.ClosingAuctionException;
import it.unibo.luckyauction.auction.usecase.port.AuctionMemoryRepository;
import it.unibo.luckyauction.auction.usecase.port.AuctionRepository;

/**
 * The class to execute the needed operations after auction closing.
 */
public class PerformOperationsPostAuctionClosing {

    private final AuctionRepository auctionRepository;
    private final AuctionMemoryRepository auctionMemoryRepository;

    /**
     * Create a new instance to use in order to close auctions.
     * @param auctionRepository         the auctions' repository (i.e. a database of auctions)
     * @param auctionMemoryRepository   the in-memory auction repository
     */
    public PerformOperationsPostAuctionClosing(final AuctionRepository auctionRepository,
                                               final AuctionMemoryRepository auctionMemoryRepository) {
        this.auctionRepository = auctionRepository;
        this.auctionMemoryRepository = auctionMemoryRepository;
    }

    /**
     * Perform all needed operation after the specified auction has closed,
     * i.e. save information into repository.
     * @param closedAuction     the just closed auction
     * @throws ClosingAuctionException  if closing time is not set in closedAuction
     */
    public void performAfterClosingOperations(final AbstractAuctionWithLife closedAuction) {
        auctionRepository.closeAuction(
                closedAuction.getAuctionId(),
                closedAuction.getClosingTime()
                        .orElseThrow(() -> new ClosingAuctionException("closing time is not set")),
                closedAuction.getWinningBid().orElse(null)
        );
        auctionMemoryRepository.deleteById(closedAuction.getAuctionId());
    }
}
