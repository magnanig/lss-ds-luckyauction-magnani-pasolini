/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.usecase.port;

import it.unibo.luckyauction.auction.domain.entity.Auction;
import it.unibo.luckyauction.auction.domain.valueobject.Bid;

import java.util.List;
import java.util.Optional;

public interface AuctionMemoryRepository {

    /**
     * Create a new auction.
     * @param auction   the auction to create
     */
    void create(Auction auction);

    /**
     * Delete an auction by its id.
     * @param auctionId     the id of the auction to remove
     * @return              the corresponding auction, if present
     */
    Optional<Auction> deleteById(String auctionId);

    /**
     * Delete all auctions of a specific seller user.
     * @param sellerUsername    the desired seller username
     * @return                  the list of just removed auctions
     */
    List<Auction> deleteByUsername(String sellerUsername);

    /**
     * Find an auction by its id.
     * @param auctionId     the desired auction id
     * @return              the corresponding auction, if present
     */
    Optional<Auction> findById(String auctionId);

    /**
     * Get auctions list of a specific username.
     * @param sellerUsername    the desired seller username
     * @return                  the list of all his auctions
     */
    List<Auction> findByUsername(String sellerUsername);

    /**
     * Get the list of all auctions.
     * @return  the list of all auctions
     */
    List<Auction> getAllAuctions();

    /**
     * Get all the bids raised for the specified auction.
     * @param auctionId     the desired auction id
     * @return              the list of bids raised for such auction
     */
    List<Bid> getBidsHistory(String auctionId);

}
