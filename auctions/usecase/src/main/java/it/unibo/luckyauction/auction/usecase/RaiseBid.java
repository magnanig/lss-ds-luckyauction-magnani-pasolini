/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.usecase;

import it.unibo.luckyauction.auction.domain.entity.Auction;
import it.unibo.luckyauction.auction.domain.service.AuctionService;
import it.unibo.luckyauction.auction.domain.valueobject.Bid;
import it.unibo.luckyauction.auction.usecase.exception.AuctionNotFoundException;
import it.unibo.luckyauction.auction.usecase.exception.InvalidBidRaiseException;
import it.unibo.luckyauction.auction.usecase.port.AuctionMemoryRepository;
import it.unibo.luckyauction.auction.usecase.port.AuctionRepository;

import java.util.Optional;

/**
 * The class to manage the bids raise.
 */
public class RaiseBid {

    private final AuctionRepository auctionRepository;
    private final AuctionMemoryRepository auctionMemoryRepository;

    /**
     * Create a new instance to use in order to raise bids for the auctions.
     * @param auctionRepository         the auction repository (i.e. a database of auctions)
     * @param auctionMemoryRepository   the in-memory auction repository
     */
    public RaiseBid(final AuctionRepository auctionRepository, final AuctionMemoryRepository auctionMemoryRepository) {
        this.auctionRepository = auctionRepository;
        this.auctionMemoryRepository = auctionMemoryRepository;
    }

    /**
     * Raise a new bid for the specified auction.
     * @param auctionId                     the id of the auction to which raise bid
     * @param raisedBid                     the raised bid
     * @return                              the last bid before the current one, if any
     * @throws AuctionNotFoundException     if no auction with the specified id exists
     * @throws InvalidBidRaiseException     if the specified bid is not valid for user, i.e. if the user is the seller,
     *                                      if the previous offer was made by the same user or if the new offer does not
     *                                      exceed the actual one by at least € 1
     */
    public Optional<Bid> raise(final String auctionId, final Bid raisedBid)
            throws AuctionNotFoundException, InvalidBidRaiseException {
        final Auction auction = new FindAuction(auctionRepository, auctionMemoryRepository).findById(auctionId)
                        .orElseThrow(() -> new AuctionNotFoundException(auctionId));
        if (!auction.isActive()) {
            throw new InvalidBidRaiseException("You can't raise for an ended auction");
        }
        if (!AuctionService.userCanRaise(raisedBid.getBidderUsername(), auction)) {
            throw new InvalidBidRaiseException("User " + raisedBid.getBidderUsername()
                    + " can't raise since he is the auction creator or the last bid is already his");
        }
        if (!AuctionService.isValidBidAmount(auction, raisedBid)) {
            throw new InvalidBidRaiseException("Current bid " + raisedBid
                    + " is not valid: it has to be at least 1 € greater than the current one");
        }
        final Optional<Bid> oldBid = auction.getActualBid();
        auction.addNewBid(raisedBid);
        auctionRepository.addNewBid(auction.getAuctionId(), raisedBid);
        return oldBid;
    }

}
