/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.usecase;

import it.unibo.luckyauction.auction.usecase.port.AuctionsHistoryRepository;

import java.util.Optional;

/**
 * The class to delete auctions histories.
 */
public class DeleteAuctionsHistory {

    private final AuctionsHistoryRepository auctionsHistoryRepository;

    /**
     * Create a new instance to use in order to delete auctions histories.
     * @param auctionsHistoryRepository     the auctions' history repository
     *                                      (i.e. a database of auctions histories)
     */
    public DeleteAuctionsHistory(final AuctionsHistoryRepository auctionsHistoryRepository) {
        this.auctionsHistoryRepository = auctionsHistoryRepository;
    }

    /**
     * Remove the auctions' history of the specified user.
     * @param username  the username of the user whose auctions' history
     *                  has to be removed
     * @return          the id of the just deleted auctions' history, if any
     */
    public Optional<String> deleteByUsername(final String username) {
        return auctionsHistoryRepository.deleteByUsername(username);
    }
}
