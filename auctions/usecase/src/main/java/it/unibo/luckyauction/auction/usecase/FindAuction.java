/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.usecase;

import it.unibo.luckyauction.auction.domain.entity.Auction;
import it.unibo.luckyauction.auction.domain.valueobject.Bid;
import it.unibo.luckyauction.auction.usecase.exception.AuctionNotFoundException;
import it.unibo.luckyauction.auction.usecase.port.AuctionFilter;
import it.unibo.luckyauction.auction.usecase.port.AuctionMemoryRepository;
import it.unibo.luckyauction.auction.usecase.port.AuctionRepository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

/**
 * The class to manage the search of an auction.
 */
public class FindAuction {

    private final AuctionRepository auctionRepository;
    private final AuctionMemoryRepository auctionMemoryRepository;

    /**
     * Create a new instance to use in order to create new auctions.
     * @param auctionRepository         the auctions' repository (i.e. a database of auctions)
     * @param auctionMemoryRepository   the in-memory auction repository
     */
    public FindAuction(final AuctionRepository auctionRepository,
                       final AuctionMemoryRepository auctionMemoryRepository) {
        this.auctionRepository = auctionRepository;
        this.auctionMemoryRepository = auctionMemoryRepository;
    }

    /**
     * Find an auction by its id. If the auction is actually ongoing,
     * a reference to the original object will be retrieved, otherwise
     * its copy.
     * @param auctionId     the desired auction id
     * @return              the corresponding auction, if present
     */
    public Optional<Auction> findById(final String auctionId) {
        return auctionMemoryRepository.findById(auctionId)
                .or(() -> auctionRepository.findById(auctionId));
    }

    /**
     * Get the auctions list for a specific username. The retrieved
     * auctions are a copy of the original objects.
     * @param sellerUsername    the desired seller username
     * @return                  the list of all their auctions
     */
    public List<Auction> findByUsername(final String sellerUsername) {
        return auctionRepository.findByUsername(sellerUsername);
    }

    /**
     * Get the list of all auctions. The retrieved auctions are a
     * copy of the original objects.
     * @return  the list of all auctions
     */
    public List<Auction> getAllAuctions() {
        return auctionRepository.getAllAuctions();
    }

    /**
     * Get the list of all active auctions.
     * @return  the list of all active auctions
     */
    public List<Auction> getAllActiveAuctions() {
        return auctionRepository.getAllActiveAuctions();
    }

    /**
     * Get the active auctions matching the specified filters.
     * @param auctionFilter     the desired filters
     * @return                  the list of active auctions matching such filter
     */
    public List<Auction> findActiveAuctions(final AuctionFilter auctionFilter) {
        return auctionRepository.findActiveAuctions(auctionFilter);
    }

    /**
     * Get the actual bid for the specified auction.
     * @param auctionId the auction id
     * @return          the actual bid, if any
     * @throws AuctionNotFoundException if no auction with the specified id exists
     */
    public Optional<Bid> getActualBid(final String auctionId) {
        return findById(auctionId)
                .orElseThrow(() -> new AuctionNotFoundException(auctionId))
                .getActualBid();
    }

    /**
     * Get the starting price of the specified auction.
     * @param auctionId the auction id
     * @return          the starting price
     * @throws AuctionNotFoundException if no auction with the specified id exists
     */
    public BigDecimal getStartingPrice(final String auctionId) {
        return findById(auctionId)
                .orElseThrow(() -> new AuctionNotFoundException(auctionId))
                .getStartingPrice();
    }

}
