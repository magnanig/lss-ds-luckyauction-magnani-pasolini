/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.usecase;

import it.unibo.luckyauction.auction.domain.entity.Auction;
import it.unibo.luckyauction.auction.domain.exception.AuctionLifeValidationException;
import it.unibo.luckyauction.auction.usecase.port.AuctionMemoryRepository;
import it.unibo.luckyauction.auction.usecase.port.AuctionRepository;
import it.unibo.luckyauction.util.port.IdGenerator;

import java.time.Duration;

import static it.unibo.luckyauction.auction.domain.config.AuctionConstants.getMaxAuctionDuration;
import static it.unibo.luckyauction.auction.domain.config.AuctionConstants.getMinAuctionDuration;

/**
 * The class to create auctions.
 */
public class CreateAuction {

    private final AuctionRepository auctionRepository;
    private final AuctionMemoryRepository auctionMemoryRepository;
    private final IdGenerator auctionIdGenerator;
    private final IdGenerator bidsHistoryIdGenerator;

    /**
     * Create a new instance to use in order to create new transactions.
     * @param auctionRepository         the auctions' repository (i.e. a database of
     *                                  movements histories)
     * @param auctionMemoryRepository   the in-memory auction repository
     * @param auctionIdGenerator        the generator of auction ids
     * @param bidsHistoryIdGenerator    the generator of bids history ids
     */
    public CreateAuction(final AuctionRepository auctionRepository,
                         final AuctionMemoryRepository auctionMemoryRepository, final IdGenerator auctionIdGenerator,
                         final IdGenerator bidsHistoryIdGenerator) {
        this.auctionRepository = auctionRepository;
        this.auctionMemoryRepository = auctionMemoryRepository;
        this.auctionIdGenerator = auctionIdGenerator;
        this.bidsHistoryIdGenerator = bidsHistoryIdGenerator;
    }

    /**
     * Create and start a new auction.
     * @param auction   the auction to create
     * @return          the just created auction, already started
     */
    public Auction createAndStart(final Auction auction) {
        checkValidDuration(auction.getDuration());
        final Auction newAuction = Auction.builder()
                .auctionId(auctionIdGenerator.generate())
                .product(auction.getProduct())
                .sellerUsername(auction.getSellerUsername())
                .creationTime(auction.getCreationTime())
                .duration(auction.getDuration())
                .startingPrice(auction.getStartingPrice())
                .extensionDuration(auction.getExtensionAmount().orElse(null))
                .bidsHistoryId(bidsHistoryIdGenerator.generate())
                .build();

        newAuction.start();
        auctionMemoryRepository.create(newAuction);
        auctionRepository.create(newAuction);
        return newAuction;
    }

    /**
     * This method should <STRONG>only be used within test classes</STRONG>.
     * Add an ended auction to the database.
     * @param endedAuction  the auction to add
     * @return              the just added auction
     */
    public Auction createEndedAuctionOnlyForTests(final Auction endedAuction) {
        final Auction auction = Auction.builder()
                .auctionId(endedAuction.getAuctionId())
                .product(endedAuction.getProduct())
                .sellerUsername(endedAuction.getSellerUsername())
                .creationTime(endedAuction.getCreationTime())
                .startingPrice(endedAuction.getStartingPrice())
                .actualBid(endedAuction.getActualBid().orElse(null))
                .duration(endedAuction.getDuration())
                .extensionDuration(endedAuction.getExtensionAmount().orElse(null))
                .expectedClosingTime(endedAuction.getExpectedClosingTime())
                .winningBid(endedAuction.getWinningBid().orElse(null))
                .closingTime(endedAuction.getClosingTime().orElse(null))
                .bidsHistoryId(endedAuction.getBidsHistoryId())
                .bids(endedAuction.getBids())
                .build();
        auctionRepository.create(auction);
        return auction;
    }

    private void checkValidDuration(final Duration duration) {
        if (duration != null && (duration.compareTo(getMaxAuctionDuration()) > 0
                || duration.compareTo(getMinAuctionDuration()) < 0)) {
            throw new AuctionLifeValidationException("Wrong duration! The auction duration "
                    + "times must be between " + getMinAuctionDuration().toHours() + " hours and "
                    + getMaxAuctionDuration().toDays() + " days");
        }
    }
}
