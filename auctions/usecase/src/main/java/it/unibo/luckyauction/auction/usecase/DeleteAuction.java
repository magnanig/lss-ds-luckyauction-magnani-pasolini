/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.usecase;

import it.unibo.luckyauction.auction.domain.entity.Auction;
import it.unibo.luckyauction.auction.usecase.exception.AuctionNotAllowedException;
import it.unibo.luckyauction.auction.usecase.port.AuctionMemoryRepository;
import it.unibo.luckyauction.auction.usecase.port.AuctionRepository;

import java.util.List;
import java.util.Optional;

/**
 * The class to delete auctions.
 */
public class DeleteAuction {

    private final AuctionRepository auctionRepository;
    private final AuctionMemoryRepository auctionMemoryRepository;

    /**
     * Create a new instance to use in order to delete auctions.
     * @param auctionRepository         the auction repository (i.e. a database of
     *                                  auctions)
     * @param auctionMemoryRepository   the in-memory auction repository
     */
    public DeleteAuction(final AuctionRepository auctionRepository,
                         final AuctionMemoryRepository auctionMemoryRepository) {
        this.auctionRepository = auctionRepository;
        this.auctionMemoryRepository = auctionMemoryRepository;
    }

    /**
     * Delete a specific user's auction.
     * @param auctionId the desired auction id to delete
     * @param username  the desired username
     * @return          the just deleted auction, if existed
     * @throws AuctionNotAllowedException   if the username does not match with the auction seller
     */
    public Optional<Auction> deleteById(final String auctionId, final String username)
            throws AuctionNotAllowedException {
        if (auctionRepository.findById(auctionId).map(auction ->
                !auction.getSellerUsername().equals(username)).orElse(false)) {
            throw new AuctionNotAllowedException(auctionId, username);
        }
        auctionMemoryRepository.deleteById(auctionId).ifPresent(Auction::cancel);
        return auctionRepository.deleteById(auctionId);
    }

    /**
     * Delete a specific auction.
     * @param auctionId the desired auction id to delete
     * @return          the just deleted auction, if existed
     */
    public Optional<Auction> deleteById(final String auctionId) {
        auctionMemoryRepository.deleteById(auctionId).ifPresent(Auction::cancel);
        return auctionRepository.deleteById(auctionId);
    }

    /**
     * Delete all auctions of a specific user.
     * @param sellerUsername  the desired seller username (i.e. the
     *                        creator of the auction)
     */
    public List<Auction> deleteByUsername(final String sellerUsername) {
        final List<Auction> removedAuctions = auctionRepository.findByUsername(sellerUsername);
        auctionMemoryRepository.deleteByUsername(sellerUsername).forEach(Auction::cancel);
        auctionRepository.deleteByUsername(sellerUsername);
        return removedAuctions;
    }
}
