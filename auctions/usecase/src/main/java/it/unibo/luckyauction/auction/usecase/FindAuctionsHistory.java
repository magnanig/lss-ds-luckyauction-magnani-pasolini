/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.usecase;

import it.unibo.luckyauction.auction.domain.entity.AuctionsHistory;
import it.unibo.luckyauction.auction.domain.valueobject.AwardedAuction;
import it.unibo.luckyauction.auction.domain.valueobject.CreatedEndedAuction;
import it.unibo.luckyauction.auction.domain.valueobject.CreatedOngoingAuction;
import it.unibo.luckyauction.auction.domain.valueobject.LostAuction;
import it.unibo.luckyauction.auction.domain.valueobject.ParticipatingAuction;
import it.unibo.luckyauction.auction.usecase.port.AuctionsHistoryRepository;

import java.util.List;
import java.util.Optional;

/**
 * The class to manage the search of an auctions' history.
 */
public class FindAuctionsHistory {

    private final AuctionsHistoryRepository auctionsHistoryRepository;

    /**
     * Create a new instance to use in order to search auctions history.
     * @param auctionsHistoryRepository     the auctions' history repository
     *                                      (i.e. a database of auctions histories)
     */
    public FindAuctionsHistory(final AuctionsHistoryRepository auctionsHistoryRepository) {
        this.auctionsHistoryRepository = auctionsHistoryRepository;
    }

    /**
     * Find the auctions' history by the associated user.
     * @param username  the desired username
     * @return          the auctions' history of the specified user, if any
     */
    public Optional<AuctionsHistory> findByUsername(final String username) {
        return auctionsHistoryRepository.findByUsername(username);
    }

    /**
     * Get the list of the awarded auctions for the specified username.
     * @param username  the desired username
     * @return          the list of awarded auctions
     */
    public List<AwardedAuction> getAwardedAuctions(final String username) {
        return auctionsHistoryRepository.getAwardedAuctions(username);
    }

    /**
     * Get the list of the created ongoing auctions for the specified username.
     * @param username  the desired username
     * @return          the list of created ongoing auctions
     */
    public List<CreatedOngoingAuction> getCreatedOngoingAuctions(final String username) {
        return auctionsHistoryRepository.getCreatedOngoingAuctions(username);
    }

    /**
     * Get the list of the created ended auctions for the specified username.
     * @param username  the desired username
     * @return          the list of created ended auctions
     */
    public List<CreatedEndedAuction> getCreatedEndedAuctions(final String username) {
        return auctionsHistoryRepository.getCreatedEndedAuctions(username);
    }

    /**
     * Get the list of the lost auctions for the specified username.
     * @param username  the desired username
     * @return          the list of lost auctions
     */
    public List<LostAuction> getLostAuctions(final String username) {
        return auctionsHistoryRepository.getLostAuctions(username);
    }

    /**
     * Get the list of the participating auctions for the specified username.
     * @param username  the desired username
     * @return          the list of participating auctions
     */
    public List<ParticipatingAuction> getParticipatingAuctions(final String username) {
        return auctionsHistoryRepository.getParticipatingAuctions(username);
    }

    /**
     * Get the desired awarded auction for the specified user.
     * @param auctionId     the desired auction id
     * @param username      the username of the desired user
     * @return              the required awarded auction, if any
     */
    public Optional<AwardedAuction> getAwardedAuction(final String auctionId, final String username) {
        return auctionsHistoryRepository.getAwardedAuction(auctionId, username);
    }

    /**
     * Get the desired created ongoing auction for the specified user.
     * @param auctionId     the desired auction id
     * @param username      the username of the desired user
     * @return              the required created ongoing auction, if any
     */
    public Optional<CreatedOngoingAuction> getCreatedOngoingAuction(final String auctionId, final String username)  {
        return auctionsHistoryRepository.getCreatedOngoingAuction(auctionId, username);
    }

    /**
     * Get the desired created ended auction for the specified user.
     * @param auctionId     the desired auction id
     * @param username      the username of the desired user
     * @return              the required created ended auction, if any
     */
    public Optional<CreatedEndedAuction> getCreatedEndedAuction(final String auctionId, final String username) {
        return auctionsHistoryRepository.getCreatedEndedAuction(auctionId, username);
    }

    /**
     * Get the desired lost auction for the specified user.
     * @param auctionId     the desired auction id
     * @param username      the username of the desired user
     * @return              the required lost auction, if any
     */
    public Optional<LostAuction> getLostAuction(final String auctionId, final String username) {
        return auctionsHistoryRepository.getLostAuction(auctionId, username);
    }

    /**
     * Get the desired participating auction for the specified user.
     * @param auctionId     the desired auction id
     * @param username      the username of the desired user
     * @return              the required participating auction, if any
     */
    public Optional<ParticipatingAuction> getParticipatingAuction(final String auctionId, final String username) {
        return auctionsHistoryRepository.getParticipatingAuction(auctionId, username);
    }
}
