/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.usecase;

import it.unibo.luckyauction.auction.domain.entity.Auction;
import it.unibo.luckyauction.auction.usecase.port.AuctionMemoryRepository;
import it.unibo.luckyauction.auction.usecase.port.AuctionRepository;
import it.unibo.luckyauction.util.CollectionsUtils;

import java.util.Calendar;
import java.util.List;
import java.util.function.Consumer;

/**
 * The class to manage the synchronization between in-memory repository
 * and the traditional one.
 */
public class SynchronizeRepositories {

    private final AuctionRepository auctionRepository;
    private final AuctionMemoryRepository auctionMemoryRepository;

    /**
     * Create a new instance to use in order to synchronize auction repositories.
     *
     * @param auctionRepository       the auction repository (i.e. a database of auctions)
     * @param auctionMemoryRepository the in-memory auction repository
     */
    public SynchronizeRepositories(final AuctionRepository auctionRepository,
                                   final AuctionMemoryRepository auctionMemoryRepository) {
        this.auctionRepository = auctionRepository;
        this.auctionMemoryRepository = auctionMemoryRepository;
    }

    /**
     * Synchronize the in-memory repository, containing only active auctions,
     * and the traditional one, containing all auctions (both active and ended).
     * Use this method on server startup or just periodically to keep both repositories
     * synchronized.
     */
    public void synchronize() {
        synchronizeWithAuctionResumingStrategy(Auction::resume);
    }

    /**
     * Synchronize the in-memory repository, containing only active auctions,
     * and the traditional one, containing all auctions (both active and ended).
     * Use this method when/if you have to restart the server and whenever there is
     * any server failure/interruption.
     * @param interruptionBeginTime     when the server interruption began
     */
    public void synchronizeAfterInterruption(final Calendar interruptionBeginTime) {
        synchronizeWithAuctionResumingStrategy(auction -> auction.resumeAfterInterruption(interruptionBeginTime));
    }

    private void synchronizeWithAuctionResumingStrategy(final Consumer<Auction> resumingAuctionStrategy) {
        final List<Auction> inMemoryActiveAuctions = auctionMemoryRepository.getAllAuctions();
        final List<Auction> allActiveAuctions = auctionRepository.getAllActiveAuctions();

        CollectionsUtils.difference(allActiveAuctions, inMemoryActiveAuctions)
                .forEach(missingActiveAuction -> {
                    resumingAuctionStrategy.accept(missingActiveAuction);
                    if (missingActiveAuction.isActive()) {
                        auctionMemoryRepository.create(missingActiveAuction);
                    }
                });
        CollectionsUtils.difference(inMemoryActiveAuctions, allActiveAuctions)
                .forEach(auctionRepository::create);
    }
}


