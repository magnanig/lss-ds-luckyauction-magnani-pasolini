/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.usecase.port;

import it.unibo.luckyauction.auction.domain.valueobject.ProductCategory;
import it.unibo.luckyauction.auction.domain.valueobject.ProductState;

import java.util.Optional;

public record AuctionFilter(ProductCategory productCategory, ProductState productState, String exceptUsername) {
    public Optional<ProductCategory> getProductCategory() {
        return Optional.ofNullable(productCategory);
    }

    public Optional<ProductState> getProductState() {
        return Optional.ofNullable(productState);
    }

    public Optional<String> getExceptUsername() {
        return Optional.ofNullable(exceptUsername);
    }

    public boolean isEmpty() {
        return getProductCategory().isEmpty() && getProductState().isEmpty() && getExceptUsername().isEmpty();
    }

    @SuppressWarnings({"checkstyle:all", "PMD.AvoidFieldNameMatchingMethodName"})
    public static class Builder {

        private ProductCategory productCategory;
        private ProductState productState;
        private String exceptUsername;

        public Builder productCategory(final ProductCategory productCategory) {
            this.productCategory = productCategory;
            return this;
        }

        public Builder productState(final ProductState productState) {
            this.productState = productState;
            return this;
        }

        public Builder exceptUsername(final String exceptUsername) {
            this.exceptUsername = exceptUsername;
            return this;
        }

        public AuctionFilter build() {
            return new AuctionFilter(productCategory, productState, exceptUsername);
        }
    }
}
