/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.usecase;

import it.unibo.luckyauction.auction.usecase.exception.AuctionNotFoundException;
import it.unibo.luckyauction.auction.usecase.exception.AuctionsHistoryNotFoundException;
import it.unibo.luckyauction.auction.usecase.port.AuctionsHistoryRepository;

/**
 * The class to remove summaries for ongoing auctions.
 */
public class RemoveSummaryOngoingAuction {

    private final AuctionsHistoryRepository auctionsHistoryRepository;

    /**
     * Create a new instance to use in order to delete summaries for ongoing auctions.
     * @param auctionsHistoryRepository     the auctions' history repository
     *                                      (i.e. a database of auctions histories)
     */
    public RemoveSummaryOngoingAuction(final AuctionsHistoryRepository auctionsHistoryRepository) {
        this.auctionsHistoryRepository = auctionsHistoryRepository;
    }

    /**
     * Remove auction from the list of the participating auction for the specified user.
     * @param auctionId                         the reference to the auction to remove
     * @param username                          the desired username
     * @throws AuctionNotFoundException         if no auction with the specified id exists
     * @throws AuctionsHistoryNotFoundException if the auctions' history for the specified
     *                                          user does not exist
     */
    public void removeParticipatingAuction(final String auctionId, final String username)
            throws AuctionNotFoundException, AuctionsHistoryNotFoundException {
        auctionsHistoryRepository.removeParticipatingAuction(auctionId, username);
    }

    /**
     * Remove auction from the list of the created ongoing auction for the specified user.
     * @param auctionId                         the reference to the auction to remove
     * @param username                          the desired username
     * @throws AuctionNotFoundException         if no auction with the specified id exists
     * @throws AuctionsHistoryNotFoundException if the auctions' history for the specified
     *                                          user does not exist
     */
    public void removeCreatedOngoingAuction(final String auctionId, final String username)
            throws AuctionNotFoundException, AuctionsHistoryNotFoundException {
        auctionsHistoryRepository.removeCreatedOngoingAuction(auctionId, username);
    }
}
