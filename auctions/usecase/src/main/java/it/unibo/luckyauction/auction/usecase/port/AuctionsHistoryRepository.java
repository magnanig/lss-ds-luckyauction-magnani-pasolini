/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.usecase.port;

import it.unibo.luckyauction.auction.domain.entity.AuctionsHistory;
import it.unibo.luckyauction.auction.domain.valueobject.AwardedAuction;
import it.unibo.luckyauction.auction.domain.valueobject.CreatedEndedAuction;
import it.unibo.luckyauction.auction.domain.valueobject.CreatedOngoingAuction;
import it.unibo.luckyauction.auction.domain.valueobject.LostAuction;
import it.unibo.luckyauction.auction.domain.valueobject.ParticipatingAuction;
import it.unibo.luckyauction.auction.domain.valueobject.UserContactInformation;
import it.unibo.luckyauction.auction.usecase.exception.AuctionNotFoundException;
import it.unibo.luckyauction.auction.usecase.exception.AuctionsHistoryNotFoundException;

import java.util.List;
import java.util.Optional;

public interface AuctionsHistoryRepository {

    /**
     * Create a new auctions' history.
     * @param auctionsHistory   the auction's history to create
     */
    void create(AuctionsHistory auctionsHistory);

    /**
     * Remove the auctions' history of the specified user.
     * @param username  the username of the user whose auctions' history
     *                  has to be removed
     * @return          the id of the just deleted auctions' history, if any
     */
    Optional<String> deleteByUsername(String username);

    /**
     * Find the auctions' history by the associated user.
     * @param username  the desired username
     * @return          the auctions' history of the specified user
     */
    Optional<AuctionsHistory> findByUsername(String username);

    /**
     * Add an awarded auction for the specified user.
     * @param auctionId                         the reference to the auction to add
     * @param sellerContacts                    the seller (i.e. auction creator) contact information
     * @param buyerContacts                     the buyer (i.e. who won the auction) contact information
     * @param username                          the username of the user to which add the
     *                                          awarded auction (i.e. who wins the auction)
     * @throws AuctionNotFoundException         if no auction with the specified id exists
     * @throws AuctionsHistoryNotFoundException if the auctions' history for the specified
     *                                          user does not exist
     */
    void addAwardedAuction(String auctionId, UserContactInformation sellerContacts,
                           UserContactInformation buyerContacts, String username)
            throws AuctionNotFoundException, AuctionsHistoryNotFoundException;

    /**
     * Add a created ended auction for the specified user.
     * @param auctionId                         the reference to the auction to add
     * @param username                          the username of the desired user
     * @throws AuctionNotFoundException         if no auction with the specified id exists
     * @throws AuctionsHistoryNotFoundException if the auctions' history for the specified
     *                                          user does not exist
     */
    void addCreatedEndedAuction(String auctionId, String username)
            throws AuctionNotFoundException, AuctionsHistoryNotFoundException;

    /**
     * Add a created ended auction for the specified user.
     * @param auctionId                         the reference to the auction to add
     * @param sellerContacts                    the seller (i.e. auction creator) contact information
     * @param buyerContacts                     the buyer (i.e. who won the auction) contact information
     * @param username                          the username of the user to which add the auction
     *                                          (i.e. the auction seller)
     * @throws AuctionNotFoundException         if no auction with the specified id exists
     * @throws AuctionsHistoryNotFoundException if the auctions' history for the specified
     *                                          user does not exist
     */
    void addCreatedEndedAuction(String auctionId, UserContactInformation sellerContacts,
                                UserContactInformation buyerContacts, String username)
            throws AuctionNotFoundException, AuctionsHistoryNotFoundException;

    /**
     * Add a created ongoing auction for the specified user.
     * @param auctionId                         the reference to the auction to add
     * @param username                          the username of the desired user
     * @throws AuctionNotFoundException         if no auction with the specified id exists
     * @throws AuctionsHistoryNotFoundException if the auctions' history for the specified
     *                                          user does not exist
     */
    void addCreatedOngoingAuction(String auctionId, String username)
            throws AuctionNotFoundException, AuctionsHistoryNotFoundException;

    /**
     * Add a lost auction for the specified user.
     * @param auctionId                         the reference to the auction to add
     * @param username                          the username of the desired user
     * @throws AuctionNotFoundException         if no auction with the specified id exists
     * @throws AuctionsHistoryNotFoundException if the auctions' history for the specified
     *                                          user does not exist
     */
    void addLostAuction(String auctionId, String username)
            throws AuctionNotFoundException, AuctionsHistoryNotFoundException;

    /**
     * Add a participating auction for the specified user.
     * @param auctionId                         the reference to the auction to add
     * @param username                          the username of the desired user
     * @throws AuctionNotFoundException         if no auction with the specified id exists
     * @throws AuctionsHistoryNotFoundException if the auctions' history for the specified
     *                                          user does not exist
     */
    void addParticipatingAuction(String auctionId, String username)
            throws AuctionNotFoundException, AuctionsHistoryNotFoundException;

    /**
     * Get the list of the awarded auctions for the specified username.
     * @param username  the desired username
     * @return          the list of awarded auctions
     */
    List<AwardedAuction> getAwardedAuctions(String username);

    /**
     * Get the list of the created ongoing auctions for the specified username.
     * @return          the list of created ongoing auctions
     */
    List<CreatedOngoingAuction> getCreatedOngoingAuctions(String username);

    /**
     * Get the list of the created ended auctions for the specified username.
     * @param username  the desired username
     * @return          the list of created ended auctions
     */
    List<CreatedEndedAuction> getCreatedEndedAuctions(String username);

    /**
     * Get the list of the lost auctions for the specified username.
     * @param username  the desired username
     * @return          the list of lost auctions
     */
    List<LostAuction> getLostAuctions(String username);

    /**
     * Get the list of the participating auctions for the specified username.
     * @return          the list of participating auctions
     */
    List<ParticipatingAuction> getParticipatingAuctions(String username);

    /**
     * Get the desired awarded auction for the specified user.
     * @param auctionId     the desired auction id
     * @param username      the username of the desired user
     * @return              the requested awarded auction, if any
     */
    Optional<AwardedAuction> getAwardedAuction(String auctionId, String username);

    /**
     * Get the desired created ongoing auction for the specified user.
     * @param auctionId     the desired auction id
     * @param username      the username of the desired user
     * @return              the requested created ongoing auction, if any
     */
    Optional<CreatedOngoingAuction> getCreatedOngoingAuction(String auctionId, String username);

    /**
     * Get the desired created ended auction for the specified user.
     * @param auctionId     the desired auction id
     * @param username      the username of the desired user
     * @return              the required created ended auction, if any
     */
    Optional<CreatedEndedAuction> getCreatedEndedAuction(String auctionId, String username);

    /**
     * Get the desired lost auction for the specified user.
     * @param auctionId     the desired auction id
     * @param username      the username of the desired user
     * @return              the required lost auction, if any
     */
    Optional<LostAuction> getLostAuction(String auctionId, String username);

    /**
     * Get the desired participating auction for the specified user.
     * @param auctionId     the desired auction id
     * @param username      the username of the desired user
     * @return              the required participating auction, if any
     */
    Optional<ParticipatingAuction> getParticipatingAuction(String auctionId, String username);

    /**
     * Get the list of the user participating in the specified auction.
     * @param auctionId     the desired auction id
     * @return              the list of the username of the user participating
     *                      in the specified auction
     */
    List<String> getUsersParticipatingInAuction(String auctionId);

    /**
     * Remove auction from the list of the participating auction for
     * the specified user.
     * @param auctionId                         the reference to the auction to remove
     * @param username                          the desired username
     * @throws AuctionNotFoundException         if no auction with the specified id exists
     * @throws AuctionsHistoryNotFoundException if the auctions' history for the specified
     *                                          user does not exist
     */
    void removeParticipatingAuction(String auctionId, String username)
            throws AuctionNotFoundException, AuctionsHistoryNotFoundException;

    /**
     * Remove auction from the list of the created ongoing auction for
     * the specified user.
     * @param auctionId                         the reference to the auction to remove
     * @param username                          the desired username
     * @throws AuctionNotFoundException         if no auction with the specified id exists
     * @throws AuctionsHistoryNotFoundException if the auctions' history for the specified
     *                                          user does not exist
     */
    void removeCreatedOngoingAuction(String auctionId, String username)
            throws AuctionNotFoundException, AuctionsHistoryNotFoundException;

    /**
     * Remove auction from all auctions histories list of all users.
     * @param auctionId the auction id
     */
    void removeAuctionFromAllHistory(String auctionId);

}
