/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.usecase;

import it.unibo.luckyauction.auction.domain.entity.AuctionsHistory;
import it.unibo.luckyauction.auction.usecase.exception.AuctionsHistoryAlreadyExistsException;
import it.unibo.luckyauction.auction.usecase.port.AuctionsHistoryRepository;

/**
 * The class to create auctions histories.
 */
public class CreateAuctionsHistory {

    private final AuctionsHistoryRepository auctionsHistoryRepository;

    /**
     * Create a new instance to use in order to create new auctions histories.
     * @param auctionsHistoryRepository     the auctions' history repository
     *                                      (i.e. a database of auctions histories)
     */
    public CreateAuctionsHistory(final AuctionsHistoryRepository auctionsHistoryRepository) {
        this.auctionsHistoryRepository = auctionsHistoryRepository;
    }

    /**
     * Create a new auctions' history.
     * @param auctionsHistory   the auction's history to create
     * @throws AuctionsHistoryAlreadyExistsException    if the history for the specified user
     *                                                  already exists
     */
    public void create(final AuctionsHistory auctionsHistory) throws AuctionsHistoryAlreadyExistsException {
        if (auctionsHistoryRepository.findByUsername(auctionsHistory.getUsername()).isPresent()) {
            throw new AuctionsHistoryAlreadyExistsException(auctionsHistory.getUsername());
        }
        auctionsHistoryRepository.create(auctionsHistory);
    }
}
