/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.usecase;

import it.unibo.luckyauction.auction.domain.valueobject.AbstractAuction;
import it.unibo.luckyauction.auction.domain.valueobject.UserContactInformation;
import it.unibo.luckyauction.auction.usecase.exception.AuctionNotFoundException;
import it.unibo.luckyauction.auction.usecase.exception.AuctionsHistoryNotFoundException;
import it.unibo.luckyauction.auction.usecase.port.AuctionsHistoryRepository;

import java.util.Collection;

/**
 * The class to add auction summaries.
 */
public class AddSummaryAuction {

    private final AuctionsHistoryRepository auctionsHistoryRepository;

    /**
     * Create a new instance to use in order to add auction summaries.
     * @param auctionsHistoryRepository     the auctions' history repository
     *                                      (i.e. a database of auctions histories)
     */
    public AddSummaryAuction(final AuctionsHistoryRepository auctionsHistoryRepository) {
        this.auctionsHistoryRepository = auctionsHistoryRepository;
    }

    /**
     * Add a created ongoing auction for the specified user.
     * @param auctionId                         the reference to the auction to add
     * @param username                          the username of the desired user
     * @throws AuctionNotFoundException         if no auction with the specified id exists
     * @throws AuctionsHistoryNotFoundException if the auctions' history for the specified
     *                                          user does not exist
     */
    public void addCreatedOngoingAuction(final String auctionId, final String username)
            throws AuctionNotFoundException, AuctionsHistoryNotFoundException {
        if (!auctionAlreadyAdded(auctionId, auctionsHistoryRepository.getCreatedOngoingAuctions(username))) {
            auctionsHistoryRepository.addCreatedOngoingAuction(auctionId, username);
        }
    }

    /**
     * Add a created ended auction for the specified user.
     * @param auctionId                         the reference to the auction to add
     * @param sellerContacts                    the seller (i.e. auction creator) contact information
     * @param buyerContacts                     the buyer (i.e. who won the auction) contact information
     * @param username                          the username of the user to which add the auction
     *                                          (i.e. the auction seller)
     * @throws AuctionNotFoundException         if no auction with the specified id exists
     * @throws AuctionsHistoryNotFoundException if the auctions' history for the specified
     *                                          user does not exist
     */
    public void addCreatedEndedAuction(final String auctionId, final UserContactInformation sellerContacts,
                                       final UserContactInformation buyerContacts, final String username)
            throws AuctionNotFoundException, AuctionsHistoryNotFoundException {
        if (!auctionAlreadyAdded(auctionId, auctionsHistoryRepository.getCreatedEndedAuctions(username))) {
            if (sellerContacts == null || buyerContacts == null) {
                auctionsHistoryRepository.addCreatedEndedAuction(auctionId, username);
            } else {
                auctionsHistoryRepository.addCreatedEndedAuction(auctionId, sellerContacts, buyerContacts, username);
            }
        }
    }

    /**
     * Add a created ended auction for the specified user.
     * @param auctionId                         the reference to the auction to add
     * @param username                          the desired username
     * @throws AuctionNotFoundException         if no auction with the specified id exists
     * @throws AuctionsHistoryNotFoundException if the auctions' history for the specified
     *                                          user does not exist
     */
    public void addCreatedEndedAuction(final String auctionId, final String username) {
        addCreatedEndedAuction(auctionId, null, null, username);
    }

    /**
     * Add a lost auction for the specified user.
     * @param auctionId                         the reference to the auction to add
     * @param username                          the username of the desired user
     * @throws AuctionNotFoundException         if no auction with the specified id exists
     * @throws AuctionsHistoryNotFoundException if the auctions' history for the specified
     *                                          user does not exist
     */
    public void addLostAuction(final String auctionId, final String username)
            throws AuctionNotFoundException, AuctionsHistoryNotFoundException {
        if (!auctionAlreadyAdded(auctionId, auctionsHistoryRepository.getLostAuctions(username))) {
            auctionsHistoryRepository.addLostAuction(auctionId, username);
        }
    }

    /**
     * Add an awarded auction for the specified user.
     * @param auctionId                         the reference to the auction to add
     * @param username                          the username of the desired user
     * @throws AuctionNotFoundException         if no auction with the specified id exists
     * @throws AuctionsHistoryNotFoundException if the auctions' history for the specified
     *                                          user does not exist
     */
    public void addAwardedAuction(final String auctionId, final UserContactInformation sellerContacts,
                                  final UserContactInformation buyerContacts, final String username)
            throws AuctionNotFoundException, AuctionsHistoryNotFoundException {
        if (!auctionAlreadyAdded(auctionId, auctionsHistoryRepository.getAwardedAuctions(username))) {
            auctionsHistoryRepository.addAwardedAuction(auctionId, sellerContacts, buyerContacts, username);
        }
    }

    /**
     * Add a participating auction for the specified user.
     * @param auctionId                         the reference to the auction to add
     * @param username                          the username of the desired user
     * @throws AuctionNotFoundException         if no auction with the specified id exists
     * @throws AuctionsHistoryNotFoundException if the auctions' history for the specified
     *                                          user does not exist
     */
    public void addParticipatingAuction(final String auctionId, final String username)
            throws AuctionNotFoundException, AuctionsHistoryNotFoundException {
        if (!auctionsHistoryRepository.getUsersParticipatingInAuction(auctionId).contains(username)) {
            auctionsHistoryRepository.addParticipatingAuction(auctionId, username);
        }
    }

    private Boolean auctionAlreadyAdded(final String auctionId, final Collection<? extends AbstractAuction> auctions) {
        return auctions.stream().anyMatch(auction -> auction.getAuctionId().equals(auctionId));
    }

}
