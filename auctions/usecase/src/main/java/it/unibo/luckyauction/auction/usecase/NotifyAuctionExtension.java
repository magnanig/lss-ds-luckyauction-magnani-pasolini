/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.usecase;

import it.unibo.luckyauction.auction.usecase.exception.AuctionNotFoundException;
import it.unibo.luckyauction.auction.usecase.port.AuctionRepository;

import java.util.Calendar;

/**
 * The class to notify the occurred extension of an auction and so to save
 * the new expected closing time in auctions repository. Remember: an auction
 * can be extended if, at the expected closing time, it has received no bids.
 */
public class NotifyAuctionExtension {

    private final AuctionRepository auctionRepository;

    /**
     * Create a new instance to use in order to notify auctions extension.
     * @param auctionRepository     the auction repository (i.e. a database of auctions)
     */
    public NotifyAuctionExtension(final AuctionRepository auctionRepository) {
        this.auctionRepository = auctionRepository;
    }

    /**
     * Notify that the specified auction has been extended, updating its expected
     * closing time in auctions repository. Note that this method does not modify
     * any object in memory: auction life is always self-managed internally by
     * auction itself.
     * @param auctionId                     the id of the auction to notify
     * @param newExpectedClosingTime        the new expected closing time for such auction
     * @throws AuctionNotFoundException     if no auction with such id exists
     */
    public void notify(final String auctionId, final Calendar newExpectedClosingTime) throws AuctionNotFoundException {
        if (auctionRepository.findById(auctionId).isEmpty()) {
            throw new AuctionNotFoundException(auctionId);
        }
        auctionRepository.extendAuction(auctionId, newExpectedClosingTime);
    }
}
