dependencies {
    implementation(project(":utils"))
    implementation(project(":auctions:domain"))

    testImplementation(project(":auctions:memory-repository"))
    testImplementation(project(":auctions:mongo-repository"))
    testImplementation(project(":uuid-generator"))

    // this will add following dependencies: test --> users.domain.testFixtures
    testImplementation(testFixtures(project(":auctions:domain")))
}