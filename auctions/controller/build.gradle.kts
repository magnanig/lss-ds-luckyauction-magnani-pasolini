dependencies {
    implementation(project(":configuration"))
    implementation(project(":utils"))
    implementation(project(":uuid-generator"))

    implementation(project(":auctions:domain"))
    implementation(project(":auctions:usecase"))
    implementation(project(":auctions:adapter"))
    implementation(project(":auctions:memory-repository"))
    implementation(project(":auctions:mongo-repository"))

    implementation(project(":users:adapter"))
    implementation(project(":movements:adapter"))
    implementation(project(":automations:adapter"))

    // this will add following dependencies: test --> users.domain.testFixtures
    testImplementation(testFixtures(project(":users:domain")))
    // this will add following dependencies: test --> auctions.domain.testFixtures
    testImplementation(testFixtures(project(":auctions:domain")))
    testImplementation(project(":utils:spring-utils"))
    testImplementation(project(":users:domain"))
    testImplementation(project(":users:usecase"))
    testImplementation(project(":users:controller"))
    testImplementation(project(":movements:domain"))
    testImplementation(project(":movements:usecase"))
    testImplementation(project(":movements:controller"))
}
