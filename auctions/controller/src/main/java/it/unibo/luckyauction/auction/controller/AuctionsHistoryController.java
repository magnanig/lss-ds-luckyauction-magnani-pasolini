/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.controller;

import it.unibo.luckyauction.auction.adapter.AuctionWeb;
import it.unibo.luckyauction.auction.adapter.api.AuctionApi;
import it.unibo.luckyauction.auction.controller.exception.UserNotFoundException;
import it.unibo.luckyauction.auction.domain.entity.Auction;
import it.unibo.luckyauction.auction.domain.entity.AuctionsHistory;
import it.unibo.luckyauction.auction.domain.valueobject.AwardedAuction;
import it.unibo.luckyauction.auction.domain.valueobject.CreatedEndedAuction;
import it.unibo.luckyauction.auction.domain.valueobject.CreatedOngoingAuction;
import it.unibo.luckyauction.auction.domain.valueobject.LostAuction;
import it.unibo.luckyauction.auction.domain.valueobject.ParticipatingAuction;
import it.unibo.luckyauction.auction.domain.valueobject.UserContactInformation;
import it.unibo.luckyauction.auction.usecase.AddSummaryAuction;
import it.unibo.luckyauction.auction.usecase.CreateAuctionsHistory;
import it.unibo.luckyauction.auction.usecase.DeleteAuctionsHistory;
import it.unibo.luckyauction.auction.usecase.FindAuctionsHistory;
import it.unibo.luckyauction.auction.usecase.FindUsersParticipatingInAuction;
import it.unibo.luckyauction.auction.usecase.RemoveAllSummariesForAuction;
import it.unibo.luckyauction.auction.usecase.RemoveSummaryOngoingAuction;
import it.unibo.luckyauction.auction.usecase.exception.AuctionNotFoundException;
import it.unibo.luckyauction.auction.usecase.exception.AuctionsHistoryAlreadyExistsException;
import it.unibo.luckyauction.auction.usecase.exception.AuctionsHistoryNotFoundException;
import it.unibo.luckyauction.auction.controller.exception.WrongAuctionStateException;
import it.unibo.luckyauction.user.adapter.ContactsWeb;
import it.unibo.luckyauction.user.adapter.PhoneWeb;
import it.unibo.luckyauction.user.adapter.api.UserApi;

import java.util.List;
import java.util.Optional;

import static it.unibo.luckyauction.util.NullableUtils.safeCall;

@SuppressWarnings("PMD.AvoidFieldNameMatchingMethodName")
public class AuctionsHistoryController {

    private final CreateAuctionsHistory createAuctionsHistory;
    private final DeleteAuctionsHistory deleteAuctionsHistory;
    private final AddSummaryAuction addSummaryAuction;
    private final FindAuctionsHistory findAuctionsHistory;
    private final FindUsersParticipatingInAuction findUsersParticipatingInAuction;
    private final RemoveSummaryOngoingAuction removeSummaryOngoingAuction;
    private final RemoveAllSummariesForAuction removeAllSummariesForAuction;

    private final AuctionApi auctionApi;
    private final UserApi userApi;

    public AuctionsHistoryController(final CreateAuctionsHistory createAuctionsHistory,
                                     final DeleteAuctionsHistory deleteAuctionsHistory,
                                     final AddSummaryAuction addSummaryAuction,
                                     final FindAuctionsHistory findAuctionsHistory,
                                     final FindUsersParticipatingInAuction findUsersParticipatingInAuction,
                                     final RemoveSummaryOngoingAuction removeSummaryOngoingAuction,
                                     final RemoveAllSummariesForAuction removeAllSummariesForAuction,
                                     final AuctionApi auctionApi, final UserApi userApi) {
        this.createAuctionsHistory = createAuctionsHistory;
        this.deleteAuctionsHistory = deleteAuctionsHistory;
        this.addSummaryAuction = addSummaryAuction;
        this.findAuctionsHistory = findAuctionsHistory;
        this.findUsersParticipatingInAuction = findUsersParticipatingInAuction;
        this.removeSummaryOngoingAuction = removeSummaryOngoingAuction;
        this.removeAllSummariesForAuction = removeAllSummariesForAuction;
        this.auctionApi = auctionApi;
        this.userApi = userApi;
    }

    /**
     * Create a new auctions' history.
     * @param auctionsHistory   the auction's history to create
     * @throws AuctionsHistoryAlreadyExistsException    if the history for the specified user
     *                                                  already exists
     */
    public void createAuctionsHistory(final AuctionsHistory auctionsHistory)
            throws AuctionsHistoryAlreadyExistsException {
        createAuctionsHistory.create(auctionsHistory);
    }

    /**
     * Remove the auctions' history of the specified user.
     * @param username  the username of the user whose auctions' history
     *                  has to be removed
     * @return          the id of the just deleted auctions' history, if any
     */
    public Optional<String> deleteByUsername(final String username) {
        return deleteAuctionsHistory.deleteByUsername(username);
    }

    /**
     * Find the auctions' history by the associated user.
     * @param username  the desired username
     * @return          the auctions' history of the specified user
     */
    public Optional<AuctionsHistory> findByUsername(final String username) {
        return findAuctionsHistory.findByUsername(username);
    }

    /**
     * Add an awarded auction for the specified user.
     * @param auctionId                         the reference to the auction to add
     * @param username                          the username of the desired user
     * @throws WrongAuctionStateException       if the specified auction is not ended
     * @throws AuctionNotFoundException         if no auction with the specified id exists
     * @throws AuctionsHistoryNotFoundException if the auctions' history for the specified
     *                                          user does not exist
     */
    public void addAwardedAuction(final String auctionId, final String username)
            throws WrongAuctionStateException, AuctionNotFoundException, AuctionsHistoryNotFoundException {
        final Auction auction = getAuctionAndAssertActiveState(auctionId, false);
        final UserContactInformation sellerContacts = getUserContacts(auction.getSellerUsername());
        final UserContactInformation buyersContacts = getUserContacts(
                auction.getWinningBid().orElseThrow().getBidderUsername()
        );
        addSummaryAuction.addAwardedAuction(auctionId, sellerContacts, buyersContacts, username);
    }

    /**
     * Get the list of the created ended auctions for the specified username.
     * @param auctionId                         the reference to the auction to add
     * @param username                          the desired username
     * @throws WrongAuctionStateException       if the specified auction is not ended
     * @throws AuctionNotFoundException         if no auction with the specified id exists
     * @throws AuctionsHistoryNotFoundException if the auctions' history for the specified
     *                                          user does not exist
     */
    public void addCreatedEndedAuction(final String auctionId, final String username)
            throws WrongAuctionStateException, AuctionNotFoundException, AuctionsHistoryNotFoundException {
        final Auction auction = getAuctionAndAssertActiveState(auctionId, false);
        auction.getWinningBid().ifPresentOrElse(winningBid -> {
            final UserContactInformation sellerContacts = getUserContacts(auction.getSellerUsername());
            final UserContactInformation buyersContacts = getUserContacts(winningBid.getBidderUsername());
            addSummaryAuction.addCreatedEndedAuction(auctionId, sellerContacts, buyersContacts, username);
        }, () -> addSummaryAuction.addCreatedEndedAuction(auctionId, username));
    }

    /**
     * Add a created ongoing auction for the specified user.
     * @param auctionId                         the reference to the auction to add
     * @param username                          the username of the desired user
     * @throws WrongAuctionStateException       if the specified auction is not ongoing
     * @throws AuctionNotFoundException         if no auction with the specified id exists
     * @throws AuctionsHistoryNotFoundException if the auctions' history for the specified
     *                                          user does not exist
     */
    public void addCreatedOngoingAuction(final String auctionId, final String username)
            throws WrongAuctionStateException, AuctionNotFoundException, AuctionsHistoryNotFoundException {
        getAuctionAndAssertActiveState(auctionId, true);
        addSummaryAuction.addCreatedOngoingAuction(auctionId, username);
    }

    /**
     * Add a lost auction for the specified user.
     * @param auctionId                         the reference to the auction to add
     * @param username                          the username of the desired user
     * @throws WrongAuctionStateException       if the specified auction is not ended
     * @throws AuctionNotFoundException         if no auction with the specified id exists
     * @throws AuctionsHistoryNotFoundException if the auctions' history for the specified
     *                                          user does not exist
     */
    public void addLostAuction(final String auctionId, final String username)
            throws WrongAuctionStateException, AuctionNotFoundException, AuctionsHistoryNotFoundException {
        getAuctionAndAssertActiveState(auctionId, false);
        addSummaryAuction.addLostAuction(auctionId, username);
    }

    /**
     * Add a participating auction for the specified user.
     * @param auctionId                         the reference to the auction to add
     * @param username                          the username of the desired user
     * @throws WrongAuctionStateException       if the specified auction is not ongoing
     * @throws AuctionNotFoundException         if no auction with the specified id exists
     * @throws AuctionsHistoryNotFoundException if the auctions' history for the specified
     *                                          user does not exist
     */
    public void addParticipatingAuction(final String auctionId, final String username)
            throws WrongAuctionStateException, AuctionNotFoundException, AuctionsHistoryNotFoundException {
        getAuctionAndAssertActiveState(auctionId, true);
        addSummaryAuction.addParticipatingAuction(auctionId, username);
    }

    /**
     * Get the list of the awarded auctions for the specified username.
     * @param username  the desired username
     * @return          the list of awarded auctions
     */
    public List<AwardedAuction> getAwardedAuctions(final String username) {
        return findAuctionsHistory.getAwardedAuctions(username);
    }

    /**
     * Get the list of the created ongoing auctions for the specified username.
     * @param username  the desired username
     * @return          the list of created ongoing auctions
     */
    public List<CreatedOngoingAuction> getCreatedOngoingAuctions(final String username) {
        return findAuctionsHistory.getCreatedOngoingAuctions(username);
    }

    /**
     * Get the list of the created ended auctions for the specified username.
     * @param username  the desired username
     * @return          the list of created ended auctions
     */
    public List<CreatedEndedAuction> getCreatedEndedAuctions(final String username) {
        return findAuctionsHistory.getCreatedEndedAuctions(username);
    }

    /**
     * Get the list of the lost auctions for the specified username.
     * @param username  the desired username
     * @return          the list of lost auctions
     */
    public List<LostAuction> getLostAuctions(final String username) {
        return findAuctionsHistory.getLostAuctions(username);
    }

    /**
     * Get the list of the participating auctions for the specified username.
     * @param username  the desired username
     * @return          the list of participating auctions
     */
    public List<ParticipatingAuction> getParticipatingAuctions(final String username) {
        return findAuctionsHistory.getParticipatingAuctions(username);
    }

    /**
     * Get the desired awarded auction for the specified user.
     * @param auctionId     the desired auction id
     * @param username      the username of the desired user
     * @return              the required awarded auction, if any
     */
    public Optional<AwardedAuction> getAwardedAuction(final String auctionId, final String username) {
        return findAuctionsHistory.getAwardedAuction(auctionId, username);
    }

    /**
     * Get the desired created ongoing auction for the specified user.
     * @param auctionId     the desired auction id
     * @param username      the username of the desired user
     * @return              the required created ongoing auction, if any
     */
    public Optional<CreatedOngoingAuction> getCreatedOngoingAuction(final String auctionId, final String username) {
        return findAuctionsHistory.getCreatedOngoingAuction(auctionId, username);
    }

    /**
     * Get the desired created ended auction for the specified user.
     * @param auctionId     the desired auction id
     * @param username      the username of the desired user
     * @return              the required created ended auction, if any
     */
    public Optional<CreatedEndedAuction> getCreatedEndedAuction(final String auctionId, final String username) {
        return findAuctionsHistory.getCreatedEndedAuction(auctionId, username);
    }

    /**
     * Get the desired lost auction for the specified user.
     * @param auctionId     the desired auction id
     * @param username      the username of the desired user
     * @return              the required lost auction, if any
     */
    public Optional<LostAuction> getLostAuction(final String auctionId, final String username) {
        return findAuctionsHistory.getLostAuction(auctionId, username);
    }

    /**
     * Get the desired participating auction for the specified user.
     * @param auctionId     the desired auction id
     * @param username      the username of the desired user
     * @return              the required participating auction, if any
     */
    public Optional<ParticipatingAuction> getParticipatingAuction(final String auctionId, final String username) {
        return findAuctionsHistory.getParticipatingAuction(auctionId, username);
    }

    /**
     * Get the list of the user participating in the specified auction.
     * @param auctionId     the desired auction id
     * @return              the list of the username of the user participating
     *                      in the specified auction
     */
    public List<String> getUsersParticipatingInAuction(final String auctionId) {
        return findUsersParticipatingInAuction.findByAuctionId(auctionId);
    }

    /**
     * Remove auction from the list of the participating auction for the specified user.
     * @param auctionId                         the reference to the auction to remove
     * @param username                          the desired username
     * @throws AuctionNotFoundException         if no auction with the specified id exists
     * @throws AuctionsHistoryNotFoundException if the auctions' history for the specified
     *                                          user does not exist
     */
    public void removeParticipatingAuction(final String auctionId, final String username) {
        removeSummaryOngoingAuction.removeParticipatingAuction(auctionId, username);
    }

    /**
     * Remove auction from the list of the created ongoing auction for the specified user.
     * @param auctionId                         the reference to the auction to remove
     * @param username                          the desired username
     * @throws AuctionNotFoundException         if no auction with the specified id exists
     * @throws AuctionsHistoryNotFoundException if the auctions' history for the specified
     *                                          user does not exist
     */
    public void removeCreatedOngoingAuction(final String auctionId, final String username) {
        removeSummaryOngoingAuction.removeCreatedOngoingAuction(auctionId, username);
    }

    /**
     * Remove the auction from all auction histories of all users.
     * @param auctionId the id of the desired auction
     */
    public void removeAuctionFromAllHistory(final String auctionId) {
        removeAllSummariesForAuction.removeAuction(auctionId);
    }

    private Auction getAuctionAndAssertActiveState(final String auctionId, final Boolean active)
            throws AuctionNotFoundException, WrongAuctionStateException {
        final Auction auction = auctionApi.findById(auctionId)
                .map(AuctionWeb::toDomainAuction)
                .orElseThrow(() -> new AuctionNotFoundException(auctionId));
        if (auction.isActive() != active) {
            throw new WrongAuctionStateException(auctionId, active);
        }
        return auction;
    }

    private UserContactInformation getUserContacts(final String username) {
        final ContactsWeb contacts = userApi.findByUsername(username)
                .orElseThrow(() -> new UserNotFoundException(username))
                .getContacts();
        return new UserContactInformation(contacts.getEmail(), contacts.getCellularNumber().getNumber(),
                safeCall(contacts.getTelephoneNumber(), PhoneWeb::getNumber));
    }
}
