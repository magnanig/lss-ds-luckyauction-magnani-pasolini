/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.controller.config;

import it.unibo.luckyauction.auction.adapter.api.AuctionApi;
import it.unibo.luckyauction.auction.controller.AuctionsHistoryController;
import it.unibo.luckyauction.auction.repository.AuctionsHistoryMongoRepository;
import it.unibo.luckyauction.auction.usecase.AddSummaryAuction;
import it.unibo.luckyauction.auction.usecase.CreateAuctionsHistory;
import it.unibo.luckyauction.auction.usecase.DeleteAuctionsHistory;
import it.unibo.luckyauction.auction.usecase.FindAuctionsHistory;
import it.unibo.luckyauction.auction.usecase.FindUsersParticipatingInAuction;
import it.unibo.luckyauction.auction.usecase.RemoveAllSummariesForAuction;
import it.unibo.luckyauction.auction.usecase.RemoveSummaryOngoingAuction;
import it.unibo.luckyauction.auction.usecase.port.AuctionsHistoryRepository;
import it.unibo.luckyauction.user.adapter.api.UserApi;

import static it.unibo.luckyauction.configuration.ConfigurationConstants.TEST_MODE;

public class AuctionsHistoryConfig {

    private final AuctionsHistoryRepository auctionsHistoryRepository;

    public AuctionsHistoryConfig() {
        this.auctionsHistoryRepository = TEST_MODE
                ? AuctionsHistoryMongoRepository.testRepository()
                : AuctionsHistoryMongoRepository.defaultRepository();
    }

    /**
     * Build the auction history controller of the application, capable of orchestrating auctions histories.
     * See {@link AuctionsHistoryController} for more details.
     * @return      the just built auction history controller
     */
    public AuctionsHistoryController auctionsHistoryController(final AuctionApi auctionApi, final UserApi userApi) {
        return new AuctionsHistoryController(createAuctionsHistory(), deleteAuctionsHistory(), addSummaryAuction(),
                findAuctionsHistory(), findUsersParticipatingToAuction(), removeSummaryOngoingAuction(),
                removeAllSummariesForAuction(), auctionApi, userApi);
    }

    private CreateAuctionsHistory createAuctionsHistory() {
        return new CreateAuctionsHistory(auctionsHistoryRepository);
    }

    private DeleteAuctionsHistory deleteAuctionsHistory() {
        return new DeleteAuctionsHistory(auctionsHistoryRepository);
    }

    private AddSummaryAuction addSummaryAuction() {
        return new AddSummaryAuction(auctionsHistoryRepository);
    }

    private FindAuctionsHistory findAuctionsHistory() {
        return new FindAuctionsHistory(auctionsHistoryRepository);
    }

    private FindUsersParticipatingInAuction findUsersParticipatingToAuction() {
        return new FindUsersParticipatingInAuction(auctionsHistoryRepository);
    }

    private RemoveSummaryOngoingAuction removeSummaryOngoingAuction() {
        return new RemoveSummaryOngoingAuction(auctionsHistoryRepository);
    }

    private RemoveAllSummariesForAuction removeAllSummariesForAuction() {
        return new RemoveAllSummariesForAuction(auctionsHistoryRepository);
    }
}
