/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.controller;

import it.unibo.luckyauction.auction.adapter.BidWeb;
import it.unibo.luckyauction.auction.adapter.api.AuctionsHistoryApi;
import it.unibo.luckyauction.auction.adapter.socket.AuctionClosed;
import it.unibo.luckyauction.auction.adapter.socket.AuctionExpectedClosingTimeChanged;
import it.unibo.luckyauction.auction.adapter.socket.AuctionSocket;
import it.unibo.luckyauction.auction.adapter.socket.BidsSocket;
import it.unibo.luckyauction.auction.controller.handler.AuctionClosedHandler;
import it.unibo.luckyauction.auction.controller.handler.AuctionExpectedClosingTimeUpdated;
import it.unibo.luckyauction.auction.controller.handler.AuctionExtendedHandler;
import it.unibo.luckyauction.auction.domain.entity.AbstractAuctionWithLife;
import it.unibo.luckyauction.auction.domain.entity.Auction;
import it.unibo.luckyauction.auction.domain.event.AuctionDomainEvent;
import it.unibo.luckyauction.auction.domain.valueobject.AbstractOngoingAuction;
import it.unibo.luckyauction.auction.domain.valueobject.Bid;
import it.unibo.luckyauction.auction.usecase.CreateAuction;
import it.unibo.luckyauction.auction.usecase.DeleteAuction;
import it.unibo.luckyauction.auction.usecase.FindAuction;
import it.unibo.luckyauction.auction.usecase.FindBidsHistory;
import it.unibo.luckyauction.auction.usecase.NotifyAuctionExtension;
import it.unibo.luckyauction.auction.usecase.NotifyAuctionPostponedOnBidRaise;
import it.unibo.luckyauction.auction.usecase.PerformOperationsPostAuctionClosing;
import it.unibo.luckyauction.auction.usecase.RaiseBid;
import it.unibo.luckyauction.auction.usecase.SynchronizeRepositories;
import it.unibo.luckyauction.auction.usecase.exception.AuctionNotAllowedException;
import it.unibo.luckyauction.auction.usecase.exception.AuctionNotFoundException;
import it.unibo.luckyauction.auction.usecase.exception.ClosingAuctionException;
import it.unibo.luckyauction.auction.usecase.exception.InvalidBidRaiseException;
import it.unibo.luckyauction.auction.controller.exception.WrongAuctionStateException;
import it.unibo.luckyauction.auction.usecase.port.AuctionFilter;
import it.unibo.luckyauction.automation.adapter.api.AuctionAutomationApi;
import it.unibo.luckyauction.movement.adapter.api.MovementApi;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@SuppressWarnings("PMD.AvoidFieldNameMatchingMethodName")
public class AuctionController {

    private final CreateAuction createAuction;
    private final DeleteAuction deleteAuction;
    private final FindAuction findAuction;
    private final RaiseBid raiseBid;
    private final FindBidsHistory findBidsHistory;
    private final NotifyAuctionExtension notifyAuctionExtension;
    private final NotifyAuctionPostponedOnBidRaise notifyAuctionPostponed;
    private final PerformOperationsPostAuctionClosing performOperationsPostAuctionClosing;
    private final SynchronizeRepositories synchronizeRepositories;

    private final MovementApi movementApi;
    private final AuctionsHistoryApi auctionsHistoryApi;
    private final AuctionAutomationApi auctionAutomationApi;

    private final AuctionSocket auctionSocket;
    private final BidsSocket bidsSockets;

    public AuctionController(final CreateAuction createAuction, final DeleteAuction deleteAuction,
                             final FindAuction findAuction, final RaiseBid raiseBid,
                             final FindBidsHistory findBidsHistory, final NotifyAuctionExtension notifyAuctionExtension,
                             final NotifyAuctionPostponedOnBidRaise notifyAuctionPostponed,
                             final PerformOperationsPostAuctionClosing performOperationsPostAuctionClosing,
                             final SynchronizeRepositories synchronizeRepositories, final MovementApi movementApi,
                             final AuctionsHistoryApi auctionsHistoryApi,
                             final AuctionAutomationApi auctionAutomationApi, final AuctionSocket auctionSocket,
                             final BidsSocket bidsSockets) {
        this.auctionAutomationApi = auctionAutomationApi;
        this.auctionSocket = auctionSocket;
        this.bidsSockets = bidsSockets;
        AuctionDomainEvent.registerAuctionClosedHandler(new AuctionClosedHandler(this));
        AuctionDomainEvent.registerAuctionExtendedHandler(new AuctionExtendedHandler(this));
        AuctionDomainEvent.registerAuctionExpectingClosingTimeUpdatedHandler(
                new AuctionExpectedClosingTimeUpdated(this));
        this.createAuction = createAuction;
        this.deleteAuction = deleteAuction;
        this.findAuction = findAuction;
        this.raiseBid = raiseBid;
        this.findBidsHistory = findBidsHistory;
        this.notifyAuctionExtension = notifyAuctionExtension;
        this.notifyAuctionPostponed = notifyAuctionPostponed;
        this.performOperationsPostAuctionClosing = performOperationsPostAuctionClosing;
        this.movementApi = movementApi;
        this.auctionsHistoryApi = auctionsHistoryApi;
        this.synchronizeRepositories = synchronizeRepositories;
        openSocketsAuctionNamespaces();
    }

    /**
     * Synchronize the in-memory repository, containing only active auctions,
     * and the traditional one, containing all auctions (both active and ended).
     * Use this method on server startup and then when/if you have to restart the server
     * or when there is any failure, or just periodically to keep both repositories
     * synchronized.
     */
    public void synchronizeRepositories() {
        synchronizeRepositories.synchronize();
    }

    /**
     * Synchronize the in-memory repository, containing only active auctions,
     * and the traditional one, containing all auctions (both active and ended).
     * Use this method when/if you have to restart the server and whenever there is
     * any server failure/interruption.
     * @param interruptionBeginTime     when the server interruption began
     */
    public void synchronizeRepositoriesAfterInterruption(final Calendar interruptionBeginTime) {
        synchronizeRepositories.synchronizeAfterInterruption(interruptionBeginTime);
    }

    /**
     * Create and start a new auction.
     * @param auction   the auction to create
     * @return          the just created auction, already started
     */
    public Auction createAndStart(final Auction auction) {
        final Auction newAuction = createAuction.createAndStart(auction);
        auctionsHistoryApi.addCreatedOngoingAuction(newAuction.getAuctionId(), newAuction.getSellerUsername());
        auctionSocket.addAuctionNamespace(newAuction.getAuctionId());
        bidsSockets.addAuctionNamespace(newAuction.getAuctionId());
        return newAuction;
    }

    /**
     * Delete a specific user's auction.
     * @param auctionId the desired auction id to delete
     * @param username  the desired username
     * @return          the just deleted auction, if existed
     * @throws AuctionNotAllowedException   if the username does not match with the auction seller
     */
    public Optional<Auction> deleteById(final String auctionId, final String username)
            throws AuctionNotAllowedException {
        auctionAutomationApi.removeAllAutomationsForAuction(auctionId);
        auctionsHistoryApi.deleteByUsername(username);
        final Optional<Auction> deletedAuction = deleteAuction.deleteById(auctionId, username);
        deletedAuction.flatMap(AbstractOngoingAuction::getActualBid).ifPresent(bid ->
                movementApi.unfreezeMoney(bid.getBidderUsername(), auctionId));
        return deletedAuction;
    }

    /**
     * Delete a specific auction.
     * @param auctionId the desired auction id to delete
     * @return          the just deleted auction, if existed
     */
    public Optional<Auction> deleteById(final String auctionId) {
        return deleteAuction.deleteById(auctionId).stream()
                .peek(auction -> auction.getActualBid().ifPresent(bid ->
                        movementApi.unfreezeMoney(bid.getBidderUsername(), auctionId)))
                .findFirst();
    }

    /**
     * Delete all auctions of a specific user.
     * @param sellerUsername  the desired seller username (i.e. the
     *                        creator of the auction)
     * @return                the list of the just deleted auctions
     */
    public List<Auction> deleteByUsername(final String sellerUsername) {
        return deleteAuction.deleteByUsername(sellerUsername).stream()
                .filter(Auction::isActive)
                .peek(auction -> {
                    auctionAutomationApi.removeAllAutomationsForAuction(auction.getAuctionId());
                    auction.getActualBid().ifPresent(bid -> movementApi.unfreezeMoney(bid.getBidderUsername(),
                            auction.getAuctionId()));
                    removeSocketsAuctionNamespace(auction.getAuctionId());
                })
                .collect(Collectors.toList());
    }

    /**
     * Find an auction by its id. If the auction is actually ongoing,
     * a reference to the original object will be retrieved, otherwise
     * its copy. <br>
     * @param auctionId     the desired auction id
     * @return              the corresponding auction, if present
     */
    public Optional<Auction> findById(final String auctionId) {
        return findAuction.findById(auctionId);
    }

    /**
     * Get the list of all auctions. The retrieved auctions are a
     * copy of the original objects.
     * @return  the list of all auctions
     */
    public List<Auction> getAllAuctions() {
        return findAuction.getAllAuctions();
    }

    /**
     * Get the list of all active auctions.
     * @return  the list of all active auctions
     */
    public List<Auction> findActiveAuctions() {
        return findAuction.getAllActiveAuctions();
    }

    /**
     * Get the active auctions matching the specified filters.
     * @param auctionFilter     the desired filters
     * @return                  the list of active auctions matching such filter
     */
    public List<Auction> findActiveAuctions(final AuctionFilter auctionFilter) {
        return findAuction.findActiveAuctions(auctionFilter);
    }

    /**
     * Raise a new bid for the specified auction.
     * @param auctionId                     the id of the auction to which raise bid
     * @param bidderUsername                the username of the user raising bid
     * @param bidAmount                     the amount of the raised bid
     * @return                              the old bid, if present
     * @throws AuctionNotFoundException     if no auction with the specified id exists
     * @throws InvalidBidRaiseException     if the specified bid is not valid for user,
     *                                      i.e. user has not enough money or the new bid
     *                                      does not overcome the actual one by at least 1 €
     */
    public Bid addNewBid(final String auctionId, final String bidderUsername, final BigDecimal bidAmount)
            throws AuctionNotFoundException, InvalidBidRaiseException {
        if (!movementApi.userHasSufficientBudget(bidderUsername, bidAmount)) {
            throw new InvalidBidRaiseException("Insufficient budget to bid raise");
        }
        final Bid raisedBid =  new Bid(bidAmount, bidderUsername);
        raiseBid.raise(auctionId, raisedBid).ifPresent(oldBid ->
                movementApi.unfreezeMoney(oldBid.getBidderUsername(), auctionId)
        );
        movementApi.freezeMoney(bidderUsername, bidAmount, auctionId, findAuction.findById(auctionId)
                .orElseThrow(() -> new AuctionNotFoundException(auctionId)).getProduct().name());
        auctionsHistoryApi.addParticipatingAuction(auctionId, bidderUsername);
        bidsSockets.send(BidWeb.fromDomain(raisedBid), auctionId);
        return raisedBid;
    }

    /**
     * Get all the bids raised for the specified auction.
     * @param auctionId     the desired auction id
     * @return              the list of bids raised for such auction,
     *                      ordered by timestamp
     */
    public List<Bid> getBidsHistory(final String auctionId) {
        return findBidsHistory.getBidsHistory(auctionId);
    }

    /**
     * Get the actual bid for the specified auction.
     * @param auctionId the auction id
     * @return          the actual bid
     * @throws AuctionNotFoundException if no auction with the specified id exists
     */
    public Optional<Bid> getActualBid(final String auctionId) throws AuctionNotFoundException {
        return findAuction.getActualBid(auctionId);
    }

    /**
     * Get the starting price of the specified auction.
     * @param auctionId the auction id
     * @return          the starting price
     * @throws AuctionNotFoundException if no auction with the specified id exists
     */
    public BigDecimal getStartingPrice(final String auctionId) throws AuctionNotFoundException {
        return findAuction.getStartingPrice(auctionId);
    }

    /**
     * Notify the expected closing time for the specified auction has been changed.
     * @param auctionId                     the id of the auction to update
     * @param newExpectedClosingTime        the new expected closing time for such auction
     * @throws AuctionNotFoundException     if no auction with such id exists
     */
    public void notifyExpectedClosingTimeChanged(final String auctionId, final Calendar newExpectedClosingTime)
            throws AuctionNotFoundException {
        notifyAuctionPostponed.notify(auctionId, newExpectedClosingTime);
        auctionSocket.send(new AuctionExpectedClosingTimeChanged(auctionId, newExpectedClosingTime));
    }

    /**
     * Notify the specified auction has been extended.
     * @param auctionId                     the id of the auction to notify
     * @param newExpectedClosingTime        the new expected closing time for such auction
     * @throws AuctionNotFoundException     if no auction with such id exists
     */
    public void notifyAuctionExtended(final String auctionId, final Calendar newExpectedClosingTime) {
        notifyAuctionExtension.notify(auctionId, newExpectedClosingTime);
        auctionSocket.send(new AuctionExpectedClosingTimeChanged(auctionId, newExpectedClosingTime));
    }

    /**
     * Notify the specified auction has been closed. Perform all post-auction closing
     * operations, such as exchanging money and saving histories properly.
     * @param closedAuction             the just closed auction
     * @throws ClosingAuctionException  if closing time is not set in closedAuction
     */
    public void notifyAuctionClosed(final AbstractAuctionWithLife closedAuction)
            throws WrongAuctionStateException, ClosingAuctionException {
        performOperationsPostAuctionClosing.performAfterClosingOperations(closedAuction);

        auctionsHistoryApi.removeCreatedOngoingAuction(closedAuction.getAuctionId(), closedAuction.getSellerUsername());
        auctionsHistoryApi.addCreatedEndedAuction(closedAuction.getAuctionId(), closedAuction.getSellerUsername());
        auctionsHistoryApi.getUsersParticipatingInAuction(closedAuction.getAuctionId()).stream()
                .peek(username -> auctionsHistoryApi.removeParticipatingAuction(closedAuction.getAuctionId(), username))
                .peek(username -> auctionAutomationApi.removeAuctionAutomation(closedAuction.getAuctionId(), username))
                .filter(username -> !getWinnerUsername(closedAuction).equals(username))
                .forEach(loser -> auctionsHistoryApi.addLostAuction(closedAuction.getAuctionId(), loser));

        closedAuction.getWinningBid().ifPresent(bid -> { // someone won the auction
            auctionsHistoryApi.addAwardedAuction(closedAuction.getAuctionId(), bid.getBidderUsername());
            movementApi.exchangeMoneyAfterAwardedAuction(
                    bid.getBidderUsername(), closedAuction.getSellerUsername(), getWinningBidAmount(closedAuction),
                    closedAuction.getAuctionId(), closedAuction.getProduct().name()
            );
        });
        auctionAutomationApi.removeAllAutomationsForAuction(closedAuction.getAuctionId());
        auctionSocket.send(
                new AuctionClosed(closedAuction.getAuctionId(), closedAuction.getClosingTime().orElseThrow())
        );
        removeSocketsAuctionNamespace(closedAuction.getAuctionId());
    }

    private String getWinnerUsername(final AbstractAuctionWithLife closedAuction) {
        return closedAuction.getWinningBid()
                .orElseThrow(() -> new ClosingAuctionException("winning bid is not set"))
                .getBidderUsername();
    }

    private BigDecimal getWinningBidAmount(final AbstractAuctionWithLife closedAuction) {
        return closedAuction.getWinningBid()
                .orElseThrow(() -> new ClosingAuctionException("winning bid is not set"))
                .getAmount();
    }

    private void openSocketsAuctionNamespaces() {
        findAuction.getAllActiveAuctions().forEach(auction -> {
            auctionSocket.addAuctionNamespace(auction.getAuctionId());
            bidsSockets.addAuctionNamespace(auction.getAuctionId());
        });
    }

    private void removeSocketsAuctionNamespace(final String auctionId) {
        auctionSocket.removeAuctionNamespace(auctionId);
        bidsSockets.removeAuctionNamespace(auctionId);
    }
}
