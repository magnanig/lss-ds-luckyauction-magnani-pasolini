/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.controller.config;

import it.unibo.luckyauction.auction.adapter.api.AuctionsHistoryApi;
import it.unibo.luckyauction.auction.adapter.socket.AuctionSocket;
import it.unibo.luckyauction.auction.adapter.socket.BidsSocket;
import it.unibo.luckyauction.auction.controller.AuctionController;
import it.unibo.luckyauction.auction.repository.AuctionMongoRepository;
import it.unibo.luckyauction.auction.repository.memory.AuctionMemoryCollection;
import it.unibo.luckyauction.auction.usecase.CreateAuction;
import it.unibo.luckyauction.auction.usecase.DeleteAuction;
import it.unibo.luckyauction.auction.usecase.FindAuction;
import it.unibo.luckyauction.auction.usecase.FindBidsHistory;
import it.unibo.luckyauction.auction.usecase.NotifyAuctionExtension;
import it.unibo.luckyauction.auction.usecase.NotifyAuctionPostponedOnBidRaise;
import it.unibo.luckyauction.auction.usecase.PerformOperationsPostAuctionClosing;
import it.unibo.luckyauction.auction.usecase.RaiseBid;
import it.unibo.luckyauction.auction.usecase.SynchronizeRepositories;
import it.unibo.luckyauction.auction.usecase.port.AuctionMemoryRepository;
import it.unibo.luckyauction.auction.usecase.port.AuctionRepository;
import it.unibo.luckyauction.automation.adapter.api.AuctionAutomationApi;
import it.unibo.luckyauction.movement.adapter.api.MovementApi;
import it.unibo.luckyauction.util.port.IdGenerator;
import it.unibo.luckyauction.uuid.UUIdGenerator;

import static it.unibo.luckyauction.configuration.ConfigurationConstants.TEST_MODE;

public class AuctionConfig {

    private final AuctionRepository auctionRepository;
    private final AuctionMemoryRepository auctionMemoryRepository = new AuctionMemoryCollection();
    private final IdGenerator auctionIdGenerator = new UUIdGenerator();
    private final IdGenerator auctionsHistoryIdGenerator = new UUIdGenerator();

    public AuctionConfig() {
        this.auctionRepository = TEST_MODE
                ? AuctionMongoRepository.testRepository()
                : AuctionMongoRepository.defaultRepository();
    }

    /**
     * Build the auction controller of the application, capable of orchestrating auctions.
     * See {@link AuctionController} for more details.
     * @return      the just built auction controller
     */
    public AuctionController auctionController(final MovementApi movementApi,
                                               final AuctionsHistoryApi auctionsHistoryApi,
                                               final AuctionAutomationApi auctionAutomationApi,
                                               final AuctionSocket auctionSocket, final BidsSocket bidsSockets) {
        return new AuctionController(createAuction(), deleteAuction(), findAuction(),
                raiseBid(), findBidsHistory(), notifyAuctionExtension(), notifyAuctionPostponedOnBidRaise(),
                closeAuction(), synchronizeRepositories(), movementApi, auctionsHistoryApi, auctionAutomationApi,
                auctionSocket, bidsSockets);
    }

    private CreateAuction createAuction() {
        return new CreateAuction(auctionRepository, auctionMemoryRepository, auctionIdGenerator, auctionsHistoryIdGenerator);
    }

    private DeleteAuction deleteAuction() {
        return new DeleteAuction(auctionRepository, auctionMemoryRepository);
    }

    private FindAuction findAuction() {
        return new FindAuction(auctionRepository, auctionMemoryRepository);
    }

    private RaiseBid raiseBid() {
        return new RaiseBid(auctionRepository, auctionMemoryRepository);
    }

    private FindBidsHistory findBidsHistory() {
        return new FindBidsHistory(auctionRepository);
    }

    private NotifyAuctionExtension notifyAuctionExtension() {
        return new NotifyAuctionExtension(auctionRepository);
    }

    private NotifyAuctionPostponedOnBidRaise notifyAuctionPostponedOnBidRaise() {
        return new NotifyAuctionPostponedOnBidRaise(auctionRepository);
    }

    private PerformOperationsPostAuctionClosing closeAuction() {
        return new PerformOperationsPostAuctionClosing(auctionRepository, auctionMemoryRepository);
    }

    private SynchronizeRepositories synchronizeRepositories() {
        return new SynchronizeRepositories(auctionRepository, auctionMemoryRepository);
    }
}
