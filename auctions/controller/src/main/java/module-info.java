module lss.ds.luckyauction.magnani.pasolini.auctions.controller.main {
    exports it.unibo.luckyauction.auction.controller;
    exports it.unibo.luckyauction.auction.controller.config;
    exports it.unibo.luckyauction.auction.controller.exception;
    requires lss.ds.luckyauction.magnani.pasolini.configuration.main;
    requires lss.ds.luckyauction.magnani.pasolini.utils.main;
    requires lss.ds.luckyauction.magnani.pasolini.auctions.domain.main;
    requires lss.ds.luckyauction.magnani.pasolini.auctions.usecase.main;
    requires lss.ds.luckyauction.magnani.pasolini.auctions.adapter.main;
    requires lss.ds.luckyauction.magnani.pasolini.movements.adapter.main;
    requires lss.ds.luckyauction.magnani.pasolini.users.adapter.main;
    requires lss.ds.luckyauction.magnani.pasolini.auctions.repository.mongo.main;
    requires lss.ds.luckyauction.magnani.pasolini.uuid.generator.main;
    requires lss.ds.luckyauction.magnani.pasolini.automations.adapter.main;
    requires lss.ds.luckyauction.magnani.pasolini.auctions.repository.memory.main;
}