/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.controller;

import it.unibo.luckyauction.auction.controller.config.AuctionConfig;
import it.unibo.luckyauction.auction.controller.config.AuctionsHistoryConfig;
import it.unibo.luckyauction.auction.domain.AuctionTestUtils;
import it.unibo.luckyauction.auction.domain.entity.Auction;
import it.unibo.luckyauction.auction.domain.entity.AuctionsHistory;
import it.unibo.luckyauction.auction.domain.valueobject.AwardedAuction;
import it.unibo.luckyauction.auction.domain.valueobject.Bid;
import it.unibo.luckyauction.auction.domain.valueobject.CreatedEndedAuction;
import it.unibo.luckyauction.auction.domain.valueobject.CreatedOngoingAuction;
import it.unibo.luckyauction.auction.domain.valueobject.LostAuction;
import it.unibo.luckyauction.auction.domain.valueobject.ParticipatingAuction;
import it.unibo.luckyauction.auction.usecase.exception.AuctionNotAllowedException;
import it.unibo.luckyauction.auction.usecase.exception.AuctionNotFoundException;
import it.unibo.luckyauction.auction.usecase.exception.InvalidBidRaiseException;
import it.unibo.luckyauction.auction.usecase.port.AuctionFilter;
import it.unibo.luckyauction.movement.controller.MovementController;
import it.unibo.luckyauction.movement.controller.config.MovementConfig;
import it.unibo.luckyauction.movement.domain.entity.AwardedAuctionTransaction;
import it.unibo.luckyauction.movement.domain.entity.Freezing;
import it.unibo.luckyauction.movement.domain.valueobject.Budget;
import it.unibo.luckyauction.spring.util.api.SpringAuctionApi;
import it.unibo.luckyauction.spring.util.api.SpringAuctionAutomationApi;
import it.unibo.luckyauction.spring.util.api.SpringAuctionsHistoryApi;
import it.unibo.luckyauction.spring.util.api.SpringMovementApi;
import it.unibo.luckyauction.spring.util.api.SpringUserApi;
import it.unibo.luckyauction.user.controller.UserController;
import it.unibo.luckyauction.user.controller.config.UserConfig;
import it.unibo.luckyauction.user.domain.UserTestUtils;
import it.unibo.luckyauction.util.NumericalConstants;
import it.unibo.luckyauction.util.Sleeper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.math.BigDecimal;
import java.time.Duration;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;

import static it.unibo.luckyauction.auction.domain.AuctionTestUtils.NEW_BID_RAISE_AMOUNT;
import static it.unibo.luckyauction.auction.domain.AuctionTestUtils.SELLER_USERNAME;
import static it.unibo.luckyauction.auction.domain.AuctionTestUtils.WINNER_USERNAME;
import static it.unibo.luckyauction.user.domain.UserTestUtils.PASSWORD;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class AuctionAndAuctionsHistoryControllerTests {
    private final UserController userController = new UserConfig().userController(new SpringMovementApi(),
            new SpringAuctionsHistoryApi(), new SpringAuctionAutomationApi());
    private final MovementController movementController = new MovementConfig().movementController();
    private final AuctionController auctionController = new AuctionConfig().auctionController(new SpringMovementApi(),
            new SpringAuctionsHistoryApi(), new SpringAuctionAutomationApi(), new EmptyAuctionSocket(),
            new EmptyBidsSocket());
    private final AuctionsHistoryController auctionsHistoryController = new AuctionsHistoryConfig()
            .auctionsHistoryController(new SpringAuctionApi(), new SpringUserApi());

    private static final Duration TWO_SECONDS_DURATION = Duration.ofSeconds(2);
    private static final BigDecimal INITIAL_PRICE = BigDecimal.ONE;
    private static final Supplier<Auction.AuctionBuilder> PREBUILT_AUCTION = () -> Auction.builder()
            .product(AuctionTestUtils.IPHONE_PRODUCT.get()).startingPrice(INITIAL_PRICE)
            .duration(TWO_SECONDS_DURATION).sellerUsername(SELLER_USERNAME);
    private static final BigDecimal WINNING_BID_AMOUNT = INITIAL_PRICE.add(BigDecimal.valueOf(4));
    private static final String LOSER_USERNAME = "loserUsername";
    private final Budget winnerBudget = new Budget(new BigDecimal(50));
    private final Budget loserBudget = new Budget(new BigDecimal(50));
    private final Budget sellerBudget = new Budget();

    @BeforeEach
    void createUsersAndHistories() throws InterruptedException {
        createUser(WINNER_USERNAME, winnerBudget.value());
        createUser(LOSER_USERNAME, loserBudget.value());
        createUser(SELLER_USERNAME);
        Thread.sleep(NumericalConstants.FIFTY);
    }

    @AfterEach
    void deleteUsersAndAuctions() {
        deleteUserAndAuctions(WINNER_USERNAME);
        deleteUserAndAuctions(LOSER_USERNAME);
        deleteUserAndAuctions(SELLER_USERNAME);
    }

    @Test
    void testEmptyAuctionLifeCycle() throws InterruptedException {
        final Auction auction = auctionController.createAndStart(PREBUILT_AUCTION.get().build());
        assertTrue(equalsEmptyAuction(PREBUILT_AUCTION.get().build(), auction));
        assertSame(auction, auctionController.findById(auction.getAuctionId()).orElseThrow());
        assertEquals(INITIAL_PRICE, auctionController.getStartingPrice(auction.getAuctionId()));
        assertTrue(auctionController.getBidsHistory(auction.getAuctionId()).isEmpty());
        assertTrue(auctionController.getActualBid(auction.getAuctionId()).isEmpty());

        final Exception invalidBidRaiseException = assertThrows(InvalidBidRaiseException.class,
                () -> auctionController.addNewBid(auction.getAuctionId(), LOSER_USERNAME, NEW_BID_RAISE_AMOUNT));
        assertTrue(invalidBidRaiseException.getMessage().contains("Insufficient budget to bid raise"));

        // wait for auction end
        new Sleeper().sleepEquallyMoreThan(auction.getDuration().toMillis());
        assertFalse(auction.isActive());
        assertEquals(Optional.empty(), auction.getWinningBid()); // nobody raised bid
        final Auction endedAuction = auctionController.findById(auction.getAuctionId()).orElseThrow();
        assertNotSame(auction, endedAuction); // now must be a copy, since original object must be removed from in-memory db
        assertFalse(endedAuction.isActive());
        assertTrue(auction.getClosingTime().isPresent());
        assertEquals(Optional.empty(), endedAuction.getWinningBid());
        assertEquals(auction.getClosingTime(), endedAuction.getClosingTime());
    }

    @Test
    void testEmptyExtendableAuctionLifeCycle() throws InterruptedException {
        final Sleeper s = new Sleeper();
        final Duration extensionAmount = Duration.ofSeconds(2);
        final Auction auctionToCreate = PREBUILT_AUCTION.get().extensionDuration(extensionAmount).build();
        final Auction auction = auctionController.createAndStart(auctionToCreate);
        assertTrue(equalsEmptyAuction(auctionToCreate, auction));
        assertSame(auction, auctionController.findById(auction.getAuctionId()).orElseThrow());
        // wait for auction end
        s.sleepEquallyMoreThan(auction.getDuration().toMillis());
        // here auction should be extended
        assertTrue(auction.isActive());
        s.sleepEquallyMoreThan(extensionAmount.toMillis());
        assertFalse(auction.isActive());
        assertTrue(auction.getClosingTime().isPresent());
        assertEquals(Optional.empty(), auction.getWinningBid()); // nobody raised bid
        final Auction endedAuction = auctionController.findById(auction.getAuctionId()).orElseThrow();
        assertNotSame(auction, endedAuction); // now must be a copy, since original object must be removed from in-memory db
        assertFalse(endedAuction.isActive());
        assertEquals(Optional.empty(), endedAuction.getWinningBid());
        assertEquals(auction.getClosingTime(), endedAuction.getClosingTime());
    }

    @Test
    void testAuctionWithWinnerLifeCycle() throws InterruptedException {
        final Auction auction = auctionController
                .createAndStart(PREBUILT_AUCTION.get().duration(Duration.ofSeconds(4)).build());
        final String auctionId = auction.getAuctionId();
        assertTrue(equalsEmptyAuction(PREBUILT_AUCTION.get().duration(Duration.ofSeconds(4)).build(), auction));
        assertSame(auction, auctionController.findById(auction.getAuctionId()).orElseThrow());
        assertTrue(auctionsHistoryController.getCreatedEndedAuction(auctionId, SELLER_USERNAME).isEmpty());
        assertEquals(auctionId, auctionsHistoryController.getCreatedOngoingAuction(auctionId, SELLER_USERNAME)
                .orElseThrow().getAuctionId());

        final BigDecimal loserBidAmount = WINNING_BID_AMOUNT.subtract(INITIAL_PRICE);
        final Bid loserBid = auctionController.addNewBid(auction.getAuctionId(), LOSER_USERNAME, loserBidAmount);
        assertTrue(bidsCorresponding(loserBidAmount, LOSER_USERNAME, loserBid));
        Thread.sleep(10); // just wait a bit
        assertEquals(auctionId, auctionsHistoryController.getParticipatingAuction(auctionId, LOSER_USERNAME)
                .orElseThrow().getAuctionId());

        final Bid winningBid = auctionController.addNewBid(auction.getAuctionId(), WINNER_USERNAME,
                WINNING_BID_AMOUNT);
        assertTrue(bidsCorresponding(WINNING_BID_AMOUNT, WINNER_USERNAME, winningBid));
        // wait for auction end
        new Sleeper(NumericalConstants.TWO_HUNDRED).sleepEquallyMoreThan(auction.remainingTime().toMillis());
        assertFalse(auction.isActive());
        assertEquals(Optional.of(winningBid), auction.getWinningBid());
        assertTrue(auction.getClosingTime().isPresent());
        final Auction endedAuction = auctionController.findById(auction.getAuctionId()).orElseThrow();
        assertNotSame(auction, endedAuction); // now must be a copy, since original object must be removed from in-memory db
        assertFalse(endedAuction.isActive());
        assertEquals(Optional.of(winningBid), endedAuction.getWinningBid());
        assertEquals(auction.getClosingTime(), endedAuction.getClosingTime());
        assertEquals(auctionId,
                auctionsHistoryController.getLostAuction(auctionId, LOSER_USERNAME).orElseThrow().getAuctionId());
        assertEquals(auctionId,
                auctionsHistoryController.getAwardedAuction(auctionId, WINNER_USERNAME).orElseThrow().getAuctionId());
    }

    @Test
    void testAuctionsHistoriesForEmptyAuction() throws InterruptedException {
        final Auction auction = auctionController.createAndStart(PREBUILT_AUCTION.get().build());
        AuctionsHistory sellerAuctionsHistory = auctionsHistoryController.findByUsername(SELLER_USERNAME).orElseThrow();
        assertEquals(List.of(CreatedOngoingAuction.fromAuction(auction)),
                sellerAuctionsHistory.getCreatedOngoingAuctions());
        // wait for auction end
        Thread.sleep(auction.getDuration().toMillis() + NumericalConstants.TWO_HUNDRED);
        sellerAuctionsHistory = auctionsHistoryController.findByUsername(SELLER_USERNAME).orElseThrow();
        assertTrue(sellerAuctionsHistory.getCreatedOngoingAuctions().isEmpty());
        assertEquals(List.of(CreatedEndedAuction.fromAuctionWithoutWinner(auction)),
                sellerAuctionsHistory.getCreatedEndedAuctions());

        // auction filters tests
        assertEquals(auction.getAuctionId(), auctionController.getAllAuctions().stream()
                .findFirst().orElseThrow().getAuctionId());
        assertTrue(auctionController.findActiveAuctions().isEmpty());
        assertTrue(auctionController
                .findActiveAuctions(new AuctionFilter.Builder().exceptUsername(SELLER_USERNAME).build()).isEmpty());
    }

    @Test
    void testAuctionsHistoriesForAuctionWithWinner() throws InterruptedException {
        final Auction auction = auctionController
                .createAndStart(PREBUILT_AUCTION.get().duration(Duration.ofSeconds(NumericalConstants.FIVE)).build());
        AuctionsHistory sellerAuctionsHistory = auctionsHistoryController.findByUsername(SELLER_USERNAME).orElseThrow();
        assertTrue(sellerAuctionsHistory.getCreatedEndedAuctions().isEmpty());
        assertEquals(List.of(CreatedOngoingAuction.fromAuction(auction)),
                sellerAuctionsHistory.getCreatedOngoingAuctions());
        // LOSER_USER make a bid
        final BigDecimal actualBidAmount = WINNING_BID_AMOUNT.subtract(INITIAL_PRICE); // actualBid = 5 - 1 = 4
        auctionController.addNewBid(auction.getAuctionId(), LOSER_USERNAME, actualBidAmount);
        AuctionsHistory loserAuctionsHistory = auctionsHistoryController.findByUsername(LOSER_USERNAME).orElseThrow();
        assertEquals(List.of(ParticipatingAuction.fromAuction(auction)),
                loserAuctionsHistory.getParticipatingAuctions());
        // WINNER_USER make a new bid
        auctionController.addNewBid(auction.getAuctionId(), WINNER_USERNAME, WINNING_BID_AMOUNT);
        AuctionsHistory winnerAuctionsHistory =
                auctionsHistoryController.findByUsername(WINNER_USERNAME).orElseThrow();
        assertEquals(List.of(ParticipatingAuction.fromAuction(auction)),
                winnerAuctionsHistory.getParticipatingAuctions());
        // wait for auction end wait to send request to update auctions history
        Thread.sleep(auction.remainingTime().toMillis() + NumericalConstants.FIVE_HUNDRED);
        assertFalse(auction.isActive());

        // let time to send request to update auctions history
        sellerAuctionsHistory = auctionsHistoryController.findByUsername(SELLER_USERNAME).orElseThrow();
        assertTrue(sellerAuctionsHistory.getCreatedOngoingAuctions().isEmpty());
        assertTrue(sellerAuctionsHistory.getCreatedEndedAuctions().contains(CreatedEndedAuction
                .fromAuctionWithWinner(auction, AuctionTestUtils.CONTACTS.get(), AuctionTestUtils.CONTACTS.get())),
                "Found: " + sellerAuctionsHistory.getCreatedEndedAuctions().toString());

        loserAuctionsHistory = auctionsHistoryController.findByUsername(LOSER_USERNAME).orElseThrow();
        assertTrue(loserAuctionsHistory.getParticipatingAuctions().isEmpty());
        assertTrue(loserAuctionsHistory.getLostAuctions().contains(LostAuction.fromAuction(auction)));

        winnerAuctionsHistory = auctionsHistoryController.findByUsername(WINNER_USERNAME).orElseThrow();
        assertTrue(winnerAuctionsHistory.getParticipatingAuctions().isEmpty());
        assertTrue(winnerAuctionsHistory.getAwardedAuctions().contains(AwardedAuction
                .fromAuction(auction, AuctionTestUtils.CONTACTS.get(), AuctionTestUtils.CONTACTS.get())
        ));

        // the BUYER user asks to delete the auction created by the SELLER user
        final Exception deleteAuctionException = assertThrows(AuctionNotAllowedException.class,
                () -> auctionController.deleteById(auction.getAuctionId(), WINNER_USERNAME));
        assertTrue(deleteAuctionException.getMessage().contains("they are not the creator of such auction"));

        auctionNotFoundExceptionTest();
    }

    @Test
    void testNoTransactionsOnEmptyAuction() throws InterruptedException {
        final Auction auction = auctionController.createAndStart(PREBUILT_AUCTION.get().build());
        // wait for auction end
        new Sleeper().sleepEquallyMoreThan(auction.getDuration().toMillis());
        assertFalse(auction.isActive());
        assertEquals(Optional.empty(), auction.getWinningBid()); // nobody raised bid
        assertTrue(movementController.getTransactions(SELLER_USERNAME).isEmpty());
        assertTrue(movementController.getFreezes(SELLER_USERNAME).isEmpty());
        assertEquals(new Budget(), movementController.getUserBudget(SELLER_USERNAME));
    }

    @Test
    void testMovementsForAuctionWithWinner() throws InterruptedException {
        final Auction auction = auctionController
                .createAndStart(PREBUILT_AUCTION.get().duration(Duration.ofSeconds(4)).build());
        final BigDecimal loserBidAmount = WINNING_BID_AMOUNT.subtract(INITIAL_PRICE);

        // 1. loserUser raise a bid
        auctionController.addNewBid(auction.getAuctionId(), LOSER_USERNAME, loserBidAmount);
        assertTrue(movementController.getTransactions(SELLER_USERNAME).isEmpty());
        assertTrue(movementController.getFreezes(SELLER_USERNAME).isEmpty());
        final Freezing loserFreezes = movementController.getFreezes(LOSER_USERNAME).stream().findFirst().orElseThrow();
        assertEquals(auction.getAuctionId(), loserFreezes.getAuctionId());
        assertEquals(loserBidAmount, loserFreezes.getAmount());
        assertEquals(loserBudget.subtract(loserBidAmount), movementController.getUserBudget(LOSER_USERNAME));
        assertEquals(sellerBudget, movementController.getUserBudget(SELLER_USERNAME));

        // 2. winnerUser raise a new bid, that later will become the winning bid
        final Bid winningBid = auctionController.addNewBid(auction.getAuctionId(), WINNER_USERNAME,
                WINNING_BID_AMOUNT);
        assertTrue(movementController.getTransactions(SELLER_USERNAME).isEmpty());
        assertTrue(movementController.getFreezes(SELLER_USERNAME).isEmpty());
        assertTrue(movementController.getFreezes(LOSER_USERNAME).isEmpty()); // money must be unfrozen now
        final Freezing winnerFreezes = movementController
                .getFreezes(WINNER_USERNAME).stream().findFirst().orElseThrow();
        assertEquals(auction.getAuctionId(), winnerFreezes.getAuctionId());
        assertEquals(WINNING_BID_AMOUNT, winnerFreezes.getAmount());
        assertEquals(loserBudget, movementController.getUserBudget(LOSER_USERNAME));
        assertEquals(winnerBudget.subtract(WINNING_BID_AMOUNT), movementController.getUserBudget(WINNER_USERNAME));
        assertEquals(sellerBudget, movementController.getUserBudget(SELLER_USERNAME));

        // wait for auction end
        Thread.sleep(auction.remainingTime().toMillis() + NumericalConstants.TWO_HUNDRED);
        assertFalse(auction.isActive());
        assertEquals(Optional.of(winningBid), auction.getWinningBid());

        // 3. auction has been ended: let's check if money have been successfully exchanged
        assertTrue(movementController.getFreezes(SELLER_USERNAME).isEmpty());
        assertTrue(movementController.getFreezes(LOSER_USERNAME).isEmpty());
        assertTrue(movementController.getFreezes(WINNER_USERNAME).isEmpty()); // money must be unfrozen now
        final AwardedAuctionTransaction sellerTransaction = (AwardedAuctionTransaction) movementController
                .getTransactions(SELLER_USERNAME).stream()
                .filter(t -> t instanceof AwardedAuctionTransaction)
                .findFirst().orElseThrow();
        final AwardedAuctionTransaction winnerTransaction = (AwardedAuctionTransaction) movementController
                .getTransactions(WINNER_USERNAME).stream()
                .filter(t -> t instanceof AwardedAuctionTransaction)
                .findFirst().orElseThrow();

        assertEquals(auction.getAuctionId(), sellerTransaction.getAuctionId());
        assertEquals(SELLER_USERNAME, sellerTransaction.getSeller());
        assertEquals(WINNER_USERNAME, sellerTransaction.getBuyer());
        assertEquals(WINNING_BID_AMOUNT, sellerTransaction.getAmount());
        assertEquals(auction.getProduct().name(), sellerTransaction.getProductName());

        assertEquals(sellerTransaction.getAuctionId(), winnerTransaction.getAuctionId());
        assertEquals(sellerTransaction.getSeller(), winnerTransaction.getSeller());
        assertEquals(sellerTransaction.getBuyer(), winnerTransaction.getBuyer());
        assertEquals(sellerTransaction.getAmount().negate(), winnerTransaction.getAmount());
        assertEquals(sellerTransaction.getProductName(), winnerTransaction.getProductName());
        assertNotEquals(sellerTransaction.getMovementId(), winnerTransaction.getMovementId());

        assertEquals(loserBudget, movementController.getUserBudget(LOSER_USERNAME));
        assertEquals(winnerBudget.subtract(WINNING_BID_AMOUNT), movementController.getUserBudget(WINNER_USERNAME));
        assertEquals(sellerBudget.sum(WINNING_BID_AMOUNT), movementController.getUserBudget(SELLER_USERNAME));
    }

    private void auctionNotFoundExceptionTest() {
        final Exception getActualBidException = assertThrows(AuctionNotFoundException.class,
                () -> auctionController.getActualBid("wrongAuctionId"));
        assertTrue(getActualBidException.getMessage().contains("not found"));

        final Exception getStartingPriceException = assertThrows(AuctionNotFoundException.class,
                () -> auctionController.getStartingPrice("wrongAuctionId"));
        assertTrue(getStartingPriceException.getMessage().contains("not found"));
    }

    private boolean equalsEmptyAuction(final Auction expected, final Auction actual) {
        return expected.getProduct().equals(actual.getProduct())
                && expected.getStartingPrice().equals(actual.getStartingPrice())
                && expected.getDuration().equals(actual.getDuration())
                && expected.getExtensionAmount().equals(actual.getExtensionAmount())
                && expected.getSellerUsername().equals(actual.getSellerUsername());
    }

    private boolean bidsCorresponding(final BigDecimal expectedBidAmount, final String expectedUsername,
                                      final Bid actualBid) {
        return expectedBidAmount.compareTo(actualBid.getAmount()) == 0
                && expectedUsername.equals(actualBid.getBidderUsername());
    }

    private void createUser(final String username, final BigDecimal... budget) {
        if (userController.findByUsername(username).isPresent()) {
            userController.deleteByUsername(username);
            auctionController.deleteByUsername(username);
        }
        userController.create(UserTestUtils.createNewUser(username), PASSWORD);

        assertTrue(userController.findByUsername(username).isPresent());
        assertTrue(movementController.findMovementsHistoryByUsername(username).isPresent());
        assertTrue(auctionsHistoryController.findByUsername(username).isPresent());
        if (budget.length > 0) {
            movementController.rechargeBudget(budget[0], username, null);
        }
    }

    private void deleteUserAndAuctions(final String username) {
        userController.deleteByUsername(username).ifPresentOrElse(user ->
                assertEquals(username, user.getUsername()), Assertions::fail);
        auctionController.deleteByUsername(username);
    }
}
