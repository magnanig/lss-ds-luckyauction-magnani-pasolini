/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.controller;

import it.unibo.luckyauction.auction.controller.config.AuctionsHistoryConfig;
import it.unibo.luckyauction.auction.controller.exception.UserNotFoundException;
import it.unibo.luckyauction.auction.controller.exception.WrongAuctionStateException;
import it.unibo.luckyauction.auction.domain.AuctionTestUtils;
import it.unibo.luckyauction.auction.domain.entity.Auction;
import it.unibo.luckyauction.auction.domain.entity.AuctionsHistory;
import it.unibo.luckyauction.auction.domain.valueobject.Bid;
import it.unibo.luckyauction.auction.repository.AuctionMongoRepository;
import it.unibo.luckyauction.auction.repository.memory.AuctionMemoryCollection;
import it.unibo.luckyauction.auction.usecase.CreateAuction;
import it.unibo.luckyauction.auction.usecase.DeleteAuction;
import it.unibo.luckyauction.auction.usecase.RaiseBid;
import it.unibo.luckyauction.auction.usecase.exception.AuctionNotFoundException;
import it.unibo.luckyauction.auction.usecase.port.AuctionMemoryRepository;
import it.unibo.luckyauction.auction.usecase.port.AuctionRepository;
import it.unibo.luckyauction.spring.util.api.SpringAuctionApi;
import it.unibo.luckyauction.spring.util.api.SpringAuctionAutomationApi;
import it.unibo.luckyauction.spring.util.api.SpringAuctionsHistoryApi;
import it.unibo.luckyauction.spring.util.api.SpringMovementApi;
import it.unibo.luckyauction.spring.util.api.SpringUserApi;
import it.unibo.luckyauction.user.controller.UserController;
import it.unibo.luckyauction.user.controller.config.UserConfig;
import it.unibo.luckyauction.user.domain.UserTestUtils;
import it.unibo.luckyauction.util.NumericalConstants;
import it.unibo.luckyauction.uuid.UUIdGenerator;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.time.Duration;
import java.util.Calendar;
import java.util.List;

import static it.unibo.luckyauction.auction.domain.AuctionTestUtils.AUCTION_TEST_ID;
import static it.unibo.luckyauction.auction.domain.AuctionTestUtils.SECOND_USERNAME;
import static it.unibo.luckyauction.auction.domain.AuctionTestUtils.SELLER_USERNAME;
import static it.unibo.luckyauction.user.domain.UserTestUtils.FIRST_USERNAME;
import static it.unibo.luckyauction.user.domain.UserTestUtils.PASSWORD;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class AuctionsHistoryControllerTests {
    // define create, raiseBid and delete use cases to manage auction on database without controller controls
    private static final AuctionMemoryRepository AUCTION_MEMORY_REPOSITORY = new AuctionMemoryCollection();
    private static final AuctionRepository AUCTION_REPOSITORY = AuctionMongoRepository.testRepository();
    private final CreateAuction createAuction = new CreateAuction(AUCTION_REPOSITORY, AUCTION_MEMORY_REPOSITORY,
            new UUIdGenerator(), new UUIdGenerator());
    private final RaiseBid raiseBid = new RaiseBid(AUCTION_REPOSITORY, AUCTION_MEMORY_REPOSITORY);
    private final DeleteAuction deleteAuction = new DeleteAuction(AUCTION_REPOSITORY, AUCTION_MEMORY_REPOSITORY);
    // define user and auctions history controller
    private final UserController userController = new UserConfig().userController(new SpringMovementApi(),
            new SpringAuctionsHistoryApi(), new SpringAuctionAutomationApi());
    private final AuctionsHistoryController auctionsHistoryController = new AuctionsHistoryConfig()
            .auctionsHistoryController(new SpringAuctionApi(), new SpringUserApi());
    private Calendar aDayAgo;
    private Calendar twoDaysBefore;

    @BeforeEach
    void createUsersAndHistories() throws InterruptedException {
        aDayAgo = Calendar.getInstance();
        aDayAgo.add(Calendar.DATE, -1);
        twoDaysBefore = Calendar.getInstance();
        twoDaysBefore.add(Calendar.DATE, -NumericalConstants.TWO);

        if (userController.findByUsername(SELLER_USERNAME).isPresent()) {
            userController.deleteByUsername(SELLER_USERNAME);
            deleteAuction.deleteByUsername(SELLER_USERNAME);
        }
        userController.create(UserTestUtils.createNewUser(SELLER_USERNAME), PASSWORD);
        Thread.sleep(NumericalConstants.FIFTY);
    }

    @AfterEach
    void deleteUsersAndAuction() {
        userController.deleteByUsername(SELLER_USERNAME).ifPresentOrElse(user ->
                assertEquals(SELLER_USERNAME, user.getUsername()), Assertions::fail);
        deleteAuction.deleteByUsername(SELLER_USERNAME);
        userController.deleteByUsername(FIRST_USERNAME);
        userController.deleteByUsername(SECOND_USERNAME);
        auctionsHistoryController.deleteByUsername(FIRST_USERNAME);
    }

    @Test
    void userContactsNotFoundException() {
        final Bid winningBid = new Bid(AuctionTestUtils.BID_RAISE_AMOUNT, FIRST_USERNAME, aDayAgo);
        final Auction endedAuctionToCreate = Auction.builder().auctionId(AUCTION_TEST_ID)
                .creationTime(twoDaysBefore).product(AuctionTestUtils.IPHONE_PRODUCT.get())
                .sellerUsername(SELLER_USERNAME).startingPrice(AuctionTestUtils.AUCTION_PRICE)
                .closingTime(Calendar.getInstance()).bidsHistoryId(AuctionTestUtils.BIDS_HISTORY_ID)
                .bids(List.of(winningBid)).winningBid(winningBid).build();
        createAuction.createEndedAuctionOnlyForTests(endedAuctionToCreate);
        // create auctions history for FIRST_USERNAME user
        auctionsHistoryController.createAuctionsHistory(
                new AuctionsHistory(AuctionTestUtils.FIRST_AUCTIONS_HISTORY_ID, FIRST_USERNAME));
        auctionsHistoryController.findByUsername(FIRST_USERNAME).ifPresentOrElse(auctionsHistory ->
                assertEquals(FIRST_USERNAME, auctionsHistory.getUsername()), Assertions::fail);

        final Exception auctionNotFoundException = assertThrows(UserNotFoundException.class,
                () -> auctionsHistoryController.addAwardedAuction(AUCTION_TEST_ID, FIRST_USERNAME));
        assertTrue(auctionNotFoundException.getMessage().contains("User " + FIRST_USERNAME + " not found"));

        // delete auctions history for FIRST_USERNAME user
        auctionsHistoryController.deleteByUsername(FIRST_USERNAME);
    }

    @Test
    void createEndedAuctionTest() {
        final Calendar loserBidTimestamp = Calendar.getInstance();
        loserBidTimestamp.add(Calendar.DATE, -1);
        loserBidTimestamp.add(Calendar.HOUR, -1);
        userController.create(UserTestUtils.createNewUser(FIRST_USERNAME), PASSWORD);
        userController.create(UserTestUtils.createNewUser(SECOND_USERNAME), PASSWORD);
        // create an ended auction with the SECOND user who lost and the FIRST user who won
        final Bid loserBid = new Bid(AuctionTestUtils.BID_RAISE_AMOUNT, SECOND_USERNAME, loserBidTimestamp);
        final Bid winningBid = new Bid(AuctionTestUtils.NEW_BID_RAISE_AMOUNT, FIRST_USERNAME, aDayAgo);
        final Auction endedAuctionToCreate = Auction.builder().auctionId(AUCTION_TEST_ID)
                .creationTime(twoDaysBefore).product(AuctionTestUtils.IPHONE_PRODUCT.get())
                .sellerUsername(SELLER_USERNAME).startingPrice(AuctionTestUtils.AUCTION_PRICE)
                .closingTime(Calendar.getInstance()).bidsHistoryId(AuctionTestUtils.BIDS_HISTORY_ID)
                .bids(List.of(loserBid, winningBid)).winningBid(winningBid).build();
        createAuction.createEndedAuctionOnlyForTests(endedAuctionToCreate);

        // check exception
        final Exception auctionNotFoundException = assertThrows(AuctionNotFoundException.class,
                () -> auctionsHistoryController.addLostAuction("wrongId", FIRST_USERNAME));
        assertTrue(auctionNotFoundException.getMessage().contains("not found"));

        // add the auction in their respective auctions histories
        auctionsHistoryController.addLostAuction(AUCTION_TEST_ID, SECOND_USERNAME);
        auctionsHistoryController.addAwardedAuction(AUCTION_TEST_ID, FIRST_USERNAME);
        auctionsHistoryController.addCreatedEndedAuction(AUCTION_TEST_ID, SELLER_USERNAME);

        assertTrue(auctionsHistoryController.getCreatedOngoingAuctions(SELLER_USERNAME).isEmpty());
        assertTrue(auctionsHistoryController.getParticipatingAuctions(SELLER_USERNAME).isEmpty());
        assertEquals(AUCTION_TEST_ID, auctionsHistoryController.getCreatedEndedAuctions(SELLER_USERNAME)
                .stream().findFirst().orElseThrow().getAuctionId());
        assertEquals(AUCTION_TEST_ID, auctionsHistoryController.getAwardedAuctions(FIRST_USERNAME)
                .stream().findFirst().orElseThrow().getAuctionId());
        assertEquals(AUCTION_TEST_ID, auctionsHistoryController.getLostAuctions(SECOND_USERNAME)
                .stream().findFirst().orElseThrow().getAuctionId());

        assertTrue(auctionsHistoryController.getUsersParticipatingInAuction(AUCTION_TEST_ID).isEmpty());
    }

    @Test
    void createOngoingAuction() throws InterruptedException {
        userController.create(UserTestUtils.createNewUser(FIRST_USERNAME), PASSWORD);
        final Calendar afterOneSecond = Calendar.getInstance();
        afterOneSecond.add(Calendar.SECOND, 1);
        final Auction ongoingAuctionToCreate = Auction.builder().product(AuctionTestUtils.SHOES_PRODUCT.get())
                .sellerUsername(SELLER_USERNAME).startingPrice(AuctionTestUtils.AUCTION_PRICE)
                .duration(Duration.ofSeconds(3)).build();
        final String auctionId = createAuction.createAndStart(ongoingAuctionToCreate).getAuctionId();

        // check exception
        final Exception wrongAuctionStateException = assertThrows(WrongAuctionStateException.class,
                () -> auctionsHistoryController.addLostAuction(auctionId, FIRST_USERNAME));
        assertTrue(wrongAuctionStateException.getMessage().contains("Auction#" + auctionId
                + " is expected to be ended but it isn't"));

        auctionsHistoryController.addCreatedOngoingAuction(auctionId, SELLER_USERNAME);
        assertEquals(auctionId, auctionsHistoryController.getCreatedOngoingAuctions(SELLER_USERNAME)
                .stream().findFirst().orElseThrow().getAuctionId());

        Thread.sleep(Duration.ofSeconds(1).toMillis());

        raiseBid.raise(auctionId, new Bid(AuctionTestUtils.BID_RAISE_AMOUNT, FIRST_USERNAME));
        auctionsHistoryController.addParticipatingAuction(auctionId, FIRST_USERNAME);
        assertEquals(auctionId, auctionsHistoryController.getParticipatingAuctions(FIRST_USERNAME)
                .stream().findFirst().orElseThrow().getAuctionId());
        // wait for the end of the auction
        Thread.sleep(Duration.ofSeconds(2).toMillis());

        auctionsHistoryController.removeCreatedOngoingAuction(auctionId, SELLER_USERNAME);
        auctionsHistoryController.removeParticipatingAuction(auctionId, FIRST_USERNAME);

        auctionsHistoryController.removeAuctionFromAllHistory(auctionId);
        assertTrue(auctionsHistoryController.getCreatedOngoingAuctions(SELLER_USERNAME).isEmpty());
        assertTrue(auctionsHistoryController.getCreatedEndedAuctions(SELLER_USERNAME).isEmpty());
        assertTrue(auctionsHistoryController.getParticipatingAuctions(FIRST_USERNAME).isEmpty());
        assertTrue(auctionsHistoryController.getAwardedAuctions(FIRST_USERNAME).isEmpty());
    }
}
