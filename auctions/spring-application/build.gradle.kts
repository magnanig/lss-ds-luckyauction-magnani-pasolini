plugins {
    id("org.springframework.boot") version "2.6.6"
}

apply(plugin = "org.springframework.boot")

rootProject.extra["mainClassProp"] = "it.unibo.luckyauction.auction.application.AuctionMain"

dependencies {
    implementation(project(":configuration"))
    implementation(project(":utils"))
    implementation(project(":utils:spring-utils"))
    implementation(project(":socketio-server"))

    implementation(project(":auctions:domain"))
    implementation(project(":auctions:usecase"))
    implementation(project(":auctions:adapter"))
    implementation(project(":auctions:controller"))

    implementation(project(":users:adapter"))
    implementation(project(":movements:adapter"))
    implementation(project(":automations:adapter"))

    implementation("org.springframework.boot:spring-boot-starter-web:2.6.6")

    compileOnly("org.projectlombok:lombok:1.18.22")
    annotationProcessor("org.projectlombok:lombok:1.18.22")
}

// the version of jar task, for spring
tasks.bootJar {
    mainClass.set("it.unibo.luckyauction.auction.application.AuctionMain")
    archiveBaseName.set("luckyauction-auctions-spring-application")
}
