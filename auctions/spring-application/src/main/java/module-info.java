module lss.ds.luckyauction.magnani.pasolini.auctions.application.spring.main {
    requires lss.ds.luckyauction.magnani.pasolini.configuration.main;
    requires lss.ds.luckyauction.magnani.pasolini.utils.main;
    requires lss.ds.luckyauction.magnani.pasolini.utils.spring.main;
    requires lss.ds.luckyauction.magnani.pasolini.socketio.server.main;
    requires lss.ds.luckyauction.magnani.pasolini.auctions.domain.main;
    requires lss.ds.luckyauction.magnani.pasolini.auctions.usecase.main;
    requires lss.ds.luckyauction.magnani.pasolini.auctions.adapter.main;
    requires lss.ds.luckyauction.magnani.pasolini.auctions.controller.main;
    requires lss.ds.luckyauction.magnani.pasolini.users.adapter.main;
    requires lss.ds.luckyauction.magnani.pasolini.movements.adapter.main;
    requires lss.ds.luckyauction.magnani.pasolini.automations.adapter.main;
    requires lombok;
    requires spring.web;
    requires spring.boot;
    requires spring.core;
    requires spring.beans;
    requires spring.context;
    requires spring.boot.autoconfigure;
    requires org.apache.tomcat.embed.core;
}