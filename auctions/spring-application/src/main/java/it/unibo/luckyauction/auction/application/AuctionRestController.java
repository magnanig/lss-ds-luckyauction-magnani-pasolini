/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.application;

import it.unibo.luckyauction.auction.adapter.AuctionWeb;
import it.unibo.luckyauction.auction.adapter.BidWeb;
import it.unibo.luckyauction.auction.adapter.NewAuctionWeb;
import it.unibo.luckyauction.auction.controller.AuctionController;
import it.unibo.luckyauction.auction.domain.valueobject.ProductCategory;
import it.unibo.luckyauction.auction.domain.valueobject.ProductState;
import it.unibo.luckyauction.auction.usecase.exception.AuctionNotFoundException;
import it.unibo.luckyauction.auction.usecase.exception.InvalidBidRaiseException;
import it.unibo.luckyauction.auction.controller.exception.WrongAuctionStateException;
import it.unibo.luckyauction.auction.usecase.port.AuctionFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/auctions")
@CrossOrigin(allowCredentials = "true", originPatterns = "http://localhost:[*]")
@SuppressWarnings("PMD.AvoidDuplicateLiterals") // to favor a better reading of the code
public class AuctionRestController {

    private final AuctionController auctionController;

    @Autowired(required = false)
    public AuctionRestController(final AuctionController auctionController, final Calendar interruptionBeginTime) {
        this.auctionController = auctionController;
        new Thread(() -> auctionController.synchronizeRepositoriesAfterInterruption(interruptionBeginTime)).start();
    }

    @Autowired(required = false)
    public AuctionRestController(final AuctionController auctionController) {
        this.auctionController = auctionController;
        new Thread(auctionController::synchronizeRepositories).start();
    }

    /**
     * Create a new auction.
     * @param auction   the new auction to create
     * @return          the just created auction
     */
    @PostMapping
    public AuctionWeb create(@RequestBody final NewAuctionWeb auction, @RequestParam final String username) {
        return AuctionWeb.fromDomain(auctionController.createAndStart(auction.toDomainAuction(username)));
    }

    /**
     * Delete an auction by its id.
     * @param auctionId     the id of the auction to remove
     * @return              the just deleted auction, if present
     */
    @DeleteMapping("/{auctionId}")
    public Optional<AuctionWeb> deleteById(@PathVariable final String auctionId, @RequestParam final String username) {
        return auctionController.deleteById(auctionId, username).map(AuctionWeb::fromDomain);
    }

    /**
     * Delete all auctions created by current user.
     * @return              the list of the just deleted auctions
     */
    @DeleteMapping
    public List<AuctionWeb> deleteByUsername(@RequestParam final String username) {
        return auctionController.deleteByUsername(username).stream()
                .map(AuctionWeb::fromDomain)
                .collect(Collectors.toList());
    }

    /**
     * Find an auction by its id.
     * @param auctionId     the desired auction id
     * @return              the corresponding auction, if present
     */
    @GetMapping("/{auctionId}")
    public Optional<AuctionWeb> findById(@PathVariable final String auctionId) {
        return auctionController.findById(auctionId).map(AuctionWeb::fromDomain);
    }

    /**
     * Get the actual bid for the specified auction.
     * @param auctionId the auction id
     * @return          the actual bid
     * @throws AuctionNotFoundException if no auction with the specified id exists
     */
    @GetMapping("/{auctionId}/bid")
    public Optional<BidWeb> getActualBid(@PathVariable final String auctionId) throws AuctionNotFoundException {
        return auctionController.getActualBid(auctionId).map(BidWeb::fromDomain);
    }

    /**
     * Get the starting price of the specified auction.
     * @param auctionId the auction id
     * @return          the starting price
     * @throws AuctionNotFoundException if no auction with the specified id exists
     */
    @GetMapping("/{auctionId}/startingPrice")
    public BigDecimal getStartingPrice(@PathVariable final String auctionId) throws AuctionNotFoundException {
        return auctionController.getStartingPrice(auctionId);
    }

    /**
     * Get the list of all auctions.
     * @return  the list of all auctions
     */
    @GetMapping
    public List<AuctionWeb> getAllAuctions() {
        return auctionController.getAllAuctions().stream()
                .map(AuctionWeb::fromDomain)
                .collect(Collectors.toList());
    }

    /**
     * Get the active auctions, eventually matching the specified filters.
     * @param productCategory       the optional product category
     * @param productState          the optional product state
     * @param excludedUsername      the optional excluded username
     * @return                      the list of active auctions, eventually matching the filters
     */
    @GetMapping("/active")
    public List<AuctionWeb> getActiveAuctions(@RequestParam(name = "category", required = false)
                                              final ProductCategory productCategory,
                                              @RequestParam(name = "state", required = false)
                                              final ProductState productState,
                                              @RequestParam(name = "excluded", required = false)
                                              final String excludedUsername) {
        return auctionController.findActiveAuctions(new AuctionFilter.Builder()
                        .productCategory(productCategory)
                        .productState(productState)
                        .exceptUsername(excludedUsername)
                        .build())
                .stream()
                .map(AuctionWeb::fromDomain)
                .collect(Collectors.toList());
    }

    /**
     * Add a new bid for the specified auction.
     * @param auctionId     the id of the auction to which add the bid
     * @param bidAmount     the amount of the bid
     * @return              the just added bid
     * @throws AuctionNotFoundException     if no auction with the specified id exists
     * @throws InvalidBidRaiseException     if the specified bid is not valid for user,
     *                                      i.e. user has not enough money or the new bid
     *                                      does not overcome the actual one by at least 1 €
     */
    @PostMapping("/{auctionId}/bids")
    public BidWeb addNewBid(@PathVariable final String auctionId,
                            @RequestParam(name = "amount") final BigDecimal bidAmount,
                            @RequestParam final String username)
            throws InvalidBidRaiseException, WrongAuctionStateException {
        return BidWeb.fromDomain(auctionController.addNewBid(auctionId, username, bidAmount));
    }

    /**
     * Get all the bids raised for the specified auction.
     * @param auctionId     the desired auction id
     * @return              the list of bids raised for such auction,
     *                      ordered by timestamp
     */
    @GetMapping("/{auctionId}/bids")
    public List<BidWeb> getBidsHistory(@PathVariable final String auctionId) {
        return auctionController.getBidsHistory(auctionId).stream()
                .map(BidWeb::fromDomain)
                .collect(Collectors.toList());
    }
}
