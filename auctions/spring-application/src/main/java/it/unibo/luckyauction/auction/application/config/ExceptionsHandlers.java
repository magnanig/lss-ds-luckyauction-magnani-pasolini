/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.application.config;

import it.unibo.luckyauction.auction.controller.exception.UserNotFoundException;
import it.unibo.luckyauction.auction.domain.exception.AuctionLifeValidationException;
import it.unibo.luckyauction.auction.domain.exception.AuctionValidationException;
import it.unibo.luckyauction.auction.domain.exception.AuctionsHistoryConsistencyErrorException;
import it.unibo.luckyauction.auction.domain.exception.AuctionsHistoryValidationException;
import it.unibo.luckyauction.auction.domain.exception.BidValidationException;
import it.unibo.luckyauction.auction.domain.exception.ProductValidationException;
import it.unibo.luckyauction.auction.domain.exception.UserContactInformationValidationException;
import it.unibo.luckyauction.auction.usecase.exception.AuctionNotAllowedException;
import it.unibo.luckyauction.auction.usecase.exception.AuctionNotFoundException;
import it.unibo.luckyauction.auction.usecase.exception.AuctionsHistoryAlreadyExistsException;
import it.unibo.luckyauction.auction.usecase.exception.AuctionsHistoryNotFoundException;
import it.unibo.luckyauction.auction.usecase.exception.ClosingAuctionException;
import it.unibo.luckyauction.auction.usecase.exception.InvalidBidRaiseException;
import it.unibo.luckyauction.auction.controller.exception.WrongAuctionStateException;
import it.unibo.luckyauction.spring.util.http.ErrorResponse;
import it.unibo.luckyauction.spring.util.http.HttpRequestException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * This class contains auctions exception handlers, which specify
 * how the Spring server should respond to the client when an exception is thrown.
 */
@ControllerAdvice
public class ExceptionsHandlers {

    /**
     * Handler for {@link UserNotFoundException}, raised when the specified user
     * is not found while performing an operation where such user is involved.
     * @param ex    the thrown exception reference
     * @return      the response with status code 401 (unauthorized), containing the
     *              error message in the body
     */
    @ExceptionHandler(UserNotFoundException.class)
    public ResponseEntity<String> handleUserNotFoundException(final UserNotFoundException ex) {
        return ResponseEntity
                .status(HttpStatus.UNAUTHORIZED)
                .body(ex.getMessage());
    }

    /**
     * Handler for {@link AuctionLifeValidationException}.
     * @param ex    the thrown exception reference
     * @return      the response with status code 400 (bad request), containing the
     *              error message in the body
     */
    @ExceptionHandler(AuctionLifeValidationException.class)
    public ResponseEntity<String> handleAuctionLifeValidationException(final AuctionLifeValidationException ex) {
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(ex.getMessage());
    }

    /**
     * Handler for {@link AuctionsHistoryConsistencyErrorException}.
     * @param ex    the thrown exception reference
     * @return      the response with status code 400 (bad request), containing the
     *              error message in the body
     */
    @ExceptionHandler(AuctionsHistoryConsistencyErrorException.class)
    public ResponseEntity<String> handleAuctionsHistoryConsistencyErrorException(
            final AuctionsHistoryConsistencyErrorException ex) {
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(ex.getMessage());
    }

    /**
     * Handler for {@link AuctionsHistoryValidationException}.
     * @param ex    the thrown exception reference
     * @return      the response with status code 400 (bad request), containing the
     *              error message in the body
     */
    @ExceptionHandler(AuctionsHistoryValidationException.class)
    public ResponseEntity<String> handleAuctionsHistoryValidationException(
            final AuctionsHistoryValidationException ex) {
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(ex.getMessage());
    }

    /**
     * Handler for {@link AuctionValidationException}.
     * @param ex    the thrown exception reference
     * @return      the response with status code 400 (bad request), containing the
     *              error message in the body
     */
    @ExceptionHandler(AuctionValidationException.class)
    public ResponseEntity<String> handleAuctionValidationException(final AuctionValidationException ex) {
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(ex.getMessage());
    }

    /**
     * Handler for {@link BidValidationException}.
     * @param ex    the thrown exception reference
     * @return      the response with status code 400 (bad request), containing the
     *              error message in the body
     */
    @ExceptionHandler(BidValidationException.class)
    public ResponseEntity<String> handleBidValidationException(final BidValidationException ex) {
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(ex.getMessage());
    }

    /**
     * Handler for {@link ProductValidationException}.
     * @param ex    the thrown exception reference
     * @return      the response with status code 400 (bad request), containing the
     *              error message in the body
     */
    @ExceptionHandler(ProductValidationException.class)
    public ResponseEntity<String> handleProductValidationException(final ProductValidationException ex) {
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(ex.getMessage());
    }

    /**
     * Handler for {@link UserContactInformationValidationException}.
     * @param ex    the thrown exception reference
     * @return      the response with status code 400 (bad request), containing the
     *              error message in the body
     */
    @ExceptionHandler(UserContactInformationValidationException.class)
    public ResponseEntity<String> handleUserContactInformationValidationException(
            final UserContactInformationValidationException ex) {
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(ex.getMessage());
    }

    /**
     * Handler for {@link AuctionNotAllowedException}, raised when trying to perform
     * actions on an auction without having the permission.
     * @param ex    the thrown exception reference
     * @return      the response with status code 401 (unauthorized), containing the
     *              error message in the body
     */
    @ExceptionHandler(AuctionNotAllowedException.class)
    public ResponseEntity<String> handleAuctionNotAllowedException(final AuctionNotAllowedException ex) {
        return ResponseEntity
                .status(HttpStatus.UNAUTHORIZED)
                .body(ex.getMessage());
    }

    /**
     * Handler for {@link AuctionNotFoundException}, raised when trying to perform any
     * action on an auction that does not exist.
     * @param ex    the thrown exception reference
     * @return      the response with status code 404 (not found), containing the
     *              error message in the body
     */
    @ExceptionHandler(AuctionNotFoundException.class)
    public ResponseEntity<String> handleAuctionNotFoundException(final AuctionNotFoundException ex) {
        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(ex.getMessage());
    }

    /**
     * Handler for {@link AuctionsHistoryAlreadyExistsException}.
     * @param ex    the thrown exception reference
     * @return      the response with status code 400 (bad request), containing the
     *              error message in the body
     */
    @ExceptionHandler(AuctionsHistoryAlreadyExistsException.class)
    public ResponseEntity<String> handleAuctionsHistoryAlreadyExistsException(
            final AuctionsHistoryAlreadyExistsException ex) {
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(ex.getMessage());
    }

    /**
     * Handler for {@link AuctionsHistoryNotFoundException}.
     * @param ex    the thrown exception reference
     * @return      the response with status code 400 (bad request), containing the
     *              error message in the body
     */
    @ExceptionHandler(AuctionsHistoryNotFoundException.class)
    public ResponseEntity<String> handleAuctionsHistoryNotFoundException(final AuctionsHistoryNotFoundException ex) {
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(ex.getMessage());
    }

    /**
     * Handler for {@link ClosingAuctionException}.
     * @param ex    the thrown exception reference
     * @return      the response with status code 400 (bad request), containing the
     *              error message in the body
     */
    @ExceptionHandler(ClosingAuctionException.class)
    public ResponseEntity<String> handleClosingAuctionException(final ClosingAuctionException ex) {
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(ex.getMessage());
    }

    /**
     * Handler for {@link InvalidBidRaiseException}, raised when a
     * user tries to raise a new bid without being able to do so.
     * @param ex    the thrown exception reference
     * @return      the response with status code 400 (bad request), containing the
     *              error message in the body
     */
    @ExceptionHandler(InvalidBidRaiseException.class)
    public ResponseEntity<String> handleInvalidBidRaiseException(final InvalidBidRaiseException ex) {
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(ex.getMessage());
    }

    /**
     * Handler for {@link WrongAuctionStateException}.
     * @param ex    the thrown exception reference
     * @return      the response with status code 400 (bad request), containing the
     *              error message in the body
     */
    @ExceptionHandler(WrongAuctionStateException.class)
    public ResponseEntity<String> handleWrongAuctionStateException(final WrongAuctionStateException ex) {
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(ex.getMessage());
    }

    /**
     * Handler for a {@link HttpRequestException}, i.e. an exception occurring
     * while forwarding a request to another microservice.
     * @param ex    the thrown exception reference
     * @return      the response with the status code got while forwarding the
     *              request, and the error message in the body
     */
    @ExceptionHandler(HttpRequestException.class)
    public ResponseEntity<String> handleHttpRequestException(final HttpRequestException ex) {
        final ErrorResponse errorResponse = ex.getErrorResponse();
        return ResponseEntity
                .status(errorResponse.httpResponseCode())
                .body(errorResponse.message());
    }

    /**
     * Handler for a generic {@link Exception}. This handler will be triggered
     * only if none of the others matches with the thrown exception.
     * @param ex    the thrown exception reference
     * @return      the response with status code 500, containing the error
     *              message in the body
     */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<String> handleException(final Exception ex) {
        ex.printStackTrace();
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(ex.getMessage());
    }
}
