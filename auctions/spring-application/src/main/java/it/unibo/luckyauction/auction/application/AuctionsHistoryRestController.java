/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.application;

import it.unibo.luckyauction.auction.adapter.AuctionsHistoryWeb;
import it.unibo.luckyauction.auction.adapter.AwardedAuctionWeb;
import it.unibo.luckyauction.auction.adapter.CreatedEndedAuctionWeb;
import it.unibo.luckyauction.auction.adapter.CreatedOngoingAuctionWeb;
import it.unibo.luckyauction.auction.adapter.LostAuctionWeb;
import it.unibo.luckyauction.auction.adapter.ParticipatingAuctionWeb;
import it.unibo.luckyauction.auction.adapter.PdfReportWeb;
import it.unibo.luckyauction.auction.controller.AuctionsHistoryController;
import it.unibo.luckyauction.auction.domain.entity.AuctionsHistory;
import it.unibo.luckyauction.auction.domain.valueobject.AwardedAuction;
import it.unibo.luckyauction.auction.usecase.exception.AuctionNotFoundException;
import it.unibo.luckyauction.auction.usecase.exception.AuctionsHistoryNotFoundException;
import it.unibo.luckyauction.auction.controller.exception.WrongAuctionStateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/auctions/histories")
@CrossOrigin(allowCredentials = "true", originPatterns = "http://localhost:[*]")
@SuppressWarnings("PMD.AvoidDuplicateLiterals") // to favor a better reading of the code
public class AuctionsHistoryRestController {

    private final AuctionsHistoryController auctionsHistoryController;

    @Autowired
    public AuctionsHistoryRestController(final AuctionsHistoryController auctionsHistoryController) {
        this.auctionsHistoryController = auctionsHistoryController;
    }

    /**
     * Create a new auctions' history.
     * @param auctionsHistoryId     the id of the new auctions' history
     * @param username              the username associated to the auctions' history
     */
    @PostMapping
    public void createAuctionsHistory(@RequestParam final String auctionsHistoryId,
                                      @RequestParam final String username) {
        auctionsHistoryController.createAuctionsHistory(new AuctionsHistory(auctionsHistoryId, username));
    }

    /**
     * Find the auctions' history by the associated user.
     * @param username  the username of the desired user
     * @return          the auctions' history of the specified user, if any
     */
    @GetMapping
    public Optional<AuctionsHistoryWeb> findByUsername(@RequestParam final String username) {
        return auctionsHistoryController.findByUsername(username)
                .map(AuctionsHistoryWeb::fromDomain);
    }

    /**
     * Remove the auctions' history of the specified user.
     * @param username  the username of the user whose auctions' history has to be removed
     * @return          the id of the just deleted auctions' history, if any
     */
    @DeleteMapping
    public Optional<String> deleteByUsername(@RequestParam final String username) {
        return auctionsHistoryController.deleteByUsername(username);
    }

    /**
     * Add an awarded auction for the specified user.
     * @param auctionId                         the reference to the auction to add
     * @param username                          the username of the desired user
     * @throws WrongAuctionStateException       if the specified auction is not ended
     * @throws AuctionNotFoundException         if no auction with the specified id exists
     * @throws AuctionsHistoryNotFoundException if the auctions' history for the specified
     *                                          user does not exist
     */
    @PostMapping("/awarded")
    public void addAwardedAuction(@RequestParam final String auctionId, @RequestParam final String username)
            throws WrongAuctionStateException, AuctionNotFoundException, AuctionsHistoryNotFoundException {
        auctionsHistoryController.addAwardedAuction(auctionId, username);
    }

    /**
     * Add a created ended auction for the specified user.
     * @param auctionId the id of the auction to add
     * @param username  the username of the desired user
     * @throws WrongAuctionStateException       if the auction is still active
     * @throws AuctionNotFoundException         if no auction with the specified id exists
     * @throws AuctionsHistoryNotFoundException if the auctions' history for the specified user does not exist
     */
    @PostMapping("/created/ended")
    public void addCreatedEndedAuction(@RequestParam final String auctionId, @RequestParam final String username)
            throws WrongAuctionStateException, AuctionNotFoundException, AuctionsHistoryNotFoundException {
        auctionsHistoryController.addCreatedEndedAuction(auctionId, username);
    }

    /**
     * Add a created ongoing auction for the specified user.
     * @param auctionId the id of the auction to add
     * @param username  the username of the desired user
     * @throws WrongAuctionStateException       if the auction has already ended
     * @throws AuctionNotFoundException         if no auction with the specified id exists
     * @throws AuctionsHistoryNotFoundException if the auctions' history for the specified user does not exist
     */
    @PostMapping("/created/ongoing")
    public void addCreatedOngoingAuction(@RequestParam final String auctionId, @RequestParam final String username)
            throws WrongAuctionStateException, AuctionNotFoundException, AuctionsHistoryNotFoundException {
        auctionsHistoryController.addCreatedOngoingAuction(auctionId, username);
    }

    /**
     * Add a lost auction for the specified user.
     * @param auctionId the id of the auction to add
     * @param username  the username of the desired user
     * @throws WrongAuctionStateException       if the auction is still active
     * @throws AuctionNotFoundException         if no auction with the specified id exists
     * @throws AuctionsHistoryNotFoundException if the auctions' history for the specified user does not exist
     */
    @PostMapping("/lost")
    public void addLostAuction(@RequestParam final String auctionId, @RequestParam final String username)
            throws WrongAuctionStateException, AuctionNotFoundException, AuctionsHistoryNotFoundException {
        auctionsHistoryController.addLostAuction(auctionId, username);
    }

    /**
     * Add a participating auction for the specified user.
     * @param auctionId the id of the auction to add
     * @param username  the username of the desired user
     * @throws WrongAuctionStateException       if the auction has already ended
     * @throws AuctionNotFoundException         if no auction with the specified id exists
     * @throws AuctionsHistoryNotFoundException if the auctions' history for the specified user does not exist
     */
    @PostMapping("/participating")
    public void addParticipatingAuction(@RequestParam final String auctionId, @RequestParam final String username)
            throws WrongAuctionStateException, AuctionNotFoundException, AuctionsHistoryNotFoundException {
        auctionsHistoryController.addParticipatingAuction(auctionId, username);
    }

    /**
     * Get the list of the user participating in the specified auction.
     * @param auctionId the desired auction id
     * @return          the list of the username of the user participating in the specified auction
     */
    @GetMapping("/participating/users")
    public List<String> getUsersParticipatingInAuction(@RequestParam final String auctionId) {
        return auctionsHistoryController.getUsersParticipatingInAuction(auctionId);
    }

    /**
     * Remove auction from the list of the participating auction for the specified user.
     * @param auctionId the id of the auction to remove
     * @param username  the username of the desired user
     */
    @DeleteMapping("/participating")
    public void removeParticipatingAuction(@RequestParam final String auctionId,
                                           @RequestParam final String username) {
        auctionsHistoryController.removeParticipatingAuction(auctionId, username);
    }

    /**
     * Remove auction from the list of the created ongoing auction for the specified user.
     * @param auctionId the id of the auction to remove
     * @param username  the username of the desired user
     */
    @DeleteMapping("/created/ongoing")
    public void removeCreatedOngoingAuction(@RequestParam final String auctionId,
                                            @RequestParam final String username) {
        auctionsHistoryController.removeCreatedOngoingAuction(auctionId, username);
    }

    /**
     * Remove the auction from all auction histories of all users.
     * @param auctionId the id of the desired auction
     */
    @DeleteMapping("/{auctionId}")
    public void removeAuctionFromAllHistory(@PathVariable final String auctionId) {
        auctionsHistoryController.removeAuctionFromAllHistory(auctionId);
    }

    /**
     * Get the list of the awarded auctions for the current logged user.
     * @return          the list of awarded auctions
     */
    @GetMapping("/awarded")
    public List<AwardedAuctionWeb> getAwardedAuctions(@RequestParam final String username) {
        return auctionsHistoryController.getAwardedAuctions(username).stream()
                .map(AwardedAuctionWeb::fromDomain)
                .collect(Collectors.toList());
    }

    /**
     * Get the list of the created ongoing auctions for the current logged user.
     * @return          the list of created ongoing auctions
     */
    @GetMapping("/created/ongoing")
    public List<CreatedOngoingAuctionWeb> getCreatedOngoingAuctions(@RequestParam final String username) {
        return auctionsHistoryController.getCreatedOngoingAuctions(username).stream()
                .map(CreatedOngoingAuctionWeb::fromDomain)
                .collect(Collectors.toList());
    }

    /**
     * Get the list of the created ended auctions for the current logged user.
     * @return          the list of created ended auctions
     */
    @GetMapping("/created/ended")
    public List<CreatedEndedAuctionWeb> getCreatedEndedAuctions(@RequestParam final String username) {
        return auctionsHistoryController.getCreatedEndedAuctions(username).stream()
                .map(CreatedEndedAuctionWeb::fromDomain)
                .collect(Collectors.toList());
    }

    /**
     * Get the list of the lost auctions for the current logged user.
     * @return          the list of lost auctions
     */
    @GetMapping("/lost")
    public List<LostAuctionWeb> getLostAuctions(@RequestParam final String username) {
        return auctionsHistoryController.getLostAuctions(username).stream()
                .map(LostAuctionWeb::fromDomain)
                .collect(Collectors.toList());
    }

    /**
     * Get the list of the participating auctions for the current logged user.
     * @return          the list of participating auctions
     */
    @GetMapping("/participating")
    public List<ParticipatingAuctionWeb> getParticipatingAuctions(@RequestParam final String username) {
        return auctionsHistoryController.getParticipatingAuctions(username).stream()
                .map(ParticipatingAuctionWeb::fromDomain)
                .collect(Collectors.toList());
    }

    /**
     * Get the desired awarded auction for the current logged user.
     * @param auctionId     the desired auction id
     * @return              the requested awarded auction, if any
     */
    @GetMapping("/awarded/{auctionId}")
    public Optional<AwardedAuctionWeb> getAwardedAuction(@PathVariable final String auctionId,
                                                         @RequestParam final String username) {
        return auctionsHistoryController.getAwardedAuction(auctionId, username)
                .map(AwardedAuctionWeb::fromDomain);
    }

    /**
     * Get the desired created ongoing auction for the current logged user.
     * @param auctionId     the desired auction id
     * @return              the requested created ongoing auction, if any
     */
    @GetMapping("/created/ongoing/{auctionId}")
    public Optional<CreatedOngoingAuctionWeb> getCreatedOngoingAuction(@PathVariable final String auctionId,
                                                                       @RequestParam final String username) {
        return auctionsHistoryController.getCreatedOngoingAuction(auctionId, username)
                .map(CreatedOngoingAuctionWeb::fromDomain);
    }

    /**
     * Get the desired created ended auction for the current logged user.
     * @param auctionId     the desired auction id
     * @return              the required created ended auction, if any
     */
    @GetMapping("/created/ended/{auctionId}")
    public Optional<CreatedEndedAuctionWeb> getCreatedEndedAuction(@PathVariable final String auctionId,
                                                                   @RequestParam final String username) {
        return auctionsHistoryController.getCreatedEndedAuction(auctionId, username)
                .map(CreatedEndedAuctionWeb::fromDomain);
    }

    /**
     * Get the desired lost auction for the current logged user.
     * @param auctionId     the desired auction id
     * @return              the required lost auction, if any
     */
    @GetMapping("/lost/{auctionId}")
    public Optional<LostAuctionWeb> getLostAuction(@PathVariable final String auctionId,
                                                   @RequestParam final String username) {
        return auctionsHistoryController.getLostAuction(auctionId, username)
                .map(LostAuctionWeb::fromDomain);
    }

    /**
     * Get the desired participating auction for the current logged user.
     * @param auctionId     the desired auction id
     * @return              the required participating auction, if any
     */
    @GetMapping("/participating/{auctionId}")
    public Optional<ParticipatingAuctionWeb> getParticipatingAuction(@PathVariable final String auctionId,
                                                                     @RequestParam final String username) {
        return auctionsHistoryController.getParticipatingAuction(auctionId, username)
                .map(ParticipatingAuctionWeb::fromDomain);
    }

    /**
     * Get the pdf report for the specified awarded auction.
     * @param auctionId     the desired auction id (must correspond to an awarded auction
     *                      for the specified user)
     * @param username      the username of the desired user
     * @return              the pdf report for the specified awarded auction
     * @throws AuctionNotFoundException if the specified auction has not been awarded to
     *                                  the current user
     */
    @GetMapping("/awarded/{auctionId}/pdf")
    public PdfReportWeb getAwardedAuctionPdfReport(@PathVariable final String auctionId,
                                                   @RequestParam final String username)
            throws AuctionNotFoundException {
        return auctionsHistoryController.getAwardedAuction(auctionId, username)
                .map(AwardedAuction::getPdfReport)
                .map(PdfReportWeb::fromDomain)
                .orElseThrow(() -> new AuctionNotFoundException(auctionId));
    }

    /**
     * Get the pdf report for the specified created (and ended) auction. If nobody has been awarded
     * the auction, pdf report will no exists.
     * @param auctionId     the desired auction id (must correspond to a created ended auction
     *                      for the current logged user)
     * @return              the pdf report for the specified created (and ended) auction, if any
     * @throws AuctionNotFoundException if the specified auction has not been created by
     *                                  the current user
     */
    @GetMapping("/created/{auctionId}/pdf")
    public Optional<PdfReportWeb> getCreatedAuctionPdfReport(@PathVariable final String auctionId,
                                                             @RequestParam final String username)
            throws AuctionNotFoundException {
        return auctionsHistoryController.getCreatedEndedAuction(auctionId, username)
                .orElseThrow(() -> new AuctionNotFoundException(auctionId))
                .getPdfReport()
                .map(PdfReportWeb::fromDomain);
    }
}
