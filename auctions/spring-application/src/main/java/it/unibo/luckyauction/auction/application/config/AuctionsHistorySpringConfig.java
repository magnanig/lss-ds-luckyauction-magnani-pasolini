/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.application.config;

import it.unibo.luckyauction.auction.adapter.api.AuctionApi;
import it.unibo.luckyauction.auction.controller.AuctionsHistoryController;
import it.unibo.luckyauction.auction.controller.config.AuctionsHistoryConfig;
import it.unibo.luckyauction.spring.util.api.SpringAuctionApi;
import it.unibo.luckyauction.spring.util.api.SpringUserApi;
import it.unibo.luckyauction.user.adapter.api.UserApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AuctionsHistorySpringConfig {

    @Bean
    public AuctionApi auctionApi() {
        return new SpringAuctionApi();
    }

    @Bean
    public UserApi userApi() {
        return new SpringUserApi();
    }

    @Bean
    public AuctionsHistoryController auctionsHistoryController(final AuctionApi auctionApi, final UserApi userApi) {
        return new AuctionsHistoryConfig().auctionsHistoryController(auctionApi, userApi);
    }
}
