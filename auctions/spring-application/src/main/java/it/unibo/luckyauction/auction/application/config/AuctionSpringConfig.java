/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.application.config;

import it.unibo.luckyauction.auction.adapter.api.AuctionsHistoryApi;
import it.unibo.luckyauction.auction.adapter.socket.AuctionSocket;
import it.unibo.luckyauction.auction.adapter.socket.BidsSocket;
import it.unibo.luckyauction.auction.controller.AuctionController;
import it.unibo.luckyauction.auction.controller.config.AuctionConfig;
import it.unibo.luckyauction.automation.adapter.api.AuctionAutomationApi;
import it.unibo.luckyauction.movement.adapter.api.MovementApi;
import it.unibo.luckyauction.socket.server.AuctionSocketIO;
import it.unibo.luckyauction.socket.server.BidsSocketsIO;
import it.unibo.luckyauction.spring.util.api.SpringAuctionAutomationApi;
import it.unibo.luckyauction.spring.util.api.SpringAuctionsHistoryApi;
import it.unibo.luckyauction.spring.util.api.SpringMovementApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static it.unibo.luckyauction.configuration.SocketsConstants.AUCTION_HOST;
import static it.unibo.luckyauction.configuration.SocketsConstants.AUCTION_PORT;
import static it.unibo.luckyauction.configuration.SocketsConstants.BIDS_HOST;
import static it.unibo.luckyauction.configuration.SocketsConstants.BIDS_PORT;

@Configuration
public class AuctionSpringConfig {
    @Bean
    public MovementApi movementApi() {
        return new SpringMovementApi();
    }

    @Bean
    public AuctionsHistoryApi auctionsHistoryApi() {
        return new SpringAuctionsHistoryApi();
    }

    @Bean
    public AuctionAutomationApi auctionAutomationApi() {
        return new SpringAuctionAutomationApi();
    }

    @Bean
    public AuctionController auctionController(final MovementApi movementApi,
                                               final AuctionsHistoryApi auctionsHistoryApi,
                                               final AuctionAutomationApi auctionAutomationApi,
                                               final AuctionSocket auctionSocket, final BidsSocket bidsSockets) {
        return new AuctionConfig().auctionController(movementApi, auctionsHistoryApi, auctionAutomationApi,
                auctionSocket, bidsSockets);
    }

    @Bean
    public AuctionSocket auctionSocket() {
        return new AuctionSocketIO(AUCTION_HOST, AUCTION_PORT).start();
    }

    @Bean
    public BidsSocket bidsSockets() {
        return new BidsSocketsIO(BIDS_HOST, BIDS_PORT).start();
    }
}
