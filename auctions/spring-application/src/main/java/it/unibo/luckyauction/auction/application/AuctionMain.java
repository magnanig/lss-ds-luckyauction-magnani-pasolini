/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.auction.application;

import it.unibo.luckyauction.util.CalendarUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.context.annotation.Bean;

import java.sql.Date;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Calendar;

@SuppressWarnings({"PMD.ClassNamingConventions", "checkstyle:HideUtilityClassConstructor"})
@SpringBootApplication(exclude = {MongoAutoConfiguration.class, MongoDataAutoConfiguration.class})
public class AuctionMain {
    private static Calendar interruptionBeginTime;

    public static void main(final String[] args) {
        if (args.length > 0) {
            final Calendar datetime = Calendar.getInstance();
            datetime.setTime(Date.from(LocalDateTime.parse(args[0]).atZone(ZoneId.of("Europe/Rome")).toInstant()));
            interruptionBeginTime = datetime;
        }
        SpringApplication.run(AuctionMain.class, args);
    }

    @Bean
    public static Calendar getInterruptionBeginTime() {
        return CalendarUtils.copy(interruptionBeginTime);
    }
}
