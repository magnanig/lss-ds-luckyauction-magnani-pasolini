import java.io.FileOutputStream
import org.gradle.api.tasks.testing.logging.TestLogEvent

plugins {
    java
    application
    checkstyle
    pmd
    id("com.github.spotbugs") version "4.7.6"
    jacoco
}

val mainClassProp by extra { "" } // to make it accessible from subprojects

val defaultGroup = "it.unibo.luckyauction"
project(":users").group = "$defaultGroup.users"
project(":auctions").group = "$defaultGroup.auctions"
project(":movements").group = "$defaultGroup.movements"

/*
 * Run all subproject applications in background with TEST MODE enabled
 * (i.e. LUCKYAUCTION_TEST=true). This is useful for testing the whole
 * application.
 * Note: this task is finalized automatically with the stopAll task,
 * called after having run all tests.
 */
tasks.register<Exec>("runAllAndStop") {
    environment["LUCKYAUCTION_TEST"] = true
    commandLine("./application.sh")
    finalizedBy("stopAll")
    doLast {
        environment.remove("LUCKYAUCTION_TEST")
    }
}

tasks.register<Exec>("runAll") {
    val interruptionTime: String by project
    if (project.hasProperty("interruptionTime")) {
        commandLine("./application.sh", "start", interruptionTime)
    } else {
        commandLine("./application.sh")
    }
}

tasks.register<Exec>("stopAll") {
    commandLine("./application.sh", "stop")
}

tasks.register("validate") {
    dependsOn("runAllAndStop", subprojects.map { it.tasks.getByName("validate") })
    finalizedBy("generateMdReport", "generatePdfReport")
    tasks.getByName("stopAll").mustRunAfter(this)
    doLast {
        println("Great! Your code is valid :)")
    }
}

tasks.test {
    dependsOn("runAllAndStop", subprojects
        .filter { it.tasks.contains(tasks.getByName("test")) }
        .map { it.tasks.getByName("test") }
    )
    tasks.getByName("stopAll").mustRunAfter(this)
}

tasks.withType<AbstractTestTask> {
    testLogging {
        exceptionFormat = org.gradle.api.tasks.testing.logging.TestExceptionFormat.FULL
        events = setOf(TestLogEvent.FAILED, TestLogEvent.SKIPPED, TestLogEvent.PASSED)
        showStandardStreams = true
        afterSuite(KotlinClosure2({ desc: TestDescriptor, result: TestResult ->
            println("Finish test")
            if (desc.parent == null) { // will match the outermost suite
                println(
                    "Results: ${result.resultType} (${result.testCount} tests, ${result.successfulTestCount} successes,"
                            + "${result.failedTestCount} failures, ${result.skippedTestCount} skipped)"
                )
            }
        }))
    }
}

tasks.register<Exec>("generateMdReport") {
    inputs.dir("report/src")
    outputs.file("report/report.md")
    // prepend [TOC] to the final markdown file to generate a table of contents
    doFirst {
        exec {
            workingDir = file("report")
            standardOutput = FileOutputStream("report/report.md")
            commandLine("echo", "[TOC]")
        }
    }
    // merge all *.md files, keeping files ordered by their name
    workingDir = file("report")
    standardOutput = FileOutputStream("report/report.md", true)
    commandLine("awk", "FNR==1{print \"\"}1",
        *wildcardReportExpansion("report", "src/*/*.md")
    )
}

tasks.register<Exec>("generateHtmlReport") {
    inputs.dir("report/src")
    outputs.file("report/report.html")
    workingDir = file("report")
    commandLine(
        "pandoc", "-f", "gfm", "-o", "report.html", "-t", "html",
        *wildcardReportExpansion("report", "src/*/*.md"), "--self-contained",
        "--metadata", "title=Relazione progetto LuckyAuction",
        "--number-sections", "--toc", "-V", "lang=it"
    )
}

tasks.register<Exec>("generatePdfReport") {
    inputs.file("report/report.html")
    inputs.dir("report/resources")
    inputs.file("report/src/report.css")
    outputs.file("report/report.pdf")
    dependsOn("generateHtmlReport")
    workingDir = file("report")
    commandLine(
        "pandoc", "-t", "html", "report.html", "-o", "report.pdf", "--self-contained", "--css=src/report.css",
        "--metadata", "title=", // title is already contained in html
    )
}

tasks.clean {
    delete("report/report.pdf")
    delete("logs/")
}

subprojects {
    apply(plugin = "java")
    apply(plugin = "application")
    apply(plugin = "checkstyle")
    apply(plugin = "pmd")
    apply(plugin = "com.github.spotbugs")
    apply(plugin = "jacoco")

    if (group == "") {
        group = defaultGroup
    }
    version = "1.0.0"

    java {
        targetCompatibility = JavaVersion.VERSION_16
        sourceCompatibility = JavaVersion.VERSION_16
        modularity.inferModulePath.set(true)
    }

    if (project.name.contains("mongo") || project.name.contains("socket") ||
        project.name.contains("akka")) {
        tasks.compileJava {
            doFirst {
                options.compilerArgs = listOf(
                    "--module-path", classpath.asPath
                )
                classpath = files()
            }
        }
    }

    repositories {
        maven {
            url = uri("https://plugins.gradle.org/m2/")
        }
        mavenCentral()
    }

    dependencies {
        implementation("com.github.spotbugs.snom:spotbugs-gradle-plugin:4.7.6")
        implementation("org.projectlombok:lombok:1.18.24")
        testImplementation("org.junit.jupiter:junit-jupiter:5.8.1")
    }

    checkstyle {
        //configFile = file("${rootDir}/config/checkstyle/checkstyle.xml")
        isIgnoreFailures = false
        toolVersion = "9.0"
        maxWarnings = 0
    }

    tasks.withType<Checkstyle> {
        reports {
            xml.required.set(false)
            html.required.set(true)
        }
        excludes.addAll(listOf("**/module-info.java", "**/Main.java"))
    }

    // see https://docs.gradle.org/current/userguide/pmd_plugin.html
    pmd {
        isIgnoreFailures = false
        isConsoleOutput = true
        toolVersion = "6.38.0"
        ruleSets = listOf("$rootDir/config/pmd/pmd.xml")
    }

    tasks.withType<Pmd> {
        reports {
            xml.required.set(false)
            html.required.set(true)
        }
    }

    // see https://github.com/spotbugs/spotbugs-gradle-plugin#readme
    spotbugs {
        ignoreFailures.set(false)
        excludeFilter.set(file("$rootDir/config/spotbugs/filters.xml"))
    }

    tasks.withType<com.github.spotbugs.snom.SpotBugsTask>().configureEach {
        reports.maybeCreate("xml").isEnabled = false
        val htmlReport = reports.maybeCreate("html")
        htmlReport.isEnabled = true
        htmlReport.setStylesheet("fancy-hist.xsl")
    }

    val jacocoReportExcludedFiles = listOf("**/Main.class")
    val jacocoMinCoveragePercentage = 0.8

    // see https://docs.gradle.org/current/userguide/jacoco_plugin.html
    jacoco {
        toolVersion = "0.8.7"
    }

    tasks.withType<JacocoReport> {
        // dependsOn(tasks.getByName("validate"))
        afterEvaluate { // exclude the Main class(es) from coverage analysis
            classDirectories.setFrom(files(classDirectories.files.map {
                fileTree(it).exclude(jacocoReportExcludedFiles)
            }))
        }

        doLast {
            println("See report at file://${buildDir.resolve("reports/jacoco/test/html/index.html")
                .toURI().rawPath}")
        }
    }

    tasks.withType<JacocoCoverageVerification> {
        dependsOn(tasks.jacocoTestReport)
        doFirst {
            println("Analyzing coverage for report file://${buildDir.resolve("reports/jacoco/test/html/index.html")
                .toURI().rawPath}")
        }
        violationRules {
            rule {
                limit {
                    minimum = jacocoMinCoveragePercentage.toBigDecimal()
                }
            }
        }
    }

    tasks.test {
        useJUnitPlatform()
        if (project.name.contains("controller")) {
            if (!file("LA-PIDS").exists()) {
                dependsOn(rootProject.tasks.getByName("runAllAndStop"))
                rootProject.tasks.getByName("stopAll").mustRunAfter(this)
            }
        }
        //testLogging.showStandardStreams = true
        if (!environment.contains("LUCKYAUCTION_TEST")) {
            doFirst {
                environment["LUCKYAUCTION_TEST"] = true
            }
            doLast {
                environment.remove("LUCKYAUCTION_TEST")
            }
        }
    }

    tasks.clean {
        delete("container/")
        delete("web-app/wait-for-it.sh")
    }

    tasks.register("validate") { // check if code is ok to be pushed
        dependsOn(tasks.check, tasks.jacocoTestCoverageVerification)
    }

    tasks.register("qualityCheck") { // check if quality code is respected
        dependsOn(tasks.compileJava, tasks.pmdMain, tasks.pmdTest, tasks.checkstyleMain, tasks.checkstyleTest,
            tasks.spotbugsMain, tasks.spotbugsTest)
    }

    tasks.register<Copy>("prepareContainer") { // copy jar(s) to the container directory + java Dockerfile
        dependsOn(tasks.getByName("qualityCheck"), tasks.assemble, tasks.jacocoTestCoverageVerification)
        if (getSubprojectDepth(project) == 2 && project.name.contains("application")
                || project.name.contains("gateway")) {
            val containerDir = projectDir.resolve("container")
            val jarsDir = file("$buildDir/libs")
            from(jarsDir) {
                // match any string not containing 'plain' (i.e. the jar used to import modules, without main class and libraries)
                include {
                    it.name.endsWith(".jar") && !it.name.contains("plain")
                            && (!it.name.contains("akka") || it.name.contains("all"))
                }
            }
            from(rootDir.resolve("Dockerfile"))
            from(rootDir.resolve("wait-for-it.sh"))
            into(containerDir)
        }
        copy {
            from(rootDir.resolve("wait-for-it.sh"))
            into(rootDir.resolve("web-app"))
        }
    }

    afterEvaluate {

        application {
            mainClass.set(mainClassProp)
        }

        if (!(project.name.contains("spring-application") || project.name.contains("gateway"))) {
            tasks.withType<Jar> {
                manifest {
                    attributes["Main-Class"] = mainClassProp
                }
                archiveBaseName.set("luckyauction-${parent?.name ?: ""}-${project.name}")
            }
        }
    }
}

fun getSubprojectDepth(project: Project): Int {
    var depth = 0
    var parent = project.parent
    while (parent != null) {
        depth++
        parent = parent.parent
    }
    return depth
}

fun wildcardReportExpansion(directory: String, pattern: String): Array<File> {
    return fileTree(directory).matching { include(pattern) }
        .sortedBy { it.name.substringBefore("-").toFloat() }
        .toTypedArray()
}