/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.configuration;

import java.util.Objects;

/**
 * The hostnames and ports of the sockets.
 */
public final class SocketsConstants {

    /**
     * The hostname where the auction socket is listening.
     */
    public static final String AUCTION_HOST = Objects
            .requireNonNullElse(System.getenv("AuctionsSocketHost"), "localhost");
    /**
     * The hostname where the bid socket is listening.
     */
    public static final String BIDS_HOST = Objects
            .requireNonNullElse(System.getenv("BidsSocketHost"), "localhost");
    /**
     * The hostname where the automation socket is listening.
     */
    public static final String AUCTION_AUTOMATION_HOST = Objects
            .requireNonNullElse(System.getenv("AutomationsSocketHost"), "localhost");

    /**
     * The port where the auction socket is listening.
     */
    public static final Integer AUCTION_PORT = 12_345;
    /**
     * The port where the bid socket is listening.
     */
    public static final Integer BIDS_PORT = 54_321;
    /**
     * The port where the automation socket is listening.
     */
    public static final Integer AUCTION_AUTOMATIONS_PORT = 10_203;

    /**
     * The default bucket for the bid socket.
     */
    public static final Integer DEFAULT_BID_SOCKET_BUCKET = 0;

    /**
     * Get the BidsSocket bucket id corresponding to the given auction id.
     * Bucket with id {@value #DEFAULT_BID_SOCKET_BUCKET} is reserved for the
     * default bucket, matching all bids for all auctions.
     * @param auctionId     the auction id
     * @return              the bucket id
     */
    public static Integer getBidSocketBucketId(final String auctionId) {
        // change this implementation to use an hash function on auctionId
        // to get the unique bid socket bucket id
        // (e.g. auctionId.hashCode() % numberOfBuckets) + 1
        return DEFAULT_BID_SOCKET_BUCKET;
    }

    private SocketsConstants() { }
}
