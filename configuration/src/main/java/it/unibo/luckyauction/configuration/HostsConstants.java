/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.configuration;

import java.util.Objects;

/**
 * The host information to reach each microservice.
 */
public final class HostsConstants {

    private static final String LOCALHOST_URL = "http://localhost";

    /**
     * The base URL of the users' microservice.
     */
    public static final String USERS_URL = Objects.requireNonNullElse(System.getenv("UsersUrl"), LOCALHOST_URL);
    /**
     * The base URL of the movements' microservice.
     */
    public static final String MOVEMENTS_URL = Objects
            .requireNonNullElse(System.getenv("MovementsUrl"), LOCALHOST_URL);
    /**
     * The base URL of the auctions' microservice.
     */
    public static final String AUCTIONS_URL = Objects
            .requireNonNullElse(System.getenv("AuctionsUrl"), LOCALHOST_URL);
    /**
     * The base URL of the automations' microservice.
     */
    public static final String AUTOMATIONS_URL = Objects
            .requireNonNullElse(System.getenv("AutomationsUrl"), LOCALHOST_URL);

    /**
     * The complete URL of the users API.
     */
    public static final String USERS_API = USERS_URL + ":8081/api/users";
    /**
     * The complete URL of the movements API.
     */
    public static final String MOVEMENTS_API = MOVEMENTS_URL + ":8082/api/movements";
    /**
     * The complete URL of the auctions API.
     */
    public static final String AUCTIONS_API = AUCTIONS_URL + ":8083/api/auctions";
    /**
     * The complete URL of the auctions histories API.
     */
    public static final String AUCTIONS_HISTORY_API = AUCTIONS_URL + ":8083/api/auctions/histories";
    /**
     * The complete URL of the automations API.
     */
    public static final String AUTOMATIONS_API = AUTOMATIONS_URL + ":8084/api/auctions/automations";

    private HostsConstants() { }
}
