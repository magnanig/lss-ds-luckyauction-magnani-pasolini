dependencies {
    implementation(project(":movements:domain"))
    implementation(project(":movements:usecase"))
    implementation(project(":utils"))
}
