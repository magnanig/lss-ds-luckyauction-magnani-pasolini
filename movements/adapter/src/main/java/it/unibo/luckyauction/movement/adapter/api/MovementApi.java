/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.movement.adapter.api;

import it.unibo.luckyauction.movement.adapter.AwardedAuctionTransactionsPairWeb;
import it.unibo.luckyauction.movement.adapter.FreezingWeb;
import it.unibo.luckyauction.movement.adapter.MovementsHistoryWeb;
import it.unibo.luckyauction.movement.domain.exception.InsufficientBudgetException;
import it.unibo.luckyauction.movement.domain.exception.FreezingNotExistsException;
import it.unibo.luckyauction.movement.usecase.exception.FreezingForAuctionAlreadyExistsException;
import it.unibo.luckyauction.movement.usecase.exception.MovementsHistoryNotFoundException;

import java.math.BigDecimal;
import java.util.Optional;

public interface MovementApi {

    /**
     * Create a new movements history.
     * @param movementsHistoryId    the id of the new movements history
     * @param username              the username associated to the movements history to create
     */
    void createMovementsHistory(String movementsHistoryId, String username);

    /**
     * Remove the movements' history for the specified user.
     * @param username    the username associated to the movements history to remove
     * @return            the just removed movements' history, if any
     */
    Optional<MovementsHistoryWeb> deleteMovementsHistory(String username);

    /**
     * Check if the user has sufficient budget.
     * @param username          the user's username
     * @param sufficientBudget  the amount you want to check
     * @return                  true if the user has sufficient budget, false otherwise
     */
    Boolean userHasSufficientBudget(String username, BigDecimal sufficientBudget);

    /**
     * Freeze part of user money (i.e. after they raised a new bid). The money amount
     * to be frozen is equals to the bid amount.
     * @param bidderUsername    the username of the user whose budget must be frozen
     * @param raisedAmount      the raised amount, i.e. the amount of money to freeze
     * @param auctionId         the auction id for which bid is referred
     * @param productName       the name of the product of the auction in question
     * @return                  the just created freezing
     * @throws FreezingForAuctionAlreadyExistsException if a freezing for the specified auction already
     *                                                  exists for the bidder user
     * @throws MovementsHistoryNotFoundException        if there is no MovementsHistory for the user
     * @throws InsufficientBudgetException              if the user has insufficient money in their budget
     */
    FreezingWeb freezeMoney(String bidderUsername, BigDecimal raisedAmount, String auctionId, String productName);

    /**
     * Unfreeze money for the specified user and auction, after a new bid raise.
     * @param username                      the username for which remove the money freezing
     * @param auctionId                     the auction id for which user freezing must be removed
     * @return                              the just removed freezing
     * @throws FreezingNotExistsException   if no freezing exists for current username and auction
     */
    FreezingWeb unfreezeMoney(String username, String auctionId);

    /**
     * Exchange money from buyer to auction seller, after the auction closes.
     * Create the two transactions for an awarded auction, for both seller and buyer.
     * The money amount for seller will be positive, while for buyer negative.
     * @param buyerUsername     the buyer username
     * @param sellerUsername    the seller username
     * @param amount            the final amount of the awarded auction
     * @param auctionId         the id of awarded auction
     * @param productName       the name of the product of the auction in question
     * @throws FreezingNotExistsException   if no freezing exists for the winning bid of the buyer user
     */
    AwardedAuctionTransactionsPairWeb exchangeMoneyAfterAwardedAuction(String buyerUsername, String sellerUsername,
                                                                       BigDecimal amount, String auctionId,
                                                                       String productName);
}
