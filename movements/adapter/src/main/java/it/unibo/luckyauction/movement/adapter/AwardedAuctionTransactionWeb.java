/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.movement.adapter;

import it.unibo.luckyauction.movement.domain.entity.AwardedAuctionTransaction;
import it.unibo.luckyauction.util.CalendarUtils;

import java.util.Calendar;

/**
 * Represents the schema of a {@link AwardedAuctionTransaction}.
 * This is a (de)serializable version with all getters and setters,
 * to be used to send object through the network.
 */
public final class AwardedAuctionTransactionWeb extends TransactionWeb {

    private String buyerUsername;       // from
    private String sellerUsername;      // to
    private String auctionId;
    private String productName;

    public AwardedAuctionTransactionWeb() {
        super();
    }

    AwardedAuctionTransactionWeb(final AwardedAuctionTransaction transaction) {
        super(transaction);
        buyerUsername = transaction.getBuyer();
        sellerUsername = transaction.getSeller();
        auctionId = transaction.getAuctionId();
        productName = transaction.getProductName();
    }

    public static AwardedAuctionTransactionWeb fromAwardedAuctionTransaction(
            final AwardedAuctionTransaction transaction) {
        return new AwardedAuctionTransactionWeb(transaction);
    }

    @Override
    public AwardedAuctionTransaction toDomainMovement() {
        final Calendar timestamp = Calendar.getInstance();
        timestamp.setTimeInMillis(getTimestamp().getTime());
        return AwardedAuctionTransaction.builder()
                .transactionId(getMovementId())
                .buyerUsername(buyerUsername)
                .sellerUsername(sellerUsername)
                .auctionId(auctionId)
                .productName(productName)
                .amount(getAmount())
                .timestamp(timestamp)
                .build();
    }

    public AwardedAuctionTransactionWeb copy() {
        final AwardedAuctionTransactionWeb transactionWeb = new AwardedAuctionTransactionWeb();
        transactionWeb.setMovementId(getMovementId());
        transactionWeb.setTimestamp(CalendarUtils.copy(getTimestamp()));
        transactionWeb.setAmount(getAmount());
        transactionWeb.setType(getType());
        transactionWeb.setBuyerUsername(getBuyerUsername());
        transactionWeb.setSellerUsername(getSellerUsername());
        transactionWeb.setAuctionId(getAuctionId());
        transactionWeb.setProductName(getProductName());
        return transactionWeb;
    }

    public String getBuyerUsername() {
        return buyerUsername;
    }

    public void setBuyerUsername(final String buyerUsername) {
        this.buyerUsername = buyerUsername;
    }

    public String getSellerUsername() {
        return sellerUsername;
    }

    public void setSellerUsername(final String sellerUsername) {
        this.sellerUsername = sellerUsername;
    }

    public String getAuctionId() {
        return auctionId;
    }

    public void setAuctionId(final String auctionId) {
        this.auctionId = auctionId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(final String productName) {
        this.productName = productName;
    }
}
