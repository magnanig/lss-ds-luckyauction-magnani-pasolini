/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.movement.adapter;

import it.unibo.luckyauction.movement.domain.entity.Freezing;

import java.util.Calendar;

/**
 * Represents the schema of an {@link Freezing}. This is a (de)serializable
 * version with all getters and setters, to be used to send object through
 * the network.
 */
public final class FreezingWeb extends MovementWeb {

    private String auctionId;
    private String productName;

    public FreezingWeb() {
        super();
    }

    FreezingWeb(final Freezing freezing) {
        super(freezing);
        auctionId = freezing.getAuctionId();
        productName = freezing.getProductName();
    }

    public static FreezingWeb fromDomainFreezing(final Freezing freezing) {
        return new FreezingWeb(freezing);
    }

    @Override
    public Freezing toDomainMovement() {
        final Calendar timestamp = Calendar.getInstance();
        timestamp.setTimeInMillis(getTimestamp().getTime());
        return Freezing.builder()
                .freezingId(getMovementId())
                .amount(getAmount())
                .timestamp(timestamp)
                .auctionId(auctionId)
                .productName(productName)
                .build();
    }

    public String getAuctionId() {
        return auctionId;
    }

    public void setAuctionId(final String auctionId) {
        this.auctionId = auctionId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(final String productName) {
        this.productName = productName;
    }
}
