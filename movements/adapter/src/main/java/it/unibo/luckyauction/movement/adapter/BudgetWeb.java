/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.movement.adapter;


import it.unibo.luckyauction.movement.domain.valueobject.Budget;

import java.math.BigDecimal;

/**
 * Represents the schema of a {@link Budget}. This is a (de)serializable
 * version with all getters and setters, to be used to send object
 * through the network.
 */
public final class BudgetWeb {

    private BigDecimal value;

    public BudgetWeb() {
        this(BigDecimal.ZERO);
    }

    public BudgetWeb(final BigDecimal value) {
        this.value = value;
    }

    public static BudgetWeb fromDomainBudget(final Budget budget) {
        return new BudgetWeb(budget.value());
    }

    public Budget toDomainBudget() {
        return new Budget(value);
    }

    public BudgetWeb copy() {
        return new BudgetWeb(value);
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(final BigDecimal value) {
        this.value = value;
    }
}
