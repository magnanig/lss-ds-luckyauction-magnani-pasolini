/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.movement.adapter;

import it.unibo.luckyauction.movement.domain.entity.WithdrawalTransaction;

import java.util.Calendar;

/**
 * Represents the schema of a {@link WithdrawalTransaction}. This is a
 * (de)serializable version with all getters and setters, to be used to
 * send object through the network.
 */
public final class WithdrawalTransactionWeb extends TransactionWeb {

    public WithdrawalTransactionWeb() {
        super();
    }

    WithdrawalTransactionWeb(final WithdrawalTransaction transaction) {
        super(transaction);
    }

    public static WithdrawalTransactionWeb fromDomainWithdrawalTransaction(final WithdrawalTransaction transaction) {
        return new WithdrawalTransactionWeb(transaction);
    }

    @Override
    public WithdrawalTransaction toDomainMovement() {
        final Calendar timestamp = Calendar.getInstance();
        timestamp.setTimeInMillis(getTimestamp().getTime());
        return WithdrawalTransaction.builder()
                .transactionId(getMovementId())
                .amount(getAmount())
                .timestamp(timestamp)
                .build();
    }

}
