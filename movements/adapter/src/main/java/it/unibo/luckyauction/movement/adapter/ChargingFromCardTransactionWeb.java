/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.movement.adapter;

import it.unibo.luckyauction.movement.domain.entity.ChargingFromCardTransaction;

import java.util.Calendar;

/**
 * Represents the schema of a {@link ChargingFromCardTransaction}.
 * This is a (de)serializable version with all getters and setters,
 * to be used to send object through the network.
 */
public final class ChargingFromCardTransactionWeb extends TransactionWeb {

    private String description;

    public ChargingFromCardTransactionWeb() {
        super();
    }

    ChargingFromCardTransactionWeb(final ChargingFromCardTransaction transaction) {
        super(transaction);
        description = transaction.getDescription();
    }

    public static ChargingFromCardTransactionWeb fromDomainChargingFromCardTransaction(
            final ChargingFromCardTransaction transaction) {
        return new ChargingFromCardTransactionWeb(transaction);
    }

    @Override
    public ChargingFromCardTransaction toDomainMovement() {
        final Calendar timestamp = Calendar.getInstance();
        timestamp.setTimeInMillis(getTimestamp().getTime());
        return ChargingFromCardTransaction.builder()
                .transactionId(getMovementId())
                .description(description)
                .amount(getAmount())
                .timestamp(timestamp)
                .build();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }
}
