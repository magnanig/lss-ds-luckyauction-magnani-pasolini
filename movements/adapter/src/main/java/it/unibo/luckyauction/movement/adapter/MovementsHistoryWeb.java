/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.movement.adapter;

import it.unibo.luckyauction.movement.domain.entity.MovementsHistory;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Represents the schema of an {@link MovementsHistory}. This is a
 * (de)serializable version with all getters and setters, to be used
 * to send object through the network.
 */
public final class MovementsHistoryWeb {

    private String movementsHistoryId;
    private String username;
    private BudgetWeb budget;
    private List<TransactionWeb> transactions;
    private List<FreezingWeb> freezes;

    public static MovementsHistoryWeb fromDomainMovementsHistory(final MovementsHistory movementsHistory) {
        final MovementsHistoryWeb movementsHistoryWeb = new MovementsHistoryWeb();
        movementsHistoryWeb.username = movementsHistory.getUsername();
        movementsHistoryWeb.movementsHistoryId = movementsHistory.getMovementsHistoryId();
        movementsHistoryWeb.budget = BudgetWeb.fromDomainBudget(movementsHistory.getBudget());
        movementsHistoryWeb.transactions = movementsHistory.getTransactions().stream()
                .map(TransactionWeb::fromDomainTransaction)
                .collect(Collectors.toList());
        movementsHistoryWeb.freezes = movementsHistory.getFreezes().stream()
                .map(FreezingWeb::fromDomainFreezing)
                .collect(Collectors.toList());
        return movementsHistoryWeb;
    }

    public MovementsHistory toDomainMovementsHistory() {
        return MovementsHistory.builder()
                .movementsHistoryId(movementsHistoryId)
                .username(username)
                .budget(budget.toDomainBudget())
                .transactions(transactions.stream()
                .map(TransactionWeb::toDomainMovement).collect(Collectors.toList()))
                .freezes(freezes.stream()
                .map(FreezingWeb::toDomainMovement).collect(Collectors.toList()))
                .build();
    }

    public String getMovementsHistoryId() {
        return movementsHistoryId;
    }

    public void setMovementsHistoryId(final String movementsHistoryId) {
        this.movementsHistoryId = movementsHistoryId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(final String username) {
        this.username = username;
    }

    public BudgetWeb getBudget() {
        return budget;
    }

    public void setBudget(final BudgetWeb budget) {
        this.budget = budget;
    }

    public List<TransactionWeb> getTransactions() {
        return new ArrayList<>(transactions);
    }

    public void setTransactions(final List<TransactionWeb> transactions) {
        this.transactions = new ArrayList<>(transactions);
    }

    public List<FreezingWeb> getFreezes() {
        return new ArrayList<>(freezes);
    }

    public void setFreezes(final List<FreezingWeb> freezes) {
        this.freezes = new ArrayList<>(freezes);
    }
}
