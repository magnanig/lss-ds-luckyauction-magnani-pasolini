/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.movement.adapter;

import it.unibo.luckyauction.movement.domain.entity.AwardedAuctionTransaction;
import it.unibo.luckyauction.movement.domain.entity.ChargingFromCardTransaction;
import it.unibo.luckyauction.movement.domain.entity.Freezing;
import it.unibo.luckyauction.movement.domain.entity.Movement;
import it.unibo.luckyauction.movement.domain.entity.WithdrawalTransaction;
import it.unibo.luckyauction.util.CalendarUtils;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Represents the schema of an {@link Movement}. This is a (de)serializable
 * version with all getters and setters, to be used to send object through
 * the network.
 */
@SuppressWarnings("checkstyle:DesignForExtension")
public abstract class MovementWeb {

    private String movementId;
    private Date timestamp;
    private BigDecimal amount;
    private MovementTypeWeb type;

    public MovementWeb() {
        timestamp = new Date();
    }

    public MovementWeb(final Movement movement) {
        movementId = movement.getMovementId();
        timestamp = movement.getTimestamp().getTime();
        amount = movement.getAmount();
        type = MovementTypeWeb.fromDomainMovement(movement.getType());
    }

    public static MovementWeb fromDomainMovement(final Movement movement) {
        if (movement instanceof ChargingFromCardTransaction chargingFromCardTransaction) {
            return new ChargingFromCardTransactionWeb(chargingFromCardTransaction);
        } else if (movement instanceof WithdrawalTransaction withdrawalTransaction) {
            return new WithdrawalTransactionWeb(withdrawalTransaction);
        } else if (movement instanceof AwardedAuctionTransaction awardedAuctionTransaction) {
            return new AwardedAuctionTransactionWeb(awardedAuctionTransaction);
        } else if (movement instanceof Freezing freezing) {
            return new FreezingWeb(freezing);
        }
        throw new IllegalArgumentException("Invalid movement");
    }

    public abstract Movement toDomainMovement();

    public String getMovementId() {
        return movementId;
    }

    public void setMovementId(final String movementId) {
        this.movementId = movementId;
    }

    public Date getTimestamp() {
        return CalendarUtils.copy(timestamp);
    }

    public void setTimestamp(final Date timestamp) {
        this.timestamp = CalendarUtils.copy(timestamp);
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(final BigDecimal amount) {
        this.amount = amount;
    }

    public MovementTypeWeb getType() {
        return type;
    }

    public void setType(final MovementTypeWeb type) {
        this.type = type;
    }

}
