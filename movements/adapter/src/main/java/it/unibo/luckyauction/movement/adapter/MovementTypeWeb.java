/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.movement.adapter;

import it.unibo.luckyauction.movement.domain.valueobject.MovementType;
import it.unibo.luckyauction.movement.domain.valueobject.SingleUserTransactionType;
import it.unibo.luckyauction.movement.domain.valueobject.UsersPairTransactionType;

/**
 * The type of movement.
 */
public enum MovementTypeWeb {
    /**
     * A recharge from user payment card.
     */
    CHARGING_FROM_CARD,
    /**
     * A withdrawal from user budget, to their payment card.
     */
    WITHDRAWAL,
    /**
     * A transaction for an awarded auction.
     */
    AWARDED_AUCTION,
    /**
     * A temporary lock of money.
     */
    FREEZING;

    public static MovementTypeWeb fromDomainMovement(final MovementType movementType) {
        if (movementType instanceof SingleUserTransactionType transactionType) {
            return switch (transactionType) {
                case CHARGING_FROM_CARD -> CHARGING_FROM_CARD;
                case WITHDRAWAL -> WITHDRAWAL;
            };
        } else if (movementType instanceof UsersPairTransactionType) {
            return AWARDED_AUCTION;
        }
        return FREEZING;
    }
}
