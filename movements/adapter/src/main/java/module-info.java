module lss.ds.luckyauction.magnani.pasolini.movements.adapter.main {
    exports it.unibo.luckyauction.movement.adapter;
    exports it.unibo.luckyauction.movement.adapter.api;
    requires lss.ds.luckyauction.magnani.pasolini.utils.main;
    requires lss.ds.luckyauction.magnani.pasolini.movements.domain.main;
    requires lss.ds.luckyauction.magnani.pasolini.movements.usecase.main;
}