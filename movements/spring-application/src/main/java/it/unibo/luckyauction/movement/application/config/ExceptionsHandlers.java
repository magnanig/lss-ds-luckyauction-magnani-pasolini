/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.movement.application.config;

import it.unibo.luckyauction.movement.domain.exception.BudgetValidationException;
import it.unibo.luckyauction.movement.domain.exception.FreezingNotExistsException;
import it.unibo.luckyauction.movement.domain.exception.InsufficientBudgetException;
import it.unibo.luckyauction.movement.domain.exception.MovementAlreadyExistsException;
import it.unibo.luckyauction.movement.domain.exception.MovementValidationException;
import it.unibo.luckyauction.movement.domain.exception.MovementsHistoryValidationException;
import it.unibo.luckyauction.movement.usecase.exception.FreezingForAuctionAlreadyExistsException;
import it.unibo.luckyauction.movement.usecase.exception.MovementsHistoryAlreadyExistsException;
import it.unibo.luckyauction.movement.usecase.exception.MovementsHistoryNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * This class contains movements exception handlers, which specify
 * how the Spring server should respond to the client when an exception is thrown.
 */
@ControllerAdvice
public class ExceptionsHandlers {

    /**
     * Handler for {@link BudgetValidationException}.
     * @param ex    the thrown exception reference
     * @return      the response with status code 400 (bad request), containing the
     *              error message in the body
     */
    @ExceptionHandler(BudgetValidationException.class)
    public ResponseEntity<String> handleBudgetValidationException(final BudgetValidationException ex) {
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(ex.getMessage());
    }

    /**
     * Handler for {@link FreezingNotExistsException}, raised when trying to unfreeze
     * a freezing that does not exist.
     * @param ex    the thrown exception reference
     * @return      the response with status code 400 (bad request), containing the
     *              error message in the body
     */
    @ExceptionHandler(FreezingNotExistsException.class)
    public ResponseEntity<String> handleFreezingNotExistsException(final FreezingNotExistsException ex) {
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(ex.getMessage());
    }

    /**
     * Handler for {@link InsufficientBudgetException}, raised when a user has not
     * the budget necessary to perform the desired transaction.
     * @param ex    the thrown exception reference
     * @return      the response with status code 400 (bad request), containing the
     *              error message in the body
     */
    @ExceptionHandler(InsufficientBudgetException.class)
    public ResponseEntity<String> handleInsufficientBudgetException(final InsufficientBudgetException ex) {
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(ex.getMessage());
    }

    /**
     * Handler for {@link MovementAlreadyExistsException}, raised when the movement you are trying
     * to create already exists.
     * @param ex    the thrown exception reference
     * @return      the response with status code 400 (bad request), containing the
     *              error message in the body
     */
    @ExceptionHandler(MovementAlreadyExistsException.class)
    public ResponseEntity<String> handleMovementAlreadyExistsException(final MovementAlreadyExistsException ex) {
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(ex.getMessage());
    }

    /**
     * Handler for {@link MovementsHistoryValidationException}.
     * @param ex    the thrown exception reference
     * @return      the response with status code 400 (bad request), containing the
     *              error message in the body
     */
    @ExceptionHandler(MovementsHistoryValidationException.class)
    public ResponseEntity<String> handleMovementsHistoryValidationException(
            final MovementsHistoryValidationException ex) {
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(ex.getMessage());
    }

    /**
     * Handler for {@link MovementValidationException}.
     * @param ex    the thrown exception reference
     * @return      the response with status code 400 (bad request), containing the
     *              error message in the body
     */
    @ExceptionHandler(MovementValidationException.class)
    public ResponseEntity<String> handleMovementValidationException(final MovementValidationException ex) {
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(ex.getMessage());
    }

    /**
     * Handler for {@link FreezingForAuctionAlreadyExistsException}, raised when trying
     * to freeze money associated to an auction for which the specified user has already
     * a pending freezing.
     * @param ex    the thrown exception reference
     * @return      the response with status code 400 (bad request), containing the
     *              error message in the body
     */
    @ExceptionHandler(FreezingForAuctionAlreadyExistsException.class)
    public ResponseEntity<String> handleFreezingForAuctionAlreadyExistsException(
            final FreezingForAuctionAlreadyExistsException ex) {
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(ex.getMessage());
    }

    /**
     * Handler for {@link MovementsHistoryAlreadyExistsException}, raised when the movement history you are trying
     * to create already exists.
     * @param ex    the thrown exception reference
     * @return      the response with status code 400 (bad request), containing the
     *              error message in the body
     */
    @ExceptionHandler(MovementsHistoryAlreadyExistsException.class)
    public ResponseEntity<String> handleMovementsHistoryAlreadyExistsException(
            final MovementsHistoryAlreadyExistsException ex) {
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(ex.getMessage());
    }

    /**
     * Handler for {@link MovementsHistoryNotFoundException}.
     * @param ex    the thrown exception reference
     * @return      the response with status code 400 (bad request), containing the
     *              error message in the body
     */
    @ExceptionHandler(MovementsHistoryNotFoundException.class)
    public ResponseEntity<String> handleMovementsHistoryNotFoundException(final MovementsHistoryNotFoundException ex) {
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(ex.getMessage());
    }

    /**
     * Handler for a generic {@link Exception}. This handler will be triggered
     * only if none of the others matches with the thrown exception.
     * @param ex    the thrown exception reference
     * @return      the response with status code 500, containing the error
     *              message in the body
     */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<String> handleException(final Exception ex) {
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(ex.getMessage());
    }
}
