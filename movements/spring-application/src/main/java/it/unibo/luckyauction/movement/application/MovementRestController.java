/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.movement.application;

import it.unibo.luckyauction.movement.adapter.ChargingFromCardTransactionWeb;
import it.unibo.luckyauction.movement.adapter.FreezingWeb;
import it.unibo.luckyauction.movement.adapter.MovementWeb;
import it.unibo.luckyauction.movement.adapter.MovementsHistoryWeb;
import it.unibo.luckyauction.movement.adapter.TransactionWeb;
import it.unibo.luckyauction.movement.adapter.WithdrawalTransactionWeb;
import it.unibo.luckyauction.movement.controller.MovementController;
import it.unibo.luckyauction.movement.domain.exception.FreezingNotExistsException;
import it.unibo.luckyauction.movement.domain.exception.InsufficientBudgetException;
import it.unibo.luckyauction.movement.usecase.exception.FreezingForAuctionAlreadyExistsException;
import it.unibo.luckyauction.movement.usecase.exception.MovementsHistoryAlreadyExistsException;
import it.unibo.luckyauction.movement.usecase.exception.MovementsHistoryNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/movements")
@CrossOrigin(allowCredentials = "true", originPatterns = "http://localhost:[*]")
public class MovementRestController {

    private final MovementController movementController;

    @Autowired
    public MovementRestController(final MovementController movementController) {
        this.movementController = movementController;
    }

    /**
     * Create a new MovementsHistory in the repository, with € 0.00 default budget.
     * @param movementsHistoryId    the new MovementsHistory id
     * @param username              the username associated to such history
     * @throws MovementsHistoryAlreadyExistsException   if a history for the specified
     *                                                  username already exists
     */
    @PostMapping("/histories")
    public void createMovementsHistory(@RequestParam final String movementsHistoryId,
                                       @RequestParam final String username)
            throws MovementsHistoryAlreadyExistsException {
        movementController.createNewMovementsHistory(movementsHistoryId, username);
    }

    /**
     * Delete a MovementsHistory by the associated username.
     * @param username  the username associated to the MovementsHistory to remove
     */
    @DeleteMapping("/histories")
    public void deleteMovementsHistory(@RequestParam final String username) {
        movementController.deleteMovementsHistoryByUsername(username)
                .map(MovementsHistoryWeb::fromDomainMovementsHistory);
    }

    /**
     * Check if the user has sufficient budget.
     * @param username          the user's username
     * @param sufficientBudget  the amount you want to check
     * @return                  true if the user has sufficient budget, false otherwise
     */
    @GetMapping("/budget")
    public Boolean userHasSufficientBudget(@RequestParam final String username,
                                           @RequestParam final BigDecimal sufficientBudget)
            throws MovementsHistoryNotFoundException {
        return movementController.userHasSufficientBudget(username, sufficientBudget);
    }

    /**
     * Find a MovementsHistory by the associated username.
     * @param username  the username for which looking for their movements history
     * @return          the corresponding MovementsHistory, if any
     */
    @GetMapping("/histories")
    public Optional<MovementsHistoryWeb> findMovementsHistoryByUsername(@RequestParam final String username) {
        return movementController.findMovementsHistoryByUsername(username)
                .map(MovementsHistoryWeb::fromDomainMovementsHistory);
    }

    /**
     * Find a movement by its id and associated username.
     * @param movementId    the id of the desired movement
     * @param username      the username of the user to which the movement belongs
     * @return              the corresponding movement, if exists
     */
    @GetMapping("/{movementId}")
    public Optional<MovementWeb> findMovementById(@PathVariable final String movementId,
                                                  @RequestParam final String username) {
        return movementController.findMovementById(movementId, username).map(MovementWeb::fromDomainMovement);
    }

    /**
     * Get all freezes associated to the desired user.
     * @param username  the username of the desired user
     * @return          the freezes of the specified user
     */
    @GetMapping("/freezes")
    public List<FreezingWeb> getFreezes(@RequestParam final String username) {
        return movementController.getFreezes(username).stream()
                .map(FreezingWeb::fromDomainFreezing).collect(Collectors.toList());
    }

    /**
     * Freeze part of user money to raise a new bid. The money amount to be frozen is equals to the bid amount.
     * @param bidderUsername    the username of the user who raised
     * @param raisedAmount      the raised amount
     * @param auctionId         the auction id for which bid is referred
     * @param productName       the name of the product of the auction in question
     * @return                  the just created freezing
     * @throws FreezingForAuctionAlreadyExistsException if a freezing for the specified auction already
     *                                                  exists for the bidder user
     * @throws MovementsHistoryNotFoundException        if there is no MovementsHistory for the user
     * @throws InsufficientBudgetException              if the user has insufficient money in their budget
     */
    @PostMapping("/freezes")
    public FreezingWeb freezeMoney(@RequestParam final String bidderUsername,
                                   @RequestParam final BigDecimal raisedAmount, @RequestParam final String auctionId,
                                   @RequestParam final String productName)
            throws FreezingForAuctionAlreadyExistsException, MovementsHistoryNotFoundException,
            InsufficientBudgetException {
        return FreezingWeb.fromDomainFreezing(movementController.freezeMoney(bidderUsername, raisedAmount, auctionId,
                productName));
    }

    /**
     * Unfreeze a precedent money freezing for a specific auction by a user.
     * @param username  the user username to unfreeze the money to
     * @param auctionId the id of the auction linked to the freezing
     * @return          the just removed freezing
     * @throws FreezingNotExistsException    if no freezing exists for current username and auction
     */
    @DeleteMapping("/freezes")
    public FreezingWeb unfreezeMoney(@RequestParam final String username,
                                     @RequestParam final String auctionId)
            throws FreezingNotExistsException {
        return FreezingWeb.fromDomainFreezing(movementController.unfreezeMoneyAfterBidRaise(username, auctionId));
    }

    /**
     * Get all transactions associated to the desired user.
     * @param username  the username of the desired user
     * @return          the transactions of the specified user
     */
    @GetMapping("/transactions")
    public List<TransactionWeb> getTransactions(@RequestParam final String username) {
        return movementController.getTransactions(username).stream()
                .map(TransactionWeb::fromDomainTransaction).collect(Collectors.toList());
    }

    /**
     * Recharge budget of the user.
     * @param amount        the (positive) amount of the recharge
     * @param description   the description associated with the transaction
     * @param username      the username associated to the transaction
     * @return              the just created charging from card transaction
     * @throws MovementsHistoryNotFoundException    if there is no MovementsHistory for the user
     */
    @PostMapping("/transactions/recharge")
    public ChargingFromCardTransactionWeb rechargeBudget(@RequestParam final BigDecimal amount,
                                                         @RequestParam(required = false) final String description,
                                                         @RequestParam final String username)
            throws MovementsHistoryNotFoundException {
        return ChargingFromCardTransactionWeb.fromDomainChargingFromCardTransaction(
                movementController.rechargeBudget(amount, username, description)
        );
    }

    /**
     * Withdraw money from the user budget.
     * @param amount    the (positive) amount to be withdrawn
     * @param username  the username associated to the transaction
     * @return          the just created withdrawal transaction
     * @throws MovementsHistoryNotFoundException    if there is no MovementsHistory for the user
     * @throws InsufficientBudgetException          if the user has insufficient money in their budget
     */
    @PostMapping("/transactions/withdrawal")
    public WithdrawalTransactionWeb withdrawBudget(@RequestParam final BigDecimal amount,
                                                   @RequestParam final String username)
            throws MovementsHistoryNotFoundException, InsufficientBudgetException {
        return WithdrawalTransactionWeb.fromDomainWithdrawalTransaction(
                movementController.withdrawBudget(amount, username)
        );
    }

    /**
     * Exchange money from buyer to auction seller, after the auction closes.
     * Create the two transactions for an awarded auction, for both seller and buyer.
     * The money amount for seller will be positive, while for buyer negative.
     * @param buyerUsername     the buyer username
     * @param sellerUsername    the seller username
     * @param amount            the final amount of the awarded auction
     * @param auctionId         the id of awarded auction
     * @param productName       the name of the product of the auction in question
     * @throws FreezingNotExistsException   if no freezing exists for the winning bid of the buyer user
     */
    @PostMapping("/exchange")
    public void exchangeMoneyAfterAwardedAuction(@RequestParam final String buyerUsername,
                                                 @RequestParam final String sellerUsername,
                                                 @RequestParam final BigDecimal amount,
                                                 @RequestParam final String auctionId,
                                                 @RequestParam final String productName)
            throws FreezingNotExistsException {
        movementController.exchangeMoneyAfterAwardedAuction(buyerUsername, sellerUsername, amount, auctionId,
                productName);
    }
}
