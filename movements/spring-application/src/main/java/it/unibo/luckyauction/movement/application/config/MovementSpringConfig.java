/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.movement.application.config;

import it.unibo.luckyauction.movement.controller.MovementController;
import it.unibo.luckyauction.movement.controller.config.MovementConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MovementSpringConfig {

    /**
     * Build the movement controller of the application, capable of orchestrating movements.
     * See {@link MovementController} for more details.
     * @return      the just built movements controller
     */
    @Bean
    public MovementController movementController() {
        return new MovementConfig().movementController();
    }
}
