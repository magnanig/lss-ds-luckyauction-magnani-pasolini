module lss.ds.luckyauction.magnani.pasolini.movements.application.spring.main {
    requires lss.ds.luckyauction.magnani.pasolini.movements.domain.main;
    requires lss.ds.luckyauction.magnani.pasolini.movements.usecase.main;
    requires lss.ds.luckyauction.magnani.pasolini.movements.adapter.main;
    requires lss.ds.luckyauction.magnani.pasolini.movements.controller.main;
    requires lombok;
    requires spring.web;
    requires spring.boot;
    requires spring.beans;
    requires spring.context;
    requires spring.boot.autoconfigure;
    requires org.apache.tomcat.embed.core;
    requires com.fasterxml.jackson.databind;
}