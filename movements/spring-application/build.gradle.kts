plugins {
    id("org.springframework.boot") version "2.6.6"
}

apply(plugin = "org.springframework.boot")

rootProject.extra["mainClassProp"] = "it.unibo.luckyauction.movement.application.MovementMain"

dependencies {
    implementation(project(":movements:domain"))
    implementation(project(":movements:usecase"))
    implementation(project(":movements:adapter"))
    implementation(project(":movements:controller"))

    implementation("org.springframework.boot:spring-boot-starter-web:2.6.6")

    compileOnly("org.projectlombok:lombok:1.18.22")
    annotationProcessor("org.projectlombok:lombok:1.18.22")
}

// the version of jar task, for spring
tasks.bootJar {
    mainClass.set("it.unibo.luckyauction.movement.application.MovementMain")
    archiveBaseName.set("luckyauction-movements-spring-application")
}
