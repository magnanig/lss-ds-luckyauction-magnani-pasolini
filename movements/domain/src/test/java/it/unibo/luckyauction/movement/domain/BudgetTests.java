/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.movement.domain;

import it.unibo.luckyauction.movement.domain.exception.BudgetValidationException;
import it.unibo.luckyauction.movement.domain.valueobject.Budget;
import it.unibo.luckyauction.util.NumericalConstants;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class BudgetTests {

    @Test
    void bigDecimalDataTypeTest() {
        // if the default "equals()" method is used, the two values are different
        assertNotEquals(new BigDecimal("0.00"), BigDecimal.ZERO);
        assertNotEquals(new BigDecimal("500.00"), new BigDecimal("500"));
        // if method "A.compareTo(B)" is used, the two values are equal
        assertEquals(0, new BigDecimal("0.00").compareTo(BigDecimal.ZERO));
        assertEquals(0, new BigDecimal("500.00").compareTo(new BigDecimal("500")));
    }

    @Test
    void equalsBudgetsTest() {
        assertEquals(BigDecimal.ZERO, new Budget().value());
        assertEquals(new Budget(new BigDecimal("52.1")), new Budget(new BigDecimal("52.10")));
        assertEquals(new Budget(new BigDecimal("100")), new Budget(new BigDecimal("100.00")));
        assertNotEquals(new Budget(new BigDecimal("1000")), new Budget(new BigDecimal("100.00")));
    }

    @Test
    void sumAndSubtractBudgetTest() {
        final Budget initialBudget = new Budget();
        final Budget tenBudget = initialBudget.sum(BigDecimal.TEN);

        assertNotSame(initialBudget, tenBudget);
        assertEquals(BigDecimal.TEN, tenBudget.value());
        assertNotEquals(initialBudget.value(), tenBudget.value());

        final Budget fiveBudget = tenBudget.subtract(BigDecimal.valueOf(NumericalConstants.FIVE));
        assertNotSame(tenBudget, fiveBudget);
        assertEquals(BigDecimal.valueOf(NumericalConstants.FIVE), fiveBudget.value());
        assertNotEquals(tenBudget.value(), fiveBudget.value());
    }

    @Test
    void budgetNotNullExceptionTest() {
        final Exception exception = assertThrows(BudgetValidationException.class,
                () -> new Budget(null));
        assertTrue(exception.getMessage().contains("Budget can't be null"));
    }

    @Test
    void budgetNotNegativeExceptionTest() {
        final Exception exception = assertThrows(BudgetValidationException.class,
                () -> new Budget(new BigDecimal("-10.33")));
        assertTrue(exception.getMessage().contains("Budget can't be negative"));
    }

    @Test
    void budgetWithTooManyDecimalsExceptionTest() {
        final Exception exception = assertThrows(BudgetValidationException.class,
                () -> new Budget(new BigDecimal("43.769")));
        assertTrue(exception.getMessage().contains("have more than two decimals"));
    }
}
