/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.movement.domain;

import it.unibo.luckyauction.movement.domain.valueobject.FreezingType;
import it.unibo.luckyauction.movement.domain.valueobject.SingleUserTransactionType;
import it.unibo.luckyauction.movement.domain.valueobject.UsersPairTransactionType;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

class MovementTypeEnumTests {

    @Test
    void freezingTypeTest() {
        assertThrows(IllegalArgumentException.class, () -> FreezingType.valueOf("Freezing"));
        assertThrows(IllegalArgumentException.class, () -> FreezingType.valueOf("freezing"));
        assertNotNull(FreezingType.valueOf("FREEZING"),
                "FREEZING value must be present in the FreezingType enum");
    }

    @Test
    void freezingTypeListTest() {
        assertEquals(List.of(FreezingType.FREEZING), Arrays.stream(FreezingType.values()).toList());
    }

    @Test
    void singleUserTransactionTypeTest() {
        assertThrows(IllegalArgumentException.class, () -> SingleUserTransactionType.valueOf("Withdrawal"));
        assertThrows(IllegalArgumentException.class, () -> SingleUserTransactionType.valueOf("withdrawal"));
        assertNotNull(SingleUserTransactionType.valueOf("CHARGING_FROM_CARD"),
                "CHARGING_FROM_CARD value must be present in the SingleUserTransactionType enum");
        assertNotNull(SingleUserTransactionType.valueOf("WITHDRAWAL"),
                "WITHDRAWAL value must be present in the SingleUserTransactionType enum");
    }

    @Test
    void singleUserTransactionTypeListTest() {
        assertEquals(List.of(SingleUserTransactionType.CHARGING_FROM_CARD, SingleUserTransactionType.WITHDRAWAL),
                Arrays.stream(SingleUserTransactionType.values()).toList());
    }

    @Test
    void userPairTransactionTypeTest() {
        assertThrows(IllegalArgumentException.class, () -> UsersPairTransactionType.valueOf("awarded_auction"));
        assertNotNull(UsersPairTransactionType.valueOf("AWARDED_AUCTION"),
                "AWARDED_AUCTION value must be present in the UsersPairTransactionType enum");
    }

    @Test
    void userPairTransactionTypeListTest() {
        assertEquals(List.of(UsersPairTransactionType.AWARDED_AUCTION),
                Arrays.stream(UsersPairTransactionType.values()).toList());
    }
}
