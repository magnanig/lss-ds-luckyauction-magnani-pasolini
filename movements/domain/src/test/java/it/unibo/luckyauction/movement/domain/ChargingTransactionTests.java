/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.movement.domain;

import it.unibo.luckyauction.movement.domain.entity.ChargingFromCardTransaction;
import it.unibo.luckyauction.movement.domain.exception.MovementValidationException;
import it.unibo.luckyauction.movement.domain.valueobject.SingleUserTransactionType;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static it.unibo.luckyauction.movement.domain.MovementTestUtils.MOVEMENT_TEST_ID;
import static it.unibo.luckyauction.movement.domain.MovementTestUtils.SHOES_AUCTION_NAME;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ChargingTransactionTests {

    @Test
    void chargingFromCardTransactionTest() {
        final String rechargeDescription = "Recharge to participate in the auction of " + SHOES_AUCTION_NAME;
        final ChargingFromCardTransaction firstTransaction = ChargingFromCardTransaction.builder()
                .transactionId(MOVEMENT_TEST_ID).amount(MovementTestUtils.MOVEMENT_TEST_AMOUNT)
                .description(rechargeDescription).build();

        assertEquals(MOVEMENT_TEST_ID, firstTransaction.getMovementId());
        assertEquals(SingleUserTransactionType.CHARGING_FROM_CARD, firstTransaction.getType());
        assertEquals(rechargeDescription, firstTransaction.getDescription());
        assertEquals(0, MovementTestUtils.MOVEMENT_TEST_AMOUNT.compareTo(firstTransaction.getAmount()));

        // test optional description field
        assertDoesNotThrow(() -> ChargingFromCardTransaction.builder()
                .transactionId(MOVEMENT_TEST_ID).amount(MovementTestUtils.MOVEMENT_TEST_AMOUNT).build());
    }

    @Test
    void amountExceptionTest() {
        final Exception chargeFromCardException = assertThrows(MovementValidationException.class,
                () -> ChargingFromCardTransaction.builder().amount(BigDecimal.valueOf(10).negate()).build());
        assertTrue(chargeFromCardException.getMessage().contains("The amount of a charging transaction "
                + "must be greater than zero"));
    }
}
