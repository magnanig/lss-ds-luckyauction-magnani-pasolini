/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.movement.domain;

import it.unibo.luckyauction.movement.domain.entity.Freezing;
import it.unibo.luckyauction.movement.domain.exception.MovementValidationException;
import it.unibo.luckyauction.movement.domain.valueobject.FreezingType;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Calendar;

import static it.unibo.luckyauction.movement.domain.MovementTestUtils.AUCTION_PRICE;
import static it.unibo.luckyauction.movement.domain.MovementTestUtils.AUCTION_TEST_ID;
import static it.unibo.luckyauction.movement.domain.MovementTestUtils.SHOES_AUCTION_NAME;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class FreezingTests {

    @Test
    void freezingTest() {
        final Freezing freezingMovement = Freezing.builder().freezingId(MovementTestUtils.MOVEMENT_TEST_ID)
                .productName(SHOES_AUCTION_NAME).amount(MovementTestUtils.MOVEMENT_TEST_AMOUNT)
                .auctionId(AUCTION_TEST_ID).build();

        assertEquals(MovementTestUtils.MOVEMENT_TEST_ID, freezingMovement.getMovementId());
        assertEquals(FreezingType.FREEZING, freezingMovement.getType());
        assertEquals(AUCTION_TEST_ID, freezingMovement.getAuctionId());
        assertEquals(SHOES_AUCTION_NAME, freezingMovement.getProductName());
        assertTrue(freezingMovement.getTimestamp().compareTo(Calendar.getInstance()) <= 0,
                "The timestamp must be less than or equal to the current time instant");
        assertEquals(0, MovementTestUtils.MOVEMENT_TEST_AMOUNT.compareTo(freezingMovement.getAmount()));
    }

    @Test
    void amountExceptionTest() {
        final Exception negativeAmountException = assertThrows(MovementValidationException.class,
                () -> Freezing.builder().amount(BigDecimal.valueOf(10).negate())
                        .auctionId(AUCTION_TEST_ID).productName(SHOES_AUCTION_NAME).build());
        assertTrue(negativeAmountException.getMessage().contains("Freezing amount must be greater than 0"));
    }

    @Test
    void auctionIdExceptionTest() {
        final Exception nullAuctionIdException = assertThrows(MovementValidationException.class,
                () -> Freezing.builder().amount(AUCTION_PRICE).productName(SHOES_AUCTION_NAME).build());
        assertTrue(nullAuctionIdException.getMessage().contains("Auction id is not set"));

        final Exception emptyAuctionIdException = assertThrows(MovementValidationException.class,
                () -> Freezing.builder().amount(AUCTION_PRICE).auctionId("").productName(SHOES_AUCTION_NAME).build());
        assertTrue(emptyAuctionIdException.getMessage().contains("Auction id is not set"));
    }

    @Test
    void productNameExceptionTest() {
        final Exception nullProductNameException = assertThrows(MovementValidationException.class,
                () -> Freezing.builder().amount(AUCTION_PRICE).auctionId(AUCTION_TEST_ID).build());
        assertTrue(nullProductNameException.getMessage().contains("Product name is not set"));

        final Exception emptyProductNameException = assertThrows(MovementValidationException.class,
                () -> Freezing.builder().amount(AUCTION_PRICE).auctionId(AUCTION_TEST_ID).productName("").build());
        assertTrue(emptyProductNameException.getMessage().contains("Product name is not set"));
    }
}
