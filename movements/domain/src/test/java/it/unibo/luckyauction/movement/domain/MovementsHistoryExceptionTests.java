/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.movement.domain;

import it.unibo.luckyauction.movement.domain.entity.ChargingFromCardTransaction;
import it.unibo.luckyauction.movement.domain.entity.Freezing;
import it.unibo.luckyauction.movement.domain.entity.MovementsHistory;
import it.unibo.luckyauction.movement.domain.entity.WithdrawalTransaction;
import it.unibo.luckyauction.movement.domain.exception.FreezingNotExistsException;
import it.unibo.luckyauction.movement.domain.exception.InsufficientBudgetException;
import it.unibo.luckyauction.movement.domain.exception.MovementAlreadyExistsException;
import it.unibo.luckyauction.movement.domain.exception.MovementsHistoryValidationException;
import org.junit.jupiter.api.Test;

import static it.unibo.luckyauction.movement.domain.MovementTestUtils.AUCTION_TEST_ID;
import static it.unibo.luckyauction.movement.domain.MovementTestUtils.FIRST_USERNAME;
import static it.unibo.luckyauction.movement.domain.MovementTestUtils.MOVEMENTS_HISTORY_TEST_ID;
import static it.unibo.luckyauction.movement.domain.MovementTestUtils.MOVEMENT_TEST_AMOUNT;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class MovementsHistoryExceptionTests {

    private static final String ALREADY_EXISTS_ASSERTION_MESSAGE = "already exists";

    private static final ChargingFromCardTransaction CHARGING_TRANSACTION = ChargingFromCardTransaction.builder()
            .transactionId("01").amount(MOVEMENT_TEST_AMOUNT).build();

    private static final WithdrawalTransaction WITHDRAWAL_WITH_SAME_ID = WithdrawalTransaction.builder()
            .transactionId("01").amount(MOVEMENT_TEST_AMOUNT).build();

    private static final Freezing FREEZING = Freezing.builder()
            .freezingId("001").productName(MovementTestUtils.SHOES_AUCTION_NAME)
            .amount(MOVEMENT_TEST_AMOUNT).auctionId("firstAuctionId").build();

    private static final Freezing FREEZING_WITH_SAME_ID = Freezing.builder()
            .freezingId("001").productName(MovementTestUtils.SHOES_AUCTION_NAME)
            .amount(MOVEMENT_TEST_AMOUNT).auctionId("secondAuctionId").build();

    @Test
    void usernameException() {
        final Exception nullUsernameException = assertThrows(MovementsHistoryValidationException.class,
                () -> MovementsHistory.builder().build());
        assertTrue(nullUsernameException.getMessage().contains("The username is null or empty"));

        final Exception emptyUsernameException = assertThrows(MovementsHistoryValidationException.class,
                () -> MovementsHistory.builder().username("").build());
        assertTrue(emptyUsernameException.getMessage().contains("The username is null or empty"));
    }

    @Test
    void unfreezeExceptionTest() {
        final MovementsHistory movementsHistory = MovementsHistory.builder()
                .movementsHistoryId(MOVEMENTS_HISTORY_TEST_ID).username(FIRST_USERNAME).build();

        final Exception exception = assertThrows(FreezingNotExistsException.class,
                () -> movementsHistory.unfreezeMoney(AUCTION_TEST_ID));
        assertTrue(exception.getMessage().contains("No freezing for auction #" + AUCTION_TEST_ID + " found"));
    }

    @Test
    void insertTransactionWithSameIdExceptionTest() {
        final MovementsHistory movementsHistory = MovementsHistory.builder()
                .movementsHistoryId(MOVEMENTS_HISTORY_TEST_ID).username(FIRST_USERNAME).build();

        movementsHistory.rechargeBudget(CHARGING_TRANSACTION);
        final Exception exception = assertThrows(MovementAlreadyExistsException.class,
                () -> movementsHistory.withdrawBudget(WITHDRAWAL_WITH_SAME_ID));
        assertTrue(exception.getMessage().contains(ALREADY_EXISTS_ASSERTION_MESSAGE));
    }

    @Test
    void insertFreezingWithSameIdExceptionTest() {
        final MovementsHistory movementsHistory = MovementsHistory.builder()
                .movementsHistoryId(MOVEMENTS_HISTORY_TEST_ID).username(FIRST_USERNAME).build();

        movementsHistory.rechargeBudget(CHARGING_TRANSACTION);
        movementsHistory.freezeMoney(FREEZING);
        final Exception exception = assertThrows(MovementAlreadyExistsException.class,
                () -> movementsHistory.freezeMoney(FREEZING_WITH_SAME_ID));
        assertTrue(exception.getMessage().contains(ALREADY_EXISTS_ASSERTION_MESSAGE));
    }

    @Test
    void insufficientBudgetExceptionTest() {
        final MovementsHistory movementsHistory = MovementsHistory.builder()
                .movementsHistoryId(MOVEMENTS_HISTORY_TEST_ID).username(FIRST_USERNAME).build();

        final Exception exception = assertThrows(InsufficientBudgetException.class,
                () -> movementsHistory.withdrawBudget(WithdrawalTransaction.builder().transactionId("01")
                        .amount(MOVEMENT_TEST_AMOUNT).build()));
        assertTrue(exception.getMessage().contains("Unable to subtract € " + MOVEMENT_TEST_AMOUNT));
    }
}
