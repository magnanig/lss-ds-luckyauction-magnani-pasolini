/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.movement.domain;

import it.unibo.luckyauction.movement.domain.entity.WithdrawalTransaction;
import it.unibo.luckyauction.movement.domain.exception.MovementValidationException;
import it.unibo.luckyauction.movement.domain.valueobject.SingleUserTransactionType;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.function.Supplier;

import static it.unibo.luckyauction.movement.domain.MovementTestUtils.MOVEMENT_TEST_AMOUNT;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class WithdrawalTransactionTests {


    // <<<---------------------------  IN COMMON TO ALL MOVEMENTS  --------------------------->>>


    @Test
    void movementAmountExceptionTest() {
        final Supplier<WithdrawalTransaction.WithdrawalTransactionBuilder> preBuiltMovement =
                WithdrawalTransaction::builder;

        final Exception nullAmountException = assertThrows(MovementValidationException.class,
                () -> preBuiltMovement.get().build());
        assertTrue(nullAmountException.getMessage().contains("The movement amount cannot be null"));

        final Exception zeroAmountException = assertThrows(MovementValidationException.class,
                () -> preBuiltMovement.get().amount(BigDecimal.ZERO).build());
        assertTrue(zeroAmountException.getMessage().contains("cannot be zero"));

        final Exception positiveDecimalsException = assertThrows(MovementValidationException.class,
                () -> preBuiltMovement.get().amount(new BigDecimal("1.00999")).build());
        assertTrue(positiveDecimalsException.getMessage().contains("amount can't have more than 2 decimals"));

        final Exception negativeDecimalsException = assertThrows(MovementValidationException.class,
                () -> preBuiltMovement.get().amount(new BigDecimal("-1.00999")).build());
        assertTrue(negativeDecimalsException.getMessage().contains("amount can't have more than 2 decimals"));
    }

    @Test
    void movementTimestampExceptionTest() {
        final Calendar afterOneDay = Calendar.getInstance();
        afterOneDay.add(Calendar.DATE, 1);

        final Exception timestampException = assertThrows(MovementValidationException.class,
                () -> WithdrawalTransaction.builder().amount(MOVEMENT_TEST_AMOUNT).timestamp(afterOneDay).build());
        assertTrue(timestampException.getMessage().contains("timestamp cannot be a future date"));
    }

    @Test
    void movementTimestampTest() {
        final Supplier<WithdrawalTransaction.WithdrawalTransactionBuilder> preBuiltWithdrawal =
                () -> WithdrawalTransaction.builder().amount(MOVEMENT_TEST_AMOUNT);
        final Calendar specificTimestamp = Calendar.getInstance();
        specificTimestamp.add(Calendar.YEAR, -1);

        final WithdrawalTransaction movementWithDefaultTimestamp = preBuiltWithdrawal.get().build();
        final WithdrawalTransaction movementWithSpecificTimestamp = preBuiltWithdrawal.get()
                .timestamp(specificTimestamp).build();

        assertEquals(specificTimestamp, movementWithSpecificTimestamp.getTimestamp());
        assertTrue(movementWithSpecificTimestamp.getTimestamp().before(Calendar.getInstance()),
                "The timestamp must be before the current date");
        assertTrue(movementWithDefaultTimestamp.getTimestamp().after(movementWithSpecificTimestamp.getTimestamp()),
                "The default timestamp must be after the specific timestamp");
    }


    // <<<---------------------------   SPECIFIC FOR WITHDRAWALS   --------------------------->>>


    @Test
    void withdrawalTransactionTest() {
        final WithdrawalTransaction wdTransaction = WithdrawalTransaction.builder()
                .transactionId(MovementTestUtils.MOVEMENT_TEST_ID).amount(MOVEMENT_TEST_AMOUNT).build();

        assertEquals(MovementTestUtils.MOVEMENT_TEST_ID, wdTransaction.getMovementId());
        assertEquals(SingleUserTransactionType.WITHDRAWAL, wdTransaction.getType());
        assertEquals(0, MOVEMENT_TEST_AMOUNT.negate().compareTo(wdTransaction.getAmount()));
    }

    @Test
    void negativeWithdrawalTransactionTest() {
        final BigDecimal positiveAmount = BigDecimal.valueOf(10);
        final BigDecimal negativeAmount = positiveAmount.negate();

        final WithdrawalTransaction positiveWithdrawal = WithdrawalTransaction.builder().amount(positiveAmount).build();
        final WithdrawalTransaction negativeWithdrawal = WithdrawalTransaction.builder().amount(negativeAmount).build();
        assertEquals(negativeAmount, positiveWithdrawal.getAmount());
        assertEquals(negativeAmount, negativeWithdrawal.getAmount());
    }
}
