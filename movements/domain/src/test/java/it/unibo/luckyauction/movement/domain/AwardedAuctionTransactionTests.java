/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.movement.domain;

import it.unibo.luckyauction.movement.domain.entity.AwardedAuctionTransaction;
import it.unibo.luckyauction.movement.domain.exception.MovementValidationException;
import it.unibo.luckyauction.movement.domain.valueobject.AwardedAuctionTransactionsPair;
import it.unibo.luckyauction.movement.domain.valueobject.UsersPairTransactionType;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.function.Supplier;

import static it.unibo.luckyauction.movement.domain.MovementTestUtils.AUCTION_TEST_ID;
import static it.unibo.luckyauction.movement.domain.MovementTestUtils.FIRST_USERNAME;
import static it.unibo.luckyauction.movement.domain.MovementTestUtils.SECOND_USERNAME;
import static it.unibo.luckyauction.movement.domain.MovementTestUtils.SHOES_AUCTION_NAME;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class AwardedAuctionTransactionTests {

    private static final Calendar TIMESTAMP = Calendar.getInstance();

    @Test
    void awardedAuctionTransactionsPairTest() {
        final BigDecimal finalAmount = BigDecimal.TEN;
        final AwardedAuctionTransactionsPair transactionsPair = new AwardedAuctionTransactionsPair(
                createNewAwardedAuctionTransaction(finalAmount), // for seller
                createNewAwardedAuctionTransaction(finalAmount.negate()) // for buyer
        );

        assertEquals(createNewAwardedAuctionTransaction(finalAmount), transactionsPair.sellerTransaction());
        assertEquals(createNewAwardedAuctionTransaction(finalAmount.negate()), transactionsPair.buyerTransaction());
    }

    @Test
    void awardedAuctionTransactionTest() {
        final AwardedAuctionTransaction aaTransaction = createNewAwardedAuctionTransaction();

        assertEquals(MovementTestUtils.MOVEMENT_TEST_ID, aaTransaction.getMovementId());
        assertEquals(TIMESTAMP.getTime(), aaTransaction.getTimestamp().getTime());
        assertEquals(SHOES_AUCTION_NAME, aaTransaction.getProductName());
        assertEquals(UsersPairTransactionType.AWARDED_AUCTION, aaTransaction.getType());
        assertEquals(FIRST_USERNAME, aaTransaction.getBuyer());
        assertEquals(SECOND_USERNAME, aaTransaction.getSeller());
        assertEquals(AUCTION_TEST_ID, aaTransaction.getAuctionId());
        assertEquals(0, MovementTestUtils.MOVEMENT_TEST_AMOUNT.compareTo(aaTransaction.getAmount()));
    }

    @Test
    void usersPairTransactionExceptionsTest() {
        final Supplier<AwardedAuctionTransaction.AwardedAuctionTransactionBuilder> preBuilt =
                () -> AwardedAuctionTransaction.builder().amount(MovementTestUtils.MOVEMENT_TEST_AMOUNT)
                        .auctionId(AUCTION_TEST_ID).productName(SHOES_AUCTION_NAME);

        final Exception firstException = assertThrows(MovementValidationException.class,
                () -> preBuilt.get().buyerUsername(FIRST_USERNAME).sellerUsername(FIRST_USERNAME).build());
        assertTrue(firstException.getMessage().contains("Buyer username and seller username can't be the same"));

        final Exception nullBuyerException = assertThrows(MovementValidationException.class,
                () -> preBuilt.get().sellerUsername(FIRST_USERNAME).build());
        assertTrue(nullBuyerException.getMessage().contains("Buyer username is not set"));

        final Exception emptyBuyerException = assertThrows(MovementValidationException.class,
                () -> preBuilt.get().sellerUsername(FIRST_USERNAME).buyerUsername("").build());
        assertTrue(emptyBuyerException.getMessage().contains("Buyer username is not set"));

        final Exception nullSellerException = assertThrows(MovementValidationException.class,
                () -> preBuilt.get().buyerUsername(FIRST_USERNAME).build());
        assertTrue(nullSellerException.getMessage().contains("Seller username is not set"));

        final Exception emptySellerException = assertThrows(MovementValidationException.class,
                () -> preBuilt.get().sellerUsername("").buyerUsername(FIRST_USERNAME).build());
        assertTrue(emptySellerException.getMessage().contains("Seller username is not set"));
    }

    @Test
    void transactionAuctionIdExceptionTest() {
        final Supplier<AwardedAuctionTransaction.AwardedAuctionTransactionBuilder> preBuilt =
                () -> AwardedAuctionTransaction.builder().amount(MovementTestUtils.MOVEMENT_TEST_AMOUNT)
                        .buyerUsername(FIRST_USERNAME).sellerUsername(SECOND_USERNAME).productName(SHOES_AUCTION_NAME);

        final Exception nullAuctionIdException = assertThrows(MovementValidationException.class,
                () -> preBuilt.get().build());
        assertTrue(nullAuctionIdException.getMessage().contains("Auction ID is not set"));

        final Exception emptyAuctionIdException = assertThrows(MovementValidationException.class,
                () -> preBuilt.get().auctionId("").build());
        assertTrue(emptyAuctionIdException.getMessage().contains("Auction ID is not set"));
    }

    @Test
    void transactionProductNameExceptionTest() {
        final Supplier<AwardedAuctionTransaction.AwardedAuctionTransactionBuilder> preBuilt =
                () -> AwardedAuctionTransaction.builder().amount(MovementTestUtils.MOVEMENT_TEST_AMOUNT)
                        .buyerUsername(FIRST_USERNAME).sellerUsername(SECOND_USERNAME).auctionId(AUCTION_TEST_ID);

        final Exception nullProductNameException = assertThrows(MovementValidationException.class,
                () -> preBuilt.get().build());
        assertTrue(nullProductNameException.getMessage().contains("Product name is not set"));

        final Exception emptyProductNameException = assertThrows(MovementValidationException.class,
                () -> preBuilt.get().productName("").build());
        assertTrue(emptyProductNameException.getMessage().contains("Product name is not set"));
    }

    private AwardedAuctionTransaction createNewAwardedAuctionTransaction(final BigDecimal... desiredAmount) {
        final BigDecimal amount = desiredAmount.length > 0 ? desiredAmount[0] : MovementTestUtils.MOVEMENT_TEST_AMOUNT;
        return AwardedAuctionTransaction.builder()
                .transactionId(MovementTestUtils.MOVEMENT_TEST_ID).productName(SHOES_AUCTION_NAME)
                .amount(amount).buyerUsername(FIRST_USERNAME).timestamp(TIMESTAMP)
                .sellerUsername(SECOND_USERNAME).auctionId(AUCTION_TEST_ID)
                .productName(SHOES_AUCTION_NAME).build();
    }
}
