/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.movement.domain;

import it.unibo.luckyauction.movement.domain.entity.AwardedAuctionTransaction;
import it.unibo.luckyauction.movement.domain.entity.ChargingFromCardTransaction;
import it.unibo.luckyauction.movement.domain.entity.Freezing;
import it.unibo.luckyauction.movement.domain.entity.MovementsHistory;
import it.unibo.luckyauction.movement.domain.entity.WithdrawalTransaction;
import it.unibo.luckyauction.movement.domain.valueobject.Budget;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

import static it.unibo.luckyauction.movement.domain.MovementTestUtils.AUCTION_TEST_ID;
import static it.unibo.luckyauction.movement.domain.MovementTestUtils.MOVEMENTS_HISTORY_TEST_ID;
import static it.unibo.luckyauction.movement.domain.MovementTestUtils.MOVEMENT_TEST_AMOUNT;
import static it.unibo.luckyauction.movement.domain.MovementTestUtils.SHOES_AUCTION_NAME;
import static org.junit.jupiter.api.Assertions.assertEquals;

class MovementsHistoryTests {

    private static final String NP_USERNAME = "np";
    private static final String GM_USERNAME = "gm";

    private static final AwardedAuctionTransaction AWARDED_AUCTION_TRANSACTION = AwardedAuctionTransaction.builder()
            .transactionId("01").auctionId(AUCTION_TEST_ID).amount(MOVEMENT_TEST_AMOUNT)
            .buyerUsername(NP_USERNAME).sellerUsername(GM_USERNAME).productName(SHOES_AUCTION_NAME).build();

    private static final WithdrawalTransaction WITHDRAWAL_TRANSACTION = WithdrawalTransaction.builder()
            .transactionId("03").amount(MOVEMENT_TEST_AMOUNT).build();

    private static final Freezing FREEZING_FOR_AUCTION_TEST_ID = Freezing.builder().freezingId("001")
            .auctionId(AUCTION_TEST_ID).productName(SHOES_AUCTION_NAME).amount(MOVEMENT_TEST_AMOUNT).build();

    private static final Freezing SECOND_FREEZING = Freezing.builder().freezingId("002").auctionId("testAuctionId")
            .productName(SHOES_AUCTION_NAME).amount(MOVEMENT_TEST_AMOUNT).build();

    @Test
    void movementsHistoryGettersTest() {
        final MovementsHistory emptyMovementsHistory = MovementsHistory.builder()
                .movementsHistoryId(MOVEMENTS_HISTORY_TEST_ID).username(NP_USERNAME).build();
        assertEquals(new Budget(), emptyMovementsHistory.getBudget());
        assertEquals(Collections.emptyList(), emptyMovementsHistory.getTransactions());
        assertEquals(Collections.emptyList(), emptyMovementsHistory.getFreezes());

        final MovementsHistory movementsHistory = MovementsHistory.builder()
                .movementsHistoryId(MOVEMENTS_HISTORY_TEST_ID).username(NP_USERNAME).budget(new Budget(BigDecimal.TEN))
                .freezes(List.of(FREEZING_FOR_AUCTION_TEST_ID)).transactions(List.of(WITHDRAWAL_TRANSACTION)).build();
        assertEquals(MOVEMENTS_HISTORY_TEST_ID, movementsHistory.getMovementsHistoryId());
        assertEquals(NP_USERNAME, movementsHistory.getUsername());
        assertEquals(0, BigDecimal.TEN.compareTo(movementsHistory.getBudget().value())); // budget is 10.00 €
        assertEquals(List.of(WITHDRAWAL_TRANSACTION), movementsHistory.getTransactions());
        assertEquals(List.of(FREEZING_FOR_AUCTION_TEST_ID), movementsHistory.getFreezes());
    }

    @Test
    void chargingAndWithdrawalTest() {
        final MovementsHistory movementsHistory = MovementsHistory.builder()
                .movementsHistoryId(MOVEMENTS_HISTORY_TEST_ID).username(NP_USERNAME).build();
        final BigDecimal amountToCharge = MOVEMENT_TEST_AMOUNT.multiply(new BigDecimal(2));
        final ChargingFromCardTransaction chargingTransaction = ChargingFromCardTransaction.builder()
                .transactionId("1").amount(amountToCharge).build();

        movementsHistory.rechargeBudget(chargingTransaction);
        assertEquals(amountToCharge, movementsHistory.getBudget().value());
        assertEquals(List.of(chargingTransaction), movementsHistory.getTransactions());

        movementsHistory.withdrawBudget(WITHDRAWAL_TRANSACTION);
        assertEquals(MOVEMENT_TEST_AMOUNT, movementsHistory.getBudget().value());
        assertEquals(List.of(chargingTransaction, WITHDRAWAL_TRANSACTION), movementsHistory.getTransactions());
        assertEquals(Collections.emptyList(), movementsHistory.getFreezes());
    }

    @Test
    void freezeAndUnfreezeTest() {
        final MovementsHistory movementsHistory = MovementsHistory.builder()
                .movementsHistoryId(MOVEMENTS_HISTORY_TEST_ID).username(NP_USERNAME).build();
        final BigDecimal amountToCharge = MOVEMENT_TEST_AMOUNT.multiply(new BigDecimal(2));
        final ChargingFromCardTransaction chargingTransaction = ChargingFromCardTransaction.builder()
                .transactionId("1").amount(amountToCharge).build();
        movementsHistory.rechargeBudget(chargingTransaction);

        movementsHistory.freezeMoney(FREEZING_FOR_AUCTION_TEST_ID);
        assertEquals(List.of(FREEZING_FOR_AUCTION_TEST_ID), movementsHistory.getFreezes());
        assertEquals(MOVEMENT_TEST_AMOUNT, movementsHistory.getBudget().value());
        movementsHistory.freezeMoney(SECOND_FREEZING);
        assertEquals(List.of(FREEZING_FOR_AUCTION_TEST_ID, SECOND_FREEZING), movementsHistory.getFreezes());
        assertEquals(0, BigDecimal.ZERO.compareTo(movementsHistory.getBudget().value())); // budget is 0.00 €

        movementsHistory.unfreezeMoney(FREEZING_FOR_AUCTION_TEST_ID.getAuctionId());
        assertEquals(List.of(SECOND_FREEZING), movementsHistory.getFreezes());
        assertEquals(MOVEMENT_TEST_AMOUNT, movementsHistory.getBudget().value());
        movementsHistory.unfreezeMoney(SECOND_FREEZING.getAuctionId());
        assertEquals(Collections.emptyList(), movementsHistory.getFreezes());
        assertEquals(amountToCharge, movementsHistory.getBudget().value());

        assertEquals(List.of(chargingTransaction), movementsHistory.getTransactions());
    }

    @Test
    void updateBudgetAfterAwardedAuctionTest() {
        final MovementsHistory historyNp = MovementsHistory.builder() // np simulate the buyer user
                .movementsHistoryId(MOVEMENTS_HISTORY_TEST_ID).username(NP_USERNAME).build();
        final MovementsHistory historyGm = MovementsHistory.builder() // gm simulate the seller user
                .movementsHistoryId(MOVEMENTS_HISTORY_TEST_ID).username(GM_USERNAME).build();

        final ChargingFromCardTransaction chargingTransaction = ChargingFromCardTransaction.builder()
                .transactionId("1").amount(MOVEMENT_TEST_AMOUNT).build();

        historyNp.rechargeBudget(chargingTransaction);
        historyNp.freezeMoney(FREEZING_FOR_AUCTION_TEST_ID);
        historyNp.updateBudgetAfterAwardedAuction(AWARDED_AUCTION_TRANSACTION);
        historyGm.updateBudgetAfterAwardedAuction(AWARDED_AUCTION_TRANSACTION);

        assertEquals(Collections.emptyList(), historyNp.getFreezes());
        assertEquals(List.of(chargingTransaction, AWARDED_AUCTION_TRANSACTION), historyNp.getTransactions());
        assertEquals(0, BigDecimal.ZERO.compareTo(historyNp.getBudget().value())); // budget must be 0.00 €

        assertEquals(Collections.emptyList(), historyGm.getFreezes());
        assertEquals(List.of(AWARDED_AUCTION_TRANSACTION), historyGm.getTransactions());
        assertEquals(MOVEMENT_TEST_AMOUNT, historyGm.getBudget().value());
    }
}
