/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.movement.domain;

import java.math.BigDecimal;

public final class MovementTestUtils {

    /**
     * ID of a generic auction.
     */
    public static final String AUCTION_TEST_ID = "4567";
    /**
     * Starting price of a generic auction.
     */
    public static final BigDecimal AUCTION_PRICE = BigDecimal.valueOf(50.00);
    /**
     * Name of a generic pair of shoes.
     */
    public static final String SHOES_AUCTION_NAME = "Golden Goose shoes white and blue limited edition 2015";


    /**
     * ID of a generic test movement.
     */
    public static final String MOVEMENT_TEST_ID = "0123";
    /**
     * ID of a generic test MovementsHistory.
     */
    public static final String MOVEMENTS_HISTORY_TEST_ID = "movHistoryId";
    /**
     * ID of the another MovementsHistory.
     */
    public static final String ANOTHER_MOVEMENTS_HISTORY_ID = "anotherMovHistoryId";
    /**
     * Test amount for a generic movement.
     */
    public static final BigDecimal MOVEMENT_TEST_AMOUNT = new BigDecimal("52.38");



    /**
     * Username of the first test user.
     */
    public static final String FIRST_USERNAME = "firstUsername";
    /**
     * Username of the second test user.
     */
    public static final String SECOND_USERNAME = "secondUsername";

    private MovementTestUtils() { }
}
