module lss.ds.luckyauction.magnani.pasolini.movements.domain.main {
    exports it.unibo.luckyauction.movement.domain.entity;
    exports it.unibo.luckyauction.movement.domain.exception;
    exports it.unibo.luckyauction.movement.domain.valueobject;
    requires lss.ds.luckyauction.magnani.pasolini.utils.main;
    requires lombok;
}