/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.movement.domain.entity;

import it.unibo.luckyauction.movement.domain.exception.MovementValidationException;
import it.unibo.luckyauction.movement.domain.valueobject.SingleUserTransactionType;

import java.math.BigDecimal;
import java.util.Calendar;

/**
 * A transaction representing a recharge from user payment card.
 */
public final class ChargingFromCardTransaction extends SingleUserTransaction {

    private final String description;

    /**
     * Create a new transaction for a recharge from user payment card.
     * @param timestamp     the timestamp for the transaction
     * @param amount        the money amount recharged
     * @param description   a description for the transaction
     */
    private ChargingFromCardTransaction(final String transactionId, final Calendar timestamp, final BigDecimal amount,
                                        final String description) {
        super(transactionId, timestamp, amount, SingleUserTransactionType.CHARGING_FROM_CARD);
        this.description = description;
    }

    /**
     * Get the description for the transaction.
     * @return  the description for the transaction
     */
    public String getDescription() {
        return description;
    }

    /**
     * Get the builder of ChargingFromCardTransaction objects.
     * @return  the ChargingFromCardTransaction builder instance
     */
    public static ChargingFromCardTransactionBuilder builder() {
        return new ChargingFromCardTransactionBuilder();
    }

    @SuppressWarnings({"checkstyle:all", "PMD.AvoidFieldNameMatchingMethodName"})
    public static class ChargingFromCardTransactionBuilder
            extends SingleUserTransaction.Builder<ChargingFromCardTransactionBuilder, ChargingFromCardTransaction> {

        protected String description;

        public ChargingFromCardTransactionBuilder description(final String description) {
            this.description = description;
            return this;
        }

        @Override
        protected ChargingFromCardTransaction createObject() {
            return new ChargingFromCardTransaction(movementId, timestamp, amount, description);
        }

        @Override
        protected void validate() {
            super.validate();
            if (amount.compareTo(BigDecimal.ZERO) <= 0) {
                throw new MovementValidationException("The amount of a charging transaction must be greater than zero");
            }
        }

    }

}
