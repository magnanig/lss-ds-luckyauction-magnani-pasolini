/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.movement.domain.entity;

import it.unibo.luckyauction.movement.domain.valueobject.TransactionType;

import java.math.BigDecimal;
import java.util.Calendar;

/**
 * A generic money transaction.
 */
public abstract class Transaction extends Movement {

    /**
     * Create a new transaction with the specified arguments.
     *
     * @param transactionId the id of the transaction
     * @param timestamp     the timestamp for the transaction
     * @param amount        the money amount exchanged
     * @param type          the transaction type (see {@link TransactionType})
     */
    protected Transaction(final String transactionId, final Calendar timestamp, final BigDecimal amount,
                          final TransactionType type) {
        super(transactionId, timestamp, amount, type);
    }

    /**
     * Get the money amount exchanged.
     * @return  the money amount exchanged
     */
    @SuppressWarnings("PMD.UselessOverridingMethod") // this is an override just to redefine javadoc properly
    @Override
    public BigDecimal getAmount() {
        return super.getAmount();
    }

    /**
     * Get the type of the transaction.
     * @return  the type of the transaction
     */
    @Override
    public TransactionType getType() {
        return (TransactionType) super.getType();
    }

    @SuppressWarnings("checkstyle:all")
    public abstract static class Builder<T extends Builder<T, R>, R extends Transaction>
            extends Movement.Builder<T, R> {

        public T transactionId(final String transactionId) {
            return movementId(transactionId);
        }

    }
}
