/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.movement.domain.exception;

import it.unibo.luckyauction.movement.domain.valueobject.Budget;

import java.io.Serial;
import java.math.BigDecimal;

/**
 * The exception to throw when an user has not enough money to perform
 * an operation.
 */
public class InsufficientBudgetException extends RuntimeException {
    @Serial
    private static final long serialVersionUID = -7298893402289714185L;

    public InsufficientBudgetException(final String message) {
        super(message);
    }

    public InsufficientBudgetException(final Budget actualBudget, final BigDecimal desiredAmount) {
        this("Unable to subtract € " + desiredAmount + " from user's current budget (€ " + actualBudget.value() + ")");
    }
}
