/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.movement.domain.entity;

import it.unibo.luckyauction.movement.domain.valueobject.SingleUserTransactionType;

import java.math.BigDecimal;
import java.util.Calendar;

/**
 * Represents a transaction involving a single user.
 */
public abstract class SingleUserTransaction extends Transaction {

    /**
     * Create a new transaction for a single user, with the specified arguments.
     *
     * @param transactionId the id of the transaction
     * @param timestamp     the timestamp for the transaction
     * @param amount        the money amount exchanged
     * @param type          the single user transaction type (see {@link SingleUserTransactionType})
     */
    protected SingleUserTransaction(final String transactionId, final Calendar timestamp, final BigDecimal amount,
                                    final SingleUserTransactionType type) {
        super(transactionId, timestamp, amount, type);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SingleUserTransactionType getType() {
        return (SingleUserTransactionType) super.getType();
    }

    public abstract static class Builder<T extends Builder<T, R>, R extends SingleUserTransaction> extends
            Transaction.Builder<T, R> { }
}
