/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.movement.domain.entity;

import it.unibo.luckyauction.movement.domain.valueobject.SingleUserTransactionType;

import java.math.BigDecimal;
import java.util.Calendar;

/**
 * A transaction representing the withdrawal from user budget.
 */
public final class WithdrawalTransaction extends SingleUserTransaction {

    /**
     * Create a new transaction for a withdrawal.
     * @param timestamp     the timestamp for the transaction
     * @param amount        the money amount withdrawn
     */
    private WithdrawalTransaction(final String transactionId, final Calendar timestamp, final BigDecimal amount) {
        super(transactionId, timestamp, amount, SingleUserTransactionType.WITHDRAWAL);
    }

    /**
     * Get the builder of WithdrawalTransaction objects.
     * @return  the WithdrawalTransaction builder instance
     */
    public static WithdrawalTransactionBuilder builder() {
        return new WithdrawalTransactionBuilder();
    }

    @SuppressWarnings("checkstyle:all")
    public static class WithdrawalTransactionBuilder
            extends Builder<WithdrawalTransactionBuilder, WithdrawalTransaction> {

        @Override
        protected WithdrawalTransaction createObject() {
            return new WithdrawalTransaction(movementId, timestamp, amount);
        }

        @Override
        protected void validate() {
            super.validate();
            if (amount.compareTo(BigDecimal.ZERO) > 0) {
                amount = amount.negate();
            }
        }
    }

}
