/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.movement.domain.valueobject;

import it.unibo.luckyauction.movement.domain.exception.BudgetValidationException;
import lombok.Generated;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * Represents the concept of user budget, intended as the money
 * amount they can spend, in Euro (€).
 *
 * @param   value the money amount
 */
public record Budget(BigDecimal value) {

    public Budget {
        if (value == null) {
            throw new BudgetValidationException("Budget can't be null");
        }
        if (value.compareTo(BigDecimal.ZERO) < 0 || value.scale() > 2) {
            throw new BudgetValidationException("Budget can't be negative or have more than two decimals");
        }
    }

    /**
     * Create a new budget with no credit (0.00 €).
     */
    public Budget() {
        this(BigDecimal.ZERO);
    }

    /**
     * Sum the specified amount to current budget.
     * @param amount    the amount to sum
     * @return          the new budget, obtained by summing the specified
     *                  amount to current budget
     */
    public Budget sum(final BigDecimal amount) {
        return new Budget(value.add(amount));
    }

    /**
     * Subtract the specified amount from current budget.
     * @param amount    the amount to subtract
     * @return          the new budget, obtained by subtracting the specified
     */
    public Budget subtract(final BigDecimal amount) {
        return new Budget(value.subtract(amount));
    }

    /**
     * {@inheritDoc}
     */
    @Override @Generated
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final Budget budget = (Budget) o;
        return value.compareTo(budget.value) == 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override @Generated
    public int hashCode() {
        return Objects.hash(value);
    }

    /**
     * Convert budget to string.
     * @return the money amount as string
     */
    @Override @Generated
    public String toString() {
        return value.toString();
    }

}
