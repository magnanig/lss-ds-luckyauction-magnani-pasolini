/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.movement.domain.entity;

import it.unibo.luckyauction.movement.domain.exception.FreezingNotExistsException;
import it.unibo.luckyauction.movement.domain.exception.InsufficientBudgetException;
import it.unibo.luckyauction.movement.domain.exception.MovementAlreadyExistsException;
import it.unibo.luckyauction.movement.domain.exception.MovementsHistoryValidationException;
import it.unibo.luckyauction.movement.domain.valueobject.Budget;
import lombok.Generated;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * The history of movements of a user.
 */
public final class MovementsHistory {

    private final String movementsHistoryId;
    private final String username;
    private Budget budget;

    private final List<Transaction> transactions;
    private final List<Freezing> freezes;

    private MovementsHistory(final String movementsHistoryId, final String username, final Budget budget,
                             final List<Transaction> transactions, final List<Freezing> freezes) {
        this.movementsHistoryId = movementsHistoryId;
        this.username = username;
        this.budget = budget;
        this.transactions = transactions;
        this.freezes = freezes;
    }

    /**
     * Get the id of the movements' history.
     */
    public String getMovementsHistoryId() {
        return movementsHistoryId;
    }

    /**
     * Get the username of the user owning this movement history.
     */
    public String getUsername() {
        return username;
    }

    /**
     * Get the actual budget of the user.
     */
    public Budget getBudget() {
        return budget;
    }

    /**
     * Get the list of all user transactions.
     */
    public List<Transaction> getTransactions() {
        return Collections.unmodifiableList(transactions);
    }

    /**
     * Get the list of all user freezes.
     */
    public List<Freezing> getFreezes() {
        return Collections.unmodifiableList(freezes);
    }

    /**
     * Freeze money for the current user. Furthermore, add the freezing to the history.
     * @param   freezing                        the freezing to perform
     * @throws  MovementAlreadyExistsException  if the freezing already exists
     * @throws  InsufficientBudgetException     if the user budget is insufficient
     */
    public void freezeMoney(final Freezing freezing)
            throws MovementAlreadyExistsException, InsufficientBudgetException {
        checkForDuplicateMovement(freezing, freezes);
        decrementBudget(freezing.getAmount().abs());
        freezes.add(freezing);
    }

    /**
     * Unfreeze a precedent money freezing for the auction.  Furthermore, remove the freezing from the history.
     * @param auctionId                         the id of the auction linked to the freezing
     * @throws  FreezingNotExistsException      if a freezing for such auction doesn't exist
     */
    public void unfreezeMoney(final String auctionId) throws FreezingNotExistsException {
        final Freezing freezingToRemove = freezes.stream()
                .filter(freezing -> freezing.getAuctionId().equals(auctionId))
                .findFirst().orElseThrow(() -> new FreezingNotExistsException(auctionId));
        incrementBudget(freezingToRemove.getAmount());
        freezes.remove(freezingToRemove);
    }

    /**
     * Recharge budget of the user. Furthermore, add the transaction to the history.
     * @param   transaction                     the transaction corresponding to recharge operation
     * @throws  MovementAlreadyExistsException  if the freezing already exists
     * @throws  InsufficientBudgetException     if the user budget is insufficient
     */
    public void rechargeBudget(final ChargingFromCardTransaction transaction)
            throws MovementAlreadyExistsException, InsufficientBudgetException {
        checkForDuplicateMovement(transaction, transactions);
        incrementBudget(transaction.getAmount());
        transactions.add(transaction);
    }

    /**
     * Withdraw money from the user budget. Furthermore, add the transaction to the history.
     * @param   transaction                     the transaction corresponding to withdrawal operation
     * @throws  MovementAlreadyExistsException  if the freezing already exists
     * @throws  InsufficientBudgetException     if the user budget is insufficient
     */
    public void withdrawBudget(final WithdrawalTransaction transaction)
            throws MovementAlreadyExistsException, InsufficientBudgetException {
        checkForDuplicateMovement(transaction, transactions);
        decrementBudget(transaction.getAmount().abs());
        transactions.add(transaction);
    }

    /**
     * Update user budget after an awarded auction. Furthermore, add the transaction to the history.
     * In the case of the buyer user, before adding the awarded auction transaction,
     * it removes the previous freezing relating to the auction in question.
     * @param   transaction                     the transaction corresponding to the awarded auction
     * @throws  FreezingNotExistsException      if no freezing exists for the winning bid of the buyer user
     * @throws  MovementAlreadyExistsException  if the freezing already exists
     * @throws  InsufficientBudgetException     if the user budget is insufficient
     */
    public void updateBudgetAfterAwardedAuction(final AwardedAuctionTransaction transaction)
            throws FreezingNotExistsException, MovementAlreadyExistsException, InsufficientBudgetException {
        checkForDuplicateMovement(transaction, transactions);
        if (transaction.getBuyer().equals(username)) {
            unfreezeMoney(transaction.getAuctionId());
            decrementBudget(transaction.getAmount().abs());
        } else {
            incrementBudget(transaction.getAmount());
        }
        transactions.add(transaction);
    }

    private void incrementBudget(final BigDecimal amount) {
        budget = new Budget(budget.value().add(amount));
    }

    private void decrementBudget(final BigDecimal amount) {
        if (amount.compareTo(budget.value()) > 0) {
            throw new InsufficientBudgetException(budget, amount);
        }
        incrementBudget(amount.negate());
    }

    private void checkForDuplicateMovement(final Movement movement, final List<? extends Movement> movements) {
        if (movements.stream().anyMatch(m -> m.getMovementId().equals(movement.getMovementId()))) {
            throw new MovementAlreadyExistsException(movement.getMovementId());
        }
    }

    public static MovementsHistoryBuilder builder() {
        return new MovementsHistoryBuilder();
    }

    /**
     * {@inheritDoc}
     */
    @Override @Generated
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final MovementsHistory that = (MovementsHistory) o;
        return movementsHistoryId.equals(that.movementsHistoryId) && username.equals(that.username)
                && transactions.equals(that.transactions) && freezes.equals(that.freezes);
    }

    /**
     * {@inheritDoc}
     */
    @Override  @Generated
    public int hashCode() {
        return Objects.hash(movementsHistoryId, username, transactions, freezes);
    }

    /**
     * {@inheritDoc}
     */
    @Override  @Generated
    public String toString() {
        return "MovementsHistory{"
                + "movementsHistoryId='" + movementsHistoryId + "'"
                + ", username='" + username + "'"
                + ", budget=" + budget.value()
                + ", transactions=" + transactions
                + ", freezes=" + freezes
                + "}";
    }

    @SuppressWarnings("PMD.AvoidFieldNameMatchingMethodName")
    public static final class MovementsHistoryBuilder {
        private String movementsHistoryId;
        private String username;
        private Budget budget;
        private List<Transaction> transactions;
        private List<Freezing> freezes;

        private MovementsHistoryBuilder() { // by default empty list and zero budget
            this.budget = new Budget();
            this.freezes = new ArrayList<>();
            this.transactions = new ArrayList<>();
        }

        public MovementsHistoryBuilder movementsHistoryId(final String movementsHistoryId) {
            this.movementsHistoryId = movementsHistoryId;
            return this;
        }

        public MovementsHistoryBuilder username(final String username) {
            this.username = username;
            return this;
        }

        public MovementsHistoryBuilder budget(final Budget budget) {
            this.budget = budget;
            return this;
        }

        public MovementsHistoryBuilder transactions(final List<Transaction> transactions) {
            this.transactions = new ArrayList<>(transactions);
            return this;
        }

        public MovementsHistoryBuilder freezes(final List<Freezing> freezes) {
            this.freezes = new ArrayList<>(freezes);
            return this;
        }

        public MovementsHistory build() {
            validate();
            return new MovementsHistory(movementsHistoryId, username, budget, transactions, freezes);
        }

        private void validate() {
            if (username == null || username.isEmpty()) {
                throw new MovementsHistoryValidationException("The username is null or empty");
            }
        }
    }
}
