/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.movement.domain.valueobject;

import it.unibo.luckyauction.movement.domain.entity.AwardedAuctionTransaction;
import lombok.Generated;

import java.util.Objects;

/**
 * A pair containing both seller and buyer awarded auction transactions.
 * @param sellerTransaction the seller awarded auction transaction
 * @param buyerTransaction  the buyer awarded auction transaction
 */
public record AwardedAuctionTransactionsPair(AwardedAuctionTransaction sellerTransaction,
                                             AwardedAuctionTransaction buyerTransaction) {

    /**
     * {@inheritDoc}
     */
    @Override @Generated
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final AwardedAuctionTransactionsPair that = (AwardedAuctionTransactionsPair) o;
        return sellerTransaction.equals(that.sellerTransaction) && buyerTransaction.equals(that.buyerTransaction);
    }

    /**
     * {@inheritDoc}
     */
    @Override @Generated
    public int hashCode() {
        return Objects.hash(sellerTransaction, buyerTransaction);
    }
}
