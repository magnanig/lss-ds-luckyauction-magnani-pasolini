/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.movement.domain.entity;

import it.unibo.luckyauction.movement.domain.exception.MovementValidationException;
import it.unibo.luckyauction.movement.domain.valueobject.UsersPairTransactionType;
import lombok.Generated;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Objects;

/**
 * A transaction involving a pair of users, i.e. a seller and a buyer.
 */
public abstract class UsersPairTransaction extends Transaction {

    private final String buyerUsername;       // from
    private final String sellerUsername;      // to

    /**
     * Create a new users pair transaction with the specified arguments.
     * @param transactionId     the id of the transaction
     * @param timestamp         the timestamp for the transaction
     * @param amount            the money amount exchanged
     * @param buyerUsername     the buyer (who sends money)
     * @param sellerUsername    the seller (who receives money)
     * @param type              the users pair transaction type (see {@link UsersPairTransactionType})
     */
    protected UsersPairTransaction(final String transactionId, final Calendar timestamp, final BigDecimal amount,
                                   final String buyerUsername, final String sellerUsername,
                                   final UsersPairTransactionType type) {
        super(transactionId, timestamp, amount, type);
        this.buyerUsername = buyerUsername;
        this.sellerUsername = sellerUsername;
    }

    /**
     * Get the buyer (i.e. who sends money).
     * @return  the user buyer
     */
    public String getBuyer() {
        return buyerUsername;
    }

    /**
     * Get the seller (i.e. who receives money).
     * @return  the user seller
     */
    public String getSeller() {
        return sellerUsername;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UsersPairTransactionType getType() {
        return (UsersPairTransactionType) super.getType();
    }

    /**
     * {@inheritDoc}
     */
    @Override @Generated
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        final UsersPairTransaction that = (UsersPairTransaction) o;
        return buyerUsername.equals(that.buyerUsername) && sellerUsername.equals(that.sellerUsername);
    }

    /**
     * {@inheritDoc}
     */
    @Override @Generated
    public int hashCode() {
        return Objects.hash(super.hashCode(), buyerUsername, sellerUsername);
    }

    @SuppressWarnings({"checkstyle:all", "PMD.AvoidFieldNameMatchingMethodName", "unchecked"})
    public abstract static class Builder<T extends Builder<T, R>, R extends UsersPairTransaction>
            extends Transaction.Builder<T, R> {

        protected String buyerUsername;   // from
        protected String sellerUsername;  // to

        public T buyerUsername(final String buyerUsername) {
            this.buyerUsername = buyerUsername;
            return (T) this;
        }

        public T sellerUsername(final String sellerUsername) {
            this.sellerUsername = sellerUsername;
            return (T) this;
        }

        @Override
        protected void validate() throws MovementValidationException {
            super.validate();
            if (buyerUsername == null || buyerUsername.isEmpty()) {
                throw new MovementValidationException("Buyer username is not set in AwardedAuctionTransaction object");
            }
            if (sellerUsername == null || sellerUsername.isEmpty()) {
                throw new MovementValidationException("Seller username is not set in AwardedAuctionTransaction object");
            }
            if (buyerUsername.equals(sellerUsername)) {
                throw new MovementValidationException("Buyer username and seller username can't be the same");
            }
        }
    }
}
