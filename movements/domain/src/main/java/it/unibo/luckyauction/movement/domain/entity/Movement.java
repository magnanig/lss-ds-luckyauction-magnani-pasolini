/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.movement.domain.entity;

import it.unibo.luckyauction.movement.domain.exception.MovementValidationException;
import it.unibo.luckyauction.movement.domain.valueobject.MovementType;
import it.unibo.luckyauction.util.CalendarUtils;
import lombok.Generated;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Objects;

/**
 * A generic money movement.
 */
@SuppressWarnings("PMD.AbstractClassWithoutAbstractMethod")
public abstract class Movement {

    private final String movementId;
    private final Calendar timestamp;
    private final BigDecimal amount;
    private final MovementType type;

    /**
     * Create a new movement with the specified arguments.
     * @param timestamp     the timestamp for the movement
     * @param amount        the money amount
     * @param type          the movement type (see {@link MovementType})
     */
    protected Movement(final String movementId, final Calendar timestamp, final BigDecimal amount,
                       final MovementType type) {
        this.movementId = movementId;
        this.timestamp = timestamp;
        this.amount = amount;
        this.type = type;
    }

    /**
     * Get the movement id.
     * @return  the movement id
     */
    public String getMovementId() {
        return movementId;
    }

    /**
     * Get the movement timestamp.
     * @return  the movement timestamp
     */
    public Calendar getTimestamp() {
        return CalendarUtils.copy(timestamp);
    }

    /**
     * Get the money amount of the movement.
     * @return  the money amount
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * Get the movement type.
     * @return  the movement type
     */
    public MovementType getType() {
        return type;
    }

    /**
     * {@inheritDoc}
     */
    @Override @Generated
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final Movement movement = (Movement) o;
        return movementId.equals(movement.movementId) && timestamp.equals(movement.timestamp)
                && amount.equals(movement.amount) && type.equals(movement.type);
    }

    /**
     * {@inheritDoc}
     */
    @Override @Generated
    public int hashCode() {
        return Objects.hash(movementId, timestamp, amount, type);
    }

    /**
     * {@inheritDoc}
     */
    @Override @Generated
    public String toString() {
        return "Movement{"
                + "movementId='" + movementId + "'"
                + ", timestamp=" + timestamp.getTime()
                + ", amount=" + amount
                + ", type=" + type
                + "}";
    }

    @SuppressWarnings({"checkstyle:all", "PMD.AvoidFieldNameMatchingMethodName", "unchecked"})
    public abstract static class Builder<T extends Builder<T, R>, R extends Movement> {
        protected String movementId;
        protected Calendar timestamp;
        protected BigDecimal amount;

        public Builder() {
            timestamp = Calendar.getInstance(); // by default, use current time
        }

        protected T movementId(final String movementId) {
            this.movementId = movementId;
            return (T) this;
        }

        public T timestamp(final Calendar timestamp) {
            this.timestamp = CalendarUtils.copy(timestamp);
            return (T) this;
        }

        public T amount(final BigDecimal amount) {
            this.amount = amount;
            return (T) this;
        }

        /**
         * Check whether object is ready to be built, i.e. if all non-null fields
         * are set with valid values. If you need to override this method, remember
         * to call super (either at the beginning or at the end).
         *
         * @throws MovementValidationException  if one or more fields are not valid
         */
        protected void validate() throws MovementValidationException {
            if (amount == null || amount.compareTo(BigDecimal.ZERO) == 0) {
                throw new MovementValidationException("The movement amount cannot be null and cannot be zero");
            }
            final int maxDecimals = 2;
            if (amount.scale() > maxDecimals) {
                throw new MovementValidationException("The movement amount can't have more than 2 decimals");
            }
            if (timestamp.after(Calendar.getInstance())) {
                throw new MovementValidationException("The movement timestamp cannot be a future date");
            }
        }

        public final R build() {
            validate();
            return createObject();
        }

        protected abstract R createObject();
    }
}
