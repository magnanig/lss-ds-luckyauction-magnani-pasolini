/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.movement.domain.entity;

import it.unibo.luckyauction.movement.domain.exception.MovementValidationException;
import it.unibo.luckyauction.movement.domain.valueobject.UsersPairTransactionType;
import lombok.Generated;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Objects;

/**
 * A transaction for an awarded auction, with the reference to the auction creator
 * (seller) and the winner (buyer).
 */
public final class AwardedAuctionTransaction extends UsersPairTransaction {

    private final String auctionId;
    private final String productName;

    /**
     * Create a new transaction for an awarded auction.
     * @param timestamp         the timestamp for the transaction
     * @param amount            the final auction price
     * @param sellerUsername    the auction creator
     * @param buyerUsername     the auction winner
     * @param auctionId         the awarded auction id
     */
    private AwardedAuctionTransaction(final String transactionId, final Calendar timestamp, final BigDecimal amount,
                                      final String buyerUsername, final String sellerUsername, final String auctionId,
                                      final String productName) {
        super(transactionId, timestamp, amount, buyerUsername, sellerUsername, UsersPairTransactionType.AWARDED_AUCTION
        );
        this.auctionId = auctionId;
        this.productName = productName;
    }

    /**
     * Get the buyer of the auction (i.e. who sends money).
     * @return  the username of the user buyer of the auction
     */
    @Override // only to redefine javadoc properly
    public String getBuyer() {
        return super.getBuyer();
    }

    /**
     * Get the seller of the auction (i.e. who receives money).
     * @return  the username of the user seller of the auction
     */
    @Override // only to redefine javadoc properly
    public String getSeller() {
        return super.getSeller();
    }

    /**
     * Get the awarded auction reference.
     * @return  the awarded auction
     */
    public String getAuctionId() {
        return auctionId;
    }

    public String getProductName() {
        return productName;
    }

    @Override @Generated
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        final AwardedAuctionTransaction that = (AwardedAuctionTransaction) o;
        return auctionId.equals(that.auctionId);
    }

    @Override @Generated
    public int hashCode() {
        return Objects.hash(super.hashCode(), auctionId);
    }

    public static AwardedAuctionTransactionBuilder builder() {
        return new AwardedAuctionTransactionBuilder();
    }

    @SuppressWarnings({"checkstyle:all", "PMD.AvoidFieldNameMatchingMethodName"})
    public static class AwardedAuctionTransactionBuilder
            extends UsersPairTransaction.Builder<AwardedAuctionTransactionBuilder, AwardedAuctionTransaction> {

        protected String auctionId;
        protected String productName;

        public AwardedAuctionTransactionBuilder auctionId(final String auctionId) {
            this.auctionId = auctionId;
            return this;
        }

        public AwardedAuctionTransactionBuilder productName(final String productName) {
            this.productName = productName;
            return this;
        }

        @Override
        protected AwardedAuctionTransaction createObject() {
            return new AwardedAuctionTransaction(movementId, timestamp, amount, buyerUsername, sellerUsername,
                    auctionId, productName);
        }

        @Override
        protected void validate() throws MovementValidationException {
            super.validate();
            if (auctionId == null || auctionId.isEmpty()) {
                throw new MovementValidationException("Auction ID is not set in AwardedAuctionTransaction object");
            }
            if (productName == null || productName.isEmpty()) {
                throw new MovementValidationException("Product name is not set in AwardedAuctionTransaction object");
            }
        }
    }

}
