/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.movement.domain.entity;

import it.unibo.luckyauction.movement.domain.exception.MovementValidationException;
import it.unibo.luckyauction.movement.domain.valueobject.FreezingType;

import java.math.BigDecimal;
import java.util.Calendar;

/**
 * Represents a temporary lock of money, waiting for auction result.
 */
public final class Freezing extends Movement {

    private final String auctionId;
    private final String productName;

    /**
     * Create a new freezing for the specified auction.
     * @param timestamp     the timestamp at which freezing occurs
     * @param amount        the amount of money to be frozen
     * @param auctionId     the auction to which freezing is associated
     * @param productName   the name of the product to which freezing is associated
     */
    private Freezing(final String freezingId, final Calendar timestamp, final BigDecimal amount,
                     final String auctionId, final String productName) {
        super(freezingId, timestamp, amount, FreezingType.FREEZING);
        this.auctionId = auctionId;
        this.productName = productName;
    }

    /**
     * Get the timestamp at which freezing occurs.
     * @return  the timestamp at which freezing occurs
     */
    @Override // only to redefine javadoc properly
    public Calendar getTimestamp() {
        return super.getTimestamp();
    }

    /**
     * Get the amount of money frozen.
     * @return  the amount of money frozen
     */
    @Override // only to redefine javadoc properly
    public BigDecimal getAmount() {
        return super.getAmount();
    }

    /**
     * Get the auction to which the freezing is associated.
     * @return  the auction to which the freezing is associated
     */
    public String getAuctionId() {
        return auctionId;
    }

    /**
     * Get the name of the product to which freezing is associated.
     * @return  the product name
     */
    public String getProductName() {
        return productName;
    }

    /**
     * Get the builder of freezing objects.
     * @return  the freezing builder instance
     */
    public static FreezingBuilder builder() {
        return new FreezingBuilder();
    }

    @SuppressWarnings({"checkstyle:all", "PMD.AvoidFieldNameMatchingMethodName"})
    public static class FreezingBuilder extends Movement.Builder<FreezingBuilder, Freezing> {

        protected String auctionId;
        protected String productName;

        public FreezingBuilder freezingId(final String freezingId) {
            return movementId(freezingId);
        }

        public FreezingBuilder auctionId(final String auctionId) {
            this.auctionId = auctionId;
            return this;
        }

        public FreezingBuilder productName(final String productName) {
            this.productName = productName;
            return this;
        }

        @Override
        protected Freezing createObject() {
            return new Freezing(movementId, timestamp, amount, auctionId, productName);
        }

        @Override
        protected void validate() {
            super.validate();
            if (auctionId == null || auctionId.isEmpty()) {
                throw new MovementValidationException("Auction id is not set");
            }
            if (productName == null || productName.isEmpty()) {
                throw new MovementValidationException("Product name is not set");
            }
            if (amount.compareTo(BigDecimal.ZERO) <= 0) {
                throw new MovementValidationException("Freezing amount must be greater than 0");
            }
        }
    }

}
