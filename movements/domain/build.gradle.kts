plugins {
    // this will add following dependencies: testFixtures --> main and test --> testFixtures
    id("java-test-fixtures")
}

dependencies {
    implementation(project(":utils"))
}