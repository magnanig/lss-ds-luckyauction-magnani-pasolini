/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.movement.controller.config;

import it.unibo.luckyauction.movement.controller.MovementController;
import it.unibo.luckyauction.movement.repository.MovementMongoRepository;
import it.unibo.luckyauction.movement.usecase.CreateMovementsHistory;
import it.unibo.luckyauction.movement.usecase.DeleteMovementsHistory;
import it.unibo.luckyauction.movement.usecase.FindMovement;
import it.unibo.luckyauction.movement.usecase.FindMovementsHistory;
import it.unibo.luckyauction.movement.usecase.ManageMovement;
import it.unibo.luckyauction.movement.usecase.port.MovementRepository;
import it.unibo.luckyauction.util.port.IdGenerator;
import it.unibo.luckyauction.uuid.UUIdGenerator;

import static it.unibo.luckyauction.configuration.ConfigurationConstants.TEST_MODE;

public class MovementConfig {
    private final MovementRepository movementRepository;
    private final IdGenerator movementIdGenerator = new UUIdGenerator();

    public MovementConfig() {
        this.movementRepository = TEST_MODE
                ? MovementMongoRepository.testRepository()
                : MovementMongoRepository.defaultRepository();
    }

    /**
     * Build the movement controller of the application, capable of orchestrating movements.
     * See {@link MovementController} for more details.
     * @return      the just built movement controller
     */
    public MovementController movementController() {
        return new MovementController(createMovementsHistory(), deleteMovementsHistory(), findMovement(),
                findMovementsHistory(), manageMovement());
    }

    private CreateMovementsHistory createMovementsHistory() {
        return new CreateMovementsHistory(movementRepository);
    }

    private DeleteMovementsHistory deleteMovementsHistory() {
        return new DeleteMovementsHistory(movementRepository);
    }

    private FindMovement findMovement() {
        return new FindMovement(movementRepository);
    }

    private FindMovementsHistory findMovementsHistory() {
        return new FindMovementsHistory(movementRepository);
    }

    private ManageMovement manageMovement() {
        return new ManageMovement(movementRepository, movementIdGenerator);
    }
}
