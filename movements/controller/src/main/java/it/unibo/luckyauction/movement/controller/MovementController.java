/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.movement.controller;

import it.unibo.luckyauction.movement.domain.entity.ChargingFromCardTransaction;
import it.unibo.luckyauction.movement.domain.entity.Freezing;
import it.unibo.luckyauction.movement.domain.entity.Movement;
import it.unibo.luckyauction.movement.domain.entity.MovementsHistory;
import it.unibo.luckyauction.movement.domain.entity.Transaction;
import it.unibo.luckyauction.movement.domain.entity.WithdrawalTransaction;
import it.unibo.luckyauction.movement.domain.exception.FreezingNotExistsException;
import it.unibo.luckyauction.movement.domain.exception.InsufficientBudgetException;
import it.unibo.luckyauction.movement.domain.valueobject.Budget;
import it.unibo.luckyauction.movement.usecase.exception.FreezingForAuctionAlreadyExistsException;
import it.unibo.luckyauction.movement.usecase.CreateMovementsHistory;
import it.unibo.luckyauction.movement.usecase.DeleteMovementsHistory;
import it.unibo.luckyauction.movement.usecase.FindMovement;
import it.unibo.luckyauction.movement.usecase.FindMovementsHistory;
import it.unibo.luckyauction.movement.usecase.ManageMovement;
import it.unibo.luckyauction.movement.usecase.exception.MovementsHistoryAlreadyExistsException;
import it.unibo.luckyauction.movement.usecase.exception.MovementsHistoryNotFoundException;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

/**
 * The movement-controller of the application, capable of handle all requests
 * involving movements, i.e. transactions, freezes and budget.
 */
public class MovementController {

    private final CreateMovementsHistory createMovementsHistory;
    private final DeleteMovementsHistory deleteMovementsHistory;
    private final FindMovement findMovement;
    private final FindMovementsHistory findMovementsHistory;
    private final ManageMovement manageMovement;

    /**
     * Create a new movement controller.
     * @param createMovementsHistory    the use case to create a new MovementsHistory
     * @param deleteMovementsHistory    the use case to delete a MovementsHistory
     * @param findMovement              the use case to find movements
     * @param findMovementsHistory      the use case to find a MovementsHistory
     * @param manageMovement            the use case to manage the movements operations
     */
    public MovementController(final CreateMovementsHistory createMovementsHistory,
                              final DeleteMovementsHistory deleteMovementsHistory,
                              final FindMovement findMovement, final FindMovementsHistory findMovementsHistory,
                              final ManageMovement manageMovement) {
        this.createMovementsHistory = createMovementsHistory;
        this.deleteMovementsHistory = deleteMovementsHistory;
        this.findMovement = findMovement;
        this.findMovementsHistory = findMovementsHistory;
        this.manageMovement = manageMovement;
    }

    /**
     * Create a new MovementsHistory in the repository, with € 0.00 default budget.
     * @param movementsHistoryId    the new MovementsHistory id
     * @param username              the username associated to such history
     * @throws MovementsHistoryAlreadyExistsException   if a history for the specified
     *                                                  username already exists
     */
    public void createNewMovementsHistory(final String movementsHistoryId, final String username)
            throws MovementsHistoryAlreadyExistsException {
        createMovementsHistory.create(movementsHistoryId, username);
    }

    /**
     * Delete a MovementsHistory by the associated username.
     * @param username  the desired username
     * @return          the just deleted MovementsHistory, if existed
     */
    public Optional<MovementsHistory> deleteMovementsHistoryByUsername(final String username) {
        return deleteMovementsHistory.deleteByUsername(username);
    }

    /**
     * Check if the user has sufficient budget.
     * @param username          the user's username
     * @param sufficientBudget  the amount you want to check
     * @return                  true if the user has sufficient budget, false otherwise
     */
    public Boolean userHasSufficientBudget(final String username, final BigDecimal sufficientBudget)
            throws MovementsHistoryNotFoundException {
        return manageMovement.userHasSufficientBudget(username, sufficientBudget);
    }

    /**
     * Find a MovementsHistory by the associated username.
     * @param username      the username for which looking for their movements history
     * @return              the corresponding MovementsHistory, if exists
     */
    public Optional<MovementsHistory> findMovementsHistoryByUsername(final String username) {
        return findMovementsHistory.findByUsername(username);
    }

    /**
     * Find a movement by its id and the associated user.
     * @param movementId    the desired movement id
     * @param username      the username associated to such movement
     * @return              the corresponding movement, if exists
     */
    public Optional<Movement> findMovementById(final String movementId, final String username) {
        return findMovement.findMovementById(movementId, username);
    }

    /**
     * Get the budget of the specified user.
     * @param username                              the desired username
     * @return                                      the user budget
     * @throws MovementsHistoryNotFoundException    if there is no MovementsHistory for the user
     */
    public Budget getUserBudget(final String username) {
        return findMovementsHistory.getUserBudget(username);
    }

    /**
     * Get all freezes associated to the desired user.
     * @param username      the desired username
     * @return              the list of freezing for the specified username
     */
    public List<Freezing> getFreezes(final String username) {
        return findMovement.getFreezes(username);
    }

    /**
     * Get all transactions associated to the desired user.
     * @param username      the desired username
     * @return              the list of transactions for the specified username
     */
    public List<Transaction> getTransactions(final String username) {
        return findMovement.getTransactions(username);
    }

    /**
     * Freeze part of user money after they raised a new bid. The money amount
     * to be frozen is equals to the bid amount.
     * @param bidderUsername    the username of the user who raised
     * @param raisedAmount      the raised amount
     * @param auctionId         the auction id for which bid is referred
     * @param productName       the name of the product of the auction in question
     * @return                  the just created freezing
     * @throws FreezingForAuctionAlreadyExistsException if a freezing for the specified auction already
     *                                                  exists for the bidder user
     * @throws MovementsHistoryNotFoundException        if there is no MovementsHistory for the user
     * @throws InsufficientBudgetException              if the user has insufficient money in their budget
     */
    public Freezing freezeMoney(final String bidderUsername, final BigDecimal raisedAmount, final String auctionId,
                                final String productName) throws FreezingForAuctionAlreadyExistsException,
            MovementsHistoryNotFoundException, InsufficientBudgetException {
        return manageMovement.freezeMoney(bidderUsername, raisedAmount, auctionId, productName);
    }

    /**
     * Unfreeze money for the specified user and auction, after a new bid raise.
     * @param username                      the username for which remove the previous money freezing
     * @param auctionId                     the id of the auction linked to the freezing
     * @return                              the just removed freezing
     * @throws FreezingNotExistsException   if no freezing exists for current username and auction
     */
    public Freezing unfreezeMoneyAfterBidRaise(final String username, final String auctionId)
            throws FreezingNotExistsException {
        return manageMovement.unfreezeMoney(username, auctionId);
    }

    /**
     * Recharge budget of the user.
     *
     * @param amount      the (positive) amount of the recharge
     * @param username    the username associated to the transaction
     * @param description the description associated with the transaction
     *                    (can be null)
     * @return the just created charging from card transaction
     * @throws MovementsHistoryNotFoundException if there is no MovementsHistory for the user
     */
    public ChargingFromCardTransaction rechargeBudget(final BigDecimal amount, final String username,
                                                      final String description) throws MovementsHistoryNotFoundException {
        return manageMovement.rechargeBudget(amount, description, username);
    }

    /**
     * Withdraw money from the user budget.
     * @param amount    the (positive) amount to be withdrawn
     * @param username  the username associated to the transaction
     * @return          the just created withdrawal transaction
     * @throws MovementsHistoryNotFoundException    if there is no MovementsHistory for the user
     * @throws InsufficientBudgetException          if the user has insufficient money in their budget
     */
    public WithdrawalTransaction withdrawBudget(final BigDecimal amount, final String username)
            throws MovementsHistoryNotFoundException, InsufficientBudgetException {
        return manageMovement.withdrawBudget(amount, username);
    }

    /**
     * Exchange money from buyer to auction seller, after the auction closes.
     * Create the two transactions for an awarded auction, for both seller and buyer.
     * The money amount for seller will be positive, while for buyer negative.
     * @param buyerUsername     the buyer username
     * @param sellerUsername    the seller username
     * @param amount            the final amount of the awarded auction
     * @param auctionId         the id of awarded auction
     * @param productName       the name of the product of the auction in question
     * @throws FreezingNotExistsException   if no freezing exists for the winning bid of the buyer user
     */
    public void exchangeMoneyAfterAwardedAuction(final String buyerUsername, final String sellerUsername,
                                                 final BigDecimal amount, final String auctionId,
                                                 final String productName) throws FreezingNotExistsException {
        manageMovement.exchangeMoneyAfterAwardedAuction(buyerUsername, sellerUsername, amount, auctionId, productName);
    }
}
