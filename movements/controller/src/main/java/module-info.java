module lss.ds.luckyauction.magnani.pasolini.movements.controller.main {
    requires lss.ds.luckyauction.magnani.pasolini.configuration.main;
    exports it.unibo.luckyauction.movement.controller;
    exports it.unibo.luckyauction.movement.controller.config;
    requires lss.ds.luckyauction.magnani.pasolini.movements.usecase.main;
    requires lss.ds.luckyauction.magnani.pasolini.movements.domain.main;
    requires lss.ds.luckyauction.magnani.pasolini.movements.repository.mongo.main;
    requires lss.ds.luckyauction.magnani.pasolini.uuid.generator.main;
    requires lss.ds.luckyauction.magnani.pasolini.utils.main;
}