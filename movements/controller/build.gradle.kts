dependencies {
    implementation(project(":configuration"))
    implementation(project(":utils"))
    implementation(project(":uuid-generator"))

    implementation(project(":movements:domain"))
    implementation(project(":movements:usecase"))
    implementation(project(":movements:adapter"))
    implementation(project(":movements:mongo-repository"))

    // this will add following dependencies: test --> movements.domain.testFixtures
    testImplementation(testFixtures(project(":movements:domain")))
}