dependencies {
    implementation(project(":mongo-utils"))
    implementation(project(":movements:domain"))
    implementation(project(":movements:usecase"))
    implementation(project(":movements:adapter"))

    implementation("org.mongodb:mongo-java-driver:3.12.10")
}