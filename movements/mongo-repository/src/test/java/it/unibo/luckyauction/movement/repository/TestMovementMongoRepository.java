/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.movement.repository;

import it.unibo.luckyauction.movement.domain.entity.AwardedAuctionTransaction;
import it.unibo.luckyauction.movement.domain.entity.ChargingFromCardTransaction;
import it.unibo.luckyauction.movement.domain.entity.Freezing;
import it.unibo.luckyauction.movement.domain.entity.MovementsHistory;
import it.unibo.luckyauction.movement.domain.entity.WithdrawalTransaction;
import it.unibo.luckyauction.movement.domain.valueobject.AwardedAuctionTransactionsPair;
import it.unibo.luckyauction.movement.domain.valueobject.Budget;
import it.unibo.luckyauction.movement.usecase.port.MovementRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TestMovementMongoRepository {

    private static final String TEST_USERNAME = "test";
    private static final String TEST_SECOND_USERNAME = "test2";
    private final MovementRepository movementRepository = MovementMongoRepository.testRepository();
    private final MovementsHistory movementsHistory = MovementsHistory.builder().movementsHistoryId("1")
            .username(TEST_USERNAME).build();

    private final ChargingFromCardTransaction chargingFromCardTransaction = ChargingFromCardTransaction.builder()
            .transactionId("1")
            .amount(new BigDecimal("100.00"))
            .build();

    private final WithdrawalTransaction withdrawalTransaction = WithdrawalTransaction.builder()
            .transactionId("2")
            .amount(new BigDecimal("50.00"))
            .build();

    private final Freezing freezing = Freezing.builder()
            .freezingId("4")
            .amount(new BigDecimal("50.00"))
            .auctionId("0")
            .productName("iPhone")
            .build();


    @Test
    @BeforeEach
    void testAndMovementsHistoryCreation() {
        if (movementRepository.findMovementsHistoryByUsername(TEST_USERNAME).isPresent()) {
            movementRepository.deleteMovementsHistoryByUsername(TEST_USERNAME);
        }
        assertEquals(Optional.empty(), movementRepository.findMovementsHistoryByUsername(TEST_USERNAME));
        movementRepository.createMovementsHistory(movementsHistory);
        assertEquals(Optional.of(movementsHistory), movementRepository.findMovementsHistoryByUsername(TEST_USERNAME));
    }

    @Test
    @AfterEach
    void testAndMovementsHistoryDeletion() {
        movementRepository.deleteMovementsHistoryByUsername(TEST_USERNAME);
        assertEquals(Optional.empty(), movementRepository.findMovementsHistoryByUsername(TEST_USERNAME));
    }

    @Test
    void testRechargeTransactionAdd() {
        assertEquals(Collections.emptyList(), movementRepository.getTransactions(TEST_USERNAME));
        movementRepository.rechargeBudget(chargingFromCardTransaction, TEST_USERNAME);
        assertEquals(Optional.of(new Budget(chargingFromCardTransaction.getAmount())),
                movementRepository.getUserBudget(TEST_USERNAME));
        assertEquals(Optional.of(chargingFromCardTransaction),
                movementRepository.findMovementById(chargingFromCardTransaction.getMovementId(), TEST_USERNAME));
        assertEquals(Optional.of(chargingFromCardTransaction),
                movementRepository.findMovementById(chargingFromCardTransaction.getMovementId()));
        assertEquals(List.of(chargingFromCardTransaction), movementRepository.getTransactions(TEST_USERNAME));
    }

    @Test
    void testWithdrawalTransactionAdd() {
        assertEquals(Collections.emptyList(), movementRepository.getTransactions(TEST_USERNAME));
        movementRepository.rechargeBudget(chargingFromCardTransaction, TEST_USERNAME);
        movementRepository.withdrawBudget(withdrawalTransaction, TEST_USERNAME);
        assertEquals(Optional.of(
                new Budget(chargingFromCardTransaction.getAmount().add(withdrawalTransaction.getAmount()))),
                movementRepository.getUserBudget(TEST_USERNAME));
        assertEquals(Optional.of(withdrawalTransaction),
                movementRepository.findMovementById(withdrawalTransaction.getMovementId(), TEST_USERNAME));
        assertEquals(List.of(chargingFromCardTransaction, withdrawalTransaction),
                movementRepository.getTransactions(TEST_USERNAME));
    }

    @Test
    void testFreezingAdd() {
        assertEquals(Collections.emptyList(), movementRepository.getFreezes(TEST_USERNAME));
        movementRepository.rechargeBudget(chargingFromCardTransaction, TEST_USERNAME);
        movementRepository.freezeMoney(freezing, TEST_USERNAME);
        assertEquals(Optional.of(
                        new Budget(chargingFromCardTransaction.getAmount().subtract(freezing.getAmount()))),
                movementRepository.getUserBudget(TEST_USERNAME));
        assertEquals(Optional.of(freezing), movementRepository.findMovementById(freezing.getMovementId(), TEST_USERNAME));
        assertEquals(List.of(freezing), movementRepository.getFreezes(TEST_USERNAME));
    }

    @Test
    void testFreezingDeletion() {
        movementRepository.rechargeBudget(chargingFromCardTransaction, TEST_USERNAME);
        movementRepository.freezeMoney(freezing, TEST_USERNAME);
        assertEquals(Optional.of(
                new Budget(chargingFromCardTransaction.getAmount().subtract(freezing.getAmount()))),
                movementRepository.getUserBudget(TEST_USERNAME));
        movementRepository.unfreezeMoney(TEST_USERNAME, freezing.getAuctionId());
        assertEquals(Optional.of(new Budget(chargingFromCardTransaction.getAmount())),
                movementRepository.getUserBudget(TEST_USERNAME));
        assertEquals(Optional.empty(), movementRepository.findMovementById(freezing.getMovementId(), TEST_USERNAME));
        assertEquals(Collections.emptyList(), movementRepository.getFreezes(TEST_USERNAME));
    }

    @Test
    void testExchangeMoney() {
        if (movementRepository.findMovementsHistoryByUsername(TEST_SECOND_USERNAME).isPresent()) {
            movementRepository.deleteMovementsHistoryByUsername(TEST_SECOND_USERNAME);
        }
        movementRepository.createMovementsHistory(MovementsHistory.builder().movementsHistoryId("2")
                .username(TEST_SECOND_USERNAME).build());
        testRechargeTransactionAdd();
        final AwardedAuctionTransaction buyerTransaction = buildDefaultAwardedAuctionTransaction("AAT1",
                chargingFromCardTransaction.getAmount().negate());
        final AwardedAuctionTransaction sellerTransaction = buildDefaultAwardedAuctionTransaction("AAT2",
                chargingFromCardTransaction.getAmount());
        movementRepository.exchangeMoneyAfterAwardedAuction(new AwardedAuctionTransactionsPair(sellerTransaction,
                buyerTransaction));
        assertEquals(Optional.of(new Budget(chargingFromCardTransaction.getAmount())),
                movementRepository.getUserBudget(TEST_SECOND_USERNAME));
        assertEquals(Optional.of(new Budget()), movementRepository.getUserBudget(TEST_USERNAME));
        assertEquals(List.of(chargingFromCardTransaction, buyerTransaction),
                movementRepository.getTransactions(TEST_USERNAME));
        assertEquals(List.of(sellerTransaction), movementRepository.getTransactions(TEST_SECOND_USERNAME));
    }

    private AwardedAuctionTransaction buildDefaultAwardedAuctionTransaction(final String transactionId,
                                                                            final BigDecimal amount) {
        return AwardedAuctionTransaction.builder()
                .transactionId(transactionId)
                .amount(amount)
                .auctionId("0")
                .productName("iPhone")
                .buyerUsername(TEST_USERNAME)
                .sellerUsername(TEST_SECOND_USERNAME)
                .build();
    }
}
