module lss.ds.luckyauction.magnani.pasolini.movements.repository.mongo.main {
    exports it.unibo.luckyauction.movement.repository;
    requires lss.ds.luckyauction.magnani.pasolini.mongo.utils.main;
    requires lss.ds.luckyauction.magnani.pasolini.movements.domain.main;
    requires lss.ds.luckyauction.magnani.pasolini.movements.usecase.main;
    requires lss.ds.luckyauction.magnani.pasolini.movements.adapter.main;
    requires mongo.java.driver;
}