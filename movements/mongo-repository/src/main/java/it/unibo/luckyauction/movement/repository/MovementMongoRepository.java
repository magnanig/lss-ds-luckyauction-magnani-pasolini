/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.movement.repository;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Projections;
import com.mongodb.client.model.Updates;
import it.unibo.luckyauction.movement.adapter.AwardedAuctionTransactionWeb;
import it.unibo.luckyauction.movement.adapter.ChargingFromCardTransactionWeb;
import it.unibo.luckyauction.movement.adapter.FreezingWeb;
import it.unibo.luckyauction.movement.adapter.MovementTypeWeb;
import it.unibo.luckyauction.movement.adapter.MovementWeb;
import it.unibo.luckyauction.movement.adapter.MovementsHistoryWeb;
import it.unibo.luckyauction.movement.adapter.TransactionWeb;
import it.unibo.luckyauction.movement.adapter.WithdrawalTransactionWeb;
import it.unibo.luckyauction.movement.domain.entity.ChargingFromCardTransaction;
import it.unibo.luckyauction.movement.domain.entity.Freezing;
import it.unibo.luckyauction.movement.domain.entity.Movement;
import it.unibo.luckyauction.movement.domain.entity.MovementsHistory;
import it.unibo.luckyauction.movement.domain.entity.Transaction;
import it.unibo.luckyauction.movement.domain.entity.WithdrawalTransaction;
import it.unibo.luckyauction.movement.domain.exception.FreezingNotExistsException;
import it.unibo.luckyauction.movement.domain.valueobject.AwardedAuctionTransactionsPair;
import it.unibo.luckyauction.movement.domain.valueobject.Budget;
import it.unibo.luckyauction.movement.usecase.port.MovementRepository;
import it.unibo.luckyauction.repository.util.MongoConnectionsConstants;
import it.unibo.luckyauction.repository.util.CommonsMongoOperations;
import it.unibo.luckyauction.repository.util.MongoDatabaseManager;
import org.bson.BsonDocument;
import org.bson.BsonString;
import org.bson.Document;
import org.bson.codecs.pojo.ClassModel;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.bson.conversions.Bson;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

public final class MovementMongoRepository implements MovementRepository {

    private static final String TYPE = "type";
    private static final String USERNAME_FIELD = "username";
    private static final String BUDGET_VALUE_FIELD = "budget.value";
    private static final String TRANSACTIONS_FIELD = "transactions";

    private final MongoCollection<MovementsHistoryWeb> movementsCollection;
    private final CommonsMongoOperations<MovementsHistoryWeb, MovementsHistory> commons;

    private MovementMongoRepository(final String connectionString, final String databaseName,
                                    final String collectionName) {
        movementsCollection = new MongoDatabaseManager(connectionString, databaseName,
                PojoCodecProvider.builder() // here we need discriminators to discriminate between movements hierarchy
                        .register(ClassModel.builder(MovementWeb.class)
                                .discriminatorKey(TYPE).enableDiscriminator(true).build())
                        .register(ClassModel.builder(TransactionWeb.class)
                                .discriminatorKey(TYPE).enableDiscriminator(true).build())
                        .register(ClassModel.builder(FreezingWeb.class)
                                .discriminatorKey(TYPE).discriminator(MovementTypeWeb.FREEZING.toString())
                                .enableDiscriminator(true).build())
                        .register(ClassModel.builder(ChargingFromCardTransactionWeb.class)
                                .discriminatorKey(TYPE).discriminator(MovementTypeWeb.CHARGING_FROM_CARD.toString())
                                .enableDiscriminator(true).build())
                        .register(ClassModel.builder(WithdrawalTransactionWeb.class)
                                .discriminatorKey(TYPE).discriminator(MovementTypeWeb.WITHDRAWAL.toString())
                                .enableDiscriminator(true).build())
                        .register(ClassModel.builder(AwardedAuctionTransactionWeb.class)
                                .discriminatorKey(TYPE).discriminator(MovementTypeWeb.AWARDED_AUCTION.toString())
                                .enableDiscriminator(true).build())
                        .automatic(true)
                        .build()
        ).getCollection(collectionName, MovementsHistoryWeb.class);
        commons = new CommonsMongoOperations<>(movementsCollection, MovementsHistoryWeb::toDomainMovementsHistory);
    }

    public static MovementMongoRepository defaultRepository() {
        return new MovementMongoRepository(MongoConnectionsConstants.MOVEMENTS_CONNECTION_STRING, MongoConnectionsConstants.MOVEMENTS_DB_NAME, MongoConnectionsConstants.MOVEMENTS_COLLECTION_NAME);
    }

    public static MovementMongoRepository testRepository() {
        return new MovementMongoRepository(MongoConnectionsConstants.TEST_CONNECTION_STRING,
                MongoConnectionsConstants.TEST_DB_NAME, MongoConnectionsConstants.MOVEMENTS_COLLECTION_NAME);
    }

    @Override
    public void createMovementsHistory(final MovementsHistory movementsHistory) {
        movementsCollection.insertOne(MovementsHistoryWeb.fromDomainMovementsHistory(movementsHistory));
    }

    @Override
    public Optional<MovementsHistory> deleteMovementsHistoryByUsername(final String username) {
        return commons.deleteOneByFilter(Filters.eq(USERNAME_FIELD, username));
    }

    @Override
    public Optional<MovementsHistory> findMovementsHistoryByUsername(final String username) {
        return commons.findOneByFilter(Filters.eq(USERNAME_FIELD, username));
    }

    @Override
    public Optional<Movement> findMovementById(final String movementId, final String username) {
        final Bson movementFilter = Filters.or(
                Filters.eq("transactions.movementId", movementId),
                Filters.eq("freezes.movementId", movementId)
        );
        final Bson filter = username == null ? movementFilter
                : Filters.and(Filters.eq("username", username), movementFilter);
        return Optional.ofNullable(movementsCollection.find(filter).first())
                .map(MovementsHistoryWeb::toDomainMovementsHistory)
                .flatMap(history -> Stream.concat(history.getFreezes().stream(), history.getTransactions().stream())
                        .filter(movement -> movement.getMovementId().equals(movementId))
                        .findFirst());
    }

    @Override
    public Optional<Movement> findMovementById(final String movementId) {
        return findMovementById(movementId, null);
    }

    @Override
    public Optional<Budget> getUserBudget(final String username) {
        return Optional.ofNullable(movementsCollection.find(Filters.eq(USERNAME_FIELD, username), Document.class)
                .projection(Projections.fields(Projections.include("budget")))
                .map(document -> document.get("budget", Document.class).get("value"))
                .first())
                .map(value -> new Budget(new BigDecimal(value.toString())));
    }

    @Override
    public List<Freezing> getFreezes(final String username) {
        return commons.findOneByFilter(Filters.eq(USERNAME_FIELD, username))
                .map(MovementsHistory::getFreezes)
                .orElse(Collections.emptyList());
    }

    @Override
    public List<Transaction> getTransactions(final String username) {
        return commons.findOneByFilter(Filters.eq(USERNAME_FIELD, username))
                .map(MovementsHistory::getTransactions)
                .orElse(Collections.emptyList());
    }

    @Override
    public void freezeMoney(final Freezing freezing, final String username) {
        movementsCollection.findOneAndUpdate(Filters.eq(USERNAME_FIELD, username), Updates.combine(
                Updates.inc(BUDGET_VALUE_FIELD, freezing.getAmount().abs().negate()),
                Updates.push("freezes", FreezingWeb.fromDomainFreezing(freezing))
        ));
    }

    @Override
    public Freezing unfreezeMoney(final String username, final String auctionId) {
        final Freezing freezing = getFreezes(username).stream().filter(f -> f.getAuctionId().equals(auctionId))
                .findFirst()
                .orElseThrow(() -> new FreezingNotExistsException(auctionId));
        return Optional.ofNullable(movementsCollection.findOneAndUpdate(
                        Filters.and(
                                Filters.eq(USERNAME_FIELD, username),
                                Filters.eq("freezes.auctionId", auctionId)
                        ), Updates.combine(
                                Updates.pullByFilter(new BsonDocument("freezes",
                                        new BsonDocument("auctionId", new BsonString(auctionId)))),
                                Updates.inc(BUDGET_VALUE_FIELD, freezing.getAmount())
                        )))
                .flatMap(movementsHistory -> getFreezingByAuctionId(movementsHistory, auctionId))
                .orElseThrow(() -> new FreezingNotExistsException(auctionId));
    }

    @Override
    public void rechargeBudget(final ChargingFromCardTransaction transaction, final String username) {
        movementsCollection.findOneAndUpdate(Filters.eq(USERNAME_FIELD, username), Updates.combine(
                Updates.inc(BUDGET_VALUE_FIELD, transaction.getAmount().abs()),
                Updates.push(TRANSACTIONS_FIELD,
                        ChargingFromCardTransactionWeb.fromDomainChargingFromCardTransaction(transaction))
        ));
    }

    @Override
    public void withdrawBudget(final WithdrawalTransaction transaction, final String username) {
        movementsCollection.findOneAndUpdate(Filters.eq(USERNAME_FIELD, username), Updates.combine(
                Updates.inc(BUDGET_VALUE_FIELD, transaction.getAmount().abs().negate()),
                Updates.push(TRANSACTIONS_FIELD,
                        WithdrawalTransactionWeb.fromDomainWithdrawalTransaction(transaction))
        ));
    }

    @Override
    public void exchangeMoneyAfterAwardedAuction(final AwardedAuctionTransactionsPair transactionsPair) {
        movementsCollection.findOneAndUpdate(Filters.eq(USERNAME_FIELD, transactionsPair.buyerTransaction().getBuyer()),
                Updates.combine(
                        Updates.inc(BUDGET_VALUE_FIELD, transactionsPair.buyerTransaction().getAmount()),
                        Updates.push(TRANSACTIONS_FIELD,
                                AwardedAuctionTransactionWeb.fromAwardedAuctionTransaction(transactionsPair.buyerTransaction()))
        ));
        movementsCollection.findOneAndUpdate(Filters.eq(USERNAME_FIELD, transactionsPair.sellerTransaction().getSeller()),
                Updates.combine(
                        Updates.inc(BUDGET_VALUE_FIELD, transactionsPair.sellerTransaction().getAmount()),
                        Updates.push(TRANSACTIONS_FIELD,
                                AwardedAuctionTransactionWeb.fromAwardedAuctionTransaction(transactionsPair.sellerTransaction()))
        ));
    }

    private Optional<Freezing> getFreezingByAuctionId(final MovementsHistoryWeb movementsHistoryWeb,
                                                      final String auctionId) {
        return movementsHistoryWeb.getFreezes().stream()
                .filter(freezing -> freezing.getAuctionId().equals(auctionId)).findFirst()
                .map(FreezingWeb::toDomainMovement);
    }
}
