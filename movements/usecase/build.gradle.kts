dependencies {
    implementation(project(":utils"))
    implementation(project(":movements:domain"))

    testImplementation(project(":movements:mongo-repository"))
    testImplementation(project(":uuid-generator"))

    // this will add following dependencies: test --> users.domain.testFixtures
    testImplementation(testFixtures(project(":movements:domain")))
}