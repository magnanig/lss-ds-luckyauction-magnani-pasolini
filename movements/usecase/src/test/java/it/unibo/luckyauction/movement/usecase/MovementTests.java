/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.movement.usecase;

import it.unibo.luckyauction.movement.domain.MovementTestUtils;
import it.unibo.luckyauction.movement.domain.entity.AwardedAuctionTransaction;
import it.unibo.luckyauction.movement.domain.entity.ChargingFromCardTransaction;
import it.unibo.luckyauction.movement.domain.entity.Freezing;
import it.unibo.luckyauction.movement.domain.entity.MovementsHistory;
import it.unibo.luckyauction.movement.domain.entity.WithdrawalTransaction;
import it.unibo.luckyauction.movement.domain.exception.FreezingNotExistsException;
import it.unibo.luckyauction.movement.domain.exception.InsufficientBudgetException;
import it.unibo.luckyauction.movement.domain.valueobject.Budget;
import it.unibo.luckyauction.movement.repository.MovementMongoRepository;
import it.unibo.luckyauction.movement.usecase.exception.FreezingForAuctionAlreadyExistsException;
import it.unibo.luckyauction.movement.usecase.exception.MovementsHistoryAlreadyExistsException;
import it.unibo.luckyauction.movement.usecase.exception.MovementsHistoryNotFoundException;
import it.unibo.luckyauction.movement.usecase.port.MovementRepository;
import it.unibo.luckyauction.util.NumericalConstants;
import it.unibo.luckyauction.uuid.UUIdGenerator;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestMethodOrder;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Collections;
import java.util.function.Function;
import java.util.function.Supplier;

import static it.unibo.luckyauction.movement.domain.MovementTestUtils.AUCTION_TEST_ID;
import static it.unibo.luckyauction.movement.domain.MovementTestUtils.FIRST_USERNAME;
import static it.unibo.luckyauction.movement.domain.MovementTestUtils.MOVEMENT_TEST_AMOUNT;
import static it.unibo.luckyauction.movement.domain.MovementTestUtils.SECOND_USERNAME;
import static it.unibo.luckyauction.movement.domain.MovementTestUtils.SHOES_AUCTION_NAME;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class MovementTests {

    private static final MovementRepository MOVEMENT_REPO = MovementMongoRepository.testRepository();
    private static final String WRONG_USERNAME = SECOND_USERNAME;

    private final CreateMovementsHistory createMovementsHistory = new CreateMovementsHistory(MOVEMENT_REPO);
    private final DeleteMovementsHistory deleteMovementsHistory = new DeleteMovementsHistory(MOVEMENT_REPO);
    private final ManageMovement manageMovement = new ManageMovement(MOVEMENT_REPO, new UUIdGenerator());
    private final FindMovementsHistory findMovementsHistory = new FindMovementsHistory(MOVEMENT_REPO);
    private final FindMovement findMovement = new FindMovement(MOVEMENT_REPO);

    private final Supplier<Budget> userBudget = () -> findMovementsHistory.getUserBudget(FIRST_USERNAME);
    private final Function<BigDecimal, ChargingFromCardTransaction> makeCharging = (amount) ->
            manageMovement.rechargeBudget(amount, "generic test description", FIRST_USERNAME);

    @BeforeAll
    void createUsersMovementsHistories() {
        createNewMovementsHistory(MovementTestUtils.MOVEMENTS_HISTORY_TEST_ID, FIRST_USERNAME);
    }

    @AfterAll
    void deleteUsersMovementsHistories() {
        deleteMovementsHistoryByUsername(FIRST_USERNAME);
    }

    @Test @Order(1)
    void createMovementsHistoryExceptionTest() {
        final Exception ex = assertThrows(MovementsHistoryAlreadyExistsException.class,
                () -> createMovementsHistory.create(MovementTestUtils.ANOTHER_MOVEMENTS_HISTORY_ID, FIRST_USERNAME));
        assertTrue(ex.getMessage().contains("movements history for user " + FIRST_USERNAME + " already exists"));
    }

    @Test @Order(2)
    void findMovementsHistoryByUsernameTest() {
        final MovementsHistory movementsHistory = findMovementsHistory.findByUsername(FIRST_USERNAME).orElseThrow();
        assertEquals(MovementTestUtils.MOVEMENTS_HISTORY_TEST_ID, movementsHistory.getMovementsHistoryId());
        assertEquals(FIRST_USERNAME, movementsHistory.getUsername());
        assertEquals(0, BigDecimal.ZERO.compareTo(movementsHistory.getBudget().value()));
        assertEquals(Collections.emptyList(), movementsHistory.getTransactions());
        assertEquals(Collections.emptyList(), movementsHistory.getFreezes());
    }

    @Test @Order(3)
    void rechargeBudgetTest() {
        assertThrows(MovementsHistoryNotFoundException.class,
                () -> manageMovement.userHasSufficientBudget("wrongId", MOVEMENT_TEST_AMOUNT));
        assertFalse(manageMovement.userHasSufficientBudget(FIRST_USERNAME, MOVEMENT_TEST_AMOUNT));

        final ChargingFromCardTransaction createdTransaction = makeCharging.apply(MOVEMENT_TEST_AMOUNT);

        assertTrue(manageMovement.userHasSufficientBudget(FIRST_USERNAME, MOVEMENT_TEST_AMOUNT));
        assertTrue(findMovement.getTransactions(FIRST_USERNAME).contains(createdTransaction));
        assertEquals(0,
                MOVEMENT_TEST_AMOUNT.compareTo(userBudget.get().value()));

        assertThrows(MovementsHistoryNotFoundException.class,
                () -> manageMovement.rechargeBudget(BigDecimal.TEN, null, WRONG_USERNAME)
        );
        assertThrows(MovementsHistoryNotFoundException.class, () -> findMovementsHistory.getUserBudget(WRONG_USERNAME));
    }

    @Test @Order(4)
    void withdrawBudgetTest() {
        final WithdrawalTransaction createdTransaction = manageMovement.withdrawBudget(MOVEMENT_TEST_AMOUNT,
                FIRST_USERNAME);

        findMovement.findMovementById(createdTransaction.getMovementId(), FIRST_USERNAME)
                .ifPresentOrElse(movement -> {
                    assertEquals(MOVEMENT_TEST_AMOUNT.negate(), movement.getAmount());
                    assertTrue(movement.getTimestamp().before(Calendar.getInstance()));
                    assertEquals(createdTransaction.getMovementId(), movement.getMovementId());
                }, Assertions::fail);
        assertTrue(findMovement.getTransactions(FIRST_USERNAME).contains(createdTransaction));
        assertEquals(0, BigDecimal.ZERO.compareTo(userBudget.get().value()));

        assertThrows(InsufficientBudgetException.class,
                () -> manageMovement.withdrawBudget(MOVEMENT_TEST_AMOUNT, FIRST_USERNAME));
        assertThrows(MovementsHistoryNotFoundException.class,
                () -> manageMovement.withdrawBudget(MOVEMENT_TEST_AMOUNT, WRONG_USERNAME));
    }

    @Test @Order(NumericalConstants.FIVE)
    void freezingAndUnfreezingTest() {
        assertThrows(FreezingNotExistsException.class,
                () -> manageMovement.unfreezeMoney(FIRST_USERNAME, AUCTION_TEST_ID));

        makeCharging.apply(MOVEMENT_TEST_AMOUNT); // to make a freeze

        final Freezing createdFreezing = manageMovement.freezeMoney(FIRST_USERNAME, MOVEMENT_TEST_AMOUNT,
                AUCTION_TEST_ID, SHOES_AUCTION_NAME);
        assertTrue(findMovement.getFreezes(FIRST_USERNAME).contains(createdFreezing));
        assertEquals(0, BigDecimal.ZERO.compareTo(userBudget.get().value()));

        assertThrows(FreezingForAuctionAlreadyExistsException.class, () ->
                manageMovement.freezeMoney(FIRST_USERNAME, BigDecimal.TEN, AUCTION_TEST_ID, SHOES_AUCTION_NAME));

        // unfreeze the previous freezing
        assertEquals(createdFreezing, manageMovement.unfreezeMoney(FIRST_USERNAME, AUCTION_TEST_ID));
        assertTrue(findMovement.getFreezes(FIRST_USERNAME).isEmpty());
        assertEquals(0, MOVEMENT_TEST_AMOUNT.compareTo(userBudget.get().value()));
    }

    @Test @Order(NumericalConstants.SIX)
    void exchangeMoneyAfterAwardedAuctionTest() {
        createNewMovementsHistory(MovementTestUtils.ANOTHER_MOVEMENTS_HISTORY_ID, SECOND_USERNAME); // for seller user
        assertEquals(0,
                BigDecimal.ZERO.compareTo(findMovementsHistory.getUserBudget(SECOND_USERNAME).value()));
        // freeze money for buyer user
        manageMovement.freezeMoney(FIRST_USERNAME, MOVEMENT_TEST_AMOUNT, AUCTION_TEST_ID, SHOES_AUCTION_NAME);
        // exchange money between buyer and seller users
        manageMovement.exchangeMoneyAfterAwardedAuction(FIRST_USERNAME, SECOND_USERNAME, MOVEMENT_TEST_AMOUNT,
                AUCTION_TEST_ID, SHOES_AUCTION_NAME);

        // buyer check
        final AwardedAuctionTransaction buyerTransaction =
                (AwardedAuctionTransaction) findMovement.getTransactions(FIRST_USERNAME).stream()
                .filter(t -> t instanceof AwardedAuctionTransaction)
                .findFirst().orElseThrow();
        assertEquals(AUCTION_TEST_ID, buyerTransaction.getAuctionId());
        assertEquals(MOVEMENT_TEST_AMOUNT.negate(), buyerTransaction.getAmount());
        assertEquals(FIRST_USERNAME, buyerTransaction.getBuyer());
        assertEquals(SECOND_USERNAME, buyerTransaction.getSeller());
        assertTrue(findMovement.getFreezes(FIRST_USERNAME).isEmpty());
        assertEquals(0, BigDecimal.ZERO.compareTo(userBudget.get().value()));

        // seller check
        final AwardedAuctionTransaction sellerTransaction =
                (AwardedAuctionTransaction) findMovement.getTransactions(SECOND_USERNAME).stream()
                        .filter(t -> t instanceof AwardedAuctionTransaction)
                        .findFirst().orElseThrow();
        assertEquals(AUCTION_TEST_ID, sellerTransaction.getAuctionId());
        assertEquals(MOVEMENT_TEST_AMOUNT, sellerTransaction.getAmount());
        assertEquals(FIRST_USERNAME, sellerTransaction.getBuyer());
        assertEquals(SECOND_USERNAME, sellerTransaction.getSeller());
        assertTrue(findMovement.getFreezes(FIRST_USERNAME).isEmpty());
        assertEquals(0,
                MOVEMENT_TEST_AMOUNT.compareTo(findMovementsHistory.getUserBudget(SECOND_USERNAME).value()));

        assertNotEquals(buyerTransaction.getMovementId(), sellerTransaction.getMovementId());
        deleteMovementsHistoryByUsername(SECOND_USERNAME);
    }

    private void createNewMovementsHistory(final String id, final String username) {
        if (findMovementsHistory.findByUsername(username).isPresent()) {
            deleteMovementsHistory.deleteByUsername(username);
        }
        createMovementsHistory.create(id, username);
    }

    private void deleteMovementsHistoryByUsername(final String username) {
        deleteMovementsHistory.deleteByUsername(username)
                .ifPresentOrElse(history -> assertEquals(username, history.getUsername()), Assertions::fail);
    }
}
