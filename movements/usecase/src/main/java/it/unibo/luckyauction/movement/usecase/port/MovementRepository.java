/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.movement.usecase.port;

import it.unibo.luckyauction.movement.domain.entity.ChargingFromCardTransaction;
import it.unibo.luckyauction.movement.domain.entity.WithdrawalTransaction;
import it.unibo.luckyauction.movement.domain.entity.Freezing;
import it.unibo.luckyauction.movement.domain.entity.Movement;
import it.unibo.luckyauction.movement.domain.entity.MovementsHistory;
import it.unibo.luckyauction.movement.domain.entity.Transaction;
import it.unibo.luckyauction.movement.domain.valueobject.Budget;
import it.unibo.luckyauction.movement.domain.valueobject.AwardedAuctionTransactionsPair;

import java.util.List;
import java.util.Optional;

public interface MovementRepository {

    /**
     * Create a new MovementsHistory object to the repository.
     * @param movementsHistory  the MovementsHistory to add
     */
    void createMovementsHistory(MovementsHistory movementsHistory);

    /**
     * Remove a MovementsHistory by the associated username.
     * @param username    the username associated to the MovementsHistory to remove
     * @return            the just removed MovementsHistory, if any
     */
    Optional<MovementsHistory> deleteMovementsHistoryByUsername(String username);

    /**
     * Find a MovementsHistory by the associated username.
     * @param username  the username for which looking for their movements history
     * @return          the corresponding MovementsHistory, if any
     */
    Optional<MovementsHistory> findMovementsHistoryByUsername(String username);

    /**
     * Find a movement by its id and associated username.
     * @param movementId    the id of the desired movement
     * @param username      the username of the user to which the movement belongs
     * @return              the corresponding movement, if any
     */
    Optional<Movement> findMovementById(String movementId, String username);

    /**
     * Find a movement by its id.
     * @param movementId    the id of the desired movement
     * @return              the corresponding movement, if any
     */
    Optional<Movement> findMovementById(String movementId);

    /**
     * Get the budget of the specified user.
     * @param username  the desired username
     * @return          the user budget
     */
    Optional<Budget> getUserBudget(String username);

    /**
     * Get all freezes associated to the desired user.
     * @param username  the username of the desired user
     * @return          the freezes of the specified user
     */
    List<Freezing> getFreezes(String username);

    /**
     * Get all transactions associated to the desired user.
     * @param username  the username of the desired user
     * @return          the transactions of the specified user
     */
    List<Transaction> getTransactions(String username);

    /**
     * Freeze part of money for the specified user.
     * @param freezing  the freezing to perform
     * @param username  the username of the user to which freezing must be linked
     */
    void freezeMoney(Freezing freezing, String username);

    /**
     * Unfreeze a precedent money freezing for a specific auction by a user.
     * @param username  the user username to unfreeze the money to
     * @param auctionId the id of the auction linked to the freezing
     * @return          the just removed freezing
     */
    Freezing unfreezeMoney(String username, String auctionId);

    /**
     * Recharge budget of the user.
     * @param transaction   the transaction corresponding to recharge operation
     * @param username      the username associated to the transaction
     */
    void rechargeBudget(ChargingFromCardTransaction transaction, String username);

    /**
     * Withdraw money from the user budget.
     * @param transaction   the transaction corresponding to withdrawal operation
     * @param username      the username associated to the transaction
     */
    void withdrawBudget(WithdrawalTransaction transaction, String username);

    /**
     * Exchange money from buyer to auction seller.
     * @param transactionsPair  the transaction pair with buyer and
     *                          seller awarded auction transactions
     */
    void exchangeMoneyAfterAwardedAuction(AwardedAuctionTransactionsPair transactionsPair);
}
