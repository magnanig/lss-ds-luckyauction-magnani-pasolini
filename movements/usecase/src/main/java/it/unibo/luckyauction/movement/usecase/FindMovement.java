/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.movement.usecase;

import it.unibo.luckyauction.movement.usecase.port.MovementRepository;
import it.unibo.luckyauction.movement.domain.entity.Freezing;
import it.unibo.luckyauction.movement.domain.entity.Movement;
import it.unibo.luckyauction.movement.domain.entity.Transaction;

import java.util.List;
import java.util.Optional;

/**
 * The class to manage the search of a movement.
 */
public class FindMovement {

    private final MovementRepository movementRepository;

    /**
     * Create a new instance to use in order to search movements.
     * @param movementRepository    the movement repository (i.e. a database of
     *                              movements histories)
     */
    public FindMovement(final MovementRepository movementRepository) {
        this.movementRepository = movementRepository;
    }

    /**
     * Find a movement by its id and associated username.
     * @param movementId    the id of the desired movement
     * @param username      the username of the user to which the movement belongs
     * @return              the corresponding movement, if exists
     */
    public Optional<Movement> findMovementById(final String movementId, final String username) {
        return movementRepository.findMovementById(movementId, username);
    }

    /**
     * Find a movement by its id.
     * @param movementId    the id of the desired movement
     * @return              the corresponding movement, if exists
     */
    public Optional<Movement> findMovementById(final String movementId) {
        return movementRepository.findMovementById(movementId);
    }

    /**
     * Get all freezes associated to the desired user.
     * @param username  the username of the desired user
     * @return          the freezes of the specified user
     */
    public List<Freezing> getFreezes(final String username) {
        return movementRepository.getFreezes(username);
    }

    /**
     * Get all transactions associated to the desired user.
     * @param username  the username of the desired user
     * @return          the transactions of the specified user
     */
    public List<Transaction> getTransactions(final String username) {
        return movementRepository.getTransactions(username);
    }
}
