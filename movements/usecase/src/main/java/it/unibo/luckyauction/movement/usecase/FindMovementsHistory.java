package it.unibo.luckyauction.movement.usecase;

import it.unibo.luckyauction.movement.domain.valueobject.Budget;
import it.unibo.luckyauction.movement.usecase.exception.MovementsHistoryNotFoundException;
import it.unibo.luckyauction.movement.usecase.port.MovementRepository;
import it.unibo.luckyauction.movement.domain.entity.MovementsHistory;

import java.util.Optional;

/**
 * The class to manage the search of a MovementsHistory objects.
 */
public class FindMovementsHistory {

    private final MovementRepository movementRepository;

    /**
     * Create a new instance to use in order to search MovementsHistory objects.
     * @param movementRepository    the movement repository (i.e. a database of
     *                              movements histories)
     */
    public FindMovementsHistory(final MovementRepository movementRepository) {
        this.movementRepository = movementRepository;
    }

    /**
     * Find a MovementsHistory by the associated username.
     * @param username  the username for which looking for their movements history
     * @return          the corresponding MovementsHistory, if any
     */
    public Optional<MovementsHistory> findByUsername(final String username) {
        return movementRepository.findMovementsHistoryByUsername(username);
    }

    /**
     * Get the budget of the specified user.
     * @param username                              the desired username
     * @return                                      the user budget
     * @throws MovementsHistoryNotFoundException    if there is no MovementsHistory for the user
     */
    public Budget getUserBudget(final String username) {
        return movementRepository.getUserBudget(username)
                .orElseThrow(() -> new MovementsHistoryNotFoundException(username));
    }
}
