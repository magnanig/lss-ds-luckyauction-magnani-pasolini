/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.movement.usecase;

import it.unibo.luckyauction.movement.domain.entity.Freezing;
import it.unibo.luckyauction.movement.domain.exception.FreezingNotExistsException;
import it.unibo.luckyauction.movement.domain.exception.InsufficientBudgetException;
import it.unibo.luckyauction.movement.domain.valueobject.AwardedAuctionTransactionsPair;
import it.unibo.luckyauction.movement.domain.entity.AwardedAuctionTransaction;
import it.unibo.luckyauction.movement.domain.valueobject.Budget;
import it.unibo.luckyauction.movement.usecase.exception.FreezingForAuctionAlreadyExistsException;
import it.unibo.luckyauction.movement.usecase.exception.MovementsHistoryNotFoundException;
import it.unibo.luckyauction.movement.usecase.port.MovementRepository;
import it.unibo.luckyauction.movement.domain.entity.ChargingFromCardTransaction;
import it.unibo.luckyauction.movement.domain.entity.WithdrawalTransaction;
import it.unibo.luckyauction.util.port.IdGenerator;

import java.math.BigDecimal;

/**
 * The class to manage the creation of new transactions.
 */
public class ManageMovement {

    private final MovementRepository movementRepository;
    private final IdGenerator movementIdGenerator;

    /**
     * Create a new instance to use in order to create new transactions.
     * @param movementRepository    the movement repository (i.e. a database of movements histories)
     * @param movementIdGenerator   the generator of movement ids
     */
    public ManageMovement(final MovementRepository movementRepository, final IdGenerator movementIdGenerator) {
        this.movementRepository = movementRepository;
        this.movementIdGenerator = movementIdGenerator;
    }

    /**
     * Freeze part of user money to raise a new bid. The money amount to be frozen is equals to the bid amount.
     * @param bidderUsername    the username of the user who raised
     * @param raisedAmount      the raised amount
     * @param auctionId         the auction id for which bid is referred
     * @param productName       the name of the product of the auction in question
     * @return                  the just created freezing
     * @throws FreezingForAuctionAlreadyExistsException if a freezing for the specified auction already
     *                                                  exists for the bidder user
     * @throws MovementsHistoryNotFoundException        if there is no MovementsHistory for the user
     * @throws InsufficientBudgetException              if the user has insufficient money in their budget
     */
    public Freezing freezeMoney(final String bidderUsername, final BigDecimal raisedAmount,
                                final String auctionId, final String productName)
    throws FreezingForAuctionAlreadyExistsException, MovementsHistoryNotFoundException, InsufficientBudgetException {
        checkIfFreezingAlreadyExisting(bidderUsername, auctionId);
        checkIfUserHaveSufficientBudget(bidderUsername, raisedAmount);
        final Freezing freezing = Freezing.builder().freezingId(movementIdGenerator.generate())
                .amount(raisedAmount).auctionId(auctionId).productName(productName).build();
        movementRepository.freezeMoney(freezing, bidderUsername);
        return freezing;
    }

    /**
     * Unfreeze a precedent money freezing for a specific auction by a user.
     * @param username  the user username to unfreeze the money to
     * @param auctionId the id of the auction linked to the freezing
     * @return          the just removed freezing
     * @throws FreezingNotExistsException    if no freezing exists for current username and auction
     */
    public Freezing unfreezeMoney(final String username, final String auctionId) throws FreezingNotExistsException {
        checkIfFreezingNotExist(username, auctionId);
        return movementRepository.unfreezeMoney(username, auctionId);
    }

    /**
     * Recharge budget of the user.
     * @param amount        the (positive) amount of the recharge
     * @param description   the description associated with the transaction
     * @param username      the username associated to the transaction
     * @return              the just created charging from card transaction
     * @throws MovementsHistoryNotFoundException    if there is no MovementsHistory for the user
     */
    public ChargingFromCardTransaction rechargeBudget(final BigDecimal amount, final String description,
                                                      final String username) throws MovementsHistoryNotFoundException {
        movementRepository.getUserBudget(username).orElseThrow(() -> new MovementsHistoryNotFoundException(username));
        final ChargingFromCardTransaction newTransaction = ChargingFromCardTransaction.builder()
                .transactionId(movementIdGenerator.generate()).amount(amount.abs()).description(description).build();
        movementRepository.rechargeBudget(newTransaction, username);
        return newTransaction;
    }

    /**
     * Withdraw money from the user budget.
     * @param amount    the (positive) amount to be withdrawn
     * @param username  the username associated to the transaction
     * @return          the just created withdrawal transaction
     * @throws MovementsHistoryNotFoundException    if there is no MovementsHistory for the user
     * @throws InsufficientBudgetException          if the user has insufficient money in their budget
     */
    public WithdrawalTransaction withdrawBudget(final BigDecimal amount, final String username)
            throws MovementsHistoryNotFoundException, InsufficientBudgetException {
        checkIfUserHaveSufficientBudget(username, amount.abs());
        final WithdrawalTransaction newTransaction = WithdrawalTransaction.builder()
                .transactionId(movementIdGenerator.generate()).amount(amount.abs().negate()).build();
        movementRepository.withdrawBudget(newTransaction, username);
        return newTransaction;
    }

    /**
     * Exchange money from buyer to auction seller, after the auction closes.
     * Create the two transactions for an awarded auction, for both seller and buyer.
     * The money amount for seller will be positive, while for buyer negative.
     * @param buyerUsername     the buyer username
     * @param sellerUsername    the seller username
     * @param amount            the final amount of the awarded auction
     * @param auctionId         the id of awarded auction
     * @param productName       the name of the product of the auction in question
     * @throws FreezingNotExistsException   if no freezing exists for the winning bid of the buyer user
     */
    public void exchangeMoneyAfterAwardedAuction(final String buyerUsername, final String sellerUsername,
                                                 final BigDecimal amount, final String auctionId,
                                                 final String productName) throws FreezingNotExistsException {
        unfreezeMoney(buyerUsername, auctionId);
        movementRepository.exchangeMoneyAfterAwardedAuction(
                new AwardedAuctionTransactionsPair(
                        AwardedAuctionTransaction.builder().transactionId(movementIdGenerator.generate())
                        .buyerUsername(buyerUsername).sellerUsername(sellerUsername).amount(amount)
                                .auctionId(auctionId).productName(productName).build(),
                        AwardedAuctionTransaction.builder().transactionId(movementIdGenerator.generate())
                                .buyerUsername(buyerUsername).sellerUsername(sellerUsername).amount(amount.negate())
                                .auctionId(auctionId).productName(productName).build()
                ));
    }

    /**
     * Check if the user has sufficient budget.
     * @param username          the user's username
     * @param sufficientBudget  the amount you want to check
     * @return                  true if the user has sufficient budget, false otherwise
     */
    public boolean userHasSufficientBudget(final String username, final BigDecimal sufficientBudget) {
        final Budget userBudget = movementRepository.getUserBudget(username)
                .orElseThrow(() -> new MovementsHistoryNotFoundException(username));
        return userBudget.value().compareTo(sufficientBudget) >= 0;
    }

    private void checkIfUserHaveSufficientBudget(final String username, final BigDecimal desiredAmount) {
        final Budget userBudget = movementRepository.getUserBudget(username)
                .orElseThrow(() -> new MovementsHistoryNotFoundException(username));
        if (userBudget.value().compareTo(desiredAmount) < 0) {
            throw new InsufficientBudgetException(userBudget, desiredAmount);
        }
    }

    private void checkIfFreezingAlreadyExisting(final String username, final String auctionId) {
        movementRepository.getFreezes(username).stream()
                .filter(freezing -> freezing.getAuctionId().equals(auctionId))
                .findFirst().ifPresent(freezing -> {
                    throw new FreezingForAuctionAlreadyExistsException(auctionId);
                });
    }

    private void checkIfFreezingNotExist(final String username, final String auctionId) {
        movementRepository.getFreezes(username).stream()
                .filter(freezing -> freezing.getAuctionId().equals(auctionId))
                .findFirst().orElseThrow(() -> new FreezingNotExistsException(auctionId));
    }
}
