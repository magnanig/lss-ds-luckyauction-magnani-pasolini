/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.movement.usecase;

import it.unibo.luckyauction.movement.usecase.port.MovementRepository;
import it.unibo.luckyauction.movement.domain.entity.MovementsHistory;

import java.util.Optional;

/**
 * The class to manage the deletion of a MovementsHistory objects.
 */
public class DeleteMovementsHistory {

    private final MovementRepository movementRepository;

    /**
     * Create a new instance to use in order to delete MovementsHistory objects.
     * @param movementRepository    the movement repository (i.e. a database of
     *                              movements histories)
     */
    public DeleteMovementsHistory(final MovementRepository movementRepository) {
        this.movementRepository = movementRepository;
    }

    /**
     * Delete a MovementsHistory by the associated username.
     * @param username  the username associated to the MovementsHistory to remove
     * @return          the just deleted MovementsHistory, if any
     */
    public Optional<MovementsHistory> deleteByUsername(final String username) {
        return movementRepository.deleteMovementsHistoryByUsername(username);
    }
}
