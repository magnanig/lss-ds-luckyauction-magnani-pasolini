/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.movement.usecase;

import it.unibo.luckyauction.movement.usecase.exception.MovementsHistoryAlreadyExistsException;
import it.unibo.luckyauction.movement.usecase.port.MovementRepository;
import it.unibo.luckyauction.movement.domain.entity.MovementsHistory;

/**
 * The class to manage the creation of a new MovementsHistory objects.
 */
public class CreateMovementsHistory {

    private final MovementRepository movementRepository;

    /**
     * Create a new instance to use in order to create new MovementsHistory objects.
     * @param movementRepository    the movement repository (i.e. a database of
     *                              movements histories)
     */
    public CreateMovementsHistory(final MovementRepository movementRepository) {
        this.movementRepository = movementRepository;
    }

    /**
     * Create a new MovementsHistory in the repository, with 0.00 € default budget.
     * @param movementsHistoryId    the new MovementsHistory id
     * @param username              the username associated to such history
     * @throws MovementsHistoryAlreadyExistsException   if a history for the specified
     *                                                  username already exists
     */
    public void create(final String movementsHistoryId, final String username)
            throws MovementsHistoryAlreadyExistsException {
        if (movementRepository.findMovementsHistoryByUsername(username).isPresent()) {
            throw new MovementsHistoryAlreadyExistsException(username);
        }
        final MovementsHistory movementsHistory = MovementsHistory.builder()
                .movementsHistoryId(movementsHistoryId).username(username).build();
        movementRepository.createMovementsHistory(movementsHistory);
    }
}
