module lss.ds.luckyauction.magnani.pasolini.movements.usecase.main {
    exports it.unibo.luckyauction.movement.usecase;
    exports it.unibo.luckyauction.movement.usecase.port;
    exports it.unibo.luckyauction.movement.usecase.exception;
    requires lss.ds.luckyauction.magnani.pasolini.utils.main;
    requires lss.ds.luckyauction.magnani.pasolini.movements.domain.main;
}