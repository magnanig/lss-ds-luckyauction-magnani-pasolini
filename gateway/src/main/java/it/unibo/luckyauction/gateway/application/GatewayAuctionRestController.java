/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.gateway.application;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import it.unibo.luckyauction.auction.adapter.AuctionWeb;
import it.unibo.luckyauction.auction.adapter.BidWeb;
import it.unibo.luckyauction.auction.adapter.NewAuctionWeb;
import it.unibo.luckyauction.gateway.util.HttpRequestUtils;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import static it.unibo.luckyauction.gateway.util.HttpRequestUtils.TIMEOUT;
import static it.unibo.luckyauction.spring.util.http.WebClientUtils.checkErrors;

@SuppressWarnings("PMD.AvoidDuplicateLiterals")
@RestController @RequestMapping("/api/auctions")
public class GatewayAuctionRestController {

    private static final String USERNAME_FIELD = "username";
    private static final String TOKEN_FIELD = "bearerAuth";

    private final WebClient webClient = HttpRequestUtils.auctionsWebClient();

    /**
     * Create a new auction for the user making the request.
     * @param auction   the new auction to create
     * @return          the just created auction
     */
    @PostMapping
    @Operation(security = @SecurityRequirement(name = TOKEN_FIELD))
    public AuctionWeb createAuction(@RequestBody final NewAuctionWeb auction, final Authentication authentication) {
        return checkErrors(webClient
                .post()
                .uri(uri -> uri.queryParam(USERNAME_FIELD, authentication.getName()).build())
                .body(Mono.just(auction), NewAuctionWeb.class)
                .retrieve())
                .bodyToMono(AuctionWeb.class)
                .block(TIMEOUT);
    }

    /**
     * Find an auction by its id.
     * @param auctionId     the desired auction id
     * @return              the corresponding auction, if present
     */
    @GetMapping("/{auctionId}")
    public Optional<AuctionWeb> findAuctionById(@PathVariable final String auctionId) {
        return webClient
                .get()
                .uri(uri -> uri.path("/{auctionId}").build(auctionId))
                .retrieve()
                .bodyToMono(AuctionWeb.class)
                .blockOptional(TIMEOUT);
    }

    /**
     * Get the active auctions that satisfy the filters, except those
     * created by the user who made the request.
     * @param productCategory       the optional product category
     * @param productState          the optional product state
     * @return                      the list of active auctions, eventually matching the filters
     */
    @CrossOrigin(origins = "*") @GetMapping("/active")
    public List<AuctionWeb> getActiveAuctions(
            @RequestParam(name = "category", required = false) final String productCategory,
            @RequestParam(name = "state", required = false) final String productState,
            @RequestParam(name = "excluded", required = false) final String username) {
        return webClient
                .get()
                .uri(uri -> uri.path("/active")
                        .queryParam("category", productCategory)
                        .queryParam("state", productState)
                        .queryParamIfPresent("excluded", Optional.ofNullable(username))
                        .build())
                .exchangeToMono(response -> response
                        .bodyToMono(new ParameterizedTypeReference<List<AuctionWeb>>() { }))
                .block(TIMEOUT);
    }

    /**
     * Add a new bid for the specified auction, for the user making the request.
     * @param auctionId     the id of the auction to which add the bid
     * @param bidAmount     the amount of the bid
     * @return              the just added bid
     */
    @PostMapping("/{auctionId}/bids")
    @Operation(security = @SecurityRequirement(name = TOKEN_FIELD))
    public BidWeb addNewBid(@PathVariable final String auctionId,
                            @RequestParam(name = "amount") final BigDecimal bidAmount,
                            final Authentication authentication) {
        return checkErrors(webClient
                .post()
                .uri(uri -> uri.path("/{auctionId}/bids")
                        .queryParam("amount", bidAmount)
                        .queryParam(USERNAME_FIELD, authentication.getName())
                        .build(auctionId))
                .retrieve())
                .bodyToMono(BidWeb.class)
                .block(TIMEOUT);
    }

    /**
     * Get all the bids raised for the specified auction.
     * @param auctionId     the desired auction id
     * @return              the list of bids raised for such auction,
     *                      ordered by timestamp
     */
    @GetMapping("/{auctionId}/bids")
    public List<BidWeb> getBidsHistory(@PathVariable final String auctionId) {
        return webClient
                .get()
                .uri(uri -> uri.path("/{auctionId}/bids").build(auctionId))
                .exchangeToMono(response -> response.bodyToMono(new ParameterizedTypeReference<List<BidWeb>>() { }))
                .block(TIMEOUT);
    }
}
