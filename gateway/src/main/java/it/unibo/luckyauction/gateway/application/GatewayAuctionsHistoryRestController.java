/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.gateway.application;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import it.unibo.luckyauction.auction.adapter.AuctionsHistoryWeb;
import it.unibo.luckyauction.auction.adapter.AwardedAuctionWeb;
import it.unibo.luckyauction.auction.adapter.CreatedEndedAuctionWeb;
import it.unibo.luckyauction.auction.adapter.CreatedOngoingAuctionWeb;
import it.unibo.luckyauction.auction.adapter.LostAuctionWeb;
import it.unibo.luckyauction.auction.adapter.ParticipatingAuctionWeb;
import it.unibo.luckyauction.auction.adapter.PdfReportWeb;
import it.unibo.luckyauction.gateway.util.HttpRequestUtils;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.List;
import java.util.Optional;

import static it.unibo.luckyauction.gateway.util.HttpRequestUtils.TIMEOUT;
import static it.unibo.luckyauction.spring.util.http.WebClientUtils.checkErrors;

@RestController @RequestMapping("/api/auctions/histories")
public class GatewayAuctionsHistoryRestController {

    private static final String USERNAME_FIELD = "username";
    private static final String TOKEN_FIELD = "bearerAuth";

    private final WebClient webClient = HttpRequestUtils.auctionsHistoryWebClient();

    /**
     * Find the auctions' history of the user making the request.
     * @return  the auctions' history of the specified user, if any
     */
    @GetMapping
    @Operation(security = @SecurityRequirement(name = TOKEN_FIELD))
    public Optional<AuctionsHistoryWeb> findAuctionsHistoryByUsername(final Authentication authentication) {
        return webClient
                .get()
                .uri(uri -> uri.queryParam(USERNAME_FIELD, authentication.getName()).build())
                .retrieve()
                .bodyToMono(AuctionsHistoryWeb.class)
                .blockOptional(TIMEOUT);
    }

    /**
     * Get the list of the awarded auctions for the user making the request.
     * @return  the list of awarded auctions
     */
    @GetMapping("/awarded")
    @Operation(security = @SecurityRequirement(name = TOKEN_FIELD))
    public List<AwardedAuctionWeb> getAwardedAuctions(final Authentication authentication) {
        return webClient
                .get()
                .uri(uri -> uri.path("/awarded")
                        .queryParam(USERNAME_FIELD, authentication.getName())
                        .build())
                .exchangeToMono(response -> response
                        .bodyToMono(new ParameterizedTypeReference<List<AwardedAuctionWeb>>() { }))
                .block(TIMEOUT);
    }

    /**
     * Get the list of the created ongoing auctions for the user making the request.
     * @return  the list of created ongoing auctions
     */
    @GetMapping("/created/ongoing")
    @Operation(security = @SecurityRequirement(name = TOKEN_FIELD))
    public List<CreatedOngoingAuctionWeb> getCreatedOngoingAuctions(final Authentication authentication) {
        return webClient
                .get()
                .uri(uri -> uri.path("/created/ongoing")
                        .queryParam(USERNAME_FIELD, authentication.getName())
                        .build())
                .exchangeToMono(response -> response
                        .bodyToMono(new ParameterizedTypeReference<List<CreatedOngoingAuctionWeb>>() { }))
                .block(TIMEOUT);
    }

    /**
     * Get the list of the created ended auctions for the user making the request.
     * @return  the list of created ended auctions
     */
    @GetMapping("/created/ended")
    @Operation(security = @SecurityRequirement(name = TOKEN_FIELD))
    public List<CreatedEndedAuctionWeb> getCreatedEndedAuctions(final Authentication authentication) {
        return webClient
                .get()
                .uri(uri -> uri.path("/created/ended")
                        .queryParam(USERNAME_FIELD, authentication.getName())
                        .build())
                .exchangeToMono(response -> response
                        .bodyToMono(new ParameterizedTypeReference<List<CreatedEndedAuctionWeb>>() { }))
                .block(TIMEOUT);
    }

    /**
     * Get the list of the lost auctions for the user making the request.
     * @return  the list of lost auctions
     */
    @GetMapping("/lost")
    @Operation(security = @SecurityRequirement(name = TOKEN_FIELD))
    public List<LostAuctionWeb> getLostAuctions(final Authentication authentication) {
        return webClient
                .get()
                .uri(uri -> uri.path("/lost")
                        .queryParam(USERNAME_FIELD, authentication.getName())
                        .build())
                .exchangeToMono(response -> response
                        .bodyToMono(new ParameterizedTypeReference<List<LostAuctionWeb>>() { }))
                .block(TIMEOUT);
    }

    /**
     * Get the list of the participating auctions for the user making the request.
     * @return  the list of participating auctions
     */
    @GetMapping("/participating")
    @Operation(security = @SecurityRequirement(name = TOKEN_FIELD))
    public List<ParticipatingAuctionWeb> getParticipatingAuctions(final Authentication authentication) {
        return webClient
                .get()
                .uri(uri -> uri.path("/participating")
                        .queryParam(USERNAME_FIELD, authentication.getName())
                        .build())
                .exchangeToMono(response -> response
                        .bodyToMono(new ParameterizedTypeReference<List<ParticipatingAuctionWeb>>() { }))
                .block(TIMEOUT);
    }

    /**
     * Get the pdf report for the specified awarded auction, for the user making the request.
     * @param auctionId     the desired auction id (must correspond to an awarded auction
     *                      for the current user)
     * @return              the pdf report for the specified awarded auction
     */
    @GetMapping("/awarded/{auctionId}/pdf")
    @Operation(security = @SecurityRequirement(name = TOKEN_FIELD))
    public PdfReportWeb getAwardedAuctionPdfReport(@PathVariable final String auctionId,
                                                   final Authentication authentication) {
        return checkErrors(webClient
                .get()
                .uri(uri -> uri.path("/awarded/{auctionId}/pdf")
                        .queryParam(USERNAME_FIELD, authentication.getName())
                        .build(auctionId))
                .retrieve())
                .bodyToMono(PdfReportWeb.class)
                .block(TIMEOUT);
    }

    /**
     * Get the pdf report for the specified created (and ended) auction, for the user making the request.
     * If nobody has been awarded the auction, pdf report will no exists.
     * @param auctionId     the desired auction id (must correspond to a created ended auction
     *                      for the current user)
     * @return              the pdf report for the specified created (and ended) auction, if any
     */
    @GetMapping("/created/{auctionId}/pdf")
    @Operation(security = @SecurityRequirement(name = TOKEN_FIELD))
    public Optional<PdfReportWeb> getCreatedAuctionPdfReport(@PathVariable final String auctionId,
                                                             final Authentication authentication) {
        return checkErrors(webClient
                .get()
                .uri(uri -> uri.path("/created/{auctionId}/pdf")
                        .queryParam(USERNAME_FIELD, authentication.getName())
                        .build(auctionId))
                .retrieve())
                .bodyToMono(PdfReportWeb.class)
                .blockOptional(TIMEOUT);
    }
}
