/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.gateway.security;

import it.unibo.luckyauction.gateway.util.JwtUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Tutte le richieste che vengono fatte a questa applicazione, passano prima tutte da qui.
 * Questa classe intercetta ogni richiesta che viene fatta e serve per verificare che
 * l'utente possa (abbia i permessi per) accedere alle risorse richieste.
 */
public class JwtTokenAuthorizationFilter extends OncePerRequestFilter {

    /**
     * Nel caso in cui si tratti di una richiesta di login, non vogliamo fare nulla qui;
     * semplicemente lasciar passare la richiesta per consentire all'utente di autenticarsi.
     * Al contrario, se è una qualsiasi altra richiesta, allora vogliamo verificare
     * se l'utente ha l'autorizzazione per accedere a quella specifica risorsa;
     * andando così ad analizzare il jwt token.
     */
    @SuppressWarnings("PMD.LiteralsFirstInComparisons")
    @Override
    protected void doFilterInternal(final HttpServletRequest request, final HttpServletResponse response,
                                    final FilterChain filterChain) throws ServletException, IOException {
        if (request.getServletPath().equals("/api/login")) {
            filterChain.doFilter(request, response); // pass the request to the next filter
        } else {
            JwtUtils.checkTokenAuthorization(request, response, filterChain, SecurityContextHolder.getContext());
        }
    }
}
