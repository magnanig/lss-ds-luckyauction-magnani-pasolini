/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.gateway.util;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static java.util.Arrays.stream;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.HttpStatus.FORBIDDEN;

public final class JwtUtils {

    private static final String JWT_SECRET = "JWT_SECRET";
    private static final String JWT_ISSUER = "luckyauction";
    private static final Algorithm ALGORITHM = Algorithm.HMAC256(JWT_SECRET); //.getBytes());
    private static final Duration ACCESS_TOKEN_DURATION = Duration.ofHours(1);
    private static final Duration REFRESH_TOKEN_DURATION = Duration.ofHours(2);

    public static DecodedJWT decodeJWT(final String token) {
        final JWTVerifier verifier = JWT.require(ALGORITHM).build();
        return verifier.verify(token);
    }

    public static String generateAccessToken(final String username, final List<String> roles) {
        return JWT.create()
                .withSubject(username) // the subject is a unique identifier for the user
                .withExpiresAt(new Date(System.currentTimeMillis() + ACCESS_TOKEN_DURATION.toMillis()))
                .withIssuer(JWT_ISSUER) // the name of the token supplier
                .withClaim("roles", roles)
                .sign(ALGORITHM);
    }

    public static String generateRefreshToken(final String username) {
        return JWT.create()
                .withSubject(username) // the subject is a unique identifier for the user
                .withExpiresAt(new Date(System.currentTimeMillis() + REFRESH_TOKEN_DURATION.toMillis()))
                .withIssuer(JWT_ISSUER) // the name of the token supplier
                .sign(ALGORITHM);
    }

    public static void checkTokenAuthorization(final HttpServletRequest request, final HttpServletResponse response,
                                               final FilterChain filterChain, final SecurityContext securityContext)
            throws IOException, ServletException {
        final String authorizationHeader = request.getHeader(AUTHORIZATION);
        if (authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
            try {
                final String token = authorizationHeader.substring("Bearer ".length());
                final DecodedJWT decodedJWT = decodeJWT(token);
                final String username = decodedJWT.getSubject();
                final String[] roles = decodedJWT.getClaim("roles").asArray(String.class);
                final Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
                stream(roles).forEach(role -> authorities.add(new SimpleGrantedAuthority(role)));
                final UsernamePasswordAuthenticationToken authenticationToken =
                        new UsernamePasswordAuthenticationToken(username, null, authorities);
                securityContext.setAuthentication(authenticationToken);
            } catch (Exception exception) {
                response.setHeader("error", "Invalid token");
                response.setStatus(FORBIDDEN.value());
                response.setContentType(MediaType.APPLICATION_JSON_VALUE);
                new ObjectMapper().writeValue(response.getOutputStream(),
                        Map.of("error_message", "Invalid token: " + exception.getMessage()));
                return;
            }
            filterChain.doFilter(request, response);
        } else {
            response.setStatus(FORBIDDEN.value());
            response.setContentType(MediaType.APPLICATION_JSON_VALUE);
            new ObjectMapper().writeValue(response.getOutputStream(),
                    Map.of("error_message", "Access token is missing"));
        }
    }

    private JwtUtils() { }
}
