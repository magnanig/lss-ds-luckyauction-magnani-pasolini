/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.gateway.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import it.unibo.luckyauction.movement.adapter.TransactionWeb;
import it.unibo.luckyauction.spring.util.api.TransactionWebDeserializer;
import it.unibo.luckyauction.user.adapter.UserAuthenticationWeb;
import org.springframework.http.MediaType;
import org.springframework.http.codec.json.Jackson2JsonDecoder;
import org.springframework.web.reactive.function.client.ExchangeStrategies;
import org.springframework.web.reactive.function.client.WebClient;

import java.time.Duration;
import java.util.Optional;

import static it.unibo.luckyauction.configuration.HostsConstants.AUCTIONS_API;
import static it.unibo.luckyauction.configuration.HostsConstants.AUCTIONS_HISTORY_API;
import static it.unibo.luckyauction.configuration.HostsConstants.AUTOMATIONS_API;
import static it.unibo.luckyauction.configuration.HostsConstants.MOVEMENTS_API;
import static it.unibo.luckyauction.configuration.HostsConstants.USERS_API;
import static it.unibo.luckyauction.util.NumericalConstants.BYTES_IN_KB;

@SuppressWarnings("checkstyle:JavadocVariable")
public final class HttpRequestUtils {

    public static final Duration TIMEOUT = Duration.ofSeconds(10);

    public static Optional<UserAuthenticationWeb> findUserAuthenticationByUsername(final String username) {
        return WebClient.create(USERS_API)
                .get()
                .uri(uri -> uri.path("/authentication").queryParam("username", username).build())
                .retrieve()
                .bodyToMono(UserAuthenticationWeb.class)
                .blockOptional(TIMEOUT);
    }

    public static WebClient usersWebClient() {
        return createWebClient(USERS_API);
    }

    public static WebClient movementsWebClient() {
        final ObjectMapper objectMapper = new ObjectMapper();
        final SimpleModule module = new SimpleModule();
        module.addDeserializer(TransactionWeb.class, new TransactionWebDeserializer());
        objectMapper.registerModule(module);
        return createWebClient(MOVEMENTS_API, ExchangeStrategies.builder()
                .codecs(configurator -> {
                    final int limitSizeMb = 20;
                    configurator.defaultCodecs().maxInMemorySize(limitSizeMb * BYTES_IN_KB * BYTES_IN_KB);
                    configurator.defaultCodecs().jackson2JsonDecoder(
                            new Jackson2JsonDecoder(objectMapper, new MediaType[]{ MediaType.APPLICATION_JSON })
                    );
                }).build()
        );
    }

    public static WebClient auctionsWebClient() {
        return createWebClient(AUCTIONS_API, 100); // more space, since auctions contain images
    }

    public static WebClient auctionsHistoryWebClient() {
        return createWebClient(AUCTIONS_HISTORY_API);
    }

    public static WebClient auctionsAutomationsWebClient() {
        return createWebClient(AUTOMATIONS_API);
    }

    private static WebClient createWebClient(final String baseUrl, final int limitSizeMb) {
        final int size = limitSizeMb * BYTES_IN_KB * BYTES_IN_KB;
        final ExchangeStrategies strategies = ExchangeStrategies.builder()
                .codecs(codecs -> codecs.defaultCodecs().maxInMemorySize(size))
                .build();
        return createWebClient(baseUrl, strategies);
    }

    private static WebClient createWebClient(final String baseUrl, final ExchangeStrategies strategies) {
        return WebClient.builder()
                .baseUrl(baseUrl)
                .exchangeStrategies(strategies)
                .build();
    }

    private static WebClient createWebClient(final String baseUrl) {
        return createWebClient(baseUrl, 10);
    }

    private HttpRequestUtils() { }
}
