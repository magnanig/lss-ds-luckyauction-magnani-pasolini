/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.gateway.application;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import it.unibo.luckyauction.automation.adapter.AuctionAutomationWeb;
import it.unibo.luckyauction.automation.adapter.UserAuctionsAutomationsWeb;
import it.unibo.luckyauction.gateway.util.HttpRequestUtils;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.Optional;

import static it.unibo.luckyauction.gateway.util.HttpRequestUtils.TIMEOUT;
import static it.unibo.luckyauction.spring.util.http.WebClientUtils.checkErrors;

@SuppressWarnings("PMD.AvoidDuplicateLiterals")
@RestController @RequestMapping("/api/auctions/automations")
public class GatewayAuctionAutomationRestController {

    private static final String USERNAME_FIELD = "username";
    private static final String TOKEN_FIELD = "bearerAuth";

    private final WebClient webClient = HttpRequestUtils.auctionsAutomationsWebClient();

    /**
     * Find user auctions automations for the user making the request.
     * @return  the corresponding user auctions automations, if exists
     */
    @GetMapping
    @Operation(security = @SecurityRequirement(name = TOKEN_FIELD))
    public Optional<UserAuctionsAutomationsWeb> findUserAuctionsAutomationsByUsername(
            final Authentication authentication) {
        return webClient
                .get()
                .uri(uri -> uri
                        .queryParam(USERNAME_FIELD, authentication.getName()).build())
                .retrieve()
                .bodyToMono(UserAuctionsAutomationsWeb.class)
                .blockOptional(TIMEOUT);
    }

    /**
     * Add an auction automation for the user making the request.
     * @param auctionAutomation the auction automation to add
     */
    @PutMapping
    @Operation(security = @SecurityRequirement(name = TOKEN_FIELD))
    public AuctionAutomationWeb addAuctionAutomation(@RequestBody final AuctionAutomationWeb auctionAutomation,
                                                     final Authentication authentication) {
        return checkErrors(webClient
                .put()
                .uri(uri -> uri.queryParam(USERNAME_FIELD, authentication.getName()).build())
                .body(Mono.just(auctionAutomation), AuctionAutomationWeb.class)
                .retrieve())
                .bodyToMono(AuctionAutomationWeb.class)
                .block(TIMEOUT);
    }

    /**
     * Find the auction automation by its auction for the user making the request.
     * @param auctionId the desired auction id
     * @return          the auction automation with specified auction id and user, if any
     */
    @GetMapping("/{auctionId}")
    @Operation(security = @SecurityRequirement(name = TOKEN_FIELD))
    public Optional<AuctionAutomationWeb> findAutomationForAuction(@PathVariable final String auctionId,
                                                                   final Authentication authentication) {
        return webClient
                .get()
                .uri(uri -> uri.path("/{auctionId}")
                        .queryParam(USERNAME_FIELD, authentication.getName())
                        .build(auctionId))
                .retrieve()
                .bodyToMono(AuctionAutomationWeb.class)
                .blockOptional(TIMEOUT);
    }

    /**
     * Sets the auction automation "enabled" field, for the user making the request.
     * @param auctionId the id of the automated auction
     * @param active    the status to be set
     */
    @PutMapping("/{auctionId}")
    @Operation(security = @SecurityRequirement(name = TOKEN_FIELD))
    public void setAutomationEnableStatus(@PathVariable final String auctionId, @RequestParam final boolean active,
                                final Authentication authentication) {
        checkErrors(webClient
                .put()
                .uri(uri -> uri.path("/{auctionId}")
                        .queryParam(USERNAME_FIELD, authentication.getName())
                        .queryParam("active", active)
                        .build(auctionId))
                .retrieve())
                .bodyToMono(Void.class)
                .block(TIMEOUT);
    }

    /**
     * Delete a user auctions automations of the user making the request.
     * @return  the just deleted user auctions automations, if existed
     */
    @DeleteMapping("/{auctionId}")
    @Operation(security = @SecurityRequirement(name = TOKEN_FIELD))
    public Optional<UserAuctionsAutomationsWeb> removeAuctionAutomation(@PathVariable final String auctionId,
                                                                        final Authentication authentication) {
        return webClient
                .delete()
                .uri(uri -> uri.path("/{auctionId}")
                        .queryParam(USERNAME_FIELD, authentication.getName())
                        .build(auctionId))
                .retrieve()
                .bodyToMono(UserAuctionsAutomationsWeb.class)
                .blockOptional(TIMEOUT);
    }
}
