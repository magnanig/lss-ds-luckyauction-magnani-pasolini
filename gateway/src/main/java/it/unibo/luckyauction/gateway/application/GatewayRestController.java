/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.gateway.application;

import com.auth0.jwt.interfaces.DecodedJWT;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import it.unibo.luckyauction.gateway.security.BadTokenException;
import it.unibo.luckyauction.gateway.security.JwtTokenAuthenticationFilter;
import it.unibo.luckyauction.gateway.util.HttpRequestUtils;
import it.unibo.luckyauction.gateway.util.JwtUtils;
import it.unibo.luckyauction.user.adapter.UserAuthenticationWeb;
import it.unibo.luckyauction.user.domain.entity.UserAuthentication;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.HttpStatus.FORBIDDEN;

@SuppressWarnings("PMD.AvoidDuplicateLiterals")
@RestController @RequestMapping("/api")
public class GatewayRestController {

    private static final String TOKEN_FIELD = "bearerAuth";

    /**
     * Generate a new access token based on the user's refresh token. If the refresh token is valid,
     * the new access token is placed in the body of the response. Otherwise, if the refresh token
     * is invalid, a 403 error is returned, forcing the user to log in again.
     * @param request    the request
     * @param response   the response
     */
    @CrossOrigin(origins = "*")
    @GetMapping("/token/refresh")
    @Operation(security = @SecurityRequirement(name = TOKEN_FIELD)) // responses = { @ApiResponse(responseCode = "401") }
    public void refreshToken(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        final String authorizationHeader = request.getHeader(AUTHORIZATION);
        if (authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
            try {
                final String refreshToken = authorizationHeader.substring("Bearer ".length());
                final DecodedJWT decodedJWT = JwtUtils.decodeJWT(refreshToken);
                final String username = decodedJWT.getSubject(); // the subject is the username
                final UserAuthentication userAuthentication = HttpRequestUtils.findUserAuthenticationByUsername(username)
                        .map(UserAuthenticationWeb::toDomain)
                        .orElseThrow(() -> new UsernameNotFoundException("Username " + username + " not found"));
                final String accessToken = JwtUtils.generateAccessToken(userAuthentication.getUsername(),
                        userAuthentication.getRoles().stream().map(Enum::toString).collect(Collectors.toList()));
                final Map<String, String> tokens = new HashMap<>();
                tokens.put("access_token", accessToken);
                tokens.put("refresh_token", refreshToken);
                // return both tokens in json format
                response.setContentType(MediaType.APPLICATION_JSON_VALUE);
                new ObjectMapper().writeValue(response.getOutputStream(), tokens);
            } catch (Exception exception) {
                response.setHeader("error", exception.getMessage());
                response.setStatus(FORBIDDEN.value());
                response.setContentType(MediaType.APPLICATION_JSON_VALUE);
                new ObjectMapper().writeValue(response.getOutputStream(),
                        Map.of("error_message", exception.getMessage()));
            }
        } else {
            throw new BadTokenException();
        }
    }

    /**
     * A dummy method to allow login OpenApi generation.
     * @param credentials   the user credentials (i.e. username and password)
     */
    @CrossOrigin(origins = "*")
    @PostMapping("/login")
    public void login(@RequestBody final JwtTokenAuthenticationFilter.Credentials credentials) { }

}
