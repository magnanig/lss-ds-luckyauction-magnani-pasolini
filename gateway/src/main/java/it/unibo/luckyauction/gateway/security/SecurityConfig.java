package it.unibo.luckyauction.gateway.security;

import it.unibo.luckyauction.gateway.util.HttpRequestUtils;
import it.unibo.luckyauction.user.adapter.UserAuthenticationWeb;
import it.unibo.luckyauction.user.domain.valueobject.Role;
import it.unibo.luckyauction.user.domain.entity.UserAuthentication;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

@Configuration @EnableWebSecurity @RequiredArgsConstructor
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private final PasswordEncoder passwordEncoder;

    /**
     * {@inheritDoc}
     */
    @Override
    protected void configure(final AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(username -> {
            final UserAuthentication userAuthentication =
                    HttpRequestUtils.findUserAuthenticationByUsername(username)
                            .map(UserAuthenticationWeb::toDomain)
                            .orElseThrow(() -> new UsernameNotFoundException("Username " + username + " not found"));
            final Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
            userAuthentication.getRoles().forEach(role ->
                    authorities.add(new SimpleGrantedAuthority(role.toString())));
            return new User(userAuthentication.getUsername(), userAuthentication.getEncryptedPassword(), authorities);
        }).passwordEncoder(passwordEncoder);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        final JwtTokenAuthenticationFilter customAuthenticationFilter =
                new JwtTokenAuthenticationFilter(authenticationManagerBean());
        customAuthenticationFilter.setFilterProcessesUrl("/api/login");

        http.csrf().disable();
        http.cors();
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        http.authorizeRequests().antMatchers("/api/login/**").permitAll();
        http.authorizeRequests().antMatchers("/api/users/**", "/api/movements/**",
                        "/api/auctions/**", "/api/auctions/histories/**", "/api/auctions/automations/**")
                .hasAuthority(Role.USER.toString());

        http.authorizeRequests().anyRequest().authenticated();
        http.addFilter(customAuthenticationFilter);
        http.addFilterBefore(new JwtTokenAuthorizationFilter(), UsernamePasswordAuthenticationFilter.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void configure(final WebSecurity web) {
        web.ignoring().antMatchers(HttpMethod.GET, "/api/auctions/active");
        web.ignoring().antMatchers(HttpMethod.GET, "/api/token/refresh");
        web.ignoring().antMatchers(HttpMethod.POST, "/api/users/signup");
        web.ignoring().antMatchers(HttpMethod.GET, "/swagger-ui/**");
        web.ignoring().antMatchers(HttpMethod.GET, "/v3/api-docs/**");
    }

    /**
     * {@inheritDoc}
     */
    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        final CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowedOrigins(List.of("*"));
        configuration.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "PATCH", "DELETE", "OPTIONS"));
        configuration.setAllowedHeaders(Arrays.asList("authorization", "content-type", "x-auth-token"));
        final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }
}
