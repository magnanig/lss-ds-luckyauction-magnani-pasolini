/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.gateway.application;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import it.unibo.luckyauction.gateway.util.HttpRequestUtils;
import it.unibo.luckyauction.user.adapter.NewUserWeb;
import it.unibo.luckyauction.user.adapter.UserWeb;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.Optional;

import static it.unibo.luckyauction.gateway.util.HttpRequestUtils.TIMEOUT;
import static it.unibo.luckyauction.spring.util.http.WebClientUtils.checkErrors;

@RestController @RequestMapping("/api/users")
public class GatewayUserRestController {

    private static final String TOKEN_FIELD = "bearerAuth";

    private final WebClient webClient = HttpRequestUtils.usersWebClient();

    /**
     * Create a new user.
     * @param newUser   the user to be created
     * @return          the just created user
     */
    @CrossOrigin(origins = "*") @PostMapping("/signup")
    public UserWeb createUser(@RequestBody final NewUserWeb newUser) {
        return checkErrors(webClient
                .post()
                .body(Mono.just(newUser), NewUserWeb.class)
                .retrieve())
                .bodyToMono(UserWeb.class)
                .block(TIMEOUT);
    }

    /**
     * Get the user making the request.
     * @return   the corresponding user
     */
    @GetMapping("/username")
    @Operation(security = @SecurityRequirement(name = TOKEN_FIELD))
    public Optional<UserWeb> findUserByUsername(final Authentication authentication) {
        return webClient
                .get()
                .uri(uri -> uri.path("/username")
                        .queryParam("username", authentication.getName()).build())
                .retrieve()
                .bodyToMono(UserWeb.class)
                .blockOptional(TIMEOUT);
    }
}
