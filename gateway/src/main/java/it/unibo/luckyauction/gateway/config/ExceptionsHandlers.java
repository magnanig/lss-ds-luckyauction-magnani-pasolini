/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.gateway.config;

import it.unibo.luckyauction.spring.util.http.ErrorResponse;
import it.unibo.luckyauction.spring.util.http.HttpRequestException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * This class contains auction exception handlers, which specify
 * how the Spring server should respond to the client when an exception is thrown.
 */
@ControllerAdvice
public class ExceptionsHandlers {

    /**
     * Handler for a {@link HttpRequestException}, i.e. an exception occurring
     * while forwarding a request to another microservice.
     * @param ex    the thrown exception reference
     * @return      the response with the status code got while forwarding the
     *              request, and the error message in the body
     */
    @ExceptionHandler(HttpRequestException.class)
    public ResponseEntity<String> handleHttpRequestException(final HttpRequestException ex) {
        final ErrorResponse errorResponse = ex.getErrorResponse();
        return ResponseEntity
                .status(errorResponse.httpResponseCode())
                .body(errorResponse.message());
    }

    /**
     * Handler for a {@link AccessDeniedException}, i.e. if the user making the request
     * does not have the required permissions.
     * @param ex    the thrown exception reference
     * @return      the response with the status code got while forwarding the
     *              request, and the error message in the body
     */
    @ExceptionHandler(AccessDeniedException.class)
    public final ResponseEntity<String> handleAccessDeniedException(final AccessDeniedException ex) {
        return ResponseEntity
                .status(HttpStatus.FORBIDDEN)
                .body(ex.getMessage());
    }

    /**
     * Handler for a generic {@link Exception}. This handler will be triggered
     * only if none of the others matches with the thrown exception.
     * @param ex    the thrown exception reference
     * @return      the response with status code 500, containing the error
     *              message in the body
     */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<String> handleException(final Exception ex) {
        ex.printStackTrace();
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(ex.getMessage());
    }
}
