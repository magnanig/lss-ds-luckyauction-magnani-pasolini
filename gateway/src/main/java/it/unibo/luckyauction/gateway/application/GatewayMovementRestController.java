/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.gateway.application;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import it.unibo.luckyauction.gateway.util.HttpRequestUtils;
import it.unibo.luckyauction.movement.adapter.ChargingFromCardTransactionWeb;
import it.unibo.luckyauction.movement.adapter.FreezingWeb;
import it.unibo.luckyauction.movement.adapter.MovementWeb;
import it.unibo.luckyauction.movement.adapter.MovementsHistoryWeb;
import it.unibo.luckyauction.movement.adapter.TransactionWeb;
import it.unibo.luckyauction.movement.adapter.WithdrawalTransactionWeb;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import static it.unibo.luckyauction.gateway.util.HttpRequestUtils.TIMEOUT;
import static it.unibo.luckyauction.spring.util.http.WebClientUtils.checkErrors;

@RestController @RequestMapping("/api/movements")
public class GatewayMovementRestController {

    private static final String USERNAME_FIELD = "username";
    private static final String TOKEN_FIELD = "bearerAuth";

    private final WebClient webClient = HttpRequestUtils.movementsWebClient();

    /**
     * Find a MovementsHistory for the user making the request.
     * @return  the corresponding MovementsHistory, if any
     */
    @GetMapping("/histories")
    @Operation(security = @SecurityRequirement(name = TOKEN_FIELD))
    public Optional<MovementsHistoryWeb> findMovementsHistory(final Authentication authentication) {
        return webClient
                .get()
                .uri(uri -> uri.path("/histories")
                        .queryParam(USERNAME_FIELD, authentication.getName()).build())
                .retrieve()
                .bodyToMono(MovementsHistoryWeb.class)
                .blockOptional(TIMEOUT);
    }

    /**
     * Find a movement for the user making the request.
     * @param movementId    the id of the desired movement
     * @return              the corresponding movement, if exists
     */
    @GetMapping("/{movementId}")
    @Operation(security = @SecurityRequirement(name = TOKEN_FIELD))
    public Optional<MovementWeb> findMovementById(@PathVariable final String movementId,
                                                  final Authentication authentication) {
        return webClient
                .get()
                .uri(uri -> uri.path("/" + movementId)
                        .queryParam(USERNAME_FIELD, authentication.getName()).build())
                .retrieve()
                .bodyToMono(MovementWeb.class)
                .blockOptional(TIMEOUT);
    }

    /**
     * Get all freezes associated to the user making the request.
     * @return  the freezes of the specified user
     */
    @GetMapping("/freezes")
    @Operation(security = @SecurityRequirement(name = TOKEN_FIELD))
    public List<FreezingWeb> getFreezes(@RequestParam final Authentication authentication) {
        return webClient
                .get()
                .uri(uri -> uri.path("/freezes")
                        .queryParam(USERNAME_FIELD, authentication.getName()).build())
                .exchangeToMono(response -> response
                        .bodyToMono(new ParameterizedTypeReference<List<FreezingWeb>>() { }))
                .block(TIMEOUT);
    }

    /**
     * Get all transactions associated to the user making the request.
     * @return  the transactions of the specified user
     */
    @GetMapping("/transactions")
    @Operation(security = @SecurityRequirement(name = TOKEN_FIELD))
    public List<TransactionWeb> getTransactions(final Authentication authentication) {
        return webClient
                .get()
                .uri(uri -> uri.path("/transactions")
                        .queryParam(USERNAME_FIELD, authentication.getName()).build())
                .exchangeToMono(response -> response
                        .bodyToMono(new ParameterizedTypeReference<List<TransactionWeb>>() { }))
                .block(TIMEOUT);
    }

    /**
     * Recharge budget of the user making the request.
     * @param amount        the (positive) amount of the recharge
     * @param description   the optional description associated with the transaction
     * @return              the just created charging from card transaction
     */
    @PostMapping("/transactions/recharge")
    @Operation(security = @SecurityRequirement(name = TOKEN_FIELD))
    public ChargingFromCardTransactionWeb rechargeBudget(@RequestParam final BigDecimal amount,
                                                         @RequestParam(required = false) final String description,
                                                         final Authentication authentication) {
        return checkErrors(webClient
                .post()
                .uri(uri -> uri.path("/transactions/recharge")
                        .queryParam("amount", amount)
                        .queryParamIfPresent("description", Optional.ofNullable(description))
                        .queryParam(USERNAME_FIELD, authentication.getName()).build())
                .retrieve())
                .bodyToMono(ChargingFromCardTransactionWeb.class)
                .block(TIMEOUT);
    }

    /**
     * Withdraw money from the user making the request.
     * @param amount    the (positive) amount to be withdrawn
     * @return          the just created withdrawal transaction
     */
    @PostMapping("/transactions/withdrawal")
    @Operation(security = @SecurityRequirement(name = TOKEN_FIELD))
    public WithdrawalTransactionWeb withdrawBudget(@RequestParam final BigDecimal amount,
                                                   final Authentication authentication) {
        return checkErrors(webClient
                .post()
                .uri(uri -> uri.path("/transactions/withdrawal")
                        .queryParam("amount", amount)
                        .queryParam(USERNAME_FIELD, authentication.getName()).build())
                .retrieve())
                .bodyToMono(WithdrawalTransactionWeb.class)
                .block(TIMEOUT);
    }
}
