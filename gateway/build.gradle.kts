plugins {
    id("org.springframework.boot") version "2.6.6"
}

apply(plugin = "org.springframework.boot")

rootProject.extra["mainClassProp"] = "it.unibo.luckyauction.gateway.application.GatewayMain"

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-web:2.6.7")
    implementation("org.springframework.boot:spring-boot-starter-security:2.6.7")
    implementation("org.springframework:spring-webflux:5.3.19")
    implementation("io.projectreactor.netty:reactor-netty-http:1.0.18")
    implementation("io.netty:netty-resolver-dns-native-macos:4.1.74.Final:osx-aarch_64")
    runtimeOnly("org.springdoc:springdoc-openapi-ui:1.6.9")
    implementation("io.swagger.core.v3:swagger-annotations:2.2.0")

    compileOnly("org.projectlombok:lombok:1.18.24")
    annotationProcessor("org.projectlombok:lombok:1.18.24")

    implementation("com.auth0:java-jwt:3.19.1")

    implementation(project(":configuration"))

    implementation(project(":utils"))
    implementation(project(":utils:spring-utils"))
    implementation(project(":sha256encoder"))

    implementation(project(":users:domain"))

    implementation(project(":users:adapter"))
    implementation(project(":movements:adapter"))
    implementation(project(":auctions:adapter"))
    implementation(project(":automations:adapter"))
}

// the version of jar task, for spring
tasks.bootJar {
    mainClass.set("it.unibo.luckyauction.gateway.application.GatewayMain")
    archiveBaseName.set("luckyauction-gateway-spring-application")
}