dependencies {
    implementation(project(":configuration"))
    implementation(project(":utils"))
    implementation(project(":sha256encoder"))
    implementation(project(":uuid-generator"))

    implementation(project(":users:domain"))
    implementation(project(":users:usecase"))
    implementation(project(":users:adapter"))
    implementation(project(":users:mongo-repository"))

    implementation(project(":movements:adapter"))
    implementation(project(":auctions:adapter"))
    implementation(project(":automations:adapter"))

    // this will add following dependencies: test --> users.domain.testFixtures
    testImplementation(testFixtures(project(":users:domain")))
    testImplementation(project(":utils:spring-utils"))
}