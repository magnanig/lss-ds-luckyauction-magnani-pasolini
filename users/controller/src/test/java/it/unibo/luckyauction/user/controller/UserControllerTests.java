/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.user.controller;

import it.unibo.luckyauction.spring.util.api.SpringAuctionAutomationApi;
import it.unibo.luckyauction.spring.util.api.SpringAuctionsHistoryApi;
import it.unibo.luckyauction.spring.util.api.SpringMovementApi;
import it.unibo.luckyauction.user.controller.config.UserConfig;
import it.unibo.luckyauction.user.domain.UserTestUtils;
import it.unibo.luckyauction.user.domain.entity.User;
import it.unibo.luckyauction.user.domain.valueobject.Role;
import it.unibo.luckyauction.user.domain.entity.UserAuthentication;
import it.unibo.luckyauction.user.usecase.exception.UserAlreadyExistsException;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.List;
import java.util.Set;

import static it.unibo.luckyauction.user.domain.UserTestUtils.FIRST_USERNAME;
import static it.unibo.luckyauction.user.domain.UserTestUtils.PASSWORD;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class UserControllerTests {
    private final UserController userController = new UserConfig().userController(new SpringMovementApi(),
            new SpringAuctionsHistoryApi(), new SpringAuctionAutomationApi());
    private User user;

    @BeforeAll
    void createUser() {
        if (userController.findByUsername(FIRST_USERNAME).isPresent()) {
            userController.deleteByUsername(FIRST_USERNAME);
        }
        user = userController.create(UserTestUtils.createNewUser(FIRST_USERNAME), PASSWORD);

        assertEquals(FIRST_USERNAME, user.getUsername());
        assertEquals(UserTestUtils.ANAGRAPHIC.get(), user.getAnagraphic());
        assertEquals(UserTestUtils.CONTACTS.get(), user.getContacts());
    }

    @AfterAll
    void deleteUser() {
        userController.deleteByUsername(FIRST_USERNAME).ifPresentOrElse(user ->
                assertEquals(FIRST_USERNAME, user.getUsername()), Assertions::fail);
    }

    @Test
    void userAlreadyExistExceptionTest() {
        final Exception exception = assertThrows(UserAlreadyExistsException.class,
                () -> userController.create(UserTestUtils.createNewUser(FIRST_USERNAME), PASSWORD));
        assertTrue(exception.getMessage().contains("already exists"));
    }

    @Test
    void findAllUserTest() {
        final List<User> users = userController.getAllUsers();

        assertTrue(users.contains(user));
        assertEquals(1, users.size());
    }

    @Test
    void findUserTest() {
        assertEquals(user, userController.findById(user.getUserId()).orElseThrow());
        assertEquals(user, userController.findByUsername(FIRST_USERNAME).orElseThrow());
    }

    @Test
    void findUserAuthenticationByUsernameTest() {
        final UserAuthentication userAuthentication = userController
                .findUserAuthenticationByUsername(FIRST_USERNAME).orElseThrow();

        assertEquals(FIRST_USERNAME, userAuthentication.getUsername());
        assertEquals(Set.of(Role.USER), userAuthentication.getRoles());
    }
}
