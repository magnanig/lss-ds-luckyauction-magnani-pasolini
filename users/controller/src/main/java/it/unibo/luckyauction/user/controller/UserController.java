/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.user.controller;

import it.unibo.luckyauction.auction.adapter.api.AuctionsHistoryApi;
import it.unibo.luckyauction.automation.adapter.api.AuctionAutomationApi;
import it.unibo.luckyauction.movement.adapter.api.MovementApi;
import it.unibo.luckyauction.user.domain.entity.User;
import it.unibo.luckyauction.user.domain.entity.UserAuthentication;
import it.unibo.luckyauction.user.usecase.CreateUser;
import it.unibo.luckyauction.user.usecase.DeleteUser;
import it.unibo.luckyauction.user.usecase.FindUser;
import it.unibo.luckyauction.user.usecase.FindUserAuthentication;
import it.unibo.luckyauction.user.usecase.exception.UserAlreadyExistsException;

import java.util.List;
import java.util.Optional;

/**
 * The user-controller of the application, capable of handle all requests
 * involving users, i.e. login, creation, deletion, search, and budget management.
 */
public class UserController {

    private final CreateUser createUser;
    private final FindUser findUser;
    private final FindUserAuthentication findUserAuthentication;
    private final DeleteUser deleteUser;
    private final MovementApi movementApi;
    private final AuctionsHistoryApi auctionsHistoryApi;
    private final AuctionAutomationApi auctionAutomationApi;

    /**
     * Create a new user controller.
     * @param createUser                the use case to createAuctionsHistory a new user
     * @param findUser                  the use case to find a user
     * @param findUserAuthentication    the use case to find a user authentication
     * @param deleteUser                the use case to delete a user
     * @param movementApi               the api to interact with movements
     * @param auctionsHistoryApi        the api to interact with auctions histories
     * @param auctionAutomationApi      the api to interact with auctions automations
     */
    public UserController(final CreateUser createUser, final FindUser findUser,
                          final FindUserAuthentication findUserAuthentication, final DeleteUser deleteUser,
                          final MovementApi movementApi, final AuctionsHistoryApi auctionsHistoryApi,
                          final AuctionAutomationApi auctionAutomationApi) {
        this.createUser = createUser;
        this.findUser = findUser;
        this.findUserAuthentication = findUserAuthentication;
        this.deleteUser = deleteUser;
        this.movementApi = movementApi;
        this.auctionsHistoryApi = auctionsHistoryApi;
        this.auctionAutomationApi = auctionAutomationApi;
    }

    /**
     * Get the list of all users registered in the application.
     * @return      the list of all users
     */
    public List<User> getAllUsers() {
        return findUser.getAllUsers();
    }

    /**
     * Find a user by their id.
     * @param userId    the desired user id
     * @return          the corresponding user, if exists
     */
    public Optional<User> findById(final String userId) {
        return findUser.findById(userId);
    }

    /**
     * Find a user by their username.
     * @param username  the desired username
     * @return          the corresponding user, if exists
     */
    public Optional<User> findByUsername(final String username) {
        return findUser.findByUsername(username);
    }

    /**
     * Find a user authentication by its username.
     * @param username  the desired username
     * @return          the corresponding user authentication, if exists
     */
    public Optional<UserAuthentication> findUserAuthenticationByUsername(final String username) {
        return findUserAuthentication.findByUsername(username);
    }

    /**
     * Create a new user starting from the specified user object. The user auctions
     * history and movements history will be also created.
     * @param user                          the user to createAuctionsHistory
     * @return                              the just created user, with identifiers
     *                                      properly set and encoded password
     * @throws UserAlreadyExistsException   if a user with the specified username
     *                                      already exists
     */
    public User create(final User user, final String password) throws UserAlreadyExistsException {
        final User newUser = createUser.create(user, password);
        movementApi.createMovementsHistory(newUser.getMovementsHistoryId(), newUser.getUsername());
        auctionsHistoryApi.createAuctionsHistory(newUser.getAuctionsHistoryId(), newUser.getUsername());
        auctionAutomationApi.createUserAuctionAutomations(user.getUsername());
        return newUser;
    }

    /**
     * Delete a user by their username. This will also remove their auctions history
     * and movements history.
     * @param username  the desired username
     * @return          the just deleted user, if existed
     */
    public Optional<User> deleteByUsername(final String username) {
        return deleteUser.deleteByUsername(username).stream()
                .peek(user -> movementApi.deleteMovementsHistory(user.getUsername()))
                .peek(user -> auctionsHistoryApi.deleteByUsername(user.getUsername()))
                .peek(user -> auctionAutomationApi.deleteByUsername(user.getUsername()))
                .findFirst();
    }

}

