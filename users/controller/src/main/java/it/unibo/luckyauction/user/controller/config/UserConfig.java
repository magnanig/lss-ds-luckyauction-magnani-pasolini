/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.user.controller.config;

import it.unibo.luckyauction.auction.adapter.api.AuctionsHistoryApi;
import it.unibo.luckyauction.automation.adapter.api.AuctionAutomationApi;
import it.unibo.luckyauction.encoder.Sha256CardEncoder;
import it.unibo.luckyauction.encoder.Sha256PasswordEncoder;
import it.unibo.luckyauction.movement.adapter.api.MovementApi;
import it.unibo.luckyauction.user.controller.UserController;
import it.unibo.luckyauction.user.repository.UserAuthenticationMongoRepository;
import it.unibo.luckyauction.user.repository.UserMongoRepository;
import it.unibo.luckyauction.user.usecase.CreateUser;
import it.unibo.luckyauction.user.usecase.DeleteUser;
import it.unibo.luckyauction.user.usecase.FindUser;
import it.unibo.luckyauction.user.usecase.FindUserAuthentication;
import it.unibo.luckyauction.util.port.CardEncoder;
import it.unibo.luckyauction.util.port.IdGenerator;
import it.unibo.luckyauction.util.port.PasswordEncoder;
import it.unibo.luckyauction.user.usecase.port.UserAuthenticationRepository;
import it.unibo.luckyauction.user.usecase.port.UserRepository;
import it.unibo.luckyauction.uuid.UUIdGenerator;

import static it.unibo.luckyauction.configuration.ConfigurationConstants.TEST_MODE;

public class UserConfig {
    private final UserRepository userRepository;
    private final UserAuthenticationRepository userAuthenticationRepository;

    private final IdGenerator userIdGenerator = new UUIdGenerator();
    private final IdGenerator auctionsHistoryIdGenerator = new UUIdGenerator();
    private final IdGenerator movementsHistoryIdGenerator = new UUIdGenerator();
    private final PasswordEncoder passwordEncoder = new Sha256PasswordEncoder();
    private final CardEncoder cardEncoder = new Sha256CardEncoder();

    public UserConfig() {
        this.userRepository = TEST_MODE
                ? UserMongoRepository.testRepository()
                : UserMongoRepository.defaultRepository();
        this.userAuthenticationRepository = TEST_MODE
                ? UserAuthenticationMongoRepository.testRepository()
                : UserAuthenticationMongoRepository.defaultRepository();
    }

    /**
     * Build the user controller of the application, capable of orchestrating users.
     * See {@link UserController} for more details.
     * @return      the just built user controller
     */
    public UserController userController(final MovementApi movementApi, final AuctionsHistoryApi auctionsHistoryApi,
                                         final AuctionAutomationApi auctionAutomationApi) {
        return new UserController(createUser(), findUser(), findUserAuthentication(), deleteUser(), movementApi,
                auctionsHistoryApi, auctionAutomationApi);
    }

    private FindUser findUser() {
        return new FindUser(userRepository);
    }

    private FindUserAuthentication findUserAuthentication() {
        return new FindUserAuthentication(userAuthenticationRepository);
    }

    private CreateUser createUser() {
        return new CreateUser(userRepository, userAuthenticationRepository, userIdGenerator, auctionsHistoryIdGenerator,
                movementsHistoryIdGenerator, passwordEncoder, cardEncoder);
    }

    private DeleteUser deleteUser() {
        return new DeleteUser(userRepository, userAuthenticationRepository);
    }
}
