module lss.ds.luckyauction.magnani.pasolini.users.controller.main {
    requires lss.ds.luckyauction.magnani.pasolini.configuration.main;
    exports it.unibo.luckyauction.user.controller;
    exports it.unibo.luckyauction.user.controller.config;
    requires lss.ds.luckyauction.magnani.pasolini.utils.main;
    requires lss.ds.luckyauction.magnani.pasolini.users.domain.main;
    requires lss.ds.luckyauction.magnani.pasolini.users.usecase.main;
    requires lss.ds.luckyauction.magnani.pasolini.uuid.generator.main;
    requires lss.ds.luckyauction.magnani.pasolini.sha256encoder.main;
    requires lss.ds.luckyauction.magnani.pasolini.users.repository.mongo.main;
    requires lss.ds.luckyauction.magnani.pasolini.users.adapter.main;
    requires lss.ds.luckyauction.magnani.pasolini.movements.adapter.main;
    requires lss.ds.luckyauction.magnani.pasolini.auctions.adapter.main;
    requires lss.ds.luckyauction.magnani.pasolini.automations.adapter.main;
}