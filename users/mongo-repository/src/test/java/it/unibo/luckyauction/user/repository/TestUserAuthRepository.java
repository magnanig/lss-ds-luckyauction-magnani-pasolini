/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.user.repository;

import it.unibo.luckyauction.user.domain.entity.UserAuthentication;
import it.unibo.luckyauction.user.usecase.port.UserAuthenticationRepository;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TestUserAuthRepository {

    private static final String USERNAME = "test";
    private static final UserAuthenticationRepository REPOSITORY = UserAuthenticationMongoRepository.testRepository();
    private static final UserAuthentication USER_AUTH = new UserAuthentication(USERNAME, "password");

    @BeforeAll
    static void createUser() {
        if (REPOSITORY.findByUsername(USERNAME).isPresent()) {
            REPOSITORY.deleteByUsername(USERNAME);
        }
        assertEquals(Optional.empty(), REPOSITORY.findByUsername(USERNAME));
        REPOSITORY.create(USER_AUTH);
    }

    @Test
    void testFindByUsername() {
        assertEquals(Optional.of(USER_AUTH), REPOSITORY.findByUsername(USERNAME));
    }

    @AfterAll
    static void deleteUser() {
        REPOSITORY.deleteByUsername(USERNAME);
        assertEquals(Optional.empty(), REPOSITORY.findByUsername(USERNAME));
    }
}
