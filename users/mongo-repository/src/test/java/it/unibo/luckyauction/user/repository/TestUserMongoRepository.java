/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.user.repository;

import it.unibo.luckyauction.user.domain.UserTestUtils;
import it.unibo.luckyauction.user.domain.entity.User;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TestUserMongoRepository {

    private static final User FIRST_USER = UserTestUtils.createNewUser("0", "firstUser");
    private static final User SECOND_USER = UserTestUtils.createNewUser("1", "secondUser");
    private static final UserMongoRepository REPOSITORY = UserMongoRepository.testRepository();

    @BeforeAll
    static void createUsers() {
        createUser(FIRST_USER);
        createUser(SECOND_USER);
    }

    @AfterAll
    static void deleteUsers() {
        deleteUser(FIRST_USER.getUsername());
        deleteUser(SECOND_USER.getUsername());
    }

    @SuppressWarnings("PMD.JUnitTestsShouldIncludeAssert")
    @Test
    void testFindUser() {
        testFindUser(FIRST_USER);
        testFindUser(SECOND_USER);
    }

    @Test
    void testGetAllUsers() {
        assertEquals(List.of(FIRST_USER, SECOND_USER), REPOSITORY.getAllUsers());
    }

    private static void createUser(final User user) {
        if (REPOSITORY.findByUsername(user.getUsername()).isPresent()) {
            REPOSITORY.deleteByUsername(user.getUsername());
        }
        assertEquals(Optional.empty(), REPOSITORY.findByUsername(user.getUsername()));
        REPOSITORY.create(user);
        assertEquals(Optional.of(user), REPOSITORY.findByUsername(user.getUsername()));
    }

    private static void deleteUser(final String username) {
        REPOSITORY.deleteByUsername(username);
        assertEquals(Optional.empty(), REPOSITORY.findById(username));
    }

    private void testFindUser(final User user) {
        assertEquals(Optional.of(user), REPOSITORY.findByUsername(user.getUsername()));
        assertEquals(Optional.of(user), REPOSITORY.findById(user.getUserId()));
    }
}
