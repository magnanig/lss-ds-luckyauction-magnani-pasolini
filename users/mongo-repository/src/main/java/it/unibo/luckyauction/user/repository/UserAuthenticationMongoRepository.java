/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.user.repository;

import com.mongodb.client.MongoCollection;
import it.unibo.luckyauction.repository.util.MongoConnectionsConstants;
import it.unibo.luckyauction.repository.util.CommonsMongoOperations;
import it.unibo.luckyauction.repository.util.MongoDatabaseManager;
import it.unibo.luckyauction.user.adapter.UserAuthenticationWeb;
import it.unibo.luckyauction.user.domain.entity.UserAuthentication;
import it.unibo.luckyauction.user.usecase.port.UserAuthenticationRepository;

import java.util.Optional;

import static com.mongodb.client.model.Filters.eq;
import static it.unibo.luckyauction.repository.util.MongoConnectionsConstants.TEST_CONNECTION_STRING;
import static it.unibo.luckyauction.repository.util.MongoConnectionsConstants.TEST_DB_NAME;
import static it.unibo.luckyauction.repository.util.MongoConnectionsConstants.USERS_AUTH_COLLECTION_NAME;
import static it.unibo.luckyauction.repository.util.MongoConnectionsConstants.USERS_AUTH_CONNECTION_STRING;
import static it.unibo.luckyauction.repository.util.MongoConnectionsConstants.USERS_AUTH_DB_NAME;

public final class UserAuthenticationMongoRepository implements UserAuthenticationRepository {

    private final MongoCollection<UserAuthenticationWeb> usersAuthenticationWebMongoCollection;
    private final CommonsMongoOperations<UserAuthenticationWeb, UserAuthentication> commons;

    private UserAuthenticationMongoRepository(final String connectionString, final String databaseName, final String collectionName) {
        usersAuthenticationWebMongoCollection = new MongoDatabaseManager(connectionString, databaseName)
                .getCollection(collectionName, UserAuthenticationWeb.class);
        commons = new CommonsMongoOperations<>(usersAuthenticationWebMongoCollection, UserAuthenticationWeb::toDomain);
    }

    /**
     * Get the default repository of users authentication, using the connection string
     * and database name specified in the {@link MongoConnectionsConstants} class.
     * @return  the repository of users
     */
    public static UserAuthenticationMongoRepository defaultRepository() {
        return new UserAuthenticationMongoRepository(USERS_AUTH_CONNECTION_STRING, USERS_AUTH_DB_NAME,
                USERS_AUTH_COLLECTION_NAME);
    }

    /**
     * Get the repository of users authentication for testing, using the connection string
     * and database name specified in the {@link MongoConnectionsConstants} class.
     * @return  the test repository of users
     */
    public static UserAuthenticationMongoRepository testRepository() {
        return new UserAuthenticationMongoRepository(TEST_CONNECTION_STRING, TEST_DB_NAME, USERS_AUTH_COLLECTION_NAME);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void create(final UserAuthentication userAuthentication) {
        usersAuthenticationWebMongoCollection.insertOne(UserAuthenticationWeb.fromDomain(userAuthentication));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<UserAuthentication> findByUsername(final String username) {
        return commons.findOneByFilter(eq("username", username));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteByUsername(final String username) {
        commons.deleteOneByFilter(eq("username", username));
    }
}
