/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.user.repository;

import com.mongodb.client.MongoCollection;
import it.unibo.luckyauction.repository.util.MongoConnectionsConstants;
import it.unibo.luckyauction.repository.util.CommonsMongoOperations;
import it.unibo.luckyauction.repository.util.MongoDatabaseManager;
import it.unibo.luckyauction.user.adapter.UserWeb;
import it.unibo.luckyauction.user.domain.entity.User;
import it.unibo.luckyauction.user.usecase.port.UserRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.mongodb.client.model.Filters.eq;
import static it.unibo.luckyauction.repository.util.MongoConnectionsConstants.TEST_CONNECTION_STRING;
import static it.unibo.luckyauction.repository.util.MongoConnectionsConstants.TEST_DB_NAME;
import static it.unibo.luckyauction.repository.util.MongoConnectionsConstants.USERS_COLLECTION_NAME;
import static it.unibo.luckyauction.repository.util.MongoConnectionsConstants.USERS_CONNECTION_STRING;
import static it.unibo.luckyauction.repository.util.MongoConnectionsConstants.USERS_DB_NAME;

public final class UserMongoRepository implements UserRepository {

    private final MongoCollection<UserWeb> usersCollection;
    private final CommonsMongoOperations<UserWeb, User> commons;

    private UserMongoRepository(final String connectionString, final String databaseName, final String collectionName) {
        usersCollection = new MongoDatabaseManager(connectionString, databaseName)
                .getCollection(collectionName, UserWeb.class);
        commons = new CommonsMongoOperations<>(usersCollection, UserWeb::toDomainUser);
    }

    /**
     * Get the default repository of users, using the connection string and database name
     * specified in the {@link MongoConnectionsConstants} class.
     * @return  the repository of users
     */
    public static UserMongoRepository defaultRepository() {
        return new UserMongoRepository(USERS_CONNECTION_STRING, USERS_DB_NAME, USERS_COLLECTION_NAME);
    }

    /**
     * Get the repository of users for testing, using the connection string and database name
     * specified in the {@link MongoConnectionsConstants} class.
     * @return  the test repository of users
     */
    public static UserMongoRepository testRepository() {
        return new UserMongoRepository(TEST_CONNECTION_STRING, TEST_DB_NAME, USERS_COLLECTION_NAME);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public User create(final User user) {
        usersCollection.insertOne(UserWeb.fromDomainUser(user));
        return user;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<User> findById(final String id) {
        return commons.findOneByFilter(eq("userId", id));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<User> findByUsername(final String username) {
        return commons.findOneByFilter(eq("username", username));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<User> getAllUsers() {
        return usersCollection.find().map(UserWeb::toDomainUser).into(new ArrayList<>());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<User> deleteByUsername(final String username) {
        return commons.deleteOneByFilter(eq("username", username));
    }

}
