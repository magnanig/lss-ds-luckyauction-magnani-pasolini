module lss.ds.luckyauction.magnani.pasolini.users.repository.mongo.main {
    exports it.unibo.luckyauction.user.repository;
    requires lss.ds.luckyauction.magnani.pasolini.mongo.utils.main;
    requires lss.ds.luckyauction.magnani.pasolini.users.domain.main;
    requires lss.ds.luckyauction.magnani.pasolini.users.usecase.main;
    requires lss.ds.luckyauction.magnani.pasolini.users.adapter.main;
    requires mongo.java.driver;
}