dependencies {
    implementation(project(":mongo-utils"))
    implementation(project(":users:domain"))
    implementation(project(":users:usecase"))
    implementation(project(":users:adapter"))

    implementation("org.mongodb:mongo-java-driver:3.12.10")
    testImplementation(testFixtures(project(":users:domain")))
}