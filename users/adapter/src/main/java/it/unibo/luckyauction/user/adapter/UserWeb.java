/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.user.adapter;

import it.unibo.luckyauction.user.domain.entity.User;

/**
 * Represents the schema of a {@link User}. This is a (de)serializable
 * version with all getters and setters, to be used to send object
 * through the network.
 */
public final class UserWeb {
    private String userId;
    private String username;
    private AnagraphicWeb anagraphic;
    private ContactsWeb contacts;
    private HiddenPaymentCardWeb card;
    private String auctionsHistoryId;
    private String movementsHistoryId;

    public static UserWeb fromDomainUser(final User user) {
        final UserWeb userWeb = new UserWeb();
        userWeb.setUserId(user.getUserId());
        userWeb.setUsername(user.getUsername());
        userWeb.setAnagraphic(AnagraphicWeb.fromDomainAnagraphic(user.getAnagraphic()));
        userWeb.setContacts(ContactsWeb.fromDomainContacts(user.getContacts()));
        userWeb.setCard(HiddenPaymentCardWeb.fromDomainPaymentCard(user.getCard()));
        userWeb.setAuctionsHistoryId(user.getAuctionsHistoryId());
        userWeb.setMovementsHistoryId(user.getMovementsHistoryId());
        return userWeb;
    }

    public User toDomainUser() {
        return User.builder()
                .userId(userId)
                .username(username)
                .anagraphic(anagraphic.toDomainAnagraphic())
                .contacts(contacts.toDomainContacts())
                .card(card.toDomainPaymentCard())
                .auctionsHistoryId(auctionsHistoryId)
                .movementsHistoryId(movementsHistoryId)
                .build();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(final String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(final String username) {
        this.username = username;
    }

    public AnagraphicWeb getAnagraphic() {
        return anagraphic.copy();
    }

    public void setAnagraphic(final AnagraphicWeb anagraphic) {
        this.anagraphic = anagraphic.copy();
    }

    public ContactsWeb getContacts() {
        return contacts.copy();
    }

    public void setContacts(final ContactsWeb contacts) {
        this.contacts = contacts.copy();
    }

    public HiddenPaymentCardWeb getCard() {
        return card.copy();
    }

    public void setCard(final HiddenPaymentCardWeb card) {
        this.card = card.copy();
    }

    public String getAuctionsHistoryId() {
        return auctionsHistoryId;
    }

    public void setAuctionsHistoryId(final String auctionsHistoryId) {
        this.auctionsHistoryId = auctionsHistoryId;
    }

    public String getMovementsHistoryId() {
        return movementsHistoryId;
    }

    public void setMovementsHistoryId(final String movementsHistoryId) {
        this.movementsHistoryId = movementsHistoryId;
    }

}
