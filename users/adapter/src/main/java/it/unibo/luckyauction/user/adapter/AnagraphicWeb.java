/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.user.adapter;

import it.unibo.luckyauction.user.domain.valueobject.Anagraphic;
import it.unibo.luckyauction.util.CalendarUtils;

import java.util.Calendar;
import java.util.Date;

/**
 * Represents the schema of an {@link Anagraphic}. This is a (de)serializable
 * version with all getters and setters, to be used to send object
 * through the network.
 */
public final class AnagraphicWeb {
    private String firstName;
    private String lastName;
    private Date birthDate;
    private AddressWeb homeAddress;

    public static AnagraphicWeb fromDomainAnagraphic(final Anagraphic anagraphic) {
        final AnagraphicWeb anagraphicWeb = new AnagraphicWeb();
        anagraphicWeb.setFirstName(anagraphic.getFirstName());
        anagraphicWeb.setLastName(anagraphic.getLastName());
        anagraphicWeb.setBirthDate(anagraphic.getBirthDate().getTime());
        anagraphicWeb.setHomeAddress(AddressWeb.fromDomainAddress(anagraphic.getHomeAddress()));
        return anagraphicWeb;
    }

    public Anagraphic toDomainAnagraphic() {
        final Calendar domainBirthDate = Calendar.getInstance();
        domainBirthDate.setTimeInMillis(birthDate.getTime());
        return Anagraphic.builder()
                .firstName(firstName)
                .lastName(lastName)
                .birthDate(domainBirthDate)
                .homeAddress(homeAddress.toDomainAddress())
                .build();
    }

    public AnagraphicWeb copy() {
        final AnagraphicWeb anagraphicWeb = new AnagraphicWeb();
        anagraphicWeb.firstName = firstName;
        anagraphicWeb.lastName = lastName;
        anagraphicWeb.birthDate = CalendarUtils.copy(birthDate);
        anagraphicWeb.homeAddress = homeAddress.copy();
        return anagraphicWeb;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthDate() {
        return CalendarUtils.copy(birthDate);
    }

    public void setBirthDate(final Date birthDate) {
        this.birthDate = CalendarUtils.copy(birthDate);
    }

    public AddressWeb getHomeAddress() {
        return homeAddress.copy();
    }

    public void setHomeAddress(final AddressWeb homeAddress) {
        this.homeAddress = homeAddress.copy();
    }
}
