/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.user.adapter.api;

import it.unibo.luckyauction.user.adapter.UserAuthenticationWeb;
import it.unibo.luckyauction.user.adapter.UserWeb;

import java.util.Optional;

public interface UserApi {

    /**
     * Get the user object from the specified user id.
     * @param userId    the user id
     * @return          the corresponding user, if a user with such username exists
     */
    Optional<UserWeb> findById(String userId);

    /**
     * Get the user object from the specified username.
     * @param username  the username
     * @return          the corresponding user, if a user with such username exists
     */
    Optional<UserWeb> findByUsername(String username);

    /**
     * Find a user authentication by its username.
     * @param username  the desired username
     * @return          the corresponding user authentication, if exists
     */
    Optional<UserAuthenticationWeb> findUserAuthenticationByUsername(String username);
}
