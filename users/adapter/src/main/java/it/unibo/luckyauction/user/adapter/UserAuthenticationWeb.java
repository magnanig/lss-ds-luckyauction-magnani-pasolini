/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.user.adapter;

import it.unibo.luckyauction.user.domain.valueobject.Role;
import it.unibo.luckyauction.user.domain.entity.UserAuthentication;

import java.util.List;

import static it.unibo.luckyauction.util.NullableUtils.safeCall;

/**
 * Represents the schema of a {@link UserAuthentication}. This is a (de)serializable
 * version with all getters and setters, to be used to send object
 * through the network.
 */
public final class UserAuthenticationWeb {

    private String username;
    private String encryptedPassword;
    private List<Role> roles;

    public static UserAuthenticationWeb fromDomain(final UserAuthentication userAuthentication) {
        final UserAuthenticationWeb userAuthenticationWeb = new UserAuthenticationWeb();
        userAuthenticationWeb.username = userAuthentication.getUsername();
        userAuthenticationWeb.encryptedPassword = userAuthentication.getEncryptedPassword();
        userAuthenticationWeb.roles = userAuthentication.getRoles().stream().toList();
        return userAuthenticationWeb;
    }

    public UserAuthentication toDomain() {
        return new UserAuthentication(username, encryptedPassword, safeCall(roles, r -> r.toArray(new Role[0])));
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(final String username) {
        this.username = username;
    }

    public String getEncryptedPassword() {
        return encryptedPassword;
    }

    public void setEncryptedPassword(final String encryptedPassword) {
        this.encryptedPassword = encryptedPassword;
    }

    public List<Role> getRoles() {
        return List.copyOf(roles);
    }

    public void setRoles(final List<Role> roles) {
        this.roles = List.copyOf(roles);
    }
}
