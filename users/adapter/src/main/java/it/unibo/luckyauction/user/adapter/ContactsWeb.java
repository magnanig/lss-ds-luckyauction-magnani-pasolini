/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.user.adapter;

import it.unibo.luckyauction.user.domain.valueobject.Contacts;

import static it.unibo.luckyauction.util.NullableUtils.safeCall;

/**
 * Represents the schema of {@link Contacts}. This is a (de)serializable
 * version with all getters and setters, to be used to send object
 * through the network.
 */
public final class ContactsWeb {

    private String email;
    private PhoneWeb telephoneNumber;
    private PhoneWeb cellularNumber;

    public static ContactsWeb fromDomainContacts(final Contacts contacts) {
        final ContactsWeb contactsWeb = new ContactsWeb();
        contactsWeb.setEmail(contacts.email());
        contactsWeb.setCellularNumber(PhoneWeb.fromDomainPhone(contacts.cellularNumber()));
        contactsWeb.setTelephoneNumber(safeCall(contacts.telephoneNumber(), PhoneWeb::fromDomainPhone));
        return contactsWeb;
    }

    public Contacts toDomainContacts() {
        return new Contacts(email, cellularNumber.toDomainPhone(), safeCall(telephoneNumber, PhoneWeb::toDomainPhone));
    }

    public ContactsWeb copy() {
        final ContactsWeb contactsWeb = new ContactsWeb();
        contactsWeb.email = email;
        contactsWeb.cellularNumber = cellularNumber;
        contactsWeb.telephoneNumber = telephoneNumber;
        return contactsWeb;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public PhoneWeb getTelephoneNumber() {
        return safeCall(telephoneNumber, PhoneWeb::copy);
    }

    public void setTelephoneNumber(final PhoneWeb telephoneNumber) {
        this.telephoneNumber = safeCall(telephoneNumber, PhoneWeb::copy);
    }

    public PhoneWeb getCellularNumber() {
        return cellularNumber.copy();
    }

    public void setCellularNumber(final PhoneWeb cellularNumber) {
        this.cellularNumber = cellularNumber.copy();
    }
}
