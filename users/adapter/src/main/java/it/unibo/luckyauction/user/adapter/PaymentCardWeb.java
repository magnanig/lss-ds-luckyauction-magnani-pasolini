/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.user.adapter;

import it.unibo.luckyauction.user.domain.valueobject.PaymentCard;
import it.unibo.luckyauction.util.CalendarUtils;

import java.util.Calendar;
import java.util.Date;

/**
 * Represents the schema of a {@link PaymentCard}. This is a (de)serializable
 * version with all getters and setters, to be used to send object
 * through the network.
 */
public final class PaymentCardWeb {

    private String cardNumber;
    private String cvv;
    private Date expirationDate;
    private String firstName;
    private String lastName;

    public static PaymentCardWeb fromDomainPaymentCard(final PaymentCard paymentCard) {
        final PaymentCardWeb paymentCardWeb = new PaymentCardWeb();
        paymentCardWeb.setCardNumber(paymentCard.getCardNumber());
        paymentCardWeb.setCvv(paymentCard.getCvv());
        paymentCardWeb.setExpirationDate(paymentCard.getExpirationDate().getTime());
        paymentCardWeb.setFirstName(paymentCard.getFirstName());
        paymentCardWeb.setLastName(paymentCard.getLastName());
        return paymentCardWeb;
    }

    public PaymentCard toDomainPaymentCard() {
        final Calendar domainExpirationDate = Calendar.getInstance();
        domainExpirationDate.setTimeInMillis(expirationDate.getTime());
        return PaymentCard.builder()
                .cardNumber(cardNumber)
                .last4digits(cardNumber.substring(cardNumber.length() - 4))
                .cvv(cvv)
                .expirationDate(domainExpirationDate)
                .firstName(firstName)
                .lastName(lastName)
                .build();
    }

    public PaymentCardWeb copy() {
        final PaymentCardWeb paymentCardWeb = new PaymentCardWeb();
        paymentCardWeb.cardNumber = cardNumber;
        paymentCardWeb.cvv = cvv;
        paymentCardWeb.expirationDate = CalendarUtils.copy(expirationDate);
        paymentCardWeb.firstName = firstName;
        paymentCardWeb.lastName = lastName;
        return paymentCardWeb;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(final String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCvv() {
        return cvv;
    }

    public void setCvv(final String cvv) {
        this.cvv = cvv;
    }

    public Date getExpirationDate() {
        return CalendarUtils.copy(expirationDate);
    }

    public void setExpirationDate(final Date expirationDate) {
        this.expirationDate = CalendarUtils.copy(expirationDate);
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }
}
