/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.user.adapter;

import it.unibo.luckyauction.util.CalendarUtils;
import it.unibo.luckyauction.user.domain.valueobject.PaymentCard;

import java.util.Calendar;
import java.util.Date;

/**
 * Represents the schema of a {@link PaymentCard}, where CVV is missing
 * and card number is encrypted. This is a (de)serializable version with
 * all getters and setters, to be used to send object through the network.
 */
public final class HiddenPaymentCardWeb {
    private String encryptedCardNumber;
    private String last4digits;
    private Date expirationDate;
    private String firstName;
    private String lastName;

    public static HiddenPaymentCardWeb fromDomainPaymentCard(final PaymentCard paymentCard) {
        final HiddenPaymentCardWeb paymentCardWeb = new HiddenPaymentCardWeb();
        paymentCardWeb.setEncryptedCardNumber(paymentCard.getCardNumber());
        paymentCardWeb.setLast4digits(paymentCard.getLast4Digits());
        paymentCardWeb.setExpirationDate(paymentCard.getExpirationDate().getTime());
        paymentCardWeb.setFirstName(paymentCard.getFirstName());
        paymentCardWeb.setLastName(paymentCard.getLastName());
        return paymentCardWeb;
    }

    public PaymentCard toDomainPaymentCard() {
        final Calendar domainExpirationDate = Calendar.getInstance();
        domainExpirationDate.setTimeInMillis(expirationDate.getTime());
        return PaymentCard.builder()
                .cardNumber(encryptedCardNumber)
                .last4digits(last4digits)
                .expirationDate(domainExpirationDate)
                .firstName(firstName)
                .lastName(lastName)
                .build();
    }

    public HiddenPaymentCardWeb copy() {
        final HiddenPaymentCardWeb paymentCardWeb = new HiddenPaymentCardWeb();
        paymentCardWeb.encryptedCardNumber = encryptedCardNumber;
        paymentCardWeb.last4digits = last4digits;
        paymentCardWeb.expirationDate = CalendarUtils.copy(expirationDate);
        paymentCardWeb.firstName = firstName;
        paymentCardWeb.lastName = lastName;
        return paymentCardWeb;
    }

    public String getLast4digits() {
        return last4digits;
    }

    public void setLast4digits(final String last4digits) {
        this.last4digits = last4digits;
    }

    public void setEncryptedCardNumber(final String encryptedCardNumber) {
        this.encryptedCardNumber = encryptedCardNumber;
    }

    public String getEncryptedCardNumber() {
        return encryptedCardNumber;
    }

    public Date getExpirationDate() {
        return CalendarUtils.copy(expirationDate);
    }

    public void setExpirationDate(final Date expirationDate) {
        this.expirationDate = CalendarUtils.copy(expirationDate);
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }
}
