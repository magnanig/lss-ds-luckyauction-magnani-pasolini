/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.user.adapter;

import it.unibo.luckyauction.user.domain.entity.User;

/**
 * Represents the schema of an {@link User}, containing only the fields
 * required while creating a new user. This is a (de)serializable version
 * with all getters and setters, to be used to send object through the network.
 */
public final class NewUserWeb {

    private String username;
    private String password;
    private AnagraphicWeb anagraphic;
    private ContactsWeb contacts;
    private PaymentCardWeb card;

    public User toDomainUser() {
        return User.builder()
                .username(username)
                .anagraphic(anagraphic.toDomainAnagraphic())
                .contacts(contacts.toDomainContacts())
                .card(card.toDomainPaymentCard())
                .build();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(final String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    public AnagraphicWeb getAnagraphic() {
        return anagraphic.copy();
    }

    public void setAnagraphic(final AnagraphicWeb anagraphic) {
        this.anagraphic = anagraphic.copy();
    }

    public ContactsWeb getContacts() {
        return contacts.copy();
    }

    public void setContacts(final ContactsWeb contacts) {
        this.contacts = contacts.copy();
    }

    public PaymentCardWeb getCard() {
        return card.copy();
    }

    public void setCard(final PaymentCardWeb card) {
        this.card = card.copy();
    }
}
