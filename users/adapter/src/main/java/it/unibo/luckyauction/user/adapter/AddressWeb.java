/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.user.adapter;

import it.unibo.luckyauction.user.domain.valueobject.Address;

/**
 * Represents the schema of an {@link Address}. This is a (de)serializable
 * version with all getters and setters, to be used to send object
 * through the network.
 */
public final class AddressWeb {

    private String street;
    private Integer houseNumber;
    private String city;
    private CapWeb cap;
    private String province;

    public static AddressWeb fromDomainAddress(final Address address) {
        final AddressWeb addressWeb = new AddressWeb();
        addressWeb.setStreet(address.street());
        addressWeb.setHouseNumber(address.houseNumber());
        addressWeb.setCity(address.city());
        addressWeb.setCap(CapWeb.fromDomainCap(address.cap()));
        addressWeb.setProvince(address.province());
        return addressWeb;
    }

    public Address toDomainAddress() {
        return Address.builder()
                .street(street)
                .houseNumber(houseNumber)
                .city(city)
                .cap(cap.toDomainCap())
                .province(province)
                .build();
    }

    public AddressWeb copy() {
        final AddressWeb addressWeb = new AddressWeb();
        addressWeb.street = street;
        addressWeb.houseNumber = houseNumber;
        addressWeb.city = city;
        addressWeb.cap = cap.copy();
        addressWeb.province = province;
        return addressWeb;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(final String street) {
        this.street = street;
    }

    public Integer getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(final Integer houseNumber) {
        this.houseNumber = houseNumber;
    }

    public String getCity() {
        return city;
    }

    public void setCity(final String city) {
        this.city = city;
    }

    public CapWeb getCap() {
        return cap.copy();
    }

    public void setCap(final CapWeb cap) {
        this.cap = cap.copy();
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(final String province) {
        this.province = province;
    }
}
