module lss.ds.luckyauction.magnani.pasolini.users.adapter.main {
    exports it.unibo.luckyauction.user.adapter;
    exports it.unibo.luckyauction.user.adapter.api;
    requires lss.ds.luckyauction.magnani.pasolini.users.domain.main;
    requires lss.ds.luckyauction.magnani.pasolini.utils.main;
}