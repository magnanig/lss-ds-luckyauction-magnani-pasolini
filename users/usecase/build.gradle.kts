dependencies {
    implementation(project(":utils"))

    implementation(project(":users:domain"))

    // this will add following dependencies: test --> users.domain.testFixtures
    testImplementation(testFixtures(project(":users:domain")))
    testImplementation(project(":users:mongo-repository"))
    testImplementation(project(":uuid-generator"))
    testImplementation(project(":sha256encoder"))
}