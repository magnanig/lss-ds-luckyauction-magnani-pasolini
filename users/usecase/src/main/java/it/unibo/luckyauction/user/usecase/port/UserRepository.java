/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.user.usecase.port;

import it.unibo.luckyauction.user.domain.entity.User;

import java.util.List;
import java.util.Optional;

/**
 * The repository of users, i.e. some database of users.
 */
public interface UserRepository {

    /**
     * Create a new user, inserting it into user repository.
     * @param user  the user to be added to the repository
     */
    User create(User user);

    /**
     * Find the user with the desired id.
     * @param id    the user id
     * @return      the user with the specified id
     */
    Optional<User> findById(String id);

    /**
     * Find the user with the desired username.
     * @param username      the username
     * @return              the user with the specified username
     */
    Optional<User> findByUsername(String username);

    /**
     * Get all users present in the repository.
     * @return  the list of users
     */
    List<User> getAllUsers();

    /**
     * Delete an user by their username.
     * @param username  the desired username
     * @return          the just deleted user, if existed
     */
    Optional<User> deleteByUsername(String username);
}
