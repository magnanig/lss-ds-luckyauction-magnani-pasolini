/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.user.usecase;

import it.unibo.luckyauction.user.domain.entity.UserAuthentication;
import it.unibo.luckyauction.user.usecase.port.UserAuthenticationRepository;

import java.util.Optional;

/**
 * The class to manage the search of a user authentication.
 */
public class FindUserAuthentication {

    private final UserAuthenticationRepository userAuthenticationRepository;

    /**
     * Create a new instance to use in order to search users authentication.
     * @param userAuthenticationRepository    the user authentication repository
     */
    public FindUserAuthentication(final UserAuthenticationRepository userAuthenticationRepository) {
        this.userAuthenticationRepository = userAuthenticationRepository;
    }

    /**
     * Find a user authentication by its username.
     * @param username  the desired username
     * @return          the corresponding user authentication, if exists
     */
    public Optional<UserAuthentication> findByUsername(final String username) {
        return userAuthenticationRepository.findByUsername(username);
    }
}
