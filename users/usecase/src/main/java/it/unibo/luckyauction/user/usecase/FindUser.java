/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.user.usecase;

import it.unibo.luckyauction.user.domain.entity.User;
import it.unibo.luckyauction.user.usecase.port.UserRepository;

import java.util.List;
import java.util.Optional;

/**
 * The class to manage the search of a user.
 */
public class FindUser {

    private final UserRepository userRepository;

    /**
     * Create a new instance to use in order to search users.
     * @param userRepository    the user repository (i.e. a database of users)
     */
    public FindUser(final UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    /**
     * Find an user by their id.
     * @param userId    the desired user id
     * @return          the corresponding user, if exists
     */
    public Optional<User> findById(final String userId) {
        return userRepository.findById(userId);
    }

    /**
     * Find a user by their username.
     * @param username  the desired username
     * @return          the corresponding user, if exists
     */
    public Optional<User> findByUsername(final String username) {
        return userRepository.findByUsername(username);
    }

    /**
     * Get the list of all users registered in the application.
     * @return      the list of all users
     */
    public List<User> getAllUsers() {
        return userRepository.getAllUsers();
    }
}
