/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.user.usecase;

import it.unibo.luckyauction.user.domain.entity.User;
import it.unibo.luckyauction.user.domain.valueobject.PaymentCard;
import it.unibo.luckyauction.user.domain.entity.UserAuthentication;
import it.unibo.luckyauction.user.usecase.exception.UserAlreadyExistsException;
import it.unibo.luckyauction.user.usecase.port.UserAuthenticationRepository;
import it.unibo.luckyauction.user.usecase.port.UserRepository;
import it.unibo.luckyauction.util.port.CardEncoder;
import it.unibo.luckyauction.util.port.IdGenerator;
import it.unibo.luckyauction.util.port.PasswordEncoder;

/**
 * The class to manage the creation of a new user.
 */
public class CreateUser {

    private final UserRepository userRepository;
    private final UserAuthenticationRepository userAuthenticationRepository;
    private final IdGenerator userIdGenerator;
    private final IdGenerator auctionsHistoryIdGenerator;
    private final IdGenerator movementsHistoryIdGenerator;
    private final PasswordEncoder passwordEncoder;
    private final CardEncoder cardEncoder;

    /**
     * Create a new instance to use in order to create new users.
     * @param userRepository                the user repository (i.e. a database of users)
     * @param userAuthenticationRepository  the user authentication repository
     * @param userIdGenerator               the generator of user ids
     * @param auctionsHistoryIdGenerator    the generator of auctions history ids
     * @param movementsHistoryIdGenerator   the generator of movements history ids
     * @param passwordEncoder               the password encoder to use to encrypt user password
     * @param cardEncoder                   the card encoder to use to encrypt user card number
     */
    public CreateUser(final UserRepository userRepository,
                      final UserAuthenticationRepository userAuthenticationRepository,
                      final IdGenerator userIdGenerator, final IdGenerator auctionsHistoryIdGenerator,
                      final IdGenerator movementsHistoryIdGenerator, final PasswordEncoder passwordEncoder,
                      final CardEncoder cardEncoder) {
        this.userRepository = userRepository;
        this.userAuthenticationRepository = userAuthenticationRepository;
        this.userIdGenerator = userIdGenerator;
        this.auctionsHistoryIdGenerator = auctionsHistoryIdGenerator;
        this.movementsHistoryIdGenerator = movementsHistoryIdGenerator;
        this.passwordEncoder = passwordEncoder;
        this.cardEncoder = cardEncoder;
    }

    /**
     * Create a new user starting from the specified user object and password.
     * @param user                          the user to create
     * @param password                      the password of the user
     * @return                              the just created user, with identifiers properly
     *                                      set and encoded password
     * @throws UserAlreadyExistsException   if a user with the specified username
     *                                      already exists
     */
    public User create(final User user, final String password) throws UserAlreadyExistsException {
        if (userRepository.findByUsername(user.getUsername()).isPresent()) {
            throw new UserAlreadyExistsException(user.getUsername());
        }
        final User newUser = User.builder()
                .userId(userIdGenerator.generate())
                .username(user.getUsername())
                .anagraphic(user.getAnagraphic())
                .contacts(user.getContacts())
                .card(PaymentCard.encrypt(user.getCard(), cardEncoder.encode(user.getCard().getCardNumber())))
                .auctionsHistoryId(auctionsHistoryIdGenerator.generate())
                .movementsHistoryId(movementsHistoryIdGenerator.generate())
                .build();
        userAuthenticationRepository.create(UserAuthentication.baseUser(user.getUsername(),
                passwordEncoder.encode(password)));
        return userRepository.create(newUser);
    }
}
