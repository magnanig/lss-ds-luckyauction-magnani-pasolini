package it.unibo.luckyauction.user.usecase.port;

/**
 * A password encoder, to use in order to encrypt password.
 */
public interface PasswordEncoder extends Encoder {

    /**
     * Encode the specified password.
     * @param password  the password to encrypt
     * @return          the properly encoded password
     */
    @Override
    String encode(String password);
}
