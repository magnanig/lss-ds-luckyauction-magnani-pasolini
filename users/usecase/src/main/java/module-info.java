module lss.ds.luckyauction.magnani.pasolini.users.usecase.main {
    exports it.unibo.luckyauction.user.usecase;
    exports it.unibo.luckyauction.user.usecase.port;
    exports it.unibo.luckyauction.user.usecase.exception;
    requires lss.ds.luckyauction.magnani.pasolini.utils.main;
    requires lss.ds.luckyauction.magnani.pasolini.users.domain.main;
}