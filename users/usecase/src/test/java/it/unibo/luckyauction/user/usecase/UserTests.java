/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.user.usecase;

import it.unibo.luckyauction.user.domain.entity.User;
import it.unibo.luckyauction.encoder.Sha256CardEncoder;
import it.unibo.luckyauction.encoder.Sha256PasswordEncoder;
import it.unibo.luckyauction.user.domain.UserTestUtils;
import it.unibo.luckyauction.user.domain.valueobject.Role;
import it.unibo.luckyauction.user.domain.entity.UserAuthentication;
import it.unibo.luckyauction.user.repository.UserAuthenticationMongoRepository;
import it.unibo.luckyauction.user.repository.UserMongoRepository;
import it.unibo.luckyauction.user.usecase.exception.UserAlreadyExistsException;
import it.unibo.luckyauction.user.usecase.port.UserAuthenticationRepository;
import it.unibo.luckyauction.user.usecase.port.UserRepository;
import it.unibo.luckyauction.uuid.UUIdGenerator;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import static it.unibo.luckyauction.user.domain.UserTestUtils.FIRST_USERNAME;
import static it.unibo.luckyauction.user.domain.UserTestUtils.PASSWORD;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class UserTests {

    private static final UserRepository USER_REPO = UserMongoRepository.testRepository();
    private static final UserAuthenticationRepository USER_AUTH_REPO =
            UserAuthenticationMongoRepository.testRepository();

    private final CreateUser createUser = new CreateUser(USER_REPO, USER_AUTH_REPO, new UUIdGenerator(),
            new UUIdGenerator(), new UUIdGenerator(), new Sha256PasswordEncoder(), new Sha256CardEncoder());
    private final FindUserAuthentication findUserAuthentication = new FindUserAuthentication(USER_AUTH_REPO);
    private final FindUser findUser = new FindUser(USER_REPO);
    private final DeleteUser deleteUser = new DeleteUser(USER_REPO, USER_AUTH_REPO);

    private User user;

    @BeforeAll
    void createTestUser() {
        if (findUser.findByUsername(FIRST_USERNAME).isPresent()) {
            deleteUser.deleteByUsername(FIRST_USERNAME);
        }
        user = createUser.create(UserTestUtils.createNewUser(FIRST_USERNAME), PASSWORD);
    }

    @AfterAll
    void deleteTestUser() {
        deleteUser.deleteByUsername(FIRST_USERNAME);
        assertTrue(findUser.findByUsername(FIRST_USERNAME).isEmpty());
        assertTrue(findUserAuthentication.findByUsername(FIRST_USERNAME).isEmpty());
    }

    @Test
    void createdUserCheck() {
        assertNotEquals(UserTestUtils.USER_ID, user.getUserId());
        assertNotEquals(UserTestUtils.CARD_NUMBER, user.getCard().getCardNumber());
        assertNotEquals(UserTestUtils.AUCTIONS_HISTORY_ID, user.getAuctionsHistoryId());
        assertNotEquals(UserTestUtils.MOVEMENTS_HISTORY_ID, user.getMovementsHistoryId());
        assertEquals(FIRST_USERNAME, user.getUsername());
        assertEquals(UserTestUtils.CONTACTS.get(), user.getContacts());
        assertEquals(UserTestUtils.ANAGRAPHIC.get(), user.getAnagraphic());
    }

    @Test
    void userCreationExceptionTest() {
        final Exception exception = assertThrows(UserAlreadyExistsException.class,
                () -> createUser.create(user, PASSWORD));
        assertTrue(exception.getMessage().contains("User " + FIRST_USERNAME + " already exists"));
    }

    @Test
    void findUserAuthenticationTest() {
        final Optional<UserAuthentication> userAuthentication = findUserAuthentication.findByUsername(FIRST_USERNAME);
        assertTrue(userAuthentication.isPresent());
        assertNotEquals(PASSWORD, userAuthentication.get().getEncryptedPassword());
        assertEquals(Set.of(Role.USER), userAuthentication.get().getRoles());
    }

    @Test
    void findUserByIdTest() {
        final User foundUser = findUser.findById(user.getUserId()).orElseThrow();
        assertEquals(user, foundUser);
    }

    @Test
    void findAllUsersTest() {
        final List<User> foundUsers = findUser.getAllUsers();
        assertTrue(foundUsers.contains(user));
    }
}
