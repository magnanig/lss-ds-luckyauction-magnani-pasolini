/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.user.domain.valueobject;

import it.unibo.luckyauction.user.domain.exception.PaymentCardValidationException;
import it.unibo.luckyauction.util.CalendarUtils;
import lombok.Generated;

import java.util.Calendar;
import java.util.Objects;

/**
 * Represents a payment card (e.g. Visa, Mastercard, Bancomat, ecc).
 */
public final class PaymentCard {
    private final String cardNumber;
    private final String last4Digits;
    private final String cvv;
    private final Calendar expirationDate;
    private final String firstName;
    private final String lastName;

    /**
     * Create a new payment card object.
     * @param cardNumber        the number of the card
     * @param last4Digits       last 4 digits of the card number
     * @param cvv               the CVV of the card (3 ciphers)
     * @param expirationDate    the expiration date
     * @param firstName         the firstname of the card owner
     * @param lastName          the lastname of the card owner
     */
    public PaymentCard(final String cardNumber, final String last4Digits, final String cvv,
                       final Calendar expirationDate, final String firstName, final String lastName) {
        this.cardNumber = cardNumber;
        this.last4Digits = last4Digits;
        this.cvv = cvv;
        this.expirationDate = CalendarUtils.copy(expirationDate);
        this.firstName = firstName;
        this.lastName = lastName;
    }

    @Generated
    public static PaymentCard encrypt(final PaymentCard card, final String encryptedCardNumber) {
        return builder()
                .cardNumber(encryptedCardNumber)
                .last4digits(card.cardNumber.substring(card.cardNumber.length() - 4))
                .cvv(card.cvv)
                .expirationDate(card.expirationDate)
                .firstName(card.firstName)
                .lastName(card.lastName)
                .build();
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public String getLast4Digits() {
        return last4Digits;
    }

    public String getCvv() {
        return cvv;
    }

    public Calendar getExpirationDate() {
        return CalendarUtils.copy(expirationDate);
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public static PaymentCardBuilder builder() {
        return new PaymentCardBuilder();
    }

    @Override @Generated
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final PaymentCard that = (PaymentCard) o;
        return cardNumber.equals(that.cardNumber) && expirationDate.equals(that.expirationDate)
                && firstName.equals(that.firstName) && lastName.equals(that.lastName);
    }

    @Override @Generated
    public int hashCode() {
        return Objects.hash(cardNumber, last4Digits, expirationDate, firstName, lastName);
    }

    @Override @Generated
    public String toString() {
        return "PaymentCard {\n"
                + "\tcardNumber='" + cardNumber + "'" + "\n"
                + "\tlast4Digits='" + last4Digits + "'" + "\n"
                + "\tcvv='" + cvv + "'" + "\n"
                + "\texpirationDate=" + expirationDate + "\n"
                + "\tfirstName='" + firstName + "'" + "\n"
                + "\tlastName='" + lastName + "'" + "\n"
                + "}";
    }

    @SuppressWarnings("PMD.AvoidFieldNameMatchingMethodName")
    public static final class PaymentCardBuilder {

        private static final Integer CVV_LENGTH = 3;

        private String cardNumber;
        private String last4digits;
        private String cvv;
        private Calendar expirationDate;
        private String firstName;
        private String lastName;

        private PaymentCardBuilder() {
        }

        public PaymentCardBuilder cardNumber(final String cardNumber) {
            this.cardNumber = cardNumber;
            return this;
        }

        public PaymentCardBuilder last4digits(final String last4digits) {
            this.last4digits = last4digits;
            return this;
        }

        public PaymentCardBuilder cvv(final String cvv) {
            this.cvv = cvv;
            return this;
        }

        public PaymentCardBuilder expirationDate(final Calendar expirationDate) {
            this.expirationDate = CalendarUtils.copy(expirationDate);
            return this;
        }

        public PaymentCardBuilder firstName(final String firstName) {
            this.firstName = firstName;
            return this;
        }

        public PaymentCardBuilder lastName(final String lastName) {
            this.lastName = lastName;
            return this;
        }

        public PaymentCard build() {
            validatePaymentCardObject();
            return new PaymentCard(cardNumber, last4digits, cvv, expirationDate, firstName, lastName);
        }

        private void validatePaymentCardObject() {
            isNotNull();
            if (cvv != null && cvv.length() != CVV_LENGTH) {
                throw new PaymentCardValidationException("CVV must be a code of three ciphers");
            }
        }

        private void isNotNull() {
            if (cardNumber == null || cardNumber.isEmpty()) {
                throw new PaymentCardValidationException("User's card number is not set in PaymentCard object");
            }
            if (expirationDate == null) {
                throw new PaymentCardValidationException("Expiration date is not set in PaymentCard object");
            }
            if (firstName == null || firstName.isEmpty()) {
                throw new PaymentCardValidationException("First name is not set in PaymentCard object");
            }
            if (lastName == null || lastName.isEmpty()) {
                throw new PaymentCardValidationException("Last name is not set in PaymentCard object");
            }
        }
    }
}
