/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.user.domain.entity;

import it.unibo.luckyauction.user.domain.exception.UserAuthenticationValidationException;
import it.unibo.luckyauction.user.domain.valueobject.Role;
import lombok.Generated;

import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Represents the user information about authentication,
 * i.e. username, password and role(s).
 */
public class UserAuthentication {

    private final String username;
    private String encryptedPassword;
    private Set<Role> roles;

    public UserAuthentication(final String username, final String encryptedPassword, final Role... roles) {
        this.username = username;
        this.encryptedPassword = encryptedPassword;
        this.roles = roles == null || roles.length == 0 ? Set.of(Role.USER) : Set.of(roles);
        validate();
    }

    public static UserAuthentication baseUser(final String username, final String encryptedPassword) {
        return new UserAuthentication(username, encryptedPassword, Role.USER);
    }

    public static UserAuthentication adminUser(final String username, final String encryptedPassword) {
        return new UserAuthentication(username, encryptedPassword, Role.ADMIN);
    }

    public static UserAuthentication systemUser(final String username, final String encryptedPassword) {
        return new UserAuthentication(username, encryptedPassword, Role.SYSTEM);
    }

    /**
     * Get the username.
     */
    public String getUsername() {
        return username;
    }

    /**
     * Get the user password.
     */
    public String getEncryptedPassword() {
        return encryptedPassword;
    }

    /**
     * Get the user roles.
     */
    public Set<Role> getRoles() {
        return Collections.unmodifiableSet(roles);
    }

    /**
     * Change user password.
     * @param encryptedPassword the new encrypted password
     */
    public void changePassword(final String encryptedPassword) {
        this.encryptedPassword = encryptedPassword;
    }

    /**
     * Set new user roles.
     * @param roles the all user roles
     */
    public void setRoles(final Set<Role> roles) {
        this.roles = new HashSet<>(roles);
    }

    /**
     * {@inheritDoc}
     */
    @Override @Generated
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final UserAuthentication that = (UserAuthentication) o;
        return username.equals(that.username) && encryptedPassword.equals(that.encryptedPassword)
                && roles.equals(that.roles);
    }

    /**
     * {@inheritDoc}
     */
    @Override @Generated
    public int hashCode() {
        return Objects.hash(username, encryptedPassword, roles);
    }

    /**
     * {@inheritDoc}
     */
    @Override @Generated
    public String toString() {
        return "UserAuthentication {\n"
                + "\tusername='" + username + "'" + "\n"
                + "\tencryptedPassword='" + encryptedPassword + "'" + "\n"
                + "\troles=" + roles + "\n"
                + "}";
    }

    private void validate() {
        if (username == null || username.isEmpty()) {
            throw new UserAuthenticationValidationException("Username is not set in UserAuthentication object");
        }
        if (encryptedPassword == null || encryptedPassword.isEmpty()) {
            throw new UserAuthenticationValidationException("Password is not set in UserAuthentication object");
        }
    }
}

