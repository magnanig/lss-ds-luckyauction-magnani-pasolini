package it.unibo.luckyauction.user.domain.valueobject;

import it.unibo.luckyauction.user.domain.exception.UserValidationException;

/**
 * Create a new CAP object with the specified code.
 *
 * @param code the code (i.e. the CAP), as string
 */
public record Cap(String code) {

    private static final Integer CODE_LENGTH = 5;

    /**
     * Create a new CAP object with the specified code.
     *
     * @param code the code (i.e. the CAP), as string
     */
    public Cap {
        if (code == null) {
            throw new UserValidationException("User's CAP is not set");
        }
        if (code.length() != CODE_LENGTH) {
            throw new UserValidationException("User's CAP is not valid");
        }
    }
}
