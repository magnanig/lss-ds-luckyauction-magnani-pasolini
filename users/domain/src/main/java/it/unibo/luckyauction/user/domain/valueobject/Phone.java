/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.user.domain.valueobject;

import it.unibo.luckyauction.user.domain.exception.UserValidationException;
import lombok.Generated;

/**
 * Create a new phone number.
 *
 * @param number the phone number
 */
public record Phone(String number) {

    public Phone {
        if (number.isEmpty()) {
            throw new UserValidationException("User's phone number is not set in the Phone object");
        }
    }

    /**
     * Convert phone number to string.
     *
     * @return the phone number as string
     */
    @Override @Generated
    public String toString() {
        return number;
    }
}
