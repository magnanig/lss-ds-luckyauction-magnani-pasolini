/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.user.domain.valueobject;

import it.unibo.luckyauction.user.domain.exception.UserValidationException;

/**
 * Create a new address with the specified information.
 *
 * @param street      the street name
 * @param houseNumber the house number
 * @param city        the city
 * @param cap         the CAP
 * @param province    the province
 */
public record Address(String street, Integer houseNumber, String city, Cap cap, String province) {

    public static AddressBuilder builder() {
        return new AddressBuilder();
    }

    @SuppressWarnings("PMD.AvoidFieldNameMatchingMethodName")
    public static final class AddressBuilder {
        private String street;
        private Integer houseNumber;
        private String city;
        private Cap cap;
        private String province;

        private AddressBuilder() { }

        public AddressBuilder street(final String street) {
            this.street = street;
            return this;
        }

        public AddressBuilder houseNumber(final Integer houseNumber) {
            this.houseNumber = houseNumber;
            return this;
        }

        public AddressBuilder city(final String city) {
            this.city = city;
            return this;
        }

        public AddressBuilder cap(final Cap cap) {
            this.cap = cap;
            return this;
        }

        public AddressBuilder province(final String province) {
            this.province = province;
            return this;
        }

        public Address build() {
            validateAddressObject();
            return new Address(street, houseNumber, city, cap, province);
        }

        private void validateAddressObject() {
            checkIsNotNull();
            final int provinceLength = 2;
            if (province.length() != provinceLength) {
                throw new UserValidationException("User's province is not valid");
            }
        }

        private void checkIsNotNull() {
            if (street == null || street.isEmpty()) {
                throw new UserValidationException("User's street is not set in Address object");
            }
            if (houseNumber == null) {
                throw new UserValidationException("User's house number is not set in Address object");
            }
            if (city == null || city.isEmpty()) {
                throw new UserValidationException("User's city is not set in Address object");
            }
            if (cap == null) {
                throw new UserValidationException("User's CAP is not set in Address object");
            }
            if (province == null || province.isEmpty()) {
                throw new UserValidationException("User's province is not set in Address object");
            }
        }
    }
}
