/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.user.domain.valueobject;

import it.unibo.luckyauction.user.domain.exception.UserValidationException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Create the user contacts, i.e. email, telephone number and
 * mobile phone number.
 * @param email           the user email
 * @param cellularNumber  the user cellular number
 * @param telephoneNumber the user telephone number
 */
public record Contacts(String email, Phone cellularNumber, Phone telephoneNumber) {

    public Contacts(final String email, final Phone cellularNumber, final Phone telephoneNumber) {
        this.email = email;
        this.cellularNumber = cellularNumber;
        this.telephoneNumber = telephoneNumber;
        validateContactsObject();
    }

    public Contacts(final String email, final Phone cellularNumber) {
        this(email, cellularNumber, null);
    }

    private void validateContactsObject() {
        checkIsNotNull();
        validateEmailAddress();
    }

    private void checkIsNotNull() {
        if (email.isEmpty()) {
            throw new UserValidationException("User's email is not set in the Contacts object");
        }
        if (cellularNumber == null) {
            throw new UserValidationException("User's cellular number is not set in the Contacts object");
        }
    }

    private void validateEmailAddress() {
        final String ePattern = "^([A-Z0-9]([._%+-][A-Z0-9])?)+@([A-Z0-9]([.-][A-Z0-9])?)+\\.[A-Z]{2,6}$";
        final Pattern p = Pattern.compile(ePattern, Pattern.CASE_INSENSITIVE);
        final Matcher m = p.matcher(email);
        if (!m.matches()) {
            throw new UserValidationException("The specified email is not valid");
        }
    }

}
