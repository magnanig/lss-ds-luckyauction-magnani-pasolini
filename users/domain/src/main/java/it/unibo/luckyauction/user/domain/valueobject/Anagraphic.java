/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.user.domain.valueobject;

import it.unibo.luckyauction.user.domain.exception.AnagraphicValidationException;
import it.unibo.luckyauction.user.domain.exception.UserValidationException;
import it.unibo.luckyauction.util.CalendarUtils;
import lombok.Generated;

import java.util.Calendar;
import java.util.Objects;

/**
 * Represents an anagraphic object containing the user information.
 */
public final class Anagraphic {
    /**
     * The majority age in current State.
     */
    public static final int MAJORITY_AGE = 18;

    private final String firstName;
    private final String lastName;
    private final Calendar birthDate;
    private final Address homeAddress;

    /**
     * Create a new anagraphic object containing the user information.
     * @param firstName   the user first name
     * @param lastName    the user last name
     * @param birthDate   the user birthdate
     * @param homeAddress the user home address
     */
    public Anagraphic(final String firstName, final String lastName, final Calendar birthDate,
                      final Address homeAddress) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = CalendarUtils.copy(birthDate);
        this.homeAddress = homeAddress;
    }

    public static AnagraphicBuilder builder() {
        return new AnagraphicBuilder();
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Calendar getBirthDate() {
        return CalendarUtils.copy(birthDate);
    }

    public Address getHomeAddress() {
        return homeAddress;
    }

    @Override @Generated
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final Anagraphic that = (Anagraphic) o;
        return firstName.equals(that.firstName) && lastName.equals(that.lastName)
                && birthDate.equals(that.birthDate) && homeAddress.equals(that.homeAddress);
    }

    @Override @Generated
    public int hashCode() {
        return Objects.hash(firstName, lastName, birthDate, homeAddress);
    }

    @SuppressWarnings("PMD.AvoidFieldNameMatchingMethodName")
    public static final class AnagraphicBuilder {
        private String firstName;
        private String lastName;
        private Calendar birthDate;
        private Address homeAddress;

        private AnagraphicBuilder() { }

        public AnagraphicBuilder firstName(final String firstName) {
            this.firstName = firstName;
            return this;
        }

        public AnagraphicBuilder lastName(final String lastName) {
            this.lastName = lastName;
            return this;
        }

        public AnagraphicBuilder birthDate(final Calendar birthDate) {
            this.birthDate = CalendarUtils.copy(birthDate);
            return this;
        }

        public AnagraphicBuilder homeAddress(final Address homeAddress) {
            this.homeAddress = homeAddress;
            return this;
        }

        public Anagraphic build() {
            validateAnagraphicObject();
            return new Anagraphic(firstName, lastName, birthDate, homeAddress);
        }

        private void validateAnagraphicObject() {
            checkIsNotNull();
            final int age = CalendarUtils.getAge(birthDate);
            if (age < MAJORITY_AGE) {
                throw new UserValidationException("User is not of legal age");
            }
        }

        private void checkIsNotNull() {
            if (firstName == null || firstName.isEmpty()) {
                throw new AnagraphicValidationException("First name is not set");
            }
            if (lastName == null || lastName.isEmpty()) {
                throw new AnagraphicValidationException("Last name is not set");
            }
            if (birthDate == null) {
                throw new AnagraphicValidationException("Birth date is not set");
            }
            if (homeAddress == null) {
                throw new AnagraphicValidationException("Home address is not set");
            }
        }
    }
}
