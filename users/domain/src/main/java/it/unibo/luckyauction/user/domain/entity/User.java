/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.user.domain.entity;

import it.unibo.luckyauction.user.domain.exception.UserValidationException;
import it.unibo.luckyauction.user.domain.valueobject.Address;
import it.unibo.luckyauction.user.domain.valueobject.Anagraphic;
import it.unibo.luckyauction.user.domain.valueobject.Contacts;
import it.unibo.luckyauction.user.domain.valueobject.PaymentCard;
import lombok.Generated;

import java.util.Calendar;
import java.util.Objects;

/**
 * The user of the application, capable of creating and enjoying auctions.
 */
public final class User {

    private final String userId;
    private final String username;
    private final Anagraphic anagraphic;
    private final Contacts contacts;
    private final PaymentCard card;

    private final String auctionsHistoryId;
    private final String movementsHistoryId;

    /**
     * Create a new user, specifying its information.
     * @param userId        the user unique identifier
     * @param username      the user username
     * @param anagraphic    the anagraphic information (see {@link Anagraphic})
     * @param contacts      the user contacts (see {@link Contacts})
     * @param card          the card to be used to recharge budget
     */
    private User(final String userId, final String username, final Anagraphic anagraphic,
                 final Contacts contacts, final PaymentCard card, final String auctionsHistoryId,
                 final String movementsHistoryId) {
        this.userId = userId;
        this.username = username;
        this.anagraphic = anagraphic;
        this.contacts = contacts;
        this.card = card;
        this.auctionsHistoryId = auctionsHistoryId;
        this.movementsHistoryId = movementsHistoryId;
    }

    public String getUserId() {
        return userId;
    }

    public String getUsername() {
        return username;
    }

    public Anagraphic getAnagraphic() {
        return anagraphic;
    }

    public String getFirstName() {
        return anagraphic.getFirstName();
    }

    public String getLastName() {
        return anagraphic.getLastName();
    }

    public Calendar getBirthDate() {
        return anagraphic.getBirthDate();
    }

    public Address getHomeAddress() {
        return anagraphic.getHomeAddress();
    }

    public Contacts getContacts() {
        return contacts;
    }

    public PaymentCard getCard() {
        return card;
    }

    public String getAuctionsHistoryId() {
        return auctionsHistoryId;
    }

    public String getMovementsHistoryId() {
        return movementsHistoryId;
    }

    @Override @Generated
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final User user = (User) o;
        return Objects.equals(userId, user.userId) && username.equals(user.username)
                && anagraphic.equals(user.anagraphic) && contacts.equals(user.contacts) && card.equals(user.card)
                && auctionsHistoryId.equals(user.auctionsHistoryId)
                && movementsHistoryId.equals(user.movementsHistoryId);
    }

    @Override @Generated
    public int hashCode() {
        return Objects.hash(userId, username, anagraphic, contacts, card, auctionsHistoryId, movementsHistoryId);
    }

    @Override @Generated
    public String toString() {
        return "User{\n"
                + "\tuserId='" + userId + "'\n"
                + "\tusername=" + username + "'\n"
                + "\tanagraphic=" + anagraphic + "\n"
                + "\tcontacts=" + contacts + "\n"
                + "}";
    }

    public static UserBuilder builder() {
        return new UserBuilder();
    }

    @SuppressWarnings({"PMD.AvoidFieldNameMatchingMethodName"})
    public static final class UserBuilder {
        private String userId;
        private String username;
        private Anagraphic anagraphic;
        private Contacts contacts;
        private PaymentCard card;

        private String auctionsHistoryId;
        private String movementsHistoryId;

        private UserBuilder() { }

        public UserBuilder userId(final String userId) {
            this.userId = userId;
            return this;
        }

        public UserBuilder username(final String username) {
            this.username = username;
            return this;
        }

        public UserBuilder anagraphic(final Anagraphic anagraphic) {
            this.anagraphic = anagraphic;
            return this;
        }

        public UserBuilder contacts(final Contacts contacts) {
            this.contacts = contacts;
            return this;
        }

        public UserBuilder card(final PaymentCard card) {
            this.card = card;
            return this;
        }

        public UserBuilder auctionsHistoryId(final String auctionsHistoryId) {
            this.auctionsHistoryId = auctionsHistoryId;
            return this;
        }

        public UserBuilder movementsHistoryId(final String movementsHistoryId) {
            this.movementsHistoryId = movementsHistoryId;
            return this;
        }

        public User build() {
            validateUserObject();
            return new User(userId, username, anagraphic, contacts, card, auctionsHistoryId, movementsHistoryId);
        }

        private void validateUserObject() {
            if (username == null) {
                throw new UserValidationException("Username is not set");
            }
            if (anagraphic == null) {
                throw new UserValidationException("Anagraphic is not set");
            }
            if (contacts == null) {
                throw new UserValidationException("Contacts is not set");
            }
            if (card == null) {
                throw new UserValidationException("Card is not set");
            }
        }
    }
}
