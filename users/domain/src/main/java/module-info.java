module lss.ds.luckyauction.magnani.pasolini.users.domain.main {
    requires lombok;
    requires lss.ds.luckyauction.magnani.pasolini.utils.main;
    exports it.unibo.luckyauction.user.domain.entity;
    exports it.unibo.luckyauction.user.domain.exception;
    exports it.unibo.luckyauction.user.domain.valueobject;
}