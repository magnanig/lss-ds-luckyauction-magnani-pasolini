/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.user.domain;

import it.unibo.luckyauction.user.domain.exception.AnagraphicValidationException;
import it.unibo.luckyauction.user.domain.exception.UserValidationException;
import it.unibo.luckyauction.user.domain.valueobject.Address;
import it.unibo.luckyauction.user.domain.valueobject.Anagraphic;
import org.junit.jupiter.api.Test;

import java.util.Calendar;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class AnagraphicTests {

    private static final Address USER_ADDRESS = UserTestUtils.ADDRESS.get();
    private static final Calendar LEGAL_AGE = UserTestUtils.BIRTH_DATE_IN_MAJORITY_AGE.get();

    @Test
    void userAnagraphicTest() {
        final Anagraphic userAnagraphic = Anagraphic.builder().firstName(UserTestUtils.TEST_FIRST_NAME)
                .lastName(UserTestUtils.TEST_LAST_NAME).birthDate(UserTestUtils.BIRTH_DATE_IN_MAJORITY_AGE.get())
                .homeAddress(UserTestUtils.ADDRESS.get()).build();

        assertEquals(UserTestUtils.TEST_FIRST_NAME, userAnagraphic.getFirstName());
        assertEquals(UserTestUtils.TEST_LAST_NAME, userAnagraphic.getLastName());
        assertEquals(UserTestUtils.BIRTH_DATE_IN_MAJORITY_AGE.get().getTime(), userAnagraphic.getBirthDate().getTime());
        assertEquals(UserTestUtils.ADDRESS.get(), userAnagraphic.getHomeAddress());
    }

    @Test
    void firstAndLastNameExceptionsTest() {
        final String firstNameExceptionMessage = "First name is not set";
        final String lastNameExceptionMessage = "Last name is not set";

        final Exception nullFirstNameException = assertThrows(AnagraphicValidationException.class,
                () -> Anagraphic.builder().lastName(UserTestUtils.TEST_LAST_NAME)
                        .birthDate(LEGAL_AGE).homeAddress(USER_ADDRESS).build());
        assertTrue(nullFirstNameException.getMessage().contains(firstNameExceptionMessage));

        final Exception emptyFirstNameException = assertThrows(AnagraphicValidationException.class,
                () -> Anagraphic.builder().firstName("").lastName(UserTestUtils.TEST_LAST_NAME)
                        .birthDate(LEGAL_AGE).homeAddress(USER_ADDRESS).build());
        assertTrue(emptyFirstNameException.getMessage().contains(firstNameExceptionMessage));

        final Exception nullLastNameException = assertThrows(AnagraphicValidationException.class,
                () -> Anagraphic.builder().firstName(UserTestUtils.TEST_FIRST_NAME)
                        .birthDate(LEGAL_AGE).homeAddress(USER_ADDRESS).build());
        assertTrue(nullLastNameException.getMessage().contains(lastNameExceptionMessage));

        final Exception emptyLastNameException = assertThrows(AnagraphicValidationException.class,
                () -> Anagraphic.builder().firstName(UserTestUtils.TEST_FIRST_NAME).lastName("")
                        .birthDate(LEGAL_AGE).homeAddress(USER_ADDRESS).build());
        assertTrue(emptyLastNameException.getMessage().contains(lastNameExceptionMessage));
    }

    @Test
    void userAnagraphicExceptionsTest() {
        final Calendar minorAge = Calendar.getInstance();
        minorAge.add(Calendar.YEAR, -Anagraphic.MAJORITY_AGE);
        minorAge.add(Calendar.DATE, 1);

        final Exception birthDateException = assertThrows(AnagraphicValidationException.class,
                () -> Anagraphic.builder().firstName(UserTestUtils.TEST_FIRST_NAME)
                        .lastName(UserTestUtils.TEST_LAST_NAME).homeAddress(USER_ADDRESS).build());
        assertTrue(birthDateException.getMessage().contains("Birth date is not set"));

        final Exception legalAgeException = assertThrows(UserValidationException.class,
                () -> Anagraphic.builder().firstName(UserTestUtils.TEST_FIRST_NAME)
                        .lastName(UserTestUtils.TEST_LAST_NAME).birthDate(minorAge).homeAddress(USER_ADDRESS).build());
        assertTrue(legalAgeException.getMessage().contains("User is not of legal age"));

        final Exception homeAddressException = assertThrows(AnagraphicValidationException.class,
                () -> Anagraphic.builder().firstName(UserTestUtils.TEST_FIRST_NAME)
                        .lastName(UserTestUtils.TEST_LAST_NAME).birthDate(LEGAL_AGE).build());
        assertTrue(homeAddressException.getMessage().contains("Home address is not set"));
    }
}
