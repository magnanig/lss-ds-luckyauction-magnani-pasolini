/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.user.domain;

import it.unibo.luckyauction.user.domain.entity.User;
import it.unibo.luckyauction.user.domain.exception.UserValidationException;
import org.junit.jupiter.api.Test;

import java.util.function.Supplier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class UserTests {

    @Test
    void userEqualityTest() {
        final User someUser = UserTestUtils.createNewUser();
        final User newInstanceOfSomeUser = UserTestUtils.createNewUser();
        final User differentUser = UserTestUtils.createNewUser("newUsername");

        assertNotEquals(someUser, differentUser);
        assertEquals(someUser, newInstanceOfSomeUser);
    }

    @Test
    void userGetterTest() {
        final User user = UserTestUtils.createNewUser();

        assertEquals(UserTestUtils.USER_ID, user.getUserId());
        assertEquals(UserTestUtils.FIRST_USERNAME, user.getUsername());
        assertEquals(UserTestUtils.FIRST_USERNAME, user.getUsername());
        assertEquals(UserTestUtils.ANAGRAPHIC.get(), user.getAnagraphic());
        assertEquals(UserTestUtils.TEST_FIRST_NAME, user.getFirstName());
        assertEquals(UserTestUtils.TEST_LAST_NAME, user.getLastName());
        assertEquals(UserTestUtils.BIRTH_DATE_IN_MAJORITY_AGE.get(), user.getBirthDate());
        assertEquals(UserTestUtils.ADDRESS.get(), user.getHomeAddress());
        assertEquals(UserTestUtils.CONTACTS.get(), user.getContacts());
        assertEquals(UserTestUtils.PAYMENT_CARD.get(), user.getCard());
        assertEquals(UserTestUtils.AUCTIONS_HISTORY_ID, user.getAuctionsHistoryId());
        assertEquals(UserTestUtils.MOVEMENTS_HISTORY_ID, user.getMovementsHistoryId());
    }

    @Test
    void userExceptionsTest() {
        final Supplier<User.UserBuilder> preBuiltUser = () -> User.builder().username(UserTestUtils.FIRST_USERNAME);

        final Exception anagraphicException = assertThrows(UserValidationException.class, () -> preBuiltUser.get()
                .contacts(UserTestUtils.CONTACTS.get()).card(UserTestUtils.PAYMENT_CARD.get()).build());
        assertTrue(anagraphicException.getMessage().contains("Anagraphic is not set"));

        final Exception contactsException = assertThrows(UserValidationException.class, () -> preBuiltUser.get()
                .anagraphic(UserTestUtils.ANAGRAPHIC.get()).card(UserTestUtils.PAYMENT_CARD.get()).build());
        assertTrue(contactsException.getMessage().contains("Contacts is not set"));

        final Exception cardException = assertThrows(UserValidationException.class, () -> preBuiltUser.get()
                .anagraphic(UserTestUtils.ANAGRAPHIC.get()).contacts(UserTestUtils.CONTACTS.get()).build());
        assertTrue(cardException.getMessage().contains("Card is not set"));

        final Exception usernameException = assertThrows(UserValidationException.class, () -> User.builder()
                .anagraphic(UserTestUtils.ANAGRAPHIC.get()).contacts(UserTestUtils.CONTACTS.get()).build());
        assertTrue(usernameException.getMessage().contains("Username is not set"));
    }
}
