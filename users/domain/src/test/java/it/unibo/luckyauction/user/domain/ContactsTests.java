/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.user.domain;

import it.unibo.luckyauction.user.domain.exception.UserValidationException;
import it.unibo.luckyauction.user.domain.valueobject.Contacts;
import it.unibo.luckyauction.user.domain.valueobject.Phone;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.function.Supplier;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ContactsTests {

    private static final String EMAIL = "test@studio.unibo.it";
    private static final Supplier<Phone> CELLULAR_NUMBER = () -> new Phone("0123456789");
    private static final Supplier<Phone> TELEPHONE_NUMBER = () -> new Phone("0123456788");

    @Test
    void userContactsTest() {
        final Contacts userContacts = new Contacts(EMAIL, CELLULAR_NUMBER.get(), TELEPHONE_NUMBER.get());

        assertDoesNotThrow(() -> new Contacts(EMAIL, CELLULAR_NUMBER.get()),
                "The telephone number is optional");

        assertEquals(TELEPHONE_NUMBER.get().number(), userContacts.telephoneNumber().number());
        assertEquals(CELLULAR_NUMBER.get().number(), userContacts.cellularNumber().number());
        assertEquals(TELEPHONE_NUMBER.get(), userContacts.telephoneNumber());
        assertEquals(CELLULAR_NUMBER.get(), userContacts.cellularNumber());
        assertEquals(EMAIL, userContacts.email());
    }

    @Test
    void userPhoneExceptionTest() {
        final Exception cellularNumberException =
                assertThrows(UserValidationException.class, () -> new Phone(""));
        assertTrue(cellularNumberException.getMessage().contains("User's phone number is not set"));
    }

    @Test
    void userContactsExceptionTest() {
        final Exception emailContactException = assertThrows(UserValidationException.class,
                () -> new Contacts("", CELLULAR_NUMBER.get(), TELEPHONE_NUMBER.get()));
        assertTrue(emailContactException.getMessage().contains("User's email is not set"));

        final Exception cellularContactException = assertThrows(UserValidationException.class,
                () -> new Contacts(EMAIL, null, null));
        assertTrue(cellularContactException.getMessage().contains("User's cellular number is not set"));
    }

    @ParameterizedTest
    @ValueSource(strings = { "me@.com.my", "me123@.com", "me123@.com.com", "me@%*.com", "me..2002@gmail.com",
                "me.@gmail.com", "me@me@gmail.com", "me@gmail.com.1a" })
    void userEmailExceptionTest(final String candidateEmail) {
        final Exception exception = assertThrows(UserValidationException.class,
                () -> new Contacts(candidateEmail, CELLULAR_NUMBER.get(), null));
        assertTrue(exception.getMessage().contains("The specified email is not valid"));
    }
}
