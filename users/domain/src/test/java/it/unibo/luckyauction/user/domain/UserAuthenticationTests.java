/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.user.domain;

import it.unibo.luckyauction.user.domain.exception.UserAuthenticationValidationException;
import it.unibo.luckyauction.user.domain.valueobject.Role;
import it.unibo.luckyauction.user.domain.entity.UserAuthentication;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static it.unibo.luckyauction.user.domain.UserTestUtils.FIRST_USERNAME;
import static it.unibo.luckyauction.user.domain.UserTestUtils.PASSWORD;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class UserAuthenticationTests {

    @Test
    void userAuthenticationTest() {
        final UserAuthentication userAuthentication = new UserAuthentication(FIRST_USERNAME, PASSWORD,
                Role.USER, Role.ADMIN, Role.SYSTEM);

        assertEquals(FIRST_USERNAME, userAuthentication.getUsername());
        assertEquals(PASSWORD, userAuthentication.getEncryptedPassword());
        assertEquals(Set.of(Role.USER, Role.ADMIN, Role.SYSTEM), userAuthentication.getRoles());

        userAuthentication.changePassword("foo");
        assertEquals("foo", userAuthentication.getEncryptedPassword());
        userAuthentication.setRoles(Set.of(Role.USER));
        assertEquals(Set.of(Role.USER), userAuthentication.getRoles());
    }

    @Test
    void userAuthenticationFactoriesTest() {
        final UserAuthentication defaultUserAuthentication = new UserAuthentication(FIRST_USERNAME, PASSWORD);
        final UserAuthentication baseUserAuthentication = UserAuthentication.baseUser(FIRST_USERNAME, PASSWORD);
        final UserAuthentication adminUserAuthentication = UserAuthentication.adminUser(FIRST_USERNAME, PASSWORD);
        final UserAuthentication systemUserAuthentication = UserAuthentication.systemUser(FIRST_USERNAME, PASSWORD);

        assertEquals(Set.of(Role.USER), defaultUserAuthentication.getRoles());
        assertEquals(defaultUserAuthentication.getRoles(), baseUserAuthentication.getRoles());
        assertEquals(Set.of(Role.ADMIN), adminUserAuthentication.getRoles());
        assertEquals(Set.of(Role.SYSTEM), systemUserAuthentication.getRoles());
    }

    @Test
    void usernameExceptionTest() {
        final Exception nullUsernameException = assertThrows(UserAuthenticationValidationException.class,
                () -> UserAuthentication.baseUser(null, PASSWORD));
        assertTrue(nullUsernameException.getMessage().contains("Username is not set"));

        final Exception emptyUsernameException = assertThrows(UserAuthenticationValidationException.class,
                () -> UserAuthentication.baseUser("", PASSWORD));
        assertTrue(emptyUsernameException.getMessage().contains("Username is not set"));
    }

    @Test
    void encryptedPasswordExceptionTest() {
        final Exception nullPasswordException = assertThrows(UserAuthenticationValidationException.class,
                () -> UserAuthentication.baseUser(FIRST_USERNAME, null));
        assertTrue(nullPasswordException.getMessage().contains("Password is not set"));

        final Exception emptyPasswordException = assertThrows(UserAuthenticationValidationException.class,
                () -> UserAuthentication.baseUser(FIRST_USERNAME, ""));
        assertTrue(emptyPasswordException.getMessage().contains("Password is not set"));
    }
}
