/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.user.domain;

import it.unibo.luckyauction.user.domain.exception.UserValidationException;
import it.unibo.luckyauction.user.domain.valueobject.Address;
import it.unibo.luckyauction.user.domain.valueobject.Cap;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class AddressTests {

    private static final Cap CAP = new Cap("12345");
    private static final String STREET = "Via da qui";
    private static final String CITY = "Roma";

    @Test
    void userAddressTest() {
        final Address userAnagraphic = Address.builder().street(STREET).houseNumber(1)
                .city(CITY).cap(new Cap("12345")).province("RM").build();

        assertEquals(STREET, userAnagraphic.street());
        assertEquals(1, userAnagraphic.houseNumber());
        assertEquals(CITY, userAnagraphic.city());
        assertEquals("12345", userAnagraphic.cap().code());
        assertEquals("RM", userAnagraphic.province());
    }

    @Test
    void streetExceptionTest() {
        final Exception nullStreetException = assertThrows(UserValidationException.class, () -> Address.builder()
                .houseNumber(1).city(CITY).cap(CAP).province("RM").build());
        assertTrue(nullStreetException.getMessage().contains("User's street is not set"));

        final Exception emptyStreetException = assertThrows(UserValidationException.class, () -> Address.builder()
                .street("").houseNumber(1).city(CITY).cap(CAP).province("RM").build());
        assertTrue(emptyStreetException.getMessage().contains("User's street is not set"));
    }

    @Test
    void houseNumberExceptionTest() {
        final Exception exception = assertThrows(UserValidationException.class, () -> Address.builder()
                .street(STREET).city(CITY).cap(CAP).province("RN").build());
        assertTrue(exception.getMessage().contains("User's house number is not set"));
    }

    @Test
    void cityExceptionTest() {
        final Exception nullCityException = assertThrows(UserValidationException.class, () -> Address.builder()
                .street(STREET).houseNumber(1).cap(CAP).province("FC").build());
        assertTrue(nullCityException.getMessage().contains("User's city is not set"));

        final Exception emptyCityException = assertThrows(UserValidationException.class, () -> Address.builder()
                .street(STREET).houseNumber(1).cap(CAP).city("").province("FC").build());
        assertTrue(emptyCityException.getMessage().contains("User's city is not set"));
    }

    @Test
    void capExceptionTest() {
        final Exception wrongCapException = assertThrows(UserValidationException.class, () -> Address.builder()
                .street(STREET).houseNumber(1).city(CITY).province("RA").build());
        assertTrue(wrongCapException.getMessage().contains("User's CAP is not set in Address object"));

        final Exception nullCapException = assertThrows(UserValidationException.class, () -> new Cap(null));
        assertTrue(nullCapException.getMessage().contains("User's CAP is not set"));

        final Exception longCapException = assertThrows(UserValidationException.class, () -> new Cap("123456"));
        assertTrue(longCapException.getMessage().contains("User's CAP is not valid"));

        final Exception shortCapException = assertThrows(UserValidationException.class, () -> new Cap("123"));
        assertTrue(shortCapException.getMessage().contains("User's CAP is not valid"));
    }

    @Test
    void provinceExceptionTest() {
        final Exception nullProvinceException = assertThrows(UserValidationException.class, () -> Address.builder()
                .street(STREET).houseNumber(1).city(CITY).cap(CAP).build());
        assertTrue(nullProvinceException.getMessage().contains("User's province is not set in Address object"));

        final Exception emptyProvinceException = assertThrows(UserValidationException.class, () -> Address.builder()
                .street(STREET).houseNumber(1).city(CITY).cap(CAP).province("").build());
        assertTrue(emptyProvinceException.getMessage().contains("User's province is not set in Address object"));

        final Exception longProvinceException = assertThrows(UserValidationException.class, () -> Address.builder()
                .street(STREET).houseNumber(1).city(CITY).cap(new Cap("51089")).province("RME").build());
        assertTrue(longProvinceException.getMessage().contains("User's province is not valid"));

        final Exception shortProvinceException = assertThrows(UserValidationException.class, () -> Address.builder()
                .street(STREET).houseNumber(1).city(CITY).cap(new Cap("51088")).province("R").build());
        assertTrue(shortProvinceException.getMessage().contains("User's province is not valid"));
    }
}
