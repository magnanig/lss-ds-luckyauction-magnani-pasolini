/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.user.domain;

import it.unibo.luckyauction.user.domain.exception.PaymentCardValidationException;
import it.unibo.luckyauction.user.domain.valueobject.PaymentCard;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.Calendar;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class PaymentCardTests {

    private static final String FIRST_NAME = "Luca";
    private static final String LAST_NAME = "Rossi";
    private static final String CARD_NUMBER = UserTestUtils.CARD_NUMBER;
    private final Calendar expirationDate = Calendar.getInstance();

    @BeforeAll
    void configExpirationDate() {
        expirationDate.add(Calendar.YEAR, 3);
    }

    @Test
    void userPaymentCardTest() {
        final PaymentCard userPaymentCard = PaymentCard.builder().cardNumber(CARD_NUMBER).last4digits("2345")
                .cvv("938").expirationDate(expirationDate).firstName("tstFirst").lastName("tstLast").build();

        assertEquals(CARD_NUMBER, userPaymentCard.getCardNumber());
        assertEquals("2345", userPaymentCard.getLast4Digits());
        assertEquals("938", userPaymentCard.getCvv());
        assertEquals(expirationDate, userPaymentCard.getExpirationDate());
        assertEquals("tstFirst", userPaymentCard.getFirstName());
        assertEquals("tstLast", userPaymentCard.getLastName());
    }

    @Test
    void cardNumberExceptionTest() {
        final Exception nullCardNumberException = assertThrows(PaymentCardValidationException.class,
                () -> PaymentCard.builder()
                .expirationDate(expirationDate).firstName(FIRST_NAME).lastName(LAST_NAME).build());
        assertTrue(nullCardNumberException.getMessage().contains("User's card number is not set"));

        final Exception emptyCardNumberException = assertThrows(PaymentCardValidationException.class,
                () -> PaymentCard.builder().cardNumber("")
                        .expirationDate(expirationDate).firstName(FIRST_NAME).lastName(LAST_NAME).build());
        assertTrue(emptyCardNumberException.getMessage().contains("User's card number is not set"));
    }

    @Test
    void expirationDateExceptionTest() {
        final Exception exception = assertThrows(PaymentCardValidationException.class, () -> PaymentCard.builder()
                .cardNumber(CARD_NUMBER).firstName(FIRST_NAME).lastName(LAST_NAME).build());
        assertTrue(exception.getMessage().contains("Expiration date is not set"));
    }

    @Test
    void firstNameExceptionTest() {
        final Exception nullFirstNameException = assertThrows(PaymentCardValidationException.class,
                () -> PaymentCard.builder().cardNumber(CARD_NUMBER)
                        .expirationDate(expirationDate).lastName(LAST_NAME).build());
        assertTrue(nullFirstNameException.getMessage().contains("First name is not set"));

        final Exception emptyFirstNameException = assertThrows(PaymentCardValidationException.class,
                () -> PaymentCard.builder().cardNumber(CARD_NUMBER)
                        .expirationDate(expirationDate).firstName("").lastName(LAST_NAME).build());
        assertTrue(emptyFirstNameException.getMessage().contains("First name is not set"));
    }

    @Test
    void lastNameExceptionTest() {
        final Exception nullLastNameException = assertThrows(PaymentCardValidationException.class,
                () -> PaymentCard.builder().cardNumber(CARD_NUMBER)
                        .expirationDate(expirationDate).firstName(FIRST_NAME).build());
        assertTrue(nullLastNameException.getMessage().contains("Last name is not set"));

        final Exception emptyLastNameException = assertThrows(PaymentCardValidationException.class,
                () -> PaymentCard.builder().cardNumber(CARD_NUMBER)
                        .expirationDate(expirationDate).firstName(FIRST_NAME).lastName("").build());
        assertTrue(emptyLastNameException.getMessage().contains("Last name is not set"));
    }

    @Test
    void cvvExceptionTest() {
        final Exception longCVVException = assertThrows(PaymentCardValidationException.class,
                () -> PaymentCard.builder().cardNumber("1234123412341234").cvv("12345")
                        .expirationDate(expirationDate).firstName("tName").lastName("tLastName").build());
        assertTrue(longCVVException.getMessage().contains("CVV must be a code of three ciphers"));

        final Exception shortCVVException = assertThrows(PaymentCardValidationException.class,
                () -> PaymentCard.builder().cardNumber("0123012301230123").cvv("12")
                        .expirationDate(expirationDate).firstName("testName").lastName("testLastName").build());
        assertTrue(shortCVVException.getMessage().contains("CVV must be a code of three ciphers"));

        assertDoesNotThrow(() -> PaymentCard.builder().cardNumber("1234123412341234")
                .expirationDate(expirationDate).firstName("tName").lastName("tLastName").build());
    }
}
