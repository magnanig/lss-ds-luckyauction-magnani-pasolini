/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.user.domain;

import it.unibo.luckyauction.user.domain.entity.User;
import it.unibo.luckyauction.user.domain.valueobject.Address;
import it.unibo.luckyauction.user.domain.valueobject.Anagraphic;
import it.unibo.luckyauction.user.domain.valueobject.Cap;
import it.unibo.luckyauction.user.domain.valueobject.Contacts;
import it.unibo.luckyauction.user.domain.valueobject.PaymentCard;
import it.unibo.luckyauction.user.domain.valueobject.Phone;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.function.Supplier;

public final class UserTestUtils {

    private static final Calendar ACTUAL_DATE = Calendar.getInstance();
    private static final int YEAR_OF_EXPIRATION_DATE = 3;

    /**
     * User's id.
     */
    public static final String USER_ID = "1f28d209";
    /**
     * Username of the test user.
     */
    public static final String FIRST_USERNAME = "firstUsername";
    /**
     * User's password.
     */
    public static final String PASSWORD = "testPwd";
    /**
     * User's first name.
     */
    public static final String TEST_FIRST_NAME = "TestFirstName";
    /**
     * User's last name.
     */
    public static final String TEST_LAST_NAME = "TestLastName";
    /**
     * User's birthdate.
     */
    public static final Supplier<Calendar> BIRTH_DATE_IN_MAJORITY_AGE =
            () -> new GregorianCalendar(ACTUAL_DATE.get(Calendar.YEAR) - Anagraphic.MAJORITY_AGE,
                    ACTUAL_DATE.get(Calendar.MONTH), ACTUAL_DATE.get(Calendar.DATE) - 1);
    /**
     * User's card expiration date.
     */
    public static final Supplier<Calendar> CARD_EXPIRATION_DATE =
            () -> new GregorianCalendar(ACTUAL_DATE.get(Calendar.YEAR) + YEAR_OF_EXPIRATION_DATE,
                    ACTUAL_DATE.get(Calendar.MONTH), ACTUAL_DATE.get(Calendar.DATE));
    /**
     * User's email.
     */
    public static final String EMAIL = "ts@email.com";
    /**
     * User's cellular number.
     */
    public static final String CELLULAR_NUMBER = "0123456789";
    /**
     * User's telephone number.
     */
    public static final String TELEPHONE_NUMBER = "0123456788";
    /**
     * Street of the user's address.
     */
    public static final String STREET = "TestStreet";
    /**
     * House number of the user's address.
     */
    public static final int HOUSE_NUMBER = 1;
    /**
     * City of the user's address.
     */
    public static final String CITY = "TestCity";
    /**
     * Cap of the user's address.
     */
    public static final Supplier<Cap> CAP = () -> new Cap("51084");
    /**
     * Province of the user's address.
     */
    public static final String PROVINCE = "TC";
    /**
     * User's card number.
     */
    public static final String CARD_NUMBER = "0123012301230123";
    /**
     * Last four digits of the user's card.
     */
    public static final String LAST_4_DIGIT = CARD_NUMBER.substring(CARD_NUMBER.length() - 4);
    /**
     * Cvv of the user's card.
     */
    public static final String CVV = "294";
    /**
     * First name of the user's card.
     */
    public static final String CARD_FIRST_NAME = "cardFirstName";
    /**
     * Last name of the user's card.
     */
    public static final String CARD_LAST_NAME = "cardLastName";
    /**
     * User's auctions history id.
     */
    public static final String AUCTIONS_HISTORY_ID = "4a17gw3";
    /**
     * User's movements history id.
     */
    public static final String MOVEMENTS_HISTORY_ID = "d9y3hv8";
    /**
     * User's address.
     */
    public static final Supplier<Address> ADDRESS = () -> Address.builder().street(STREET).houseNumber(HOUSE_NUMBER)
            .city(CITY).cap(CAP.get()).province(PROVINCE).build();
    /**
     * User's anagraphic.
     */
    public static final Supplier<Anagraphic> ANAGRAPHIC = () -> Anagraphic.builder().firstName(TEST_FIRST_NAME)
            .lastName(TEST_LAST_NAME).birthDate(BIRTH_DATE_IN_MAJORITY_AGE.get()).homeAddress(ADDRESS.get()).build();
    /**
     * User's contacts.
     */
    public static final Supplier<Contacts> CONTACTS = () -> new Contacts(EMAIL, new Phone(CELLULAR_NUMBER),
            new Phone(TELEPHONE_NUMBER));
    /**
     * User's payment card.
     */
    public static final Supplier<PaymentCard> PAYMENT_CARD = () -> PaymentCard.builder().cardNumber(CARD_NUMBER)
            .last4digits(LAST_4_DIGIT).cvv(CVV).expirationDate(CARD_EXPIRATION_DATE.get())
            .firstName(CARD_FIRST_NAME).lastName(CARD_LAST_NAME).build();

    private UserTestUtils() { }

    /**
     * Create a new User instance. All objects in the new user instance
     * are all created with new instances. The method accepts one
     * optional parameter: username. If specified, the new user
     * will be created with this specific username, otherwise
     * the default username will be used.
     */
    public static User createNewUser(final String... username) {
        final String userUsername = username.length > 0 ? username[0] : FIRST_USERNAME;

        return User.builder()
                .userId(USER_ID)
                .username(userUsername)
                .anagraphic(ANAGRAPHIC.get())
                .contacts(CONTACTS.get())
                .card(PAYMENT_CARD.get())
                .auctionsHistoryId(AUCTIONS_HISTORY_ID)
                .movementsHistoryId(MOVEMENTS_HISTORY_ID)
                .build();
    }

    /**
     * Create a new user instance with the given userId and username.
     * @param userId    user's id
     * @param username  user's username
     * @return          the new user instance with desired parameters
     */
    public static User createNewUser(final String userId, final String username) {
        return User.builder()
                .userId(userId)
                .username(username)
                .anagraphic(ANAGRAPHIC.get())
                .contacts(CONTACTS.get())
                .card(PAYMENT_CARD.get())
                .auctionsHistoryId(AUCTIONS_HISTORY_ID)
                .movementsHistoryId(MOVEMENTS_HISTORY_ID)
                .build();
    }
}
