module lss.ds.luckyauction.magnani.pasolini.users.application.spring.main {
    requires lss.ds.luckyauction.magnani.pasolini.users.domain.main;
    requires lss.ds.luckyauction.magnani.pasolini.users.usecase.main;
    requires lss.ds.luckyauction.magnani.pasolini.users.controller.main;
    requires lss.ds.luckyauction.magnani.pasolini.users.adapter.main;
    requires lss.ds.luckyauction.magnani.pasolini.movements.adapter.main;
    requires lss.ds.luckyauction.magnani.pasolini.automations.adapter.main;
    requires org.apache.tomcat.embed.core;
    requires spring.web;
    requires spring.context;
    requires spring.beans;
    requires spring.boot;
    requires spring.boot.autoconfigure;
    requires lombok;
    requires lss.ds.luckyauction.magnani.pasolini.auctions.adapter.main;
    requires lss.ds.luckyauction.magnani.pasolini.utils.spring.main;
}