/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.user.application;

import it.unibo.luckyauction.user.adapter.NewUserWeb;
import it.unibo.luckyauction.user.adapter.UserAuthenticationWeb;
import it.unibo.luckyauction.user.adapter.UserWeb;
import it.unibo.luckyauction.user.controller.UserController;
import it.unibo.luckyauction.user.usecase.exception.UserAlreadyExistsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * The Spring Rest Controller for Users APIs.
 */
@RestController @RequestMapping("/api/users")
@CrossOrigin(allowCredentials = "true", originPatterns = "http://localhost:[*]")
public class UserRestController {

    private final UserController userController;

    /**
     * Create a new Spring user controller for the APIs rest.
     * @param userController    the canonical user controller
     */
    @Autowired
    public UserRestController(final UserController userController) {
        this.userController = userController;
    }

    /**
     * Get the list of all registered users.
     * @return      the list of all users
     */
    @GetMapping
    public List<UserWeb> getAllUsers() {
        return userController.getAllUsers().stream()
                .map(UserWeb::fromDomainUser)
                .collect(Collectors.toList());
    }

    /**
     * Create a new user.
     * @param user                          the user to be created
     * @return                              the just created user
     * @throws UserAlreadyExistsException   if a user with the specified username
     *                                      already exists
     */
    @PostMapping
    public UserWeb create(@RequestBody final NewUserWeb user) throws UserAlreadyExistsException {
        return UserWeb.fromDomainUser(userController.create(user.toDomainUser(), user.getPassword()));
    }

    /**
     * Delete the specified user.
     * @param username  the username of the user to delete
     * @return          the just deleted user, if any
     */
    @DeleteMapping
    public Optional<UserWeb> deleteUserByUsername(@RequestParam final String username) {
        return userController.deleteByUsername(username).map(UserWeb::fromDomainUser);
    }

    /**
     * Find a user authentication by its username.
     * @param username  the desired username
     * @return          the corresponding user authentication, if exists
     */
    @GetMapping("/authentication")
    public Optional<UserAuthenticationWeb> findUserAuthenticationByUsername(@RequestParam final String username) {
        return userController.findUserAuthenticationByUsername(username).map(UserAuthenticationWeb::fromDomain);
    }

    /**
     * Get the user object from the specified user id.
     * @param userId    the user id
     * @return          the corresponding user, if a user with such username exists
     */
    @GetMapping("/{userId}")
    public Optional<UserWeb> findById(@PathVariable final String userId) {
        return userController.findById(userId).map(UserWeb::fromDomainUser);
    }

    /**
     * Get the user object from the specified username.
     * @param username      the username
     * @return              the corresponding user, if a user with such username exists
     */
    @GetMapping("/username")
    public Optional<UserWeb> findByUsername(@RequestParam final String username) {
        return userController.findByUsername(username).map(UserWeb::fromDomainUser);
    }
}
