/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.user.application.config;

import it.unibo.luckyauction.auction.adapter.api.AuctionsHistoryApi;
import it.unibo.luckyauction.automation.adapter.api.AuctionAutomationApi;
import it.unibo.luckyauction.movement.adapter.api.MovementApi;
import it.unibo.luckyauction.spring.util.api.SpringAuctionAutomationApi;
import it.unibo.luckyauction.spring.util.api.SpringAuctionsHistoryApi;
import it.unibo.luckyauction.spring.util.api.SpringMovementApi;
import it.unibo.luckyauction.user.controller.UserController;
import it.unibo.luckyauction.user.controller.config.UserConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UserSpringConfig {

    @Bean
    public MovementApi movementApi() {
        return new SpringMovementApi();
    }

    @Bean
    public AuctionsHistoryApi auctionsHistoryApi() {
        return new SpringAuctionsHistoryApi();
    }

    @Bean
    public AuctionAutomationApi auctionAutomationApi() {
        return new SpringAuctionAutomationApi();
    }

    /**
     * Build the user controller of the application, capable of orchestrating users.
     * See {@link UserController} for more details.
     * @return      the just built user controller
     */
    @Bean
    public UserController userController(final MovementApi movementApi, final AuctionsHistoryApi auctionsHistoryApi,
                                         final AuctionAutomationApi auctionAutomationApi) {
        return new UserConfig().userController(movementApi, auctionsHistoryApi, auctionAutomationApi);
    }
}
