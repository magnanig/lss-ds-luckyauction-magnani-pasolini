/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.unibo.luckyauction.user.application.config;

import it.unibo.luckyauction.spring.util.http.ErrorResponse;
import it.unibo.luckyauction.spring.util.http.HttpRequestException;
import it.unibo.luckyauction.user.domain.exception.AnagraphicValidationException;
import it.unibo.luckyauction.user.domain.exception.PaymentCardValidationException;
import it.unibo.luckyauction.user.domain.exception.UserAuthenticationValidationException;
import it.unibo.luckyauction.user.domain.exception.UserValidationException;
import it.unibo.luckyauction.user.usecase.exception.UserAlreadyExistsException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * This class contains users exception handlers, which specify
 * how the Spring server should respond to the client when an exception is thrown.
 */
@ControllerAdvice
public class ExceptionsHandlers {

    /**
     * Handler for {@link AnagraphicValidationException}.
     * @param ex    the thrown exception reference
     * @return      the response with status code 400 (bad request), containing the
     *              error message in the body
     */
    @ExceptionHandler(AnagraphicValidationException.class)
    public ResponseEntity<String> handleAnagraphicValidationException(final AnagraphicValidationException ex) {
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(ex.getMessage());
    }

    /**
     * Handler for {@link PaymentCardValidationException}.
     * @param ex    the thrown exception reference
     * @return      the response with status code 400 (bad request), containing the
     *              error message in the body
     */
    @ExceptionHandler(PaymentCardValidationException.class)
    public ResponseEntity<String> handlePaymentCardValidationException(final PaymentCardValidationException ex) {
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(ex.getMessage());
    }

    /**
     * Handler for {@link UserAuthenticationValidationException}.
     * @param ex    the thrown exception reference
     * @return      the response with status code 400 (bad request), containing the
     *              error message in the body
     */
    @ExceptionHandler(UserAuthenticationValidationException.class)
    public ResponseEntity<String> handleUserAuthenticationValidationException(
            final UserAuthenticationValidationException ex) {
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(ex.getMessage());
    }

    /**
     * Handler for {@link UserValidationException}.
     * @param ex    the thrown exception reference
     * @return      the response with status code 400 (bad request), containing the
     *              error message in the body
     */
    @ExceptionHandler(UserValidationException.class)
    public ResponseEntity<String> handleUserValidationException(final UserValidationException ex) {
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(ex.getMessage());
    }

    /**
     * Handler for {@link UserAlreadyExistsException}, raised during the user
     * registration, if a user with the specified username already exists.
     * @param ex    the thrown exception reference
     * @return      the response with status code 400 (bad request), containing the
     *              error message in the body
     */
    @ExceptionHandler(UserAlreadyExistsException.class)
    public ResponseEntity<String> handleUserAlreadyExistsException(final UserAlreadyExistsException ex) {
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(ex.getMessage());
    }

    /**
     * Handler for a {@link HttpRequestException}, i.e. an exception occurring
     * while forwarding a request to another microservice.
     * @param ex    the thrown exception reference
     * @return      the response with the status code got while forwarding the
     *              request, and the error message in the body
     */
    @ExceptionHandler(HttpRequestException.class)
    public ResponseEntity<String> handleHttpRequestException(final HttpRequestException ex) {
        final ErrorResponse errorResponse = ex.getErrorResponse();
        return ResponseEntity
                .status(errorResponse.httpResponseCode())
                .body(errorResponse.message());
    }

    /**
     * Handler for a generic {@link Exception}. This handler will be triggered
     * only if none of the others matches with the thrown exception.
     * @param ex    the thrown exception reference
     * @return      the response with status code 500, containing the error
     *              message in the body
     */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<String> handleException(final Exception ex) {
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(ex.getMessage());
    }
}
