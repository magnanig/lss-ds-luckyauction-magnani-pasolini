$(document).ready(function(){
    $.ajax({
        url: baseUrl() + "/api/users/username",
        type: "GET",
        headers: {"Authorization": "Bearer " + localStorage.getItem(access_token)},
        success: function (result, status, xhr) {
            compileUserInformationForm(result);
            document.getElementById("userUsername").innerHTML = "Dati utente - " +
            result.username;
            document.getElementsByTagName("html")[0].style.visibility = "visible";
        },
        error: function (error) {
            alert("Profilo - Per accedere al profilo effettuare il login");
            logout();
        }
    });
});

function compileUserInformationForm(json) {
    var date, month, str;
    document.getElementById("firstName").value = json.anagraphic.firstName;
    document.getElementById("lastName").value = json.anagraphic.lastName;
    date = new Date(json.anagraphic.birthDate);
    month = date.getMonth() + 1;
    str = date.getDate() + "/" + month + "/" + date.getFullYear();
    document.getElementById("birthDate").value = str;
    document.getElementById("street").value = json.anagraphic.homeAddress.street;
    document.getElementById("houseNumber").value = json.anagraphic.homeAddress.houseNumber;
    document.getElementById("city").value = json.anagraphic.homeAddress.city;
    document.getElementById("cap").value = json.anagraphic.homeAddress.cap.code;
    document.getElementById("province").value = json.anagraphic.homeAddress.province;
    document.getElementById("email").value = json.contacts.email;
    document.getElementById("cellularNumber").value = json.contacts.cellularNumber.number;
    if("telephoneNumber" in json.contacts && json.contacts.telephoneNumber !== null) {
        document.getElementById("telephoneNumber").value = json.contacts.telephoneNumber.number;
    }
    document.getElementById("cardNumber").value = "**** " + json.card.last4digits;
    date = new Date(json.card.expirationDate);
    month = date.getMonth() + 1;
    str = month + "/" + date.getFullYear();
    document.getElementById("expirationDate").value = str;
    document.getElementById("holderFirstName").value = json.card.firstName;
    document.getElementById("holderLastName").value = json.card.lastName;
}