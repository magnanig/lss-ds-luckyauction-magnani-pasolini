const access_token = "luckyauctionAccessToken";
const user_logged = "luckyauctionUserLogged"
const protocol = "http";
const host = "localhost";
const port = 8080;

const socketProtocol = "http";
const auctionsSocketHost = "localhost";
const bidsSocketHost = "localhost";
const automationsSocketHost = "localhost";

function baseUrl() {
    return protocol + "://" + host + ":" + port;
}

function logout() {
    localStorage.removeItem(user_logged);
    localStorage.removeItem(access_token);
    window.location.replace("../login/login.html");
}