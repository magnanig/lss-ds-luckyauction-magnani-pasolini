var filterFunction;

function filterAuctions(event) {
    event.preventDefault();
    if (filterIsSet()) {
        deleteOldTimers();
        composeDocument(auctions.filter(filterFunction).sort(getSortFunction()));
        setAllTimer();
    } else {
        deleteOldTimers();
        composeDocument(auctions.sort(getSortFunction()));
        setAllTimer();
    }
}

function deleteOldTimers() {
    expectedClosingTimes = new Array();
    for (let i = 0; i < countdownTimers.length; i++) {
        clearInterval(countdownTimers[i]);
    }
}

function filterIsSet() {
    var category = document.getElementById("product-category").value;
    var state = document.getElementById("product-state").value;
    if (category !== "0" && state !== "0") {
        filterFunction = (auction) => auction.product.category === category &&
            auction.product.state === state;
        return true;
    } else if (category !== "0") {
        filterFunction = (auction) => auction.product.category === category;
        return true;
    } else if (state !== "0") {
        filterFunction = (auction) => auction.product.state === state;
        return true;
    }
    return false;
}

function getSortFunction() {
    var orderBy = document.getElementById("order").value;
    var defaultFunction = (a, b) => new Date(b.creationTime).getTime() - new Date(a.creationTime).getTime();
    
    if (orderBy !== "0") {
        if (orderBy === "EXPIRING") {
            return (a, b) => new Date(a.expectedClosingTime).getTime() - new Date(b.expectedClosingTime).getTime();
        }
        if (orderBy === "JUST_CREATED") {
            return defaultFunction;
        }
        if (orderBy === "HIGHER_PRICE") {
            return (a, b) => getAuctionPrice(b) - getAuctionPrice(a);
        }
        if (orderBy === "LOWEST_PRICE") {
            return (a, b) => getAuctionPrice(a) - getAuctionPrice(b);
        }
    } else {
        return defaultFunction;
    }
}

function getAuctionPrice(auction) {
    if ("actualBid" in auction && auction.actualBid !== null) {
        return Number(auction.actualBid.amount).toFixed(2);
    } else {
        return Number(auction.startingPrice).toFixed(2);
    }
}