var auctions = new Array();
var countdownTimers = new Array();
var expectedClosingTimes = new Array();

$(document).ready(function() {
    showOrHideLogout();
    $.ajax({
        url: baseUrl() + "/api/auctions/active",
        type: "GET",
        crossDomain: true,
        data: {excluded: localStorage.getItem(user_logged)},
        success: function (auctionsJson, status, xhr) {
            auctions = auctionsJson.sort(function(a, b) {
                return new Date(b.creationTime).getTime() - new Date(a.creationTime).getTime();
            });
            composeDocument(auctions);
            setAllTimer();
            document.getElementsByTagName("html")[0].style.visibility = "visible";
        },
        error: function (error) {
            alert("Error (" + error.status + "): " + error.responseText);
        }
    });
});

function showOrHideLogout() {
    const divLogout = document.getElementById("divLogout");
    if (localStorage.getItem(access_token) == null) {
        divLogout.innerHTML = '';
    }
}

function setAllTimer() {
    for (let i = 0; i < expectedClosingTimes.length; i++) {
        var t1 = new Date();
        var t2 = new Date(expectedClosingTimes[i]);
        expectedClosingTimes[i] = Math.round((t2.getTime() - t1.getTime()) / 1000); // duration in seconds
        // the remaining time is displayed immediately without waiting a minute
        updateTimer(i)
        countdownTimers[i] = setInterval(() => updateTimer(i), 60000);
    }
}

function updateTimer(timerIndex) {
    var totalSeconds = expectedClosingTimes[timerIndex];
    var days        = Math.floor(totalSeconds / 86400); // gg:hh:mm:ss
    var hoursLeft   = Math.floor((totalSeconds) - (days * 86400));
    var hours       = Math.floor(hoursLeft / 3600);
    var minutesLeft = Math.floor((hoursLeft) - (hours * 3600));
    var minutes     = Math.floor(minutesLeft / 60);
    document.getElementById("auction-timer-" + timerIndex).innerHTML = days + "g " +
    hours + "h " + minutes + "m ";
    if (totalSeconds <= 0) {
        clearInterval(countdownTimers[timerIndex]);
        document.getElementById("auction-timer-" + timerIndex).innerHTML = "finished";
    } else {
        expectedClosingTimes[timerIndex] -= 60;
    }
}

function composeDocument(auctions) {
    var htmlAuctions = "";
    for (var i = 0; i < auctions.length; i++) {
        var auction = auctions[i];
        expectedClosingTimes[i] = auction.expectedClosingTime;
        var htmlAuction = "<a href='../auction/auction.html?auctionId=" + 
        auction.auctionId + "'>" +
        "<div class='auction-container'>" + "<div class='auction-description'>" +
            "<div class='auction-image'>" +
                "<img src='" + auction.product.image + "'/>" +
            "</div>" +
            "<div class='auction-information'>" +
                "<p class='auction-title'>" + auction.product.name + "</p>" +
                "<p class='auction-creation-time'>Creata il: " + 
                getFormattedDate(auction.creationTime) + "</p>" +
                "<div id='auction-remaining-time'>" +
                    "<p>Tempo rimanente: </p>" +
                    "<p id='auction-timer-" + i +"'></p>" +
                "</div>";
                htmlAuction += "<p class='auction-price'>";
                if ("actualBid" in auction && auction.actualBid !== null) {
                    htmlAuction += Number(auction.actualBid.amount).toFixed(2) + " €";
                } else {
                    htmlAuction += Number(auction.startingPrice).toFixed(2) + " €";
                }
                htmlAuction += " - " + auction.bidsHistory.bids.length + " offerte" + "</p>";
            htmlAuction += "</div>" + "</div>" + "</div>" + "</a>";
        htmlAuctions += htmlAuction;
    }
    if (htmlAuctions == "") {
        htmlAuctions = "<p>Non sono presenti aste in corso</p>"
    }
    document.getElementById("auctions-container").innerHTML = htmlAuctions;
}

function getFormattedDate(date) {
    var fullDate = new Date(date);

    var dateOption = {'dateStyle': 'medium'};
    var date = fullDate.toLocaleString('it-IT', dateOption);

    var timeOption = {'timeStyle': 'medium'};
    var time = fullDate.toLocaleString('it-IT', timeOption);

    return date + " alle " + time;
}