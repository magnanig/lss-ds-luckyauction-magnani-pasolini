$(document).ready(function() {
    whenDocumentIsReady();
});

function whenDocumentIsReady() {
    $.ajax({
        url: baseUrl() + "/api/movements/histories",
        type: "GET",
        headers: {"Authorization": "Bearer " + localStorage.getItem(access_token)},
        success: function (movementsHistories, status, xhr) {
            document.getElementById("title-page").innerHTML = "Saldo attuale: " + 
            Number(movementsHistories.budget.value).toFixed(2) + " € - utente " +
            localStorage.getItem(user_logged);
            addTransactionsToDiv(movementsHistories.transactions.sort(function(a, b) {
                return new Date(b.timestamp).getTime() - new Date(a.timestamp).getTime();
            }));
            addFreezesToDiv(movementsHistories.freezes.sort(function(a, b) {
                return new Date(b.timestamp).getTime() - new Date(a.timestamp).getTime();
            }));
            openBoxById("divTransactions");
            document.getElementsByTagName("html")[0].style.visibility = "visible";
        },
        error: function (error) {
            alert("Gestione movimenti - Per consultare i propri movimenti effettuare il login");
            logout();
        }
    });
}

// callback when you want to recharge the user budget
function recharge(event) {
    event.preventDefault();
    if (document.getElementById("rechargeForm").checkValidity()) {
        $.ajax({
            url: baseUrl() + "/api/movements/transactions/recharge",
            type: "POST",
            headers: {"Authorization": "Bearer " + localStorage.getItem(access_token)},
            data: getRechargeParameters(),
            success: function (result, status, xhr) {
                alert("Ricarica avvenuta con successo");
                location.reload();
            },
            error: function (error) {
                alert("Error (" + error.status + "): " + error.responseText);
            }
        });
    } else {
        alert("Error: enter an amount greater than or equal to 0.01 €");
    }
}

// callback when you want to withdraw the user budget
function withdrawal(event) {
    event.preventDefault();
    if (document.getElementById("withdrawalForm").checkValidity()) {
        $.ajax({
            url: baseUrl() + "/api/movements/transactions/withdrawal",
            type: "POST",
            headers: {"Authorization": "Bearer " + localStorage.getItem(access_token)},
            data: {amount: $("#amountWithdrawal").val()},
            success: function (result, status, xhr) {
                alert("Prelievo avvenuto con successo");
                location.reload();
            },
            error: function (error) {
                alert("Error (" + error.status + "): " + error.responseText);
            }
        });
    } else {
        alert("Error: enter an amount greater than or equal to 0.01 €");
    }
}

// generate the body of the http request 
// when you want to recharge the user budget
function getRechargeParameters() {
    var amountValue = $("#amountRecharge").val();
    var descriptionValue = $("#txtArea-description").val();
    return descriptionValue === "" || descriptionValue == null
        ? {amount: amountValue}
        : {amount: amountValue, description: descriptionValue};
}

function addFreezesToDiv(freezes) {
    var htmlFreezes = "";
    for (var i = 0; i < freezes.length; i++) {
        var freezing = freezes[i];

        var htmlFreezing = "<a href='../auction/auction.html?auctionId="
        + freezing.auctionId + "'>";

        htmlFreezing += "<div class='movement-information'>" +
        "<p class='movement-type'>Tipo di transazione: " + freezing.type + "</p>" +
        "<p>" + getFormattedDate(freezing.timestamp) + "</p>" +
        "<p>Prodotto: " + freezing.productName + "</p>"
        
        htmlFreezing += "" +
        "<p>" + Number(freezing.amount).toFixed(2) + " €</p>";
        htmlFreezing += "</div></a>";
        htmlFreezes += htmlFreezing;
    }
    if (htmlFreezes == "") {
        htmlFreezes = "<p>Non sono presenti congelamenti</p>"; 
    }
    document.getElementById("divFreezes").innerHTML = htmlFreezes;
}

// populate divs of transactions and freezes
function addTransactionsToDiv(transactions) {
    var htmlTransactions = "";
    for (var i = 0; i < transactions.length; i++) {
        var transaction = transactions[i];
        var transactionAmount = Number(transaction.amount);
        var isAwardedTransaction = transaction.type === "AWARDED_AUCTION" ? true : false;
        var htmlTransaction = isAwardedTransaction
        ? "<a href='../auction/auction.html?auctionId=" + transaction.auctionId + "'>" : "";

        htmlTransaction += "<div class='movement-information'>" +
        "<p class='movement-type'>Tipo di transazione: " + transaction.type + "</p>" +
        "<p>" + getFormattedDate(transaction.timestamp) + "</p>";

        if (isAwardedTransaction) {
            htmlTransaction += composeAwardedAuctionInfo(transaction.productName, 
                transaction.sellerUsername, transaction.buyerUsername);
        }

        htmlTransaction += Math.sign(transactionAmount) >= 0
        ? "<p>+" + transactionAmount.toFixed(2) + " €</p>"
        : "<p>" + transactionAmount.toFixed(2) + " €</p>";

        if ("description" in transaction && transaction.description !== null) {
            htmlTransaction += "<p>Descrizione: " + transaction.description + "</p>";
        }
        htmlTransaction += isAwardedTransaction ? "</div></a>" : "</div>";
        htmlTransactions += htmlTransaction;
    }

    if (htmlTransactions == "") {
        htmlTransactions = "<p>Non sono presenti transazioni</p>";  
    }
    document.getElementById("divTransactions").innerHTML = htmlTransactions;
}

function composeAwardedAuctionInfo(product, seller, buyer) {
    var htmlInfo = "<p>Prodotto: " + product + "</p>";
    if (localStorage.getItem(user_logged) === seller) {
        htmlInfo += "<p>Vincitore: " + buyer + " user</p>";
    } else {
        htmlInfo += "<p>Venditore: " + seller + " user</p>";
    }
    return htmlInfo;
}

// validate the (insertion of) amount in the "euro" currency
var validateAmount = function(e) {
    var prev = e.getAttribute("data-prev");
    prev = (prev != '') ? prev : '';
    if (Math.round(e.value * 100) / 100 != e.value) {
        e.value = prev;
    }
    e.setAttribute("data-prev", e.value)
}

function getFormattedDate(date) {
    var fullDate = new Date(date);

    var dateOption = {'dateStyle': 'medium'};
    var date = fullDate.toLocaleString('it-IT', dateOption);

    var timeOption = {'timeStyle': 'medium'};
    var time = fullDate.toLocaleString('it-IT', timeOption);

    return date + " alle " + time;
}

function openBoxById(boxId) {
    var i, tabcontent;
    tabcontent = document.getElementsByClassName("box");
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
    }
    document.getElementById(boxId).style.display = "block";
}