$(document).ready(function() {
    document.getElementsByTagName("html")[0].style.visibility = "visible";
});

function signup(event) {
    event.preventDefault();
    if (checkForm()) {
        $.ajax({
            url: baseUrl() + "/api/users/signup",
            type: "POST",
            crossDomain: true,
            contentType: "application/json",
            data: generateBody(),
            success: function(result, status, xhr) {
                login(event, $('#username').val(), $('#password').val());
            },
            error: function(error) {
                alert("Error (" + error.status + "): " + error.responseText);
            }
        });
    }
}

function checkForm() {    
    if (!document.querySelector('form').checkValidity()) {
        alert("Error: fill in all mandatory fields marked with (*)");
        return false;
    }

    if (monthDiff(new Date(), new Date($("#expirationDate").val())) < 1) {
        alert("Error: expired card");
        return false;
    }

    if ($("#password").val() !== $("#passwordRepeat").val()) {
        alert("Error: passwords don't match");
        return false;
    }

    return true;
}

function monthDiff(d1, d2) {
    var months;
    months = (d2.getFullYear() - d1.getFullYear()) * 12;
    months -= d1.getMonth();
    months += d2.getMonth();
    return months <= 0 ? 0 : months;
}

function generateBody() {
    var jsonBody = {
        username: $("#username").val(),
        password: $("#password").val(),
        anagraphic: {
            firstName: $("#firstName").val(),
            lastName: $("#lastName").val(),
            birthDate: $("#birthDate").val(),
            homeAddress: {
                street: $("#street").val(),
                houseNumber: $("#houseNumber").val(),
                city: $("#city").val(),
                cap: $("#cap").val(),
                province: $("#province").val()
            }
        },
        contacts: {
            email: $("#email").val(),
            cellularNumber: $("#cellularNumber").val()
        },
        card: {
            cardNumber: $("#cardNumber").val(),
            cvv: $("#cvv").val(),
            expirationDate: $("#expirationDate").val() + "-01",
            firstName: $("#holderFirstName").val(),
            lastName: $("#holderLastName").val()
        }
    }

    var telephoneNumber = $("#telephoneNumber").val();
    if (telephoneNumber) {
        jsonBody.contacts["telephoneNumber"] = telephoneNumber;
    }

    return JSON.stringify(jsonBody);
}