var automationId;
var auctionId;
var sellerUser;
var actualPrice;
var numBids;
var isOngoing;
var isAutomationActive;

// callback before showing the form
function configAuctionAutomaionForm() {
    if (!isOngoing || localStorage.getItem(user_logged) === sellerUser) {
        removeForms();
    } else {
        $.ajax({
            url: baseUrl() + "/api/auctions/automations/" + auctionId,
            type: "GET",
            headers: {"Authorization": "Bearer " + localStorage.getItem(access_token)},
            success: function (auctionAutomation, status, xhr) {
                if (auctionAutomation) {
                    configFormWithAutomationInProgress(auctionAutomation);
                } else {
                    configFormWithoutAutomationInProgress();
                }
            },
            error: function (error) {
                alert("Error (" + error.status + "): " + error.responseText);
            }
        });
    }
}

// callback when create a new auciton automation
function setAutomation(event) {
    event.preventDefault();
    if (document.getElementById("auctionAutomationForm").checkValidity()) {
        $.ajax({
            url: baseUrl() + "/api/auctions/automations",
            type: "PUT",
            contentType: "application/json",
            headers: {"Authorization": "Bearer " + localStorage.getItem(access_token)},
            data: generateJsonPreferences(),
            success: function (auctionAutomation, status, xhr) {
                configFormWithAutomationInProgress(auctionAutomation);
            },
            error: function(res) {
                alert("Error (" + res.status + "): " + res.responseText);
            }
        });
    } else {
        alert("Error: fill in all mandatory fields marked with (*)");
    }
}

// callback when remove auciton automation
function removeAutomation(event) {
    event.preventDefault();
    $.ajax({
        url: baseUrl() + "/api/auctions/automations/" + auctionId,
        type: "DELETE",
        headers: {"Authorization": "Bearer " + localStorage.getItem(access_token)},
        success: function (userAuctionsAutomations, status, xhr) {
            // nothing: listening for events from the automations socket
        },
        error: function (error) {
            alert("Error (" + error.status + "): " + error.responseText);
        }
    });
}

function configFormWithAutomationInProgress(auctionAutomation) {
    automationId = auctionAutomation.auctionAutomationId;
    isAutomationActive = true;
    setAutomationFieldToReadOnly(auctionAutomation);
    document.getElementById("automationBtn").innerHTML = "Rimuovi automazione asta";
    document.getElementById("automationBtn").setAttribute('onclick', "removeAutomation(event)");

    // connect the socket to handle automation events
    var automationsSocket = new Socket(automationsSocketHost, automationPort, automationsBaseNamespace + "/" + automationId);
    automationsSocket.addEventListener("STOP_AUTOMATION", () => {
        configFormWithoutAutomationInProgress();
    });
}

function configFormWithoutAutomationInProgress() {
    isAutomationActive = false;
    setAutomationFieldConstraints();
    document.getElementById("automationBtn").innerHTML = "Automatizza asta";
    document.getElementById("automationBtn").setAttribute('onclick', "setAutomation(event)");
}

function setAutomationFieldToReadOnly(automation) {
    $('#auctionAutomationForm input').attr('readonly', 'readonly');

    document.getElementById("limitPrice").value = automation.limitPrice;
    document.getElementById("priceIncrease").value = automation.priceIncrease;
    document.getElementById("raiseDelay").value = durationToTime(automation.raiseDelay);
    document.getElementById("automationEnd").value = automation.automationEnd
    ? getLocaleDate(automation.automationEnd)
    : "";
}

function setAutomationFieldConstraints() {
    $('#auctionAutomationForm input').removeAttr('readonly');

    document.getElementById("raiseDelay").value = "00:00:01";
    document.getElementById("limitPrice").min = actualPrice + 2;
    document.getElementById("limitPrice").value = document.getElementById("limitPrice").min;

    document.getElementById("priceIncrease").min = 1;
    document.getElementById("priceIncrease").value = 1;

    var minDate = new Date();
    minDate.setMinutes(minDate.getMinutes() + 1);
    var maxDate = new Date();
    maxDate.setDate(maxDate.getDate() + 14);
    document.getElementById("automationEnd").setAttribute("min", getDateStringFormat(minDate));
    document.getElementById("automationEnd").setAttribute("max", getDateStringFormat(maxDate));
}

// called whenever the "priceIncrease" field is changed
var validatePriceIncrease = function(e) {
    var maxValue = Number(document.getElementById("limitPrice").value) - actualPrice;
    var prev = e.getAttribute("data-prev");
    // limit to two decimal places
    prev = (prev != '') ? prev : '';
    if (Math.round(e.value * 100) / 100 != e.value) {
        e.value = prev;
    }
    e.setAttribute("data-prev", e.value)
    // limit price increase
    if (e.value > maxValue) {
        e.value = maxValue;
    }
}

function generateJsonPreferences() {
    var raiseDelayValue = document.getElementById("raiseDelay").value;
    var raiseDelay = raiseDelayValue.split(':');
    var raiseDelayInSeconds = (+raiseDelay[0]) * 60 * 60 + 
    (+raiseDelay[1]) * 60 + (+raiseDelay[2]);
    var jsonBody = {
        auctionId: auctionId,
        limitPrice: document.getElementById("limitPrice").value,
        priceIncrease: document.getElementById("priceIncrease").value,
        raiseDelay: {
            seconds: raiseDelayInSeconds
        },
    }
    var automationEnd = document.getElementById("automationEnd").value;
    if (automationEnd) {
        jsonBody["automationEnd"] = new Date(automationEnd);
    }
    return JSON.stringify(jsonBody);
}

function getDateStringFormat(dateObject) {
    var dd = dateObject.getDate();
    dd = dd < 10 ? "0" + dd : dd;
    var mm = dateObject.getMonth() + 1; //January is 0 so need to add 1
    mm = mm < 10 ? "0" + mm : mm;
    var yyyy = dateObject.getFullYear();
    return yyyy + "-" + mm + "-" + dd;
}

function disableFormsButtons() {
    document.getElementById("automationBtn").disabled = true;
    document.getElementById("newBidBtn").disabled = true;
}

function enableFormsButtons() {
    document.getElementById("automationBtn").disabled = false;
    document.getElementById("newBidBtn").disabled = false;
}

function removeForms() {
    document.getElementById("divNewBid").remove();
    document.getElementById("divConfigAutomation").remove();
}

function durationToTime(duration) {
    return ("0" + duration.hours).slice(-2) + ":" + ("0" + duration.minutes).slice(-2) 
    + ":" + ("0" + duration.seconds).slice(-2);
}