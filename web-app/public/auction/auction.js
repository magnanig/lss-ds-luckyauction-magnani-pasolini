const params = new Proxy(new URLSearchParams(window.location.search), {
    get: (searchParams, prop) => searchParams.get(prop),
});
auctionId = params.auctionId;
var countdownTimer;
var expectedClosingTime;

$(document).ready(function() {
    $.ajax({
        url: baseUrl() + "/api/auctions/" + auctionId,
        type: "GET",
        headers: {"Authorization": "Bearer " + localStorage.getItem(access_token)},
        success: function (auction, status, xhr) {
            if ("actualBid" in auction && auction.actualBid !== null) {
                actualPrice = Number(auction.actualBid.amount);
            } else {
                actualPrice = Number(auction.startingPrice);
            }
            numBids = auction.bidsHistory.bids.length;
            sellerUser = auction.sellerUsername;
            isOngoing = "closingTime" in auction && auction.closingTime !== null ? false : true;
            expectedClosingTime = auction.expectedClosingTime;
            configAuctionAutomaionForm();
            composeDocument(auction);
            if (isOngoing) {
                setTimer();
                startSockets();
            }
            document.getElementsByTagName("html")[0].style.visibility = "visible";
        },
        error: function (error) {
            alert("Asta - Per visualizzare i dettagli dell'asta effettuare il login");
            logout();
        }
    });
});

function addNewBid(event) {
    event.preventDefault();
    if (isValidBid()) {
        $.ajax({
            url: baseUrl() + "/api/auctions/" + auctionId + "/bids",
            type: "POST",
            headers: {"Authorization": "Bearer " + localStorage.getItem(access_token)},
            data: {amount: $("#bidAmount").val()},
            success: function (result, status, xhr) {
                document.getElementById("bidAmount").value = "";
            },
            error: function(error) {
                alert("Rilancio offerta - Error (" + error.status + "): " + error.responseText);
            }
        });
    }
}

function isValidBid() {
    if (!document.getElementById("newBidForm").checkValidity()) {
        alert("Error: fill in all mandatory fields marked with (*)");
        return false;
    }
    if (document.getElementById("bidsTable").rows.length > 1) {
        var lastBider = document.getElementById("bidsTable").rows[1].cells[0].innerHTML;
        if (localStorage.getItem(user_logged) === lastBider) {
            alert("Error: user can't raise since the last bid is already his");
            return false;
        }
    }
    return true;
}

function setTimer() {
    var t1 = new Date();
    var t2 = new Date(expectedClosingTime);
    // set expected closing time in seconds
    expectedClosingTime = Math.round((t2.getTime() - t1.getTime()) / 1000);
    // the remaining time is displayed immediately without waiting a second
    updateTimer();
    if (countdownTimer) {
        clearInterval(countdownTimer);
    }
    countdownTimer = setInterval(() => updateTimer(), 1000);
}

function updateTimer() {
    var totalSeconds = expectedClosingTime;
    var days        = Math.floor(totalSeconds / 86400); // gg:hh:mm:ss
    var hoursLeft   = Math.floor((totalSeconds) - (days * 86400));
    var hours       = Math.floor(hoursLeft / 3600);
    var minutesLeft = Math.floor((hoursLeft) - (hours * 3600));
    var minutes     = Math.floor(minutesLeft / 60);
    var seconds = totalSeconds % 60;
    document.getElementById("auction-timer-0").innerHTML = days + "g " +
    hours + "h " + minutes + "m " + seconds + "s";
    if (totalSeconds <= 0) {
        clearInterval(countdownTimer);
        disableFormsButtons();
        document.getElementById("auction-timer-0").innerHTML = "Ending...";
    } else {
        expectedClosingTime--;
    }
}

function composeDocument(auction) {
    addAuctionInfo(auction);
    if ("actualBid" in auction && auction.actualBid !== null) {
        composeBidTable(auction.bidsHistory.bids.sort(function(a, b) {
            return new Date(b.timestamp).getTime() - new Date(a.timestamp).getTime();
        }));
    } else {
        $("<p id='emptyBids'>Offerte assenti</p>").insertBefore($("#bidsTable"));
    }
}

function addAuctionInfo(auction) {
    var htmlAuction = "" + 
    "<div class='auction-description'>" +
    "<div class='auction-image'>" +
        "<img src='" + auction.product.image + "'/>" +
    "</div>" +
    "<div class='auction-information'>" +
        "<p class='auction-title'>" + auction.product.name + "</p>" +
        "<p class='auction-creation-time'>Creata il: " + 
        getFormattedDate(auction.creationTime) + "</p>";
        if (isOngoing) {
            htmlAuction += "" +
            "<div id='auction-remaining-time'>" +
                "<p>Tempo rimanente: </p>" +
                "<p id='auction-timer-0'></p>" +
            "</div>";
        } else {
            htmlAuction += "<p>Terminata il: " +
            getFormattedDate(auction.closingTime) + "</p>";
        }
        htmlAuction += "<p id='actual-price' class='auction-price'>" + 
        actualPrice.toFixed(2) + " €" + " - " + numBids + " offerte" + "</p>";
    htmlAuction += "</div>" + "</div>";
    document.getElementById("auction-container").innerHTML = htmlAuction;

    addAuctionDetails(auction.sellerUsername, auction.product.category,
    auction.product.state, auction.product.description);
}

function addAuctionDetails(sellerUsername, category, state, description) {
    var htmlAuctionDetails = "<h2>Dettagli asta</h2>" +
    "<p><b>Categoria: </b>" + category + "; " +
    "<b>Stato: </b>" + state + "</p>" +
    "<p><b>Venditore: </b>utente " + sellerUsername + "</p>";
    if (description !== null) {
        htmlAuctionDetails += "<p><b>Altre informazioni</b></p><p>" + description + "</p>";
    }
    document.getElementById("auction-details").innerHTML = htmlAuctionDetails;
}

function composeBidTable(bids) {
    var htmlTable = "" +
    "<tr>" +
        "<th>Offerente</th>" +
        "<th>Importo</th>" +
        "<th>Data</th>" +
    "</tr>";
    for (var i = 0; i < bids.length; i++) {
        var bid = bids[i];
        var htmlBid = "" +
        "<tr>" +
            "<td>" + bid.bidderUsername + "</td>" +
            "<td>EUR " + Number(bid.amount).toFixed(2) + "</td>" +
            "<td>" + getFormattedDate(bid.timestamp) + "</td>" +
        "</tr>";
        htmlTable += htmlBid;
    }
    document.getElementById("bidsTable").innerHTML = htmlTable;
}

function getFormattedDate(date) {
    var fullDate = new Date(date);

    var dateOption = {'dateStyle': 'medium'};
    var date = fullDate.toLocaleString('it-IT', dateOption);

    var timeOption = {'timeStyle': 'medium'};
    var time = fullDate.toLocaleString('it-IT', timeOption);

    return date + " alle " + time;
}

// resource: https://stackoverflow.com/questions/10830357/javascript-toisostring-ignores-timezone-offset
function getLocaleDate(date) {
    var fullDate = new Date(date);
    var dateUtc = Date.UTC(fullDate.getUTCFullYear(), fullDate.getUTCMonth(), fullDate.getUTCDate(), 
    fullDate.getUTCHours(), fullDate.getUTCMinutes(), fullDate.getUTCSeconds());
    var tzoffset = fullDate.getTimezoneOffset() * 60000;
    return new Date(dateUtc - tzoffset).toISOString().substring(0, 16);
}

var validatePrice = function(e) {
    var prev = e.getAttribute("data-prev");
    prev = (prev != '') ? prev : '';
    if (Math.round(e.value * 100) / 100 != e.value) {
        e.value = prev;
    }
    e.setAttribute("data-prev", e.value);
}