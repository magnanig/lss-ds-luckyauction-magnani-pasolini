const automationPort = 10203;
const auctionPort = 12345;
const bidsPort = 54321;

const automationsBaseNamespace = "/automations";
const auctionsNamespace = "/auctions/" + auctionId;
const bidsNamespace = "/bids/" + auctionId;

class Socket {
    constructor(host, port, namespace){
        this.socket = io(socketProtocol + "://" + host + ":" + port + namespace, {
            transports: [ "websocket" ],
        });

        this.socket.on("connect_error", function (err) {
            console.log("Errore: " + err);
        });
        
        this.socket.on("connect", function () {
            console.log("Client connesso al server " + namespace);
        });
        
        this.socket.on("disconnect", function () {
            console.log("Client disconnesso");
        });
        
        this.socket.on("reconnect_attempt", (attempts) => {
            console.log("Provo a riconnettermi con il server " + namespace + " (" + attempts + ")");
        });
      }

      addEventListener(eventName, handler) {
          this.socket.on(eventName, handler);
          return this;
      }
}

function startSockets() {
    // connect the socket to handle auction events
    var auctionsSocket = new Socket(auctionsSocketHost, auctionPort, auctionsNamespace);
    auctionsSocket.addEventListener("EXPECTED_CLOSING_TIME_CHANGED", (id, newExpectedClosingTime) => {
        expectedClosingTime = newExpectedClosingTime;
        setTimer();
        enableFormsButtons();
    });
    auctionsSocket.addEventListener("AUCTION_CLOSED", (id, endDate) => {
        isOngoing = false;
        isAutomationActive = false;
        document.getElementById("auction-remaining-time").innerHTML = "<p>Terminata il: " +
        getFormattedDate(endDate) + "</p>";
        if (localStorage.getItem(user_logged) !== sellerUser) {
            removeForms();
        }
    });

    // connect the socket to handle bid raise events
    var bidsSocket = new Socket(bidsSocketHost, bidsPort, bidsNamespace);
    bidsSocket.addEventListener("BID_RAISED", bid => {
        numBids += 1;
        actualPrice = Number(bid.amount);
        if (document.getElementById("bidsTable").rows.length == 0) {
            var bidsArray = [];
            bidsArray.push(bid);
            document.getElementById("emptyBids").remove();
            composeBidTable(bidsArray);
        } else {
            insertBidInTable(bid);
        }
        updateActualPrice();
        if (!isAutomationActive) {
            setAutomationFieldConstraints();
        }
    });
}

function updateActualPrice() {
    document.getElementById("actual-price").innerHTML = actualPrice.toFixed(2) + " €" + 
    " - " + numBids + " offerte";
}

function insertBidInTable(bid) {
    var table = document.getElementById("bidsTable");
    var row = table.insertRow(1);
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2);
    cell1.innerHTML = bid.bidderUsername;
    cell2.innerHTML = "EUR " + actualPrice.toFixed(2);
    cell3.innerHTML = getFormattedDate(bid.timestamp);
  }