var base64image;

/* ----------------- READ INPUT IMAGE AND CONVERT IT TO base64 FORMAT ----------------- */

const fileDataURL = file => new Promise((resolve, reject) => {
    let fr = new FileReader();
    fr.onload = () => resolve(fr.result);
    fr.onerror = reject;
    fr.readAsDataURL(file)
});

function convertToBase64Format(file) {
    if (file.size < 1000000) { // image size limited to 1 MB
        fileDataURL(file)
        .then(data => {
            base64image = data;
        })
        .catch(err => console.log(err));
    } else {
        alert("Image file is too big! Insert an image smaller than 1 MB");
        // unselect image file in html form
        var inputFile = document.getElementById("image");
        inputFile.value = inputFile.defaultValue;
    }
}

/* ------------------------------------------------------------------------------------ */

$(document).ready(function() {
    document.getElementById("title-page").innerHTML = "Aste - utente " + 
    localStorage.getItem(user_logged);
    $.ajax({
        url: baseUrl() + "/api/auctions/histories",
        type: "GET",
        headers: {"Authorization": "Bearer " + localStorage.getItem(access_token)},
        success: function (auctionsHistory, status, xhr) {
            addAuctionsToDivs(auctionsHistory);
            openBoxById("divParticipatingAuctions");
            document.getElementsByTagName("html")[0].style.visibility = "visible";
        },
        error: function (error) {
            alert("Gestione aste - Per gestire le proprie aste effettuare il login");
            logout();
        }
    });
});

function createNewAuction(event) {
    event.preventDefault();
    if (checkForm()) {
        $.ajax({
            url: baseUrl() + "/api/auctions",
            type: "POST",
            contentType: "application/json",
            headers: {"Authorization": "Bearer " + localStorage.getItem(access_token)},
            data: composeJsonAuction(),
            success: function (result, status, xhr) {
                location.reload();
            },
            error: function(error) {
                alert("Gestione aste - Error (" + error.status + "): " + error.responseText);
            }
        });
    }
}

function checkForm() {
    if (!document.querySelector('form').checkValidity()) {
        alert("Error: fill in all mandatory fields marked with (*)");
        return false;
    }
    if (!checkCorrectnessDates()) {
        alert("Date sbagliate. Controllare il log della console per ulteriori dettagli");
        return false;
    }
    return true;
}

// minClosingTime = actualDate + 1min;
// maxClosingTime = actualDate + 7gg;
// minClosingTime <= inputClosingTime <= maxClosingTime

// minExtensionTime = inputClosingTime + 1min;
// maxExtensionTime = inputClosingTime + 7gg;
// minExtensionTime <= inputExtensionTime <= maxExtensionTime

// check the correctness of the dates entered in the auction creation form
function checkCorrectnessDates() {
    var correctness = false;
    var actualDate = new Date();

    var inputClosingTime = new Date($("#closingTime").val());
    var minClosingTime = new Date(actualDate);
    minClosingTime.setMinutes(actualDate.getMinutes() + 1);
    var maxClosingTime = new Date(actualDate);
    maxClosingTime.setDate(actualDate.getDate() + 7);
    // at this point the ClosingTime input has already been set 
    // due to the previous javascript validation
    if (minClosingTime <= inputClosingTime && inputClosingTime <= maxClosingTime) {
        correctness = true;
    } else {
        correctness = false;
        console.log("La data di fine asta non rispetta i vincoli richiesti");
    }

    var inputExtensionTime = new Date($("#expectedExtensionClosingTime").val());
    if (!isNaN(inputExtensionTime) && correctness) {
        var minExtensionTime = new Date(inputClosingTime);
        minExtensionTime.setMinutes(inputClosingTime.getMinutes() + 1);
        var maxExtensionTime = new Date(inputClosingTime);
        maxExtensionTime.setDate(inputClosingTime.getDate() + 7);
        if (minExtensionTime <= inputExtensionTime && inputExtensionTime <= maxExtensionTime) {
            correctness = true;
        } else {
            correctness = false;
            console.log("La data di estensione della durata non rispetta i vincoli richiesti");
        }
    }
    return correctness;
}

function composeJsonAuction() {
    var newAuction = {
        product: {
            name: $("#productName").val(),
            category: $("#productCategory").val(),
            state: $("#productState").val(),
            image: base64image
        },
        expectedClosingTime: new Date($("#closingTime").val()),
        startingPrice: $("#startingPrice").val()
    }

    var auctionDesc = $("#auctionDescription").val();
    if (auctionDesc) {
        newAuction.product["description"] = auctionDesc;
    }

    if ($("#expectedExtensionClosingTime").val().length > 0) {
        newAuction["expectedExtensionClosingTime"] = new Date($("#expectedExtensionClosingTime").val());
    }

    return JSON.stringify(newAuction);
}

var validatePrice = function(e) {
    var prev = e.getAttribute("data-prev");
    prev = (prev != '') ? prev : '';
    if (Math.round(e.value * 100) / 100 != e.value) {
        e.value = prev;
    }
    e.setAttribute("data-prev", e.value)
}

function openBoxById(boxId) {
    var i, tabcontent;
    tabcontent = document.getElementsByClassName("box");
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
    }
    document.getElementById(boxId).style.display = "block";
}