var auctionId;
var auctionType;

$(document).ready(function() {
    const params = new Proxy(new URLSearchParams(window.location.search), {
        get: (searchParams, prop) => searchParams.get(prop),
    });
    auctionId = params.auctionId;
    auctionType = params.auctionType;
    whenDocumentIsReady();
});

function whenDocumentIsReady() {
    $.ajax({
        url: baseUrl() + "/api/auctions/histories/" + auctionType + "/" + auctionId + "/pdf",
        type: "GET",
        responseType: "arraybuffer",
        headers: {"Authorization": "Bearer " + localStorage.getItem(access_token)},
        success: function (result, status, xhr) {
            var base64EncodedPDF = result.pdfReportBytes;
            const blob = b64toBlob(base64EncodedPDF);
            const blobUrl = URL.createObjectURL(blob);

            // to download the pdf with a specific name
            var fileLink = document.createElement('a');
            fileLink.href = blobUrl;
            fileLink.download = "report_auction_" + auctionId;
            fileLink.click();

            // to open the pdf from the browser without downloading
            // window.location = blobUrl;
        },
        error: function (error) {
            alert("Error (" + error.status + "): " + error.responseText);
        }
    });
}

// resource: https://stackoverflow.com/questions/16245767/creating-a-blob-from-a-base64-string-in-javascript
const b64toBlob = (b64Data, sliceSize=512) => {
    const byteCharacters = atob(b64Data);
    const byteArrays = [];
  
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);
  
      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }
  
      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }
  
    const blob = new Blob(byteArrays, {type: 'application/pdf'});
    return blob;
}