var timerCount = 0;
var countdownTimers = new Array();
var expectedClosingTimes = new Array();

function addAuctionsToDivs(auctionsHistory) {
    addCreatedOngoingAuctionsToDiv("divCreatedOngoingAuctions", 
    auctionsHistory.createdOngoingAuctions.sort(function(a, b) {
        return new Date(b.creationTime).getTime() - new Date(a.creationTime).getTime();
    }));
    addParticipatingAuctionsToDiv("divParticipatingAuctions", 
    auctionsHistory.participatingAuctions.sort(function(a, b) {
        return new Date(b.creationTime).getTime() - new Date(a.creationTime).getTime();
    }));
    addCreatedEndedAuctionsToDiv("divCreatedEndedAuctions", 
    auctionsHistory.createdEndedAuctions.sort(function(a, b) {
        return new Date(b.creationTime).getTime() - new Date(a.creationTime).getTime();
    }));
    addAwardedAuctionsToDiv("divAwardedAuctions", 
    auctionsHistory.awardedAuctions.sort(function(a, b) {
        return new Date(b.creationTime).getTime() - new Date(a.creationTime).getTime();
    }));
    addLostAuctionsToDiv("divLostAuctions", 
    auctionsHistory.lostAuctions.sort(function(a, b) {
        return new Date(b.creationTime).getTime() - new Date(a.creationTime).getTime();
    }));
    setAllTimer();
}

function setAllTimer() {
    for (let i = 0; i < expectedClosingTimes.length; i++) {
        var t1 = new Date();
        var t2 = new Date(expectedClosingTimes[i]);
        expectedClosingTimes[i] = Math.round((t2.getTime() - t1.getTime()) / 1000); // duration in seconds
        // the remaining time is displayed immediately without waiting a minute
        updateTimer(i)
        countdownTimers[i] = setInterval(() => updateTimer(i), 60000);
    }
}

function updateTimer(timerIndex) {
    var totalSeconds = expectedClosingTimes[timerIndex];
    var days        = Math.floor(totalSeconds / 86400); // gg:hh:mm:ss
    var hoursLeft   = Math.floor((totalSeconds) - (days * 86400));
    var hours       = Math.floor(hoursLeft / 3600);
    var minutesLeft = Math.floor((hoursLeft) - (hours * 3600));
    var minutes     = Math.floor(minutesLeft / 60);
    document.getElementById("auction-timer-" + timerIndex).innerHTML = days + "g " +
    hours + "h " + minutes + "m ";
    if (totalSeconds <= 0) {
        clearInterval(countdownTimers[timerIndex]);
        document.getElementById("auction-timer-" + timerIndex).innerHTML = "finished";
    } else {
        expectedClosingTimes[timerIndex] -= 60;
    }
}

function addCreatedOngoingAuctionsToDiv(divId, auctions) {
    var title = "<h3>Aste create: in corso</h3>";
    var htmlAuctions = "";
    for (var i = 0; i < auctions.length; i++, timerCount++) {
        var auction = auctions[i];
        expectedClosingTimes[timerCount] = auction.expectedClosingTime;
        var htmlAuction = "<div class='auction-container'>" +
        "<a href='../auction/auction.html?auctionId=" + auction.auctionId + "'>" +
        "<div class='auction-description'>" +
            "<div class='auction-image'>" +
                "<img src='" + auction.product.image + "'/>" +
            "</div>" +
            "<div class='auction-information'>" +
                "<p class='auction-title'>" + auction.product.name + "</p>" +
                "<p class='auction-creation-time'>Creata il: " + 
                getFormattedDate(auction.creationTime) + "</p>" +
                "<div id='auction-remaining-time'>" +
                    "<p>Tempo rimanente: </p>" +
                    "<p id='auction-timer-" + timerCount +"'></p>" +
                "</div>" +
                "<p class='auction-price'>";
                if ("actualBid" in auction && auction.actualBid !== null) {
                    htmlAuction += Number(auction.actualBid.amount).toFixed(2);
                } else {
                    htmlAuction += Number(auction.startingPrice).toFixed(2);
                }
            htmlAuction += " €</p></div>";
        htmlAuction += "</div>" + "</a>" + "</div>";
        htmlAuctions += htmlAuction;
    }
    if (htmlAuctions == "") {
        htmlAuctions = "<p>Non sono presenti aste create in corso</p>"
    }
    document.getElementById(divId).innerHTML = title + htmlAuctions;
}

function addParticipatingAuctionsToDiv(divId, auctions) {
    var title = "<h3>Aste a cui sto partecipando</h3>";
    var htmlAuctions = "";
    for (var i = 0; i < auctions.length; i++, timerCount++) {
        var auction = auctions[i];
        expectedClosingTimes[timerCount] = auction.expectedClosingTime;
        var htmlAuction = "<div class='auction-container'>" +
        "<a href='../auction/auction.html?auctionId=" + auction.auctionId + "'>" +
        "<div class='auction-description'>" +
            "<div class='auction-image'>" +
                "<img src='" + auction.product.image + "'/>" +
            "</div>" +
            "<div class='auction-information'>" +
                "<p class='auction-title'>" + auction.product.name + "</p>" +
                "<p class='auction-creation-time'>Creata il: " + 
                getFormattedDate(auction.creationTime) + "</p>" +
                "<div id='auction-remaining-time'>" +
                    "<p>Tempo rimanente: </p>" +
                    "<p id='auction-timer-" + timerCount +"'></p>" +
                "</div>" +
                "<p class='auction-price'>";
                if ("actualBid" in auction && auction.actualBid !== null) {
                    htmlAuction += Number(auction.actualBid.amount).toFixed(2);
                } else {
                    htmlAuction += Number(auction.startingPrice).toFixed(2);
                }
            htmlAuction += " €</p></div>";
        htmlAuction += "</div>" + "</a>" + "</div>";
        htmlAuctions += htmlAuction;
    }
    if (htmlAuctions == "") {
        htmlAuctions = "<p>Non sono presenti aste a cui stai partecipando</p>"
    }
    document.getElementById(divId).innerHTML = title + htmlAuctions;
}

function addCreatedEndedAuctionsToDiv(divId, auctions) {
    var title = "<h3>Aste create: concluse</h3>";
    var htmlAuctions = "";
    for (var i = 0; i < auctions.length; i++) {
        var auction = auctions[i];
        var winningBidisPresent = "winningBid" in auction && auction.winningBid !== null
        ? true : false;

        var htmlAuction = "<div class='auction-container'>";

        if (winningBidisPresent) {
            htmlAuction += "<a class='download-report' href='./reportPdf.html?auctionType=created" +
            "&auctionId=" + auction.auctionId + "' target='_blank'>Report finale</a>";
        }

        htmlAuction += "<a href='../auction/auction.html?auctionId=" + auction.auctionId + "'>" +
        "<div class='auction-description'>" +
            "<div class='auction-image'>" +
                "<img src='" + auction.product.image + "'/>" +
            "</div>" +
            "<div class='auction-information'>" +
                "<p class='auction-title'>" + auction.product.name + "</p>" +
                "<p class='auction-creation-time'>Creata il: " + 
                getFormattedDate(auction.creationTime) + "</p>" +
                "<p>Terminata il: " + getFormattedDate(auction.closingTime) + "</p>" +
                "<p class='auction-price'>";
                if (winningBidisPresent) {
                    htmlAuction += Number(auction.winningBid.amount).toFixed(2);
                } else {
                    htmlAuction += Number(auction.startingPrice).toFixed(2);
                }
            htmlAuction += " €</p></div>";
        htmlAuction += "</div>" + "</a>" + "</div>";
        htmlAuctions += htmlAuction;
    }
    if (htmlAuctions == "") {
        htmlAuctions = "<p>Non sono presenti aste create concluse</p>"
    }
    document.getElementById(divId).innerHTML = title + htmlAuctions;
}

function addAwardedAuctionsToDiv(divId, auctions) {
    var title = "<h3>Aste vinte</h3>";
    var htmlAuctions = "";
    for (var i = 0; i < auctions.length; i++) {
        var auction = auctions[i];
        var htmlAuction = "" +
        "<div class='auction-container'>" +
        "<a class='download-report' href='./reportPdf.html?auctionType=awarded" +
        "&auctionId=" + auction.auctionId + "' target='_blank'>Report finale</a>" +
        
        "<a href='../auction/auction.html?auctionId=" + auction.auctionId + "'>" +
        "<div class='auction-description'>" +
            "<div class='auction-image'>" +
                "<img src='" + auction.product.image + "'/>" +
            "</div>" +
            "<div class='auction-information'>" +
                "<p class='auction-title'>" + auction.product.name + "</p>" +
                "<p class='auction-creation-time'>Creata il: " + 
                getFormattedDate(auction.creationTime) + "</p>" +
                "<p>Terminata il: " + getFormattedDate(auction.closingTime) + "</p>" +
                "<p class='auction-price'>";
                if ("winningBid" in auction && auction.winningBid !== null) {
                    htmlAuction += Number(auction.winningBid.amount).toFixed(2);
                } else {
                    htmlAuction += Number(auction.startingPrice).toFixed(2);
                }
            htmlAuction += " €</p></div>";
        htmlAuction += "</div>" + "</a>" + "</div>";
        htmlAuctions += htmlAuction;
    }
    if (htmlAuctions == "") {
        htmlAuctions = "<p>Non sono presenti aste vinte</p>"
    }
    document.getElementById(divId).innerHTML = title + htmlAuctions;
}

function addLostAuctionsToDiv(divId, auctions) {
    var title = "<h3>Aste perse</h3>";
    var htmlAuctions = "";
    for (var i = 0; i < auctions.length; i++) {
        var auction = auctions[i];
        var htmlAuction = "<div class='auction-container'>" +
        "<a href='../auction/auction.html?auctionId=" + auction.auctionId + "'>" +
        "<div class='auction-description'>" +
            "<div class='auction-image'>" +
                "<img src='" + auction.product.image + "'/>" +
            "</div>" +
            "<div class='auction-information'>" +
                "<p class='auction-title'>" + auction.product.name + "</p>" +
                "<p class='auction-creation-time'>Creata il: " + 
                getFormattedDate(auction.creationTime) + "</p>" +
                "<p>Terminata il: " + getFormattedDate(auction.closingTime) + "</p>" +
                "<p class='auction-price'>";
                if ("winningBid" in auction && auction.winningBid !== null) {
                    htmlAuction += Number(auction.winningBid.amount).toFixed(2);
                } else {
                    htmlAuction += Number(auction.startingPrice).toFixed(2);
                }
            htmlAuction += " €</p></div>";
        htmlAuction += "</div>" + "</a>" + "</div>";
        htmlAuctions += htmlAuction;
    }
    if (htmlAuctions == "") {
        htmlAuctions = "<p>Non sono presenti aste perse</p>"
    }
    document.getElementById(divId).innerHTML = title + htmlAuctions;
}

function getFormattedDate(date) {
    var fullDate = new Date(date);

    var dateOption = {'dateStyle': 'medium'};
    var date = fullDate.toLocaleString('it-IT', dateOption);

    var timeOption = {'timeStyle': 'medium'};
    var time = fullDate.toLocaleString('it-IT', timeOption);

    return date + " alle " + time;
}