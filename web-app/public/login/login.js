$(document).ready(function() {
    document.getElementsByTagName("html")[0].style.visibility = "visible";
});

function login(event, username, password) {
    event.preventDefault();
    if (document.querySelector('form').checkValidity()) {
        $.ajax({
            url: baseUrl() + "/api/login",
            type: "POST",
            contentType: "application/json",
            crossDomain: true,
            data: JSON.stringify({
                username: username,
                password: password
            }),
            success: function(result, status, xhr) {
                localStorage.setItem(user_logged, result.username);
                localStorage.setItem(access_token, result.access_token);
                window.location.replace("../homepage/homepage.html");
            },
            error: function(error) {
                console.log("Error (" + error.status + "): " + error.responseText);
                alert("Login: Incorrect username or password");
            }
        });
    } else {
        alert("Error: fill in both fields username and password");
    }
}